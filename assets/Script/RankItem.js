// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        labelRank:{
            type:cc.Label,
            default:null
        },

        labelName:{
            type:cc.Label,
            default:null
        },

        labelScore:{
            type:cc.Label,
            default:null
        },

        avator:{
            type:cc.Sprite,
            default:null
        },

        avatorMask:{
            type:cc.Node,
            default:null
        },

        top1Sprite: {
            type:cc.SpriteFrame,
            default:null
        },
    
        top2Sprite: {
            type:cc.SpriteFrame,
            default:null
        },
    
        top3Sprite: {
            type:cc.SpriteFrame,
            default:null
        },
    
        normalSprite: {
            type:cc.SpriteFrame,
            default:null
        },

        rankSprite: {
            type:cc.Sprite,
            default:null
        },
    },

    setUser(user, index, type){
        let nickName = user.nickName ? user.nickName : user.nickname;
        let avatarUrl = user.avatarUrl;
        let kvDataArray = user.KVDataList;
        let kvData = null;
        for (let index = 0; index < kvDataArray.length; index++) {
            if (type == 4) {
                if (kvDataArray[index].key=='limit_level') {
                    kvData = kvDataArray[index];
                    break;
                }
            }else{
                if (kvDataArray[index].key=='level') {
                    kvData = kvDataArray[index];
                    break;
                }
            }
            
            
        }
        
        console.log("setUser user: ", user);
        console.log("setUser kvData: ", kvData);
        if (kvData == null) {
            this.node.active =false;
            return;
        }
        let level;
        if(kvDataArray.length == 0){
            level = 1;
        }else{
            level = kvData["value"];
            if(isNaN(parseInt(level))){
                level = 1;
            
            }
        }
        
        if(index == 1) {
            this.rankSprite.getComponent(cc.Sprite).spriteFrame = this.top1Sprite;
            this.labelRank.node.active =false;
           
            this.setTopRankColor(); 
            
        } else if(index == 2) {
            this.rankSprite.getComponent(cc.Sprite).spriteFrame = this.top2Sprite;
            this.labelRank.node.active =false;
            this.setTopRankColor(); 
        } else if(index == 3) {
            this.rankSprite.getComponent(cc.Sprite).spriteFrame = this.top3Sprite;
            this.labelRank.node.active =false;
            this.setTopRankColor(); 
        } else{
            this.rankSprite.getComponent(cc.Sprite).spriteFrame = null;
            this.labelRank.node.active =true;
            
        }
        // if(kvDataArray.length >= 2){
        //     let levelData = kvDataArray[1];
        //     level = levelData["level"];
        //     console.log("user level" + level);
        //     if(isNaN(parseInt(level))){
        //         level = 1;
        //     }
        // }
 
        // var levelIndex = Math.floor( ( parseInt(level) / 5)  - 0.01);
        // if(levelIndex >= game.levelArray.length){
        //     levelIndex = game.levelArray.length - 1;
        // }


        // console.log("game.levelArray",game.levelArray[levelIndex]);
        // this.levelName.string = game.levelArray[levelIndex];
        // this.levelSprite.spriteFrame = game.levelSpriteArray[levelIndex];
        this.labelRank.string = index + "";
        this.labelScore.string = "第" + level + "关";
        this.labelName.string = nickName;
 
        if(avatarUrl != ""){
            cc.loader.load({
                url: avatarUrl, type: 'png'
            }, (err, texture) => {
                if (err) console.error(err);
                this.avator.spriteFrame = new cc.SpriteFrame(texture);
                console.log("this.avatormask width = " + this.avator.node.width + ",height = " + this.avator.node.height);
            }); 
        }
                          
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // console.log("this.avatormask width = " + this.avatorMask.width + ",height = " + this.avatorMask.height);
    },

    start () {

    },

    // update (dt) {},

    setTopRankColor(){
        this.labelScore.node.color = new cc.Color().fromHEX('#FA6F00');
        this.labelName.node.color = new cc.Color().fromHEX('#FA6F00');
    }
});
