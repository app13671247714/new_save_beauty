

const { ccclass, property } = cc._decorator;
import StorageUtil from './Common/StorageUtil'
import UserData from './Bean/UserData'
const WX = window["wx"];

@ccclass
export default class CoinsUtil {

    storageUtil:StorageUtil = new StorageUtil();
    userData:UserData=null;
    //添加金币
    addCoins(coin:number){
        this.userData = this.storageUtil.getData("userData");
        if (this.userData == null) {
            this.userData = new UserData();
        }
        this.userData.gold += coin; 
        this.storageUtil.saveData('userData', this.userData);

        return this.userData.gold;
    }

    costCoins(coin:number){
        this.userData = this.storageUtil.getData("userData");
        if (this.userData == null) {
            this.userData = new UserData();
        }
        let gold = this.userData.gold;
        gold -= coin; 
        if(gold < 0){
            // if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                // WX.showToast({
                //     title: '金币不足,快去赚金币吧',
                //     icon: 'none',
                //     duration: 2000
                // });
            // }
            return false;
        }
        this.userData.gold = gold;
        this.storageUtil.saveData('userData', this.userData);
        return true;
    }
    
}