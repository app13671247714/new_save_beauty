// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { i18nMgr } from "../i18n/i18nMgr";
import ReportMgr from "./Common/ReportMgr";
import { ResConfig } from "./Framework/Config/ResConfig";
import ResMgr from "./Framework/Managers/ResMgr";
import SoundMgr from "./Framework/Managers/SoundMgr";

const { ccclass, property } = cc._decorator;
@ccclass
export default class AudioController extends cc.Component {


    @property(cc.SpriteFrame)
    openAudio: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    openAudioClick: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    closeAudio: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    closeAudioClick: cc.SpriteFrame = null;



    //点击按钮
    // @property({
    //     type: cc.AudioClip
    // })
    // audioClickButton: cc.AudioClip = null;

    // //倒计时
    // @property({
    //     type: cc.AudioClip
    // })
    // audioCountDown: cc.AudioClip = null;

    // //获得金币
    // @property({
    //     type: cc.AudioClip
    // })
    // audioGetCoins: cc.AudioClip = null;

    // //胜利过关
    // @property({
    //     type: cc.AudioClip
    // })
    // audioGamePass: cc.AudioClip = null;

    // //箭头提示
    // @property({
    //     type: cc.AudioClip
    // })
    // audioArrowNotify: cc.AudioClip = null;

    // //移动提示
    // @property({
    //     type: cc.AudioClip
    // })
    // audioMove: cc.AudioClip = null;

    // //点击金币
    // @property({
    //     type: cc.AudioClip
    // })
    // audioCoinClick: cc.AudioClip = null;

    // //捡猫币弹框提示
    // @property({
    //     type: cc.AudioClip
    // })
    // audioDialogShow: cc.AudioClip = null;

    // //小猫叫
    // @property({
    //     type: cc.AudioClip
    // })
    // audioMiao: cc.AudioClip = null;
    // @property({
    //     type: cc.AudioClip
    // })
    // startAudio: cc.AudioClip = null;
    // @property({
    //     type: cc.AudioClip
    // })
    // moveEditAudio: cc.AudioClip = null;
    // @property({
    //     type: cc.AudioClip
    // })
    // gameStartAudio: cc.AudioClip = null;
    // @property({
    //     type: cc.AudioClip
    // })
    // bgAudio: cc.AudioClip[] = [];
    // @property({
    //     type: cc.AudioClip
    // })
    // helpAudio: cc.AudioClip[] = [];
    // @property({
    //     type: cc.AudioClip
    // })
    // endAudio: cc.AudioClip[] = [];
    @property({
        type: cc.Node
    })
    adNode: cc.Node = null;

    isOpenAudio = true;


     onLoad() {
        let isOpen = cc.sys.localStorage.getItem("is_audio_open");
        if (isOpen == null || this.isNull(isOpen)) {
            isOpen = "1";
            cc.sys.localStorage.setItem("is_audio_open", isOpen);
        }

        if (Number(isOpen) == 1) {
            this.isOpenAudio = true;
        } else {
            this.isOpenAudio = false;
        }

        this.node.addComponent(ResMgr);
        this.node.addComponent(SoundMgr);
        ResMgr.Instance.loadAllBundles(ResConfig.AllBundle, (now: number, total: number) => {
            console.log(now, total);
        }, () => {
            console.log('配置的AB包全部加载完成!!!');
            this.preLoadAllSound();
        })
    }

    //预加载声音包资源到内存
    async preLoadAllSound(){
        await ResMgr.Instance.preLoadAssetFromBundle(ResConfig.SoundBundle,cc.AudioClip,(now:number , total:number) =>{
            console.log(now, total);
        })
        console.log('SoundBundle AB包资源加载完毕');
    }

    isNull(str) {
        if (typeof (str) == "undefined" || str == "") {
            return true;
        } else {
            return false;
        }
    }

    playCountDownAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioCountDown, false, 1);
            SoundMgr.Instnce.playSound("count_down")
        }
    }

    playGetCoinsAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioGetCoins, false, 1);
            SoundMgr.Instnce.playSound("get_coins")
        }
    }

    playClickButtonAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioClickButton, false, 1);
            SoundMgr.Instnce.playSound("btn_click");
        }
    }

    playGamePassAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioGamePass, false, 1);
            SoundMgr.Instnce.playSound("passLevel")
        }
    }

    playArrowNotifyAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioArrowNotify, false, 1);
            SoundMgr.Instnce.playSound("arrow_notify")
        }
    }

    playMoveAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioMove, false, 1);
            SoundMgr.Instnce.playSound("moveLine")
        }
    }

    playCoinClickAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioCoinClick, false, 1);
            SoundMgr.Instnce.playSound("pick_gold")
        }
    }

    playDialogShowAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.audioDialogShow, false, 1);
            SoundMgr.Instnce.playSound("pick_gold_dialog")
        }
    }

    // playCatAudio() {

    //     if (this.isOpenAudio) {
    //         // cc.audioEngine.play(this.audioMiao, false, 1);
    //     }
    // }
    playStartAudio() {

        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.startAudio, false, 1);
            SoundMgr.Instnce.playSound("start")
        }
    }
    playMoveEditAudio() {
        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.moveEditAudio, false, 1);
            SoundMgr.Instnce.playSound("moveBack")
        }
    }
    playGameStartAudio() {
        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.gameStartAudio, false, 1);
            SoundMgr.Instnce.playSound("start")
        }
    }
    playGameHelpAudio() {
        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.helpAudio[i18nMgr.getLanguage() == "pt" ? 1 : 0], false, 1);
            if(i18nMgr.getLanguage() == "pt"){
                SoundMgr.Instnce.playSound("pt-BR-Help-me");
            }else{
                SoundMgr.Instnce.playSound("en-US-Help-me");
            }
        }
    }
    playGameEndAudio() {
        if (this.isOpenAudio) {
            // cc.audioEngine.play(this.endAudio[i18nMgr.getLanguage() == "pt" ? 1 : 0], false, 1);
            if(i18nMgr.getLanguage() == "pt"){
                SoundMgr.Instnce.playSound("pt-BR-I-love-you");
            }else{
                SoundMgr.Instnce.playSound("en-US-I-love-you");
            }
        }
    }
    playBgMusic(id?) {
        cc.log(id, "id")
        if (this.isOpenAudio) {
            // cc.audioEngine.playMusic(this.bgAudio[id], true);
            SoundMgr.Instnce.playBgMusic("ad" + (id + 1),true);
        }
    }
    stopMusic() {
        // cc.audioEngine.stopMusic()
        SoundMgr.Instnce.stopBgMusic()
    }

    soundBtnClick() {
        this.playClickButtonAudio();
        if (this.isOpenAudio) {
            this.isOpenAudio = false;
            this.adNode.getComponent(cc.Sprite).spriteFrame = this.closeAudio;
        } else {
            this.isOpenAudio = true;
            this.adNode.getComponent(cc.Sprite).spriteFrame = this.openAudio;
        }
        SoundMgr.Instnce.setMusicMute(this.isOpenAudio)
        SoundMgr.Instnce.setSoundsMute(this.isOpenAudio)
        ReportMgr.getInst().uploadEvent("click_audio_switch", "page_set", undefined, undefined, undefined, this.isOpenAudio ? 1 : 0)
        cc.sys.localStorage.setItem("is_audio_open", this.isOpenAudio ? 1 : 0);
    }
    showUI() {
        if (this.isOpenAudio) {
            this.adNode.getComponent(cc.Sprite).spriteFrame = this.openAudio;
        } else {
            this.adNode.getComponent(cc.Sprite).spriteFrame = this.closeAudio;
        }
    }

    start() {

    }

}
