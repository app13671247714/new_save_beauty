// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import { i18nMgr } from '../i18n/i18nMgr';
import UserData from './Bean/UserData'
import ReportMgr from './Common/ReportMgr';
import StorageUtil from './Common/StorageUtil'
import GlobalConst from './GlobalConst';

const WX = window["wx"];

@ccclass
export default class ThemeScrollControl extends cc.Component {

    @property(cc.Prefab)
    m_ThemeGridPrefab: cc.Prefab = null;

    @property(cc.Node)
    m_ThemeView: cc.Node = null;
    @property(cc.Node)
    m_InnerGridView: cc.Node = null;
    @property(cc.PageView)
    m_ThemePage: cc.PageView = null;

    @property(cc.Node)
    leftNode: cc.Node = null;
    @property(cc.Node)
    rightNode: cc.Node = null;

    //解锁的标题描述
    @property(cc.Label)
    m_unLockLabel: cc.Label = null;
    //
    @property(cc.Label)
    lockLabel: cc.Label = null;
    //
    @property(cc.Label)
    totalLabel: cc.Label = null;

    @property(cc.ProgressBar)
    progress:cc.ProgressBar = null;


    m_ThemeGridArray: Array<cc.Node> = new Array<cc.Node>();

    userData: UserData = null;
    storageUtil: StorageUtil = new StorageUtil();
    globalConst: GlobalConst = new GlobalConst();


    m_ArrayLength: number = this.globalConst.themeCount;


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.initThemeArray();
        let lv = this.storageUtil.getData('playLevel');
        cc.log(lv, "playLevel")
        if (lv) {
            this.pageNum = lv - 1
        }
        this.scheduleOnce(() => {
            this.skip()
        }, 0.1)

    }

    protected start(): void {
        console.log("主题尺寸1："+ this.node.width + this.node.height);
    }

    initThemeArray() {
        for (let index = 0; index < this.m_ArrayLength; index++) {
            this.m_ThemeGridArray[index] = cc.instantiate(this.m_ThemeGridPrefab);

            this.m_ThemePage.addPage(this.m_ThemeGridArray[index]);
            this.m_ThemeGridArray[index].getChildByName('ThemeGridLayout').getComponent('ThemeGridControl').setTheme(index + 1);
            this.m_ThemeGridArray[index].getChildByName('ThemeGridLayout').getComponent('ThemeGridControl').setThemeView(this.m_ThemeView);
            this.m_ThemeGridArray[index].getChildByName('ThemeGridLayout').getComponent('ThemeGridControl').setInnerGird(this.m_InnerGridView);
            // this.m_ThemeGridArray[index].height = this.node.height * 0.8;
            // this.m_ThemeGridArray[index].getChildByName('ThemeGridLayout').getComponent('ThemeGridControl').initThemeBoxData();
        }

        this.getUnlockNum();
    }

    // update (dt) {}
    getUnlockNum() {
        this.userData = this.storageUtil.getData('userData');
        if (this.userData == null) {
            this.userData = new UserData();
        }
        // let Membership = cc.sys.localStorage.getItem('Membership')

        // let str;
        // if (Membership) {
        //     str = i18nMgr._getLabel("txt_05") + this.globalConst.themeCount * this.globalConst.chapterCount + "/" + this.globalConst.themeCount * this.globalConst.chapterCount

        // } else {
        //     if ((this.globalConst.themeCount * this.globalConst.chapterCount) < mainTs.unLockThemeCount) {
        //         mainTs.unLockThemeCount = this.globalConst.themeCount * this.globalConst.chapterCount
        //     }
        //     str = i18nMgr._getLabel("txt_05") + (mainTs.unLockThemeCount).toString() + "/" + this.globalConst.themeCount * this.globalConst.chapterCount
        // }
        // this.m_unLockLabel.string = str// '成功解锁  ' ;


        this.refreshThemeProgress();

    }

    //刷新主题进度
    refreshThemeProgress(){
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        this.lockLabel.string = (mainTs.unLockThemeCount).toString();
        this.totalLabel.string = "/" + this.globalConst.themeCount * this.globalConst.chapterCount
        let progress = mainTs.unLockThemeCount / (this.globalConst.themeCount * this.globalConst.chapterCount)
        this.progress.progress = Math.max(progress,0.05)
    }


    refreshTheme() {
        for (let index = 0; index < this.m_ThemeGridArray.length; index++) {

            this.m_ThemeGridArray[index].getChildByName('ThemeGridLayout').getComponent('ThemeGridControl').initThemeBoxData1();
        }

        this.getUnlockNum();
    }
    public pageNum = 0;
    //主题右边切换按钮点击
    btnRight() {
        if (this.pageNum == this.globalConst.themeCount - 1) {
            return;
        }
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.pageNum++;
        this.skip(undefined,true)
        // ReportMgr.getInst().uploadEvent("click_pageRight","pageIndex-" + this.pageNum)
    }
    skip(page?,isfromBtn?) {
        if (page) this.pageNum = page
        if (this.pageNum == this.globalConst.themeCount - 1) {
            this.rightNode.active = false;
            this.leftNode.active = true;
        } else if (this.pageNum == 0) {
            this.leftNode.active = false;
            this.rightNode.active = true;
        } else {
            this.leftNode.active = true;
            this.rightNode.active = true;
        }
        this.m_ThemePage.scrollToPage(this.pageNum, 0.3)
        let theme = this.pageNum + 1;
        if(!isfromBtn){//从按钮点击事件过来不重复上报
            ReportMgr.getInst().uploadEvent("evt_show_theme","page_home",theme);
        }
    }
    //主题翻页滑动
    skinEvent(event) {
        this.pageNum = this.m_ThemePage.getCurrentPageIndex()
        this.skip()
    }
    //主题左边切换按钮点击
    btnLeft() {
        if (this.pageNum == 0) {
            return;
        }
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.pageNum--;
        this.skip(undefined,true)
    }

}
