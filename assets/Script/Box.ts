// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import GameControl from './GameControl'
import GlobalConst from './GlobalConst';
import LevelUtils from './LevelUtils';

@ccclass
export default class Box extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    //0表示未绘制，联通区域
    //1表示已绘制
    //-1表示不可到达
    value: number = 0;

    //bodynode的缩放比例
    scale: number = 0;

    //node的原始宽度
    originWidth: number = 0;

    gameControl: GameControl = null;

    x_Index: number;
    y_Index: number;

    m_Oritation: String = "Bottom";

    @property(cc.SpriteFrame)
    drawedFrame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    undrawFrame: cc.SpriteFrame = null;

    @property(cc.Prefab)
    bodyPrefab: cc.Prefab = null;

    @property(cc.Node)
    arrowNode: cc.Node = null;

    @property(cc.SpriteFrame)
    arrowSprite: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    arrowEndSprite: cc.SpriteFrame = null;

    @property(cc.Node)
    bgNode: cc.Node = null;

    bodyNode: cc.Node = null;

    bodyNodeOriginVec: cc.Vec2;
    move2Vec: cc.Vec2 = cc.v2();

    isMovingForward: boolean = false;
    isMovingBackward: boolean = false;

    scale2X: number = 1;
    scale2Y: number = 1;
    originScale: number = 1;

    //要放大到的宽度
    scale2Width: number = 0;
    //要放大到的高度
    scale2Height: number = 0;

    move2X: number = 0;
    move2Y: number = 0;

    forwardOri: String = '';
    forwardAnimTime: number = 0;

    backwardAnimTime: number = 0;

    forwardStartTime: number = 0;
    backwardStartTime: number = 0;

    drawedColor: cc.Color = new cc.Color();
    defaultColor: cc.Color = new cc.Color().fromHEX('#418D90');

    arrowColor: cc.Color = new cc.Color();

    globalConst: GlobalConst = new GlobalConst();
    levelUtil: LevelUtils = new LevelUtils();





    // LIFE-CYCLE CALLBACKS:


    onLoad() {
        this.gameControl = this.node.getParent().getComponent('GameControl');

        // this.drawedColor.fromHEX('#DD3800');


        // this.node.color = this.drawedColor;
        // this.bgNode.color = new cc.Color().fromHEX('#DD3800');

    }


    processTouch(event: cc.Event.EventTouch) {
        if (this.value == -1) {
            return;
        }

        if (this.gameControl != null) {

            //表示该节点没有画到
            if (this.value == 0) {
                this.gameControl.hasAroundNode(this.x_Index, this.y_Index);
                // if(this.m_Oritation !== "null"){
                //     this.node.getComponent(cc.Sprite).spriteFrame = this.drawedFrame;

                // }
            }
            //表示该节点已经画过了
            else {
                this.gameControl.popPathArrayList(this.node);
            }

        }
    }

    setValue(value: number, scale: number) {
        this.value = value;
        this.scale = scale;
        if (this.bodyNode != null) {
            this.initBoxWidth();
        }

        this.changeThemeColor();
    }

    //回退操作时重置属性
    reset(resetNotify) {
        this.value = 0;
        this.m_Oritation = "null";
        this.node.getComponent(cc.Sprite).spriteFrame = this.undrawFrame;
        this.node.color = this.defaultColor;
        if (this.bodyNode != null) {
            this.bodyNode.opacity = 0;
            // this.bodyNode.active=false;
        }
        // this.isMovingBackward = false;
        // this.isMovingForward = false;
        if (resetNotify) {
            // this.arrowNode.active = false;
            this.arrowNode.opacity = 0;
        }

        this.node.scale = 1;
        this.bgNode.opacity = 0;

    }



    generateBodyNode() {
        // this.bodyNode = new cc.Node();
        // this.bodyNode.addComponent(cc.Sprite);
        this.bodyNode = cc.instantiate(this.bodyPrefab);

        // this.bodyNode.getComponent(cc.Sprite).spriteFrame = this.drawedFrame;
        this.node.getParent().getParent().getChildByName("body").addChild(this.bodyNode);
        this.initBoxWidth();
        // this.scale-=0.1;
        // this.bodyNode.scale = this.scale;


        // this.bodyNode.width = (this.node.width+2)/this.scale;
        // this.bodyNode.height = (this.node.height+2)/this.scale;

        // this.originWidth = this.bodyNode.width;


        var worldVec = this.node.getParent().convertToWorldSpaceAR(this.node.getPosition());
        var rootSpaceVec = this.node.getParent().getParent().convertToNodeSpaceAR(worldVec);

        this.bodyNode.setPosition(rootSpaceVec);
        this.bodyNodeOriginVec = rootSpaceVec;

        this.bodyNode.opacity = 0;

        this.bodyNode.color = this.drawedColor;
        // this.bodyNode.color = new cc.Color().fromHEX('#1DD62C');
        // this.bodyNode.active = false;
    }

    playForward(oritation: String, time: number, size: number) {
        if (this.bodyNode == null) {
            this.generateBodyNode();
        }

        this.forwardAnimTime = time;
        this.bodyNode.opacity = 255;
        // this.bodyNode.active =true;


        this.setBodyNodeState(oritation, size);


        // if(this.isMovingBackward){
        this.bodyNode.width = this.originWidth;
        this.bodyNode.height = this.originWidth;
        // this.bodyNode.setScale(this.originScale, this.originScale);
        this.bodyNode.setPosition(this.bodyNodeOriginVec);
        this.isMovingBackward = false;
        // }
        this.bodyNode.stopAllActions();

        this.isMovingForward = true;
        this.forwardStartTime = Date.now();
        // let spawn = cc.spawn(
        //     // cc.scaleTo(time, this.scale2X, this.scale2Y),
        //     cc.moveTo(time, this.move2Vec));
        let seq = cc.sequence(cc.delayTime(time), cc.callFunc(function () {
            this.isMovingForward = false;




            this.bodyNode.width = this.scale2Width;
            this.bodyNode.height = this.scale2Height;
            this.bodyNode.setPosition(this.move2Vec);
        }.bind(this)));
        this.bodyNode.runAction(seq);
    }

    playBackward(time: number, oritation: String) {
        if (this.bodyNode == null) {
            this.generateBodyNode();
        }


        this.backwardAnimTime = time;



        // if(this.isMovingForward){
        this.bodyNode.opacity = 255;
        // this.bodyNode.active =false;

        //往回移动时要将size的值再置回没有等比缩放的大小
        let size = Math.max(this.scale2Width, this.scale2Height) * this.scale;

        this.setBodyNodeState(oritation, size);

        this.bodyNode.width = this.scale2Width;
        this.bodyNode.height = this.scale2Height;
        // this.bodyNode.setScale(this.scale2X, this.scale2Y);
        this.bodyNode.setPosition(this.move2Vec);
        this.isMovingForward = false;

        // }
        this.bodyNode.stopAllActions();

        this.isMovingBackward = true;
        this.forwardStartTime = Date.now();
        // let spawn = cc.spawn(
        // cc.scaleTo(time, this.originScale, this.originScale),
        // cc.moveTo(time, this.bodyNodeOriginVec));
        let seq = cc.sequence(cc.delayTime(time), cc.callFunc(function () {
            this.bodyNode.opacity = 0;


            this.isMovingBackward = false;
            this.scale2Width = this.originWidth;
            this.scale2Height = this.originWidth;
            this.move2Vec = cc.v2(this.bodyNodeOriginVec);

            this.bodyNode.width = this.originWidth;
            this.bodyNode.height = this.originWidth;
            this.bodyNode.setPosition(this.move2Vec);



        }.bind(this)));

        this.bodyNode.runAction(seq);

    }

    setBodyNodeState(oritation: String, size: number) {
        this.forwardOri = oritation;
        switch (oritation) {
            case "Left":
                // this.bodyNode.anchorX =1;
                // this.bodyNode.x = this.bodyNodeOriginVec.x + this.node.width/2;
                this.move2Vec.x = this.bodyNodeOriginVec.x + this.node.width / 2 - size / 2;
                this.move2Vec.y = this.bodyNodeOriginVec.y;


                this.scale2Width = size / this.scale;
                this.scale2Height = this.originWidth;
                // this.scale2X = scale;   
                // this.scale2Y = this.originScale;   
                break;
            case "Top":
                // this.bodyNode.anchorY =0;
                // this.bodyNode.y = this.bodyNodeOriginVec.y - this.node.height/2;
                this.move2Vec.y = this.bodyNodeOriginVec.y - this.node.height / 2 + size / 2;
                this.move2Vec.x = this.bodyNodeOriginVec.x;


                this.scale2Width = this.originWidth;
                this.scale2Height = size / this.scale;
                // this.scale2Y = scale;  
                // this.scale2X = this.originScale;    
                break;
            case "Right":
                // this.bodyNode.anchorX =0;
                // this.bodyNode.x = this.bodyNodeOriginVec.x - this.node.width/2;
                this.move2Vec.x = this.bodyNodeOriginVec.x - this.node.width / 2 + size / 2;
                this.move2Vec.y = this.bodyNodeOriginVec.y;

                this.scale2Width = size / this.scale;
                this.scale2Height = this.originWidth;
                // this.scale2X = scale;   
                // this.scale2Y = this.originScale;   
                break;
            case "Bottom":
                // this.bodyNode.anchorY =1;
                // this.bodyNode.y = this.bodyNodeOriginVec.y + this.node.height/2;
                this.move2Vec.y = this.bodyNodeOriginVec.y + this.node.height / 2 - size / 2;
                this.move2Vec.x = this.bodyNodeOriginVec.x;

                this.scale2Width = this.originWidth;
                this.scale2Height = size / this.scale;
                // this.scale2Y = scale;   
                // this.scale2X = this.originScale;   
                break;
        }
    }
    update(dt) {
        //向前移动

        let deltaTime = (Date.now() - this.forwardStartTime) / 1000;
        if (this.isMovingForward) {
            let bodywidth = this.originWidth + (this.scale2Width - this.originWidth) / this.forwardAnimTime * deltaTime;
            let bodyX = this.bodyNodeOriginVec.x + (this.move2Vec.x - this.bodyNodeOriginVec.x) / this.forwardAnimTime * deltaTime;
            let bodyHeight = this.originWidth + (this.scale2Height - this.originWidth) / this.forwardAnimTime * deltaTime;
            let bodyY = this.bodyNodeOriginVec.y + (this.move2Vec.y - this.bodyNodeOriginVec.y) / this.forwardAnimTime * deltaTime;
            switch (this.forwardOri) {
                case "Left":
                    this.bodyNode.height = this.originWidth;

                    if (bodywidth > this.scale2Width) {

                        bodywidth = this.scale2Width;
                    }
                    this.bodyNode.width = bodywidth;
                    if (bodyX < this.move2Vec.x) {
                        bodyX = this.move2Vec.x;
                    }
                    this.bodyNode.x = bodyX;
                    break;
                case "Right":
                    this.bodyNode.height = this.originWidth;

                    if (bodywidth > this.scale2Width) {

                        bodywidth = this.scale2Width;
                    }
                    this.bodyNode.width = bodywidth;



                    if (bodyX > this.move2Vec.x) {
                        bodyX = this.move2Vec.x;
                    }
                    this.bodyNode.x = bodyX;
                    break;
                case "Top":
                    this.bodyNode.width = this.originWidth;

                    if (bodyHeight > this.scale2Height) {

                        bodyHeight = this.scale2Height;
                    }
                    this.bodyNode.height = bodyHeight;


                    if (bodyY > this.move2Vec.y) {
                        bodyY = this.move2Vec.y;
                    }
                    this.bodyNode.y = bodyY;
                    break;
                case "Bottom":
                    this.bodyNode.width = this.originWidth;

                    if (bodyHeight > this.scale2Height) {

                        bodyHeight = this.scale2Height;
                    }
                    this.bodyNode.height = bodyHeight;


                    if (bodyY < this.move2Vec.y) {
                        bodyY = this.move2Vec.y;
                    }
                    this.bodyNode.y = bodyY;
                    break;
            }

        }

        //向后移动

        if (this.isMovingBackward) {
            let bodywidth = this.scale2Width - (this.scale2Width - this.originWidth) / this.forwardAnimTime * deltaTime;
            let bodyX = this.move2Vec.x - (this.move2Vec.x - this.bodyNodeOriginVec.x) / this.forwardAnimTime * deltaTime;
            let bodyHeight = this.scale2Height - (this.scale2Height - this.originWidth) / this.forwardAnimTime * deltaTime;
            let bodyY = this.move2Vec.y - (this.move2Vec.y - this.bodyNodeOriginVec.y) / this.forwardAnimTime * deltaTime;
            switch (this.forwardOri) {
                case "Left":
                    this.bodyNode.height = this.originWidth;
                    if (bodywidth < this.node.width) {
                        bodywidth = this.node.width;
                    }
                    this.bodyNode.width = bodywidth;


                    if (bodyX > this.bodyNodeOriginVec.x) {
                        bodyX = this.bodyNodeOriginVec.x;
                    }
                    this.bodyNode.x = bodyX;
                    break;
                case "Right":
                    this.bodyNode.height = this.originWidth;
                    if (bodywidth < this.node.width) {
                        bodywidth = this.node.width;
                    }
                    this.bodyNode.width = bodywidth;


                    if (bodyX < this.bodyNodeOriginVec.x) {
                        bodyX = this.bodyNodeOriginVec.x;
                    }
                    this.bodyNode.x = bodyX;

                    break;
                case "Top":
                    this.bodyNode.width = this.originWidth;
                    if (bodyHeight < this.node.height) {
                        bodyHeight = this.node.height;
                    }
                    this.bodyNode.height = bodyHeight;


                    if (bodyY < this.bodyNodeOriginVec.y) {
                        bodyY = this.bodyNodeOriginVec.y;
                    }
                    this.bodyNode.y = bodyY;

                    break;
                case "Bottom":
                    this.bodyNode.width = this.originWidth;
                    if (bodyHeight < this.node.height) {
                        bodyHeight = this.node.height;
                    }
                    this.bodyNode.height = bodyHeight;


                    if (bodyY > this.bodyNodeOriginVec.y) {
                        bodyY = this.bodyNodeOriginVec.y;
                    }
                    this.bodyNode.y = bodyY;

                    break;
            }
        }
    }

    notifyClicked(oritation: String, isLastNode: boolean) {
        this.arrowNode.width = this.node.width * (1 - this.arrowNode.getComponent(cc.Widget).left - this.arrowNode.getComponent(cc.Widget).right);
        this.arrowNode.height = this.node.width * (1 - this.arrowNode.getComponent(cc.Widget).top - this.arrowNode.getComponent(cc.Widget).bottom);

        if (isLastNode) {
            this.arrowNode.getComponent(cc.Sprite).spriteFrame = this.arrowEndSprite;
        } else {
            this.arrowNode.getComponent(cc.Sprite).spriteFrame = this.arrowSprite;
        }
        var action = cc.fadeIn(0.1);
        cc.find('Canvas').getComponent('MainControl').getVolumeControl().playMoveAudio();
        this.arrowNode.runAction(action);
        // this.arrowNode.active = true;
        switch (oritation) {
            case "Left":
                this.arrowNode.angle = 90;
                break;
            case "Top":
                this.arrowNode.angle = 0;
                break;
            case "Right":
                this.arrowNode.angle = -90;
                break;
            case "Bottom":
                this.arrowNode.angle = -180;
                break;
        }
    }

    //移除bodyNode
    removeBodyNode() {
        this.node.getParent().getParent().getChildByName("body").removeChild(this.bodyNode);
        this.bodyNode = null;
        this.isMovingBackward = false;
        this.isMovingForward = false;
        // this.arrowNode.active = false;
        this.arrowNode.opacity = 0;
        this.node.scale = 1;
        this.bgNode.opacity = 0;
        this.node.getComponent(cc.Sprite).spriteFrame = this.undrawFrame;
        this.node.color = this.defaultColor;

    }

    //节点的提示动画
    showNotifyAnim() {
        let scaleAnim = cc.scaleTo(0.1, 0.8);
        this.node.runAction(scaleAnim);
        this.bgNode.runAction(cc.fadeIn(0.1));
    }

    cancelNotifyAnim() {
        let scaleAnim = cc.scaleTo(0.1, 1);
        this.node.runAction(scaleAnim);
        this.bgNode.runAction(cc.fadeOut(0.1));
    }

    changeThemeColor() {

        if (this.gameControl == null) {
            this.gameControl = this.node.getParent().getComponent('GameControl');
        }
        let currentLevel = this.gameControl.currentLevel;
        let isLimitGame = this.gameControl.isLimitGame;
        if (isLimitGame) {
            this.drawedColor = new cc.Color().fromHEX('#FCAE00');
            this.defaultColor = new cc.Color().fromHEX('#FFFFFF');
            this.arrowColor = new cc.Color().fromHEX('#6D2CFF');
            this.node.color = this.defaultColor;
            if (this.value > 0) {
                this.node.getComponent(cc.Sprite).spriteFrame = this.drawedFrame;
                this.node.color = this.drawedColor;
            }
            // this.arrowNode.color = this.arrowColor;
        } else {
            let theme = Math.ceil(currentLevel / this.globalConst.chapterCount / this.globalConst.levelCount);
            //更换主题颜色
            this.levelUtil.loadThemeJson(function (json) {


                this.drawedColor = new cc.Color().fromHEX("#0BF9ED");
                // this.drawedColor = new cc.Color().fromHEX(json[theme].body);
                this.defaultColor = new cc.Color().fromHEX("#FFFFFF"); //new cc.Color().fromHEX(json[theme].node);
                this.arrowColor = new cc.Color().fromHEX(json[theme].arrow);
                this.node.color = this.defaultColor;
                if (this.value > 0) {
                    this.node.getComponent(cc.Sprite).spriteFrame = this.drawedFrame;
                    this.node.color = this.drawedColor;
                }
                // this.arrowNode.color = this.arrowColor;

            }.bind(this));
        }

    }


    setOritation(oritation: string) {
        this.m_Oritation = oritation;
    }

    initBoxWidth() {

        // this.scale-=0.1;
        // this.bodyNode.scale = this.scale;


        // this.bodyNode.width = (this.node.width+2)/this.scale;
        // this.bodyNode.height = (this.node.height+2)/this.scale;

        // this.scale = 0.8;
        this.bodyNode.scale = this.scale;
        this.bodyNode.width = this.node.width / this.scale;
        this.bodyNode.height = this.node.width / this.scale;


        this.originWidth = this.bodyNode.width;

    }

}
