// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class DialogAnimation extends cc.Component {

    // LIFE-CYCLE CALLBACKS:
    originScale: number = 1;

    needUpdate: boolean = false;

    start() {

    }

    onEnable() {
        this.originScale = this.node.scale;
        this.needUpdate = true;
        this.node.scale = 0;
        // this.scheduleOnce(function(){
        // this.show(true);
        // }.bind(this), 0.1);
    }

    lateUpdate() {
        if (this.needUpdate) {
            this.needUpdate = false;
            this.show(true);
        }
    }

    show(isScale: boolean) {
        // console.log('show node y 0= '+this.node.y);

        let targetScale = this.originScale;
        // console.log('show node this.node.getComponent(cc.Widget).top '+this.node.getComponent(cc.Widget).top);
        this.node.y = this.node.getParent().height / 2 - this.node.getComponent(cc.Widget).top * this.node.getParent().height - this.node.height / 2;
        // this.node.scale = 0;
        // if (isScale) {
        //     let srcScaleForShowAll = Math.min(cc.winSize.width / 540, cc.winSize.height / 960);
        //     targetScale = this.originScale * srcScaleForShowAll;
        // }
        // console.log('show node targetScale = '+targetScale);
        let action = cc.scaleTo(1, targetScale);
        action.easing(cc.easeElasticOut(1));
        this.node.stopAllActions();
        // console.log('show node y = '+this.node.y);
        // console.log('show node parent heigth = '+this.node.getParent().height);
        this.node.runAction(action);
    }

    showRank() {
        let targetScale = this.node.scale;
        let screenHeight = cc.winSize.height;
        // console.log("wangjinfeng node height = " + this.node.height);

        this.node.scale = 0;
        this.node.y = screenHeight / 2 - 0.126 * screenHeight - this.node.height / 2;


        let action = cc.scaleTo(1, targetScale, targetScale);
        action.easing(cc.easeElasticOut(1));
        let action2 = cc.sequence(action, cc.callFunc(function () {
            this.node.y = screenHeight / 2 - 0.126 * screenHeight - this.node.height / 2;
        }.bind(this)));
        this.node.stopAllActions();
        this.node.runAction(action2);
    }
}
