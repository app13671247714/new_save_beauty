export default class AnalyticsUtilities {

    public static async logEvent(event: string, bundle: object = null) {
        let target = null;
        if (typeof gmbox != 'undefined' && gmbox["logEvent"]) {
            if (bundle != null) {
                bundle['event'] = event;
            } else {
                bundle = {
                    event: event
                }
            }
            gmbox["logEvent"](bundle);
            target = "gmbox";
        }

        if (typeof FBInstant !== 'undefined' && FBInstant['logEvent']) {
            FBInstant['logEvent'](event, 1, bundle);
            target = "FB";
        }

        if (typeof wx !== 'undefined' && wx['aldSendEvent']) {
            wx['aldSendEvent'](event, bundle);
            target = "wx";
        }

        console.info("打点" + target + ": event = " + event + ", bundle = " + JSON.stringify(bundle));
    }
}
