// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class RedPackage extends cc.Component {

    gameview: any = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    }

    start() {

    }
    onEnable() {
        this.node.x = -cc.winSize.width / 2;
        let action = cc.moveTo(10, cc.v2(cc.winSize.width, this.node.y));
        let action2 = cc.moveTo(10, cc.v2(-cc.winSize.width / 2, this.node.y));
        let seq = cc.sequence(action, action2);
        this.node.runAction(seq.repeatForever());
    }

    onClick() {

        if (this.gameview) {
            this.gameview.m_AwardView.getComponent('AwardViewControl').startAwardFromRedPack();
        }

        this.node.active = false;
    }
    onDisable() {
        this.node.stopAllActions();
    }

    // update(dt){
    //     // console.log('jiaji this.gameview.m_CatFace.zIndex = ',this.gameview.m_CatFace.zIndex)
    //     console.log('jiaji this.gameview.m_CatFace.zIndex = ',this.gameview.m_CatFace.zIndex)
    //     this.node.zIndex = this.gameview.m_CatFace.zIndex+1;
    // }

}
