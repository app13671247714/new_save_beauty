
const { ccclass, property } = cc._decorator;
/**按下放大 */
@ccclass
export class ButtonScaler extends cc.Component {

    private safeTime = 0.5;
    private clickEvents = [];
    onLoad() {
        // this.node.on(cc.Node.EventType.TOUCH_START, this.playSound, this);
        let button = this.node.getComponent(cc.Button);
        if (!button) {
            return;
        }
        this.clickEvents = button.clickEvents;
        this.node.on('click', () => {
            if (button.clickEvents.length == 0) return;
            button.clickEvents = [];
            let delayFun = setTimeout(() => {
                cc.log(delayFun, "delayFun++++++++")
                clearTimeout(delayFun)
                if (this && this.node && button)
                    button.clickEvents = this.clickEvents;
            }, 1000);
        }, this);
    }
    // playSound() {
    //     AudioManage.instance.PlaySound('normal_btn_click')
    // }
}
