

const { ccclass, property } = cc._decorator;

@ccclass
export default class LevelData {

    //关卡颜色
    currentColor: string = "#000000";
    currentUrl: string = "https://rescuebeauty.weililiang1.com/theme_20231027";

    //当前通关路径
    currentPathIndex: Array<Array<number>> = new Array<Array<number>>();
    //是否完美通关
    perfect: boolean = false;

    //行数
    rowNum: number = 1;
    //列数
    colNum: number = 1;
    //单元格宽度
    itemWidth: number = 1;


}
