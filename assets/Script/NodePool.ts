// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
@ccclass('NodePool')
export class NodePool {

    @property(cc.Prefab)
    prefab: cc.Prefab = null;

    enemyPool: cc.NodePool;

    @property(cc.Integer) size = 0;

    init(nodeType: string) {
        this.enemyPool = new cc.NodePool(nodeType);
        for (let i = 0; i < this.size; i++) {
            let enemy = cc.instantiate(this.prefab);
            this.enemyPool.put(enemy);
        }
    }

    spawn(): cc.Node {
        var enemy = null;
        if (this.enemyPool.size() > 0) {
            enemy = this.enemyPool.get(this);
        } else {
            enemy = cc.instantiate(this.prefab);
        }
        return enemy;
    }

    despawn(obj: cc.Node) {
        this.enemyPool.put(obj);
    }

}