
import { BaseLayer } from './BaseLayer'
import { BaseConfig } from './BaseConfig';
import HttpRequestNew from '../Common/HttpRequestNew';
/**
 * 工具静态类
 */
export class Utils {

    //使用严格模式
    'use strict'

    /**
     * 输出log
     */
    static CCLog(..._custome) {
        if (BaseConfig.Debug) {
            console.log.apply(console, _custome);
        }
    }

    /**
     * 计数输出log
     */
    static CCLogCount(..._custome) {
        if (BaseConfig.Debug) {
            console.count.apply(console, _custome);
        }
    }

    /**
     * 计数输出log
     */
    static CCLogResetCount(..._custome) {
        if (BaseConfig.Debug) {
            console.countReset.apply(console, _custome);
        }
    }

    /**
     * 输出对象
     */
    static CCLogDir(_object) {
        if (BaseConfig.Debug) {
            console.dir(_object);
            // console.count.apply(console, _custome);
        }
    }

    /**
     * 输出跟踪
     */
    static CCLogTrace(..._custome) {
        if (BaseConfig.Debug) {
            console.trace.apply(console, _custome);
            // console.count.apply(console, _custome);
        }
    }

    /**
     * 分组输出log
     */
    static CCLogGroup(..._custome) {
        if (BaseConfig.Debug) {
            console.group.apply(console, _custome);
        }
    }

    /**
     * 分组结束输出log
     */
    static CCLogGroupEnd(..._custome) {
        if (BaseConfig.Debug) {
            console.groupEnd.apply(console, _custome);
        }
    }

    /**
     * 时间输出log
     */
    static CCLogTime(..._custome) {
        if (BaseConfig.Debug) {
            console.time.apply(console, _custome);
        }
    }

    /**获取时间戳*/
    static nowTime() {
        return new Date().getTime()
    }
    /**获取时间戳 返回秒*/
    static nowSTime() {
        return Math.floor(new Date().getTime() / 1000)
    }
    /**
     * 时间结束输出log
     */
    static CCLogTimeEnd(..._custome) {
        if (BaseConfig.Debug) {
            console.timeEnd.apply(console, _custome);
        }
    }

    /**
     * 输出错误
     */
    static CCLogError(..._custome) {
        if (BaseConfig.Debug) {
            console.error.apply(console, _custome);
            // console.count.apply(console, _custome);
        }
    }

    /**
     * 输出警告
     */
    static CCLogWarn(..._custome) {
        if (BaseConfig.Debug) {
            console.warn.apply(console, _custome);
            // console.count.apply(console, _custome);
        }
    }
    /**
     * 清理输出log
     */
    static CCLogClear() {
        if (BaseConfig.Debug) {
            console.clear();
            // console.count.apply(console, _custome);
        }
    }

    /**
     * 返回一个随机数
     * @param {number} _min 
     * @param {number} _max 
     * 返回一个整数
     */
    static RandNum(_min, _max) {

        if (_max < _min) {
            return Math.round(_max) + Math.round(Math.random() * (_min - _max));
        }
        else if (_max === _min) {
            return Math.round(_min);
        }
        else {
            return Math.round(_min) + Math.round(Math.random() * (_max - _min));
        }
    }

    /**
     * 返回一个随机数
     * @param {Float} _min 
     * @param {Float} _max 
     * 返回一个浮点数
     */
    static RandFloat(_min, _max) {

        if (_max < _min) {
            return _max + Math.random() * (_min - _max);
        }
        else if (_max === _min) {
            return _min;
        }
        else {
            return _min + Math.random() * (_max - _min);
        }
    }

    /**
     * 返回一个随机弧度
     * 范围(-π,+π)
     * 返回一个浮点数
     */
    static RandRadian() {
        return Math.random() > 0.5 ? Math.random() % Math.PI : -Math.random() % Math.PI;
    }

    /**
     * 判读对象是否为空
     * @param {object} _arr 
     */
    static IsNull(_value) {

        if (_value === null ||
            _value === "" ||
            _value === "null" ||
            _value === "Null" ||
            _value === "NULL" ||
            _value === undefined
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * 深度拷贝对象
     * @param {object} obj 
     */
    static CloneObj(obj) {
        var newObj = {};
        if (obj instanceof Array) {
            newObj = [];
        }
        for (var key in obj) {
            var val = obj[key];
            if (val === null) {
                newObj[key] = null;
            }
            else {
                newObj[key] = typeof val === 'object' ? this.CloneObj(val) : val;
            }
        }
        return newObj;
    }

    /**
     * 返回一个唯一的uuid
     * 
     */
    static GetUUID() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }

    /**
     * 返回一个字符串对应字节的长度的片段
     * @param {string} _str 
     * @param {number} _bytes
     * 
     */
    static GetStringCutOut(_str, _bytes = 10) {

        let result = "";

        if (_str == null) {
            return result;
        }

        var len = 0;
        for (var i = 0; i < _str.length; i++) {

            if (_str.charCodeAt(i) > 127 || _str.charCodeAt(i) == 94) {
                len += 2;

            } else {
                len++;
            }

            if (len <= _bytes) {
                result += _str.charAt(i);
            }
        }

        if (result.length < _str.length) {
            result += "...";
        }

        return result;
    }

    /**
     * 返回字符串根据 字符 分割数组
     * @param {string} _str 
     * @param {string} _key 
     */
    static GetStringToArray(_arr, _key = ';') {

        if (Utils.IsNull(_arr)) {
            return [];
        }

        return _arr.split(_key);
    }

    /**
     * 返回弧度对应长度的位置 
     * 默认为1长度
     * @param {Float} _eadian 
     */
    static RadianToVec2(_eadian, _lenght = 1) {
        return cc.v2(Math.cos(_eadian) * _lenght, Math.sin(_eadian) * _lenght);
    }

    /**
     * 返回角度对应的弧度
     * @param {Float} _angle
     */
    static AngleToRadian(_angle) {
        return _angle / 180 * Math.PI// (_eadian/Math.PI)*180;
    }

    /**
     * 返回弧度对应的角度
     * @param {Float} _eadian 
     */
    static RadianToAngle(_eadian) {
        return (_eadian / Math.PI) * 180;
    }

    /**
     * 返回两个向量的距离
     * @param {Float} _eadian 
     */
    static Vec2Distance(_vec21, _vec22) {
        let out = cc.v2(0, 0);
        out.x = _vec21.x - _vec22.x;
        out.y = _vec21.y - _vec22.y;
        return Math.sqrt(out.x * out.x + out.y * out.y);
    }

    /**
     * 返回二维向量的减法
     * @param {cc.Vec2} _vec21
     * @param {cc.Vec2} _vec22 
     * 
     */
    static Vec2Sub(_vec21, _vec22) {
        let out = cc.v2(0, 0);
        out.x = _vec21.x - _vec22.x;
        out.y = _vec21.y - _vec22.y;
        return out;
    }

    /**
     * 返回二维向量的弧度
     * @param {cc.Vec2} _vec2 
     */
    static Vec2Radian(_vec2) {
        return Math.atan2(_vec2.y, _vec2.x);
    }

    /**
     * 返回二维向量的长度
     * @param {cc.Vec2} _vec2 
     */
    static Vec2Lenght(_vec2) {
        return Math.sqrt(_vec2.x * _vec2.x + _vec2.y * _vec2.y);
    }

    /**
     * 返回二维向量归一化
     * @param {cc.Vec2} _vec2 
     */
    static Vec2Normalize(_vec2) {
        let out = cc.v2(0, 0);
        var magSqr = _vec2.x * _vec2.x + _vec2.y * _vec2.y;
        if (magSqr === 1.0)
            return _vec2;

        if (magSqr === 0.0) {
            return _vec2;
        }

        var invsqrt = 1.0 / Math.sqrt(magSqr);
        out.x = _vec2.x * invsqrt;
        out.y = _vec2.y * invsqrt;
        return out;
    }

    /**
     * 返回二维向量的缩放
     * @param {cc.Vec2} _vec2 
     * @param {Number} _lenght 
     * 
     */
    static Vec2Mul(_vec2, _lenght) {
        let out = cc.v2(0, 0);
        out.x = _vec2.x * _lenght;
        out.y = _vec2.y * _lenght;
        return out;
    }

    /**
     * 将毫秒数转换成时间格式 2019:05:30:10:12:10
     * @param {number} _time
     */
    static GetTimeFormat(_time) {
        var now = new Date(_time),
            y = now.getFullYear(),
            m = now.getMonth() + 1,
            d = now.getDate();
        return y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d) + " " + now.toTimeString().substr(0, 8);
    }
    /**
     * 将秒数转换成时间格式10:10:50 秒
     * @param {number} _time 
     */
    static GetTimeTimesMinutesSeconds(_time) {
        let result = _time
        let h = Math.floor(result / 3600) < 10 ? '0' + Math.floor(result / 3600) : Math.floor(result / 3600);
        let m = Math.floor((result / 60 % 60)) < 10 ? '0' + Math.floor((result / 60 % 60)) : Math.floor((result / 60 % 60));
        let s = Math.floor((result % 60)) < 10 ? '0' + Math.floor((result % 60)) : Math.floor((result % 60));
        let res = `${h}:` + `${m}:` + `${s}`;
        return res;
    }

    //将时间戳转换成日期格式
    static timestampToTime(timestamp) {
        var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
        var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + '';
        return Y + M + D;
    }
    /**
     * 将秒数转换成时间格式 10:50 秒
     * @param {number} _time 
     */
    static GetTimeMinutesSeconds(_time) {
        let minutes = Math.floor(_time / 60);
        let seconds = _time - (minutes * 60);
        return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    }
    /**
     * 将秒数转换成时间格式 10:50 秒
     * @param {number} _time 
     */
    static GetTimeMinutesSecondsCh(_time) {
        let minutes = Math.floor(_time / 60);
        let seconds = _time - (minutes * 60);
        return minutes + "分" + seconds + "秒";
    }

    /**
     * 将毫秒数转换成时间格式  分钟
     * @param {number} _time 
     */
    static GetTimeMinutes(_time) {
        return Math.ceil(_time / 60);
    }

    /**
     * 将毫秒数转换成时间格式  天
     * @param {number} _time 
     */
    static GetTimeDay(_time) {
        return Math.ceil(_time / 1000 / 60 / 60 / 24);
    }

    /**
     * 将毫秒数转换成时间格式  小时
     * @param {number} _time 
     */
    static GetTimeHour(_time) {
        return Math.ceil(_time / 1000 / 60 / 60);
    }

    /**
     * 数字字符串相加
     * @param {String} _strA
     * @param {String} _strB
     * 
     */
    static StringCalculateSum(_strA, _strB) {

        //先判断是否是字符串
        if (typeof _strA !== 'string') {
            _strA = _strA + '';
        }

        if (typeof _strB !== 'string') {
            _strB = _strB + '';
        }

        let raList = _strA.split(''),
            rbList = _strB.split(''),
            result = '',
            count = 0;
        // Utils.CCLog('StringCalculateAddition  raList', raList, 'rbList', rbList);

        while (raList.length || rbList.length || count) {
            // Utils.CCLogCount('StringCalculateSum whiel')
            count += ~~raList.pop() + ~~rbList.pop();
            result = count % 10 + result;
            count = count > 9;
        }
        // Utils.CCLog('StringCalculateSum  raList', raList, 'rbList', rbList, 'result', result);

        return result;
    }

    /**
     * 数字字符串相减
     * @param {String} _strA 被减数
     * @param {String} _strB 减数
     * 
     */
    static StringCalculateSub(_strA, _strB) {

        //先判断是否是字符串
        if (typeof _strA !== 'string') {
            _strA = _strA + '';
        }

        if (typeof _strB !== 'string') {
            _strB = _strB + '';
        }
        let raList = _strA.split(''),
            rbList = _strB.split(''),
            result = '',
            count = 0,
            desc = 0;

        if (parseInt(_strA) < parseInt(_strB)) {
            let temp = raList;
            raList = rbList;
            rbList = temp;
            desc = -1;
        }
        else if (_strA == _strB) {
            return 0;
        }

        Utils.CCLog('StringCalculateAddition  raList', raList, 'rbList', rbList);

        while (raList.length || rbList.length || count) {
            Utils.CCLogCount('StringCalculateSub whiel');
            let ad = ~~raList.pop() - ~~rbList.pop() - count;
            Utils.CCLog('ad', ad, 'count', count);
            result = (ad < 0 ? 10 + ad : ad) + result;
            count = ad < 0;
        }

        let relist = result.split('');

        for (let index = 0; index < relist.length; index++) {
            const element = relist[index];
            if (element != 0) {
                relist.splice(0, index)
                break;
            }

            if (index == relist.length - 1) {
                relist.splice(0, relist.length - 1);
                break;
            }
        }

        if (desc == -1) {
            relist.splice(0, 0, '-');
        }
        Utils.CCLog('StringCalculateSub  raList', raList, 'rbList', rbList, 'result', result, 'relist.join', relist.join(''));

        return relist.join('');
    }

    /**
     * 返回换算后的字符串
     * @param {String} _strA 
     * 
     */
    static StringGetConversionUnit(_strA) {

        //先判断是否是字符串
        if (typeof _strA !== 'string') {
            _strA = _strA + '';
        }

        let unitSymbols = ['', 'K', 'B', 'M', 'T', 'P', 'E', 'Z', 'Y', 'S', 'L', 'X', 'D'],
            ra = _strA.replace(/(^|\s)\d+/g, (m) => m.replace(/(?=(?!\b)(\d{3})+$)/g, ',')),
            result = '',
            desc = 0;

        let splitArray = ra.split(',');
        if (splitArray.length > unitSymbols.length) {
            splitArray.splice(splitArray.length - unitSymbols.length, unitSymbols.length - 1);
            desc = unitSymbols[unitSymbols.length - 1];
        } else {
            desc = unitSymbols[splitArray.length - 1];
            splitArray.splice(1, splitArray.length - 1);
        }
        result = splitArray.join('') + desc;
        return result;
    }

    /**
     * 字符串转Unicode编码
     * @param {String} _str
     * 
     */
    static Str_To_Unicode(_str) {
        var unid = '\\u';
        for (let i = 0, len = _str.length; i < len; i++) {
            if (i < len - 1) {
                unid += _str.charCodeAt(i).toString(16) + '\\u';
            } else if (i === len - 1) {
                unid += _str.charCodeAt(i).toString(16);
            }
        }
        return unid;
    }

    /**
     * Unicode编码转字符串
     * @param {String} _unicode 
     * 
     */
    static Unicode_To_Str(_unicode) {
        var result = [];
        var strArr = _unicode.split('\\u');
        for (var i = 2, len = strArr.length - 1; i < len; i++) {

            if (strArr[i]) {
                result.push(String.fromCharCode(parseInt(strArr[i], 16)));
            }
        }
        return result.join('');
    }
    static toChinese(num) {
        num = Math.floor(num);
        var chinese = "";
        var digits = Math.floor(Math.log10(num)) + 1;
        var digit = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
        var times = ['', '十', '百', '千', '万'];
        if (digits >= 9) {
            var quotient = Math.floor(num / Math.pow(10, 8));
            var remainder = num % Math.pow(10, 8);
            var remainderDigits = Math.floor(Math.log10(remainder)) + 1;
            return this.toChinese(quotient) + '亿' + (remainderDigits < 8 ? "零" : "") + (remainder > 0 ? this.toChinese(remainder) : "");
        }
        //10000 0000
        if (digits == 1) return digit[num];
        if (digits == 2) {
            var quotient = Math.floor(num / 10);
            var remainder = num % 10;
            if (quotient > 1) {
                chinese = digit[quotient];
            }
            chinese = chinese + times[1];
            if (remainder > 0) {
                chinese = chinese + digit[remainder];
            }
            return chinese;
        }
        if (digits > 5) {
            var quotient = num / Math.pow(10, 4);
            var remainder = num % Math.pow(10, 4);
            var remainderDigits = Math.floor(Math.log10(remainder)) + 1;
            return this.toChinese(quotient) + '万' + (remainderDigits < 4 ? "零" : "") + (remainder > 0 ? this.toChinese(remainder) : "");
        }
        for (var index = digits; index >= 1; index--) {
            var number = Math.floor(num / Math.pow(10, index - 1) % 10);
            //console.log(index+" "+number);
            if (number > 0) {
                chinese = chinese + digit[number] + times[index - 1];
            }
            else {
                if (index > 1) {
                    var nextNumber = Math.floor(num / Math.pow(10, index - 2) % 10);
                    if (nextNumber > 0 && index > 1) {
                        chinese = chinese + digit[number];
                    }
                }

            }

        }
        return chinese;

    }
    static toRome(num: number) {
        let romes = ["", "Ⅰ", "Ⅱ", "Ⅲ", "Ⅳ", "Ⅴ"];
        if (num < 0 || num > 5) return "";
        return romes[num]

    }
    /**随机分成几个数 */
    static fillWithRandom(max, total, len) {
        let arr = new Array(len);
        let sum = 0;
        do {
            for (let i = 0; i < len; i++) {
                arr[i] = Math.random();
            }
            sum = arr.reduce((acc, val) => acc + val, 0);
            const scale = (total - len) / sum;
            arr = arr.map(val => Math.min(max, Math.round(val * scale) + 1));
            sum = arr.reduce((acc, val) => acc + val, 0);
        } while (sum - total);
        return arr;
    };
    static getRandomArrayElements(arr, count) {
        var shuffled = arr.slice(0), i = arr.length, min = i - count, temp, index;
        while (i-- > min) {
            index = Math.floor((i + 1) * Math.random());
            temp = shuffled[index];
            shuffled[index] = shuffled[i];
            shuffled[i] = temp;
        }
        return shuffled.slice(min);
    }

    public static getTopPage() {
        let page = BaseConfig.currentPages[BaseConfig.currentPages.length - 1]
        return page
    }

    public static backPage() {
        MenuManage.instance.RmoveMenu(this.getTopPage())
    }
    static getLocalUrl(url) {
        let filePath = "https://answer-1304723711.cos.ap-beijing.myqcloud.com/cocos/canvas/" + url
        // url = "resBundle/images/canvas/" + url
        // let changeExtname = cc.path.changeExtname(url.substr(10));
        // let extname = cc.path.extname(url);
        // let bundle = cc.AssetManager.BuiltinBundleName.RESOURCES;
        // let filePath = cc.AssetManager.prototype._transform({ path: changeExtname, bundle: "resBundle", __isNative__: true, ext: extname });
        return filePath;
    }

    static getLocaltionPath(url) {
        url = "resBundle/images/canvas/" + url
        let changeExtname = cc.path.changeExtname(url.substr(10));
        let extname = cc.path.extname(url);
        let bundle = cc.AssetManager.BuiltinBundleName.RESOURCES;
        let filePath = cc.AssetManager.prototype._transform({ path: changeExtname, bundle: "resBundle", __isNative__: true, ext: extname });
        return filePath;
    }
    public static topicMedia(i, j) {
        if (i >= j) {
            return 4
        }
        let n = (i / j) * 100
        if (n > BaseConfig.UserData.medalConfig[2]) {
            return 3
        } else if (n > BaseConfig.UserData.medalConfig[1]) {
            return 2
        }
        return 1

    }
    /**
     * 
     * @param node 
     * @param b 
     */
    public static setNodeGray(node: cc.Node, b: boolean) {
        if (Utils.IsNull(node)) {
            return;
        }
        let sprList = node.getComponentsInChildren(cc.Sprite);
        if (sprList) {
            for (let i = 0; i < sprList.length; i++) {
                if (b) {
                    sprList[i].setMaterial(0, cc.Material.getBuiltinMaterial('2d-gray-sprite'));
                } else {
                    sprList[i].setMaterial(0, cc.Material.getBuiltinMaterial('2d-sprite'));
                }
            }
        }
    }
    public static todayJudge(param, type = 'timestamp') {
        if ((typeof param === 'string') && type == 'timestamp') {
            param = Number(param)
        }
        if (type == 'datetime') {	// ios日期时间兼容
            param = param.replace(/-/g, "/")
        }
        var currentStamp = new Date().setHours(0, 0, 0, 0)		// 当天日期，转换为时间部分为0的时间戳
        var paramStamp = new Date(param).setHours(0, 0, 0, 0)	// 传入时间戳，将时间部分转换为0
        // 若两个时间戳相等，说明传入的时间戳即今天
        if (currentStamp == paramStamp) {
            return true
        }
        return false
    }
    public static shuffle(array) {
        var copy = [],
            n = array.length,
            i;
        while (n) {
            i = Math.floor(Math.random() * array.length);
            if (i in array) {
                copy.push(array[i]);
                delete array[i];
                n--;
            }
        }
        return copy
    }
    static requet =new HttpRequestNew()
    public static event(event, params){
        params.event = event
        if (Utils.IsNull(params.deviceId)){
            return;
        }
        if(!Utils.IsNull(params.adNetworkPlatformName)){
            params.media = params.adNetworkPlatformName
        }
        this.requet.event("api/v1/log?logType=Ad&Ver=v1.0", params, ()=>{

        })
    }
}

export class MenuType {

    'use strict'

    /**
     * 界面过度动画类型
     * None：没有
     * FadeIn：渐显
     * FadeOut：渐隐
     * PopUp：弹出
     * LeftIn：左侧进入
     * LeftOut：左侧移除
     * RightIn：右侧进入
     * RightOut：右侧移除
     * 
     */
    static TransitionType = {
        None: 1,
        FadeIn: 2,
        FadeOut: 3,
        PopUp: 4,
        LeftIn: 5,
        LeftOut: 6,
        RightIn: 7,
        RightOut: 8,
        ScaleFadeIn: 9,
        ScaleFadeOut: 10
    }
}