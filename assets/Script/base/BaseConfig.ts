
export class BaseConfig {
    static IsLoading = false;
    static Debug = false;
    static TestInfo = false;
    static Global = {
        Game_Name: "",
        Game_Bind_App_List: ["wx5b7716ae421f9b60"],        //绑定游戏列表
        Game_Query: {},

        Game_Is_Review: true,       //审核开关
        gane_is_open_break: false,
        Game_Is_privacy: false,
        //NetRoot: "http://101.37.33.113",    //测试使用的域名
        NetRoot: "https://cathome8.com",
        NetRes: "/zjkj_h5/gongfu/gongfu_wx/res_1001/json/",

        BoxMenuNetRes: "/zjkj_boxmenu/",
        BoxMenuVersion: "zy_game_ml/res_lqwz_20200407/",//global配置地址
        TgSDK_ServerUrl: '/zjserver/tgsdk/tgsdk/protocol/',  //统计请求网址
        Zjrank_ServerUrl: '/zjserver/zjrank/zjrank/protocol/',  //排行榜请求网址

        TgSDK_gameID: "wgfth",
        Zjrank_AppID: 'fnxqc',
        Zjrank_Channel: 'weixin',

        Zjrank_RankName: 'fnxqc_rank_weixin_20191114_1',

        game_window_height_num: 640,

        game_banner_is_show: false,
        Quiet: 0, // 0-不是静音，1-是静音
    }
    static currentPages = [] // 当前页面上存在的预制体
    static hideType = '' // 导致页面hide的方式
    static game_global = null;
    /**
    * 微信 平台信息
    */
    static WeChatGlobal = {
        WxAppType: "zy",

        WxAppId: "wx1cc825ee4274a726",  //

        //test... 六角
        //WxAppId: "wx4f585094799e2ce4",
        BannerAdUnitId: "adunit-33bf016591b22189",          //banner广告id
        RewardedVideoAdUnitId: "adunit-c8e7051beb53cef2",   //视频广告id
        InterstitialAdUnitId: "adunit-a3c388a689286227",    //插屏广告id
    }

    /**
     * wx分包名字
     */
    static WxSubPackageName = 'subpackage';

    /**
     * 游戏服务器json资源
     */
    static GlobalJson = {
        Game_Level_Json: "game_level.json",
        Game_Skin_Json: "game_skin.json",
        Game_Sigin_Json: "game_sigin.json",
    }

    /**
     * 本地音乐资源
     * 文件目录 music
     */
    static GameResMusics = {
        Sound_btn: "normal_btn_click",
        Sound_main_bgm: "bgm",
        Sound_props: "props",
        downTimes: "downTimes",
        rise: "rise",
        No1: "No1",
        timeSd: "timeSd",
        wait: "wait",
        Sound_right_answer: "right_answer",
        Sound_wrong_answer: "wrong_answer",
        throwEgg: "throwEgg",
        excludeError: "excludeError",
        match: "match",
        loss: "loss"

    }

    /**
     * 主页提示器
     * 
     */
    static IndexTextData = {
        ShareText1: "没有人比我更懂",
        ShareText2: "俏丽哇",
        ShareText3: "埋汰王轻松到手",
        ShareText4: "头好痒要长脑子了",
        ShareText5: "你是什么垃圾？",
        ShareText6: "我们简直就是萌妹的代表作",
        ShareText7: "我不李姐",
        ShareText8: "我蚌埠住了",
        ShareText9: "盖亚！",
        ShareText10: "你是对的猪",
        ShareText11: "不愧是我",
        ShareText12: "你有freestyle吗？",
        ShareText13: "大聪明",
        ShareText14: "阿瑟请坐",
        ShareText15: "秀一波操作",
        ShareText16: "冲鸭！",
        ShareText17: "小趴菜",
        ShareText18: "大肠包小肠",
        ShareText19: "鼠鼠我呀",
        ShareText20: "我可能上了假大学",
        ShareText21: "脑子已被格式化",
        ShareText22: "我没惹你们任何人",
        ShareText23: "乌拉！",
        ShareText24: "一整个懵住",
        ShareText25: "请开始你的表演",
        ShareText26: "会答题你就多答点",
        ShareText27: "咱就是说"
    }
    /**
     * 分享语
     * 
     */
    static ShareTextData = {
        ShareText1: "分享答题",
        ShareText2: "分享答题",
        ShareText3: "分享答题"
    }

    /**
     *  分享图片
     * 
     */
    static ShareImgData = {
        ShareImg1: "wx_share_1.png",
        ShareImg2: "wx_share_1.png",
        ShareImg3: "wx_share_1.png",
    }

    /**
     * 分享图片
     * 
     */
    static TmplIds = {
        TmplId1: "VC0_fHOW3wchZdJDKLsi9gvUgWVsuZ9xvB7O4iG3-_k",
        TmplId3: "9BXaBXm-XNXv5YcVe60BQzdLj4BxZPu86W4MGq729iQ",
        TmplId6: "VMn0LEmmV1E3owX_vINPHX20L6eRXSCCT5Y8k-dRV34",
        TmplId2: "m2TBkD9iEqrecgvv8QMlrWY_kgAx9q2NZLjLeykwGn0",
        TmplId4: "_Yy5gFSUjWLH1gdEmLhNTrd7JIC6UKjgBFgEJdQct2I",
        TmplId5: "9BXaBXm-XNXv5YcVe60BQ8wPb7hkb5-J_48KM_0s28Q",
    }
    /**
     * 界面预制体
     */
    static MenuRes = {
        login: "prefab/loginPrefab/login",
        userInfo: "prefab/loginPrefab/userInfo",
        mainMenu: "prefab/mainMenu/mainMenu",
        planFight: "prefab/mainMenu/planFight",
        planFight3Result: "prefab/mainMenu/planFight3Result",
        ranking: "prefab/mainMenu/ranking",
        shareNode: "prefab/mainMenu/shareNode",
        classify: "prefab/mainMenu/classify",
        classLv: "prefab/mainMenu/classLv",
        classLvCompe: "prefab/mainMenu/classLvCompe",
        classRank: "prefab/mainMenu/classRank",
        classifyCompe: "prefab/mainMenu/classifyCompe",
        review: "prefab/mainMenu/review",
        friendPk: "prefab/mainMenu/friendPk",
        friendPkResult: "prefab/mainMenu/friendpkResult",
        friendpkWx: "prefab/mainMenu/friendpkWx",
        mine: "prefab/mainMenu/mine",
        mineCate: "prefab/mainMenu/mineCate",
        topic: "prefab/mainMenu/topic",
        priagree: "prefab/mainMenu/priagree",
        webPage: "prefab/mainMenu/webPage",
        photoSelect: "prefab/mainMenu/photoSelect",
        fightSelect: "prefab/mainMenu/fightSelect",
        userVideo: "prefab/mainMenu/userVideo",
        netError: "prefab/mainMenu/netError",
        matching: "prefab/mainMenu/matching",
        topticList: "prefab/mainMenu/topticList2",
        topticUnload: "prefab/mainMenu/topticUnload",
        topicRank: "prefab/mainMenu/topicRank",
        topicStar: "prefab/mainMenu/topicStar",
        topicMine: "prefab/mainMenu/topicMine",
        topicSearch: "prefab/mainMenu/topicSearch",
        fightTip: "prefab/mainMenu/fightTip",
        honor: "prefab/mainMenu/honor",
        videoCenter: "prefab/mainMenu/videoCenter",
        rewardRes: "prefab/component/rewardRes",
        rewardType: "prefab/component/rewardType",
        lackCoin: "prefab/component/lackCoin",
        chooseReward: "prefab/component/chooseReward",
        ageTips: "prefab/component/ageTips",
        propStore: "prefab/mainMenu/propStore",
        loginReward: "prefab/mainMenu/loginReward",
        welfare: "prefab/mainMenu/welfare",
        setting: "prefab/mainMenu/setting",
        // rewardDialog: "prefab/component/rewardDialog",
        PkSelect: "prefab/mainMenu/PkSelect",
        PkSelecDetail: "prefab/mainMenu/PkSelecDetail",
        PkWait: "prefab/mainMenu/PkWait",


        toast: "prefab/zjadbar/toast",
        ZjadBar_break: "script/zjadbar/ZjadBar_break",
        ZjadBar_full: "script/zjadbar/ZjadBar_full",
        ZjadBar_pop: "script/zjadbar/ZjadBar_pop",
        EggMenu: "script/zjadbar/EggMenu",
        subContext: "script/zjadbar/subContext",

    }

    /**
     * 场景名
     */
    static SceneRes = {
        GameMain: "GameMain",
    }

    /**
     * 对象池
     */
    static NodePoolKey = {
        Npc: 'cccnpc',
    }

    /**
     * 玩家uuidKey   使用uuid区分玩家存档 lookPkRed4
     */
    static UserDataTime = "lookPkTimeTest1";//存档pk时间
    static UserDataRed = "lookPkRedTest1";//
    static UserDataPlayTime = "UserDataPlayTime0";//
    static UserDataTopicRed = "UserDataTopicRed12";//
    static UserDataRankClick = "UserDataRankClick";//排行榜点击时间
    static UserDataLoginClick = "UserDataLoginClick";//登录奖励
    static UserDataTopicHotClick = "UserDataTopicHotClick";//
    static UserDataSub = "UserDataSub";//通知状态

    static UserDataKey = "lanqiu_gameData";//存档id
    static UserUUIDKey = "lwdx_2019_11_25_00";
    static UserUUID = "";
    static UserADState = true;//广告状态
    static isShare = false;//广告状态
    static Channel = ""
    static Version = "149"
    static mediaList = ["路人粉", "铁粉", "忠粉", "代言人"]

    static BaseConfig = {
        realNameAuth: 0,
        PutOlineInterval: 60,
        CheckConfig: {
            Channel: "",
            Version: 0,
            List: "",
            AdSelect: 1,
            ChannelStatus: "0"
        }
    }
    /**
     * 角色初始化数据 avatarUrl
     * 进入游戏会读取存档如果没有存档就会将以下数值存入存档
     */
    static UserData = {
        Authorization: "",
        nickName: "游客",                         //用户名字
        avatarUrl: "",                    //用户头像地址
        gender: 0,                    //用户头像地址
        lvData: { Grade: 1, Score: 0 },
        Trophy: [0, 0],
        classLvData: null,
        province: '', //用户省份
        user_id: 0, //用户id
        lookPkRed: 0, //历史红点存储
        lookPkTime: 0, //红点时间
        playTime: [], //上次答题时间
        topicRed: {}, //上次答题时间
        newToday: false, //上次答题时间
        medalConfig: [0, 30, 70, 100], //主题答题勋章配置等级
        itemsConfig: [], //答题道具使用频次
        goldConfig: null, //晋级赛消耗和奖励配置
        raceConfig: null, //晋级赛比赛积分相关配置
        shareVideoReward: { max: 3, countdown: 0, coins: 500 }, //分享视频相关配置
        medal: null, //主题答题勋章配置等级
        topicConfig: {
            HomeRefresh: 300, EachPlayTime: 20, ABPrice: 500, LocalTest: false, Price: 0
        }, //主题答题时间开始金额配置
        topicGetTimes: 1,
        shareText: null, //
        lossVideo: "",
        lossVideoNoAudio: "",
        victoryVideo: "",
        victoryVideoNoAudio: "",
        imageVideo: "",
        wxAvatarUrl: "",
        idCheck: true,
        property: { // 道具
            balance: 0,
            items: [
                {
                    ItemID: 2,
                    Name: "去除错误答案",
                    Icon: "",
                    Description: "去除错误答案",
                    Amount: 200,
                    Total: 0
                },
                {
                    ItemID: 3,
                    Name: "显示正确答案",
                    Icon: "",
                    Description: "显示正确答案",
                    Amount: 200,
                    Total: 0
                },
                {
                    ItemID: 4,
                    Name: "重新答题",
                    Icon: "",
                    Description: "重新答题",
                    Amount: 200,
                    Total: 0
                },
                {
                    ItemID: 5,
                    Name: "对对手进行减分",
                    Icon: "",
                    Description: "对对手进行减分",
                    Amount: 200,
                    Total: 0
                },
            ]
        },
        store: [
            {
                ItemID: 2,
                Name: "去除错误答案",
                Icon: "",
                Description: "去除错误答案",
                Amount: 200,
                Total: 0
            },
            {
                ItemID: 3,
                Name: "显示正确答案",
                Icon: "",
                Description: "显示正确答案",
                Amount: 200,
                Total: 0
            },
            {
                ItemID: 4,
                Name: "重新答题",
                Icon: "",
                Description: "重新答题",
                Amount: 200,
                Total: 0
            },
            {
                ItemID: 5,
                Name: "对对手进行减分",
                Icon: "",
                Description: "对对手进行减分",
                Amount: 200,
                Total: 0
            },
        ]
    }

    static GameAiVideo: any = {
        url: null
    }

    static Query: any = { // 小程序分享参数
        type: '', //match：答题对战分享。 normal：右上角分享
        matchId: '', // 对战分享时会有对战ID"Score"
        userId: '', // 谁分享的 就是谁的userID
        url: '',//分享视频url
    }

    static Scene: any = ""//场景值
    static UserDataList = {//存档数据
        highScore: 0,//最高分数
        money: 9999,//玩家钱数
        ballType: 0,//玩家当前使用的球
        ballTypeList: [0],//玩家一共解锁的球
    }

    static lvData = [//等级配置
        { "lv": 1, "Score": 0 },
        { "lv": 2, "Score": 10 },
        { "lv": 3, "Score": 30 },
        { "lv": 4, "Score": 50 },
        { "lv": 5, "Score": 100 },
        { "lv": 6, "Score": 150 },
        { "lv": 7, "Score": 200 },
        { "lv": 8, "Score": 250 },
        { "lv": 9, "Score": 300 },
        { "lv": 10, "Score": 350 },
        { "lv": 11, "Score": 400 },
        { "lv": 12, "Score": 450 },
        { "lv": 13, "Score": 500 },
        { "lv": 14, "Score": 550 },
        { "lv": 15, "Score": 600 },
        { "lv": 16, "Score": 700 },
        { "lv": 17, "Score": 800 },
        { "lv": 18, "Score": 900 },
        { "lv": 19, "Score": 1000 },
        { "lv": 20, "Score": 1100 },
        { "lv": 21, "Score": 1300 },
        { "lv": 22, "Score": 1500 },
        { "lv": 23, "Score": 1700 },
        { "lv": 24, "Score": 1900 },
        { "lv": 25, "Score": 2100 },
        { "lv": 26, "Score": 2600 },
        { "lv": 27, "Score": 3100 },
        { "lv": 28, "Score": 3600 },
        { "lv": 29, "Score": 4100 },
        { "lv": 30, "Score": 4600 },

    ]

    static Game = {
        ballCount: 0,//进球数 
        ballScore: 0,//游戏分数
        ballFaultCount: 0,//游戏失误的球数
        pop: 0,//游戏进入次数
        ballADType: false,//是否开启广告使用
        ballADTypeCount: false,//使用编号
        moneyCount: 0,//金蛋钱数
    }

    static GameZJadBar = {
        ZjadBar_pop: null,
        ZjadBar_alone: null,
        ZjadBar_bottom: null,
    }

    /**------------------------------------------------------------------------------------------------------------- 
     ****************************************开放域数据*************************************************************
    ----------------------------------------------------------------------------------------------------------------/
    
    /**
     * 向开放域发送的消息类型
     * 需要在开放域里处理对应逻辑
     */
    static OpenDataMessageType = {
        GetCloudStorage: 'getcloudstorage',
        ShowEndlessRank: 'showendlessrank',
        ShowLimitTimeRank: 'showLimittimerank',
        ShowGoodTimeRank: 'showgoodtimerank',
    }

    /**
     * 向开放域发送的消息数据格式
     * 需要在开放域里读取对应数据
     */
    static OpenDataMessageData = {
        MessageType: '',
        KVDataList: [],
        Data: '',
        DataKey: '',
    }

    /**
     * 上传微信的数据格式
     * UUID 用户唯一id
     * Score 用户分数
     * UpdateTime 更新时间
     * 
     */
    static CloudData = {
        UUID: '',
        Score: 0,
        UpdateTime: 0,
    }

    static AdMap = {
        'adunit-fa207acad4e9a1e2': 'CoinsPopupBanner',
        'adunit-fd5318a90d6f52a0': 'RewardPopupBanner',
        'adunit-a9994f3e7072ec1a': 'LoginRewardBanner',
        'adunit-c8e7051beb53cef2': 'RewardVideo'
    }

    static NativeData = {
        nativeTopicImg: ""
    }

    static CloudInfo = {
        nextcount: 2,
        click_back_count:3,
        challengeCount:3,
    }

    static DeviceInfo = {
        deviceId: "",
        mac: "",
        version: "",
        uuid: "",
        user_id: "",
        channel: 0,
        app_code: "",
        android_id: "",
        os:1,
        oaid: "",
        vendor: "",
        model: "",
        os_version: "",
        language : "",
    }
}
