import { Utils } from "./BaseUtils";
import { BaseConfig } from "./BaseConfig";
import { PropManager } from "./PropManager";

/**
 * 基础类
 */
export class BaseLayer extends cc.Component {
    //使用严格模式
    'use strict'

    /**
     * 初始化
     */
    upView: null;
    constructor() {
        super();
        // Utils.CCLog("BaseLayer constructor");
    }

    //     // onLoad () {},

    // start() {
    // },

    // update (dt) {},
    /**
     * 设置图片
     * @param {cc.Node} _node 
     * @param {Number} _url        //资源地址
     * @param {Number} _urlType    //资源类型 0 为本地资源 1：网络资源
     * @param {Function} _callFun  //资源加载回调
     */
    SetSprite(_node, _url, _urlType = 0, _callFun = null, ext?, _error = null) {

        if (!_node) {
            console.error("SetSprite node is null")
            return;
        }

        //网络资源
        if (_urlType) {
            this.LoadNetImg(_url, (texture) => {
                texture.packable = false;
                let spriteFrame = new cc.SpriteFrame(texture);
                if (!_node.isValid) return;
                var sprite = _node.getComponent(cc.Sprite);
                if (sprite) {
                    sprite.spriteFrame = spriteFrame;
                }
                else {
                    _node.addComponent(cc.Sprite).spriteFrame = spriteFrame;
                }
                if (_callFun) {
                    _callFun();
                }
            }, ext, _error)
        }
        else { //本地资源
            this.LoadResImg(_url, (spriteFrame) => {
                if (!_node.isValid) return;
                var sprite = _node.getComponent(cc.Sprite);
                if (sprite) {
                    sprite.spriteFrame = spriteFrame;
                }
                else {
                    _node.addComponent(cc.Sprite).spriteFrame = spriteFrame;
                }
                if (_callFun) {
                    _callFun();
                }
            })
        }
    }

    /**
     * 加载本地图片
     * @param {Number} _url        //资源地址
     * @param {function} _callFun  //资源加载回调
     */
    LoadResImg(_resUrl, _callFun = null) {
        cc.assetManager.loadBundle("resBundle", (error, bundle) => {
            bundle.load(_resUrl, cc.SpriteFrame, (err, spriteFrame) => {
                if (err) {
                    console.error(err);
                }
                if (_callFun) {
                    _callFun(spriteFrame);
                }
            });
        });
    }

    /**
     * 加载网络图片
     * @param {Number} _url        //资源地址
     * @param {function} _callFun  //资源加载回调
     */
    LoadNetImg(_netUrl, _callFun = null, ext?, _error?) {
        cc.assetManager.loadRemote(_netUrl, ext, function (err, texture) {
            if (err) {
                console.error(err)
                if (_error) {
                    _error(err);
                }
                return;
            }

            if (_callFun) {
                _callFun(texture);
            }
        });
    }
    /**
     * 加载预制体
     * @param {cc.Node} _parent 父节点
     * @param {string} _prefabUrl 预制体资源目录
     * @param {fun} _callFun    回调函数
     * 
     */
    LoadPrefab(_parent, _prefabUrl, _customeData = null, _callFun = null) {

        if (!_parent) {
            return;
        }

        cc.assetManager.loadBundle("resBundle", (error, bundle) => {
            bundle.load(_prefabUrl, cc.Prefab, (err, assets) => {
                if (err) {
                    console.error(err)
                    return;
                }
                if (!_parent) {
                    return;
                }

                let obj = cc.instantiate(assets);
                obj.getComponent(BaseLayer).Init(_customeData)

                _parent.addChild(obj);

                if (_callFun) {
                    _callFun(obj);
                }
            });
        });
    }

    /**
     * 移动一段距离
     * @param {cc.Node} _node   要移动的node
     * @param {number} _time    移动的时间
     * @param {number} _actTag  移动动作的tag 大于1 则回调
     * @param {fun} _actCallFun 回调函数
     */
    ActMoveBy(_node, _time, _x, _y, _actTag = 0, _actCallFun = this.ActCallFun) {
        if (_actTag > 0) {
            var finished = cc.callFunc(_actCallFun, this, _actTag);
            var action = cc.moveBy(_time, cc.v2(_x, _y));
            action.setTag(_actTag);
            _node.runAction(cc.sequence(action, finished));
        }
        else {
            _node.runAction(cc.moveBy(_time, cc.v2(_x, _y)));
        }
    }

    /**
     * 移动到指定位置
     * @param {cc.Node} _node 要移动的node
     * @param {number} _time    移动的时间
     * @param {number} _actTag 移动动作的tag 大于1 则回调
     * @param {fun} _actCallFun 回调函数
     */
    ActMoveTo(_node, _time, _x, _y, _actTag = 0, _actCallFun = this.ActCallFun) {
        if (_actTag > 0) {
            var finished = cc.callFunc(_actCallFun, this, _actTag);
            var action = cc.moveTo(_time, cc.v2(_x, _y));
            action.setTag(_actTag);
            _node.runAction(cc.sequence(action, finished));
        }
        else {
            _node.runAction(cc.moveTo(_time, cc.v2(_x, _y)));
        }
    }

    /**
     * 等待一段时间
     * @param {cc.Node} _node 执行等待动作的node
     * @param {number} _time 等待的时间
     * @param {number} _actTag 移动动作的tag 大于1 则回调
     * @param {fun} _actCallFun 回调函数
     */
    ActDelayTime(_node, _time, _actTag = 0, _actCallFun = this.ActCallFun) {
        if (_actTag > 0) {
            var finished = cc.callFunc(_actCallFun, this, _actTag);
            var action = cc.delayTime(_time);
            action.setTag(_actTag);
            _node.runAction(cc.sequence(action, finished));
        }
        else {
            _node.runAction(cc.delayTime(_time));
        }
    }

    /**
     * 动作回调函数
     * @param {cc.Node} _node 执行动作完的node
     * @param {number} _actTag 移动动作的tag
     */
    ActCallFun(_node, _actTag) {
        //子类实现
    }

    /**
     * 播放anim动画
     * @param {cc.Node} _node 要播放的的node 要有cc.Animition 组件
     * @param {String} _animName 动画名字
     * @param {Function} _onPlayCallFun 播放事件
     * @param {Function} _onPauseCallFun 播放暂停事件
     * @param {Function} _onResumeCallFun 播放恢复事件
     * @param {Function} _onLastframeCallFun 最后一帧事件
     * @param {Function} _onFinishedCallFun 播放完事件
     * 
     */
    PlayAnimtion(_node, _animName = null, _onPlayCallFun = null, _onFinishedCallFun = null, _onPauseCallFun = null, _onResumeCallFun = null, _onLastframeCallFun = null) {
        let animationcompoent = _node.getComponent(cc.Animation);
        if (animationcompoent) {
            if (_animName) {
                animationcompoent.play(_animName);
            }
            else {
                animationcompoent.play();
            }
            //播放事件
            if (_onPlayCallFun) { animationcompoent.on('play', _onPlayCallFun, this); }
            //最后一帧事件
            if (_onLastframeCallFun) { animationcompoent.on('lastframe', _onLastframeCallFun, this); }
            //播放完事件
            if (_onFinishedCallFun) { animationcompoent.on('finished', _onFinishedCallFun, this); }
            //播放暂停事件
            if (_onPauseCallFun) { animationcompoent.on('pause', _onPauseCallFun, this); }
            //播放恢复事件
            if (_onResumeCallFun) { animationcompoent.on('resume', _onResumeCallFun, this); }
        }
        else {
            Utils.CCLog("PlayAnimtion nodename:" + _node.name + 'PlayAnimtion not Animation');
        }
    }

    /**
     *  播放spine动作
     * @param {cc.Node} _node  要播放的的node 要有sp.Skeleton 组件
     * @param {String} _animName 动作名字
     * @param {Bollon} _isLoop  当前动画是否循环
     * @param {*} _onPlayCallFun 开始播放事件
     * @param {*} _onFinishedCallFun 播放完成事件
     * @param {*} _onDisposeCallFun 被销毁的事件
     * @param {*} _onInterruptCallFun 被打断事件
     */
    PlaySpineAnimtion(_node, _animName = null, _isLoop = false, _onPlayCallFun = null, _onFinishedCallFun = null, _onDisposeCallFun = null, _onInterruptCallFun = null) {
        // findAnimation
        let skeletoncomponent = _node.getComponent(sp.Skeleton);
        if (skeletoncomponent) {
            //开始播放事件
            if (_onPlayCallFun) { skeletoncomponent.setStartListener(_onPlayCallFun); }
            //播放完成事件
            if (_onFinishedCallFun) { skeletoncomponent.setEndListener(_onFinishedCallFun); }
            //被销毁的事件
            if (_onDisposeCallFun) { skeletoncomponent.setDisposeListener(_onDisposeCallFun); }
            //被打断事件
            if (_onInterruptCallFun) { skeletoncomponent.setInterruptListener(_onInterruptCallFun); }
            //设置循环
            skeletoncomponent.loop = _isLoop;
            //动画名字
            skeletoncomponent.animation = _animName;// (_animName);
        }
        else {
            Utils.CCLog('PlaySpineAnimtion node is not sp.Skeleton component')
        }
    }

    /**
     * 设置spine皮肤
     * @param {cc.Node} _node 带有spine的界面
     * @param {string} _skinName 皮肤名字
     */
    SetSpineSkin(_node, _skinName) {
        let skeletoncomponent = _node.getComponent(sp.Skeleton);
        if (skeletoncomponent) {
            // Utils.CCLog('SetSpineSkin', skeletoncomponent, '_skinName', _skinName);
            skeletoncomponent.setSkin(_skinName);
        }
    }

    /**
     * 网络加载spine
     * @param {cc.Node} _node 要加载组件的节点
     * @param {string} _url spine 配置文件网络地址
     * @param {string} _spineName spine 名字
     * @param {funtion} _actCallFun 加载完成的动画
     */
    LoadNetSpineSkeleton(_node, _url, _spineName, _actCallFun = null) {
        if (_node == null) {
            Utils.CCLog('_node is null');
            return;
        }

        cc.loader.load(_url + _spineName + '.png', (error, texture) => {
            // Utils.CCLog('LoadNetSpineSkeleton _atlasImg texture', texture);
            cc.loader.load({ url: _url + _spineName + '.atlas', type: 'txt' }, (error, atlasJson) => {
                // Utils.CCLog('LoadNetSpineSkeleton _atlas atlasJson', atlasJson);
                cc.loader.load({ url: _url + _spineName + '.json', type: 'txt' }, (error, spineJson) => {
                    // Utils.CCLog('LoadNetSpineSkeleton _json spineJson', spineJson);
                    let skeleton = _node.getComponent(sp.Skeleton);
                    if (!skeleton) {
                        skeleton = _node.addComponent(sp.Skeleton);
                    }

                    var asset = new sp.SkeletonData();
                    asset.skeletonJson = spineJson;
                    asset.atlasText = atlasJson;
                    asset.textures = [texture];
                    asset['textureNames'] = [_spineName + '.png'];
                    skeleton.skeletonData = asset;
                    if (_actCallFun) {
                        _actCallFun();
                    }
                });
            });
        });
    }

    // LoadParticleSystem(_parent, _prefabUrl, _onFinishedCallFun){
    //     if (!_parent) {
    //         return;
    //     }

    //     cc.loader.loadRes(_prefabUrl, cc.prefab, (err, assets) => {
    //         if (err) {
    //             console.error(err)
    //             return;
    //         }
    //         if (!_parent) {
    //             return;
    //         }

    //         let obj = cc.instantiate(assets);
    //         _parent.addChild(obj);
    //         // life duration 

    //         let 

    //     });
    // }
    /**
     * 给出自己世界坐标下node包围盒 
     * 跟 getBoundingBoxToWorld 不同 只给自己的aabb 不包含子节点
     */
    GetWorldBoundingBox(_node) {
        if (_node) {
            let nodeworld = _node.parent.convertToWorldSpaceAR(cc.v2(_node.x, _node.y));
            // let nodeworld = _node.convertToWorldSpaceAR(cc.Vec2.ZERO);
            return cc.Rect.fromMinMax(cc.v2(nodeworld.x - _node.width * _node.anchorX, nodeworld.y - _node.height * _node.anchorY),
                cc.v2(nodeworld.x + _node.width * (1 - _node.anchorX), nodeworld.y + _node.height * (1 - _node.anchorY)));
        }
        else {
            let nodeworld = this.node.convertToWorldSpaceAR(cc.Vec2.ZERO);
            return cc.Rect.fromMinMax(cc.v2(nodeworld.x - this.node.width * this.node.anchorX, nodeworld.y - this.node.height * this.node.anchorY),
                cc.v2(nodeworld.x + this.node.width * (1 - this.node.anchorX), nodeworld.y + this.node.height * (1 - this.node.anchorY)));
        }
    }

    //加载网络资源 播放图片帧动画
    loaderSpriteFrameToNode(node, url, img_name, atc_time = 0.2) {

        let indexOf = img_name.indexOf('@');
        if (indexOf == -1) {

            cc.loader.load({ url: url + img_name, type: 'png' }, (err, texture) => {
                let spriteFrame = new cc.SpriteFrame(texture);
                node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
            });
        } else {

            let indexOf2 = img_name.indexOf('.');
            let str = img_name.substring(indexOf + 1, indexOf2);
            let str_arr = str.split('_');

            cc.loader.load({ url: url + img_name, type: 'png' }, (err, texture) => {
                // let spriteFrame = new cc.SpriteFrame();
                // spriteFrame.setTexture(texture, new cc.Rect(0, 0, 100, 100), false, new cc.v2(0, 0), new cc.size(100, 100))
                // node.getComponent(cc.Sprite).spriteFrame = spriteFrame;

                // let colnum = Number.parseInt(str_arr[0]);
                // let rownum = Number.parseInt(str_arr[1]);

                // let texture_width = texture.width / colnum;
                // let texture_height = texture.height / rownum;

                // let frames = [];
                // let framesNum = colnum * rownum;

                // for (let row = 0; row < rownum; row++) {
                //     for (let col = 0; col < colnum; col++) {
                //         let spriteFrame = new cc.SpriteFrame();
                //         let textureRect = new cc.Rect(
                //             col * texture_width,
                //             row * texture_width,
                //         )
                //     }

                // }
                // console.log("loaderSpriteFrameToNode framesNum", framesNum);
                // for (let index = 0; index < framesNum; index++) {
                //     let spriteFrame = new cc.SpriteFrame();
                //     let textureRect = new cc.Rect(
                //         index * texture_width,
                //     )
                //     spriteFrame.setTexture(texture, new cc.Rect(0, 0, texture_width, texture_height), false, new cc.v2(0, 0), new cc.size(100, 100))
                //     frames.push(spriteFrame)
                // }
                // // var animation = this.node.getComponent(cc.Animation);
                // // frames 这是一个 SpriteFrame 的数组.
                // // var clip = cc.AnimationClip.createWithSpriteFrames(frames, 17);
                // // clip.name = "anim_run";
                // // clip.wrapMode = cc.WrapMode.Loop;

                // // // 添加帧事件
                // // clip.events.push({
                // //     frame: 1,               // 准确的时间，以秒为单位。这里表示将在动画播放到 1s 时触发事件
                // //     func: "frameEvent",     // 回调函数名称
                // //     params: [1, "hello"]    // 回调参数
                // // });

                // // animation.addClip(clip);
                // // animation.play('anim_run');
                // console.log("loaderSpriteFrameToNode spriteFrame", spriteFrame);

                node.m_texture = texture;
                this.ActFpriteFrame(node, str_arr[0], str_arr[1], atc_time, 0);
            });

        }
    }

    //播放图片帧动画
    ActFpriteFrame(_node, map_w, map_h, atc_time, start_act_time = 0) {

        let num = map_w * map_h;
        if (start_act_time >= num) {
            start_act_time = 0;
        }

        let pos_x = start_act_time % map_w;
        let pos_y = Math.floor(start_act_time / map_h);
        let img_width = _node.m_texture.width / map_w;
        let img_height = _node.m_texture.height / map_h;

        let spriteFrame = new cc.SpriteFrame(_node.m_texture);

        let rect = {
            width: img_width,
            height: img_height,
            x: pos_x * img_width,
            y: pos_y * img_height,
        }
        spriteFrame.setRect(rect);
        // spriteFrame._calculateUV();

        // spriteFrame._rect.width = img_width;
        // spriteFrame._rect.height = img_height;
        // spriteFrame._rect.x = pos_x * img_width;
        // spriteFrame._rect.y = pos_y * img_height;
        _node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
        start_act_time++;

        let action1 = cc.delayTime(atc_time);
        let callfunc = cc.callFunc(() => {

            this.ActFpriteFrame(_node, map_w, map_h, atc_time, start_act_time);
        });
        let action = cc.sequence(action1, callfunc);
        _node.runAction(action);

    }

    /**道具查找 */
    findProp(itemID) {
        let items = PropManager.get().items
        for (let index = 0; index < items.length; index++) {
            const item = items[index];
            if (itemID == item.ItemID) return item
        }
        return null
    }


    /**
     * 界面初始化
     * @param {Object} _customeData 自定义数据
     *  
     */
    Init(_customeData) {
        // Utils.CCLog("BaseLayer Init customeData:", _customeData);
    }

    /**
     * 界面释放
     * @param {Object} _customeData 自定义数据
     * 
     */
    Free(_customeData) {
        // Utils.CCLog("BaseLayer Free customeData:", _customeData);
    }

    /**
     * 按钮点击事件
     * @param {cc.Touch} touch 
     * if (touch.target.name == 'StartGame') {
     * }
     */
    OnClick(touch) {
        //实现方法 示例
        // Utils.CCLog("BaseLayer click event:", touch);
    }

    /**
     * 复选按钮回调
     * @param {cc.Touch} toggle 
     * if (toggle.node.name == 'StartGame') {
     * }
     */
    OnToggle(toggle) {
    }

}

