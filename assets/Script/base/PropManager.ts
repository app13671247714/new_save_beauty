import { BaseConfig } from "./BaseConfig";

/**
 * 查询或者修改用户道具数据
 */
export class PropManager {
    public static get() {
        console.log(BaseConfig.UserData.property)
        return BaseConfig.UserData.property
    }

    public static set(custom) {
        console.log(BaseConfig.UserData.property)
        BaseConfig.UserData.property.balance = custom.balance != undefined ? custom.balance : BaseConfig.UserData.property.balance
        BaseConfig.UserData.property.items = custom.items && custom.items.length ? custom.items : BaseConfig.UserData.property.items
    }
}