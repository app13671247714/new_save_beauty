
const { ccclass, property } = cc._decorator;
/**按下放大 */
@ccclass
export class ButtonScaler extends cc.Component {
    @property
    public scaleTo = new cc.Vec3(1.2, 1.2, 1.2);
    @property
    public transDuration = 0.2;

    public initScale: Number;
    public button: cc.Button | null = null;
    private _scale = new cc.Vec3(1, 1, 1);
    private _lastScale = new cc.Vec3();
    private _start = new cc.Vec3();

    // use this for initialization
    onLoad() {
        this.initScale = this.node.scale;
        this.button = this.getComponent(cc.Button);
        const tweenDown = cc.tween(this._scale);
        const tewenUp = cc.tween(this._scale);
        this.node.getScale(this._start);
        tweenDown.to(this.transDuration, this.scaleTo, { easing: 'cubicInOut' });
        tewenUp.to(this.transDuration, this._start, { easing: 'cubicInOut' });
        this._lastScale.set(this._scale);
        function onTouchDown(event: cc.Event.EventTouch) {
            tweenDown.start();
        }
        function onTouchUp(event: cc.Event.EventTouch) {
            tweenDown.stop();
            tewenUp.start();
        }
        this.node.on(cc.Node.EventType.TOUCH_START, onTouchDown, this);
        this.node.on(cc.Node.EventType.TOUCH_END, onTouchUp, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, onTouchUp, this);
    }

    update() {
        if (!this._scale.equals(this._lastScale)) {
            this.node.setScale(this._scale);
            this._lastScale.set(this._scale);
        }
    }
}
