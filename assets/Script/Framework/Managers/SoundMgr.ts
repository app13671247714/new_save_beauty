// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import { ResConfig } from "../Config/ResConfig";
import ResMgr from "./ResMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SoundMgr extends cc.Component {

    public static Instnce: SoundMgr = null;

    //声音开关
    private m_isMusicOpen: boolean = false;
    private m_isSoundOpen: boolean = false;

    onLoad() {
        if (SoundMgr.Instnce == null) {
            SoundMgr.Instnce = this;
        }
        this.initStatus()
    }

    initStatus() {
        let isOpen = cc.sys.localStorage.getItem("is_audio_open");
        if (isOpen == null || typeof (isOpen) == "undefined" || isOpen == "") {
            isOpen = "1";
            cc.sys.localStorage.setItem("is_audio_open", isOpen);
            this.m_isMusicOpen = true;
            this.m_isSoundOpen = true;
        }else{
            let st = parseInt(isOpen) == 1 ? true : false;
            this.m_isMusicOpen = st
            this.m_isSoundOpen = st
        }
    }

    start() {

    }

    /**播放背景音乐 */
    playBgMusic(musicPath: string, isLoop: boolean) {
        let clip = ResMgr.Instance.getAssetFomeCache(ResConfig.SoundBundle, musicPath, cc.AudioClip);
        if (clip) {
            let audioID = cc.audioEngine.playMusic(clip, isLoop);
            let volume = this.m_isMusicOpen ? 1 : 0;
            cc.audioEngine.setVolume(audioID, volume);
        }
    }

    /**停止背景音乐 */
    public stopBgMusic(): void {
        cc.audioEngine.stopMusic();
    }

    /**播放音效 */
    public playSound(soundPath: string) {
        if (this.m_isSoundOpen === false) {
            return;
        }

        let clip = ResMgr.Instance.getAssetFomeCache(ResConfig.SoundBundle, soundPath, cc.AudioClip);
        if (clip) {
            cc.audioEngine.playEffect(clip, false);
            let volume = (this.m_isSoundOpen) ? 1 : 0;
            cc.audioEngine.setEffectsVolume(volume);
        }
    }

    /**设置背景音乐开关 */
    public setMusicMute(isOpen: boolean): void {
        this.m_isMusicOpen = isOpen;
        cc.sys.localStorage.setItem("is_audio_open", isOpen ? "1" : "0");

        // if (cc.audioEngine.isMusicPlaying) {
        //     cc.audioEngine.setMusicVolume(isMute ? 0 : 1)
        // }
    }
    /**设置音效开关 */
    public setSoundsMute(isOpen: boolean): void {
        this.m_isSoundOpen = isOpen;
        cc.sys.localStorage.setItem("is_audio_open", isOpen ? "1" : "0");

        // let volume = (this.m_isSoundMute) ? 0 : 1.0;
        // cc.audioEngine.setEffectsVolume(volume);
    }
}
