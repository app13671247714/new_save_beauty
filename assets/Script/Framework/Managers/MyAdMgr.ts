import { JsBridge } from "../../native/JsBridge";
import { ADEventConfig } from "../Config/ADEventConfig";


export default class MyAdMgr {

    //激励视频广告位id
    private static videoAdId = "b75a2f0d4f8a1275";
    //插屏广告位id
    private static interAdId = "f64ce2f4cf7f2520";

    private static videoCallBack:((result:any) => boolean) = null;


    /**
     * 
     * @param main 首页节点 用于loading页显示隐藏
     * @param adType 广告事件
     * @param callBack 回调
     */
    static showRewardAd(main:cc.Node,adType,callBack){

        if(JsBridge.isAndroidPlatform()){
            //loading
            main.getChildByName("loading").active = true;

            //参数
            let obj: any = {};
            obj.data = this.videoAdId;
            obj.event = this.getAdEvent(adType);
            obj.event_msg = this.getAdEventMsg(adType);
            JsBridge.callWithCallback(JsBridge.AD_REWARD,obj,(json:any)=>{
                main.getChildByName("loading").active = false;
                if(json){
                    let resultType = json.resultType
                    if(resultType == 6){
                        callBack(true);
                    }else{
                        callBack(false);
                    }
                }
            })
        }else{
            callBack(true);
        }
    }

        /**
     * 
     * @param main 首页节点 用于loading页显示隐藏
     * @param adType 广告事件
     * @param callBack 回调
     */
        static showInterAd(main:cc.Node,adType,callBack){

            if(JsBridge.isAndroidPlatform()){
                //loading
                main.getChildByName("loading").active = true;
    
                //参数
                let obj: any = {};
                obj.data = this.interAdId;
                obj.event = this.getAdEvent(adType);
                obj.event_msg = this.getAdEventMsg(adType);
                JsBridge.callWithCallback(JsBridge.AD_INTER,obj,(json:any)=>{
                    main.getChildByName("loading").active = false;
                    if(json){
                        let resultType = json.resultType
                        if(resultType == 6){
                            callBack(true);
                        }else{
                            callBack(false);
                        }
                    }
                })
            }else{
                callBack(true);
            }
        }

    static getAdEvent(adType){
        switch (adType) {
            case ADEventConfig.AD_ALBUM_ENTER_GAME:
                return "page_album_pic";
        
            default:
                return "";
        }
    }

    static getAdEventMsg(adType){
        switch (adType) {
            case ADEventConfig.AD_ALBUM_ENTER_GAME:
                return "page_ad_page_album_pic_click_album_page_unlock_inter";
        
            default:
                return "";
        }
    }
}
