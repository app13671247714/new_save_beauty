// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ImgBlurControl extends cc.Component {

    @property(cc.Material)
    blur:cc.Material = null;

    @property(cc.Material)
    default:cc.Material = null;

    // onLoad () {}

    start () {
        
    }

    //添加毛玻璃
    applyBlur(){
        this.node.getComponent(cc.Sprite).setMaterial(0,this.blur);
    }

    //去除毛玻璃
    cleanBlur(){
        this.node.getComponent(cc.Sprite).setMaterial(0,this.default)
    }

}
