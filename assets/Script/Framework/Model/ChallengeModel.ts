// PlayerModel.ts

export default class ChallengeModel {
    private _name: string;
    private _level: number;
    private _head: number;

    constructor(name?: string, level?: number, head?: number) {
        this._name = name;
        this._level = level;
        this._head = head;
    }

    // Getters and setters
    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
    }

    public get level(): number {
        return this._level;
    }

    public set level(value: number) {
        this._level = value;
    }

    public get head(): number {
        return this._head;
    }

    public set head(value: number) {
        this._head = value;
    }
}
