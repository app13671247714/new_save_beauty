//*********************************系统事件配置*********************************/
export namespace EventConfig {
    /**更新挑战赛排名 */
    export const CHALLENGE_UPDATE_RANK: string = 'CHALLENGE_UPDATE_RANK';

    /**开始挑战赛倒计时 */
    export const CHALLENGE_START_COUNT_DOWN: string = 'CHALLENGE_START_COUNT_DOWN';
   
}