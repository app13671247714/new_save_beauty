//*********************************系统资源配置*********************************/
export namespace ResConfig {
    /**类型结构 */
    export interface BundleData {
        bundleName: string,
        isRemote: boolean,
    }

    // /**@description Bundle Image名称 */
    // export const ImageBundle: BundleData = { bundleName: 'ImageBundle', isRemote: false };

    // /**@description Bundle Prefab名称 */
    // export const PrefabBundle: BundleData = { bundleName: 'PrefabBundle', isRemote: false };

    /**@description Bundle Sound名称 */
    export const SoundBundle: BundleData = { bundleName: 'SoundBundle', isRemote: false };

    /**@description Bundle 所有的Bundle数组 */
    export const AllBundle: BundleData[] = [SoundBundle];


    /**@description Bundle 远程路径 */
    export const BundlePath: string = 'http://192.168.2.237:5001/';
}