// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import LocalDataManager from './Common/LocalDataManager';
import ReportMgr, { ReportConfig } from './Common/ReportMgr';
import StorageUtil from './Common/StorageUtil';
import GlobalConst from './GlobalConst';
import InnerBox from './InnerBox';
import LevelData from './LevelData';
import { BaseLayer } from './base/BaseLayer';
const WX = window["wx"];

@ccclass
export default class InnerLevelControl extends BaseLayer {

    @property(cc.Node)
    gameNode: cc.Node = null;

    @property(cc.Prefab)
    m_InnerBoxPrefab: cc.Prefab = null;

    @property(cc.Sprite)
    m_BottomView: cc.Sprite = null;

    @property(cc.Sprite)
    m_Bg: cc.Sprite = null;
    @property(cc.Node)
    themeNode: cc.Node = null;

    @property(cc.Node)
    HandSpine: cc.Node = null;

    m_InnerBoxArray: Array<Array<cc.Node>> = new Array<Array<cc.Node>>();

    m_RowNum: number = 5;
    m_ColNum: number = 4;
    m_BoxWidth: number = 0;
    m_BoxHeight: number = 0;

    m_Theme: number = 1;
    m_Chapter: number = 1;

    globalConst: GlobalConst = new GlobalConst();

    //一个主题9个猫
    chapterCount = 0;
    //一只猫20关
    levelCount = 0;




    onLoad() {
        this.chapterCount = this.globalConst.chapterCount;
        this.levelCount = this.globalConst.levelCount;
        // this.node.setPosition(0, 0);
        this.initBoxArray();
    }

    protected start(): void {
        this.showLeadSpine();
    }

    showLeadSpine() {
        let isShow = LocalDataManager.shareManager().isShowLevelHand();
        if (!isShow) {
            // 在这里加载 Spine 动画
            this.HandSpine.active = true;
            // this.spine.setAnimation(0, "newAnimation", true)
        }
    }

    hideLeadHand(){
        this.HandSpine.active = false;
    }

    initBoxArray() {
        let layout = this.node.getComponent(cc.Layout);
        this.m_BoxWidth = (this.node.width - (this.m_ColNum - 1) * layout.spacingX) / (this.m_ColNum + 1);
        layout.paddingLeft = 0.5 * this.m_BoxWidth;
        layout.paddingRight = 0.5 * this.m_BoxWidth;
        // layout.cellSize = cc.size(m_BoxWidth,m_BoxWidth);
        // let node1 = cc.instantiate(this.m_InnerBoxPrefab);
        // let node2 = cc.instantiate(this.m_InnerBoxPrefab);

        // this.node.addChild(node1);
        // node2.x = 100;

        // this.node.addChild(node2);

        for (let i = 0; i < this.m_RowNum; i++) {
            this.m_InnerBoxArray.push([]);
            for (let j = 0; j < this.m_ColNum; j++) {
                let box: cc.Node = cc.instantiate(this.m_InnerBoxPrefab);
                box.getComponent(InnerBox).setThemeView(this.themeNode)
                this.m_InnerBoxArray[i][j] = box;
                this.m_InnerBoxArray[i][j].width = this.m_BoxWidth - 1;
                this.m_InnerBoxArray[i][j].height = this.m_BoxWidth - 1;
                this.node.addChild(this.m_InnerBoxArray[i][j]);
            }
        }
    }

    onLevelSelected(selectedLevel,state?) {
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        // mainTs.hideBanner();
        this.node.getParent().active = false;
        this.gameNode.getParent().active = true;
        this.gameNode.getComponent('GameControl').playSelectedLevel(selectedLevel);

        //当前关卡计算 总官卡 - 前面所有主题的关卡总数 - 当前前面章节的关卡总数
        let currentlevel = selectedLevel - (this.m_Theme - 1)* this.chapterCount * this.levelCount - (this.m_Chapter -1)*this.levelCount

        let info = ReportConfig.ReportMap;
        info.theme = this.m_Theme;
        info.chapter = this.m_Chapter;
        info.level = currentlevel;
        info.state = state;
        info.event = "click_innerBox";
        info.event_msg = "page_level";
        // info.page = "page_ad_page_level_click_innerBox_rewarded";
        ReportMgr.getInst().uploadConfig(info);

        // ReportMgr.getInst().uploadEvent("click_innerBox", "page_ad_page_level_click_innerBox_rewarded", this.m_Theme, this.m_Chapter, currentlevel, 1);
    }

    uploadLockEvent(level){
        let currentlevel = level - (this.m_Theme - 1)* this.chapterCount * this.levelCount - (this.m_Chapter -1)*this.levelCount
        
        let info = ReportConfig.ReportMap;
        info.theme = this.m_Theme;
        info.chapter = this.m_Chapter;
        info.level = currentlevel;
        info.state = 0;
        info.event = "click_innerBox";
        info.event_msg = "page_level";
        info.page = "page_ad_page_level_click_innerBox_rewarded";
        ReportMgr.getInst().uploadConfig(info);
        
        // ReportMgr.getInst().uploadEvent("click_innerBox", "page_ad_page_level_click_innerBox_rewarded", this.m_Theme, this.m_Chapter, currentlevel, 0);

    }

    loadInnerBoxState() {
        let level = 1;
        for (let i = 0; i < this.m_RowNum; i++) {

            for (let j = 0; j < this.m_ColNum; j++) {
                this.m_InnerBoxArray[i][j].getComponent('InnerBox').init((this.m_Theme - 1) * this.chapterCount * this.levelCount + (this.m_Chapter - 1) * this.levelCount + level);
                level++;
            }
        }
        this.changeBg();
    }

    //初始化chapter和theme数据
    
    noPassNodeIdx = 20;
    initLevelData(theme, chapter, unlockIndex?) {
        cc.log(theme, chapter, "theme, chapter+++++")
        if(this.m_InnerBoxArray.length == 0){
            return;
        }
        this.noPassNodeIdx = 20;
        this.m_Theme = theme;
        this.m_Chapter = chapter;

        let level = 1;
        for (let i = 0; i < this.m_RowNum; i++) {
            for (let j = 0; j < this.m_ColNum; j++) {
                this.m_InnerBoxArray[i][j].getComponent('InnerBox').init((this.m_Theme - 1) * this.chapterCount * this.levelCount + (this.m_Chapter - 1) * this.levelCount + level ,unlockIndex);
                level++;
            }
        }

        this.changeBg();
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        mainTs.showBanner(mainTs.InnerSelectBanner, function (bannerHeight) {}.bind(this));
    }
    // update (dt) {}

    storageUtil: StorageUtil = new StorageUtil();
    private boxChapter
    changeBg() {
        this.m_BottomView.spriteFrame = null;
        let boxChapter = (this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter;
        this.boxChapter = boxChapter
        let bgPath = `${new LevelData().currentUrl}/themeIcon/bg${boxChapter}.jpg`
        if (this.m_Bg['boxChapter'] != boxChapter) {
            this.m_Bg.spriteFrame = null;
        }
        this.m_Bg['boxChapter'] = boxChapter
        console.log("+++++++++++++++++++++++++++")
        this.LoadNetImg(bgPath, (teture) => {
            if (this.boxChapter != boxChapter) return;
            let frame = new cc.SpriteFrame(teture)
            cc.loader.setAutoReleaseRecursively(frame, true);
            if (this.m_Bg['boxChapter'] != boxChapter) {
                return;
            }
            this.m_Bg.node.parent.getComponent(cc.Widget).updateAlignment()
            if (this.m_Bg.node.parent.height > teture.height) {
                this.m_Bg.node.height = this.m_Bg.node.parent.height
                console.log(this.m_Bg.node.height,)
                this.m_Bg.node.width = this.m_Bg.node.height / teture.height * teture.width
                console.log(this.m_Bg.node.width,)
            }
            this.m_Bg.node.y = - this.m_Bg.node.parent.height / 2
            this.m_Bg.spriteFrame = frame;
        })
        // cc.loader.loadRes(bgPath, cc.SpriteFrame, function (err, frame) {
        //     if (err) {
        //         cc.log('error to loadRes: ' + bottomPath + ', ' + err || err.message);
        //         return;
        //     }
        //     // 自动释放SpriteFrame和关联Texture资源
        //     cc.loader.setAutoReleaseRecursively(frame, true);
        //     this.m_Bg.spriteFrame = frame;
        // }.bind(this));

    }
}
