import WXCommon from "./Common/WXCommon";
import HttpRequest from "./Common/HttpRequest";
import StorageUtil from "./Common/StorageUtil";
import UserData from "./Bean/UserData";
import CoinsUtil from "./CoinsUtil";
import GlobalConst from "./GlobalConst";


const { ccclass, property } = cc._decorator;
const WX = window["wx"];
@ccclass
export default class InviteItem extends cc.Component {

    @property(cc.Sprite)
    icon: cc.Sprite = null;

    @property(cc.Node)
    inviteBtn: cc.Node = null;

    @property(cc.Label)
    inviteLabel: cc.Label = null;
    @property(cc.Label)
    coinLabel: cc.Label = null;

    @property(cc.SpriteFrame)
    inviteFrame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    inviteFinishFrame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    getGoldFrame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    getGoldFinishFrame: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    defaultHeader: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    activeHeader: cc.SpriteFrame = null;


    @property(cc.Prefab)
    getWxInfoDialog: cc.Prefab = null;
    // onLoad () {}

    @property(cc.Node)
    divider: cc.Node = null;

    wxCommon: WXCommon = new WXCommon();
    httpRequest: HttpRequest = new HttpRequest();
    storageUtill: StorageUtil = new StorageUtil();

    data: object = null;

    goldNum: number = 20;

    coinsUtil: CoinsUtil = new CoinsUtil();

    globalConst: GlobalConst = new GlobalConst();

    index: number = 0;

    setIsInvited(isInvited) {
        // if(isInvited) {
        //    this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.inviteFinishFrame;
        //    this.inviteBtn.parent.getComponent(cc.Button).interactable = false;
        // } else {
        this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.inviteFrame;
        // }
    }

    start() {

    }



    setData(data) {
        this.data = data;
        console.log("setData ", data);
        this.icon.spriteFrame = this.activeHeader;


        if (data.rewardStatus == null || data.rewardStatus == "" || data.rewardStatus == "0") {
            let wxUserInfo = this.storageUtill.getData("wxUserInfo");
            WX.aldSendEvent('邀请按钮曝光', {
                '位置': this.index,
                'uid': wxUserInfo.uid
            });
            this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.getGoldFrame;
        } else {
            this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.getGoldFinishFrame;
            this.inviteBtn.parent.getComponent(cc.Button).interactable = false;
        }


    }

    setIndex(index) {
        this.index = index;
        this.inviteLabel.string = '第' + index + '位好友';
        this.icon.node.width = this.node.height * 0.6;
        this.icon.node.height = this.icon.node.width;

        this.inviteBtn.height = this.node.height * 0.8;
        this.inviteBtn.width = this.inviteBtn.height * 2;
        this.icon.spriteFrame = this.defaultHeader;


        this.divider.y = -this.node.height / 2 + 2;


        this.coinLabel.string = '奖励猫币' + this.globalConst.inviteCoin * index + '枚';
        this.goldNum = this.globalConst.inviteCoin * index;
    }

    onShareClick() {

        if (this.inviteBtn.getComponent(cc.Sprite).spriteFrame == this.getGoldFrame) {
            let userBean: UserData = this.storageUtill.getData("userData");
            this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.getGoldFinishFrame;
            this.inviteBtn.parent.getComponent(cc.Button).interactable = false;
            this.coinsUtil.addCoins(this.goldNum);

            let wxUserInfo = this.storageUtill.getData("wxUserInfo");
            WX.aldSendEvent('邀请领取按钮点击', {
                '位置': this.index,
                'uid': wxUserInfo.uid,
                '猫币值': this.goldNum
            });

            cc.find('Canvas').getComponent('MainControl').getVolumeControl().playCountDownAudio();


            this.httpRequest.uploadReward(userBean.uid, this.data.uid, 1, function () {
                // this.dialog.updateList();
                // this.game.updateGamestrengthLevel(this.goldNum);
            }.bind(this))

        } else {
            if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                if (this.wxCommon == null) {
                    this.wxCommon = new WXCommon();
                }
                let wxUserInfo = this.storageUtill.getData("wxUserInfo");
                if (wxUserInfo.nickName == null || wxUserInfo.nickName == "") {
                    let getWxDialog = cc.instantiate(this.getWxInfoDialog);
                    WX.aldSendEvent('邀请授权曝光');
                    getWxDialog.getComponent('GetWXInfoDialog').setContent("允许获取头像和昵称可领取邀请奖励");
                    getWxDialog.getComponent('GetWXInfoDialog').showWxDialog(function (nickName, headImage) {
                        WX.aldSendEvent('邀请授权成功');
                        let wxUserInfo = this.storageUtill.getData("wxUserInfo");
                        this.wxCommon.inviteFriends("", "", wxUserInfo.uid);


                        WX.aldSendEvent('邀请按钮点击', {
                            '位置': this.index,
                            'uid': wxUserInfo.uid
                        });

                        let inviteNum = this.storageUtill.getNumberData(this.getDate() + "inviteNum", 0);
                        this.storageUtill.saveNumberData(this.getDate() + 'inviteNum', inviteNum++);
                        this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.inviteFinishFrame;
                        this.inviteBtn.parent.getComponent(cc.Button).interactable = false;

                        // let gameBean =  this.storageUtill.getData("current_level_conf");
                        // this.httpRequest.saveGameData(userBean.uid, gameBean, function(data) {
                        //     this.wxCommon.inviteFriends("", "", userBean.uid);
                        //     this.dialog.isShared = true;
                        // }.bind(this))

                    }.bind(this));
                    cc.find('Canvas').addChild(getWxDialog);
                    // this.node.parent.parent.parent.parent.addChild(getWxDialog);
                } else {

                    this.wxCommon.inviteFriends("", "", wxUserInfo.uid);

                    WX.aldSendEvent('邀请按钮点击', {
                        '位置': this.index,
                        'uid': wxUserInfo.uid
                    });

                    let inviteNum = this.storageUtill.getNumberData(this.getDate() + "inviteNum", 0);
                    this.storageUtill.saveNumberData(this.getDate() + 'inviteNum', inviteNum++);
                    // this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.inviteFinishFrame;  
                    // this.inviteBtn.parent.getComponent(cc.Button).interactable = false;


                }
            }
        }

    }

    getDate() {
        var myDate = new Date();
        var year = myDate.getFullYear();
        var yue = myDate.getMonth();
        var day = myDate.getDate();
        var riqi = year + "-" + yue + "-" + day;
        return riqi;
    }
    // update (dt) {}
}
