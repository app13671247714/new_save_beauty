// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { i18nMgr } from "../i18n/i18nMgr";
import UserData from "./Bean/UserData";
import StorageUtil, { LanguageKey } from "./Common/StorageUtil";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingLabel extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    storageUtil: StorageUtil = new StorageUtil();
    costTime: number = 0;
    // onLoad () {}

    start() {
        let languageType = this.storageUtil.getData(LanguageKey);
        if (languageType == null) {
            languageType = cc.sys.languageCode
            languageType = languageType.split("_")[0].split("-")[0];
            if (languageType === "zh") {
                languageType = cc.sys.languageCode;
                if (languageType.indexOf("tw") !== -1 || languageType.indexOf("hk") !== -1 || languageType.indexOf("mo") !== -1) {
                    languageType = "ft"; //繁体中文
                } else {
                    languageType = "zh"; //简体中文
                }
            } else if (languageType === "id") {
                languageType = "in";
            } else if (languageType === "nb") {
                languageType = "no";
            } else if (languageType === "he") {
                languageType = "iw";
            }
        }
        i18nMgr.setLanguage(languageType)
    }

    update(dt) {
        this.costTime++;
        let temp = this.costTime % 90;
        let str = i18nMgr._getLabel("txt_01")
        if (str != "txt_01") {
            if (temp <= 30) {
                this.label.string = str + '.';
            } else if (temp <= 60) {
                this.label.string = str + '..';
            } else {
                this.label.string = str + '...';
            }
        }
    }
}
