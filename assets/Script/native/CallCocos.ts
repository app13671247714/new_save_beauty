// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import EventManager from "../Common/LocalEvents";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CallCocos{

    public static LOADING_CLOSE: string = "LOADING_CLOSE";
    public static LOADING_OPEN: string = "LOADING_OPEN";

    private static TAG:string = "CallCocos"
    public static callCocos(message: string): any{
        console.warn(this.TAG, "java to cocos back..." + message)
        console.log(this.TAG, "java to cocos back..." + message)
        switch (message) {
            case this.LOADING_CLOSE:
                EventManager.emit(message)
                break;
            case this.LOADING_OPEN:
                EventManager.emit(message)
                break;
        
            default:
                break;
        }
    }
}
