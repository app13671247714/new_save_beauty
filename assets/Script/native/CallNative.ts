import {MethodManager} from "./MethodManager";
import {JsBridge} from "./JsBridge";

/**Single thread method manager.
 * methods are saved in MethodMap.
 */
export class CallNative {
    private static TAG:string = "CallNative"
    public static callNative(message: string): any{
        console.log(this.TAG, "to..." + message)
        if (JsBridge.isAndroidPlatform()){
            return jsb.reflection.callStaticMethod("com/wll/rescue/beauty/JsBridge", "toNative", "(Ljava/lang/String;)V", message);
        }else if(JsBridge.isIosPlatform()){
            return jsb.reflection.callStaticMethod("JsBridge", "toNative:", message);
        }
    }

    public static callCocos(message: string): any{
        console.log(this.TAG, "back..." + message)
        let jsonStr = JSON.parse(message);
        if (jsonStr != null && MethodManager.instance.hasMethod(jsonStr.cmd)){
            MethodManager.instance.applyMethod(jsonStr.cmd, jsonStr.msg);
        }
    }
}
