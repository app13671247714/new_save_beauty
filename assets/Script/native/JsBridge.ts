import { MethodManager } from "./MethodManager";
import { CallNative } from "./CallNative";

export class JsBridge {
    public static PAY: string = "PAY";
    public static CONSUME: string = "CONSUME";
    public static DEVICE: string = "DEVICE";
    public static AD_REWARD: string = "AD_REWARD";
    public static AD_INTER: string = "AD_INTER";
    public static AD_BANNER: string = "AD_BANNER";
    public static AD_SPLASH: string = "AD_SPLASH";
    public static AD_BANNER_HIDE: string = "AD_BANNER_HIDE";
    public static TOAST: string = "TOAST";
    public static MEMBERSHIP: string = "MEMBERSHIP";
    public static SAVE: string = "SAVE";
    public static SAVE_LANGUAGE: string = "SAVE_LANGUAGE";//存储选中语言
    public static REPORT_EVENT: string = "REPORT_EVENT";

    public static CLICK_ALBUM: string = "CLICK_ALBUM";//图集点击
    public static CLICK_CHALLENGE: string = "CLICK_CHALLENGE";//挑战赛点击
    public static CLICK_PLAY_BACK: string = "CLICK_PLAY_BACK";//游戏页面点击返回
    public static REFRESH_GAME_PAGE: string = "REFRESH_GAME_PAGE";//刷新游戏页面

    public static callWithCallback(method: string, jsonData: object, callback: Function) {
        let obj: any = {};
        obj.cmd = method;
        obj.msg = jsonData;

        if (callback != null) {
            MethodManager.instance.addMethod(method, callback);
        }
        CallNative.callNative(JSON.stringify(obj));
    }

    public static isAndroidPlatform(): boolean {
        return cc.sys.platform == cc.sys.ANDROID;
    }

    public static isIosPlatform(): boolean {
        return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
    }

    public static isMobile(): boolean {
        return this.isAndroidPlatform() || this.isIosPlatform();
    }

    public static IsNull(_value) {

        if (_value === null ||
            _value === "" ||
            _value === "null" ||
            _value === "Null" ||
            _value === "NULL" ||
            _value === undefined
        ) {
            return true;
        }
        else {
            return false;
        }
    }
}