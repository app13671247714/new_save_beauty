/**Single thread method manager.
 * methods are saved in MethodMap.
 */
export class MethodManager {
    private methodMap: Map<String, Function>;
    public static instance: MethodManager = new MethodManager;
    public addMethod(methodName: String, f: Function): boolean {
        if (this.hasMethod(methodName)){
            this.removeMethod(methodName);
        }
        this.methodMap.set(methodName, f);
        return true;
    }
    public applyMethod(methodName: String, arg?: object): boolean {
        if (!this.methodMap.get(methodName)) {
            return false;
        }
        var f = this.methodMap.get(methodName);
        try {
            f?.call(null, arg);
            return true;
        } catch (e) {
            console.log("Function trigger error: " + e);
            return false;
        }
    }
    public removeMethod(methodName: String): any {
        return this.methodMap.delete(methodName);
    }

    public hasMethod(methodName: String): boolean{
        return this.methodMap.has(methodName);
    }

    constructor() {
        this.methodMap = new Map<String, Function>();
        MethodManager.instance = this;

    }
}
