// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { BaseConfig } from "./base/BaseConfig";

const { ccclass, property } = cc._decorator;

import SettleUtil from './SettleUtil'
import GlobalConst from './GlobalConst'
import UserData from './Bean/UserData'
import StorageUtil, { LanguageKey } from './Common/StorageUtil'
import SubDomainControl from './SubDomainControl';
import HttpRequest from './Common/HttpRequest';
import WXUserBean from './Bean/WXUserBean';
import WXCommon from './Common/WXCommon';
import CoinsUtil from './CoinsUtil';
import RecordBean from './Bean/RecordBean';
import PassLevelBean from './Bean/PassLevelBean'; import List from './scrollViewTools/List';
import { i18nMgr } from '../i18n/i18nMgr';
import LevelData from './LevelData';
import { BaseLayer } from './base/BaseLayer';
import { JsBridge } from './native/JsBridge';
import ThemeScrollControl from './ThemeScrollControl';
import { Utils } from "./base/BaseUtils";
import ReportMgr, { ReportConfig } from "./Common/ReportMgr";
import GameControl from "./GameControl";
import EventManager from "./Common/LocalEvents";
import CallCocos from "./native/CallCocos";
import GameChallenge from "./GameChallenge";
import MyAdMgr from "./Framework/Managers/MyAdMgr";
import { ADEventConfig } from "./Framework/Config/ADEventConfig";

const WX = window["wx"];


@ccclass
export default class MainControl extends BaseLayer {

    //视频激励广告
    //宝箱双倍
    public PressureDoubleGold = 0;
    //抢猫币双倍
    public AwardDoubleGold = 4;
    //每日登陆双倍
    public DayDoubleGold = 5;
    //复活
    public Revive = 6;
    //解锁
    public UnLockChapter = 7;
    //提示
    public Notify = 8;
    //提示对话框
    public NotifyDialog = 9;
    //下一关
    public NextLevel = 10;
    //重玩
    public Reset = 11;
    //解锁主题
    public unlockTheme = 12;
    //解锁关卡
    public unlockLevel = 13;
    //下载壁纸
    public download = 14;
    public downloadAlbum = 15;

    ///解锁下一主题关卡
    public UnlockNextThemeLevel = 16;
    adUnlockNextThemeLevel:boolean = false;

    //挑战赛过关出广告
    public ChallengePassLevel = 17;
    //挑战赛加时
    public ChallengeAddTime = 18;
    adChallengeAddTime: boolean = false;



    //banner广告
    //过关结算界面-底部banner
    public SettleViewBanner = 0;
    //抢猫币界面-底部banner
    public AwardBanner = 1;
    //每日登陆界面-底部banner
    public DailyBanner = 2;
    //宝箱界面-底部banner
    public PressureBanner = 3;
    //挑战赛界面-底部banner
    public LimitBanner = 4;
    //选关界面-底部banner
    public InnerSelectBanner = 5;
    //提示弹框-底部banner
    public NotifyGuide = 6;
    //游戏界面-底部banner
    public PlayGameBanner = 7;
    //挑战赛
    public ChallengeBanner = 8;

    //插屏
    //结算界面插屏
    public SettleViewInter = 0;

    @property(cc.Material)
    blur:cc.Material = null;

    @property(cc.Material)
    default:cc.Material = null;

    //设置页
    @property(cc.Node)
    launchView: cc.Node = null;
    //九宫格主题页
    @property(cc.Node)
    themeView: cc.Node = null;
    //游戏格子界面
    @property(cc.Node)
    gameView: cc.Node = null;
    //关卡页
    @property(cc.Node)
    innerView: cc.Node = null;
    //游戏页面
    @property(cc.Node)
    gameControlView: cc.Node = null;

    @property(cc.Node)
    rankView: cc.Node = null;

    @property(cc.Label)
    albumLabel: cc.Label = null;
    @property(cc.SpriteFrame)
    rankTitle: cc.SpriteFrame = null;

    @property(cc.Node)
    volumeControl: cc.Node = null;

    @property(List)
    list: List = null;
    @property(List)
    list1: List = null;
    @property(List)
    list2: List = null;
    @property(List)
    list3: List = null;

    @property(cc.Node)
    topPoint:cc.Node = null;
    @property(cc.Node)
    bottomPoint:cc.Node = null;
    @property(cc.Node)
    content:cc.Node = null;

    settleUtil: SettleUtil = new SettleUtil();

    globalConst: GlobalConst = new GlobalConst();

    subDomainControl: SubDomainControl = new SubDomainControl();
    userData: UserData = null;
    storageUtil: StorageUtil = new StorageUtil();
    httpRequest: HttpRequest = new HttpRequest();
    recordData: RecordBean = null;
    passData: PassLevelBean = null;

    coinUtil: CoinsUtil = new CoinsUtil();

    catAudioScheduleID: number = 0;

    //双倍领取勾选状态
    doubleCheckBoxChecked: boolean = false;

    coinsUtil: CoinsUtil = new CoinsUtil();

    //求助的关卡
    requsterLevel: number = 1;

    //是否求助进入
    isRequestHelp: boolean = false;

    //是否看答案进入
    isAnswer: boolean = false;
    //解锁主题数
    unLockThemeCount = 1;
    //会员
    Membership = false;


    SubViewRequestCount: number = 0;
    innerBoxArray: Array<Object> = new Array();
    launcheGameNode: cc.Node = null;


    /**
     * 激励视频组件
     */
    rewardedVideoAd: any = null;

    adPressureDoubleGold: boolean = false;
    adRevive: boolean = false;
    adUnlock: boolean = false;
    adNotify: boolean = false;
    adDialogNotify: boolean = false;
    adNextLevel: boolean = false;
    adRest: boolean = false;
    adUnlockTheme: boolean = false;
    adUnlockLevel: boolean = false;
    adDownload: boolean = false;
    adDownloadAlbum: boolean = false;

    bannerAdshow: boolean = true;

    onLoad() {
        let count = this.storageUtil.getData('unLockThemeCount');
        if (count) {
            this.unLockThemeCount = count
        }
        console.log("当前系统语言:" + cc.sys.language + "===" + cc.sys.languageCode);

        this.node.addComponent(GameChallenge);

        this.initGlobalData();
        this.themeView.active = true;
        this.userData = this.storageUtil.getData('userData');
        if (this.userData == null) {
            this.userData = new UserData();
        }
        this.themeView.setPosition(0, 0);
        this.innerView.setPosition(0, 0);
        // this.scheduleCatAudio();
        this.updateAlbum();

        this.Membership = cc.sys.localStorage.getItem('Membership')
        //首次登陆奖励金币
        if (this.isFirstLaunch()) {
            this.storageUtil.saveData('first_launch', false);
            this.storageUtil.saveData('unLockTheme' + 1, true);
            this.storageUtil.saveData('unLockLevel' + 1, 1);
            this.storageUtil.saveData('unLockThemeMaxLevel' + 1, 1);
            this.adTimes = this.storageUtil.getData('adTimes')
        } else {
            //弹会员
            let last = this.storageUtil.getData('lastTime');
            if (!last) {
                last = 0;
            }
            if (!this.Membership) {
                if (!this.todayJudge(last)) {
                    this.btnOpenShop();
                    this.storageUtil.saveData('lastTime', new Date().getTime())
                    this.adTimes = 0;
                    this.storageUtil.saveData('adTimes', 0)
                } else {
                    this.adTimes = this.storageUtil.getData('adTimes')
                }
            }
        }

        this.addList3Listener()

        if (JsBridge.isMobile()) {
            cc["CallCocos"] = CallCocos
        }

        EventManager.on(CallCocos.LOADING_CLOSE,this.closeLoading,this);
        EventManager.on(CallCocos.LOADING_OPEN,this.showLoading,this);
    }

    showLoading(){
        this.node.getChildByName("loading").active = true;
    }
    closeLoading(){
        console.log("closeLoading");
        this.node.getChildByName("loading").active = false;
    }

    /**
     * 图集相册曝光处理 滑动精准曝光
     */
    addList3Listener(){
        let scroll3 = this.list3.node.getComponent(cc.ScrollView);
        scroll3.node.on("scroll-began",this.touchStart,this)
        scroll3.node.on("scroll-ended",this.touchEnd,this)
    }

    private albumOffsety = 0;
    private startIndex = 1;
    private endIndex = 9;

    //计算起始位置index
    calculateStartIndex(offsetY){
        let top = this.convertNodeAR(this.topPoint,this.content);
        // let bot = this.convertNodeAR(this.bottomPoint,this.content);
        console.log("top:"+top);
        offsetY = top;
        let spaceY  = 20;
        let imgHeight = 1148;
        let itemHeight = spaceY + imgHeight;
        //获取屏幕高度
        let winHeight = cc.winSize.height;
        //计算 主题整数倍
        let themeIndex = Math.floor(offsetY/itemHeight) * 9
        //计算不足一个主题
        let topSapce = 175
        let outHeight = offsetY % itemHeight - topSapce;
        let cellHeight = 330
        let cellSpaceY = 16
        let cellIndex  = 1;
        if(outHeight > 0){//超出顶部标签位置
            cellIndex = 1 + Math.round(outHeight / (cellHeight + cellSpaceY)) * 3 
        }
        let startIndex = themeIndex + cellIndex;
        // console.log("开始的index" + startIndex);
        return startIndex;
    }

    calculateEndIndex(offsetY){
        // let top = this.convertNodeAR(this.topPoint,this.content);
        let bot = this.convertNodeAR(this.bottomPoint,this.content);
        console.log("bottom:"+bot);
        offsetY = bot 
        let spaceY  = 20;
        let imgHeight = 1148;
        let itemHeight = spaceY + imgHeight;
        // //获取屏幕高度
        // let winHeight = cc.winSize.height;
        // let scolllHeight = winHeight - 202;
        //算上屏幕显示区的偏移量
        // let offset = offsetY + scolllHeight;
        let offset = bot
        //计算 主题整数倍
        let themeIndex = Math.floor(offset / itemHeight) * 9
        let topSapce = 175
        let cellHeight = 330
        let cellSpaceY = 16
        let outHeight = offset % itemHeight - topSapce;
        let cellIndex  = 0;
        if(outHeight > 0){//超出顶部标签位置
            let row = Math.round(outHeight / (cellHeight + cellSpaceY))
            cellIndex =  row * 3 
        }
        let endIndex = themeIndex + cellIndex;
        // console.log("结束的index" + endIndex);
        return endIndex;
    }

    private _startIndex = 1;
    private _endIndex = 9;
    touchStart(event){
        let scroll3 = this.list3.node.getComponent(cc.ScrollView);
        this.albumOffsety = Math.max(scroll3.getScrollOffset().y, 0) 
        console.log("滑动开始偏移量:" + this.albumOffsety);
        let startIndex = this.calculateStartIndex(this.albumOffsety)
        this._startIndex = startIndex;
        let endIndex = this.calculateEndIndex(this.albumOffsety)
        this._endIndex = endIndex;
        console.log("滑动开始startIndex:" + startIndex);
        console.log("滑动开始endIndex:" + endIndex);
    }

    public convertNodeAR(node1: cc.Node, node2: cc.Node) {
        let worldPos1 = node1.convertToWorldSpaceAR(cc.v2(0,0));
        let localB = node2.convertToNodeSpaceAR(worldPos1);
        return Math.abs(localB.y) 
    }  

    touchEnd(event){
        let scroll3 = this.list3.node.getComponent(cc.ScrollView);
        let offsetEnd = scroll3.getScrollOffset().y
        // if(offsetEnd < 1) return;//刚开始
        console.log("滑动3结束偏移量" + offsetEnd);
        //上拉加载
        let startIndex = this.calculateStartIndex(offsetEnd)
        let isMoveUp = this.albumOffsety > offsetEnd ? 0 : 1;
        let endIndex = this.calculateEndIndex(offsetEnd)
        console.log("滑动结束startIndex:" + startIndex);
        console.log("滑动结束endIndex:" + endIndex);
        if(isMoveUp){
            if(endIndex == this._endIndex) return;
            this.uploadEvent(endIndex,this._endIndex);
            this._endIndex = endIndex;
            this._startIndex = startIndex;
        }else{
            if(startIndex ==  this._startIndex) return;
            this.uploadEvent(startIndex - 1, this._startIndex - 1);
            this._startIndex = startIndex;
            this._endIndex = endIndex;
        }
    }

    uploadEvent(a:number,b:number){
        let min = Math.min(a, b);
        let max = Math.max(a, b);
        for (let i = min + 1; i <= max; i++) {
            let theme = Math.ceil(i / 9);
            let chapter = (i - 1) % 9 + 1;
            let info = ReportConfig.ReportMap;
            info.theme = theme;
            info.chapter = chapter;
            let themeLock = this.storageUtil.getData('unLockTheme' + i);
            info.state = themeLock ? 1 : 0;
            info.event = "evt_show_album";
            info.event_msg = "page_album";
            ReportMgr.getInst().uploadConfig(info);
            // ReportMgr.getInst().uploadEvent("evt_show_album", "page_album", theme, chapter)
        }
    }

    protected start(): void {
        console.log("进入游戏");
        ReportMgr.getInst().uploadEvent("evt_show_home","page_home");
    }

    adTimes: number = 0;
    public todayJudge(param, type = 'timestamp') {
        if ((typeof param === 'string') && type == 'timestamp') {
            param = Number(param)
        }
        if (type == 'datetime') {	// ios日期时间兼容
            param = param.replace(/-/g, "/")
        }
        var currentStamp = new Date().setHours(0, 0, 0, 0)		// 当天日期，转换为时间部分为0的时间戳
        var paramStamp = new Date(param).setHours(0, 0, 0, 0)	// 传入时间戳，将时间部分转换为0
        // 若两个时间戳相等，说明传入的时间戳即今天
        if (currentStamp == paramStamp) {
            return true
        }
        return false
    }


    back2ThemeView() {
        if (this.albumBoo) {
            this.albumBoo = false;
            this.node.getChildByName("Album").active = true;
            this.list1.node.active = true;
            this.list1.updateAll()
        }
        this.hideBanner();
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.innerView.active = false;
        this.themeView.active = true;
        // ReportMgr.getInst().uploadEvent("click_game_back","page_select_level");
    }

    back2LaunchView() {
        this.hideBanner();
        this.gameControlView.getComponent('GameControl').closeAllDialog();
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.themeView.active = true;
        this.gameView.active = false;
        this.innerView.active = false;
        // this.scheduleOnce(this.scheduleCatAudio.bind(this), 0.5);
        // this.scheduleCatAudio();
    }

    showLimitRankList() {
        // this.unScheduleCatAudio();
        // clearInterval(this.catAudioScheduleID);
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
    }
    playLimitGame() {
        // clearInterval(this.catAudioScheduleID);
        // this.unScheduleCatAudio();
        WX.aldSendEvent('挑战赛开始挑战点击');

        GameChallenge.Instance.resetRank();
        this.themeView.active = false;
        this.gameView.active = true;
        this.innerView.active = false;
        this.gameView.getChildByName('GameView').getComponent('GameControl').playLimitGame();
        ReportMgr.getInst().uploadEvent("click_challenge","page_home");
        JsBridge.callWithCallback(JsBridge.CLICK_CHALLENGE, {}, (jsonStr: object) => {})
    }

    isFirstLaunch() {
        let first = this.storageUtil.getData("first_launch");
        if (first || first == null) {
            return true;
        } else {
            return false;
        }
    }

    getVolumeControl() {
        return this.volumeControl.getComponent('AudioController');
    }

    isNull(str) {
        if (typeof (str) == "undefined" || str == "" || str == null) {
            return true;
        } else {
            return false;
        }
    }

    setDoubleAwardState(toggle: cc.Toggle) {
        console.log('toggle is checked  = ' + toggle.isChecked);
        this.doubleCheckBoxChecked = toggle.isChecked;
    }

    //挑战更多关卡点击
    onChallengeMoreClick() {
        this.back2LaunchView();
    }

    loadInnerLevelData() {
        let currentLevel = this.userData.gameLevel;
        this.innerBoxArray.splice(0, this.innerBoxArray.length);
        let _this = this;
        return new Promise((resolve)=> {
            for (let index = 1; index <= currentLevel; index++) {
                _this.innerBoxArray.push(this.storageUtil.getData('levelData' + index));

            }
            console.log('Inner level data load finish');
            resolve(undefined);
        })

    }

    getLevelPathData(level) {
        let path = this.innerBoxArray[level - 1];
        if (path == null || typeof (path) == 'undefined') {
            path = this.storageUtil.getData('levelData' + level);

        }
        return path;
    }

    saveLevelPathData(level, path) {
        this.storageUtil.saveData('levelData' + level, path);
        if (level < this.innerBoxArray.length) {
            this.innerBoxArray[level - 1] = path;
        }

    }

    showGameRecommend() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
    }

    // 广告相关
    closeFun(res?) {
        cc.log(res, "res")
        if (res && res.isEnded || res === undefined) {
            // 正常播放结束，可以下发游戏奖励
            if (this.adPressureDoubleGold) {
                this.adPressureDoubleGold = false;
                this.gameControlView.getComponent('GameControl').gainPressure();
                // this.hideBanner();

            } else if (this.adRevive) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("挑战赛复活视频完整看完");
                }
                this.gameControlView.getComponent('GameControl').gainRevive();
                this.adRevive = false;

            } else if (this.adUnlock) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("解锁主题视频完整看完");
                }
                this.themeView.getComponent('ThemeViewControl').onShared2Friends();
                this.adUnlock = false;

            } else if (this.adNotify) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("提示视频完整看完");
                }
                console.log('视频看完提示 返回');
                this.gameControlView.getComponent('GameControl').isShareNotify = true;
                this.gameControlView.getComponent('GameControl').onNotifyClick(null);
                this.adNotify = false;

            } else if (this.adDialogNotify) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("对话框提示视频完整看完");
                }
                console.log('视频看完提示 返回');
                this.gameControlView.getComponent('GameControl').onDialogNotifyClick();
                this.adDialogNotify = false;
            } else if (this.adNextLevel) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("下一关视频完整看完");
                }
                console.log('视频看完提示 返回');
                this.adNextLevel = false;
                this.themeView.getComponent('ThemeViewControl').successLockLevel();
                this.gameControlView.getComponent('GameControl').passNextLevel();
            } else if (this.adRest) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("重玩视频完整看完");
                }
                console.log('视频看完提示 返回');

                this.gameControlView.getComponent('GameControl').resetClick();
                this.adRest = false;
            } else if (this.adUnlockTheme) {
                console.log('主题相册视频看完提示 返回');
                this.themeView.getComponent('ThemeViewControl').successLockTheme();
                this.adUnlockTheme = false;
            } else if (this.adUnlockLevel) {
                console.log('视频看完提示 返回');
                this.themeView.getComponent('ThemeViewControl').successLockLevel();
                this.adUnlockLevel = false;
            } else if (this.adDownload) {
                console.log('视频看完提示 返回');
                this.gameControlView.getComponent('GameControl').successAd();
                this.adDownload = false;
            } else if (this.adDownloadAlbum) {
                console.log('视频看完提示 返回');
                this.successAdSave();
                this.adDownloadAlbum = false;
            } else if(this.adUnlockNextThemeLevel){
                this.adUnlockNextThemeLevel = false;
                this.gameControlView.getComponent('GameControl').successLockThemeLevel();
            } else if(this.adChallengeAddTime){
                this.gameControlView.getComponent(GameControl).successAdAddTime();
                this.adChallengeAddTime = false;
            }
        } else {
            // 播放中途退出，不下发游戏奖励
            console.log("wx ad onclose game 333");
            if (this.adPressureDoubleGold) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("宝箱结算双倍奖励视频未看完，中途关闭");
                }
                this.adPressureDoubleGold = false;
                // this.hideBanner();

            }  else if (this.adRevive) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("挑战赛复活视频未看完，中途关闭");
                }
                this.showBanner(this.LimitBanner, null);
                this.adRevive = false;

            } else if (this.adUnlock) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("解锁主题视频未看完，中途关闭");
                }

                this.adUnlock = false;

            } else if (this.adNotify) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("提示视频未看完，中途关闭");
                }
                this.gameControlView.getComponent('GameControl').startCountDown = true;
                this.adNotify = false;

            } else if (this.adDialogNotify) {
                if (cc.sys.platform == cc.sys.WECHAT_GAME) {
                    WX.aldSendEvent("对话框提示视频未看完，中途关闭");
                }
                this.gameControlView.getComponent('GameControl').startCountDown = true;
                this.adDialogNotify = false;

            } else if (this.adNextLevel) {
                this.adNextLevel = false;

            } else if (this.adRest) {
                this.adRest = false;
            } else if (this.adUnlockTheme) {
                this.adUnlockTheme = false;
            } else if (this.adUnlockLevel) {
                this.adUnlockLevel = false;
            } else if (this.adDownload) {
                this.adDownload = false;
            } else if (this.adDownloadAlbum) {
                this.adDownloadAlbum = false;
            } else if (this.adUnlockNextThemeLevel){
                this.adUnlockNextThemeLevel = false;
            }else if(this.adChallengeAddTime){
                this.adChallengeAddTime = false;
            }
        }
    };

    loadInterAd(type: number,state?:number){
        let _this = this;
        console.log("ad type...", type)
        if (JsBridge.isMobile()) {
            let adId = "f64ce2f4cf7f2520"
            console.log("ad type...", type)
            this.loadTimer = () => {
                this.node.getChildByName("loading").active = true;
            }
            this.scheduleOnce(this.loadTimer, 1)
            let arr = this.getPageWithAdType(type,state);
            console.log("page:"+arr[0]+" action:" + arr[1])
            JsBridge.callWithCallback(JsBridge.AD_INTER, { data: adId,event:arr[1],event_msg:arr[0]}, (jsonStr: any) => {
                if (jsonStr != null) {
                    let resultType = jsonStr.resultType
                    console.log(resultType, "======native ad callback=======")
                    if (resultType != null) {
                        if (resultType != 0) {
                            this.unschedule(this.loadTimer)
                            this.node.getChildByName("loading").active = false;
                        }
                        switch (resultType) {
                            case 6:
                                this.adTimes++;
                                this.storageUtil.saveData("adTimes", this.adTimes)
                                if (this.adTimes == 1 || this.adTimes == 3 || this.adTimes == 5) {
                                    this.scheduleOnce(() => {
                                        this.btnOpenShop()
                                    }, 0.5)

                                }
                                this.parseAdType(type);
                                this.closeFun(undefined)
                                break;
                            case -1:
                            case -2:
                                if (type == this.NextLevel) {
                                    this.parseAdType(type);
                                    this.closeFun(undefined)
                                } else {
                                    this.parseAdType(type);
                                    this.closeFun(null)
                                    if (resultType != -2) {
                                        let tips = i18nMgr._getLabel("txt_120")
                                        JsBridge.callWithCallback(JsBridge.TOAST, { tips: tips }, (jsonStr: object) => {

                                        });
                                    }
                                }
                                break;
                        }
                    }
                }
            })
        } else {
            this.parseAdType(type);
            this.closeFun()
        }

    }

    loadTimer: any = null
    /**
     * 
     * @param type 广告type类型
     * @param state 区别同一个事件不同来源
     * @returns 
     */
    loadVideoAd(type: number,state?:number) {
        // if (this.Membership) {
        //     this.parseAdType(type);
        //     this.closeFun(undefined)
        //     return;
        // }
        let _this = this;
        console.log("ad video type...", type)
        if (JsBridge.isMobile()) {
            let adId = "b75a2f0d4f8a1275"
            console.log("ad type...", type)
            this.loadTimer = () => {
                this.node.getChildByName("loading").active = true;
            }
            this.scheduleOnce(this.loadTimer, 1)
            let arr = this.getPageWithAdType(type,state);
            console.log("page:"+arr[0]+" action:" + arr[1])
            JsBridge.callWithCallback(JsBridge.AD_REWARD, { data: adId,event:arr[1],event_msg:arr[0]}, (jsonStr: any) => {
                if (jsonStr != null) {
                    let resultType = jsonStr.resultType
                    console.log(resultType, "======native ad callback=======")
                    if (resultType != null) {
                        if (resultType != 0) {
                            this.unschedule(this.loadTimer)
                            this.node.getChildByName("loading").active = false;
                        }
                        /** 0 ad_request 1 ad_full 2 ad_show 7 ad_close 8 ad_click*/
                        switch (resultType) {
                            case 6:
                                this.adTimes++;
                                this.storageUtil.saveData("adTimes", this.adTimes)
                                if (this.adTimes == 1 || this.adTimes == 3 || this.adTimes == 5) {
                                    this.scheduleOnce(() => {
                                        this.btnOpenShop()
                                    }, 0.5)
                                }
                                this.parseAdType(type);
                                this.closeFun(undefined)
                                break;
                            case -1://广告加载失败
                            case -2://广告取消
                                if (type == this.NextLevel) {
                                    this.parseAdType(type);
                                    this.closeFun(undefined)
                                } else {
                                    this.parseAdType(type);
                                    this.closeFun(null)
                                    if (resultType != -2) {
                                        let tips = i18nMgr._getLabel("txt_120")
                                        JsBridge.callWithCallback(JsBridge.TOAST, { tips: tips }, (jsonStr: object) => {

                                        });
                                    }
                                }
                                break;
                        }
                    }
                }
            })
        } else {
            this.parseAdType(type);
            this.closeFun()
        }
    }
    
    getPageWithAdType(type,state){
        let page = "";
        let action = "";
        switch (type) {
            case this.download:
                page = "page_ad_page_game_click_save_pic_rewarded";
                action = "click_save_pic";
                break;
            case this.Revive:
                page = "page_ad_page_challenge_click_retry_rewarded";
                action = "click_retry";
                break;
            case this.Notify:
                page = state == 1 ? "page_ad_page_challenge_click_game_noti_rewarded" : "page_ad_page_game_click_noti_alert_tips_rewarded"
                action = "click_game_noti"
                break;
            case this.NotifyDialog://第2步提示看广告
                page = "page_ad_page_game_click_noti_alert_tips_rewarded";
                action = "click_noti_alert_tips";
                break;
            case this.NextLevel:
                page = "page_ad_page_game_click_next_level_inter";
                action = "click_next_level";
                break;
            case this.unlockLevel:
                page = "page_ad_page_level_click_innerBox_rewarded";
                action = "click_innerBox";
                break;
            case this.unlockTheme:
                if(state == 1){
                    page = "page_ad_page_album_click_album_box_inter";
                    action = "click_alert_unlock_album";
                }else{
                    page = "page_ad_page_home_click_theme_item_inter";
                    action = "click_alert_unlock_theme";
                }
                break;
            case this.downloadAlbum:
                page = "page_ad_page_album_pic_detail_cliclk_save_album_pic_rewarded";
                action = "click_save_album_pic";
                break;
            case this.ChallengePassLevel:
                page = "page_ad_page_challenge_auto_next_level_inter";
                action = "challenge_auto_next_level";
                break;
            case this.ChallengeAddTime:
                page = "page_ad_page_challenge_click_overtime_rewarded";
                action = "click_overtime";

            default:
                break;
        }
        // console.log("广告事件上报：page->"+page+"action->"+action);
        return [page,action];
    }

    parseAdType(type: number) {
        if (type == this.PressureDoubleGold) {
            this.adPressureDoubleGold = true;
        } else if (type == this.Revive) {
            this.adRevive = true;
        } else if (type == this.UnLockChapter) {
            this.adUnlock = true;
        } else if (type == this.Notify) {
            this.adNotify = true;
        } else if (type == this.NotifyDialog) {
            this.adDialogNotify = true;
        } else if (type == this.NextLevel) {
            this.adNextLevel = true;
        } else if (type == this.Reset) {
            this.adRest = true;
        } else if (type == this.unlockTheme) {
            this.adUnlockTheme = true;
        } else if (type == this.unlockLevel) {
            this.adUnlockLevel = true;
        } else if (type == this.download) {
            this.adDownload = true;
        } else if (type == this.downloadAlbum) {
            this.adDownloadAlbum = true;
        } else if (type == this.UnlockNextThemeLevel){
            this.adUnlockNextThemeLevel = true;
        } else if (type == this.ChallengeAddTime){
            this.adChallengeAddTime = true;
        }
    }


    /**
     * banner组件
     */
    bannerAd: any = null;
    showBanner(type: number, callbacks) {
        if (!((type == this.ChallengeBanner) || (type ==  this.PlayGameBanner))) return;
        console.log("show banner");
        /**
         * type 
         */
       if (JsBridge.isMobile()) {
            let adId = "cc134c3b9dbdd014"
            if (JsBridge.isIosPlatform()) {
                adId = "b651145b5e67ca"
            }
            let event_msg = "page_game";
            if(type == this.ChallengeBanner){
                event_msg = "page_challenge"
            }
            JsBridge.callWithCallback(JsBridge.AD_BANNER, { data: adId,event_msg:event_msg }, (jsonStr: any) => {
                if (jsonStr != null) {
                    let resultType = jsonStr.resultType
                    console.log(resultType, "======native ad callback=======")
                }
            })
        }
    }


    hideBanner() {
        if (JsBridge.isMobile()) {
            JsBridge.callWithCallback(JsBridge.AD_BANNER_HIDE, {}, (jsonStr: object) => {
                if (jsonStr != null) {
                }
            })
        }
    }

    /**
     * 插屏组件
     */
    interstitialAd: any = null;
    showInterstitial(type: number) {
        console.log("show interstitial");
    }

    launchViewIsTop() {
        return false;
    }

    showGameIcon1() {
        // this.node.getComponent('Pop').addPopByName();
        // (<any>window).popUpMgr.getComponent('Pop').addPopByName('moreGame/GameBox', null, true, true);
        (<any>window).popUpMgr.getComponent('Pop').addPopByName('moreGame/GameBox', null, true, true, true, 'left');
    }

    initGlobalData() {
        (<any>window).popUpMgr = this.node;
        (<any>window).facadeMgr = this.node;
        (<any>window).facadeMgr.wxAppId = 'wxfd9901848f148530';
    }

    isgameRecorderDurationValid() {
        // if(this.gameRecorderDuration <= 3500){
        //     console.log("isgameRecorderDurationValid gameRecorderDuration",this.gameRecorderDuration);
        //     return false;
        // }
        this.userData = this.storageUtil.getData('userData');
        // if(this.userData.gameLevel <= 5 ){
        //     console.log("isgameRecorderDurationValid gameLevel",this.userData.gameLevel);
        //     return false;
        // }

        this.recordData = this.storageUtil.getData('recordData');
        if (this.recordData == null) {
            this.recordData = new RecordBean();
        }


        if (this.recordData.date === new Date().getMonth() + '_' + new Date().getDate() && this.recordData.num >= 3) {
            console.log("isgameRecorderDurationValid recordData", this.recordData.num);
            return false;
        }

        this.passData = this.storageUtil.getData('passData');


        if (this.passData == null) {
            this.passData = new PassLevelBean();
        }


        if (this.passData.date === new Date().getMonth() + '_' + new Date().getDate() && this.passData.level % 3 == 0) {
            console.log("isgameRecorderDurationValid passData", this.passData.level);
            return true;
        }

        return false;

    }

    //点击设置多语言
    private langList = { "zh": "简体中文", "ft": "繁體中文", "en": "English", "fr": "Français", "de": "Deutsch", "ru": "Русский", "ja": "日本語", "ko": "한국어", "es": "Español", "ar": "العربية", "pt": "Português", "hi": "हिंदी", "it": "Italiano", "nl": "Nederlands", "tr": "Türkçe", "in": "Bahasa Indonesia", "th": "ไทย", "vi": "Tiếng Việt", "ph": "Filipino", "ms": "Bahasa Melayu", "pl": "Polski", "sv": "Svenska", "no": "Norsk", "da": "Dansk", "el": "Ελληνικά", "fi": "Suomi", "cs": "Čeština", "uk": "Українська", "ro": "Română", "hu": "Magyar", "bg": "Български", "iw": "עברית", "fa": "فارسی", "bn": "বাংলা", "gu": "ગુજરાતી", "mr": "मराठी", "ta": "தமிழ்", "te": "తెలుగు" }
    private langIdx = 0
    btnOpenSettingLang() {
        this.node.getChildByName("settingView").active = true
        this.list.node.active = true;
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.volumeControl.getComponent('AudioController').showUI();

        let data = i18nMgr.getLanguage()
        if (data) {
            for (let index = 0; index < Object.keys(this.langList).length; index++) {
                const key = Object.keys(this.langList)[index];
                if (data == key) {
                    this.langIdx = index
                    break;
                }
            }
        }
        this.node.getChildByName("settingView").getChildByName("txtLabel").getComponent(cc.Label).string = Object.values(this.langList)[this.langIdx]
        this.scheduleOnce(() => {
            if (this.list.numItems == 0) {
                this.list.numItems = Object.values(this.langList).length;
            }
            this.list.scrollTo(this.langIdx - 3, 0)
        }, 0.1)

        ReportMgr.getInst().uploadEvent("click_set","page_home");
    }
    btnCloseSettingLang() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.list.scrollTo(0, 0)
        this.node.getChildByName("settingView").active = false;
    }
    btnSettingLang(event) {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        let language = Object.keys(this.langList)[event.target['_index']]
        ReportMgr.getInst().uploadEvent("evt_select_language","page_set",undefined,undefined,undefined,language);
        this.storageUtil.saveData(LanguageKey, language);
        if (JsBridge.isMobile()) {
            JsBridge.callWithCallback(JsBridge.SAVE_LANGUAGE, { language: language}, (jsonStr: object) => {

            })
        }
        this.btnCloseSettingLang();
        cc.game.restart();
    }
    onListRender(item: cc.Node, idx: number) {
        item['_index'] = idx;
        item.getChildByName("txt").getComponent(cc.Label).string = Object.values(this.langList)[idx]
        item.getChildByName("txt").color = new cc.Color().fromHEX(this.langIdx == idx ? "#000000" : "#A797B4")
    }

    //点击相册
    btnOpenAlbum() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.node.getChildByName("Album").active = true;
        this.list3.node.active = true;
        this.list3.numItems = this.globalConst.themeCount;
        ReportMgr.getInst().uploadEvent("click_album","page_home")
        JsBridge.callWithCallback(JsBridge.CLICK_ALBUM, {}, (jsonStr: any) => {
            console.log(jsonStr);
            if(jsonStr.adclose == 1){
                this.uploadAlbumShowFirstIn(1)
            }
        })
    }

    uploadAlbumShowFirstIn(theme){
        for (let i = 1; i < 10; i++) {
            let info = ReportConfig.ReportMap;
            info.theme = theme;
            info.chapter = i;
            let themeLock = this.storageUtil.getData('unLockTheme' + i);
            info.state = themeLock ? 1 : 0;
            info.event = "evt_show_album";
            info.event_msg = "page_album";
            ReportMgr.getInst().uploadConfig(info);
            // ReportMgr.getInst().uploadEvent("evt_show_album","page_album",theme,i)   
        }
    }

    colorList = ["#DDC5FF", "#FFC5CA", "#D1C8FF", "#C9FDF6", "#FFF2C8", "#DCC4FF"]
    onListRender3(item: cc.Node, idx: number) {
        // this.uploadAlbumShowFirstIn(idx + 1)
        // this.list3.node.getComponent(cc.ScrollView).getScrollOffset().y
        item.getChildByName("nameLabel").color = new cc.Color().fromHEX(this.colorList[idx])
        item.getChildByName("nameLabel").getComponent(cc.Label).string = i18nMgr._getLabel("txt_" + (1000 + idx))
        for (let index = 0; index < this.globalConst.chapterCount; index++) {
            const node = item.getChildByName("node").children[index];
            let themeId = idx * this.globalConst.chapterCount + index + 1;
            let lv = (themeId - 1) * this.globalConst.levelCount + 1
            let themeLock = this.storageUtil.getData('unLockTheme' + themeId);
            let spriteNode = node.getChildByName("sprite")
            if (this.Membership || themeLock) {
                node.getChildByName("lock").active = false;
            } else {
                node.getChildByName("lock").active = true;
            }
            spriteNode.active = true;
            if (spriteNode["lv"] != lv) {
                spriteNode.getComponent(cc.Sprite).spriteFrame = this.rankTitle;
            }
            spriteNode["lv"] = lv
            node["theme"] = themeId
            node["unlock"] = themeLock
            node["lv"] = lv
            let bgPath = `${new LevelData().currentUrl}/bg/id_${themeId}/level_bg_${1}.jpg`
            this.LoadNetImg(bgPath, (teture) => {
                if (spriteNode["lv"] != lv) return;
                let frame = new cc.SpriteFrame(teture)
                cc.loader.setAutoReleaseRecursively(    frame, true);
                spriteNode.getComponent(cc.Sprite).spriteFrame = frame;
            })
            node.getChildByName("nameLabel").getComponent(cc.Label).string = i18nMgr._getLabel('txt_' + (1005 + themeId))

        }
    }

    private selectTheme: number = 1

    private albumSelectTheme: number = 1;//记录相册内选择的主题
    private albumSelectChapter: number = 1;//记录相册内选择的章
    // private albumSelectLevel: number = 1;//记录相册内选择的节 关卡
    btnOpenIsInner(event) {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        let theme = event.target["theme"]
        let lv = event.target["lv"]
        this.albumSelectTheme = Math.ceil(theme/this.globalConst.chapterCount);
        this.albumSelectChapter = (theme-1)%this.globalConst.chapterCount + 1;
        // this.albumSelectLevel = lv - (this.albumSelectTheme -1)*180 - this.albumSelectChapter*20
        let state = 1;
        if (event.target.unlock || this.Membership) {
            this.list1.node.active = true;
            this.list3.node.active = false;
            this.list1._customSize = {}
            this.selectTheme = event.target["theme"]
            for (let index = 0; index < 6; index++) {
                if (index == 0) {
                    this.list1._customSize[index] = 80
                } else {
                    this.list1._customSize[index] = 229
                }
            }
            this.list1.numItems = 6
            // ReportMgr.getInst().uploadEvent("click_album_box", "page_ad_page_album_click_album_box_inter", this.albumSelectTheme, this.albumSelectChapter, 1);
            state = 1;
        } else {
            this.themeView.getComponent('ThemeViewControl').showUnlockAlbum(event.target["theme"], event.target["lv"],{fromType:3,theme:this.albumSelectTheme,chapter:this.albumSelectChapter},this);
            // ReportMgr.getInst().uploadEvent("click_album_box", "page_ad_page_album_click_album_box_inter", this.albumSelectTheme, this.albumSelectChapter, 0);
            state = 0;
        }

        let info = ReportConfig.ReportMap;
        info.theme = this.albumSelectTheme;
        info.chapter = this.albumSelectChapter;
        // info.level = currentlevel;
        info.state = state;
        info.event = "click_album_box";
        info.event_msg = "page_album";
        if(state == 0){
            info.page = "page_ad_page_album_click_album_box_inter";
        }
        ReportMgr.getInst().uploadConfig(info);
    }

    btnCloseAlbum() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        if (this.list1.node.active) {
            this.list1.scrollTo(0, 0)
            this.list3.node.active = true;
            this.list1.node.active = false;
            // ReportMgr.getInst().uploadEvent("click_album_back","album_page_2");
        } else {
            // ReportMgr.getInst().uploadEvent("click_album_back","album_page_1");
            this.closeAlbum()
        }

    }
    closeAlbum() {
        this.list3.scrollTo(0, 0)
        this.node.getChildByName("Album").active = false;
        this.list3.node.active = false;
        this.list1.node.active = false;
    }
    onListRender1(item: cc.Node, idx: number) {
        item['_index'] = idx;
        let img1 = item.getChildByName("img1");
        let img2 = item.getChildByName("img2");
        let img3 = item.getChildByName("img3");
        let img4 = item.getChildByName("img4");
        if (idx % 6 == 0) {
            item.height = 50;
            img1.active = false;
            img2.active = false;
            img3.active = false;
            img4.active = false;
            item.getChildByName("label").active = true;
            item.getChildByName("label").getComponent(cc.Label).string = i18nMgr._getLabel('txt_' + (1005 + this.selectTheme))
        } else {
            item.height = 230;
            img1.active = true;
            img2.active = true;
            img3.active = true;
            img4.active = true;
            item.getChildByName("label").active = false;
            let level = ((idx % 6) - 1) * 4;
            for (let index = 1; index < 5; index++) {
                const node = item.getChildByName("img" + [index]);
                node["theme"] = Math.ceil(this.selectTheme / this.globalConst.chapterCount)
                let lv = (this.selectTheme - 1) * this.globalConst.levelCount + level + index
                let chapterTemp = lv - (node["theme"] - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
                node["chapter"] = Math.ceil(chapterTemp / this.globalConst.levelCount);
                let levelData = this.storageUtil.getData('unLockLevel' + lv);
                node["levelData"] = levelData
                node["lv"] = lv
                // node.getChildByName("sprite").active = false;
                let bgPath = `${new LevelData().currentUrl}/bg/id_${this.selectTheme}/level_bg_${level + index}.jpg`
                this.LoadNetImg(bgPath, (teture) => {
                    if (node["lv"] != lv) return;
                    let frame = new cc.SpriteFrame(teture)
                    cc.loader.setAutoReleaseRecursively(frame, true);
                    node.getChildByName("sprite1").getComponent(cc.Sprite).spriteFrame = frame;
                    if(levelData && levelData == 2){
                        node.getChildByName("sprite1").getComponent(cc.Sprite).setMaterial(0,this.default);
                    }else{
                        node.getChildByName("sprite1").getComponent(cc.Sprite).setMaterial(0,this.blur);
                    }
                })

                let state = 1;
                if ((levelData && levelData == 2)) {
                    node.getChildByName("sprite2").active = false;
                } else {
                    node.getChildByName("sprite2").active = true;
                    state = 0;
                }
                let curLevel = (idx - 1) * 4 + index;//计算当前真实展示level
                let info = ReportConfig.ReportMap;
                info.theme = this.albumSelectTheme;
                info.chapter = this.albumSelectChapter;
                info.level = curLevel;
                info.state = state;
                info.event = "evt_show_album_pic";
                info.event_msg = "page_album_pic";
                ReportMgr.getInst().uploadConfig(info);
            }
        }
    }
    picLv: number = 0
    passPic = []
    albumBoo: boolean = false
    btnOpenInner(event) {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        let theme = Math.ceil(event.target["lv"] / this.globalConst.levelCount)
        let themeLock = this.storageUtil.getData('unLockTheme' + theme);
        cc.log(theme, "theme", themeLock)
        if (event.target["levelData"] == 2) {
            this.passPic = []
            let pic = this.node.getChildByName("pic");
            pic.active = true;
            pic.opacity = 0;
            for (let j = 1; j <= this.globalConst.levelCount; j++) {
                let lv = ((theme - 1) * this.globalConst.levelCount + j)
                let themeLock = this.storageUtil.getData('unLockLevel' + lv);
                if (themeLock == 2) {
                    this.passPic.push(lv)
                }
            }
            let idx = this.passPic.indexOf(event.target["lv"])
            if (idx != -1) {
                this.picLv = idx;
            } else {
                this.picLv = 0
            }
            pic.getChildByName("titLabel").getComponent(cc.Label).string = this.passPic.length + "/" + this.globalConst.levelCount
            this.list2.numItems = this.passPic.length
            this.hideVideo()
            this.scheduleOnce(() => {
                this.list2.skipPage(this.picLv, 0)
                pic.opacity = 255;
            }, 0.1)
            ReportMgr.getInst().uploadEvent("click_album_page_box", "page_album_pic", event.target["theme"], event.target["chapter"], event.target["lv"]);
        } else {
            this.list1.scrollView.stopAutoScroll();
            this.albumBoo = true;
            // this.innerView.active = true;
            // this.themeView.active = false;
            // this.innerView.getChildByName("GridLayout").getComponent('InnerLevelControl').initLevelData(event.target["theme"], event.target["chapter"]);
            // this.themeView.getParent().getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();

            let info = ReportConfig.ReportMap;
            info.theme = event.target["theme"];
            info.chapter = event.target["chapter"];
            info.level = event.target["lv"];
            info.event = "click_album_page_unlock";
            info.event_msg = "page_album_pic";
            info.page = "page_ad_page_album_pic_click_album_page_unlock_inter";
            ReportMgr.getInst().uploadConfig(info);
            let level = event.target["lv"];
            MyAdMgr.showInterAd(this.node,ADEventConfig.AD_ALBUM_ENTER_GAME,(result:boolean)=>{
                if(result){
                    this.goSeletedLevel(level);
                }else{
                    console.log("观看失败");
                }
            });
        }
    }

    goSeletedLevel(level) {
        this.node.getChildByName("Album").active = false;
        this.themeView.active = false;
        this.gameView.active = true;
        this.innerView.active = false;
        this.gameControlView.getComponent('GameControl').playSelectedLevel(level,true);
    }


    updateAlbum() {
        let passCount = 0;
        passCount = this.storageUtil.getData('passCount');
        if (!passCount) {
            passCount = 0;
        }
        let value = passCount / (this.globalConst.levelCount * this.globalConst.themeCount * this.globalConst.chapterCount) * 100;
        this.albumLabel.string = i18nMgr._getLabel("txt_118")
        // .replace("@", Math.ceil(value) + "%")
    }
    hideVideo() {
        let pic = this.node.getChildByName("pic");
        if (this.Membership) {
            pic.getChildByName("btn1").active = false;
            pic.getChildByName("btn").getChildByName("layout").getChildByName("icon").active = false;
        }
        this.showPic()
    }
    //弹窗会员页
    btnOpenShop(event?,data?) {
        return;//屏蔽会员弹窗
        console.log(data)
        let from = "other"
        if(data){
            from = data;
        }
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.node.getChildByName("Membership").active = true;
    }
    btnLeft() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.picLv--;
        this.showPic()
        this.list2.skipPage(this.picLv, 0.3)
        // ReportMgr.getInst().uploadEvent("click_album_pic_btnLeft","picIndex_" + this.picLv)
    }
    btnRight() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.picLv++;
        this.showPic()
        this.list2.skipPage(this.picLv, 0.3)
        // ReportMgr.getInst().uploadEvent("click_album_pic_btnRight","picIndex_" + this.picLv)
    }
    pageEvent(idx) {
        cc.log(idx, "idx")
        this.picLv = idx
        this.showPic()
        let level = idx + 1
        ReportMgr.getInst().uploadEvent("evt_show_album_pic_detail","page_album_pic_detail",this.albumSelectTheme,this.albumSelectChapter,level)
    }
    onListRender2(item: cc.Node, idx: number) {
        let picLv = this.passPic[idx]
        let chapter = Math.ceil(picLv / this.globalConst.levelCount)
        let level = picLv % this.globalConst.levelCount
        if (item["picLv"] != picLv) {
            item.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.rankTitle
        }
        item["picLv"] = picLv;
        let bgPath = `${new LevelData().currentUrl}/bg/id_${chapter}/level_bg_${level || 20}.jpg`
        this.LoadNetImg(bgPath, (teture) => {
            if (item["picLv"] != picLv) return;
            let frame = new cc.SpriteFrame(teture)
            cc.loader.setAutoReleaseRecursively(frame, true);
            item.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = frame
        })
    }
    showPic() {
        let pic = this.node.getChildByName("pic");
        if (this.list2.numItems == 1) {
            pic.getChildByName("btnright").active = false;
            pic.getChildByName("btnleft").active = false;
        } else {
            if (this.picLv == this.list2.numItems - 1) {
                pic.getChildByName("btnright").active = false;
                pic.getChildByName("btnleft").active = true;
            } else if (this.picLv == 0) {
                pic.getChildByName("btnright").active = true;
                pic.getChildByName("btnleft").active = false;
            } else {
                pic.getChildByName("btnright").active = true;
                pic.getChildByName("btnleft").active = true;
            }
        }
    }
    saveBtn() {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        console.log("保存图片主题-章-节"+this.albumSelectTheme,this.albumSelectChapter,this.picLv);
        let info = ReportConfig.ReportMap;
        info.theme = this.albumSelectTheme;
        info.chapter = this.albumSelectChapter;
        info.level = this.picLv + 1;
        info.event = "click_save_album_pic";
        info.event_msg = "page_album_pic_detail";
        info.page = "page_ad_page_album_pic_detail_cliclk_save_album_pic_rewarded";
        ReportMgr.getInst().uploadConfig(info);
        if (this.Membership) {
            this.successAdSave()
        } else {
            this.loadVideoAd(this.downloadAlbum)
        }
    }
    successAdSave() {
        let chapter = Math.ceil((this.passPic[this.picLv]) / this.globalConst.levelCount)
        //20关处理一下
        let level = (this.passPic[this.picLv] - 1) % this.globalConst.levelCount + 1
        let bgPath = `${new LevelData().currentUrl}/bg/id_${chapter}/level_bg_${level}.jpg`
        cc.log("相册保存图片地址" + bgPath)
        if (JsBridge.isMobile()) {
            JsBridge.callWithCallback(JsBridge.SAVE, { type: 'img', url: bgPath }, (jsonStr: object) => {

            })
        }
    }
    btnCloseBigPic(event) {
        this.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.node.getChildByName("pic").active = false;
        // ReportMgr.getInst().uploadEvent("click_album_back","album_page_3");
    }
}
