// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { i18nMgr } from "../i18n/i18nMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Prefab)
    m_ThemeBoxPrefab: cc.Prefab = null;


    @property(cc.Node)
    m_ThemeLabel: cc.Node = null;
    m_ThemeBoxArray: Array<Array<cc.Node>> = new Array<Array<cc.Node>>();

    m_InnerGrid: cc.Node = null;
    m_ThemeView: cc.Node = null;


    m_RowNum: number = 3;
    m_ColNum: number = 3;
    m_BoxWidth: number = 0;
    m_BoxHeight: number = 0;

    m_Theme: number = 1;
    m_Chapter: number = 1;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        // this.initBoxArray();
    }

    // update (dt) {}

    initBoxArray() {

        for (let i = 0; i < this.m_RowNum; i++) {
            this.m_ThemeBoxArray.push([]);
            for (let j = 0; j < this.m_ColNum; j++) {
                let box: cc.Node = cc.instantiate(this.m_ThemeBoxPrefab);
                this.m_ThemeBoxArray[i][j] = box;
                box.y = 0;
                this.node.addChild(this.m_ThemeBoxArray[i][j]);
            }

        }

        this.scheduleOnce(function () {
            this.initThemeBoxData();
        }, 0.1);
    }

    setTheme(theme) {
        this.m_Theme = theme;
        this.initBoxArray();
        this.changeTheme();
    }

    setInnerGird(innerGrid) {
        this.m_InnerGrid = innerGrid;
    }

    setThemeView(themeView) {
        this.m_ThemeView = themeView;
    }

    initThemeBoxData() {
        let count = 1;
        for (let i = 0; i < this.m_RowNum; i++) {
            for (let j = 0; j < this.m_ColNum; j++) {
                this.m_ThemeBoxArray[i][j].getComponent('ThemeBox').setChapterTheme(count, this.m_Theme);
                if (this.m_InnerGrid != null) {
                    this.m_ThemeBoxArray[i][j].getComponent('ThemeBox').setInnerGird(this.m_InnerGrid);
                }
                if (this.m_ThemeView != null) {
                    this.m_ThemeBoxArray[i][j].getComponent('ThemeBox').setThemeView(this.m_ThemeView);
                }
                count++;
            }
        }
    }
    initThemeBoxData1() {
        let count = 1;
        for (let i = 0; i < this.m_RowNum; i++) {
            for (let j = 0; j < this.m_ColNum; j++) {
                this.m_ThemeBoxArray[i][j].getComponent('ThemeBox').setChapterTheme(count, this.m_Theme);
                if (this.m_InnerGrid != null) {
                    this.m_ThemeBoxArray[i][j].getComponent('ThemeBox').setInnerGird(this.m_InnerGrid);
                }
                if (this.m_ThemeView != null) {
                    this.m_ThemeBoxArray[i][j].getComponent('ThemeBox').setThemeView(this.m_ThemeView);
                }
                count++;
            }
        }
    }

    changeTheme() {
        let themeTitlePath = 'ThemeBg/bg' + this.m_Theme;
        this.m_ThemeLabel.getComponent(cc.Label).string = i18nMgr._getLabel("txt_" + (999 + this.m_Theme))
        cc.loader.loadRes(themeTitlePath, cc.SpriteFrame, function (err, frame) {
            if (err) {
                cc.log('error to loadRes: ' + themeTitlePath + ', ' + err || err.message);
                return;
            }
            // 自动释放SpriteFrame和关联Texture资源
            cc.loader.setAutoReleaseRecursively(frame, true);
            this.node.parent.getComponent(cc.Sprite).spriteFrame = frame;
        }.bind(this));
    }


}
