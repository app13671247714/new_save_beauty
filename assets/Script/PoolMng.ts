// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import { NodePool } from './NodePool'

@ccclass
export default class PoolMng extends cc.Component {

    @property(NodePool)
    boxPool: NodePool[] = [];

    @property(NodePool)
    coinPool: NodePool[] = [];

    @property(NodePool)
    coinAnimPool: NodePool[] = [];


    init() {
        for (let i = 0; i < this.boxPool.length; i++) {
            this.boxPool[i].init("Box");
        }

        for (let i = 0; i < this.coinPool.length; i++) {
            this.coinPool[i].init("Coin");
        }

        for (let i = 0; i < this.coinAnimPool.length; i++) {
            this.coinAnimPool[i].init("GoldAnim");
        }
    }


    spawnBox(): cc.Node {
        let pool = this.boxPool[0];
        return pool.spawn();
    }

    despwanBox(ballType: number, obj: cc.Node) {
        let pool = this.boxPool[ballType];
        pool.despawn(obj);
    }

    spawnCoin(): cc.Node {
        let pool = this.coinPool[0];
        return pool.spawn();
    }

    despwanCoin(ballType: number, obj: cc.Node) {
        let pool = this.coinPool[ballType];
        pool.despawn(obj);
    }

    spawnCoinAnim(): cc.Node {
        let pool = this.coinAnimPool[0];
        return pool.spawn();
    }
    despwanCoinAnim(obj) {
        let pool = this.coinAnimPool[0];
        return pool.despawn(obj);
    }


    start() {

    }


    // update (dt) {}
}
