import WXCommon from "./Common/WXCommon";
import HttpRequest from "./Common/HttpRequest";
import StorageUtil from "./Common/StorageUtil";
import UserData from "./Bean/UserData";


const { ccclass, property } = cc._decorator;

@ccclass
export default class InviteControl extends cc.Component {

    @property(cc.Prefab)
    inviteItem: cc.Prefab = null;

    m_ArrayLength: number = 5;
    m_InviteListArray: Array<cc.Node> = new Array<cc.Node>();

    wxCommon: WXCommon = null;

    httpRequest: HttpRequest = new HttpRequest();
    storageUtill: StorageUtil = new StorageUtil();



    onLoad() {
        // this.initListView();
    }

    start() {

    }

    initListView() {


        // cc.find('InviteList').height =  cc.winSize.height*0.57;
        this.node.getParent().getParent().height = cc.winSize.height * 0.57;
        let itemHeight = this.node.getParent().getParent().height / this.m_ArrayLength;


        this.node.removeAllChildren();
        this.m_InviteListArray.splice(0, this.m_InviteListArray.length);
        let inviteNum = this.storageUtill.getNumberData(this.getDate() + "inviteNum", 0);
        for (let i = 0; i < inviteNum; i++) {

            let item = cc.instantiate(this.inviteItem).getComponent('InviteItem');
            this.m_InviteListArray.push(item.node);
            item.setIndex(i + 1);
            item.setIsInvited(true);
            // item.setGameAndDialog(this.game, this);
            // item.node.width = cc.winSize.width - cc.winSize.width * 0.056 * 2 - 48;
            // item.node.height = itemHeight;
            // item.node.x = 0;
            this.node.addChild(item.node);
        }
        for (let i = inviteNum; i < this.m_ArrayLength; i++) {
            let item = cc.instantiate(this.inviteItem).getComponent('InviteItem');
            this.m_InviteListArray.push(item.node);
            item.setIndex(i + 1);
            item.setIsInvited(false);
            // item.node.height = itemHeight;
            console.log('invite item height = ' + item.node.height);
            // item.setGameAndDialog(this.game, this);
            // item.node.width = cc.winSize.width - cc.winSize.width * 0.056 * 2 - 48;
            // item.node.height = 130 *this.content.node.width/540;
            // item.node.x = 0;
            this.node.addChild(item.node);
        }

        this.node.getComponent(cc.Layout).updateLayout();

        // for(let i = 0; i < this.m_ArrayLength; i++) {

        //     let item = this.m_InviteListArray[i].getComponent('InviteItem');
        //     console.log('invite item height 1= '+item.node.height);
        //     item.setIndex(i+1);
        // }

    }

    getDate() {
        var myDate = new Date();
        var year = myDate.getFullYear();
        var yue = myDate.getMonth();
        var day = myDate.getDate();
        var riqi = year + "-" + yue + "-" + day;
        return riqi;
    }


    setData() {
        this.node.removeAllChildren();
        // this.initDefaultData();
        let userBean: UserData = this.storageUtill.getData("userData");

        this.httpRequest.queryShareUser(userBean.uid, 1, 20, function (data) {

            console.log("queryShareUser ", data);
            let length = data.data.length > 5 ? 5 : data.data.length;
            for (let i = 0; i < length; i++) {
                let itemData = data.data[i];
                let item = cc.instantiate(this.inviteItem).getComponent('InviteItem');
                // item.setGameAndDialog(this.game, this);
                // item.node.width = cc.winSize.width - cc.winSize.width * 0.056 * 2 - 48;
                // item.node.height = 130 *this.content.node.width/540;
                // item.node.x = 0;
                item.setIndex(i + 1);
                item.setData(itemData);
                this.node.addChild(item.node);
            }

            for (let i = length; i < 5; i++) {
                let item = cc.instantiate(this.inviteItem).getComponent('InviteItem');
                item.setIndex(i + 1);
                item.setIsInvited(false);
                // item.setGameAndDialog(this.game, this);
                // item.node.width = cc.winSize.width - cc.winSize.width * 0.056 * 2 - 48;
                // item.node.height = 130 *this.content.node.width/540;
                // item.node.x = 0;
                this.node.addChild(item.node);
            }
        }.bind(this));

    }

    updateList() {
        console.log("updateList")
        this.initListView()
        this.setData();
    }

    // update (dt) {}
}
