// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import StorageUtil from './Common/StorageUtil'
import RecordBean from './Bean/RecordBean';
import AnalyticsUtilities from "./platform/analytics/AnalyticsUtilities";
import WXCommon from './Common/WXCommon';

@ccclass
export default class RecordShareDialog extends cc.Component {

    @property(cc.Node)
    gameView: cc.Node = null;

    @property(cc.Button)
    getBtn: cc.Button = null;

    @property(cc.Button)
    closeBtn: cc.Button = null;

    storageUtil: StorageUtil = new StorageUtil();

    recordData: RecordBean = new RecordBean();;
    wxCommon: WXCommon = new WXCommon();

    onLoad() {

    }

    start() {

    }
    show() {
        AnalyticsUtilities.logEvent('record_share_show');
        this.node.active = true;
        this.closeBtn.node.active = false;
        setTimeout(() => {
            this.closeBtn.node.active = true;
        }, 3000);
        this.recordData = this.storageUtil.getData('recordData');
        if (this.recordData == null) {
            this.recordData = new RecordBean();
        }
        if (this.recordData.date && this.recordData.date === new Date().getMonth() + '_' + new Date().getDate()) {
            this.recordData.num = this.recordData.num + 1;
        } else {
            this.recordData.date = new Date().getMonth() + '_' + new Date().getDate()
            this.recordData.num = 1;
        }
        this.storageUtil.saveData('recordData', this.recordData);
    }

    onGetBtnClick() {
        //添加分享或者视频接口
        this.wxCommon.shareToFrends('', '', '', '超神时刻分享领取奖励');
        var myDate = new Date();
        let currentTime = myDate.getTime();
        this.storageUtil.saveData("shareTime", currentTime);
        this.storageUtil.saveData("shareType", this.wxCommon.GOD_SHARE);


        this.node.active = false;
        AnalyticsUtilities.logEvent('record_share_click');
    }



    onCloseBtnClick() {

        let gameTs = cc.find('Canvas').getComponent('MainControl').gameControlView.getComponent('GameControl');
        let awardPercent = gameTs.currentLevel % gameTs.awardStep;
        gameTs.showSettleDialog(awardPercent);
        this.node.active = false;
    }
    // update (dt) {}
}
