// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import { i18nMgr } from '../i18n/i18nMgr';
import UserData from './Bean/UserData'
import LocalDataManager from './Common/LocalDataManager';
import ReportMgr, { ReportConfig } from './Common/ReportMgr';
import StorageUtil from './Common/StorageUtil'
import GlobalConst from './GlobalConst';
import LevelData from './LevelData';
import { BaseLayer } from './base/BaseLayer';
const WX = window["wx"];

@ccclass
export default class ThemeBox extends BaseLayer {

    @property(cc.Node)
    unlockNode: cc.Node = null;
    @property(cc.Node)
    spriteNode: cc.Node = null;
    @property(cc.Node)
    txtNode: cc.Node = null;

    themeView: cc.Node = null;

    innerLevelGrid: cc.Node = null;

    m_Chapter: number = null;
    m_Theme: number = null;


    userData: UserData;
    storageUtil: StorageUtil = new StorageUtil();
    globalConst: GlobalConst = new GlobalConst();
    // LIFE-CYCLE CALLBACKS:

    @property(sp.Skeleton)
    spine: sp.Skeleton = null;

    //未玩到的主题，不解锁,无法点击
    lockEnable: boolean = true;
    onLoad() {
        
     }

    setChapterTheme(chapter, theme) {
        this.m_Chapter = chapter;
        this.m_Theme = theme;
        this.changeBoxBg();
        let isShow = LocalDataManager.shareManager().isShowThemeHand();
        if(chapter == 1 && theme == 1 && !isShow){
            // 在这里加载 Spine 动画
            this.spine.node.active = true;
            this.spine.setAnimation(0, "newAnimation", true)
        }

        let index = (theme - 1) * 9 + chapter;
        cc.resources.load("head/icon" + index,cc.SpriteFrame,(err,asset:cc.SpriteFrame)=>{
            this.spriteNode.getComponent(cc.Sprite).spriteFrame = asset;
        });
        
    }

    onClick() {
        if (this.unlockNode.active == true) {
            let theme = (this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter
            let unlockLevel = ((this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter - 1) * this.globalConst.levelCount + 1
            // this.themeView.getComponent('ThemeViewControl').showUnlockView(theme, unlockLevel,{fromType:2,theme:this.m_Theme,chapter:this.m_Chapter});
            this.themeView.getComponent('ThemeViewControl').showUnlockAction(theme, unlockLevel,{fromType:2,theme:this.m_Theme,chapter:this.m_Chapter},this);
            // ReportMgr.getInst().uploadEvent("click_theme_item", "page_ad_page_home_click_theme_item_inter", this.m_Theme, this.m_Chapter, undefined, 0);
            this.uploadState(0);
            return;
        }
        if (this.lockEnable == true) {
            return;
        }
        this.spine.node.active = false;
        this.themeView.active = false;
        this.innerLevelGrid.getParent().active = true;
        this.innerLevelGrid.getComponent('InnerLevelControl').initLevelData(this.m_Theme, this.m_Chapter);
        this.themeView.getParent().getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        // ReportMgr.getInst().uploadEvent("click_theme_item", "page_ad_page_home_click_theme_item_inter", this.m_Theme, this.m_Chapter, undefined, 1);
        this.uploadState(1);
    }

    unlockEnterIn(){
        this.spine.node.active = false;
        this.themeView.active = false;
        this.innerLevelGrid.getParent().active = true;
        this.innerLevelGrid.getComponent('InnerLevelControl').initLevelData(this.m_Theme, this.m_Chapter);
        this.themeView.getParent().getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        // ReportMgr.getInst().uploadEvent("click_theme_item", "page_ad_page_home_click_theme_item_inter", this.m_Theme, this.m_Chapter, undefined, 1);
        // this.uploadState(1);
    }

    uploadState(state){
        let info = ReportConfig.ReportMap;
        info.theme = this.m_Theme;
        info.chapter = this.m_Chapter;
        // info.level = this.level;
        info.state = state;
        info.event = "click_theme_item";
        info.event_msg = "page_home";
        if(state == 0){
            info.page = "page_ad_page_home_click_theme_item_inter";
        }
        ReportMgr.getInst().uploadConfig(info);
    }


    setInnerGird(innerGrid) {
        this.innerLevelGrid = innerGrid;
    }

    setThemeView(themeView) {
        this.themeView = themeView;
    }

    // update (dt) {}
    changeBoxBg() {

        this.unlockNode.active = false;
        this.userData = this.storageUtil.getData('userData');
        if (this.userData == null) {
            this.userData = new UserData();
        }

        let unLockLevel = this.storageUtil.getNumberData('unLockLevel', 0);
        let unLockChapter = Math.ceil(unLockLevel / this.globalConst.levelCount);

        let currentChapter = Math.ceil(this.userData.gameLevel / this.globalConst.levelCount);
        let boxChapter = (this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter;
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        let lock = this.storageUtil.getData('unLockTheme' + boxChapter);
        let colorList = ['#713D97', '#D948A3', '#4852D9', '#30B450', '#E36D28', '#4852D9']
        if (mainTs.Membership || boxChapter == 1 || lock) {
            this.lockEnable = false;
            let themeBoxPath = new LevelData().currentUrl + "/themeIcon/icon" + boxChapter + ".png"//;
            this.spriteNode['picLv'] = boxChapter
            // this.LoadNetImg(themeBoxPath, (image) => {
            //     if (this.spriteNode['picLv'] != boxChapter) return;
            //     let frame = new cc.SpriteFrame(image)
            //     cc.loader.setAutoReleaseRecursively(frame, true);
            //     this.spriteNode.getComponent(cc.Sprite).spriteFrame = frame;
            // })

            // cc.loader.loadRes(themeBoxPath, cc.SpriteFrame, function (err, frame) {
            // }.bind(this));
            this.unlockNode.active = false;
            // if (currentChapter == boxChapter && unLockChapter == currentChapter) {
            //     this.unlockNode.active = true;
            // }
        } else {
            this.unlockNode.active = true;
            // let themeBoxPath = new LevelData().currentUrl + "/themeIcon/iconBurl" + boxChapter + ".png"//;
            this.spriteNode['picLv'] = boxChapter
            // this.LoadNetImg(themeBoxPath, (image) => {
            //     if (this.spriteNode['picLv'] != boxChapter) return;
            //     let frame = new cc.SpriteFrame(image)
            //     cc.loader.setAutoReleaseRecursively(frame, true);
            //     this.spriteNode.getComponent(cc.Sprite).spriteFrame = frame;
            // })
        }

        this.txtNode.active = true;
        // this.txtNode.getComponent(cc.Widget).updateAlignment()
        this.txtNode.getComponent(cc.LabelOutline).color = new cc.Color().fromHEX(colorList[this.m_Theme - 1])
        this.txtNode.getComponent(cc.LabelShadow).color = new cc.Color().fromHEX(colorList[this.m_Theme - 1])
        this.txtNode.getComponent(cc.Label).string = i18nMgr._getLabel('txt_' + (1005 + boxChapter))


        let themeBoxPath = 'ThemeTitle/bg' + this.m_Theme;
        cc.loader.loadRes(themeBoxPath, cc.SpriteFrame, function (err, frame) {
            if (err) {
                cc.log('error to loadRes: ' + themeBoxPath + ', ' + err || err.message);
                return;
            }
            // 自动释放SpriteFrame和关联Texture资源
            cc.loader.setAutoReleaseRecursively(frame, true);
            this.node.getComponent(cc.Sprite).spriteFrame = frame;
        }.bind(this));
    }

    isNull(str) {
        if (typeof (str) == "undefined" || str == "") {
            return true;
        } else {
            return false;
        }
    }
}
