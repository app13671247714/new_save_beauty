// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
        isLabel : false,
        isWidgetTop : false,
        isWidgetLeft : false,
        isWidgetRight : false,
        isWidgetBottom : false,
        isVerticalCenter:false,
        isHorizontalCenter:false,
        isScale : false,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
         // 1. 先找到 SHOW_ALL 模式适配之后，本节点的实际宽高以及初始缩放值
        //  let srcScaleForShowAll = Math.min(cc.winSize.width / 540, cc.winSize.height / 960);
        // //  let realWidth = this.node.width * srcScaleForShowAll;
        // //  let realHeight = this.node.height * srcScaleForShowAll;
 
        // //  // 2. 基于第一步的数据，再做节点宽高适配
        // if(this.isScale){
        //     this.node.scale = this.node.scale * srcScaleForShowAll;
        // }else{
        //     this.node.width = this.node.width * srcScaleForShowAll;
        //     this.node.height = this.node.height * srcScaleForShowAll;
        // }
         
        // if(this.isLabel){
        //     var label = this.node.getComponent(cc.Label);
        //     label.fontSize = label.fontSize * srcScaleForShowAll;
        // }
        // if(this.isWidgetTop){
        //     var widget = this.node.getComponent(cc.Widget);
        //     widget.top = widget.top * srcScaleForShowAll;
        // }

        // if(this.isWidgetLeft){
        //     var widget = this.node.getComponent(cc.Widget);
        //     widget.left = widget.left * srcScaleForShowAll;
        // }
        // if(this.isVerticalCenter){
        //     this.node.getComponent(cc.Widget).verticalCenter = 0;
        // }
    },

    start () {

    },

    // update (dt) {},
});
