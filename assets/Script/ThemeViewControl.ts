

const { ccclass, property } = cc._decorator;
import UserData from './Bean/UserData'
import StorageUtil from './Common/StorageUtil'
import WXCommon from './Common/WXCommon'
import CoinsUtil from './CoinsUtil'
import GlobalConst from './GlobalConst';
import { i18nMgr } from '../i18n/i18nMgr';
import MainControl from './MainControl';
import HttpRequestNew from "./Common/HttpRequestNew";
import { JsBridge } from "./native/JsBridge";
import List from './scrollViewTools/List';
import ReportMgr from './Common/ReportMgr';
import ThemeBox from './ThemeBox';

const WX = window["wx"];

@ccclass
export default class ThemeViewControl extends cc.Component {


    @property(cc.Node)
    m_ScrollView: cc.Node = null;
    @property(cc.Node)
    m_UnlockChapterView: cc.Node = null;

    @property(cc.Node)
    m_UnlockAnim: cc.Node = null;
    @property(cc.Node)
    GridLayout: cc.Node = null;

    @property(cc.Node)
    m_Dialog: cc.Node = null;
    @property(cc.Label)
    unlockLabel: cc.Label = null;
    @property(cc.Node)
    shopingView: cc.Node = null;
    @property(cc.Label)
    shopingLabel: cc.Label = null;
    @property(cc.SpriteFrame)
    btnSprite: cc.SpriteFrame[] = [];
    @property(List)
    list3: List = null;

    @property(cc.Label)
    titleLabel:cc.Label = null;

    m_ThemeGridArray: Array<cc.Node> = new Array<cc.Node>();

    userData: UserData = null;
    storageUtil: StorageUtil = new StorageUtil();
    globalConst: GlobalConst = new GlobalConst();
    httpRequestNew: HttpRequestNew = new HttpRequestNew();
    wxCommon: WXCommon = new WXCommon();

    coinsUtil: CoinsUtil = new CoinsUtil();

    fromData;
    themeBox:ThemeBox = null;
    mainControl:MainControl = null;

    start() {
        // this.wxCommon.registerOnshow(this.onShared2Friends);
        if(i18nMgr.getLanguage() == "ja"){
            this.titleLabel.string = "レスキュービューティ";
        }else if(i18nMgr.getLanguage() == "ko"){
            this.titleLabel.string = "레스큐 뷰티";
        }else{
            this.titleLabel.string = "Rescue Beauty";
        }
    }


    //点击解锁分享
    onUnlockShareClick() {
        // this.storageUtil.saveData('unLock', true);
        this.storageUtil.saveData('unLockLevel', 0);
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        mainTs.loadVideoAd(mainTs.UnLockChapter);
    }
    public theme; unlockLevel
    successLockTheme() {
        this.storageUtil.saveData('unLockTheme' + this.theme, true);
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        mainTs.unLockThemeCount++;
        this.storageUtil.saveData('unLockThemeCount', mainTs.unLockThemeCount);
        this.storageUtil.saveData('unLockLevel' + this.unlockLevel, 1);
        this.storageUtil.saveData('playLevel', Math.ceil(this.theme / this.globalConst.chapterCount));
        this.storageUtil.saveData('unLockThemeMaxLevel' + this.theme, this.unlockLevel);
        this.m_UnlockAnim.active = true;
        this.m_Dialog.active = false;
        
        if(this.themeBox){
            this.refreshThemeUI();
        }else if(this.mainControl){
            this.refreshAlbumUI();
        }else{
            this.m_UnlockAnim.getComponent(dragonBones.ArmatureDisplay).playAnimation('newAnimation', 1);
            this.m_UnlockAnim.getComponent(dragonBones.ArmatureDisplay).addEventListener(dragonBones.EventObject.COMPLETE, this.animEventHandler, this);
            ReportMgr.getInst().uploadEvent("evt_unlock_theme","success");
        }
    }
    successLockLevel() {
        cc.log(this.unlockLevel)
        this.storageUtil.saveData('unLockLevel' + this.unlockLevel, 1);
        let theme = Math.ceil(this.unlockLevel / this.globalConst.levelCount);
        this.storageUtil.saveData('unLockThemeMaxLevel' + theme, this.unlockLevel);
        let theme1 = Math.ceil(this.unlockLevel / this.globalConst.chapterCount / this.globalConst.levelCount);
        let chapterTemp = this.unlockLevel - (theme1 - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
        let chapter = Math.ceil(chapterTemp / this.globalConst.levelCount);
        cc.log(theme1, chapter, "theme1, chapter")
        //传入解锁的等级 382 
        this.GridLayout.getComponent('InnerLevelControl').initLevelData(theme1, chapter,this.unlockLevel);
        // ReportMgr.getInst().uploadEvent("evt_unlock_level","success");
    }
    onUnlockCostClick() {
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        mainTs.loadInterAd(mainTs.unlockTheme,this.fromData.fromType == 3 ? 1 : 0);

        if(this.fromData.fromType == 3){
            ReportMgr.getInst().uploadEvent("click_alert_unlock_album", "page_album", undefined, undefined, undefined, 1)
        }else if(this.fromData.fromType == 2){
            ReportMgr.getInst().uploadEvent("click_alert_unlock_theme", "page_home", this.fromData.theme, this.fromData.chapter, undefined, 1)
        }
    }

    animEventHandler(event) {
        if (event.type === dragonBones.EventObject.COMPLETE) {
            this.m_UnlockAnim.active = false;
            this.m_UnlockChapterView.active = false;
            this.m_ScrollView.getComponent('ThemeScrollControl').refreshTheme();
            if (this.list3.node.active) {
                this.list3.updateAll()
            }
        }
    }

    /**
     * 
     * @param theme 
     * @param unlockLevel 
     * @param fromType 弹窗来源 3图集内相册解锁 2解锁主题
     */
    showUnlockView(theme, unlockLevel,fromData?) {
        if(fromData){
            this.fromData = fromData;
            if(fromData.fromType == 3){
                ReportMgr.getInst().uploadEvent("evt_alert_unlock_album", "page_album", this.fromData.theme, this.fromData.chapter)
            }else if(fromData.fromType == 2){
                ReportMgr.getInst().uploadEvent("evt_alert_unlock_theme", "page_home", this.fromData.theme, this.fromData.chapter)
            }
        }
        cc.log(theme, unlockLevel, "theme, unlockLevel")
        this.theme = theme
        this.unlockLevel = unlockLevel
        this.m_UnlockChapterView.active = true;
        this.m_Dialog.active = true;
        this.m_Dialog.active = true;
    }

    showUnlockAction(theme, unlockLevel,fromData,node){
        if(fromData){
            this.fromData = fromData;
        }
        this.theme = theme
        this.unlockLevel = unlockLevel
        if(node){
            this.themeBox = node;
        }
        this.onUnlockCostClick();
    }

    refreshThemeUI(){
        this.m_UnlockAnim.active = false;
        this.m_UnlockChapterView.active = false;
        this.m_ScrollView.getComponent('ThemeScrollControl').refreshTheme();
        this.themeBox.unlockEnterIn();
        this.themeBox = null;
    }

    showUnlockAlbum(theme, unlockLevel,fromData,node){
        if(fromData){
            this.fromData = fromData;
        }
        this.theme = theme
        this.unlockLevel = unlockLevel
        if(node){
            this.mainControl = node;
        }
        this.onUnlockCostClick();
    }

    refreshAlbumUI(){
        this.m_UnlockAnim.active = false;
        this.m_UnlockChapterView.active = false;
        if (this.list3.node.active) {
            this.list3.updateAll()
        }
        this.mainControl = null;
    }

    closeUnlockView() {
        this.m_UnlockChapterView.active = false;
        this.m_ScrollView.getComponent('ThemeScrollControl').refreshTheme();
    }

    onBackClick() {
        WX.aldSendEvent('解锁界面返回点击');
        this.closeUnlockView();
        if(this.fromData.fromType == 2){//相册关闭
            ReportMgr.getInst().uploadEvent("click_alert_unlock_album", "page_album", undefined, undefined, undefined, 0)
        }
    }

    refreshTheme() {
        this.m_ScrollView.getComponent('ThemeScrollControl').refreshTheme();
    }
    btnOpenShop() {
        this.shopingView.active = false;
        let mainTs = cc.find('Canvas').getComponent(MainControl);
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.shopingLabel.string = i18nMgr._getLabel(mainTs.Membership ? "txt_116" : "txt_114")
        let bg = this.shopingView.getChildByName("bg").getComponent(cc.Sprite)
        let btn = this.shopingView.getChildByName("btn").getComponent(cc.Sprite)
        let icon = btn.node.getChildByName("layout").getChildByName("icon").getComponent(cc.Sprite)
        if (mainTs.Membership) {
            bg.node.height = 1028
            bg.spriteFrame = this.btnSprite[1]
            btn.spriteFrame = this.btnSprite[3]
            icon.spriteFrame = this.btnSprite[5]
            btn.node.getComponent(cc.Button).enabled = false;
            this.shopingView.getChildByName("recover").active = false;
        } else {
            this.shopingView.getChildByName("recover").active = true;
            bg.node.height = 1090
            bg.spriteFrame = this.btnSprite[0]
            btn.spriteFrame = this.btnSprite[2]
            icon.spriteFrame = this.btnSprite[4]
            btn.getComponent(cc.Button).enabled = true;
        }
    }
    btnOpenShop1() {
        this.m_UnlockChapterView.active = false;
        this.shopingView.active = false;
    }
    btnCloseShop() {
        let mainTs = cc.find('Canvas').getComponent(MainControl);
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.shopingView.active = false;
    }
    btnPaySuccess(event) {
        let mainTs = cc.find('Canvas').getComponent(MainControl);
        if (mainTs.Membership) {
            this.shopingView.active = false;
            return;
        }
        JsBridge.callWithCallback(JsBridge.PAY, {}, (jsonStr: any) => {
            if (jsonStr != null) {
                let resultType = jsonStr.resultType
                let bodyParams = jsonStr.params
                if (resultType == 99) {
                    let type = JsBridge.isIosPlatform() ? 1 : 2
                    this.httpRequestNew.callback(type, bodyParams, (data) => {
                        console.log("oder result:", data)
                        if (data.Code == 0 || data.Code == 10004) {
                            JsBridge.callWithCallback(JsBridge.CONSUME, { token: bodyParams.token }, (jsonStr: any) => {
                                if (jsonStr != null) {
                                    let resultType = jsonStr.resultType
                                    if (resultType == 1) {
                                        this.paySuccess()
                                    }
                                }
                            });
                            if (data.Code == 10004) {
                                this.paySuccess()
                            }
                        }
                    })
                }
            }
        });
    }

    paySuccess() {
        this.shopingView.active = false;
        let mainTs = cc.find('Canvas').getComponent(MainControl);
        mainTs.Membership = true;
        cc.sys.localStorage.setItem("Membership", true)
        this.refreshTheme()
        mainTs.hideVideo()
        if (this.list3.node.active) {
            this.list3.updateAll()
        }
        JsBridge.callWithCallback(JsBridge.MEMBERSHIP, {}, (jsonStr: object) => {
            if (jsonStr != null) {

            }
        });
    }
}
