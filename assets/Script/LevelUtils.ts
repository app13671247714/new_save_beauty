// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
const WX = window["wx"];

@ccclass
export default class LevelUtils {
    //主题配置
    theme_config:string='';
    loadThemeJson(callback) {
        if(this.theme_config === ""){
            let jsonFileName: string = 'theme';
            cc.loader.loadRes(jsonFileName, cc.JsonAsset, function (err, jsonAsset: cc.JsonAsset) {
                if (!err) {
                    let json = jsonAsset.json;
                    this.theme_config = json;
                    callback(json);
                }
            }.bind(this));
        }else{
            callback(this.theme_config);
        }
        
    }

    loadLevelJson(worldLevel: number, secondaryLevel: number,callback) {
        let jsonFileName: string = worldLevel.toString() + "/" + secondaryLevel.toString();
        console.log('levelData loadLevelJson jsonFileName  = ', jsonFileName);
        cc.loader.loadRes(jsonFileName, cc.JsonAsset, function (err, jsonAsset: cc.JsonAsset) {
            console.log('levelData loadLevelJson err  = ', err);
            console.log('levelData loadLevelJson jsonAsset  = ', jsonAsset);
            if (!err) {
                let json = jsonAsset.json.json;
                callback(json);
            }else{
                this.plsWaitUpdate();
            }
        }.bind(this));
    }

    getBinString(bin: string): string {
        let binStr: string = "";
        for (let i = 0; i < bin.length; i++) {
            let char = bin.charAt(i);
            let charNum = this.hex_to_bin(char);
            binStr = binStr + charNum;
        }
        return binStr;
    }


    hex_to_bin(str: string): string {
        let hex_array = [{ key: 0, val: "0000" }, { key: 1, val: "0001" }, { key: 2, val: "0010" }, { key: 3, val: "0011" }, { key: 4, val: "0100" }, { key: 5, val: "0101" }, { key: 6, val: "0110" }, { key: 7, val: "0111" },
        { key: 8, val: "1000" }, { key: 9, val: "1001" }, { key: 'a', val: "1010" }, { key: 'b', val: "1011" }, { key: 'c', val: "1100" }, { key: 'd', val: "1101" }, { key: 'e', val: "1110" }, { key: 'f', val: "1111" }]

        let value = "";
        for (let i = 0; i < str.length; i++) {
            for (let j = 0; j < hex_array.length; j++) {
                if (str.charAt(i) == hex_array[j].key) {
                    value = value.concat(hex_array[j].val)
                    break
                }
            }
        }
        return value;
    }

    hex_to_levelData(binStr : string){
        let result = [];
        let binStrTemp = binStr;
        if("1111" == binStrTemp.substring(0,4)){
            binStrTemp = binStrTemp.substring(6);
        }
        let startStrX = binStrTemp.substring(0, 4);
        let startStrY = binStrTemp.substring(4, 8);
        binStrTemp = binStrTemp.substring(8);
        let startX = parseInt(startStrX,2);
        let startY = parseInt(startStrY,2);
        result.push([startX,startY]);
        for(;binStrTemp.length >= 2;){
            
            let tempStr = binStrTemp.substring(0, 2);
            if("00" == tempStr){
                startY = startY - 1;
            }
            if("01" == tempStr){
                startY = startY + 1;
            }
            if("10" == tempStr){
                startX = startX - 1;
            }
            if("11" == tempStr){
                startX = startX + 1;
            }
            result.push([startX,startY]);
            binStrTemp = binStrTemp.substring(2);
        }
        return result;
        
        
    }

    plsWaitUpdate(){
        cc.find('Canvas').getComponent('MainControl').back2LaunchView();
        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            WX.showToast({
                title: '恭喜您闯关成功，敬请期待新的挑战！',
                icon: 'none',
                duration: 2000
            });
        }
    }
}