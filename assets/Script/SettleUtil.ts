

const { ccclass, property } = cc._decorator;

@ccclass
export default class SettleUtil {
     //结算耗时配置
     settle_timer_config:string='';
     //结算排名配置
     settle_rank_config:string='';

    loadTimerJson(callback) {
        if(this.settle_timer_config === ""){
            let jsonFileName: string = 'timer';
            cc.loader.loadRes(jsonFileName, cc.JsonAsset, function (err, jsonAsset: cc.JsonAsset) {
                if (!err) {
                    let json = jsonAsset.json;
                    this.settle_timer_config = json;
                    callback(json);
                }
            }.bind(this));
        }else{
            callback(this.settle_timer_config);
        }
        
    }


    getOverPlayerPercent(timeCost:number, pathLength:number, callback){
        this.loadTimerJson(function(json){
            let pathRange = this.getValueByNumber(pathLength, json);
            let timeRange = this.getValueByNumber(timeCost, pathRange);
            callback(this.getRandom(timeRange[1], timeRange[0]));
            
        }.bind(this))

    }

    getValueByNumber(num:number, obj:object){
        for(var i in obj) {
            if(i != 'other'){
                let iNum = Number(i);
                let index = Math.floor(Number(num)/iNum) * iNum + iNum;
                if(index === iNum){
                    return obj[index];
                }
            }else{
                return obj['other'];
            }
        }
    }

    getRandom(min, max){
        max = max * 100;
        min = min * 100;
        return Math.floor((Math.random()*(max-min+1)+min))/100;
    }

    getOverPlayerNums(currentLevel) {
        let random = 1e5 * (Math.random() + currentLevel - 1) + 3e5;
        return Math.floor(random);
    }
}