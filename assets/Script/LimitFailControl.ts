
const { ccclass, property } = cc._decorator;
import { i18nMgr } from '../i18n/i18nMgr';
import ReportMgr, { ReportConfig } from './Common/ReportMgr';
import GameChallenge from './GameChallenge';
@ccclass
export default class LimitFailControl extends cc.Component {

    @property(cc.Label)
    currentLabel: cc.Label = null;

    @property(cc.Node)
    noRank:cc.Node = null;

    @property(cc.Node)
    rankNode:cc.Node = null;

    @property(cc.Label)
    rankLabel: cc.Label = null;

    @property(cc.Label)
    msgLabel: cc.Label = null;


    currentLevel: number = 1;
    mainCanvas;
    m_CatFace;

    onLoad() {
        this.currentLabel.string = i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString());
    }



    init(mainCanvas,m_CatFace){
        this.mainCanvas = mainCanvas;
        this.m_CatFace = m_CatFace;
    }

    showDialog(currentLevel) {
        this.currentLevel = currentLevel;
        this.currentLabel.string = i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString());
        this.node.zIndex = this.m_CatFace.zIndex + 1;
        this.show()
    }

    refreshUI(){
        let rank = GameChallenge.Instance.myRankIndex
        if(rank > 0){
            this.noRank.active = false;
            this.rankNode.active = true;
            this.rankLabel.string = rank.toString()
        }else{
            this.noRank.active = true;
            this.rankNode.active = false;
        }

        this.msgLabel.node.active = true;
        let needIndex = GameChallenge.Instance.needLevel;
        if(needIndex > 0){
            this.msgLabel.string = i18nMgr._getLabel("txt_139").replace("&",needIndex.toString());
        }else{
            this.msgLabel.node.active = false;
        }
    }

    show(){
        this.node.active = true;
        this.node.zIndex = this.m_CatFace.zIndex + 1;
        this.refreshUI()
    }

    close() {
        this.node.active = false;
    }

    //继续挑战
    onReviveClick() {
        let mainTs = this.mainCanvas.getComponent('MainControl');
        mainTs.loadVideoAd(mainTs.Revive);

        let info = ReportConfig.ReportMap;
        info.level = this.currentLevel;
        info.event = "click_retry";
        info.event_msg = "page_challenge";
        info.rank = GameChallenge.Instance.myRankIndex >= 0 ? GameChallenge.Instance.myRankIndex : 0;
        info.page = "page_ad_page_challenge_click_retry_rewarded";
        ReportMgr.getInst().uploadConfig(info);
    }

}
