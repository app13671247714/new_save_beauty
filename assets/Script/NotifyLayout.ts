import ReportMgr, { ReportConfig } from "./Common/ReportMgr";


const { ccclass, property } = cc._decorator;
const WX = window['wx'];

@ccclass
export default class NotifyLayout extends cc.Component {
    @property(cc.Node)
    notify: cc.Node = null;
    @property(cc.Node)
    closeBtn: cc.Node = null;
    @property(cc.Node)
    videoNode: cc.Node = null;

    gameControl: any = null;

    theme:number = 0;
    chapter:number = 0;
    level:number = 0;

    setGameControl(gameControl) {
        this.gameControl = gameControl;
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    }

    show(theme,chapter,level) {
        ReportMgr.getInst().uploadEvent("evt_show_Notify", "page_game", theme, chapter, level)
        this.theme = theme;
        this.chapter = chapter;
        this.level = level;
        this.gameControl.startCountDown = false;
        this.node.active = true;
        this.closeBtn.opacity = 0;
        let action = cc.fadeIn(1);
        let delay = cc.delayTime(2);
        this.closeBtn.runAction(cc.sequence(delay, action));
        // WX.aldSendEvent('提示弹框界面展示');

        let mainTs = cc.find('Canvas').getComponent('MainControl');
        if (mainTs.Membership) {
            this.videoNode.active = false;
        }
        mainTs.showBanner(mainTs.NotifyGuide, function (bannerHeight) {

        }.bind(this));
    }

    onCloseClick() {
        // WX.aldSendEvent('提示弹框界面关闭');
        this.close();
    }
    close() {
        ReportMgr.getInst().uploadEvent("click_noti_alert_close", "page_game", this.theme, this.chapter, this.level);
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        this.node.active = false;
        this.gameControl.startCountDown = true;
    }

    onNotifyClick() {
        WX.aldSendEvent('提示弹框界面-看提示点击');
        this.gameControl.onNotifyVideoClick();

        let info = ReportConfig.ReportMap;
        info.theme = this.theme;
        info.chapter = this.chapter;
        info.level = this.level;
        // info.state = state;
        info.event = "click_noti_alert_tips";
        info.event_msg = "page_game";
        info.page = "page_ad_page_game_click_noti_alert_tips_rewarded";
        ReportMgr.getInst().uploadConfig(info);

        // ReportMgr.getInst().uploadEvent("click_noti_alert_tips", "page_ad_page_game_click_noti_alert_tips_rewarded", this.theme, this.chapter, this.level);
    }

}
