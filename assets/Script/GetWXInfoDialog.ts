// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import HttpRequest from "./Common/HttpRequest"
import User from './Bean/WXUserBean'
import GameBean from './Bean/UserData'
import StorageUtill from './Common/StorageUtil'
import WXUserInfo from './Bean/WXUserBean'
import UserData from "./Bean/UserData";

const WX = window["wx"];

@ccclass
export default class GetWXInfoDialog extends cc.Component {

    @property(cc.Label)
    content: cc.Label = null;

    @property(cc.Node)
    WXInfoButton: cc.Node = null;

    httpRequest: HttpRequest = new HttpRequest();

    userBean: User = null;
    gameBean: GameBean = null;

    storageUtil: StorageUtill = new StorageUtill();

    // onLoad () {}

    callback = null;

    start() {

    }

    close() {
        this.node.active = false;
        this.wxButton.hide();
    }

    setContent(content) {
        this.content.string = content;
    }

    initWXLogin() {
        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            this.wxCheckUserState();
        }
    }

    wxCheckUserState() {
        WX.getSetting({
            fail: function (res) {
            }.bind(this),

            success: function (res) {
                if (res.authSetting['scope.userInfo']) {
                    this.getUserInfo()
                }
            }.bind(this)
        })
    }

    showWxDialog(callback) {
        this.initWxButton()
        this.node.active = true;
        this.callback = callback;
    }

    wxButton = null;

    initWxButton() {

        // console.log("cc.winSize.height ", cc.winSize.height);

        let btnSize = cc.size(this.WXInfoButton.width + 10, this.WXInfoButton.height + 10);
        let frameSize = cc.view.getFrameSize();
        let winSize = cc.director.getWinSize();

        let left = (winSize.width * 0.5 + this.WXInfoButton.x - btnSize.width * 0.5) / winSize.width * frameSize.width;
        let top = (winSize.height * 0.5 - this.WXInfoButton.y - btnSize.height * 0.5) / winSize.height * frameSize.height;
        let width = btnSize.width / winSize.width * frameSize.width;
        let height = btnSize.height / winSize.height * frameSize.height;



        // console.log("cc.winSize. WXInfoButton x ", this.WXInfoButton.x);
        // console.log("cc.winSize. WXInfoButton y ", this.WXInfoButton.y);
        // console.log("cc.winSize. btnSize width ", btnSize.width);
        // console.log("cc.winSize. btnSize height", btnSize.height);
        // console.log("cc.winSize. frameSize width ", frameSize.width);
        // console.log("cc.winSize. frameSize height ", frameSize.height);
        // console.log("cc.winSize. left ", left);
        // console.log("cc.winSize. top ", top);
        // console.log("cc.winSize. width ", width);
        // console.log("cc.winSize. height ", height);


        this.wxButton = WX.createUserInfoButton({
            type: 'text',
            text: '',
            style: {
                left: left,
                top: top,
                width: width,
                height: height,
                lineHeight: height,
                backgroundColor: '#ffff0000',
                color: '#ffffff',
                textAlign: 'center',
                fontSize: 20,
                borderRadius: 4
            }
        })
        this.wxButton.onTap((res) => {
            console.log("wxGetUserInfo:", res)
            this.updateWXUserInfo(res.userInfo);
            this.node.active = false;
            this.wxButton.hide();
        })
    }

    updateWXUserInfo(userInfo) {
        console.log("updateWXUserInfo userInfo: ", userInfo);
        let userBean = this.storageUtil.getData("userData");
        if (userBean == null) {
            return;
        }
        console.log("updateWXUserInfo userbean uid: ", userBean.uid);
        let wxUserInfo = new WXUserInfo();
        wxUserInfo.uid = userBean.uid;
        wxUserInfo.avatarUrl = userInfo.avatarUrl;

        wxUserInfo.nickName = userInfo.nickName;
        this.storageUtil.saveData('wxUserInfo', wxUserInfo);
        this.httpRequest.saveUserInfo(userBean.uid, userInfo, function (data) {
            console.log("updateWXUserInfo data: ", data);
            // this.userBean.uid = data.uid;
            // this.getRemountGameData();

            // this.storageUtil.saveData("userInfo", this.userBean);
            this.callback(userInfo.nickName, userInfo.avatarUrl);
        }.bind(this))
    }

    getUserInfo() {
        WX.getUserInfo({
            fail: function (res) {
                if (res.errMsg.indexOf('auth deny') > -1 || res.errMsg.indexOf('auth denied') > -1) {
                    // 处理用户拒绝授权的情况
                    this.guideActive()
                }
            }.bind(this),
            success: function (res) {
                console.log("getUserInfo: ", res);
                this.updateWXUserInfo(res.userInfo);
            }.bind(this)
        })
    }

}
