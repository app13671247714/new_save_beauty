/**
 * author: Li_Cheng
 *
 * 用户信息
 */

module.exports = {

    token:          '',         // token
    userId:         '',       // id
    nickName:       null,       // 昵称
    gameUrl:        null,       // 游戏地址
    avatarUrl:      null,       // 头像
    gender:         null,       // 性别
    isAuthorize:    false,      // 是否授权
};