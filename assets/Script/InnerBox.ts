// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

import LevelData from './LevelData'
import UserData from './Bean/UserData'
import StorageUtil from './Common/StorageUtil'
import LevelUtils from './LevelUtils';
import GlobalConst from './GlobalConst';
import ReportMgr from './Common/ReportMgr';
import LocalDataManager from './Common/LocalDataManager';
import { BaseLayer } from './base/BaseLayer';

class SmallNodeInfo {
    //x相对坐标
    x: number = 0;
    //y相对坐标
    y: number = 0;
    //宽度
    width: number = 0;
    //高度
    height: number = 0;
}

enum LockState {
    state_unlock = 1,
    state_AD
}

@ccclass
export default class InnerBox extends BaseLayer {

    @property(cc.Prefab)
    smallInnerPrefab: cc.Prefab = null;
    @property(cc.Node)
    parentNode: cc.Node = null;
    @property({
        type: cc.Integer
    })
    maxItemWidth = 10;

    @property(cc.Node)
    lockNode: cc.Node = null;
    @property(cc.Node)
    playingNode: cc.Node = null;
    @property(cc.Node)
    adNode: cc.Node = null;
    @property(cc.SpriteFrame)
    lockSprite: cc.SpriteFrame[] = [];

    levelData: LevelData = null;
    userData: UserData = null;
    m_Level: number = 1;
    //间隔距离
    spacing: number = 4;

    //子元素宽度
    itemWidth: number = 0;

    m_BoxArray: Array<SmallNodeInfo> = new Array<SmallNodeInfo>();
    m_BoxNodeArray: Array<cc.Node> = new Array<cc.Node>();

    storageUtil: StorageUtil = new StorageUtil();


    drawedColor: cc.Color = new cc.Color();
    playColor: cc.Color = new cc.Color();

    levelUtil: LevelUtils = new LevelUtils();
    globalConst: GlobalConst = new GlobalConst();


    m_RowNum: number = 0;
    m_ColNum: number = 0;
    m_BoxWidth: number = 0;
    m_MaxBoxWidth: number = 85;
    m_LevelConfigArray: Array<Array<number>> = new Array<Array<number>>();
    pathColor: string = '';

    state:LockState;


    // LIFE-CYCLE CALLBACKS:
    init(level,unlockLevel?) {
        this.m_Level = level;
        cc.log(this.m_Level, "this.m_Level____________")
        this.levelData = cc.find('Canvas').getComponent('MainControl').getLevelPathData(this.m_Level);
        // this.levelData = this.storageUtil.getData('levelData'+this.m_Level);
        this.userData = this.storageUtil.getData('userData');
        this.removeAllSmallInnerBox();
        if (this.userData == null) {
            this.userData = new UserData();
        }
        this.changeThemeColor();
        this.setBgImage(level);

        if(level == unlockLevel){
            this.unlockEnterGame();
        }
    }

    setBgImage(level){
        let theme = Math.ceil(level / this.globalConst.chapterCount / this.globalConst.levelCount);
        let chapterTemp = level - (theme - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
        let chapter = Math.ceil(chapterTemp / this.globalConst.levelCount);
        let lv = chapterTemp % this.globalConst.levelCount || 20;
        let facePath = `${new LevelData().currentUrl}/icons/id_${chapter + (theme - 1) * this.globalConst.chapterCount}/level_icon_${lv}.jpg`
        this.LoadNetImg(facePath,(asset)=>{
            let frame = new cc.SpriteFrame(asset)
            this.lockNode.getComponent(cc.Sprite).spriteFrame = frame;
        })
    }

    refreshInnerBox() {
        let maxRowCol = Math.max(this.levelData.colNum, this.levelData.rowNum);
        this.itemWidth = (this.node.width - (maxRowCol - 1) * this.spacing) / maxRowCol;
        this.itemWidth = Math.min(this.itemWidth, this.maxItemWidth);

        this.drawedColor.fromHEX(this.levelData.currentColor);
        // this.lockNode.color = this.drawedColor;
        this.getCatBodyInfo(this.levelData.currentPathIndex, this.levelData.rowNum, this.levelData.colNum, this.itemWidth, this.spacing);
        this.getOriAndUpdatePos(this.levelData.currentPathIndex);
        this.generateSmallNode();
    }

    getCatBodyInfo(path: Array<Array<number>>, row: number, col: number, width: number, space: number) {
        this.m_BoxArray.splice(0, this.m_BoxArray.length);
        for (let index = 0; index < path.length; index++) {
            this.m_BoxArray[index] = new SmallNodeInfo();
            this.m_BoxArray[index].x = this.getPosByIndex(path[index][1], col, width, space);
            this.m_BoxArray[index].y = this.getPosByIndex((row - 1) - path[index][0], row, width, space);
            this.m_BoxArray[index].height = this.itemWidth;
            this.m_BoxArray[index].width = this.itemWidth;
        }
    }

    //根据索引和总个数计算该节点的居中坐标位置
    getPosByIndex(index, count, itemWidth, space) {
        return -(count * itemWidth + (count - 1) * space) / 2 + itemWidth / 2 + index * (itemWidth + space);
    }

    //获取方向后更新x,y以及宽度和高度
    getOriAndUpdatePos(path) {
        for (let index = 1; index < path.length; index++) {
            let cur = path[index];
            let pre = path[index - 1];
            let orientation = "Bottom";
            let verticalOri = cur[0] - pre[0];
            let horizOri = cur[1] - pre[1];
            if (verticalOri < 0) {
                orientation = "Top";
                this.m_BoxArray[index - 1].height = this.itemWidth * 2;
                this.m_BoxArray[index - 1].y += this.itemWidth / 2;
            }

            if (verticalOri > 0) {
                orientation = "Bottom";
                this.m_BoxArray[index - 1].height = this.itemWidth * 2;
                this.m_BoxArray[index - 1].y -= this.itemWidth / 2;
            }

            if (verticalOri == 0) {
                if (horizOri < 0) {
                    orientation = "Left";
                    this.m_BoxArray[index - 1].width = this.itemWidth * 2;
                    this.m_BoxArray[index - 1].x -= this.itemWidth / 2;
                }

                if (horizOri > 0) {
                    orientation = "Right";
                    this.m_BoxArray[index - 1].width = this.itemWidth * 2;
                    this.m_BoxArray[index - 1].x += this.itemWidth / 2;
                }
            }

        }
    }

    generateSmallNode() {
        if (this.m_BoxArray.length > 0) {
            this.removeAllSmallInnerBox();
        }
        this.m_BoxNodeArray.splice(0, this.m_BoxNodeArray.length);
        for (let index = 0; index < this.m_BoxArray.length; index++) {
            let smallNode: cc.Node = cc.instantiate(this.smallInnerPrefab);
            this.parentNode.addChild(smallNode);
            smallNode.x = this.m_BoxArray[index].x;
            smallNode.y = this.m_BoxArray[index].y;
            smallNode.width = this.m_BoxArray[index].width;
            smallNode.height = this.m_BoxArray[index].height;
            this.m_BoxNodeArray[index] = smallNode;
            this.m_BoxNodeArray[index].color = new cc.Color().fromHEX("#FFFFFF")//this.drawedColor;
        }
    }
    private theme = 0
    themeView: cc.Node = null;
    setThemeView(themeView) {
        this.themeView = themeView;
    }

    onClick() {
        if(this.state == LockState.state_unlock){
            cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
            this.node.getParent().getComponent('InnerLevelControl').onLevelSelected(this.m_Level,1);
        }else{
            this.node.getParent().getComponent('InnerLevelControl').uploadLockEvent(this.m_Level);
            //解锁
            this.themeView.getComponent('ThemeViewControl').unlockLevel = this.m_Level;
            this.themeView.getComponent('ThemeViewControl').theme = this.theme;
            let mainTs = cc.find('Canvas').getComponent('MainControl')
            mainTs.loadVideoAd(mainTs.unlockLevel)
        }
        this.node.getParent().getComponent('InnerLevelControl').hideLeadHand()
    }

    unlockEnterGame(){
        this.node.getParent().getComponent('InnerLevelControl').hideLeadHand()
        //重置解锁状态
        this.state = LockState.state_unlock;
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.node.getParent().getComponent('InnerLevelControl').onLevelSelected(this.m_Level,0);
    }

    removeAllSmallInnerBox() {
        this.parentNode.destroyAllChildren()
    }

    changeThemeColor() {
        let currentLevel = this.m_Level;
        // console.log('innerBox theme m_Level', currentLevel);
        let theme = Math.ceil(currentLevel / this.globalConst.chapterCount / this.globalConst.levelCount);
        //更换主题颜色
        this.levelUtil.loadThemeJson(function (json) {
            // console.log('innerBox theme json', json);
            // console.log('innerBox theme json body', json[theme].body);
            this.playColor = new cc.Color().fromHEX(json[theme].play);
            // this.playingNode.color = this.playColor;
            this.pathColor = json[theme].body;
            this.refreshNodeState();
        }.bind(this));
    }

    saveAnswer2LevelPath() {
        this.levelData = new LevelData();
        let theme = Math.ceil(this.m_Level / this.globalConst.chapterCount / this.globalConst.levelCount);
        let chapterTemp = this.m_Level - (theme - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
        let secondaryLevel = Math.ceil(chapterTemp / this.globalConst.levelCount);


        this.levelUtil.loadLevelJson(theme, secondaryLevel, function (json) {
            let levelIndex = this.m_Level - (theme - 1) * this.globalConst.chapterCount * this.globalConst.levelCount - (secondaryLevel - 1) * this.globalConst.levelCount - 1;
            let levelData = this.levelUtil.hex_to_levelData(this.levelUtil.getBinString(json[levelIndex]));
            // console.log("innerBox levelData", levelData);
            this.initRowColNum(levelData);
            this.caculateBoxWidth();
            let index = 0;
            for (var val in levelData) {
                this.levelData.currentPathIndex.push([]);
                this.levelData.currentPathIndex[index][0] = levelData[val][1];
                this.levelData.currentPathIndex[index][1] = levelData[val][0];
                index++;
            }
            // for (let index = 0; index < this.levelData.length; index++) {
            //     this.levelData.currentPathIndex.push([]);
            //     this.levelData.currentPathIndex[index][0] = levelData[index][1];
            //     this.levelData.currentPathIndex[index][1] = levelData[index][0];

            // }
            this.saveLevelData();
            this.refreshInnerBox();

        }.bind(this));
    }


    saveLevelData() {
        this.levelData.currentColor = this.pathColor;
        this.levelData.perfect = true;
        this.levelData.colNum = this.m_ColNum;
        this.levelData.rowNum = this.m_RowNum;
        this.levelData.itemWidth = this.m_BoxWidth;
        // this.storageUtil.saveData('levelData'+this.m_Level,this.levelData);
        cc.find('Canvas').getComponent('MainControl').saveLevelPathData(this.m_Level, this.levelData);
    }

    initRowColNum(levelData) {
        this.m_RowNum = 0;
        this.m_ColNum = 0;
        for (let i = 0; i < levelData.length; i++) {
            let data = levelData[i];
            if (data[1] > this.m_RowNum) {
                this.m_RowNum = data[1];
            }
            if (data[0] > this.m_ColNum) {
                this.m_ColNum = data[0];
            }
        }
        this.m_RowNum += 1;
        this.m_ColNum += 1;
    }

    caculateBoxWidth() {
        this.m_BoxWidth = (cc.winSize.width - (this.m_ColNum - 1)) / (this.m_ColNum + 1);
        // this.m_BoxHeight = (this.node.height - layout.paddingTop - layout.paddingBottom - (this.m_RowNum - 1)*layout.spacingY)/this.m_RowNum;
        this.m_BoxWidth = Math.min(this.m_BoxWidth, this.m_MaxBoxWidth);
    }

    refreshNodeState() {
        //本地数据被清掉后，将路径数据置为答案数据
        let levelData = this.storageUtil.getData('unLockLevel' + this.m_Level);
        let theme = Math.ceil(this.m_Level / this.globalConst.levelCount)
        let maxLevel = this.storageUtil.getData('unLockThemeMaxLevel' + theme);
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        if (mainTs.Membership) maxLevel = theme * this.globalConst.levelCount;
        let InnerLevelControl = this.node.getParent().getComponent('InnerLevelControl')
        let lv = this.m_Level % this.globalConst.levelCount
        if (levelData || mainTs.Membership) {
            this.state = LockState.state_unlock;
            if (levelData && levelData == 2) {
                this.playingNode.active = false;
                this.lockNode.active = false;
                this.saveAnswer2LevelPath();
            } else {
                this.playingNode.active = false;
                this.lockNode.active = true;
                this.adNode.active = false;
                if (lv == 0) {
                    if (InnerLevelControl.noPassNodeIdx > 19) {
                        InnerLevelControl.noPassNodeIdx = 19
                    }
                } else {
                    if (InnerLevelControl.noPassNodeIdx > (lv - 1)) {
                        InnerLevelControl.noPassNodeIdx = lv - 1
                    }
                }
                // if (this.storageUtil.getData('unLockLevel' + this.m_Level-1))
            }
        } else {
            this.playingNode.active = false;
            this.lockNode.active = true;
            this.adNode.active = true;
            this.state = LockState.state_AD;
        }
        if (lv == 0 && InnerLevelControl.noPassNodeIdx < 20) {
            InnerLevelControl.node.children[InnerLevelControl.noPassNodeIdx].getComponent("InnerBox").playingNode.active = true;
            InnerLevelControl.node.children[InnerLevelControl.noPassNodeIdx].getComponent("InnerBox").lockNode.active = false;
        }
    }
}
