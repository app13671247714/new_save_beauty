

const {ccclass, property} = cc._decorator;
import AwardControl from './AwardViewControl';


@ccclass
export default class NewClass extends cc.Component {

    awardControl:AwardControl= null;

    @property(cc.Node)
    pickAnim:cc.Node=null;

    @property(cc.Node)
    coinBg:cc.Node=null;

    @property(cc.Label)
    moneyLabel:cc.Label = null;
 
     // LIFE-CYCLE CALLBACKS:
 
    
        
     

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.on(cc.Node.EventType.TOUCH_START,this.onTouchEvent,this);
        // this.node.on(cc.Node.EventType.TOUCH_MOVE,this.onTouchEvent,this);
        // this.node.on(cc.Node.EventType.TOUCH_END,this.onTouchEvent,this);
        // this.node.on(cc.Node.EventType.TOUCH_CANCEL,this.onTouchEvent,this);
       
    }

    start () {

    }

    setAwardControl(awardControl){
        this.awardControl = awardControl;
        
    }
    // update (dt) {}

    onTouchEvent(){
        if(this.isClicked()){
            return;
        }
        let score = this.getRandom(1, 3);
        this.awardControl.addScore(score);
        this.moneyLabel.string = '+'+score;
        this.coinBg.opacity = 0;
        this.pickAnim.active = true;

        cc.find('Canvas').getComponent('MainControl').getVolumeControl().playCoinClickAudio();

        this.labelAnim();
        this.pickAnim.getComponent(dragonBones.ArmatureDisplay).playAnimation('newAnimation', 1);
        this.pickAnim.getComponent(dragonBones.ArmatureDisplay).addEventListener(dragonBones.EventObject.COMPLETE, this.animEventHandler, this);
        
    }

    animEventHandler(event){
        if (event.type === dragonBones.EventObject.COMPLETE) {            
             this.node.active = false;
             this.coinBg.opacity = 255;
             this.pickAnim.active = false;
        }
    }

    isClicked(){
        return this.coinBg.opacity==0;
    }

    labelAnim(){
        var _this  = this;
        _this.moneyLabel.node.opacity = 255;
        this.moneyLabel.node.y = 0
        var moveAction = cc.spawn(cc.moveTo(1,cc.v2(this.moneyLabel.node.x,20)),cc.fadeOut(1));
        this.moneyLabel.node.runAction(moveAction);
    }

    getRandom(min, max){
        return Math.floor(Math.random()*(max-min+1)+min);
    }
}
