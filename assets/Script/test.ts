const { ccclass, property } = cc._decorator;

@ccclass
export default class test extends cc.Component {

    @property(cc.SpriteFrame)
    BackGround: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    BackGround1: cc.SpriteFrame = null;

    @property
    speed: number = -100;


    node1: cc.Node;
    node2: cc.Node;
    winHeight: number;


    onLoad() {
        this.node1 = this.addnode(1);
        this.node2 = this.addnode();
        this.winHeight = 1880;
        this.node1.y = 0;
        if (this.speed > 0) {
            this.node2.y = -this.winHeight;
        }
        else {
            this.node2.y = this.winHeight;
        }

    }

    start() {

    }

    update(dt) {
        this.node2.y += this.speed * dt;
        this.node1.y += this.speed * dt;
        if (this.speed > 0) {
            if (this.node1.y >= this.winHeight) this.node1.y = this.node2.y - this.winHeight;
            if (this.node2.y >= this.winHeight) this.node2.y = this.node1.y - this.winHeight;
        }
        else {
            if (this.node1.y <= -this.winHeight) this.node1.y = this.node2.y + this.winHeight;
            if (this.node2.y <= -this.winHeight) this.node2.y = this.node1.y + this.winHeight;
        }

    }


    addnode(index?) {
        var node = new cc.Node();
        var sp = node.addComponent(cc.Sprite);
        sp.spriteFrame = index ? this.BackGround : this.BackGround1;
        node.parent = this.node;
        node.width = 750;

        node.height = 1880;
        return node;
    }

}