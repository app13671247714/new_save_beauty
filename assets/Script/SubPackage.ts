import { BaseConfig } from "./base/BaseConfig";


const { ccclass, property } = cc._decorator;

import WXCommon from './Common/WXCommon'
import StorageUtil, { LanguageKey } from './Common/StorageUtil'
import HttpRequest from "./Common/HttpRequest"
import GameBean from './Bean/UserData';
import User from './Bean/WXUserBean'
import { i18nMgr } from '../i18n/i18nMgr';
import { JsBridge } from './native/JsBridge';
import { CallNative } from './native/CallNative';
import HttpRequestNew from './Common/HttpRequestNew';
import { Utils } from "./base/BaseUtils";
import CallCocos from "./native/CallCocos";
import GameApp from "./GameApp";

const WX = window["wx"];
@ccclass
export default class NewClass extends cc.Component {
  storageUtill: StorageUtil = new StorageUtil();
  httpRequestNew: HttpRequestNew = new HttpRequestNew();

  onLoad() {
    // this.node.addComponent(GameApp);
  }

  private Membership = false
  start() {
    if (JsBridge.isMobile()) {
      cc["CallNative"] = CallNative
      if (JsBridge.isIosPlatform()) {
        cc.sys.localStorage.setItem("Membership", true)
      }
      this.Membership = cc.sys.localStorage.getItem('Membership')
      JsBridge.callWithCallback(JsBridge.DEVICE, {}, (jsonStr: any) => {
        if (jsonStr != null) {
          BaseConfig.DeviceInfo.deviceId = jsonStr.deviceId
          BaseConfig.DeviceInfo.mac = jsonStr.mac
          BaseConfig.DeviceInfo.version = jsonStr.version
          BaseConfig.DeviceInfo.uuid = jsonStr.uuid
          BaseConfig.DeviceInfo.user_id = jsonStr.uuid
          BaseConfig.DeviceInfo.channel = jsonStr.channel
          BaseConfig.DeviceInfo.app_code = jsonStr.app_code
          BaseConfig.DeviceInfo.os = JsBridge.isIosPlatform() ? 2 : 1
          BaseConfig.DeviceInfo.android_id = jsonStr.android_id
          BaseConfig.DeviceInfo.oaid = jsonStr.oaid
          BaseConfig.DeviceInfo.vendor = jsonStr.vendor
          BaseConfig.DeviceInfo.model = jsonStr.model
          BaseConfig.DeviceInfo.os_version = jsonStr.os_version
          BaseConfig.DeviceInfo.language = jsonStr.language
          let language = jsonStr.language
          if (language && language.length) {
            this.storageUtill.saveData(LanguageKey, language)
          }

          if (jsonStr.CloudInfo.nextcount) {
            BaseConfig.CloudInfo.nextcount = jsonStr.CloudInfo.nextcount
          }
          if (jsonStr.CloudInfo.click_back_count) {
            BaseConfig.CloudInfo.click_back_count = jsonStr.CloudInfo.click_back_count
          }
          if (jsonStr.CloudInfo.challengeCount) {
            BaseConfig.CloudInfo.challengeCount = jsonStr.CloudInfo.challengeCount;
          }

          let bodyParams = jsonStr.params
          if (!this.Membership) {
            if (!JsBridge.IsNull(BaseConfig.DeviceInfo.deviceId)) {
              this.httpRequestNew.order(bodyParams, (data) => {
                console.log("oder result:", data)
                if (!JsBridge.IsNull(data.Data) && data.Data.length > 0) {
                  cc.sys.localStorage.setItem("Membership", true)
                }
              })
            }
          } else {
            JsBridge.callWithCallback(JsBridge.MEMBERSHIP, {}, (jsonStr: object) => {
              if (jsonStr != null) {

              }
            });

          }
        }
        this.loadMain();
        if (JsBridge.isMobile()) {
          let adId = "969359704a9a0be0";
          JsBridge.callWithCallback(JsBridge.AD_SPLASH, {}, (jsonStr: any) => {
            if (jsonStr != null) {
              let resultType = jsonStr.resultType
              console.log(resultType, "======native ad callback=======")
            }
          })
        }
      });
    } else {
      this.loadMain();
    }
  }

  loadMain() {
    cc.director.loadScene("gameScence");
  }

  protected onDestroy(): void {
      console.log("释放loading页")
  }
}
