import { i18nMgr } from "../i18n/i18nMgr";
import LocalDataManager from "./Common/LocalDataManager";
import EventManager from "./Common/LocalEvents";
import { EventConfig } from "./Framework/Config/EventConfig";
import ChallengeModel, { } from "./Framework/Model/ChallengeModel";
import { GameUtils } from "./Framework/Utils/GameUtils";
import { RandomUtils } from "./Framework/Utils/RandomUtils";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameChallenge extends cc.Component {
    public static Instance:GameChallenge = null;
    private _originRank = [];//存储一开始创建好的50组等级数据
    private _originName = [];//存储昵称
    private _originRankInfo = [];//存储初始50数据模型
    private _rankInfo:ChallengeModel[] = [];

    private _myRank = 0;
    private _myName = "ME";
    public myRankIndex = -1;

    //初始50组数据
    public OriginRankDataKey:string = "OriginRankDataKey";
    //初始日语数组
    public OriginJaRankDataKey: string = "OriginJaRankDataKey";
    // 初始过关等级数据
    public OriginRankKey:string = "OriginRankKey";

    public MyRankIndexKey:string = "MyRankIndexKey";
    //第一次挑战时间
    public FirstChallenge:string = "FirstChallenge";
    //第一次挑战完成的时间记录
    private firstTime = 0;

    twentyFourHoursInMillis:number = 24 * 60 * 60 * 1000;

    needLevel:number = -1;//差多少关

    onLoad() {
        if (GameChallenge.Instance == null) {
            GameChallenge.Instance = this
        } else {
            this.destroy()
        }
        this.initData();
    }

    //清除本地缓存
    clearLocalData() {
        LocalDataManager.shareManager().RemoveItem(this.FirstChallenge)
        LocalDataManager.shareManager().RemoveItem(this.OriginRankDataKey);
        LocalDataManager.shareManager().RemoveItem(this.MyRankIndexKey);
        LocalDataManager.shareManager().RemoveItem(this.OriginRankKey);
    }

    ///初始化挑战赛逻辑
    initData() {
        //完成第一次挑战赛时间
        let first = LocalDataManager.shareManager().GetItem(this.FirstChallenge)
        if (first) {
            this.firstTime = parseInt(first);
            let now = Date.now()
            //24小时内
            if (this.firstTime + this.twentyFourHoursInMillis >= now) {
                this.scheduleOnce(() => {
                    EventManager.emit(EventConfig.CHALLENGE_START_COUNT_DOWN, this.firstTime);
                }, 1);
                this.readLocalData();
            } else {
                this.resetCountDown();
            }
        } else {
            this.createOriginInfo()
        }
    }

    resetCountDown() {
        //重置数据
        this.clearLocalData();
        this.firstTime = 0;
        this._myRank = 0;
        this.myRankIndex = -1;
        this.needLevel = -1;
        this.createOriginInfo();
        EventManager.emit(EventConfig.CHALLENGE_START_COUNT_DOWN, this.firstTime);
    }

    //生成初始随机数据
    createOriginInfo() {
        let jaNames = this.getJANames();
        let isJa = false;
        if(i18nMgr.getLanguage() == "ja"){
            isJa = true;
        }

        let level = RandomUtils.getRandomInt(60, 80);

        //生成第一名数据
        let model:ChallengeModel = new ChallengeModel();
        model.level = level;
        model.name = RandomUtils.RandomIntStr(8);

        //日语第一名数据
        let jaModel:ChallengeModel = new ChallengeModel();
        jaModel.level = level;
        jaModel.name = jaNames[0];

        //随机生成数据
        let arr = RandomUtils.getRandomNFromM(level - 1, 49);
        arr.sort((a, b) => b - a)
        arr[arr.length - 1] = 1;//重置最后一个为第一关
        this._originRank = GameUtils.clone(arr);
        this.needLevel = 1;
        //添加第一名
        this._originRank.unshift(level);
        //存储过关等级
        LocalDataManager.shareManager().SetItemArray(this.OriginRankKey, this._originRank);

        let infoArr = [];
        let jaInfoArr = [];
        infoArr.push(model);
        jaInfoArr.push(jaModel);
        for (let i = 0; i < arr.length; i++) {
            let element = arr[i];
            let name = RandomUtils.RandomIntStr(8);
            let itemModel:ChallengeModel = new ChallengeModel();
            itemModel.level = element;
            itemModel.name = name;
            infoArr.push(itemModel);

            let jaItemModel:ChallengeModel = new ChallengeModel();
            jaItemModel.level = element;
            jaItemModel.name = jaNames[i+1];
            jaInfoArr.push(jaItemModel);
        }

        let copyJaArr = GameUtils.clone(jaInfoArr);
        let copyArr = GameUtils.clone(infoArr);

        if(isJa){
            this._originRankInfo = copyJaArr
            this._rankInfo = jaInfoArr;
        }else{
            this._originRankInfo = copyArr
            this._rankInfo = infoArr;
        }
        //存储过关昵称模型数组
        LocalDataManager.shareManager().SetItemArray(this.OriginRankDataKey, copyArr);
        LocalDataManager.shareManager().SetItemArray(this.OriginJaRankDataKey,copyJaArr);
    }

    //读取本地数据
    readLocalData() {
        let isJa = false;
        if(i18nMgr.getLanguage() == "ja"){
            isJa = true;
        }
        let rankDataArr = LocalDataManager.shareManager().GetItemArray(isJa ? this.OriginJaRankDataKey : this.OriginRankDataKey);
        if (rankDataArr) {
            this._originRankInfo = rankDataArr;
            this._rankInfo = this.getOriginRankArray();
        }


        let rankArr = LocalDataManager.shareManager().GetItemArray(this.OriginRankKey);
        if (rankArr) {
            this._originRank = rankArr;
        }

        let rankIndex = LocalDataManager.shareManager().GetItem(this.MyRankIndexKey);
        if (rankIndex) {
            this.myRankIndex = parseInt(rankIndex);
            EventManager.emit(EventConfig.CHALLENGE_UPDATE_RANK, this.myRankIndex)
        }
    }

    getRankInfo() {
        return this._rankInfo;
    }

    getOriginRankArray() {
        let arr:ChallengeModel[] = []
        this._originRankInfo.forEach(data => {
            let model = new ChallengeModel()
            model.name = data._name;
            model.level = data._level;
            arr.push(model);
        });
        return arr;
    }

    updateLevel(level:number) {
        this._myRank = level;
        let index = -1;
        for (let i = 0; i < this._originRank.length; i++) {
            const element = this._originRank[i];
            if (level >= element) {
                index = i
                break;
            }
        }

        if (this.firstTime == 0) {
            this.markFirstTime();
        }

        let lastRank = this.myRankIndex;
        this.myRankIndex = index + 1;
        let arr = this.getOriginRankArray();

        //计算和上一名差值
        if (index > 0) {
            let nextItem = arr[index - 1];
            this.needLevel = nextItem.level - level;
        } else {//差值为0说明是第一名，差值为-1 说暂未上上榜
            this.needLevel = index;
        }

        let model:ChallengeModel = new ChallengeModel();
        model.name = this._myName;
        model.level = level;
        arr.splice(index, 0, model)
        this._rankInfo = arr;



        LocalDataManager.shareManager().SetItem(this.MyRankIndexKey, this.myRankIndex);
        //通知更新排名
        if(lastRank != this.myRankIndex){
            EventManager.emit(EventConfig.CHALLENGE_UPDATE_RANK, this.myRankIndex)
        }
    }

    //记录第一次挑战赛时间
    markFirstTime() {
        let time = Date.now()
        this.firstTime = time;
        //保存时间
        LocalDataManager.shareManager().SetItem(this.FirstChallenge, this.firstTime);
        //开启倒计时
        EventManager.emit(EventConfig.CHALLENGE_START_COUNT_DOWN, this.firstTime);
    }

    //每次进入挑战赛重置排名
    resetRank() {
        this.myRankIndex = -1;
        this._myRank = 0;
        LocalDataManager.shareManager().SetItem(this.MyRankIndexKey, this.myRankIndex);
        //通知更新排名
        EventManager.emit(EventConfig.CHALLENGE_UPDATE_RANK, this.myRankIndex)
    }


    private nameList = ["庭山美佳", "緑間并恵", "神森富恵", "昌谷芳子", "金高舞","藤長桜", "伊武知子","石阪佐恵子", "羅圣","矢附繁", "神宮寺富栄","裾分志保", "漆原淳子","新羽亮子", "楽喜久江","猿田絵里", "林原裕纪","手代木美砂", "松坂尚子","剣持江美子", "山宮千秋","融典子", "浮谷升","挟間亮", "内沼真树","雁田恵み", "胸組和夫","莇麻由美", "道重嘉美","多久和奈津江", "木下馨子","片川紫苑", "山柿映子","山本真弓", "達健一郎","春菜敏美", "牛円英子","小方冴子", "鳩沢新一","一ノ渡加那き", "寺崎直希","辰尾玉绪ち", "仁科和歌子","樋原友里恵", "代田纪江","勝谷奈绪", "佐牟田真由子","真栄田真季子", "大豊隆一","秋枝清见", "岩永三千代","浪谷川福子", "菜切亜佐美","内藤竜子", "用木柚子","新堀梢", "関川启之","須釜伸弥", "実本代志子","駒ケ岳沙希", "津名静香","矢袋干夫", "羽黒山雄二","西俣寿美枝", "荒畑摩耶","矢柳博", "納谷安纪子","横笛町", "酒口槙子","栗城薫理", "力山彻","香咲风见", "越中郁美","酒枝裕子", "月廼屋吉子", "瀬里慧", "永冨朝美", "大薮恵以子", "恵那桜音叶", "柳父诗音", "神内筱", "棟田健一", "田野口知美", "香内晃", "岡副哲子", "中保利恵", "徳弘鞠", "秋松航", "薮谷鸣海", "桃木観铃", "永田杏子", "尾子匡志", "栄多康仪", "箱島可怜", "高貝有理奈", "川井理穂", "山仲玲", "比賀尚纪", "門田恵里", "越沢美纪", "久野薫", "長谷田静奈", "猿渡久恵", "岩堂园子", "東谷有美", "布田美和", "横岡香子", "涌島蕾米", "古家朝美", "日下田千枝子", "新見里香", "新栄雅美", "大津寄桃子", "瓜生爱理", "旺伊久美", "芹田奈津子", "饗庭奈々子", "小笹媛子", "浜西昭彦"]

    getJANames(){
        let arr = RandomUtils.getRandomNFromM(119,50);
        let arr1 = [];
        arr.forEach(element => {
            let name = this.nameList[element]
            arr1.push(name);
        });
        return arr1;
    }
    
}
