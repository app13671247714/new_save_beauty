
const { ccclass, property } = cc._decorator;

@ccclass
export default class CatFace extends cc.Component {
    @property(cc.Node)
    m_EyeOpen: cc.Node = null;
    @property(cc.Node)
    m_EyeClose: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    openEye() {
        this.m_EyeOpen.active = true;
        this.m_EyeClose.active = false;
    }

    closeEye() {
        this.m_EyeOpen.active = false;
        this.m_EyeClose.active = true;
    }

    start() {

    }

    // update (dt) {}
}
