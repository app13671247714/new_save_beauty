import StorageUtil from "./StorageUtil";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;
const WX = window["wx"];


@ccclass('WXCommon')
export default class WXCommon {

    // httpRequest: HttpRequest = new HttpRequest();

    public REQUEST_HELPER  = 0;//求助
    public SHARE_ANSWER  = 1;//分享答案
    public SHARE_INVITE = 2;//邀请好友
    public PICK_ICON_AWARD = 3;//捡猫币分享
    public PICK_PRESSURE = 4;//宝箱分享
    public PICK_DOUBLE = 5;//双倍奖励分享
    public PICK_REVIVE = 6;//复活分享
    public PICK_UNLOCK = 7;//主题解锁分享
    public PICK_ANSWER = 8;//分享答案领取奖励
    public SHARE_NOTIFY = 9;//金币不足时分享提示
    public PICK_DAYAWARD = 10;//每日登陆奖励
    public PICK_ADD_COIN = 11;//点击添加金币奖励
    public SHARE_SETTLE = 13;//闯关结算界面分享
    public GOD_SHARE = 14;//分享超神领取奖励

    type: number = 0;

    storageUtil:StorageUtil = new StorageUtil();

    //是否从分享页面返回
    hasShared:boolean = false;
    constructor() {

    }

    shareToFrends(shareTitle, url, uid, desc) {
        // let query = "uid=" + uid + "&type=" + this.SHARE + "&time=" + this.getTime();
        // this.requestWx(shareTitle, url, query);

        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            let query = "";
            this.hasShared = true;
            let index =0;
            if (shareTitle == '') {
                index = this.storageUtil.getShareIndex();
                console.log('share index =', index);
                shareTitle = this.storageUtil.shareTitle[index];
                url = this.storageUtil.shareImageUrl[index];
            }
            this.requestWx(shareTitle, url, query, desc);
        }
    }

    requestHelper(shareTitle, url, level, user) {
        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            shareTitle = '我卡在第'+level+'关了，帮我过一下~~';
            let nickName = user.nickName;
            if (nickName == null || nickName == '' || typeof(nickName) == 'undefined') {
                nickName = '好友';
            }
            let query = "uid=" + user.uid + "&level=" + level + "&nickName=" + nickName + "&avatarUrl=" + user.avatarUrl + "&type=" + this.REQUEST_HELPER + "&time=" + this.getTime();
            console.log("requestHelper query = "  + query);
            this.requestWx(shareTitle, url, query,'闯关赛求助分享');
        }
       
    }

    shareAnswer(shareTitle, url, level, user){
        if (cc.sys.platform == cc.sys.WECHAT_GAME) {
            let nickName = user.nickName;
            if (nickName == null || nickName == '' || typeof(nickName) == 'undefined') {
                nickName = '好友';
            }
            shareTitle = nickName+'做不出来的难题被我解决了，答案在此，要的速来！'
            let query = "uid=" + user.uid + "&level=" + level + "&nickName=" + nickName + "&avatarUrl=" + user.avatarUrl + "&type=" + this.SHARE_ANSWER + "&time=" + this.getTime();
            console.log("requestHelper query = " + query);
            this.requestWx(shareTitle, url, query,'求助分享答案分享');
        }
        
    }

    // requestHelper(shareTitle, url, level, isAnswer) {
    //     if (cc.sys.platform == cc.sys.WECHAT_GAME) {
    //         let query = "level=" + level + '&isAnswer='+isAnswer;
    //         this.hasShared = true;
    //         console.log("requestHelper query = " + query);
    //         this.requestWx(shareTitle, url, query);
    //     }
        
    // }


    inviteFriends(shareTitle, url, uid) {
        let query = "uid=" + uid + "&type=" + this.SHARE_INVITE + "&time=" + this.getTime();
        console.log("inviteFriends query = " + query);
        this.requestWx(shareTitle, url, query,'邀请分享');
    }


    getTime() {
        var myDate = new Date();
        var currentTime = myDate.getTime();
        return currentTime;
    }


    requestWx(shareTitle, url, queryT, desc) {
        if(cc.sys.platform == cc.sys.WECHAT_GAME) {
            if (shareTitle == '') {
                shareTitle = "能一口气画完的，都是天才！！";
            }

            if(url == ''){
                url = "http://n.qikucdn.com/t/2i75bf43df4254fe83ta52.png";   
            }

            WX.aldShareAppMessage({
                title: shareTitle,
                imageUrl: url,
                ald_desc: desc, 
                // title: shareTitle,
                // imageUrl: url,
                query: queryT
            });
            var currentTime = this.getTime();
            console.log("currentTime = " + currentTime);
            cc.sys.localStorage.setItem("requestTime", currentTime);
        } 
    }

    share(shareTitle, url) {
        if(cc.sys.platform == cc.sys.WECHAT_GAME) {
            WX.shareAppMessage({
                // title: "测试",
                // imageUrl: "http://n.qikucdn.com/t/2g72bb282f79cc7ff25ux6.png",
                title: shareTitle,
                imageUrl: url,
            });
            var currentTime = this.getTime();
            console.log("currentTime = " + currentTime);
            cc.sys.localStorage.setItem("shareTime", currentTime);
        } 
    }


    saveDataToWx(level) {
        let kvDataList: Array<object> = new Array();
        kvDataList.push({
            key: "level",
            value: String(level)
        });
        kvDataList.push({
            key: "time",
            value: String(this.getTime())
        });

        WX.setUserCloudStorage({
            KVDataList: kvDataList,
            success: res => {
                console.log("ok success", res);
            },
            fail: res => {
                console.log("fail " + res);
            }
        });

    }

    saveLimitDataToWx(level) {
        let kvDataList: Array<object> = new Array();
        kvDataList.push({
            key: "limit_level",
            value: String(level)
        });
        kvDataList.push({
            key: "limit_time",
            value: String(this.getTime())
        });

        WX.setUserCloudStorage({
            KVDataList: kvDataList,
            success: res => {
                console.log("upload limit data ok success", res);
            },
            fail: res => {
                console.log("upload limit data fail " + res);
            }
        });

    }

    login(callback) {

        WX.login({
            success: function(res) {
              if (res.code) {
                console.log('登录成功！' + res.code)
                callback(res.code);
              } else {
                console.log('登录失败！' + res.errMsg)
              }
            }
        })
    }

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    // update (dt) {}

    // //注册监听是否分享
    // registerOnshow(callback){
    //     if(cc.sys.platform == cc.sys.WECHAT_GAME) {
    //         WX.onShow(function (res) { 
    //             if (this.hasShared) {
    //                 this.hasShared = false;
    //                 callback();
    //             }
    //         }.bind(this));
    //     }
        
    // }
    //获取阿拉丁分享打点描述
    getAldShareDesc(shareType){
        let desc = '';
        if (shareType == this.PICK_ICON_AWARD) {
         
            desc = '闯关赛奖励结算界面再次分享';
        }else if (shareType == this.PICK_PRESSURE) {
           
            desc = '闯关赛宝箱领取再次分享';
        }else if (shareType == this.PICK_DOUBLE) {
            
            desc = '闯关赛领取奖励再次分享';
        }else if (shareType == this.PICK_REVIVE) {
            
            desc = '挑战赛失败复活再次分享';
        }else if (shareType == this.PICK_UNLOCK) {
            
            desc = '解锁界面再次分享';
        }else if (shareType == this.SHARE_ANSWER) {
           
            desc = '求助答案再次分享';
        }else if (shareType == this.PICK_ANSWER) {
          
            desc = '求助分享答案奖励领取再次分享';
        }else if (shareType == this.SHARE_NOTIFY) {
       
            desc = '提示再次分享';
        }else if (shareType == this.PICK_DAYAWARD) {
           
            desc = '登陆领取再次分享';
        }else if (shareType == this.PICK_ADD_COIN) {
      
            desc = '闯关赛添加猫币再次分享';
        }

        return desc;
    }
   
}
