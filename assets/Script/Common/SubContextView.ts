import ccclass = cc._decorator.ccclass;
import Wx from "./Wx";
import property = cc._decorator.property;
import StorageUtil from "./StorageUtil";
import UserData from "../Bean/UserData";

/**
 * 排行榜数据过期方式.
 */
enum DateFilter {
    NONE = 0, WEEK = 1, MONTH = 2
}

const WX = window["wx"];

@ccclass
export class SubContextView extends cc.Component {
    private sprite: cc.Sprite = null;
    private readonly texture: cc.Texture2D = new cc.Texture2D();

    // 用于表示当前子域需要绘制哪个View.
    @property
    private viewName: string = "";

    // 约定要获取的key的名称.即 getCloudStorage(keyList:key)
    @property
    private key: string = "";

    // 用于表示子域数据根据什么key 进行排行.
    @property
    private sortKey: string = "";

    storageUtil:StorageUtil = new StorageUtil();

    // nickName:string='';
    // avatarUrl:string='';
    level:number=-1;

    //控制子域是否刷新
    private enableUpdate:boolean = false;

    @property({
        type: cc.Enum(DateFilter),
        serializable: true,
        displayName: "过滤方式",
        tooltip: "指定排行数据是否按周/月进行过滤,仅显示本周/月的数据."
    })
    private dateFilter: DateFilter = DateFilter.NONE;

    public onLoad() {
        this.sprite = this.getComponent(cc.Sprite);
        if (!this.sprite) {
            this.sprite = this.addComponent(cc.Sprite);
        }
    }

    public start() {
    }

    public onEnable() {
        console.log("SubContextView onEnable");
        this.enableUpdate = true;
        this.node.opacity = 0;
        if (!Wx.isWeChatEnv()) {
            return;
        }
        if (this.viewName.length <= 0 || this.key.length <= 0) {
            console.error("SubContextView 未设置子域viewName 和key ,无法显示子域");
            return;
        }
        console.log("SubContextView 显示子域:", this.viewName, this.key);
        const shareCanvas = Wx.getShareCanvas();
        shareCanvas.width = this.node.width;
        shareCanvas.height = this.node.height;

        cc.find('Canvas').getComponent('MainControl').SubViewRequestCount++;

        // Wx.sendMsgToContext("close_all", {view: this.viewName, bound: this.node.getContentSize()});
        // this.scheduleOnce(function () {
            Wx.sendMsgToContext("action_init_context", { 
                view: this.viewName,
                bound: this.node.getContentSize(), 
                updateCount: cc.find('Canvas').getComponent('MainControl').SubViewRequestCount,
                // nickName:this.nickName,
                // avatarUrl:this.avatarUrl,              
            });

            // this.updateView();
            // this.setPage(0);


            this.scheduleOnce(function () {

                this.node.opacity = 255;
            //     console.log("SubContextView opacity 255 " + this.viewName);
            // }.bind(this), 1);
            this.enableUpdate = false;
            this.schedule(function(){
                this.render();
            }.bind(this), 0.5, 5, 0.01);

           
        }.bind(this), 2);
        
        
    }

    public onDisable() {
    //    this.clearRender();
        // this.updateView();
    }

    public updateView() {
        Wx.sendMsgToContext(this.viewName, {
            sortKey: this.sortKey, 
            key: this.key,
            dateFilter: this.dateFilter, 
            updateCount:cc.find('Canvas').getComponent('MainControl').SubViewRequestCount,
            level:this.level
        });
    }

    public setPage(page: number) {
        Wx.sendMsgToContext("action_paging", {page: page, view: this.viewName});
    }

    // 向前翻页.
    public pageUp() {
        Wx.sendMsgToContext("action_paging", {offset: -1, view: this.viewName});
    }

    // 向后翻页.
    public pageDown() {
        Wx.sendMsgToContext("action_paging", {offset: 1, view: this.viewName});
    }

    public lateUpdate() {
        
        if (this.enableUpdate || this.viewName != 'Forthcoming') {
            
            this.render();
        }
       
    }

    private render() {
        if (!Wx.isWeChatEnv()) {
            return;
        }
        const shareCanvas = Wx.getShareCanvas();
        this.texture.initWithElement(shareCanvas);
        this.texture.handleLoadedTexture();


        // const ratio = WX.getSystemInfoSync().pixelRatio;//获取设备像素比
        // let openDataContext = WX.getOpenDataContext();
        // let sharedCanvas = openDataContext.canvas;
        // shareCanvas.width =  this.node.width * ratio; // 在主域将sharedCanvas宽高都按像素比放大
        // shareCanvas.height =  this.node.height * ratio;

        // console.log('context sharedCanvas.width = ',sharedCanvas.width);
        // console.log('context sharedCanvas.height = ',sharedCanvas.height);

        if (!this.sprite.spriteFrame) {
            this.sprite.spriteFrame = new cc.SpriteFrame(this.texture);
        } else {
            this.sprite.spriteFrame.setTexture(this.texture);
        }

        


        
    }

    private clearRender(){
        if (!Wx.isWeChatEnv()) {
            return;
        }
        if (this.viewName.length <= 0 || this.key.length <= 0) {
            console.error("SubContextView 未设置子域viewName 和key ,无法显示子域");
            return;
        }
        console.log("SubContextView  clear render 显示子域:", this.viewName, this.key);
        const shareCanvas = Wx.getShareCanvas();
        shareCanvas.width = this.node.width;
        shareCanvas.height = this.node.height;
        Wx.sendMsgToContext("close_all", {view: this.viewName, bound: this.node.getContentSize()});
    }

    setSelfData(level){
        // this.nickName = nickName;
        // this.avatarUrl = avatarUrl;
        this.level = level;
        this.updateView();
        console.log("SubContextView setSelfData level; = ",level);
    }
}
