

const {ccclass, property} = cc._decorator;
import HttpRequest from "./HttpRequest"
import GameBean from '../Bean/UserData'
import WXUserBean from '../Bean/WXUserBean';
import UserData from "../Bean/UserData";

export const LanguageKey = "userLang";

@ccclass
export default class StorageUtil{
    gameBean: UserData = new UserData();
    httpRequest: HttpRequest = new HttpRequest();
    wxUserBean: WXUserBean = new WXUserBean();

    saveData(key, value){
        cc.sys.localStorage.setItem(key, JSON.stringify(value));

        //如果是本地游戏数据则保存到服务器
        if (key === 'userData') {
            if(cc.sys.platform == cc.sys.WECHAT_GAME){
                let uid = this.getData('userData').uid;
                if (uid == null || uid == '' || typeof(uid) == 'undefined') {
                    return;
                }
                this.httpRequest.saveGameData(uid, value, function(data){
                    console.log('userdata saved', data);
                });
            }
            
        }
    }

    getData(key){
    
        let value = cc.sys.localStorage.getItem(key);
        if(value == null || value === '' || typeof(value)=='undefined'){
            return null;
        }
        return JSON.parse(value);
    }
    
    getGameBeanData(callBack){
        let value = cc.sys.localStorage.getItem('userData');
        if(value == null || value === '' || typeof(value)=='undefined'){
            this.httpRequest.getUserGameData(this.getData('wxUserInfo').uid, function(data){
                console.log('userdata get', data);
                if (value == null || typeof(value)=='undefined') {
                    
                    callBack(null);
                }else{
                    this.saveData('userData', data);
                    callBack(data);
                }
                
            }.bind(this));
            
        }else{
            let data = JSON.parse(value);
            if (typeof(data.gameLevel)=='undefined') {
                data.gameLevel = 1;
            }
            this.saveData('userData', data);
            callBack(JSON.parse(data))
        }
        
    }


    getNumberData(key:string,defaultValue:number):number{
    
        let value = cc.sys.localStorage.getItem(key);
        if(value == null || value === ''){
            return defaultValue;
        }
        return value;
    }
 
    saveNumberData(key:string,value:number){
    
        cc.sys.localStorage.setItem(key, value);
    }



    //全局调用
    //srcPos:开始位置,
    //dstPos:目标位置
    //radius:圆半径
    //goldCount:切分多少块,多少个金币
    //gold:需要增加多少金币
    //callBack:动画结束回调
    createGoldAnim(srcPos, dstPos, radius, goldCount,isCircle,targetNode,parentNode,poolMng) {
        var array = this.getPoint(radius, srcPos.x, srcPos.y, goldCount);
 
        var nodeArray = new Array();
        for (var i = 0; i < array.length; i++) {
            var gold = this.createGold(parentNode,poolMng);
            var randPos = cc.v2(array[i].x + this.random(0, 30), array[i].y + this.random(0, 30));
            gold.setPosition(srcPos);
            nodeArray.push({ gold, randPos });
        }
        let _this = this;
        nodeArray.sort(function (a, b) {
            var disa = _this.distance(a.randPos, dstPos);
            var disb = _this.distance(b.randPos, dstPos);
            return disa - disb;
        });
        var notPlay = false;
        // var targetGoldNode = this.m_Top.getGoldNode();
        for (var i = 0; i < nodeArray.length; i++) {
 
            var pos = nodeArray[i].randPos;
            var node = nodeArray[i].gold;
            node.zIndex = 1000;
            nodeArray[i].gold.id = i;
            let xTime1 = 0.5;
            let xTime2 = 0.03;
            if(!isCircle){
                xTime1 = 0;
                xTime2 = 0;
            }
            var seq = cc.sequence(
                cc.moveTo(xTime1, pos),
                cc.delayTime(i * xTime2),
                cc.moveTo(0.5, dstPos),
                cc.callFunc(function (node) {
                    // targetGoldNode.stopAllActions();
                    // _this.player.stopAllActions();
                    if (!notPlay) {
                        notPlay = true;
                        var seq = cc.sequence(
                            cc.scaleTo(0.1, 1.5, 1.5),
                            cc.scaleTo(0.1, 1.0, 1.0),
                            cc.callFunc(function () {
                                notPlay = false;
                            }),
                        );
                        targetNode.runAction(seq);
                    }
                    this.onGoldKilled(node,poolMng);
                }.bind(this))
            );
 
            node.runAction(seq);
        }
    }
 
    createGold (parentNode,poolMng) {
        let enemy = poolMng.spawnCoinAnim();
        enemy.parent = parentNode; // 将生成的敌人加入节点树
        return enemy;
    }
 
    onGoldKilled(gold,poolMng) {
        // enemy 应该是一个 cc.Node
        poolMng.despwanCoinAnim(gold);
        // this.goldPool.put(gold); // 和初始化时的方法一样，将节点放进对象池，这个方法会同时调用节点的 removeFromParent
    }
    /*
    * 求圆周上等分点的坐标
    * ox,oy为圆心坐标
    * r为半径
    * count为等分个数
    */
    getPoint(r, ox, oy, count) {
        var point = []; //结果
        var radians = (Math.PI / 180) * Math.round(360 / count), //弧度
            i = 0;
        for (; i < count; i++) {
            var x = ox + r * Math.sin(radians * i),
                y = oy + r * Math.cos(radians * i);
 
            point.unshift({ x: x, y: y }); //为保持数据顺时针
        }
        return point;
    }
 
    random(lower, upper) {
        return Math.floor(Math.random() * (upper - lower)) + lower;
    }
 
    distance(p1, p2) {
        var dx = Math.abs(p2.x - p1.x);
        var dy = Math.abs(p2.y - p1.y);
        return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    }

    isSpaceShapeScreen():boolean{
        let ratio = cc.winSize.height / cc.winSize.width;
        if(ratio >= 2){
            return true;
        }else{
            return false;
        }
    }

    getShareIndex(){
        return this.random(0, this.shareTitle.length);
    }
    shareTitle = ['还记得小时候一起玩的这个吗......',
                '还记得小时候一起玩的这个吗......',
                '这小学入学考试题出的太过分了！',
                '老同学，还记得当年的我们吗？',
                ];
    shareImageUrl = ['http://n.qikucdn.com/t/2re2762ef66d0a066ftsyu.png',
        'http://n.qikucdn.com/t/2r635f4d4d287c0b13tszo.png',
        'http://n.qikucdn.com/t/2re6c4c0da38dc3777tt2j.png',
        'http://n.qikucdn.com/t/2r38e181c1c29ee89att4c.png',
       ];     
        
        
    settleShareLabel=[
        '下一关',
        '领取猫币',
        '分享',
        '领取猫币',
        '免费抽大奖'];

    //结算界面弹框优先级
    //抽奖界面
    TURNTABLE = 0;
    //插屏广告
    INTERSTITIAL = 1;
    //插屏推荐
    MOREGAMEPAGE = 2;
    
        
}
