const APPNAME = "Ball2048_"
const THEME_GUIDE =  APPNAME + "THEME_GUIDE"//主题引导
const LEVEL_GUIDE = APPNAME +  "LEVEL_GUIDE"//关卡列表引导

const GMAE_BACK_TIMES = APPNAME + "GMAE_BACK_TIMES"

export default class LocalDataManager  {

    private static _dataManager = null;
    themeGuide:boolean = false;
    levelGuide:boolean = false;

    backTimes:number = 0;

    public static shareManager(): LocalDataManager{
        if(LocalDataManager._dataManager == null){
            LocalDataManager._dataManager  = new LocalDataManager()
        }
        return LocalDataManager._dataManager;
    }

    private constructor(){
        let  isThemeGuide = this.GetItem(THEME_GUIDE)
        if(isThemeGuide != null){
            this.themeGuide = true;
        }

       let  isLevelGuide = this.GetItem(LEVEL_GUIDE)
        if(isLevelGuide != null){
            this.levelGuide = parseInt(isLevelGuide) == 1 ? true : false;
        }

        let backTimes = this.GetItem(GMAE_BACK_TIMES)
        if(backTimes !=null){
            this.backTimes = parseInt(backTimes)
        }
    }

    isShowThemeHand(){
        if(this.themeGuide){
            return true
        }
        this.SetItem(THEME_GUIDE,1)
        this.themeGuide = true;
        return false;
    }

    isShowLevelHand(){
        if(this.levelGuide){
            return true;
        }
        this.SetItem(LEVEL_GUIDE,1)
        this.levelGuide = true;
        return false
    }

    saveBackTimes(){
        this.backTimes ++;
        this.SetItem(GMAE_BACK_TIMES,this.backTimes.toString());
    }

    resetBackTimes(){
        this.backTimes = 0;
        this.SetItem(GMAE_BACK_TIMES,this.backTimes.toString());
    }

    getBackTimes(){
        return this.backTimes;
    }


    SetItem(key, value) {
        cc.sys.localStorage.setItem(key, value);
    }
    GetItem(key) {
        return cc.sys.localStorage.getItem(key);
    }

    SetItemArray(key,arr){
        let json = JSON.stringify(arr);
        cc.sys.localStorage.setItem(key,json);
    }

    GetItemArray(key){
        let data = cc.sys.localStorage.getItem(key);
        let arr = JSON.parse(data);
        return arr;
    }

    RemoveItem(key){
        cc.sys.localStorage.removeItem(key);
    }


    mapToJson(map){
        let jsonString = JSON.stringify(Array.from(map.entries()));
        return jsonString;
    }
    jsonToMap(jsonString){
        // 解析 JSON 字符串
        let parsedJson = JSON.parse(jsonString);
        let map = new Map()
        parsedJson.forEach(item => {
            map.set(item[0],item[1]);
        });
        console.log(map)
        return map
    }
    

}
