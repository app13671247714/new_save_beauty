// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.startAnamation();
    }

    startAnamation(){
        this.node.stopAllActions();
        let currentPos = this.node.getPosition();
        let upMoveAction = cc.moveTo(0.3,currentPos.x,currentPos.y + 20);
        upMoveAction.easing(cc.easeSineOut());
        let sacaleUpAction = cc.scaleTo(0.3,0.9,1.1);
        sacaleUpAction.easing(cc.easeSineOut());
        let upAction = cc.spawn(upMoveAction,sacaleUpAction);
        let downMoveAction = cc.moveTo(0.3,currentPos.x,currentPos.y - 10);
        downMoveAction.easing(cc.easeSineIn());
        let sacaleDownAction = cc.scaleTo(0.3,1.1,0.9);
        sacaleDownAction.easing(cc.easeSineIn());
        let downAction = cc.spawn(downMoveAction,sacaleDownAction);
        let action1 = cc.moveTo(0.2,currentPos.x,currentPos.y);
        let action2 = cc.scaleTo(0.2,1.0,1.0);
        let action3 = cc.spawn(action1,action2);
        // let action1 = cc.scaleTo(0.2,1.2,1.0);
        // let action2 = cc.scaleTo(0.2,0.8,1.0);
        // let action3 = cc.scaleTo(0.2,1.0,1.0);
        let action4 = cc.sequence(upAction,downAction,action3);
        action4 = cc.repeat(action4,2);
        action4 = cc.sequence(action4,cc.delayTime(5));

        
        this.node.runAction(cc.repeatForever(action4));
    }

    start () {

    }

    // update (dt) {}
}
