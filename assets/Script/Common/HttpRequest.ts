

const { ccclass, property } = cc._decorator;

const HOST = "https://ad-api-test.weililiang1.com/pay/api/v1/order?userId=3898c904-5009-49de-a921-464ee3aa7858&deviceId=3898c904-5009-49de-a921-464ee3aa7858&platformProductId=onelinecat&payType=2&businessId=com.wll.onelinecat";
// const HOST = "http://10.143.186.172:8833/paint/app/api/"
const TOKEN = "q86OgVzZ8UkFHg8c86zH4iVlyv73sC6H8dD";

@ccclass('HttpRequest')
export default class HttpRequest {

    // /**
    //  * 获取10关游戏数据
    //  * @param level 
    //  * @param callback 
    //  */
    // getRemountConfigBylevels(level, callback) {
    //     let levels: Array<Number> = new Array();
    //     for(let i = 0; i <10; i++) {
    //         levels[i] = level + i;
    //     }
    //     let bodyParams = {listLevel:levels};
    //     this.request("nosign/getDataByLevel", bodyParams, function(response) {
    //         let array: Array<object> = new Array();
    //         let data = response.data;
    //         for(let i = 0; i < data.length; i++) {
    //             array[Number(data[i].lvl)] = data[i].conf;          
    //         }
    //         cc.sys.localStorage.setItem("level_conf", JSON.stringify(array));
    //         callback(JSON.parse(cc.sys.localStorage.getItem("level_conf")));
    //     }.bind(this))
    // }

    // /**
    //  * 获取某一关游戏数据
    //  * @param level 
    //  * @param callback 
    //  */
    // getConfigByLevel(level, callback) {
    //     console.log("getConfigByLevel " + level);

    //     let levels: Array<Number> = new Array();
    //     levels[0] = level;
    //     let bodyParams = {listLevel:levels};
    //     this.request("nosign/getDataByLevel", bodyParams, function(response) {
    //         let data = response.data;
    //         callback(data[0])
    //     }.bind(this))
    // }

    // /**
    //  * 成语示意
    //  * @param level 
    //  * @param callback 
    //  */
    // getRemountIdiomInfo(idioms, callback) {
    //     console.log("getRemountIdiomInfo");
    //     let bodyParams = {idiomNameList:idioms};
    //     this.request("nosign/getDescByIdiom", bodyParams,function(data) {
    //         callback(data.data)
    //     })
    // }

    /**
     * 保存用户信息
     * @param level 
     * @param callback 
     */

    saveUserInfo(uid, userInfo, callback) {

        let bodyParams = {
            "uid": uid, "nickName": userInfo.nickName, "gender": userInfo.gender,
            "citiy": userInfo.city, "province": userInfo.province, "country": userInfo.country, "avatarUrl": userInfo.avatarUrl
        };
        this.request("user/nosign/saveUserInfo", bodyParams, callback)
    }

    login(jsCode, callback) {
        let bodyParams = { "jsCode": jsCode };

        // let data = {
        //     "uid": '05da30146bb53b27f4c26ad5f36c0168',
        // };
        // callback(data);

        this.request("user/nosign/userRegist", bodyParams, function (data) {
            callback(data.data)
        })
    }

    /**
     * 保存游戏信息
     * @param level 
     * @param callback 
     */

    saveGameData(uid, gameBean, callback) {
        console.log("saveGameData")
        let bodyParams = { "uid": uid, "gold": gameBean.gold, "gameLevel": gameBean.gameLevel, "limitLevel": gameBean.limitLevel };
        this.request("user/nosign/uploadLevel", bodyParams, function (data) {
            let array: Array<object> = new Array();
            for (let i = 0; i < data.length; i++) {
                array[Number(data[i].lvl)] = data[i].conf;
            }
            callback(JSON.parse(JSON.stringify(array)));
        }.bind(this))
    }

    getUserGameData(uid, callback) {
        let bodyParams = { "uid": uid }
        this.request("user/nosign/getUserInfo", bodyParams, function (data) {
            callback(data.data)
        })
    }

    /**
     * 获取世界排名
     * @param level 
     * @param callback 
     */

    getUserLevel(uid, page, pageSize, callback) {
        let bodyParams = { "uid": uid, "page": page, "pageSize": pageSize };
        this.request("user/nosign/getUserGameLevel", bodyParams, callback)
    }

    /**
     * 获取挑战赛世界排名
     * @param level 
     * @param callback 
     */

    getUserLimitLevel(uid, page, pageSize, callback) {
        let bodyParams = { "uid": uid, "page": page, "pageSize": pageSize };
        this.request("user/nosign/getUserLimitLevel", bodyParams, callback)
    }



    /**
     * 上传分享信息
     * @param uid 
     * @param shareUid 
     * @param status 
     * @param callback 
     */

    uploadShareInfo(uid, shareUid, status, callback) {
        let bodyParams = { "uid": uid, "shareUid": shareUid, "rewardStatus": status };
        console.log("uploadShareInfo bodyParams = " + bodyParams);
        this.request("share/nosign/uploadShareInfo", bodyParams, callback)
    }

    /**
     * 用户领取奖励状态更新
     * @param uid 
     * @param shareUid 
     * @param status 
     * @param callback 
     */

    uploadReward(uid, shareUid, status, callback) {
        let bodyParams = { "uid": uid, "shareUid": shareUid, "rewardStatus": 1 };
        console.log("uploadReward bodyParams = " + bodyParams);
        this.request("share/nosign/getRewards", bodyParams, callback)
    }

    /**
     * 查询我的分享结果
     * @param uid 
     * @param shareUid 
     * @param status 
     * @param callback 
     */

    queryShareUser(uid, page, pageSize, callback) {
        let bodyParams = { "uid": uid, "page": page, "pageSize": pageSize };
        this.request("share/nosign/queryShareUser", bodyParams, callback)
    }




    request(method, bodyParams, callback) {
        // bodyParams.token = TOKEN;
        new Promise((resolve, reject) => {
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = function () {
                console.log('xhr.readyState=' + xhr.readyState + '  xhr.status=' + xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    let response = xhr.responseText;
                    console.log("request response:", response);
                    let data = JSON.parse(response);
                    callback(data);
                }
            };
            var url_temp = HOST;
            xhr.open("GET", url_temp, true);
            xhr.timeout = 5000;// 5 seconds for timeout
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send();
        })
    }
    requestGet(method, callback) {
        // bodyParams.token = TOKEN;
        new Promise((resolve, reject) => {
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = function () {
                console.log('xhr.readyState=' + xhr.readyState + '  xhr.status=' + xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    let response = xhr.responseText;
                    console.log("request response:", response);
                    let data = JSON.parse(response);
                    callback(data);
                }
            };
            var url_temp = HOST + method;
            console.log("url_temp", url_temp)
            xhr.open("GET", url_temp, true);
            xhr.timeout = 5000;
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send();
        })
    }

    loadImage(avatarUrl: string, avator: cc.Sprite) {
        cc.loader.load({
            url: avatarUrl, type: 'png'
        }, (err, texture) => {
            if (err) console.error(err);
            avator.spriteFrame = new cc.SpriteFrame(texture);
        });
    }

    // loadImage(avatarUrl:string,avator:cc.Sprite) {

    //     cc.loader.load({
    //         url: avatarUrl, type: 'png'
    //     }, (err, texture) => {
    //         if (err) console.error(err);
    //         avator.spriteFrame = new cc.SpriteFrame(texture);
    //     }); 
    // }
}
