

export default class EventManager extends cc.EventTarget {
    public static _instance: EventManager = null;
    public static Instance(){
        if(EventManager._instance == null){
            EventManager._instance = new EventManager()
        }
        return EventManager._instance;
    }

    // private eventTarget: cc.EventTarget = new cc.EventTarget();

    public static on(event: string, callback: Function, target: any): void {
        // this.init();
        this.Instance().on(event, callback, target);
    }

    public static once(event: string, callback: any, target?: any): void {
        this.Instance().once(event, callback, target);
    }

    public static off(event: string, callback: Function, target: any): void {
        this.Instance().off(event, callback, target);
    }

    public static targetOff(target: any): void {
        this.Instance().targetOff(target);
    }

    public static emit(event: string, arg1?: any, arg2?: any, arg3?: any, arg4?: any, arg5?: any): void{
        this.Instance().emit(event,arg1,arg2,arg3,arg4,arg5);
    }

}