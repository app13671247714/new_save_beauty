import { BaseConfig } from "../base/BaseConfig";
import { JsBridge } from "../native/JsBridge";
import HttpRequestNew from "./HttpRequestNew";
import Singleton from "./Singleton";


export class ReportConfig {
    static ReportMap = {
        event: "",
        event_msg: "",
        //json字段
        theme: -1,
        chapter: -1,
        level: -1,
        state: -1,
        passtime: -1,
        page: "",//广告扩展页
        rank: -1,
    }
}

export default class ReportMgr extends Singleton {

    /**
    evt_show_home",""           首页曝光
    evt_show_theme              首页主题曝光（主题标记，1,2,3.。。）
    evt_show_album              图集页面，相册曝光（主题_章，1_1)
    evt_show_album_pic          相册内，图片曝光（主题_章_节，1_2_1）
    evt_show_Notify             游戏中，提示弹窗曝光“获得2步提示，助你成功通关”（主题_章_节，1_2_1）
    evt_show_passLeve_alert     通关弹窗曝光（主题_章_节，1_2_1）
    evt_alert_unlock_theme      章目解锁提示弹窗曝光（主题_章，1_2）
    evt_alert_unlock_album      图集内，相册解锁提示弹窗曝光（主题_章，1_2）
    evt_challenge_fail          挑战赛通过失败提示弹窗曝光
    click_theme_item            主题下，章目点击（主题_章，1_2；已解锁/广告解锁）
    click_alert_unlock_theme    章目解锁提示弹窗点击按钮（免费解锁/关闭；主题_章，1_2）
    click_innerBox 关卡节点的点击（主题_章_节，1_2_1_1；已解锁/广告解锁）
    click_game_noti             右下角提示按钮点击（主题_章_节，1_2_1）
    click_noti_alert_tips/click_noti_alert_close 游戏中，提示弹窗点击，提示弹窗指的是“获得2步提示，助你成功通关”（提示/关闭，主题_章_节，1_2_1_1 提示）
    click_next_level/click_save_pic 通关弹窗按钮点击（下一关/保存美女图片，主题_章_节，1_2_1）
    click_album                 图集按钮点击
    click_album_box             图集内，相册点击（主题_章，1_1；已解锁/广告解锁)
    click_alert_unlock_album    图集内，相册解锁提示弹窗点击（免费解锁/关闭）
    click_album_page_box        相册内，图片点击（主题_章_节，1_2_1）
    click_album_page_unlock     相册内，点击图片解锁按钮
    click_save_album_pic       相册内，点击图片后显示弹窗，点击免费下载（主题_章_节，1_2_1）
    click_challenge             挑战赛按钮点击
    evt_challenge_pass_level    挑战赛通关成功（每通过一关上报一次）
    click_retry                 挑战赛通过失败提示弹窗，重玩按钮点击
    click_set                   设置按钮点击
    evt_select_language         语言选择按钮点击、选择的语种
    click_audio_switch          声音开关点击（开启/关闭）
     */

   
    uploadConfig(info){
        console.log("打点上报:");
        console.log(info);
        let params = {}
        if(info.theme >= 0){
            params["theme"] = info.theme;
        }
        if(info.chapter >= 0){
            params["chapter"] = info.chapter;
        }
        if(info.level >= 0){
            params["level"] = info.level;
        }
        if(info.state >= 0){
            params["state"] = info.state;
        }
        if(info.passtime >= 0){
            params["passtime"] = info.passtime;
        }
        if(info.page.length){
            params["page"] = info.page;
        }
        if(info.rank >= 0){
            params["rank"] = info.rank;
        }
        let jsonParams = JSON.stringify(params);
        console.log("上报json:"+jsonParams)
        if (JsBridge.isMobile()) {
            JsBridge.callWithCallback(JsBridge.REPORT_EVENT, { event:info.event, event_msg: info.event_msg,event_json:jsonParams}, (jsonStr: object) => {

            })
        }

        ReportConfig.ReportMap.theme = -1;
        ReportConfig.ReportMap.chapter = -1;
        ReportConfig.ReportMap.level = -1;
        ReportConfig.ReportMap.state = -1;
        ReportConfig.ReportMap.passtime = -1;
        ReportConfig.ReportMap.rank = -1;
        ReportConfig.ReportMap.event = "";
        ReportConfig.ReportMap.event_msg = "";
        ReportConfig.ReportMap.page = "";
    }


    /**
     * 
     * @param event 事件名
     * @param event_msg 事件状态集合（已停用）
     * @param theme  主题
     * @param chapter  章
     * @param level 关卡
     * @param state 事件状态 （开启/关闭  解锁/未解锁） 
     */
    uploadEvent(event, event_msg, theme?, chapter?, level?, state?,passtime?) {
        console.log("打点上报:" + event + " - " + event_msg);
        let params = {}
        if(theme){
            params["theme"] = theme;
        }
        if(chapter){
            params["chapter"] = chapter;
        }
        if(level){
            params["level"] = level;
        }
        if(state || state === 0){
            params["state"] = state;
        }
        if(passtime){
            params["passtime"] = passtime;
        }
        let jsonParams = JSON.stringify(params);
        console.log("上报json:"+jsonParams)
        if (JsBridge.isMobile()) {
            JsBridge.callWithCallback(JsBridge.REPORT_EVENT, { event: event, event_msg: event_msg,event_json:jsonParams}, (jsonStr: object) => {

            })
        }
    }
}
