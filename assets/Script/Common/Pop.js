var ME = require('ModuleEventEnum');
cc.Class({
  extends: cc.Component,

  properties: {

  },

  start() {

  },
  modeClick(event) {
    if (this.modeClose) {
      this.removeTop()
    }
  },
  releasePop(one) {
    if (this.IN_POOL_POPS[one.name]) {
      if (!window.pool.has(one.name)) {
        window.pool.create(one.name, 1, one)
      } else {

        window.pool.put(one.name, one)
      }
    } else {
      one.removeFromParent()
      one.destroy()
    }
  },
  removeAll(excpectUI) {
    // this.loadingUrls = {}
    for (var k in this.loadingUrls) {
      if (k == excpectUI) {
        continue
      }
      delete this.loadingUrls[k]
    }

    for (var i = 0; i < this.popList.length; i++) {
      if (this.popList[i].name == excpectUI) {
        continue
      }
      if (this.popList[i].getComponent(this.popList[i].name)['doClose'] != null) {
        this.popList[i].getComponent(this.popList[i].name).doClose();
      }
      this.releasePop(this.popList[i])

      // this.popList[i].removeFromParent()

      this.popList.splice(i, 1)
      i--
    }

    // this.popList = []
    if (this.popList.length <= 0 && this.mode) {
      this.mode.removeFromParent()
      this.mode = null
      this.modeShowed = false

    }
    cc.systemEvent.emit(ME.POP_REMOVED);
  },
  //获取弹框绑定的代码逻辑
  getPopComponent(popName) {
    if (this.popList == null || this.popList.length <= 0) {
      return null
    }

    var nameOne = null
    for (var i = 0; i < this.popList.length; i++) {
      if (this.popList[i].name == popName) {
        nameOne = this.popList[i]
        break
      }
    }

    if (nameOne) {
      return nameOne.getComponent(popName + "JS")
    }

    return null
  },
  getPopByName(popName) {
    if (this.popList == null || this.popList.length <= 0) {
      return null
    }

    var nameOne = null
    for (var i = 0; i < this.popList.length; i++) {
      if (this.popList[i].name == popName) {
        nameOne = this.popList[i]
        break
      }
    }

    if (nameOne) {
      return nameOne
    }
    else {
      return null
    }
  },
  removeByName(popName, doAction) {

    if (this.popList == null || this.popList.length <= 0) {
      return
    }

    var nameOne = null
    for (var i = 0; i < this.popList.length; i++) {
      if (this.popList[i].name == popName) {
        nameOne = this.popList[i]
        break
      }
    }
    if (!nameOne) {
      return
    }
    this._clearOne = nameOne

    doAction != undefined && (this.doAction = doAction)
    
    if (this.doAction) {
      var delayTime = 0;
      if (this._clearOne.getComponent(this._clearOne.name).closeActTime != null) {
        delayTime = this._clearOne.getComponent(this._clearOne.name).closeActTime
      }

      if (this._clearOne.getComponent(this._clearOne.name)["doClose"] != null) {
        this._clearOne.getComponent(this._clearOne.name).doClose();
      }

      if (nameOne.fadeType == 'top') {
        nameOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.moveBy(0.3, cc.v2(0, 2000)), cc.callFunc(this.clearPop, this)))
        return
      }
      else if (nameOne.fadeType == 'bottom') {
        nameOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.moveBy(0.3, cc.v2(0, -2000)), cc.callFunc(this.clearPop, this)))
      } else if (nameOne.fadeType == 'left') {
        let bgWidth = nameOne.getChildByName('ht_boxbg').width;
        let closeWidth = nameOne.getChildByName('ht_close').width;
        // pop.x = -(bgWidth/2 + closeWidth);
        
        let action = cc.moveTo(0.2, cc.v2(-(bgWidth/2 + closeWidth), 0));
        // action.easing(cc.easeElasticOut(1));
        let seq = cc.sequence(action, cc.callFunc(this.clearPop, this));
        nameOne.runAction(seq);
  
        return
      }
      nameOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.scaleTo(0.1, 1.1), cc.scaleTo(0.1, 0), cc.callFunc(this.clearPop, this)))
    } else {
      this.clearPop()
      // this._clearOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.callFunc(this.clearPop, this)));
    }
  },
  clearPop() {
    for (var i = 0; i < this.popList.length; i++) {
      if (this.popList[i] == this._clearOne) {
        this.popList.splice(i, 1)
        break
      }
    }


    if (this.modeShowed && this.mode && this.mode.parent) {
      this.mode.zIndex = (window.facadeMgr.PopOrder + this.popList.length - 1)
    }

    this.releasePop(this._clearOne)
    cc.systemEvent.emit(ME.POP_REMOVED, this._clearOne.name);

    if (this.popList.length <= 0 && this.mode) {
      this.mode.removeFromParent()
      this.mode = null
      this.modeShowed = false

      if (this._clearOne.name == "HoverUI" && facadeMgr.getComponent("GameModel").isNeedEnergy) {
        this.addPopByName("VideoTipUI", null, true);
      }

    }

  },
  removeTop(doAction) {
    // window.audioMgr.getComponent('SoundManager').playEffect('touch2', false, 100)
   
    if (this.popList == null || this.popList.length <= 0) {
      return
    }
    var topOne = this.popList[this.popList.length - 1]
    this._clearOne = topOne

    doAction != undefined && (this.doAction = doAction)

    if (this.doAction) {
      var delayTime = 0;
      if (this._clearOne.getComponent(this._clearOne.name) &&
        this._clearOne.getComponent(this._clearOne.name).closeActTime != null) {
        delayTime = this._clearOne.getComponent(this._clearOne.name).closeActTime;
      }

      if (this._clearOne.getComponent(this._clearOne.name) &&
        this._clearOne.getComponent(this._clearOne.name)["doClose"] != null) {
        this._clearOne.getComponent(this._clearOne.name).doClose();
      }

      if (topOne.fadeType == 'top') {
        topOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.moveBy(0.3, cc.v2(0, 2000)), cc.callFunc(this.clearPop, this)))
        return
      }
      else if (topOne.fadeType == 'bottom') {
        topOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.moveBy(0.3, cc.v2(0, -2000)), cc.callFunc(this.clearPop, this)))
      }else if (topOne.fadeType == 'left') {
        let bgWidth = topOne.getChildByName('ht_boxbg').width;
        let closeWidth = topOne.getChildByName('ht_close').width;
        // pop.x = -(bgWidth/2 + closeWidth);
        
        let action = cc.moveTo(0.5, cc.v2(-(bgWidth/2 + closeWidth), 0));
        // action.easing(cc.easeElasticOut(1));
        let seq = cc.sequence(action, cc.callFunc(this.clearPop, this));
        topOne.runAction(seq);
  
        return
      }
      topOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.scaleTo(0.1, 1.1), cc.scaleTo(0.1, 0), cc.callFunc(this.clearPop, this)))
    } else {
      this.clearPop()
      // topOne.runAction(cc.sequence(cc.delayTime(delayTime), cc.callFunc(this.clearPop, this)))
    }
  },
  showMode(modeClose) {
    if (this.modeShowed && this.mode && this.mode.parent) {
      this.mode.opacity = 204
      this.mode.zIndex = (window.facadeMgr.PopOrder + this.popList.length + 1)

      console.log("addMode:", this.mode.zIndex)
      return
    }

    this.mode = new cc.Node()

    this.mode.addComponent(cc.Sprite)
    this.mode.getComponent(cc.Sprite).spriteFrame = cc.loader.getRes('modeBg', cc.SpriteFrame)
    this.mode.color = "#383838";
    this.mode.scale = 100
    this.mode.opacity = 220
    //this.mode.runAction(cc.fadeIn(0.2))

    this.modeShowed = true

    this.mode.runAction(cc.sequence(cc.delayTime(0.21), cc.callFunc(function () {
      this.mode.addComponent(cc.Button)
      this.mode.on('click', this.modeClick, this)
      this.modeClose = modeClose
    }.bind(this))))
    console.log("addMode:", facadeMgr.PopOrder)
    cc.director.getScene().getChildByName('Canvas').addChild(this.mode, window.facadeMgr.PopOrder)
  },
  popFadeIn(pop) {

    var node = pop.getComponent(pop.name);
    if (node && node['viewDidAppear'] != null) {
      node.viewDidAppear();
    }

  },
  fadeInPop(pop, fadeType, pos) {
    pop.fadeType = fadeType
    if (pos == null) {
      pos = cc.v2(0, 0)
    }

    if (fadeType == 'top') {
      pop.y = 2000
      var movetop = cc.moveTo(0.3, pos)
      movetop.easing(cc.easeElasticOut(2))
      pop.runAction(movetop)
      return
    }
    else if (fadeType == 'bottom') {
      pop.y = -2000
      var movebottom = cc.moveTo(0.3, pos)
      movebottom.easing(cc.easeElasticOut(2))
      pop.runAction(movebottom)
      return
    } else if (fadeType == 'left') {
      let bgWidth = pop.getChildByName('ht_boxbg').width;
      let closeWidth = pop.getChildByName('ht_close').width;
      pop.x = -(bgWidth/2 + closeWidth);
    
      let action = cc.moveTo(0.5, cc.v2(bgWidth/2 - cc.winSize.width/2, 0));
      action.easing(cc.easeElasticOut(1));
      let seq = cc.sequence(action, cc.callFunc(this.popFadeIn, this));
      pop.runAction(seq);

      return
    }


    pop.scale = 0.1
    pop.runAction(cc.sequence(cc.scaleTo(0.1, 1.1), cc.scaleTo(0.1, 1), cc.callFunc(this.popFadeIn, this)))
    
    
  },
  addPop(pop, mode, modeClose, fadeType, pos) {
    if (!cc.director.getScene() || !cc.director.getScene().getChildByName('Canvas')) {
      return
    }

    if (mode) {
      this.showMode(modeClose)
    }

    cc.director.getScene().getChildByName('Canvas').addChild(pop, window.facadeMgr.PopOrder + this.popList.length + 1)
    // pop.x = cc.director.getScene().getChildByName('Canvas').getChildByName('touchEvent').x
    // pop.y = cc.director.getScene().getChildByName('Canvas').getChildByName('touchEvent').y
    pop.x = 0
    pop.y = 0
    this.popList.push(pop)

    console.log("addPop:", pop.name, pop.zIndex)

    if (this.doAction) {
      this.fadeInPop(pop, fadeType, pos)
    } else {
      var node = pop.getComponent(pop.name);
      if (node && node['viewDidAppear'] != null) {
        node.viewDidAppear();
      }

    }

    cc.systemEvent.emit(ME.POP_ADDED, pop.name);
  },
  createNew(res, url, data) {
    console.log('createNew:', url)
    var newPop = null
    if (!this.IN_POOL_POPS[url]) {  //!window.pool.has(url) ||
      newPop = cc.instantiate(res)
    } else {
      newPop = cc.instantiate(res)
      // newPop = window.pool.get(url)
    }

    if (newPop.getComponent(url) && data != null) {
      newPop.getComponent(url).initData(data)
    }

    return newPop
  },
  hideLoading() {
    if (this.loading != null) {
      this.loading.removeFromParent()
      this.loading = null
    }

    if (this.loadingAni != null) {
      this.loadingAni.removeFromParent()
      this.loadingAni = null
    }
  },
  showLoading() {
    if (this.loading == null) {
      this.loading = new cc.Node()
      this.loading.addComponent(cc.Sprite)
      this.loading.getComponent(cc.Sprite).spriteFrame = cc.loader.getRes('modeBg', cc.SpriteFrame)
      this.loading.scale = 100
      this.loading.opacity = 40
      this.loading.addComponent(cc.Button)
      cc.director.getScene().getChildByName('Canvas').addChild(this.loading, window.facadeMgr.PopOrder - 1)
    } else {
      this.loading.active = true
    }
  },
  addPopByName(url, data, mode, modeClose, doAction, fadeType, pos) {
    console.log('addPopByName:', url)
    if (!this._tempDatas) {
      this._tempDatas = {}
    }
    this._tempDatas[url] = null
    if (!this.loadingUrls) {
      this.loadingUrls = {}
    }
    // window.audioMgr.getComponent('SoundManager').playEffect('touch1', false, 100)
    for (var i = 0; i < this.popList.length; i++) {
      if (this.popList[i].getName() == url) {
        this._clearOne = this.popList[i]
        if (this._clearOne.getComponent(this._clearOne.name)["doClose"] != null) {
          this._clearOne.getComponent(this._clearOne.name).doClose();
        }
        this.clearPop()
        break
      }
    }

    if (modeClose == null) {
      modeClose = true
    }

    this.doAction = doAction == false ? false : true

    if (this.loadedKeys[url] || cc.loader.getRes('pfb/' + url)) {
      var res = cc.loader.getRes('pfb/' + url)
      var newone = this.createNew(res, url, data)
      newone.setName(url)
      this.addPop(newone, mode, modeClose, fadeType, pos)
    }
    else {
      this._tempDatas[url] = data
      this.showLoading()
      this.loadingUrls[url] = true
      cc.loader.loadRes('pfb/' + url, function (error, res) {
        this.hideLoading()

        if (!this.loadingUrls[url]) {
          return
        }
        delete this.loadingUrls[url]

        var newone = this.createNew(res, url, this._tempDatas[url])
        newone.setName(url)
        this.loadedKeys[url] = true
        this.addPop(newone, mode, modeClose, fadeType, pos)
      }.bind(this))
    }
  },
  addAlert(detailString, enterCallBack, needCancel, animSpeed) {
    var data = {
      detailString: detailString,
      enterCallBack: enterCallBack,
      needCancel: needCancel,
      animSpeed: animSpeed
    }

    this.addPopByName('Alert', data, true, true)
  },
  reset() {
    this.removeAll()
    this.popList = []
    this.modeShowed = false
  },
  onLoad() {


 

    this.loadedKeys = {}
    this.popList = []
    this.modeShowed = false

    this.doAction = true
    cc.loader.loadRes('modeBg', cc.SpriteFrame)
    // cc.loader.loadRes('modeBg2', cc.SpriteFrame)

    this.IN_POOL_POPS = { 'DailyReward': 1, 'DatingEdit': 1, 'LoveSeaLucky': 1, 'VipReward': 1, 'WXRank': 1, 'InfoEdit': 1, 'GoodInfo': 1, 'Alert': 1, 'Claim': 1 }
  },

})
