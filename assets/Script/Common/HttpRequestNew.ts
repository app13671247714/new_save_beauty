import url = cc.url;
import {JsBridge} from "../native/JsBridge";
const CryptoJS = require('crypto-js');

const {ccclass, property} = cc._decorator;

const HOST_TEST = "https://ad-api-test.weililiang1.com/pay/";
const HOST = "https://pay-api.weililiang1.com/";

const EVENT_TEST = "https://ad-api-test.weililiang1.com/";
const EVENT = "https://global-postback.weililiang1.com/";

const REPORT = "https://api.usefulnls.com/api/stat/event/"

@ccclass('HttpRequestNew')
export default class HttpRequestNew {

    public key = CryptoJS.enc.Utf8.parse("BD3A6DE43FF080A512A00AE7B036134092A0600D1FAF0BF641183F95D4EEC016")
    public iv = CryptoJS.enc.Utf8.parse("8DA8F20226552F19F907767B3529C016")

    order(bodyParams, callback) {
        console.log("order")
        let urlGet = "api/v1/order?userId="+bodyParams.userId
        if (!JsBridge.IsNull(bodyParams.deviceId)){
            urlGet += "&deviceId="+bodyParams.deviceId
        }
        if (!JsBridge.IsNull(bodyParams.platformProductId)){
            urlGet += "&platformProductId="+bodyParams.platformProductId
        }
        if (!JsBridge.IsNull(bodyParams.payType)){
            urlGet += "&payType="+bodyParams.payType
        }
        if (!JsBridge.IsNull(bodyParams.businessId)){
            urlGet += "&businessId="+bodyParams.businessId
        }

        this.requestGet(urlGet, function(data) {
                callback(data);
        }.bind(this))
    }

    callback(type, bodyParams, callback) {
        console.log("callback")
        this.requestPost( "api/v1/order/callback/"+type, bodyParams, function(data) {
            callback(data);
        }.bind(this))
    }

    requestGet(method, callback) {
        // bodyParams.token = TOKEN;
        new Promise((resolve,reject)=>{
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = ()=> {
                console.log('xhr.readyState='+xhr.readyState+'  xhr.status='+xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    let response = xhr.responseText;
                    console.log("request response:", response);
                    let data = JSON.parse(response);
                    callback(data);
                }
            };
            xhr.ontimeout = ()=> {
                callback({data: null})
            }
            xhr.onabort = ()=> {
                callback({data: null})
            }
            xhr.onerror = ()=> {
                callback({data: null})
            }
            var url_temp = HOST + method;
            console.log("url_temp", url_temp)
            xhr.timeout = 10000;// 5 seconds for timeout
            xhr.open("GET", url_temp, true);
            xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            xhr.send();
        })
    }

    retryCount = 0;
    requestPost(method, bodyParams, callback) {
        // bodyParams.token = TOKEN;
        new Promise((resolve,reject)=>{
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = ()=> {
                console.log('xhr.readyState='+xhr.readyState+'  xhr.status='+xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    let response = xhr.responseText;
                    console.log("request response:", response);
                    let data = JSON.parse(response);
                    callback(data);
                }
            };
            xhr.ontimeout = (e)=> {
                console.log("timeout")
            }
            var url_temp = HOST + method;
            console.log("url_temp", url_temp)
            console.log("bodyParams", JSON.stringify(bodyParams))
            xhr.timeout = 10000;// 5 seconds for timeout
            xhr.open("POST", url_temp, true);
            xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            let proto_json = JSON.stringify(bodyParams);
            xhr.send(proto_json);
        })
    }

    event(method, bodyParams, callback) {
        // bodyParams.token = TOKEN;
        new Promise((resolve,reject)=>{
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = ()=> {
                console.log('xhr.readyState='+xhr.readyState+'  xhr.status='+xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var response = JSON.parse(xhr.responseText);
                    let data = response.Data
                    if (data) {
                        data = JSON.parse(this.decrypt(response.Data))
                        response.Data = data
                    }
                    console.log("request response:", response.Code, response.Msg);
                    console.log("request data:", response.Data);
                    callback(data);
                }
            };
            xhr.ontimeout = (e)=> {
                console.log("timeout")
            }
            var url_temp = EVENT + method;
            console.log("url_temp", url_temp)
            console.log("bodyParams", JSON.stringify(bodyParams))
            xhr.timeout = 10000;// 5 seconds for timeout
            xhr.open("POST", url_temp, true);
            xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            let proto_json = this.encrypt(JSON.stringify(bodyParams));
            console.log("encrypt", proto_json);
            let decode = this.decrypt(proto_json)
            console.log("decode", decode);
            xhr.send(proto_json);
        })
    }

    report(bodyParams, callback) {
        new Promise((resolve,reject)=>{
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.onreadystatechange = ()=> {
                console.log('xhr.readyState='+xhr.readyState+'  xhr.status='+xhr.status);
                if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                    var response = JSON.parse(xhr.responseText);
                    let data = response.Data
                    if (data) {
                        data = JSON.parse(this.decrypt(response.Data))
                        response.Data = data
                    }
                    console.log("report response:", response.Code, response.Msg);
                    console.log("report data:", response.Data);
                    callback(data);
                }
            };
            xhr.ontimeout = (e)=> {
                console.log("timeout")
            }
            var url_temp = REPORT;
            console.log("url_temp", url_temp)
            console.log("reportParams", JSON.stringify(bodyParams))
            xhr.timeout = 10000;// 5 seconds for timeout
            xhr.open("POST", url_temp, true);
            // xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
            xhr.setRequestHeader("Content-Type","text/plain");
            xhr.setRequestHeader("app","happyfruit2048")
            let proto_json = this.encrypt(JSON.stringify(bodyParams));

            let params = JSON.stringify(bodyParams)
            console.log("encrypt", proto_json);
            let decode = this.decrypt(proto_json)
            console.log("decode", decode);
            xhr.send(proto_json);
        })
    }

    public decrypt(encrypt: string) {
        let decrypt = CryptoJS.AES.decrypt(encrypt, this.key, { iv: this.iv, mode: CryptoJS.mode.CTR });
        let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
        return decryptedStr.toString();
    }

    public encrypt(encrypt: string) {
        let srcs = CryptoJS.enc.Utf8.parse(encrypt);
        let encrypted = CryptoJS.AES.encrypt(srcs, this.key, { iv: this.iv, mode: CryptoJS.mode.CTR});
        return encrypted.toString()
    }
}
