import ZTimer from "./ZTimer";
const { ccclass, property } = cc._decorator;

@ccclass
export default class ShakeAnimation extends cc.Component {
    @property(cc.Node)
    targetNode: cc.Node = null;

    // @property(cc.Vec2)
    // shakeAmount: cc.Vec2 = cc.v2(10, 10);

    // @property(cc.Float)
    // shakeDuration: number = 0.5;
    
    // originalPosition;

    private timer = new ZTimer();
    timerMil = 10000;

    startShake() {
        
        // cc.tween(this.targetNode)
        // .to(this.shakeDuration/3,{position: cc.v3(-this.shakeAmount.x,-this.shakeAmount.y)})
        // .to(this.shakeDuration/3,{position: cc.v3(this.shakeAmount.x,this.shakeAmount.y)})
        // .to(this.shakeDuration/3,{position: cc.v3(0,0)})
        // .repeat(3)
        // .start();

        let x = 0; let y = 0;
        let duration = 0.05;
        let offset = 20;
        let action = cc.repeatForever(
            cc.sequence(
                cc.rotateTo(duration, -offset),
                cc.rotateTo(duration, offset),
                cc.rotateTo(duration, -offset*0.7),
                cc.rotateTo(duration, offset*0.7),
                cc.rotateTo(duration, -offset*0.4),
                cc.rotateTo(duration, offset*0.4),
                cc.rotateTo(duration, -offset*0.2),
                cc.rotateTo(duration, offset*0.2),
                cc.rotateTo(duration, -offset*0.05),
                cc.rotateTo(duration, offset*0.05),


                // cc.moveTo(0.018, cc.v2(x - offset, 0)),
                // cc.moveTo(0.018, cc.v2(x + offset, 0)),
                // cc.moveTo(0.018, cc.v2(x - offset, 0)),
                // cc.moveTo(0.018, cc.v2(x + offset, 0)),
                // cc.moveTo(0.018, cc.v2(x - offset, 0)),
                // cc.moveTo(0.018, cc.v2(x + offset, 0)),
                // cc.moveTo(0.018, cc.v2(x - offset, 0)),
                // cc.moveTo(0.018, cc.v2(x + offset, 0)),
                // cc.moveTo(0.018, cc.v2(x - offset, 0)),
                // cc.moveTo(0.018, cc.v2(x + offset, 0)),
            )
        )
        this.targetNode.runAction(action);
        this.scheduleOnce(()=>{
            this.targetNode.stopAction(action);
            this.targetNode.rotation = 0;
        },1);
    }

    protected start(): void {
        // this.originalPosition = this.targetNode.getPosition();
        // this.startTimerShake()
    }

    startTimerShake() {
        // 启动定时器，每10秒调用一次
        this.timer.startTimer(() => {
            this.startShake();
        }, this.timerMil);
    }

    stopTimerShake() {
        this.timer.stopTimer();
    }

    resetTimerShake() {
        // 重置定时器，每10秒调用一次
        this.timer.resetTimer(() => {
            this.startShake()
        }, this.timerMil);
    }

}


