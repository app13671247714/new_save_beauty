export default class ZTimer {
    private intervalId: number | null = null;
    private isRunning: boolean = false;

    // 启动定时器
    startTimer(callback: () => void, interval: number) {
        if (!this.isRunning) {
            this.intervalId = setInterval(callback, interval);
            this.isRunning = true;
        }
    }

    // 停止定时器
    stopTimer() {
        if (this.isRunning && this.intervalId !== null) {
            clearInterval(this.intervalId);
            this.intervalId = null;
            this.isRunning = false;
        }
    }

    // 重置定时器
    resetTimer(callback: () => void, interval: number) {
        this.stopTimer();
        this.startTimer(callback, interval);
    }
}
