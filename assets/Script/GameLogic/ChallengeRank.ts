// Learn TypeScript:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/2.4/manual/en/scripting/life-cycle-callbacks.html

import { i18nMgr } from "../../i18n/i18nMgr";
import ChallengeModel from "../Framework/Model/ChallengeModel";
import GameChallenge from "../GameChallenge";
import List from "../scrollViewTools/List";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ChallengeRank extends cc.Component {

    @property(cc.SpriteFrame)
    meIcon:cc.SpriteFrame = null;

    @property(List)
    list: List = null;

    private _dataArr = [];

    onLoad () {
        this.node.on("click",this.hide,this);
    }

    start () {
        // this.list.numItems = 20;
        this.loadData()
    }

    loadData(){
       this._dataArr = GameChallenge.Instance.getRankInfo()
        this.list.numItems = 50;
        let scrollIndex = GameChallenge.Instance.myRankIndex > 0 ? (GameChallenge.Instance.myRankIndex - 3) : 0;
        this.list.scrollTo(scrollIndex)
    }

    onListRender(item: cc.Node, idx: number) {
        let model:ChallengeModel = this._dataArr[idx]
        let isMe = model.name == "ME" ? true : false;

        //背景
        let bg = item.getChildByName("bg");
        bg.active = isMe;

        //排名
        let info = item.getChildByName("info");
        let rankLabel = info.getChildByName("rankLabel").getComponent(cc.Label);
        rankLabel.string = (idx + 1) + "";

        let rank1 = info.getChildByName("rank1");
        rank1.active = idx == 0 ? true : false;
        let rank2 = info.getChildByName("rank2");
        rank2.active = idx == 1 ? true : false;
        let rank3 = info.getChildByName("rank3");
        rank3.active = idx == 2 ? true : false;


        //昵称
        let titleLabel = info.getChildByName("titleLabel").getComponent(cc.Label);
        // titleLabel.string = "nickName" + idx;
        titleLabel.string = model.name;
        if(isMe){
            titleLabel.string = i18nMgr._getLabel("txt_138")
        }

        //头像
        let icon = info.getChildByName("img").getChildByName("icon").getComponent(cc.Sprite)
        let index = idx + 1;
        if(idx >= 20){
            index = Math.ceil(Math.random() * 20)
        }
        if(isMe){
            icon.spriteFrame = this.meIcon;
        }else{
            cc.loader.loadRes("icon/"+index,cc.SpriteFrame,(err,res) =>{
                icon.spriteFrame = res
            })
        }


        //过关等级
        let levelCount = info.getChildByName("levelCount").getComponent(cc.Label);
        levelCount.string = model.level.toString();
        levelCount.node.color = idx < 3 ? cc.color().fromHEX('#FF5E00') : cc.color().fromHEX('#FFFFFF')
    }

    show(parentNode: cc.Node) {
        if (parentNode) {
            parentNode.addChild(this.node);
            this.node.zIndex = 10000;
            this.node.active = false;            
            this.scheduleOnce(()=>{
                this.node.active = true;
                this.node.setPosition(0,-this.node.height)
            })
            this.scheduleOnce(()=>{
                this.node.runAction(cc.sequence(
                    cc.moveTo(1,cc.v2(0,0)).easing(cc.easeBackOut()),
                    cc.callFunc(() => {
                        console.log("动画结束")
                    })
                ));
            },1)
        }
    }

    hide() {
        this.node.runAction(cc.sequence(
            cc.moveTo(0.3,cc.v2(0, -this.node.height)),
            cc.callFunc(() => {
                this.node.removeFromParent();
                this.destroy()
            })
        ));
    }

    protected onDestroy(): void {
        console.log("销毁了")
    }

}
