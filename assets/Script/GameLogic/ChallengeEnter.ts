import EventManager from "../Common/LocalEvents";
import { EventConfig } from "../Framework/Config/EventConfig";
import { GameUtils } from "../Framework/Utils/GameUtils";
import GameChallenge from "../GameChallenge";


const { ccclass, property } = cc._decorator;

@ccclass
export default class ChallengeEnter extends cc.Component {

    @property(cc.Label)
    startLabel: cc.Label = null;

    @property(cc.Label)
    timeLabel: cc.Label = null;

    @property(cc.Label)
    rankLabel: cc.Label = null;

    @property(cc.Node)
    timerNode: cc.Node = null;

    @property(cc.Node)
    noRank: cc.Node = null;

    @property(cc.Node)
    rankNode: cc.Node = null;

    @property(cc.Node)
    challengeNode: cc.Node = null;

    time;


    onLoad() {
        this.timerNode.active = false;
        
        this.startLabel.node.active = true;
        EventManager.on(EventConfig.CHALLENGE_START_COUNT_DOWN, this.startCountDown, this)
    }

    start() {

    }

    startCountDown(time) {
        this.timerNode.active = true;
        this.challengeNode.active = false;
        this.startLabel.node.active = false;
        this.time = time;
        let limitTime = this.time + GameChallenge.Instance.twentyFourHoursInMillis;
        let now = Date.now()
        let leftTime = Math.ceil((limitTime - now) / 1000)
        if (leftTime > 0) {
            this.schedule(this.updateUI, 1);
        }else{
            this.timerNode.active = false;
            this.startLabel.node.active = true;
            this.challengeNode.active = true;
        }
    }

    updateUI() {
        let limitTime = this.time + GameChallenge.Instance.twentyFourHoursInMillis;
        let now = Date.now()
        let leftTime = Math.ceil((limitTime - now) / 1000)
        if (leftTime <= 0) {
            this.unschedule(this.updateUI);
            GameChallenge.Instance.resetCountDown()
        }
        let limitStr = GameUtils.formatTimeForSecond(leftTime)
        this.timeLabel.string = limitStr;

        let index = GameChallenge.Instance.myRankIndex;
        if (index > 0) {
            this.rankLabel.string = GameChallenge.Instance.myRankIndex.toString()
            this.noRank.active = false;
            this.rankNode.active = true;
        } else {
            this.rankNode.active = false;
            this.noRank.active = true;
        }
        this.rankLabel.string = GameChallenge.Instance.myRankIndex > 0 ? GameChallenge.Instance.myRankIndex.toString() : "暂无上榜"
    }

}
