

const { ccclass, property } = cc._decorator;
import { NodePool } from './NodePool';

@ccclass
export default class Money extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    @property(cc.Label)
    moneyLabel: cc.Label = null;

    mng: NodePool = null;

    start() {

    }

    startMove(pos) {
        var _this = this;
        var moveAction = cc.sequence(
            cc.spawn(cc.moveBy(1, cc.v2(0, 100)), cc.fadeOut(1)),
            cc.callFunc(function () {
                _this.scheduleOnce(function () {
                    _this.despawn();
                }, 0.3)
            })
        );
        this.node.runAction(moveAction);
    }

    setFontSize(scale) {
        this.moneyLabel.fontSize = 100 * scale;
    }

    despawn() {
        this.node.stopAllActions();
        this.node.opacity = 255;
        if (this.mng != null) {
            this.mng.despawn(this.node);
        } else {
            this.node.removeFromParent();
        }
    }

    reuse(mng) {
        this.init(mng);
    }

    init(mng) {
        this.mng = mng;
        this.enabled = true;
    }

    // update (dt) {},
}
