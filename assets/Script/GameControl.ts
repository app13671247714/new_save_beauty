// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import { JsBridge } from "./native/JsBridge";
import { CallNative } from "./native/CallNative";

const { ccclass, property } = cc._decorator;
import PoolMng from './PoolMng'
import Box from './Box'
import LevelUtils from './LevelUtils'
import UserData from './Bean/UserData'
import LevelData from './LevelData'
import PassLevelBean from './Bean/PassLevelBean'
import StorageUtil from './Common/StorageUtil'
import GlobalConst from './GlobalConst';
import SettleUtil from './SettleUtil';
import SubDomainControl from './SubDomainControl';
import CoinsUtil from './CoinsUtil';
import { i18nMgr } from '../i18n/i18nMgr';
import DualBlur from './DualBlur/DualBlur';
import { BaseLayer } from './base/BaseLayer';
import ReportMgr, { ReportConfig } from "./Common/ReportMgr";
import { BaseConfig } from "./base/BaseConfig";
import LocalDataManager from "./Common/LocalDataManager";
import MainControl from "./MainControl";
import ThemeViewControl from "./ThemeViewControl";
import ThemeScrollControl from "./ThemeScrollControl";
import ShakeAnimation from "./Custom/ShakeAnimation";
import ChallengeRank from "./GameLogic/ChallengeRank";
import GameChallenge from "./GameChallenge";
import LimitFailControl from "./LimitFailControl";

const WX = window["wx"];

@ccclass
export default class GameControl extends BaseLayer {

    @property(cc.Label)
    label: cc.Label = null;

    //结算对话框
    @property(cc.Node)
    m_WinLayout: cc.Node = null;

    @property(cc.Node)
    m_CatFace: cc.Node = null;

    @property(cc.Node)
    m_InnerLayout: cc.Node = null;

    //结算界面关卡标题
    @property(cc.Label)
    settleLevelLabel: cc.Label = null;

    //结算界面耗时百分比标题
    @property(cc.Label)
    settlePercentLabel: cc.Label = null;

    //结算界面超过玩家标题
    @property(cc.Label)
    settlePlayerLabel: cc.Label = null;

    //挑战赛倒计时动画
    @property(cc.Node)
    countDownNode: cc.Node = null;
    //挑战赛倒计时动画背景
    @property(cc.Node)
    countDownNodeBg: cc.Node = null;

    @property(cc.Label)
    countDownLabel: cc.Label = null;

    @property(cc.Node)
    limitFailDialog: cc.Node = null;
    //挽留弹窗
    limitFail:LimitFailControl = null;

    @property(cc.Node)
    mainCanvas: cc.Node = null;

    @property(cc.Node)
    m_ChallengeBg: cc.Node = null;
    @property(cc.Node)
    m_LimitBg: cc.Node = null;

    @property(cc.Node)
    m_Tool: cc.Node = null;

    @property(cc.Node)
    m_ToolBg: cc.Node = null;

    @property(cc.Node)
    m_GoldLayout: cc.Node = null;

    //宝箱进度条
    @property(cc.Node)
    m_PressureProgress: cc.Node = null;

    //结算对话框中排序Node
    @property(cc.Node)
    m_DialogRank: cc.Node = null;

    //底部背景
    @property(cc.Sprite)
    m_BottomView: cc.Sprite = null;
    @property(cc.Sprite)
    m_Bg: cc.Sprite = null;

    //双倍领取
    @property(cc.Node)
    m_DoubleSettleView: cc.Node = null;

    //金币动画的目标Node
    @property(cc.Node)
    coinTargetNode: cc.Node = null;

    @property(cc.ParticleSystem)
    settilePassAnim: cc.ParticleSystem = null;

    @property(cc.Node)
    limitBackDialog: cc.Node = null;

    @property(cc.Node)
    fingerGuide: cc.Node = null;

    //视频下一关
    @property(cc.Node)
    videoNextBtn: cc.Node = null;

    @property(cc.SpriteFrame)
    BgSprite: cc.SpriteFrame = null;

    //游戏页提示
    @property(cc.Node)
    aniNode: cc.Node = null;

    gamePageNode: cc.Node = null;

    poolMng: PoolMng = null;

    m_RowNum: number = 0;
    m_ColNum: number = 0;
    m_BoxWidth: number = 0;
    m_BoxHeight: number = 0;

    m_MaxBoxWidth: number = 120;

    m_BoxArray: Array<Array<cc.Node>> = new Array<Array<cc.Node>>();
    m_LevelConfigArray: Array<Array<number>> = new Array<Array<number>>();
    m_PathArray: Array<cc.Node> = new Array();
    m_AnswerPathArray: Array<Array<number>> = new Array();

    m_StartTouchMode: boolean = false;

    //每一关总的节点数
    m_TotalItemCount: number = 0;


    levelUtils: LevelUtils = new LevelUtils();
    storageUtil: StorageUtil = new StorageUtil();

    coinsUtil: CoinsUtil = new CoinsUtil();

    playAnimTime: number = 0.08;

    //一次提示4步
    notifyStep: number = 4;
    //当前已经提示到的位置索引
    currentStep: number = 1;

    currentLevel: number = 1;

    userData: UserData;
    levelData: LevelData;

    globalConst: GlobalConst = new GlobalConst();

    //单关游戏消耗时间
    costTime: number = 0;
    //单关游戏消耗时间
    helpTime: number = 0;
    lastRestTime: number = this.globalConst.countDownTime;

    //是否挑战赛
    isLimitGame: boolean = false;

    settleUtil: SettleUtil = new SettleUtil();

    limitLevel: number = 1;

    //挑战模式关卡随机数组
    limitLevelArray: Array<number> = new Array<number>();
    //挑战模式是否开始倒计时，排除动画时间
    startCountDown: boolean = false;

    //每3关奖励一次宝箱
    awardStep: number = 3;

    //金币数
    gold: number = 0;

    //是否分享后提示
    isShareNotify: boolean = false;


    subDomainControl: SubDomainControl = new SubDomainControl();

    //身体颜色
    bodyNodeColor: string = '';

    originHeight: number;

    //关卡是否加载完成
    boxArrayLoaded: boolean = false;

    //主题
    theme: number = 1;
    //类别
    chapter: number = 1;
    // LIFE-CYCLE CALLBACKS:
    //当前关卡 1-20 
    level: number = 1;

    shareLabelIndex: number = -1;

    //是否展示过提示框
    notifyDialogShow: boolean = false;
    //提示弹框Prefab
    notifyDialogPref: cc.Prefab = null;
    //提示弹框node
    notifyDialogNode: cc.Node = null;

    //弹框优先级数组
    settlePopupPriorityArray: Array<number> = new Array<number>();
    settleLevel: number = 0;
    passData: PassLevelBean = null;

    //每20关过关进入下一关数据
    nextUnlokTheme:number = 1;
    nextUnlokLevel:number = 1;
    //累计关卡
    skipIndex:number = 0;
    //广告加时
    adAddATime:number = 0;

    onLoad() {
        this.originHeight = cc.winSize.height * 0.616;
        this.poolMng = this.node.getComponent('PoolMng');
        this.poolMng.init();


        this.limitFail = this.limitFailDialog.getComponent(LimitFailControl);
        this.limitFail.init(this.mainCanvas,this.m_CatFace);

        // this.initTestLevel();
        // this.initLevel(this.currentLevel);
        // this.initBoxArray();

        this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEvent, this);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchEvent, this);

        if (JsBridge.isMobile()) {
            cc["CallNative"] = CallNative;
        }
    }

    initBoxArray() {
        this.helpTime = 0;
        this.node.parent.getChildByName("Tools").opacity = 255
        this.node.parent.getChildByName("Topbar").opacity = 255
        this.m_CatFace.opacity = 255
        this.node.opacity = 255
        this.node.parent.getChildByName("body").opacity = 255
        let layout: cc.Layout = this.node.getComponent(cc.Layout);
        this.node.removeAllChildren();

        this.node.height = this.originHeight;
        // this.node.getComponent(cc.Widget).top = 0.21 * this.node.getParent().height;
        // this.node.getComponent(cc.Widget).bottom = 0.18* this.node.getParent().height;


        //spacingx为0.1的节点宽度
        this.m_BoxWidth = this.node.width / (1.1 * this.m_ColNum + 0.9);
        this.m_BoxWidth = Math.min(this.m_BoxWidth, this.node.height / (1.1 * this.m_RowNum + 0.9));

        // this.m_BoxWidth = (this.node.width- (this.m_ColNum - 1)*layout.spacingX)/(this.m_ColNum+1);
        // this.m_BoxHeight = (this.node.height - layout.paddingTop - layout.paddingBottom - (this.m_RowNum - 1)*layout.spacingY)/this.m_RowNum;

        this.m_BoxWidth = Math.min(this.m_BoxWidth, this.m_MaxBoxWidth);


        layout.spacingX = 0.1 * this.m_BoxWidth;
        layout.spacingY = layout.spacingX;


        layout.paddingLeft = (this.node.width - this.m_BoxWidth * this.m_ColNum - (this.m_ColNum - 1) * layout.spacingX) / 2;
        layout.paddingRight = layout.paddingLeft;

        this.m_BoxHeight = this.m_BoxWidth;

        // this.m_CatFace.height = this.m_BoxHeight;
        // this.m_CatFace.width = this.m_BoxWidth;

        for (let i = 0; i < this.m_RowNum; i++) {
            this.m_BoxArray.push([]);
            for (let j = 0; j < this.m_ColNum; j++) {
                let box: cc.Node = this.poolMng.spawnBox();
                let scale = (this.m_BoxWidth - 1) / 95;
                box.width = this.m_BoxWidth - 1;
                box.height = this.m_BoxHeight - 1;

                this.m_BoxArray[i][j] = box;
                this.node.addChild(box);
                let boxTs: Box = box.getComponent('Box');
                boxTs.setValue(this.m_LevelConfigArray[i][j], scale);

                if (this.m_LevelConfigArray[i][j] == -1) {
                    box.opacity = 0;
                } else {
                    this.m_TotalItemCount++;
                    if (this.m_LevelConfigArray[i][j] == 1) {
                        this.m_PathArray.push(this.m_BoxArray[i][j]);
                    }
                    box.opacity = 255;
                }
                boxTs.x_Index = i;
                boxTs.y_Index = j;
            }
        }


        layout.updateLayout();
        let faceScale = this.m_BoxWidth / this.m_CatFace.width;
        this.m_CatFace.setScale(faceScale);
        this.m_CatFace.zIndex = this.m_TotalItemCount;
        this.drawCatFace(this.m_PathArray[0], this.m_PathArray[0].getComponent('Box').m_Oritation);

        if (!this.isLimitGame) {
            this.storageUtil.saveData('playLevel', this.theme);
            this.scheduleOnce(() => {
                cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playGameHelpAudio();
            }, 0.2)
        }
        this.boxArrayLoaded = true;
        let mainTs = cc.find('Canvas').getComponent('MainControl');
        if (!mainTs.Membership) {
            mainTs.showBanner(this.isLimitGame ? mainTs.ChallengeBanner : mainTs.PlayGameBanner, function (bannerHeight) {
            }.bind(this));
        } else {
            // this.notifyCostVedio.acti ve = false;
        }
        this.mainCanvas.getComponent('MainControl').getVolumeControl().playBgMusic(this.theme - 1);
    }

    initTestLevel() {
        for (let i = 0; i < this.m_RowNum; i++) {
            this.m_LevelConfigArray.push([]);
            for (let j = 0; j < this.m_ColNum; j++) {

                this.m_LevelConfigArray[i][j] = -1;

            }
        }
        this.m_LevelConfigArray[0][0] = 1;
        this.m_LevelConfigArray[0][1] = 0;
        this.m_LevelConfigArray[0][2] = 0;
        this.m_LevelConfigArray[1][1] = 0;
        this.m_LevelConfigArray[1][2] = 0;
        this.m_LevelConfigArray[2][1] = 0;
    }

    //该节点周围是否有连通节点，返回连通后节点的方向
    hasAroundNode(x: number, y: number) {
        let orientation: String;
        //左边有已绘制相邻节点
        if (y - 1 >= 0 && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[x][y - 1]) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
            orientation = "Right";
            this.m_BoxArray[x][y].getComponent('Box').setOritation(orientation);
            this.drawNextNode(x, y, this.m_BoxArray[x][y - 1], orientation);
            return;
        }
        //上边有已绘制相邻节点
        if (x - 1 >= 0 && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[x - 1][y]) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
            orientation = "Bottom";
            this.m_BoxArray[x][y].getComponent('Box').setOritation(orientation);
            this.drawNextNode(x, y, this.m_BoxArray[x - 1][y], orientation);
            return;
        }
        //右边有已绘制相邻节点
        if (y + 1 < this.m_ColNum && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[x][y + 1]) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
            orientation = "Left";
            this.m_BoxArray[x][y].getComponent('Box').setOritation(orientation);
            this.drawNextNode(x, y, this.m_BoxArray[x][y + 1], orientation);
            return;
        }
        //下边有已绘制相邻节点
        if (x + 1 < this.m_RowNum && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[x + 1][y]) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
            orientation = "Top";
            this.m_BoxArray[x][y].getComponent('Box').setOritation(orientation);
            this.drawNextNode(x, y, this.m_BoxArray[x + 1][y], orientation);
            return;
        }


        //表示无相邻节点则处理头部节点周围的节点提示
        // this.notifyAroundBox(this.m_PathArray[this.m_PathArray.length-1]);

        this.m_BoxArray[x][y].getComponent('Box').setOritation("null");

    }

    onTouchEvent(event: cc.Event.EventTouch) {
        if (this.fingerGuide.active == true) {
            this.fingerGuide.active = false;
        }
        //如果关卡信息没有加载完成，不响应点击事件，防止pathArray报错
        if (this.boxArrayLoaded == false) {
            return;
        }


        let x: number = event.getLocationX();
        let y: number = event.getLocationY();

        var spaceVec = this.node.convertToNodeSpaceAR(cc.v2(x, y));

        let layout: cc.Layout = this.node.getComponent(cc.Layout);
        //触点在layout中的x索引和y索引
        // let y_Index = -1;
        // let x_Index = -1;
        let y_Index: number = Math.floor((spaceVec.x - layout.paddingLeft + this.node.width / 2) / (this.m_BoxWidth + layout.spacingX));
        let x_Index: number = Math.floor((this.node.height / 2 - spaceVec.y - layout.paddingTop) / (this.m_BoxHeight + layout.spacingY));



        // for (let i = 0; i < this.m_RowNum; i++) {

        //     for (let j = 0; j < this.m_ColNum; j++) {
        //         if(this.m_BoxArray[i][j].getComponent('Box').value != -1
        //             && this.m_BoxArray[i][j].getBoundingBox().contains(spaceVec)){
        //                 x_Index = i;
        //                 y_Index = j;
        //         }
        //     }
        // }

        //防止挑战赛刚过关path还未初始化完成点击节点报错
        if (this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box') == null) {
            return;
        }
        let node = this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box')
        if (event.type == 'touchstart' && node.y_Index == y_Index && node.x_Index == x_Index) {
            this.mainCanvas.getComponent('MainControl').getVolumeControl().playStartAudio();
        }

        if (y_Index < 0 || y_Index >= this.m_ColNum) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
            return;
        }

        if (x_Index < 0 || x_Index >= this.m_RowNum) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
            return;
        }

        //已经过关直接返回，防止误触小猫回退
        if (this.m_TotalItemCount <= this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box').value) {
            return;
        }

        if (!this.m_BoxArray[x_Index][y_Index].getBoundingBox().contains(spaceVec)) {
            return;
        }


        if (cc.Node.EventType.TOUCH_END == event.getType()
            || cc.Node.EventType.TOUCH_CANCEL == event.getType()) {
            this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
        } else {
            this.m_BoxArray[x_Index][y_Index].getComponent('Box').processTouch(event);
        }

    }


    //往回画图时让Path队列中的数据出栈
    popPathArrayList(popNode: cc.Node) {
        let popIndex = this.m_PathArray.indexOf(popNode);
        if (popIndex < 0 || popIndex >= this.m_PathArray.length - 1) {
            return;
        }
        let count = 0;
        this.drawCatFace(this.m_PathArray[this.m_PathArray.length - 1], this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box').m_Oritation);
        let popNodeCount = this.m_PathArray.length - 1 - popIndex;
        let backAnimTime = this.playAnimTime;
        if (popNodeCount * this.playAnimTime > 0.5) {
            backAnimTime = 0.5 / popNodeCount;
        }
        //递归调用
        this.moveBackward(this.m_PathArray[this.m_PathArray.length - 1], this.m_PathArray[this.m_PathArray.length - 2], popIndex, backAnimTime);
    }

    onTouchEventStart(event: cc.Event.EventTouch) {
        this.onTouchEvent(event);
    }

    onTouchEventMove(event: cc.Event.EventTouch) {
        this.onTouchEvent(event);
    }

    //成功填充下一个节点
    drawNextNode(x: number, y: number, preNode: cc.Node, orientation: String) {
        let preValue = preNode.getComponent('Box').value;
        this.m_BoxArray[x][y].getComponent('Box').value = preValue + 1;
        this.m_PathArray.push(this.m_BoxArray[x][y]);
        this.drawCatFace(preNode, orientation);
        this.moveForward(preNode, this.m_BoxArray[x][y], orientation);
        this.winOrNot();
    }

    //判断是否成功通关
    winOrNot() {
        if (this.m_TotalItemCount > this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box').value) {
            return;
        }
        this.mainCanvas.getComponent('MainControl').getVolumeControl().stopMusic();
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playGameEndAudio();
        //闯关模式弹出结算界面
        this.scheduleOnce(function () {
            let mainTs = cc.find('Canvas').getComponent(MainControl)
            if (!this.isLimitGame) {
                if (this.m_WinLayout.active === false) {
                    mainTs.hideBanner()
                    if (this.storageUtil.getData('unLockLevel' + this.currentLevel) != 2) {
                        this.storageUtil.saveData('unLockLevel' + this.currentLevel, 2);
                        let passCount = this.storageUtil.getData('passCount');
                        if (passCount) {
                            passCount++
                        } else {
                            passCount = 1
                        }
                        this.storageUtil.saveData('passCount', passCount)
                        let value = passCount / (this.globalConst.levelCount * this.globalConst.themeCount * this.globalConst.chapterCount) * 100
                        this.settlePlayerLabel.string = i18nMgr._getLabel("txt_118")
                    }
                    this.m_WinLayout.zIndex = this.m_CatFace.zIndex + 1;
                    this.settleLevel = this.currentLevel;

                    this.showSettleData();
                    this.levelData = new LevelData();
                    this.saveLevelData();
                    this.currentLevel++;
                    if (this.userData.gameLevel < this.currentLevel) {
                        this.userData.gameLevel = this.currentLevel;
                        this.userData.gold = this.gold;
                        this.storageUtil.saveData('userData', this.userData);
                    }
                    //记录通关时间
                    this.passData = this.storageUtil.getData('passData');
                    if (this.passData == null) {
                        this.passData = new PassLevelBean();
                    }
                    if (this.passData.date && this.passData.date === new Date().getMonth() + '_' + new Date().getDate()) {
                        this.passData.level = this.passData.level + 1;
                        this.storageUtil.saveData('passData', this.passData);
                    } else {
                        this.passData.level = 0;
                        this.passData.date = new Date().getMonth() + '_' + new Date().getDate();
                        this.storageUtil.saveData('passData', this.passData);
                    }
                }
            } else {
                //挑战赛模式直接进入下一关
                if(this.currentLevel % BaseConfig.CloudInfo.challengeCount == 0){
                    console.log("挑战赛当前完成关卡:"+this.currentLevel);
                    mainTs.loadInterAd(mainTs.ChallengePassLevel);
                }
                this.startCountDown = false;
                this.currentLevel++;
                this.calculateLimitLevel();
                this.passNextLevel();
            }
        }.bind(this), 0.2);

    }

    onPassNextLevelClick() {
        this.refreshGamePage();
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        if (!this.isLimitGame) {
            let passTime = this.costTime ? Math.round(this.costTime) : 0;
            if (this.currentLevel - 1 == this.globalConst.chapterCount * this.globalConst.levelCount * this.globalConst.themeCount) {
                this.backBtnClick()
            } else {
                let info = ReportConfig.ReportMap;
                info.theme = this.theme;
                info.chapter = this.chapter;
                info.level = this.level;
                info.passtime = passTime;
                info.event = "click_next_level";
                info.event_msg = "page_game";
                if (this.m_PressureProgress.active) {
                    info.page = "page_ad_page_game_click_next_level_inter";
                    this.onVideoNextClick()
                } else {
                    this.passNextLevel();
                }

                //跨20关 处理
                if ((this.currentLevel - 1) % this.globalConst.levelCount == 0) {
                    let nextTheme = Math.ceil(this.currentLevel / this.globalConst.levelCount)
                    if(!this.storageUtil.getData('unLockTheme' + nextTheme)){
                        this.storageUtil.saveData('unLockTheme' + nextTheme, true);
                        let themeView = this.mainCanvas.getComponent('MainControl').themeView.getComponent(ThemeViewControl)
                        themeView.refreshTheme();
                        //  刷新进度条
                        let mainTs = cc.find('Canvas').getComponent('MainControl');
                        mainTs.unLockThemeCount++;
                        this.storageUtil.saveData('unLockThemeCount', mainTs.unLockThemeCount);
                        themeView.m_ScrollView.getComponent(ThemeScrollControl).refreshThemeProgress();
                    }
                }
                ReportMgr.getInst().uploadConfig(info);
            }
        }
    }

    saveBtn() {
        let mainTs = this.mainCanvas.getComponent('MainControl');
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        if (mainTs.Membership) {
            this.successAd();
        } else {
            mainTs.loadVideoAd(mainTs.download);
        }

        let info = ReportConfig.ReportMap;
        info.theme = this.theme;
        info.chapter = this.chapter;
        info.level = this.level;
        info.event = "click_save_pic";
        info.event_msg = "page_game";
        info.page = "page_ad_page_game_click_save_pic_rewarded";
        ReportMgr.getInst().uploadConfig(info);
    }
    successAd() {
        let bgPath = `${new LevelData().currentUrl}/bg/id_${this.chapter + (this.theme - 1) * this.globalConst.chapterCount}/level_bg_${this.level}.jpg`
        console.log(bgPath, "bgPath+++++++++")
        if (JsBridge.isMobile()) {
            JsBridge.callWithCallback(JsBridge.SAVE, { type: 'img', url: bgPath }, (jsonStr: object) => {

            })
        }
    }

        passNextLevel() {
        this.boxArrayLoaded = false;
        let mainTs = this.mainCanvas.getComponent('MainControl');
        // mainTs.hideBanner();
        this.m_DialogRank.active = false;
        this.m_WinLayout.active = false;
        this.aniNode.getComponent(ShakeAnimation).resetTimerShake()
        //挑战赛且第一关防止倒计时阶段开始计时
        if (!(this.isLimitGame && this.currentLevel == 1)) {
            this.startCountDown = true;
        }

        let passTime = this.costTime ? Math.round(this.costTime) : 0;

        this.currentStep = 1;
        this.costTime = 0;
        this.notifyDialogShow = false;

        this.resetLevelInfo();
        this.refreshGamePage();
        if (this.isLimitGame) {
            this.adAddATime = 0;
            if(this.currentLevel > 1){
                let info = ReportConfig.ReportMap;
                info.level = this.currentLevel - 1;
                info.event = "evt_challenge_pass_level";
                info.event_msg = "page_challenge";
                info.rank = GameChallenge.Instance.myRankIndex;
                info.passtime = passTime;
                ReportMgr.getInst().uploadConfig(info);
                GameChallenge.Instance.updateLevel(this.currentLevel -1)
            }
            this.initLimitBg();
            this.initLevel(this.limitLevel);
        } else {
            this.initGameBg();
            this.initLevel(this.currentLevel);
        }
        // this.scheduleCatAudio();
    }

    drawCatFace(faceNode: cc.Node, orientation: String) {

        this.m_CatFace.active = true;
        var faceWorldVec = this.node.convertToWorldSpaceAR(faceNode.getPosition());
        var faceSpaceVec = this.node.getParent().convertToNodeSpaceAR(faceWorldVec);
        // if (orientation == "Right") {
        //     this.m_CatFace.angle = 90;
        //     faceSpaceVec.x = faceSpaceVec.x + faceNode.width / 2;
        // } else if (orientation == "Left") {
        //     this.m_CatFace.angle = -90;
        //     faceSpaceVec.x = faceSpaceVec.x - faceNode.width / 2;
        // } else if (orientation == "Top") {
        //     this.m_CatFace.angle = -180;
        //     faceSpaceVec.y = faceSpaceVec.y + faceNode.height / 2;
        // } else {
        //     this.m_CatFace.angle = 0;
        //     faceSpaceVec.y = faceSpaceVec.y - faceNode.height / 2;
        // }

        this.m_CatFace.setPosition(faceSpaceVec);


        if (this.m_PathArray.length > 1) {
            if (this.m_PathArray[1].getComponent('Box').m_Oritation == 'null') {
                this.drawCatTail(this.m_PathArray[0], orientation);
            } else {
                this.drawCatTail(this.m_PathArray[0], this.m_PathArray[1].getComponent('Box').m_Oritation);
            }

        } else {
            this.drawCatTail(this.m_PathArray[0], this.m_PathArray[0].getComponent('Box').m_Oritation);
        }

    }

    moveForward(preNode: cc.Node, curNode: cc.Node, orientation: String) {
        // let scale = (curNode.width * 2+this.node.getComponent(cc.Layout).spacingX)/curNode.width;
        let wideWidth = curNode.width * 2 + this.node.getComponent(cc.Layout).spacingX;
        preNode.getComponent('Box').playForward(orientation, this.playAnimTime, wideWidth);

        var faceWorldVec = this.node.convertToWorldSpaceAR(curNode.getPosition());
        var faceSpaceVec = this.node.getParent().convertToNodeSpaceAR(faceWorldVec);

        this.mainCanvas.getComponent('MainControl').getVolumeControl().playMoveAudio();
        // if (orientation == "Right") {
        //     this.m_CatFace.angle = 90;
        //     faceSpaceVec.x = faceSpaceVec.x + curNode.width / 2;
        // } else if (orientation == "Left") {
        //     this.m_CatFace.angle = -90;
        //     faceSpaceVec.x = faceSpaceVec.x - curNode.width / 2;
        // } else if (orientation == "Top") {
        //     this.m_CatFace.angle = 180;
        //     faceSpaceVec.y = faceSpaceVec.y + curNode.height / 2;
        // } else {
        //     this.m_CatFace.angle = 0;
        //     faceSpaceVec.y = faceSpaceVec.y - curNode.height / 2;
        // }

        this.m_CatFace.stopAllActions();
        this.m_CatFace.runAction(cc.moveTo(this.playAnimTime, faceSpaceVec));

    }

    /*
    * preNode表示回退的前一个节点, curNode表示当前选中需要回退的节点
    */
    moveBackward(preNode: cc.Node, curNode: cc.Node, endIndex: number, backAnimTime: number) {
        var orientation: String = preNode.getComponent('Box').m_Oritation;
        // curNode.getComponent('Box').playBackward(1);

        var faceWorldVec = this.node.convertToWorldSpaceAR(curNode.getPosition());
        var faceSpaceVec = this.node.getParent().convertToNodeSpaceAR(faceWorldVec);

        this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);

        this.mainCanvas.getComponent('MainControl').getVolumeControl().playMoveEditAudio();

        if (orientation == "Right") {
            // this.m_CatFace.angle = 90;
            faceSpaceVec.x = faceSpaceVec.x + curNode.width / 2;
        } else if (orientation == "Left") {
            // this.m_CatFace.angle = -90;
            faceSpaceVec.x = faceSpaceVec.x - curNode.width / 2;
        } else if (orientation == "Top") {
            // this.m_CatFace.angle = 180;
            faceSpaceVec.y = faceSpaceVec.y + curNode.height / 2;
        } else {
            // this.m_CatFace.angle = 0;
            faceSpaceVec.y = faceSpaceVec.y - curNode.height / 2;
        }


        this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box').reset(false);
        this.m_PathArray.splice(this.m_PathArray.length - 1, 1);


        var spwan = cc.spawn(
            cc.callFunc(function () {
                this.drawCatFace(preNode, orientation);
                curNode.getComponent('Box').playBackward(backAnimTime, orientation);

            }.bind(this)),
            cc.moveTo(backAnimTime, faceSpaceVec));

        // var delayTime = loopCount * 1;
        var backSeq = cc.sequence(
            spwan,
            cc.callFunc(function () {
                this.drawCatFace(curNode, curNode.getComponent('Box').m_Oritation);

                if (this.m_PathArray.length - 2 >= endIndex) {
                    this.moveBackward(this.m_PathArray[this.m_PathArray.length - 1], this.m_PathArray[this.m_PathArray.length - 2], endIndex, backAnimTime);
                }

            }.bind(this)));
        this.m_CatFace.stopAllActions();
        this.m_CatFace.runAction(backSeq);
    }

    initLevel(level: number) {
        this.userData = this.storageUtil.getData("userData");
        if (this.userData == null) {
            this.userData = new UserData();
        }
        let theme = Math.ceil(level / this.globalConst.chapterCount / this.globalConst.levelCount);
        let chapterTemp = this.currentLevel - (theme - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
        if(this.isLimitGame){
            chapterTemp = level - (theme - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
        }
        let chapter = Math.ceil(chapterTemp / this.globalConst.levelCount);
        cc.log(theme, chapterTemp, chapter, "chapter++++")

        // let secondaryLevel = Math.ceil(level / this.globalConst.levelCount);
        let secondaryLevel = chapter;
        this.levelUtils.loadLevelJson(theme, secondaryLevel, function (json) {
            let levelIndex = level - (theme - 1) * this.globalConst.chapterCount * this.globalConst.levelCount - (secondaryLevel - 1) * this.globalConst.levelCount - 1;
            console.log('levelData level jason  = ', json);
            console.log('levelData levelIndex = ', levelIndex);
            let levelData = this.levelUtils.hex_to_levelData(this.levelUtils.getBinString(json[levelIndex]));
            console.log("levelData", levelData);

            this.initRowColNum(levelData);
            for (let i = 0; i < this.m_RowNum; i++) {
                this.m_LevelConfigArray.push([]);
                for (let j = 0; j < this.m_ColNum; j++) {
                    this.m_LevelConfigArray[i][j] = -1;
                }
            }


            for (let i = 0; i < levelData.length; i++) {
                let data = levelData[i];
                this.m_AnswerPathArray.push(data);
                if (i == 0) {
                    this.m_LevelConfigArray[data[1]][data[0]] = 1;
                } else {
                    this.m_LevelConfigArray[data[1]][data[0]] = 0;
                }

            }
            let tempData = levelData[0];

            this.initBoxArray();

            this.label.string = i18nMgr._getLabel("txt_06").replace("&", this.currentLevel);

            //如果是答案分享进入,显示所有提示
            // if (this.isAnswerGame && !this.isLimitGame) {
            //     this.onNotifyClick(null);
            // }
            if (!this.isLimitGame && this.needGuideView()) {
                this.showGuideAnim();
            }
        }.bind(this));
    }

    initRowColNum(levelData) {
        this.m_RowNum = 0;
        this.m_ColNum = 0;
        for (let i = 0; i < levelData.length; i++) {
            let data = levelData[i];
            if (data[1] > this.m_RowNum) {
                this.m_RowNum = data[1];
            }
            if (data[0] > this.m_ColNum) {
                this.m_ColNum = data[0];
            }
        }
        this.m_RowNum += 1;
        this.m_ColNum += 1;
    }


    onNotifyBtnClick() {
        //如果已经提示到头不扣金币，啥也不做
        if (this.currentStep == this.m_AnswerPathArray.length) {
            return;
        }
        let mainTs = this.mainCanvas.getComponent('MainControl');
        if (mainTs.Membership) {
            // this.startCountDown = false;
            this.onNotifyClick()
        } else {
            //todo 是否需要暂停 看广告会自动打断暂停
            // this.startCountDown = false;
            mainTs.loadVideoAd(mainTs.Notify,this.isLimitGame);

            let info = ReportConfig.ReportMap;
            info.theme = this.theme;
            info.chapter = this.chapter;
            info.level = this.level;
            info.state = this.isLimitGame ? 1 : 0;
            info.event = "click_game_noti";
            info.event_msg = this.isLimitGame ? "page_challenge" : "page_game";
            if(this.isLimitGame){
                info.page = "page_ad_page_challenge_click_game_noti_rewarded";
            }else{
                info.page = "page_ad_page_game_click_noti_alert_tips_rewarded";
            }
            ReportMgr.getInst().uploadConfig(info);
        }
    }

    //点击了提示按钮
    onNotifyClick(notifyStep?: number) {
        let step = this.notifyStep;
        if (notifyStep) {
            step = notifyStep;
        }
        console.log('视频看完提示');

        //如果是挑战赛看完视频继续计时
        if (this.isLimitGame) {
            this.startCountDown = true;
        }

        //如果是分享答案进入，直接显示所有答案
        // if (this.isAnswerGame) {
        //     step = this.m_AnswerPathArray.length;
        // } else if (this.isShareNotify) {
        //     //如果是分享提示进入，直接提示
        //     this.isShareNotify = false;
        // }

        if (this.currentStep + step > this.m_AnswerPathArray.length) {
            step = this.m_AnswerPathArray.length - this.currentStep;
        }

        //Pop到当前已经标记的提示位置
        let currentPopData;
        if (this.currentStep == 0) {
            currentPopData = this.m_AnswerPathArray[this.currentStep];
        } else {
            currentPopData = this.m_AnswerPathArray[this.currentStep - 1];
        }

        this.popPathArrayList(this.m_BoxArray[currentPopData[1]][currentPopData[0]]);
        //将各个提示背景显示出来
        let count = 1;
        for (let index = this.currentStep; index < this.currentStep + step; index++) {
            let indexData = this.m_AnswerPathArray[index];
            let notifyNode = this.m_BoxArray[indexData[1]][indexData[0]];
            //计算方向
            let orientation = "Bottom";
            if (index > 0) {
                let preIndexData = this.m_AnswerPathArray[index - 1];
                let verticalOri = indexData[1] - preIndexData[1];
                let horizOri = indexData[0] - preIndexData[0];
                if (verticalOri < 0) {
                    orientation = "Top";
                }

                if (verticalOri > 0) {
                    orientation = "Bottom";
                }

                if (verticalOri == 0) {
                    if (horizOri < 0) {
                        orientation = "Left";
                    }

                    if (horizOri > 0) {
                        orientation = "Right";
                    }
                }
                this.scheduleOnce(function () {
                    let isLastNode = index == this.m_AnswerPathArray.length - 1;
                    notifyNode.getComponent('Box').notifyClicked(orientation, isLastNode);

                }.bind(this), 0.1 * index);

            }
            count++;
        }
        this.currentStep = this.currentStep + step;
    }

    resetLevelInfo() {
        this.m_TotalItemCount = 0;
        for (let i = 0; i < this.m_RowNum; i++) {
            for (let j = 0; j < this.m_ColNum; j++) {
                this.m_BoxArray[i][j].getComponent('Box').reset(true);
                this.m_BoxArray[i][j].getComponent('Box').removeBodyNode();
                this.poolMng.despwanBox(0, this.m_BoxArray[i][j]);
            }
        }

        this.m_BoxArray.splice(0, this.m_BoxArray.length);
        this.m_LevelConfigArray.splice(0, this.m_LevelConfigArray.length);
        this.m_PathArray.splice(0, this.m_PathArray.length);
        this.m_AnswerPathArray.splice(0, this.m_AnswerPathArray.length);
    }

    //提示周围相邻节点
    notifyAroundBox(targetBox: cc.Node) {

        let boxTs = targetBox.getComponent('Box');
        let x = boxTs.x_Index;
        let y = boxTs.y_Index;
        //左边有未绘制相邻节点
        let arr = []
        if (y - 1 >= 0 && this.m_BoxArray[x][y - 1].getComponent('Box').value == 0) {
            // this.m_BoxArray[x][y - 1].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x][y - 1])
        }
        //上边有未绘制相邻节点
        if (x - 1 >= 0 && this.m_BoxArray[x - 1][y].getComponent('Box').value == 0) {
            // this.m_BoxArray[x - 1][y].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x - 1][y])
        }
        //右边有未绘制相邻节点
        if (y + 1 < this.m_ColNum && this.m_BoxArray[x][y + 1].getComponent('Box').value == 0) {
            // this.m_BoxArray[x][y + 1].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x][y + 1])
        }
        //下边有未绘制相邻节点
        if (x + 1 < this.m_RowNum && this.m_BoxArray[x + 1][y].getComponent('Box').value == 0) {
            // this.m_BoxArray[x + 1][y].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x + 1][y])
        }
        return arr;
    }

    //提示周围相邻节点
    notifyAroundBox1(targetBox: cc.Node) {

        let boxTs = targetBox.getComponent('Box');
        let x = boxTs.x_Index;
        let y = boxTs.y_Index;
        //左边有未绘制相邻节点
        let arr = []
        if (y - 1 >= 0 && (this.m_BoxArray[x][y - 1].getComponent('Box').value == 0 || this.m_BoxArray[x][y - 1].getComponent('Box').value == 2)) {
            // this.m_BoxArray[x][y - 1].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x][y - 1])
        }
        //上边有未绘制相邻节点
        if (x - 1 >= 0 && (this.m_BoxArray[x - 1][y].getComponent('Box').value == 0 || this.m_BoxArray[x - 1][y].getComponent('Box').value == 2)) {
            // this.m_BoxArray[x - 1][y].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x - 1][y])
        }
        //右边有未绘制相邻节点
        if (y + 1 < this.m_ColNum && (this.m_BoxArray[x][y + 1].getComponent('Box').value == 0 || this.m_BoxArray[x][y + 1].getComponent('Box').value == 2)) {
            // this.m_BoxArray[x][y + 1].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x][y + 1])
        }
        //下边有未绘制相邻节点
        if (x + 1 < this.m_RowNum && (this.m_BoxArray[x + 1][y].getComponent('Box').value == 0 || this.m_BoxArray[x + 1][y].getComponent('Box').value == 2)) {
            // this.m_BoxArray[x + 1][y].getComponent('Box').showNotifyAnim();
            arr.push(this.m_BoxArray[x + 1][y])
        }
        return arr;
    }
    //取消提示周围相邻节点
    cancelAroundBox(targetBox: cc.Node) {

        let boxTs = targetBox.getComponent('Box');
        let x = boxTs.x_Index;
        let y = boxTs.y_Index;
        //左边
        if (y - 1 >= 0) {
            this.m_BoxArray[x][y - 1].getComponent('Box').cancelNotifyAnim();
        }
        //上边
        if (x - 1 >= 0) {
            this.m_BoxArray[x - 1][y].getComponent('Box').cancelNotifyAnim();
        }
        //右边
        if (y + 1 < this.m_ColNum) {
            this.m_BoxArray[x][y + 1].getComponent('Box').cancelNotifyAnim();
        }
        //下边
        if (x + 1 < this.m_RowNum) {
            this.m_BoxArray[x + 1][y].getComponent('Box').cancelNotifyAnim();
        }
    }

    resetClick() {
        this.startCountDown = true;
        this.popPathArrayList(this.m_PathArray[0]);
    }

    //点击重玩
    onRestClick() {
        WX.aldSendEvent('闯关赛重玩点击', {
            '关卡': this.currentLevel
        });
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        let mainTs = this.mainCanvas.getComponent('MainControl');
        mainTs.loadVideoAd(mainTs.Reset);
        this.startCountDown = false;

    }
    saveLevelData() {
        this.levelData.currentColor = this.bodyNodeColor;
        this.levelData.perfect = true;
        this.levelData.colNum = this.m_ColNum;
        this.levelData.rowNum = this.m_RowNum;
        this.levelData.itemWidth = this.m_BoxWidth;
        for (let index = 0; index < this.m_PathArray.length; index++) {
            this.levelData.currentPathIndex.push([]);
            this.levelData.currentPathIndex[index][0] = this.m_PathArray[index].getComponent('Box').x_Index;
            this.levelData.currentPathIndex[index][1] = this.m_PathArray[index].getComponent('Box').y_Index;
        }
        this.storageUtil.saveData('levelData' + this.currentLevel, this.levelData);
        // cc.sys.localStorage.setItem('levelData'+this.currentLevel,JSON.stringify(this.levelData));
    }

    //游戏页面入口  isFromAlbum 是否是从相册列表过来的
    private isFromAlbum = false;
    playSelectedLevel(level, isFromAlbum?:boolean) {
        if(isFromAlbum){
            this.isFromAlbum = isFromAlbum;
        }
        this.isLimitGame = false;
        this.countDownLabel.node.active = false;
        this.m_InnerLayout.active = false;
        this.currentLevel = level;
        this.passNextLevel();
    }

    gameBackAction(isFromLimitClose?:boolean){
        this.aniNode.getComponent(ShakeAnimation).stopTimerShake()
        this.refreshGamePage();
        this.mainCanvas.getComponent('MainControl').getVolumeControl().stopMusic();
        let mainTs = cc.find('Canvas').getComponent('MainControl')
        mainTs.hideBanner()
        mainTs.volumeControl.getComponent('AudioController').playClickButtonAudio();
        // ReportMgr.getInst().uploadEvent("click_game_back","page_play");
        if (this.isLimitGame) {
            if(isFromLimitClose){
                this.mainCanvas.getComponent('MainControl').back2LaunchView();
            }else{
                this.limitFail.showDialog(this.currentLevel);
            }
        } else {
            this.back2LevelSelect();
        }
    }

    //返回按钮点击事件
    backBtnClick() {
        this.gameBackAction()
    }

    //返回选关界面
    back2LevelSelect() {
        this.startCountDown = false;
        if(this.isFromAlbum){
            let mainTs = cc.find('Canvas').getComponent(MainControl)
            mainTs.back2ThemeView();
            this.isFromAlbum = false;
            this.m_InnerLayout.active = false;
        }else{
            this.m_InnerLayout.active = true;
            // this.m_InnerLayout.getChildByName('GridLayout').getComponent('InnerLevelControl').loadInnerBoxState();
            this.m_InnerLayout.getChildByName('GridLayout').getComponent('InnerLevelControl').initLevelData(this.theme, this.chapter);
        }

        LocalDataManager.shareManager().saveBackTimes();
        if(LocalDataManager.shareManager().getBackTimes() >= BaseConfig.CloudInfo.click_back_count){
            LocalDataManager.shareManager().resetBackTimes();
            JsBridge.callWithCallback(JsBridge.CLICK_PLAY_BACK,{},()=>{});
        }
    }

    closeLimitFailAlert(){
        this.limitFail.close();
        this.gameBackAction(true);
    }

    update(dt) {
        if (this.isLimitGame) {
            if (this.startCountDown) {
                this.costTime = this.costTime + dt;
                let restTime = Math.ceil(this.globalConst.countDownTime + this.adAddATime - this.costTime);
                if (restTime <= 0) {
                    let info = ReportConfig.ReportMap;
                    info.level = this.currentLevel;
                    info.event = "evt_challenge_fail";
                    info.event_msg = "page_challenge";
                    info.rank = GameChallenge.Instance.myRankIndex;
                    ReportMgr.getInst().uploadConfig(info);
                    // ReportMgr.getInst().uploadEvent("evt_challenge_fail", "page_challenge",undefined,undefined,this.currentLevel);
                    restTime = 0;
                    let mainTs = this.mainCanvas.getComponent('MainControl');
                    if (mainTs.Membership) {
                        this.limitBackDialog.active = false;
                    }
                    this.limitFail.showDialog(this.currentLevel);
                    this.startCountDown = false;
                    this.lastRestTime = this.globalConst.countDownTime;
                }

                if (restTime < 10) {
                    if (restTime != this.lastRestTime) {
                        this.lastRestTime = restTime;
                        this.mainCanvas.getComponent('MainControl').getVolumeControl().playCountDownAudio();
                    }
                    this.countDownLabel.node.color = new cc.Color().fromHEX('#DD0606');
                    this.countDownLabel.string = '00:0' + restTime;
                } else {
                    this.countDownLabel.node.color = new cc.Color().fromHEX('#FFFFFF');
                    this.countDownLabel.string = '00:' + restTime;
                }
            }
        } else {
            if (this.startCountDown) {
                this.costTime = this.costTime + dt;
                if (this.costTime >= this.globalConst.costTimeThre
                    && !this.notifyDialogShow
                    && this.m_TotalItemCount > this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box').value) {
                        //游戏弹窗过关提示 隐藏
                    // this.showNotifyGuaidDialog();
                }
            }

        }
        if (this.startCountDown) {
            this.helpTime = this.helpTime + dt;
            if (Math.floor(this.helpTime) == this.globalConst.helpMe
                && !this.notifyDialogShow
                && this.m_TotalItemCount > this.m_PathArray[this.m_PathArray.length - 1].getComponent('Box').value) {
                this.helpTime = 0;
                cc.log(123321)
                cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playGameHelpAudio();
            }
        }
    }

    //显示结算数据
    showSettleData() {
        let awardPercent = this.currentLevel % this.awardStep;
        this.skipIndex ++;
        let unlockChapterPercent = this.skipIndex;
        console.log('awardPercent = ' + awardPercent);
        //如果是看答案进入
        // if (this.isAnswerGame) {
        //     //是自己的求助正常结算
        //     if (this.requestHelperInfo.uid == this.userData.uid) {
        //         this.settleDialogControl(awardPercent, unlockChapterPercent);
        //     }
        // } else {
            //正常模式的结算
            this.settleDialogControl(awardPercent, unlockChapterPercent);
        // }
    }

    //开始挑战赛
    playLimitGame() {
        this.isLimitGame = true;
        this.currentLevel = 1;
        this.startCountDown = false;
        this.countDownLabel.string = '00:30';
        //更新挑战赛的实际关卡
        this.resetLimitLevelArray();
        this.calculateLimitLevel();
        this.passNextLevel();
        //播放倒计时动画
        this.countDownNodeBg.active = true;
        this.countDownLabel.node.active = true;
        this.countDownNodeBg.zIndex = 999;

        let count = 0;
        this.schedule(function () {
            // 这里的 this 指向 component
            cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playCountDownAudio();

            if (count == 0) {

                this.countDownNode.getComponent(dragonBones.ArmatureDisplay).playAnimation('newAnimation', 1);
                this.scheduleOnce(function () {
                    this.countDownNodeBg.active = false;
                    this.startCountDown = true;
                    cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playGameHelpAudio();
                }.bind(this), 3);
            }
            count++;
        }, 1, 2, 0.01);


        this.scheduleOnce(function () {
            this.countDownNodeBg.active = false;
            WX.aldSendEvent('挑战赛界面曝光');
            this.startCountDown = true;
        }.bind(this), 3);
    }

    private nextLevel = -1;
    calculateLimitLevel() {
        let minLevel = Math.floor(this.currentLevel / this.globalConst.levelCount) + 1;
        let maxLevel = this.globalConst.levelCount + minLevel - 1;
        if (this.nextLevel != -1) {
            this.limitLevel = this.nextLevel;
        } else {
            let randomIndex = Math.round(this.settleUtil.getRandom(0, this.limitLevelArray.length - 1));
            this.limitLevel = this.limitLevelArray[randomIndex];
            this.limitLevelArray.splice(randomIndex, 1);
        }
        if (this.limitLevelArray.length <= 0) {
            this.nextLevel = -1
            this.resetLimitLevelArray();
        } else {
            let randomIndex1 = Math.round(this.settleUtil.getRandom(0, this.limitLevelArray.length - 1));
            this.nextLevel = this.limitLevelArray[randomIndex1];
            this.limitLevelArray.splice(randomIndex1, 1);
        }
    }

    resetLimitLevelArray() {
        // this.limitLevelArray.splice(0, this.limitLevelArray.length);
        let minLevel = Math.floor(this.currentLevel / this.globalConst.levelCount) * this.globalConst.levelCount + 1;
        let maxLevel = this.globalConst.levelCount + minLevel - 1;
        for (let index = minLevel; index <= maxLevel; index++) {
            this.limitLevelArray[index - minLevel] = index;
        }
    }


    drawCatTail(tailNode: cc.Node, orientation: String) {
        var tailWorldVec = this.node.convertToWorldSpaceAR(tailNode.getPosition());
        var tailSpaceVec = this.node.getParent().convertToNodeSpaceAR(tailWorldVec);
    }

    initLimitBg() {
        this.m_LimitBg.active = true;
        // this.m_ChallengeBg.active = false;
        this.m_GoldLayout.active = false;
        this.m_Tool.getComponent('ToolsControl').setLimitMode();
        cc.log(this.limitLevel, this.currentLevel, "this.limitLevel+++++++++++++")
        this.changeTheme(this.limitLevel);

    }

    initGameBg() {
        this.m_LimitBg.active = false;
        this.m_ChallengeBg.active = true;
        this.m_GoldLayout.active = true;
        this.m_Tool.getComponent('ToolsControl').setGameMode();
        this.changeTheme();
    }

    showSettleDialog(awardPercent) {
        let currentLevel = this.currentLevel;
        this.scheduleOnce(function () {
            this.node.opacity = 0;
            this.node.opacity = 0;
            this.node.opacity = 0;
            this.node.opacity = 0;
            this.m_CatFace.runAction(cc.fadeOut(0.2))
            let bg = this.m_WinLayout.getChildByName("DialogBg").getChildByName("Bg")
            // bg.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.m_Bg.node.parent.getChildByName("ChallengeBg").getComponent(cc.Sprite).spriteFrame;
            let bgPath = `${new LevelData().currentUrl}/bg/id_${this.chapter + (this.theme - 1) * this.globalConst.chapterCount}/level_bg_${this.level}.jpg`
            this.LoadNetImg(bgPath,(texture:cc.Texture2D)=>{
                let frame = new cc.SpriteFrame(texture)
                bg.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = frame;
            });
            let s = this.m_WinLayout.height / bg.getChildByName("Sprite").height
            bg.scale = s + 0.2;
            bg.stopAllActions()
            cc.tween(bg)
                .delay(0.2).call(() => {
                    ReportMgr.getInst().uploadEvent("evt_show_passLeve_alert", "page_game", this.theme, this.chapter, this.level)
                    this.m_WinLayout.active = true;
                    this.m_DialogRank.active = true;
                    this.refreshGamePage();
                    bg.runAction(cc.scaleTo(1, 1).easing(cc.easeBackOut()))
                })
                .delay(0).call(() => {
                    // bg.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.m_Bg.node.parent.getChildByName("bg").getComponent(cc.Sprite).spriteFrame;
                    this.LoadNetImg(bgPath,(texture:cc.Texture2D)=>{
                        let frame = new cc.SpriteFrame(texture)
                        bg.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = frame;
                    });
                    this.settilePassAnim.resetSystem();
                })
                .start()
            // this.showSettleShare();
            this.mainCanvas.getComponent('MainControl').getVolumeControl().playGamePassAudio();
        }.bind(this), 0.3);
        WX.aldSendEvent('闯关赛结算曝光', {
            '关卡': this.currentLevel
        });
        this.settleLevelLabel.getComponent(cc.Label).string = i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString())
        this.settleUtil.getOverPlayerPercent(this.costTime, this.m_PathArray.length, function (percent) {
            // this.settlePercentLabel.active = false;
            this.settlePercentLabel.getComponent(cc.Label).string = i18nMgr._getLabel("txt_106").replace("*", this.costTime.toFixed(1) + 's').replace("&", percent)
            // '用时' + this.costTime.toFixed(1) + 's,击败' + percent + '%的对手';
        }.bind(this));
    }

    getAnswerFromHelper() {
        let step = this.m_AnswerPathArray.length - 1;
        if (this.currentStep + step > this.m_AnswerPathArray.length) {
            step = this.m_AnswerPathArray.length - this.currentStep;
        }

        //Pop到当前已经标记的提示位置
        let currentPopData;
        if (this.currentStep == 0) {
            currentPopData = this.m_AnswerPathArray[this.currentStep];
        } else {
            currentPopData = this.m_AnswerPathArray[this.currentStep - 1];
        }

        this.popPathArrayList(this.m_BoxArray[currentPopData[1]][currentPopData[0]]);

        //将各个提示背景显示出来
        for (let index = this.currentStep; index < this.currentStep + step; index++) {
            let indexData = this.m_AnswerPathArray[index];
            let notifyNode = this.m_BoxArray[indexData[1]][indexData[0]];
            //计算方向
            let orientation = "Bottom";
            if (index > 0) {
                let preIndexData = this.m_AnswerPathArray[index - 1];
                let verticalOri = indexData[1] - preIndexData[1];
                let horizOri = indexData[0] - preIndexData[0];
                if (verticalOri < 0) {
                    orientation = "Top";
                }

                if (verticalOri > 0) {
                    orientation = "Bottom";
                }

                if (verticalOri == 0) {
                    if (horizOri < 0) {
                        orientation = "Left";
                    }

                    if (horizOri > 0) {
                        orientation = "Right";
                    }
                }
                notifyNode.getComponent('Box').notifyClicked(orientation);
            }
        }
        this.currentStep = this.currentStep + step;
    }

    gainRevive() {
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        this.limitFail.close();
        this.startCountDown = true;
        this.costTime = 0;
        this.adAddATime = 0;
    }
    
    //更换对应主题
    changeTheme(lv?) {
        this.theme = Math.ceil(this.currentLevel / this.globalConst.chapterCount / this.globalConst.levelCount);
        let chapterTemp = this.currentLevel - (this.theme - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
        this.chapter = Math.ceil(chapterTemp / this.globalConst.levelCount);
        let level = chapterTemp % this.globalConst.levelCount || 20;
        let theme, chapter
        if (lv) {
            theme = Math.ceil(lv / this.globalConst.chapterCount / this.globalConst.levelCount);
            let chapterTemp = lv - (theme - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
            chapter = Math.ceil(chapterTemp / this.globalConst.levelCount);
            level = chapterTemp % this.globalConst.levelCount || 20;
            this.level = level
            this.theme = theme
            this.chapter = chapter
        } else {
            theme = this.theme
            chapter = this.chapter
            this.level = level;
            if (this.currentLevel > this.globalConst.themeCount * this.globalConst.chapterCount * this.globalConst.levelCount) {
                this.levelUtils.plsWaitUpdate();
                return;
            }
        }
        let bgPath = `${new LevelData().currentUrl}/bg/id_${this.chapter + (this.theme - 1) * this.globalConst.chapterCount}/level_bg_${this.level}.jpg`
        console.log("图片地址:" + bgPath);
        let bg = this.m_Bg.node.parent.getChildByName("bg")
        if (this.currentLevel != bg['level'] && !this.isLimitGame) {
            this.m_Bg.node.getComponent(cc.Sprite).spriteFrame = this.BgSprite;
        }
        bg['level'] = this.currentLevel
        this.LoadNetImg(bgPath, (teture) => {
            if (this.currentLevel != mask['level']) return;
            let frame = new cc.SpriteFrame(teture)
            cc.loader.setAutoReleaseRecursively(frame, true);
            this.m_Bg.node.parent.getChildByName("bg").getComponent(cc.Sprite).spriteFrame = frame;
            this.m_Bg.node.getComponent(cc.Widget).updateAlignment();
            this.m_Bg.node.width = this.m_Bg.node.height / teture.height * teture.width
            this.m_Bg.getComponent(DualBlur).init()
        })
        cc.log(bgPath)
        this.scheduleOnce(() => {
            let bgPath1 = ""
            let facePath1 = ""
            if (lv) {
                if (this.nextLevel > -1) {
                    let theme1 = Math.ceil(this.nextLevel / this.globalConst.chapterCount / this.globalConst.levelCount);
                    let chapterTemp1 = this.nextLevel - (theme1 - 1) * (this.globalConst.chapterCount * this.globalConst.levelCount);
                    let chapter1 = Math.ceil(chapterTemp1 / this.globalConst.levelCount);
                    let level1 = chapterTemp1 % this.globalConst.levelCount || 20;
                    if (this.level == 20) {
                        facePath1 = `${new LevelData().currentUrl}/icons/id_${chapter1 + (theme1) * this.globalConst.chapterCount}/level_icon_${1}.jpg`
                        bgPath1 = `${new LevelData().currentUrl}/bg/id_${chapter1 + (theme1) * this.globalConst.chapterCount}/level_bg_${1}.jpg`
                    } else {
                        facePath1 = `${new LevelData().currentUrl}/icons/id_${chapter1 + (theme1 - 1) * this.globalConst.chapterCount}/level_icon_${level1}.jpg`
                        bgPath1 = `${new LevelData().currentUrl}/bg/id_${chapter1 + (theme1 - 1) * this.globalConst.chapterCount}/level_bg_${level1}.jpg`
                    }
                }
            } else {
                if (this.level == 20) {
                    let th = (1 + this.chapter + (this.theme - 1) * this.globalConst.chapterCount) > 54 ? 1 : (1 + this.chapter + (this.theme - 1) * this.globalConst.chapterCount)
                    facePath1 = `${new LevelData().currentUrl}/icons/id_${th}/level_icon_${1}.jpg`
                    bgPath1 = `${new LevelData().currentUrl}/bg/id_${th}/level_bg_${1}.jpg`
                } else {
                    facePath1 = `${new LevelData().currentUrl}/icons/id_${this.chapter + (this.theme - 1) * this.globalConst.chapterCount}/level_icon_${this.level + 1}.jpg`
                    bgPath1 = `${new LevelData().currentUrl}/bg/id_${this.chapter + (this.theme - 1) * this.globalConst.chapterCount}/level_bg_${this.level + 1}.jpg`
                }
            }
            cc.log(bgPath1, "bgPath1++++++++++")
            if (bgPath1 != "") {
                this.LoadNetImg(bgPath1, (teture) => {
                })
                this.LoadNetImg(facePath1, (teture) => {
                })
            }
        }, 0.5)
        this.levelUtils.loadThemeJson(function (json) {
            console.log('game theme json', json);
            this.bodyNodeColor = json[theme].body;
            if (typeof (json[theme].bottom) == 'string') {
                this.m_ToolBg.color = new cc.Color().fromHEX(json[theme].bottom);
            } else {
                this.m_ToolBg.color = new cc.Color().fromHEX(json[theme].bottom[chapter - 1]);
            }
        }.bind(this));

        let facePath = `${new LevelData().currentUrl}/icons/id_${this.chapter + (this.theme - 1) * this.globalConst.chapterCount}/level_icon_${level}.jpg`

        let mask = this.m_CatFace.getChildByName("mask")
        if (this.currentLevel != mask['level'] && !this.isLimitGame) {
            this.m_CatFace.getChildByName("mask").getChildByName("img").getComponent(cc.Sprite).spriteFrame = null;
        }
        mask['level'] = this.currentLevel
        this.LoadNetImg(facePath, (teture) => {
            if (this.currentLevel != mask['level']) return;
            let frame = new cc.SpriteFrame(teture)
            cc.loader.setAutoReleaseRecursively(frame, true);
            this.m_CatFace.getChildByName("mask").active = true;
            this.m_CatFace.getChildByName("mask").getChildByName("img").getComponent(cc.Sprite).spriteFrame = frame;
        })
    }

    onAddCoinsClick() {
        WX.aldSendEvent('闯关赛添加猫币点击', {
            '关卡': this.currentLevel
        });
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();
        var myDate = new Date();
        let currentTime = myDate.getTime();
        this.storageUtil.saveData("shareTime", currentTime);
    }

    closeAllDialog() {
        this.limitFail.close();
    }

    //显示解锁chapter
    showUnlockChapter() {
        let mainTs = cc.find('Canvas').getComponent(MainControl) ;
        mainTs.getVolumeControl().playGamePassAudio();
        this.userData.gameLevel = this.currentLevel + 1;
        this.storageUtil.saveData('userData', this.userData);
        this.storageUtil.saveData('unLockLevel', this.currentLevel + 1);
        // mainTs.back2ThemeView();
        // mainTs.closeAlbum();

        let unlockLevel = this.currentLevel + 1;
        let theme = Math.ceil(unlockLevel / this.globalConst.levelCount);
        // let chapterTemp = unlockLevel % (this.globalConst.chapterCount * this.globalConst.levelCount);
        // let chapter = Math.ceil(chapterTemp / this.globalConst.levelCount);
        this.nextUnlokTheme  = theme;
        this.nextUnlokLevel = unlockLevel;
        mainTs.loadInterAd(mainTs.UnlockNextThemeLevel);

        // this.mainCanvas.getComponent('MainControl').themeView.getComponent('ThemeViewControl').showUnlockView(theme, unlockLevel);
    }

    successLockThemeLevel(){
        //广告成功后 1、刷新跳转当前游戏页到对应官卡 2、刷新上一级选换卡数据页 3、刷新首页主题对应解锁的主题
        this.currentLevel ++;
        this.passNextLevel();
        this.storageUtil.saveData('unLockLevel' + this.nextUnlokLevel, 1);
        this.storageUtil.saveData('unLockThemeMaxLevel' + this.nextUnlokTheme, this.nextUnlokLevel);
        this.storageUtil.saveData('unLockTheme' + this.nextUnlokTheme, true);
        // 刷新解锁主题
        let themeView = this.mainCanvas.getComponent('MainControl').themeView.getComponent(ThemeViewControl)
        themeView.refreshTheme();
    }


    //非看答案的结算方式
    settleDialogControl(awardPercent, unlockChapterPercent) {
        cc.log(unlockChapterPercent, "unlockChapterPercent")
        let mainTs = this.mainCanvas.getComponent('MainControl');
        let levelData = this.storageUtil.getData('unLockLevel' + (this.currentLevel + 1));
        cc.log(unlockChapterPercent % 5, "unlockChapterPercent % 5 ", levelData, this.currentLevel + 1)
        let result = unlockChapterPercent % (BaseConfig.CloudInfo.nextcount + 1)
        if (result != 0) {
            if (!levelData) {
                this.storageUtil.saveData('unLockLevel' + (this.currentLevel + 1), 1);
                let theme = Math.ceil(this.currentLevel / this.globalConst.levelCount)
                this.storageUtil.saveData('unLockThemeMaxLevel' + theme, this.currentLevel + 1);
            }
            this.m_PressureProgress.active = false
        } else {
            this.m_PressureProgress.active = true;
            this.mainCanvas.getComponent('MainControl').themeView.getComponent('ThemeViewControl').unlockLevel = this.currentLevel + 1;
            console.log("当前过关：" + this.currentLevel + "下一关" + (this.currentLevel + 1))
        }
        this.showSettleDialog(awardPercent);
    }

    //显示引导动画
    showGuideAnim() {
        this.storageUtil.saveData('guaidLevel' + this.currentLevel, true);
        this.fingerGuide.active = true;
        this.fingerGuide.opacity = 255;
        this.fingerGuide.zIndex = this.m_CatFace.zIndex + 1;
        let actions = new Array<cc.ActionInterval>();
        this.fingerGuide.stopAllActions();
        for (let index = 0; index < 2; index++) {
            for (let i = 0; i < this.m_AnswerPathArray.length; i++) {
                let indexData = this.m_AnswerPathArray[i];
                let answerNode = this.m_BoxArray[indexData[1]][indexData[0]];
                if (i == 0) {
                    if (index != 0) {
                        actions.push(cc.sequence(cc.moveTo(0.1, cc.v2(answerNode.position)), cc.fadeIn(0.3)));
                    } else {
                        this.fingerGuide.setPosition(cc.v2(answerNode.position));
                    }
                }

                if (i < this.m_AnswerPathArray.length - 1) {
                    let indexNextData = this.m_AnswerPathArray[i + 1];
                    let answerNextNode = this.m_BoxArray[indexNextData[1]][indexNextData[0]];
                    let action = cc.moveTo(0.4, cc.v2(answerNextNode.position));
                    // let sequence = cc.sequence(delayTime, action);
                    actions.push(action);
                }
            }
            actions.push(cc.fadeOut(0.4));
        }
        let repeatAction = cc.repeat(cc.sequence(actions), 1);
        this.fingerGuide.runAction(repeatAction);
    }


    needGuideView() {
        let guaidLevel = this.storageUtil.getData('guaidLevel' + this.currentLevel);
        if ((!guaidLevel || guaidLevel == null) && this.currentLevel <= 3) {
            return true;
        } else {
            return false;
        }
    }

    //超过时间弹出提示引导框
    showNotifyGuaidDialog() {
        this.notifyDialogShow = true;

        if (this.notifyDialogPref == null) {
            cc.loader.loadRes('pfb/' + 'NotifyLayout', function (error, res) {
                this.notifyDialogPref = res;
                this.notifyDialogNode = cc.instantiate(this.notifyDialogPref);
                this.node.getParent().addChild(this.notifyDialogNode);
                this.notifyDialogNode.getComponent('NotifyLayout').setGameControl(this);
                this.notifyDialogNode.getComponent('NotifyLayout').show(this.theme,this.chapter,this.level);
                this.notifyDialogNode.zIndex = this.m_CatFace.zIndex + 1;
            }.bind(this))
        } else {
            this.notifyDialogNode.getComponent('NotifyLayout').show(this.theme,this.chapter,this.level);
            this.notifyDialogNode.zIndex = this.m_CatFace.zIndex + 1;
        }
    }

    //提示弹框提示点击
    onNotifyVideoClick() {
        let mainTs = this.mainCanvas.getComponent('MainControl');
        mainTs.loadVideoAd(mainTs.NotifyDialog);
    }

    //提示弹框，提示2次
    onDialogNotifyClick() {
        this.notifyDialogNode.getComponent('NotifyLayout').close();
        this.onNotifyClick(this.notifyStep * 2);
    }

    showSettleShare() {
        this.settlePopupPriorityArray.splice(0, this.settlePopupPriorityArray.length);
        // if (this.shareLabelIndex == this.storageUtil.settleShareLabel.length) {
        //     this.shareLabelIndex = 1;
        // }
        let index = this.shareLabelIndex % (this.storageUtil.settleShareLabel.length - 1) + 1;
        console.log('this.shareLabelIndex =', this.shareLabelIndex, this.storageUtil.settleShareLabel);

        this.gamePageNode.active = false;
        switch (index) {

            case 4:
                this.settlePopupPriorityArray.push(this.storageUtil.TURNTABLE);


                break;
            default:
                break;
        }
        this.m_WinLayout.getChildByName("DialogBg").getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.m_Bg.node.parent.getChildByName("bg").getComponent(cc.Sprite).spriteFrame;

        if (this.shareLabelIndex % 4 == 0 && this.shareLabelIndex > 0) {
            this.settlePopupPriorityArray.push(this.storageUtil.MOREGAMEPAGE);
        }

        //前20关3关一个插屏，后面4关一个视频
        if (index % 3 == 0 && this.shareLabelIndex > 0) {
            this.settlePopupPriorityArray.push(this.storageUtil.INTERSTITIAL);
        }
        this.videoNextBtn.active = true;
        this.showPopSettle();
        this.shareLabelIndex++;

    }

    //结算界面弹框优先级逻辑
    showPopSettle() {
        let priority = 99;
        for (let i = 0; i < this.settlePopupPriorityArray.length; i++) {
            if (this.settlePopupPriorityArray[i] < priority) {
                priority = this.settlePopupPriorityArray[i];
            }
        }
        return;
    }

    onShareBtnClick() {
        let index = (this.shareLabelIndex - 1) % (this.storageUtil.settleShareLabel.length - 1) + 1;
        switch (index) {
            case 0:
                this.onPassNextLevelClick();
                break;
        }
    }

    onVideoNextClick() {
        if (this.videoNextBtn.active == true) {
            WX.aldSendEvent('闯关赛下一关视频点击', {
                '关卡': this.currentLevel
            });
            let mainTs = this.mainCanvas.getComponent('MainControl');
            mainTs.loadInterAd(mainTs.NextLevel);
        }
    }

    refreshGamePage(){
        JsBridge.callWithCallback(JsBridge.REFRESH_GAME_PAGE,{},(json)=>{});
    }

    addTimeClick(){
        let mainTs = cc.find('Canvas').getComponent(MainControl);
        mainTs.loadVideoAd(mainTs.ChallengeAddTime);

        let info = ReportConfig.ReportMap;
        info.level = this.currentLevel;
        info.event = "click_overtime";
        info.event_msg = "page_challenge";
        info.page = "page_ad_page_challenge_click_overtime_rewarded";
        ReportMgr.getInst().uploadConfig(info);
    }

    successAdAddTime(){
        this.adAddATime += 30;
    }

    showRankView(){
        let info = ReportConfig.ReportMap;
        info.level = this.currentLevel;
        info.event = "click_leaderboard";
        info.event_msg = "page_challenge";
        info.level = this.currentLevel;
        info.rank = GameChallenge.Instance.myRankIndex;
        ReportMgr.getInst().uploadConfig(info);

        this.refreshGamePage();

        cc.loader.loadRes('pfb/ChallengeRank',(err,asset:cc.Prefab)=>{
            if(asset){
                let node = cc.instantiate(asset);
                let rank = node.getComponent(ChallengeRank)
                rank.show(this.node.getParent());
            }
        })
    }

}
