

const { ccclass, property } = cc._decorator;

@ccclass
export default class UserData {

    uid: string = '';
    //金币
    gold: number = 0;
    //当前关卡
    gameLevel: number = 1;
    //挑战赛关卡
    limitLevel: number = 0;

    // //是否新用户
    // firstLaunch:boolean = true;

}
