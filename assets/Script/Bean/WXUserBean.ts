

const {ccclass, property} = cc._decorator;

@ccclass
export default class WXUserBean{

    nickName: string = null;
 
    uid: string = null;
 
    avatarUrl: string = null;
 
    gameLevel: Number = 0;

    openID:string=null;
    
}
