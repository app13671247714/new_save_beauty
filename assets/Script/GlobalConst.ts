

const { ccclass, property } = cc._decorator;

@ccclass
export default class GlobalConst {

    //主题个数
    themeCount = 6;
    //一个主题9只猫
    chapterCount = 9;
    //一只猫20关
    levelCount = 20;

    //倒计时时间
    countDownTime = 30;

    //奖励倒计时时间
    awardTime = 15;

    //获得奖励的金币数
    awardCoin = 100;

    //双倍领取关卡步长
    doubleGetAwardStep = 5;

    //过关领取金币金额
    doubleGetAward = 50;

    //过关领取金币金额1
    doubleGetAward1 = 100;

    //提示消耗的金币
    notifyCost = 80;

    //解锁主题消耗金币
    unlockChapterCost = 200;

    //分享答案领取金币
    shareAnswerCoin = 100;

    //邀请单个人奖励金币
    inviteCoin = 50;

    //每日登陆奖励
    dayAwardCoin = 100;

    //点击添加猫币奖励 
    adddCoinClick = 50;


    //超神时刻猫币奖励 
    addCoinGod = 200;

    //单关游戏消耗时间大于X秒弹出提示引导框
    costTimeThre: number = 30;
    //单关游戏消耗时间大于X秒弹出提示引导框
    helpMe: number = 5;
}
