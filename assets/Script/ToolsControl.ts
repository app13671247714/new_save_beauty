// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import EventManager from "./Common/LocalEvents";
import { EventConfig } from "./Framework/Config/EventConfig";
import List from "./scrollViewTools/List";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ToolsControl extends cc.Component {

    //增加时间按钮
    @property(cc.Node)
    addTime:cc.Node = null;

    @property(cc.Node)
    rankButton:cc.Node = null;

    @property(cc.Label)
    rankLabel:cc.Label = null;

    @property(cc.Node)
    rankNode:cc.Node = null;

    @property(cc.Node)
    noRank:cc.Node = null;

    currentIndex: number = 0;

    @property(List)
    scrollList:List = null;

    onLoad () {
        EventManager.on(EventConfig.CHALLENGE_UPDATE_RANK,this.updateUI,this);
    }

    protected start(): void {
        this.scrollList.numItems = 50;

        // //缩放适配
        // let widthS =  750 / cc.view.getCanvasSize().width;
        // let heightS = 1334 / cc.view.getCanvasSize().height;
        // let srcScaleForShowAll = Math.min(
        //     widthS,heightS
        // );
        // this.rankButton.width =  334 * srcScaleForShowAll;
        // this.rankButton.height = 96 * srcScaleForShowAll;

        // this.rankButton.scaleX = srcScaleForShowAll;
        // this.rankButton.scaleY = srcScaleForShowAll;
        // console.log(this.rankButton.width);
    }d

    updateUI(event){
        console.log(event)
        if(event > 0){
            this.rankLabel.string = event.toString()
            if(this.currentIndex > 0){
                this.scrollList.scrollTo(event > 25 ? 0 : 49);
            }
            this.scheduleOnce(()=>{
                this.scrollList.scrollTo(event - 1);
            },0.2)

            this.currentIndex = event;
            this.rankNode.active = true;
            this.noRank.active = false;
        }else{
            this.currentIndex = 0;
            this.rankNode.active = false;
            this.noRank.active = true;
        }
    }

    //挑战赛
    setLimitMode(){
        this.addTime.active = true;
        this.rankButton.active = true;
    }

    //九宫格
    setGameMode(){
        this.addTime.active = false;
        this.rankButton.active = false;
    }

    // 从 startValue 翻转到 endValue
    flipToValue(label:cc.Label, startValue:number, endValue:number) {
        if(startValue == endValue) return
        let add = -1;
        if(startValue < endValue){
            add = 1;
        }
        let currentValue = startValue;
        label.string = currentValue.toString();
        let flipAndIncrement = function() {
            let rotateAction = cc.rotate3DBy(0.8,0,0, 360); // 在0.5秒内沿 Z 轴旋转180度
            let func = cc.callFunc(()=>{
                currentValue += add
                this.label.string = currentValue.toString();
                if(currentValue != endValue){
                    flipAndIncrement();
                }
            });
            let sequenceAction = cc.sequence(rotateAction,func);
            this.node.runAction(sequenceAction);
        };
        flipAndIncrement();
    }


    onListRender(item: cc.Node, idx: number){
        let label = item.getChildByName("label").getComponent(cc.Label)
        label.string = (idx + 1).toString()
    }

}
