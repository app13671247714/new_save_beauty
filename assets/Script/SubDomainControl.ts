import LevelUtils from "./LevelUtils";


const { ccclass, property } = cc._decorator;
const WX = window["wx"];
@ccclass
export default class SubDomainControl extends cc.Component {

    levelUtil: LevelUtils = new LevelUtils();
    bottomTheme: number = 1;
    bottomChapter: number = 1;

    removeSubView(subNode: cc.Node, dialogNode: cc.Node, showType) {





        // //remove时提高帧率
        // if(subNode != null){
        //     subNode.getComponent(cc.WXSubContextView).fps = 60;
        //     subNode.getComponent(cc.WXSubContextView).enabled = true;
        //     subNode.getComponent(cc.WXSubContextView).update();
        // }

        // if(cc.sys.platform == cc.sys.WECHAT_GAME) {
        //     WX.getOpenDataContext().postMessage({
        //         message: "closeAll",
        //     });
        // }

        // subNode.getComponent('SubContextView').clearRender();
        if (dialogNode != null) {
            dialogNode.opacity = 0;
            let block = dialogNode.getComponent(cc.BlockInputEvents);
            if (block != null) {
                block.enabled = false;
            }
        }

        this.scheduleOnce(function () {

            // node.getComponent(cc.WXSubContextView).updateSubContextViewport()      
            if (dialogNode != null) {
                dialogNode.opacity = 255;
                dialogNode.active = false;
            }

            if (subNode != null) {
                // subNode.getComponent(cc.WXSubContextView).fps = 24;
                subNode.active = false;

            }

        }.bind(this), 0.5);
    }

    showSubView(subNode: cc.Node, showType, dialogNode: cc.Node) {
        subNode.opacity = 0;
        subNode.active = true;
        if (dialogNode != null) {
            dialogNode.active = true;
            // subNode.getComponent(cc.WXSubContextView).enabled = false;
        }



        this.scheduleOnce(function () {

            // if(showType == 2){
            //     subNode.getComponent(cc.WXSubContextView).fps = 24;

            // }else{
            // subNode.getComponent(cc.WXSubContextView).fps = 60;
            // }

            // subNode.active = true;
            console.log('Subnode show width = ' + subNode.width);
            console.log('Subnode show height = ' + subNode.height);
            // if(cc.sys.platform == cc.sys.WECHAT_GAME) {

            //     WX.getOpenDataContext().postMessage({
            //         message: "message",
            //         theme: this.bottomTheme,
            //         chapter:this.bottomChapter,
            //         type: showType,
            //         width: subNode.width,
            //         height: subNode.height
            //     });

            subNode.opacity = 255;
            // this.scheduleOnce(function () {
            //     subNode.opacity = 255;
            // },2);

            // this.scheduleOnce(function () {

            //     if (showType == 2) {
            //         console.log('subnode subcontext enable false ');
            //         subNode.getComponent(cc.WXSubContextView).enabled = false;
            //         subNode.getComponent(cc.WXSubContextView).update();
            //     }
            // }, 2);
            // }

        }.bind(this), 2);

        if (dialogNode != null) {
            let block = dialogNode.getComponent(cc.BlockInputEvents);
            if (block != null) {
                block.enabled = true;
            }
        }
    }

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    // update (dt) {}

    setBottomTheme(theme, chapter) {
        this.bottomTheme = theme;
        this.bottomChapter = chapter;
    }
}
