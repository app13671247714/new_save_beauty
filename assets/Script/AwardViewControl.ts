
const { ccclass, property } = cc._decorator;
import GlobalConst from './GlobalConst';
import PoolMng from './PoolMng';
import SettleUtil from './SettleUtil';
import StorageUtil from './Common/StorageUtil'
import UserData from './Bean/UserData';
import WXCommon from './Common/WXCommon';
const WX = window["wx"];

@ccclass
export default class AwardViewControl extends cc.Component {

    @property(cc.Node)
    countDownAnim: cc.Node = null;

    @property(cc.Label)
    countDownTime: cc.Label = null;

    //金币的背景节点
    @property(cc.Node)
    coinsView: cc.Node = null;

    @property(cc.Node)
    settleDialog: cc.Node = null;

    @property(cc.Label)
    gainedCoinsLabel: cc.Label = null;

    @property(cc.Node)
    gameView: cc.Node = null;

    @property(cc.Node)
    startBtn: cc.Node = null;

    @property(cc.Node)
    startView: cc.Node = null;

    @property(cc.Node)
    mainView: cc.Node = null;

    //金币动画的目标Node
    @property(cc.Node)
    coinTargetNode: cc.Node = null;

    @property(cc.Node)
    doubleGainToggle: cc.Node = null;

    @property(cc.Node)
    notifyLabel: cc.Node = null;

    @property(cc.Node)
    countDownPlayView: cc.Node = null;

    @property(cc.Node)
    checkBoxNode: cc.Node = null;

    @property(cc.Node)
    gainCoinBtn: cc.Node = null;

    speed: number = 600;
    //金币数组
    coinsArray: cc.Node[] = [];

    globalConsts: GlobalConst = new GlobalConst();
    poolMng: PoolMng = null;
    settleUtil: SettleUtil = new SettleUtil();

    //是否开始倒计时
    startCountDown: boolean = false;

    //消耗的时间
    costTime: number = 0;

    //单次得分
    score: number = 0;

    //产生的金币数
    coinsNum: number = 60;

    storageUtil: StorageUtil = new StorageUtil();

    //双倍领取勾选状态
    doubleCheckBoxChecked: boolean = true;

    wxCommon: WXCommon = new WXCommon();

    scheduleId: number = 0;

    isHide: boolean = false;

    // bannerShowing:boolean = false;
    // bannerHeight:number = 0;


    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.poolMng = this.node.getComponent('PoolMng');
        this.poolMng.init();

        cc.game.on(cc.game.EVENT_HIDE, (function (res) {
            this.isHide = true;
        }.bind(this)));

        cc.game.on(cc.game.EVENT_SHOW, (function (res) {
            this.isHide = false;
        }.bind(this)));
    }

    showStartView() {
        WX.aldSendEvent('闯关赛奖励引导界面曝光');
        this.node.zIndex = 999;
        this.node.setPosition(0, 0);
        this.node.active = true;


        this.countDownPlayView.active = true;

        this.notifyLabel.active = false;
        this.countDownAnim.active = false;
        this.startView.active = true;
        this.countDownTime.string = '00:' + this.globalConsts.awardTime;
        this.countDownTime.node.color = new cc.Color().fromHEX('#FFFFFF');

        this.gameView.getComponent('GameControl').startCountDown = false;
    }

    startAwardFromRedPack() {
        this.node.zIndex = 999;
        this.node.setPosition(0, 0);
        this.node.active = true;


        this.countDownPlayView.active = true;
        this.node.active = true;
        this.notifyLabel.active = false;
        this.countDownAnim.active = false;
        this.startView.active = false;
        this.countDownTime.string = '00:' + this.globalConsts.awardTime;
        this.countDownTime.node.color = new cc.Color().fromHEX('#FFFFFF');

        this.gameView.getComponent('GameControl').startCountDown = false;
        this.playAwardGame();
    }

    // update (dt) {}
    playAwardGame() {
        WX.aldSendEvent('闯关赛奖励引导界面开始点击');
        this.notifyLabel.active = true;
        let date = new Date().getTime();
        this.storageUtil.saveData('awardTime', date);
        cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playClickButtonAudio();

        this.node.active = true;

        this.startView.active = false;

        let count = 0;


        this.schedule(function () {
            // 这里的 this 指向 component
            cc.find('Canvas').getComponent('MainControl').volumeControl.getComponent('AudioController').playCountDownAudio();

            if (count == 0) {
                this.countDownAnim.active = true;
                this.countDownAnim.getComponent(dragonBones.ArmatureDisplay).playAnimation('newAnimation', 1);
                this.scheduleOnce(function () {
                    this.score = 0;
                    this.countDownAnim.active = false;
                    this.startCountDown = true;
                    this.settleDialog.active = false;
                    this.costTime = 0;

                    // this.playAwardGame();
                    this.generateCoins();
                }.bind(this), 3);
            }
            count++;
        }, 1, 2, 0.01);
        //播放倒计时动画


    }

    update(dt) {
        if (this.startCountDown) {
            this.costTime = this.costTime + dt;
            let restTime = Math.ceil(this.globalConsts.awardTime - this.costTime);
            if (restTime < 10) {
                this.countDownTime.node.color = new cc.Color().fromHEX('#DD0606');
                this.countDownTime.string = '00:0' + restTime;
            } else {
                this.countDownTime.node.color = new cc.Color().fromHEX('#FFFFFF');
                this.countDownTime.string = '00:' + restTime;
            }

            if (restTime <= 0) {
                this.startCountDown = false;

                this.notifyLabel.active = false;
                this.doubleGainToggle.getComponent(cc.Toggle).isChecked = true;

                this.doubleGainToggle.getComponent(cc.Toggle).check();

                this.countDownPlayView.active = false;
                this.settleDialog.active = true;
                WX.aldSendEvent('闯关赛奖励结算曝光', {
                    '获得猫币数量': this.score
                });
                cc.find('Canvas').getComponent('MainControl').getVolumeControl().playDialogShowAudio();


                let mainTs = this.mainView.getComponent('MainControl');
                this.resetBannerPos();
                mainTs.showBanner(mainTs.AwardBanner, function (bannerHeight) {

                    this.moveBanner(bannerHeight);
                }.bind(this));

                this.checkBoxNode.opacity = 0;
                let action = cc.fadeIn(1);
                let delay = cc.delayTime(2);
                this.checkBoxNode.runAction(cc.sequence(delay, action));


                this.gainedCoinsLabel.string = this.score + '';
                while (this.coinsArray.length > 0) {
                    this.removeCoin(this.coinsArray[0]);
                }

                // this.unschedule(this.generateCoin);
                clearInterval(this.scheduleId);


            }

            for (let index = 0; index < this.coinsArray.length; index++) {

                if (this.coinsArray[index].getComponent('AwardCoin').isClicked()) {
                    continue;
                }


                this.coinsArray[index].y = this.coinsArray[index].y - dt * this.speed;
                if (this.coinsArray[index].y < -this.coinsView.height / 2 - 20) {
                    this.removeCoin(this.coinsArray[index]);
                }

            }
        }

    }

    generateCoins() {
        // 以秒为单位的时间间隔
        var interval = 0.5;
        // 重复次数
        var repeat = this.coinsNum;
        // 开始延时
        var delay = 0.01;
        this.scheduleId = setInterval(this.generateCoin.bind(this), 500);
        // this.schedule(this.generateCoin.bind(this), interval, repeat, delay);
    }

    generateCoin() {
        if (this.startCountDown && !this.isHide) {
            let coin: cc.Node = this.poolMng.spawnCoin();
            let xPos = this.settleUtil.getRandom(-this.coinsView.width / 2 + coin.width / 2, this.coinsView.width / 2 - coin.width / 2);
            let yPos = this.coinsView.height / 2 + 20;
            coin.active = true;
            coin.x = xPos;
            coin.y = yPos;
            coin.getComponent('AwardCoin').setAwardControl(this);
            this.coinsArray.push(coin);
            this.coinsView.addChild(coin);

        }
    }

    removeCoin(coinItem) {
        coinItem.active = false;
        let index = this.coinsArray.indexOf(coinItem);
        this.coinsArray.splice(index, 1);
        this.poolMng.despwanCoin(0, coinItem);
        this.coinsView.removeChild(coinItem);
    }

    addScore(scoreNum) {
        this.score = scoreNum + this.score;
    }

    //点击领取金币
    onGetAwardClick() {
        this.gainAwardCoins();
    }

    gainAwardCoins() {


        this.mainView.getComponent('MainControl').hideBanner();
        var myDate = new Date();
        let currentTime = myDate.getTime();
        this.storageUtil.saveData('awardClick', currentTime);
        this.gameView.getComponent('GameControl').scheduleAwardBtn();


        let userData = this.storageUtil.getData('userData');
        if (userData == null) {
            userData = new UserData();
        }
        if (this.doubleCheckBoxChecked) {
            userData.gold += this.score * 2;
        } else {
            userData.gold += this.score;
        }

        let targetWordPos = this.coinTargetNode.parent.convertToWorldSpaceAR(this.coinTargetNode.getPosition());
        let targetPos = this.node.convertToNodeSpaceAR(targetWordPos);
        this.storageUtil.createGoldAnim(cc.v2(0, 0), targetPos, 150, 20, true, this.coinTargetNode, this.mainView, this.poolMng);



        this.mainView.getComponent('MainControl').getVolumeControl().playGetCoinsAudio();

        this.storageUtil.saveData('userData', userData);

        this.settleDialog.active = false;
        this.node.active = false;

        this.gameView.getComponent('GameControl').startCountDown = true;

        this.gameView.getComponent('GameControl').updateCoinsLabel(userData.gold + '');
        this.doubleCheckBoxChecked = true;
    }

    setDoubleAwardState(toggle: cc.Toggle) {

        this.doubleCheckBoxChecked = toggle.isChecked;
    }

    resetBannerPos() {
        // this.gainCoinBtn.y =cc.winSize.height *this.gainCoinBtn.getComponent(cc.Widget).bottom -cc.winSize.height/2; 
    }

    moveBanner(bannerHeight) {




        // let action = cc.moveTo(0.2,cc.v2(0, this.gainCoinBtn.y + bannerHeight * 0.8));
        // this.gainCoinBtn.runAction(action);
    }
}
