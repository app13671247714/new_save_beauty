
const { ccclass, property } = cc._decorator;

@ccclass
export default class gameFit extends cc.Component {

    onLoad() {
        let widthS = cc.view.getCanvasSize().width / this.node.width;
        let heightS = cc.view.getCanvasSize().height / this.node.height
        let srcScaleForShowAll = Math.min(
            cc.view.getCanvasSize().width / this.node.width,
            cc.view.getCanvasSize().height / this.node.height
        );
        if (widthS < heightS) {
            let realWidth = this.node.width * srcScaleForShowAll;
            let realHeight = this.node.height * srcScaleForShowAll;
            this.node.width = this.node.width *
                (cc.view.getCanvasSize().width / realWidth);
            this.node.height = this.node.height *
                (cc.view.getCanvasSize().height / realHeight);
        } else {
            let realHeight = this.node.height * heightS;
            this.node.width = this.node.width *
                (cc.view.getCanvasSize().height / realHeight);
            this.node.height = this.node.height *
                (cc.view.getCanvasSize().height / realHeight);
        }
        // cc.winSize.height = this.node.height
        // cc.winSize.width = this.node.width
    }
}
