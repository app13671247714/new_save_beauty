
/**
 * author: Li_Cheng
 *
 * 工具 -- 网络请求类
 */


var SkiNetwork = {

    getRequest(url, params, callback) {
        // 发送 code
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = (function (res) {
            if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 300)) {
                let jsonData = JSON.parse(xhr.responseText);

                // if (jsonData.code != 0) 
                // {
                //     if (callback.failure())
                //     {
                //         callback.failure(jsonData);
                //     }
                // } 
                // else 
                // {
                if (callback.success) {
                    callback.success(jsonData);
                }
                // }
                // }

            }
        }).bind(this);

        xhr.onerror = (function (res) {
            if (callback.failure()) {
                callback.failure(res);
            }
        });

        //验证微信临时登录凭证code
        // if (url == GameConfig.USER_CHECK_CODE) {
        //     if (params == null) {
        //         params = 'game_id=' + GameConfig.gameId;
        //     }
        //     else {
        //         params = params + '&game_id=' + GameConfig.gameId;
        //     }
        // }
        // else {
        //     if (params == null) {
        //         params = 'game_id=' + GameConfig.gameId + '&token=' + window.UserInfo.token;
        //     }
        //     else {
        //         params = params + '&game_id=' + GameConfig.gameId + '&token=' + window.UserInfo.token;
        //     }
        // }

        xhr.open("GET", url, false);
        xhr.send(params);
        // xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        // xhr.timeout = 10000;
    },

    postRequest(url, params, callback) {
        // 发送 code
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = (function (res) {
            if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status < 300)) {
                let jsonData = JSON.parse(xhr.responseText);
                
                if (jsonData.code != 0) {
                    if (callback.failure) {
                        callback.failure(jsonData.code);
                    }
                }
                else {
                    if (callback.success) {
                        callback.success(jsonData.data);
                    }
                }
            }
        }).bind(this);

        xhr.onerror = (function (res) {
            if (callback.failure()) {
                callback.failure(res);
            }
        });

        // //验证微信临时登录凭证code
        // if (url == GameConfig.USER_CHECK_CODE) 
        // {
        //     if (params == null) 
        //     {
        //         params = 'game_id='+GameConfig.gameId;
        //     } 
        //     else 
        //     {
        //         params = params + '&game_id='+GameConfig.gameId;
        //     }
        // } 
        // else 
        // {
        //     if (params == null) 
        //     {
        //         params = 'game_id='+GameConfig.gameId + '&token='+window.UserInfo.token;
        //     } 
        //     else 
        //     {
        //         params = params + '&game_id='+GameConfig.gameId + '&token='+window.UserInfo.token;
        //     }
        // }

        xhr.open("POST", url, false);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send(params);
        // xhr.timeout = 10000;
    },

    // 归档数据
    saveGameData(dataM) {

        let url = window.Config.RECORD_SAVE;
        dataM.value = dataM.value == 0 ? -1 : dataM.value;
        dataM.value = JSON.stringify(dataM.value);
        let params = "record_type=" + dataM.key + '&record=' + dataM.value;

        this.postRequest(url, params, {

            success(data) {
                //成功
                dataM.suc(data);
            },

            failure(err) {
                if (dataM.fail) {
                    dataM.fail(err);
                }
            }
        });
    },


    // 存档数据
    getGameData(dataM) {

        let url = window.Config.RECORD_GET;
        let params = "record_type=" + dataM.key;

        this.postRequest(url, params, {

            success(data) {
                data.record = JSON.parse(data.record);
                data.record = data.record == -1 ? 0 : data.record;
                dataM.suc(data);
            },

            failure(err) {
                if (dataM.fail) {
                    dataM.fail(err);
                }
            }
        });
    },
}

module.exports = SkiNetwork;
