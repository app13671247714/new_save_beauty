window.__require = function e(t, i, n) {
function r(a, s) {
if (!i[a]) {
if (!t[a]) {
var c = a.split("/");
c = c[c.length - 1];
if (!t[c]) {
var l = "function" == typeof __require && __require;
if (!s && l) return l(c, !0);
if (o) return o(c, !0);
throw new Error("Cannot find module '" + a + "'");
}
a = c;
}
var h = i[a] = {
exports: {}
};
t[a][0].call(h.exports, function(e) {
return r(t[a][1][e] || e);
}, h, h.exports, e, t, i, n);
}
return i[a].exports;
}
for (var o = "function" == typeof __require && __require, a = 0; a < n.length; a++) r(n[a]);
return r;
}({
1: [ function(e, t, i) {
"use strict";
const n = i;
n.bignum = e("bn.js");
n.define = e("./asn1/api").define;
n.base = e("./asn1/base");
n.constants = e("./asn1/constants");
n.decoders = e("./asn1/decoders");
n.encoders = e("./asn1/encoders");
}, {
"./asn1/api": 2,
"./asn1/base": 4,
"./asn1/constants": 8,
"./asn1/decoders": 10,
"./asn1/encoders": 13,
"bn.js": 15
} ],
2: [ function(e, t, i) {
"use strict";
const n = e("./encoders"), r = e("./decoders"), o = e("inherits");
i.define = function(e, t) {
return new a(e, t);
};
function a(e, t) {
this.name = e;
this.body = t;
this.decoders = {};
this.encoders = {};
}
a.prototype._createNamed = function(e) {
const t = this.name;
function i(e) {
this._initNamed(e, t);
}
o(i, e);
i.prototype._initNamed = function(t, i) {
e.call(this, t, i);
};
return new i(this);
};
a.prototype._getDecoder = function(e) {
e = e || "der";
this.decoders.hasOwnProperty(e) || (this.decoders[e] = this._createNamed(r[e]));
return this.decoders[e];
};
a.prototype.decode = function(e, t, i) {
return this._getDecoder(t).decode(e, i);
};
a.prototype._getEncoder = function(e) {
e = e || "der";
this.encoders.hasOwnProperty(e) || (this.encoders[e] = this._createNamed(n[e]));
return this.encoders[e];
};
a.prototype.encode = function(e, t, i) {
return this._getEncoder(t).encode(e, i);
};
}, {
"./decoders": 10,
"./encoders": 13,
inherits: 138
} ],
3: [ function(e, t, i) {
"use strict";
const n = e("inherits"), r = e("../base/reporter").Reporter, o = e("safer-buffer").Buffer;
function a(e, t) {
r.call(this, t);
if (o.isBuffer(e)) {
this.base = e;
this.offset = 0;
this.length = e.length;
} else this.error("Input not Buffer");
}
n(a, r);
i.DecoderBuffer = a;
a.isDecoderBuffer = function(e) {
return e instanceof a || "object" == typeof e && o.isBuffer(e.base) && "DecoderBuffer" === e.constructor.name && "number" == typeof e.offset && "number" == typeof e.length && "function" == typeof e.save && "function" == typeof e.restore && "function" == typeof e.isEmpty && "function" == typeof e.readUInt8 && "function" == typeof e.skip && "function" == typeof e.raw;
};
a.prototype.save = function() {
return {
offset: this.offset,
reporter: r.prototype.save.call(this)
};
};
a.prototype.restore = function(e) {
const t = new a(this.base);
t.offset = e.offset;
t.length = this.offset;
this.offset = e.offset;
r.prototype.restore.call(this, e.reporter);
return t;
};
a.prototype.isEmpty = function() {
return this.offset === this.length;
};
a.prototype.readUInt8 = function(e) {
return this.offset + 1 <= this.length ? this.base.readUInt8(this.offset++, !0) : this.error(e || "DecoderBuffer overrun");
};
a.prototype.skip = function(e, t) {
if (!(this.offset + e <= this.length)) return this.error(t || "DecoderBuffer overrun");
const i = new a(this.base);
i._reporterState = this._reporterState;
i.offset = this.offset;
i.length = this.offset + e;
this.offset += e;
return i;
};
a.prototype.raw = function(e) {
return this.base.slice(e ? e.offset : this.offset, this.length);
};
function s(e, t) {
if (Array.isArray(e)) {
this.length = 0;
this.value = e.map(function(e) {
s.isEncoderBuffer(e) || (e = new s(e, t));
this.length += e.length;
return e;
}, this);
} else if ("number" == typeof e) {
if (!(0 <= e && e <= 255)) return t.error("non-byte EncoderBuffer value");
this.value = e;
this.length = 1;
} else if ("string" == typeof e) {
this.value = e;
this.length = o.byteLength(e);
} else {
if (!o.isBuffer(e)) return t.error("Unsupported type: " + typeof e);
this.value = e;
this.length = e.length;
}
}
i.EncoderBuffer = s;
s.isEncoderBuffer = function(e) {
return e instanceof s || "object" == typeof e && "EncoderBuffer" === e.constructor.name && "number" == typeof e.length && "function" == typeof e.join;
};
s.prototype.join = function(e, t) {
e || (e = o.alloc(this.length));
t || (t = 0);
if (0 === this.length) return e;
if (Array.isArray(this.value)) this.value.forEach(function(i) {
i.join(e, t);
t += i.length;
}); else {
"number" == typeof this.value ? e[t] = this.value : "string" == typeof this.value ? e.write(this.value, t) : o.isBuffer(this.value) && this.value.copy(e, t);
t += this.length;
}
return e;
};
}, {
"../base/reporter": 6,
inherits: 138,
"safer-buffer": 183
} ],
4: [ function(e, t, i) {
"use strict";
const n = i;
n.Reporter = e("./reporter").Reporter;
n.DecoderBuffer = e("./buffer").DecoderBuffer;
n.EncoderBuffer = e("./buffer").EncoderBuffer;
n.Node = e("./node");
}, {
"./buffer": 3,
"./node": 5,
"./reporter": 6
} ],
5: [ function(e, t) {
"use strict";
const i = e("../base/reporter").Reporter, n = e("../base/buffer").EncoderBuffer, r = e("../base/buffer").DecoderBuffer, o = e("minimalistic-assert"), a = [ "seq", "seqof", "set", "setof", "objid", "bool", "gentime", "utctime", "null_", "enum", "int", "objDesc", "bitstr", "bmpstr", "charstr", "genstr", "graphstr", "ia5str", "iso646str", "numstr", "octstr", "printstr", "t61str", "unistr", "utf8str", "videostr" ], s = [ "key", "obj", "use", "optional", "explicit", "implicit", "def", "choice", "any", "contains" ].concat(a);
function c(e, t, i) {
const n = {};
this._baseState = n;
n.name = i;
n.enc = e;
n.parent = t || null;
n.children = null;
n.tag = null;
n.args = null;
n.reverseArgs = null;
n.choice = null;
n.optional = !1;
n.any = !1;
n.obj = !1;
n.use = null;
n.useDecoder = null;
n.key = null;
n.default = null;
n.explicit = null;
n.implicit = null;
n.contains = null;
if (!n.parent) {
n.children = [];
this._wrap();
}
}
t.exports = c;
const l = [ "enc", "parent", "children", "tag", "args", "reverseArgs", "choice", "optional", "any", "obj", "use", "alteredUse", "key", "default", "explicit", "implicit", "contains" ];
c.prototype.clone = function() {
const e = this._baseState, t = {};
l.forEach(function(i) {
t[i] = e[i];
});
const i = new this.constructor(t.parent);
i._baseState = t;
return i;
};
c.prototype._wrap = function() {
const e = this._baseState;
s.forEach(function(t) {
this[t] = function() {
const i = new this.constructor(this);
e.children.push(i);
return i[t].apply(i, arguments);
};
}, this);
};
c.prototype._init = function(e) {
const t = this._baseState;
o(null === t.parent);
e.call(this);
t.children = t.children.filter(function(e) {
return e._baseState.parent === this;
}, this);
o.equal(t.children.length, 1, "Root node can have only one child");
};
c.prototype._useArgs = function(e) {
const t = this._baseState, i = e.filter(function(e) {
return e instanceof this.constructor;
}, this);
e = e.filter(function(e) {
return !(e instanceof this.constructor);
}, this);
if (0 !== i.length) {
o(null === t.children);
t.children = i;
i.forEach(function(e) {
e._baseState.parent = this;
}, this);
}
if (0 !== e.length) {
o(null === t.args);
t.args = e;
t.reverseArgs = e.map(function(e) {
if ("object" != typeof e || e.constructor !== Object) return e;
const t = {};
Object.keys(e).forEach(function(i) {
i == (0 | i) && (i |= 0);
const n = e[i];
t[n] = i;
});
return t;
});
}
};
[ "_peekTag", "_decodeTag", "_use", "_decodeStr", "_decodeObjid", "_decodeTime", "_decodeNull", "_decodeInt", "_decodeBool", "_decodeList", "_encodeComposite", "_encodeStr", "_encodeObjid", "_encodeTime", "_encodeNull", "_encodeInt", "_encodeBool" ].forEach(function(e) {
c.prototype[e] = function() {
const t = this._baseState;
throw new Error(e + " not implemented for encoding: " + t.enc);
};
});
a.forEach(function(e) {
c.prototype[e] = function() {
const t = this._baseState, i = Array.prototype.slice.call(arguments);
o(null === t.tag);
t.tag = e;
this._useArgs(i);
return this;
};
});
c.prototype.use = function(e) {
o(e);
const t = this._baseState;
o(null === t.use);
t.use = e;
return this;
};
c.prototype.optional = function() {
this._baseState.optional = !0;
return this;
};
c.prototype.def = function(e) {
const t = this._baseState;
o(null === t.default);
t.default = e;
t.optional = !0;
return this;
};
c.prototype.explicit = function(e) {
const t = this._baseState;
o(null === t.explicit && null === t.implicit);
t.explicit = e;
return this;
};
c.prototype.implicit = function(e) {
const t = this._baseState;
o(null === t.explicit && null === t.implicit);
t.implicit = e;
return this;
};
c.prototype.obj = function() {
const e = this._baseState, t = Array.prototype.slice.call(arguments);
e.obj = !0;
0 !== t.length && this._useArgs(t);
return this;
};
c.prototype.key = function(e) {
const t = this._baseState;
o(null === t.key);
t.key = e;
return this;
};
c.prototype.any = function() {
this._baseState.any = !0;
return this;
};
c.prototype.choice = function(e) {
const t = this._baseState;
o(null === t.choice);
t.choice = e;
this._useArgs(Object.keys(e).map(function(t) {
return e[t];
}));
return this;
};
c.prototype.contains = function(e) {
const t = this._baseState;
o(null === t.use);
t.contains = e;
return this;
};
c.prototype._decode = function(e, t) {
const i = this._baseState;
if (null === i.parent) return e.wrapResult(i.children[0]._decode(e, t));
let n, o = i.default, a = !0, s = null;
null !== i.key && (s = e.enterKey(i.key));
if (i.optional) {
let n = null;
null !== i.explicit ? n = i.explicit : null !== i.implicit ? n = i.implicit : null !== i.tag && (n = i.tag);
if (null !== n || i.any) {
a = this._peekTag(e, n, i.any);
if (e.isError(a)) return a;
} else {
const n = e.save();
try {
null === i.choice ? this._decodeGeneric(i.tag, e, t) : this._decodeChoice(e, t);
a = !0;
} catch (e) {
a = !1;
}
e.restore(n);
}
}
i.obj && a && (n = e.enterObject());
if (a) {
if (null !== i.explicit) {
const t = this._decodeTag(e, i.explicit);
if (e.isError(t)) return t;
e = t;
}
const n = e.offset;
if (null === i.use && null === i.choice) {
let t;
i.any && (t = e.save());
const n = this._decodeTag(e, null !== i.implicit ? i.implicit : i.tag, i.any);
if (e.isError(n)) return n;
i.any ? o = e.raw(t) : e = n;
}
t && t.track && null !== i.tag && t.track(e.path(), n, e.length, "tagged");
t && t.track && null !== i.tag && t.track(e.path(), e.offset, e.length, "content");
i.any || (o = null === i.choice ? this._decodeGeneric(i.tag, e, t) : this._decodeChoice(e, t));
if (e.isError(o)) return o;
i.any || null !== i.choice || null === i.children || i.children.forEach(function(i) {
i._decode(e, t);
});
if (i.contains && ("octstr" === i.tag || "bitstr" === i.tag)) {
const n = new r(o);
o = this._getUse(i.contains, e._reporterState.obj)._decode(n, t);
}
}
i.obj && a && (o = e.leaveObject(n));
null === i.key || null === o && !0 !== a ? null !== s && e.exitKey(s) : e.leaveKey(s, i.key, o);
return o;
};
c.prototype._decodeGeneric = function(e, t, i) {
const n = this._baseState;
return "seq" === e || "set" === e ? null : "seqof" === e || "setof" === e ? this._decodeList(t, e, n.args[0], i) : /str$/.test(e) ? this._decodeStr(t, e, i) : "objid" === e && n.args ? this._decodeObjid(t, n.args[0], n.args[1], i) : "objid" === e ? this._decodeObjid(t, null, null, i) : "gentime" === e || "utctime" === e ? this._decodeTime(t, e, i) : "null_" === e ? this._decodeNull(t, i) : "bool" === e ? this._decodeBool(t, i) : "objDesc" === e ? this._decodeStr(t, e, i) : "int" === e || "enum" === e ? this._decodeInt(t, n.args && n.args[0], i) : null !== n.use ? this._getUse(n.use, t._reporterState.obj)._decode(t, i) : t.error("unknown tag: " + e);
};
c.prototype._getUse = function(e, t) {
const i = this._baseState;
i.useDecoder = this._use(e, t);
o(null === i.useDecoder._baseState.parent);
i.useDecoder = i.useDecoder._baseState.children[0];
if (i.implicit !== i.useDecoder._baseState.implicit) {
i.useDecoder = i.useDecoder.clone();
i.useDecoder._baseState.implicit = i.implicit;
}
return i.useDecoder;
};
c.prototype._decodeChoice = function(e, t) {
const i = this._baseState;
let n = null, r = !1;
Object.keys(i.choice).some(function(o) {
const a = e.save(), s = i.choice[o];
try {
const i = s._decode(e, t);
if (e.isError(i)) return !1;
n = {
type: o,
value: i
};
r = !0;
} catch (t) {
e.restore(a);
return !1;
}
return !0;
}, this);
return r ? n : e.error("Choice not matched");
};
c.prototype._createEncoderBuffer = function(e) {
return new n(e, this.reporter);
};
c.prototype._encode = function(e, t, i) {
const n = this._baseState;
if (null !== n.default && n.default === e) return;
const r = this._encodeValue(e, t, i);
return void 0 === r || this._skipDefault(r, t, i) ? void 0 : r;
};
c.prototype._encodeValue = function(e, t, n) {
const r = this._baseState;
if (null === r.parent) return r.children[0]._encode(e, t || new i());
let o = null;
this.reporter = t;
if (r.optional && void 0 === e) {
if (null === r.default) return;
e = r.default;
}
let a = null, s = !1;
if (r.any) o = this._createEncoderBuffer(e); else if (r.choice) o = this._encodeChoice(e, t); else if (r.contains) {
a = this._getUse(r.contains, n)._encode(e, t);
s = !0;
} else if (r.children) {
a = r.children.map(function(i) {
if ("null_" === i._baseState.tag) return i._encode(null, t, e);
if (null === i._baseState.key) return t.error("Child should have a key");
const n = t.enterKey(i._baseState.key);
if ("object" != typeof e) return t.error("Child expected, but input is not object");
const r = i._encode(e[i._baseState.key], t, e);
t.leaveKey(n);
return r;
}, this).filter(function(e) {
return e;
});
a = this._createEncoderBuffer(a);
} else if ("seqof" === r.tag || "setof" === r.tag) {
if (!r.args || 1 !== r.args.length) return t.error("Too many args for : " + r.tag);
if (!Array.isArray(e)) return t.error("seqof/setof, but data is not Array");
const i = this.clone();
i._baseState.implicit = null;
a = this._createEncoderBuffer(e.map(function(i) {
const n = this._baseState;
return this._getUse(n.args[0], e)._encode(i, t);
}, i));
} else if (null !== r.use) o = this._getUse(r.use, n)._encode(e, t); else {
a = this._encodePrimitive(r.tag, e);
s = !0;
}
if (!r.any && null === r.choice) {
const e = null !== r.implicit ? r.implicit : r.tag, i = null === r.implicit ? "universal" : "context";
null === e ? null === r.use && t.error("Tag could be omitted only for .use()") : null === r.use && (o = this._encodeComposite(e, s, i, a));
}
null !== r.explicit && (o = this._encodeComposite(r.explicit, !1, "context", o));
return o;
};
c.prototype._encodeChoice = function(e, t) {
const i = this._baseState, n = i.choice[e.type];
n || o(!1, e.type + " not found in " + JSON.stringify(Object.keys(i.choice)));
return n._encode(e.value, t);
};
c.prototype._encodePrimitive = function(e, t) {
const i = this._baseState;
if (/str$/.test(e)) return this._encodeStr(t, e);
if ("objid" === e && i.args) return this._encodeObjid(t, i.reverseArgs[0], i.args[1]);
if ("objid" === e) return this._encodeObjid(t, null, null);
if ("gentime" === e || "utctime" === e) return this._encodeTime(t, e);
if ("null_" === e) return this._encodeNull();
if ("int" === e || "enum" === e) return this._encodeInt(t, i.args && i.reverseArgs[0]);
if ("bool" === e) return this._encodeBool(t);
if ("objDesc" === e) return this._encodeStr(t, e);
throw new Error("Unsupported tag: " + e);
};
c.prototype._isNumstr = function(e) {
return /^[0-9 ]*$/.test(e);
};
c.prototype._isPrintstr = function(e) {
return /^[A-Za-z0-9 '()+,-./:=?]*$/.test(e);
};
}, {
"../base/buffer": 3,
"../base/reporter": 6,
"minimalistic-assert": 142
} ],
6: [ function(e, t, i) {
"use strict";
const n = e("inherits");
function r(e) {
this._reporterState = {
obj: null,
path: [],
options: e || {},
errors: []
};
}
i.Reporter = r;
r.prototype.isError = function(e) {
return e instanceof o;
};
r.prototype.save = function() {
const e = this._reporterState;
return {
obj: e.obj,
pathLen: e.path.length
};
};
r.prototype.restore = function(e) {
const t = this._reporterState;
t.obj = e.obj;
t.path = t.path.slice(0, e.pathLen);
};
r.prototype.enterKey = function(e) {
return this._reporterState.path.push(e);
};
r.prototype.exitKey = function(e) {
const t = this._reporterState;
t.path = t.path.slice(0, e - 1);
};
r.prototype.leaveKey = function(e, t, i) {
const n = this._reporterState;
this.exitKey(e);
null !== n.obj && (n.obj[t] = i);
};
r.prototype.path = function() {
return this._reporterState.path.join("/");
};
r.prototype.enterObject = function() {
const e = this._reporterState, t = e.obj;
e.obj = {};
return t;
};
r.prototype.leaveObject = function(e) {
const t = this._reporterState, i = t.obj;
t.obj = e;
return i;
};
r.prototype.error = function(e) {
let t;
const i = this._reporterState, n = e instanceof o;
t = n ? e : new o(i.path.map(function(e) {
return "[" + JSON.stringify(e) + "]";
}).join(""), e.message || e, e.stack);
if (!i.options.partial) throw t;
n || i.errors.push(t);
return t;
};
r.prototype.wrapResult = function(e) {
const t = this._reporterState;
return t.options.partial ? {
result: this.isError(e) ? null : e,
errors: t.errors
} : e;
};
function o(e, t) {
this.path = e;
this.rethrow(t);
}
n(o, Error);
o.prototype.rethrow = function(e) {
this.message = e + " at: " + (this.path || "(shallow)");
Error.captureStackTrace && Error.captureStackTrace(this, o);
if (!this.stack) try {
throw new Error(this.message);
} catch (e) {
this.stack = e.stack;
}
return this;
};
}, {
inherits: 138
} ],
7: [ function(e, t, i) {
"use strict";
function n(e) {
const t = {};
Object.keys(e).forEach(function(i) {
(0 | i) == i && (i |= 0);
const n = e[i];
t[n] = i;
});
return t;
}
i.tagClass = {
0: "universal",
1: "application",
2: "context",
3: "private"
};
i.tagClassByName = n(i.tagClass);
i.tag = {
0: "end",
1: "bool",
2: "int",
3: "bitstr",
4: "octstr",
5: "null_",
6: "objid",
7: "objDesc",
8: "external",
9: "real",
10: "enum",
11: "embed",
12: "utf8str",
13: "relativeOid",
16: "seq",
17: "set",
18: "numstr",
19: "printstr",
20: "t61str",
21: "videostr",
22: "ia5str",
23: "utctime",
24: "gentime",
25: "graphstr",
26: "iso646str",
27: "genstr",
28: "unistr",
29: "charstr",
30: "bmpstr"
};
i.tagByName = n(i.tag);
}, {} ],
8: [ function(e, t, i) {
"use strict";
const n = i;
n._reverse = function(e) {
const t = {};
Object.keys(e).forEach(function(i) {
(0 | i) == i && (i |= 0);
const n = e[i];
t[n] = i;
});
return t;
};
n.der = e("./der");
}, {
"./der": 7
} ],
9: [ function(e, t) {
"use strict";
const i = e("inherits"), n = e("bn.js"), r = e("../base/buffer").DecoderBuffer, o = e("../base/node"), a = e("../constants/der");
function s(e) {
this.enc = "der";
this.name = e.name;
this.entity = e;
this.tree = new c();
this.tree._init(e.body);
}
t.exports = s;
s.prototype.decode = function(e, t) {
r.isDecoderBuffer(e) || (e = new r(e, t));
return this.tree._decode(e, t);
};
function c(e) {
o.call(this, "der", e);
}
i(c, o);
c.prototype._peekTag = function(e, t, i) {
if (e.isEmpty()) return !1;
const n = e.save(), r = l(e, 'Failed to peek tag: "' + t + '"');
if (e.isError(r)) return r;
e.restore(n);
return r.tag === t || r.tagStr === t || r.tagStr + "of" === t || i;
};
c.prototype._decodeTag = function(e, t, i) {
const n = l(e, 'Failed to decode tag of "' + t + '"');
if (e.isError(n)) return n;
let r = h(e, n.primitive, 'Failed to get length of "' + t + '"');
if (e.isError(r)) return r;
if (!i && n.tag !== t && n.tagStr !== t && n.tagStr + "of" !== t) return e.error('Failed to match tag: "' + t + '"');
if (n.primitive || null !== r) return e.skip(r, 'Failed to match body of: "' + t + '"');
const o = e.save(), a = this._skipUntilEnd(e, 'Failed to skip indefinite length body: "' + this.tag + '"');
if (e.isError(a)) return a;
r = e.offset - o.offset;
e.restore(o);
return e.skip(r, 'Failed to match body of: "' + t + '"');
};
c.prototype._skipUntilEnd = function(e, t) {
for (;;) {
const i = l(e, t);
if (e.isError(i)) return i;
const n = h(e, i.primitive, t);
if (e.isError(n)) return n;
let r;
r = i.primitive || null !== n ? e.skip(n) : this._skipUntilEnd(e, t);
if (e.isError(r)) return r;
if ("end" === i.tagStr) break;
}
};
c.prototype._decodeList = function(e, t, i, n) {
const r = [];
for (;!e.isEmpty(); ) {
const t = this._peekTag(e, "end");
if (e.isError(t)) return t;
const o = i.decode(e, "der", n);
if (e.isError(o) && t) break;
r.push(o);
}
return r;
};
c.prototype._decodeStr = function(e, t) {
if ("bitstr" === t) {
const t = e.readUInt8();
return e.isError(t) ? t : {
unused: t,
data: e.raw()
};
}
if ("bmpstr" === t) {
const t = e.raw();
if (t.length % 2 == 1) return e.error("Decoding of string type: bmpstr length mismatch");
let i = "";
for (let e = 0; e < t.length / 2; e++) i += String.fromCharCode(t.readUInt16BE(2 * e));
return i;
}
if ("numstr" === t) {
const t = e.raw().toString("ascii");
return this._isNumstr(t) ? t : e.error("Decoding of string type: numstr unsupported characters");
}
if ("octstr" === t) return e.raw();
if ("objDesc" === t) return e.raw();
if ("printstr" === t) {
const t = e.raw().toString("ascii");
return this._isPrintstr(t) ? t : e.error("Decoding of string type: printstr unsupported characters");
}
return /str$/.test(t) ? e.raw().toString() : e.error("Decoding of string type: " + t + " unsupported");
};
c.prototype._decodeObjid = function(e, t, i) {
let n;
const r = [];
let o = 0, a = 0;
for (;!e.isEmpty(); ) {
o <<= 7;
o |= 127 & (a = e.readUInt8());
if (0 == (128 & a)) {
r.push(o);
o = 0;
}
}
128 & a && r.push(o);
const s = r[0] / 40 | 0, c = r[0] % 40;
n = i ? r : [ s, c ].concat(r.slice(1));
if (t) {
let e = t[n.join(" ")];
void 0 === e && (e = t[n.join(".")]);
void 0 !== e && (n = e);
}
return n;
};
c.prototype._decodeTime = function(e, t) {
const i = e.raw().toString();
let n, r, o, a, s, c;
if ("gentime" === t) {
n = 0 | i.slice(0, 4);
r = 0 | i.slice(4, 6);
o = 0 | i.slice(6, 8);
a = 0 | i.slice(8, 10);
s = 0 | i.slice(10, 12);
c = 0 | i.slice(12, 14);
} else {
if ("utctime" !== t) return e.error("Decoding " + t + " time is not supported yet");
n = 0 | i.slice(0, 2);
r = 0 | i.slice(2, 4);
o = 0 | i.slice(4, 6);
a = 0 | i.slice(6, 8);
s = 0 | i.slice(8, 10);
c = 0 | i.slice(10, 12);
n = n < 70 ? 2e3 + n : 1900 + n;
}
return Date.UTC(n, r - 1, o, a, s, c, 0);
};
c.prototype._decodeNull = function() {
return null;
};
c.prototype._decodeBool = function(e) {
const t = e.readUInt8();
return e.isError(t) ? t : 0 !== t;
};
c.prototype._decodeInt = function(e, t) {
const i = e.raw();
let r = new n(i);
t && (r = t[r.toString(10)] || r);
return r;
};
c.prototype._use = function(e, t) {
"function" == typeof e && (e = e(t));
return e._getDecoder("der").tree;
};
function l(e, t) {
let i = e.readUInt8(t);
if (e.isError(i)) return i;
const n = a.tagClass[i >> 6], r = 0 == (32 & i);
if (31 == (31 & i)) {
let n = i;
i = 0;
for (;128 == (128 & n); ) {
n = e.readUInt8(t);
if (e.isError(n)) return n;
i <<= 7;
i |= 127 & n;
}
} else i &= 31;
return {
cls: n,
primitive: r,
tag: i,
tagStr: a.tag[i]
};
}
function h(e, t, i) {
let n = e.readUInt8(i);
if (e.isError(n)) return n;
if (!t && 128 === n) return null;
if (0 == (128 & n)) return n;
const r = 127 & n;
if (r > 4) return e.error("length octect is too long");
n = 0;
for (let t = 0; t < r; t++) {
n <<= 8;
const t = e.readUInt8(i);
if (e.isError(t)) return t;
n |= t;
}
return n;
}
}, {
"../base/buffer": 3,
"../base/node": 5,
"../constants/der": 7,
"bn.js": 15,
inherits: 138
} ],
10: [ function(e, t, i) {
"use strict";
const n = i;
n.der = e("./der");
n.pem = e("./pem");
}, {
"./der": 9,
"./pem": 11
} ],
11: [ function(e, t) {
"use strict";
const i = e("inherits"), n = e("safer-buffer").Buffer, r = e("./der");
function o(e) {
r.call(this, e);
this.enc = "pem";
}
i(o, r);
t.exports = o;
o.prototype.decode = function(e, t) {
const i = e.toString().split(/[\r\n]+/g), o = t.label.toUpperCase(), a = /^-----(BEGIN|END) ([^-]+)-----$/;
let s = -1, c = -1;
for (let e = 0; e < i.length; e++) {
const t = i[e].match(a);
if (null !== t && t[2] === o) {
if (-1 !== s) {
if ("END" !== t[1]) break;
c = e;
break;
}
if ("BEGIN" !== t[1]) break;
s = e;
}
}
if (-1 === s || -1 === c) throw new Error("PEM section not found for: " + o);
const l = i.slice(s + 1, c).join("");
l.replace(/[^a-z0-9+/=]+/gi, "");
const h = n.from(l, "base64");
return r.prototype.decode.call(this, h, t);
};
}, {
"./der": 9,
inherits: 138,
"safer-buffer": 183
} ],
12: [ function(e, t) {
"use strict";
const i = e("inherits"), n = e("safer-buffer").Buffer, r = e("../base/node"), o = e("../constants/der");
function a(e) {
this.enc = "der";
this.name = e.name;
this.entity = e;
this.tree = new s();
this.tree._init(e.body);
}
t.exports = a;
a.prototype.encode = function(e, t) {
return this.tree._encode(e, t).join();
};
function s(e) {
r.call(this, "der", e);
}
i(s, r);
s.prototype._encodeComposite = function(e, t, i, r) {
const o = l(e, t, i, this.reporter);
if (r.length < 128) {
const e = n.alloc(2);
e[0] = o;
e[1] = r.length;
return this._createEncoderBuffer([ e, r ]);
}
let a = 1;
for (let e = r.length; e >= 256; e >>= 8) a++;
const s = n.alloc(2 + a);
s[0] = o;
s[1] = 128 | a;
for (let e = 1 + a, t = r.length; t > 0; e--, t >>= 8) s[e] = 255 & t;
return this._createEncoderBuffer([ s, r ]);
};
s.prototype._encodeStr = function(e, t) {
if ("bitstr" === t) return this._createEncoderBuffer([ 0 | e.unused, e.data ]);
if ("bmpstr" === t) {
const t = n.alloc(2 * e.length);
for (let i = 0; i < e.length; i++) t.writeUInt16BE(e.charCodeAt(i), 2 * i);
return this._createEncoderBuffer(t);
}
return "numstr" === t ? this._isNumstr(e) ? this._createEncoderBuffer(e) : this.reporter.error("Encoding of string type: numstr supports only digits and space") : "printstr" === t ? this._isPrintstr(e) ? this._createEncoderBuffer(e) : this.reporter.error("Encoding of string type: printstr supports only latin upper and lower case letters, digits, space, apostrophe, left and rigth parenthesis, plus sign, comma, hyphen, dot, slash, colon, equal sign, question mark") : /str$/.test(t) ? this._createEncoderBuffer(e) : "objDesc" === t ? this._createEncoderBuffer(e) : this.reporter.error("Encoding of string type: " + t + " unsupported");
};
s.prototype._encodeObjid = function(e, t, i) {
if ("string" == typeof e) {
if (!t) return this.reporter.error("string objid given, but no values map found");
if (!t.hasOwnProperty(e)) return this.reporter.error("objid not found in values map");
e = t[e].split(/[\s.]+/g);
for (let t = 0; t < e.length; t++) e[t] |= 0;
} else if (Array.isArray(e)) {
e = e.slice();
for (let t = 0; t < e.length; t++) e[t] |= 0;
}
if (!Array.isArray(e)) return this.reporter.error("objid() should be either array or string, got: " + JSON.stringify(e));
if (!i) {
if (e[1] >= 40) return this.reporter.error("Second objid identifier OOB");
e.splice(0, 2, 40 * e[0] + e[1]);
}
let r = 0;
for (let t = 0; t < e.length; t++) {
let i = e[t];
for (r++; i >= 128; i >>= 7) r++;
}
const o = n.alloc(r);
let a = o.length - 1;
for (let t = e.length - 1; t >= 0; t--) {
let i = e[t];
o[a--] = 127 & i;
for (;(i >>= 7) > 0; ) o[a--] = 128 | 127 & i;
}
return this._createEncoderBuffer(o);
};
function c(e) {
return e < 10 ? "0" + e : e;
}
s.prototype._encodeTime = function(e, t) {
let i;
const n = new Date(e);
"gentime" === t ? i = [ c(n.getUTCFullYear()), c(n.getUTCMonth() + 1), c(n.getUTCDate()), c(n.getUTCHours()), c(n.getUTCMinutes()), c(n.getUTCSeconds()), "Z" ].join("") : "utctime" === t ? i = [ c(n.getUTCFullYear() % 100), c(n.getUTCMonth() + 1), c(n.getUTCDate()), c(n.getUTCHours()), c(n.getUTCMinutes()), c(n.getUTCSeconds()), "Z" ].join("") : this.reporter.error("Encoding " + t + " time is not supported yet");
return this._encodeStr(i, "octstr");
};
s.prototype._encodeNull = function() {
return this._createEncoderBuffer("");
};
s.prototype._encodeInt = function(e, t) {
if ("string" == typeof e) {
if (!t) return this.reporter.error("String int or enum given, but no values map");
if (!t.hasOwnProperty(e)) return this.reporter.error("Values map doesn't contain: " + JSON.stringify(e));
e = t[e];
}
if ("number" != typeof e && !n.isBuffer(e)) {
const t = e.toArray();
!e.sign && 128 & t[0] && t.unshift(0);
e = n.from(t);
}
if (n.isBuffer(e)) {
let t = e.length;
0 === e.length && t++;
const i = n.alloc(t);
e.copy(i);
0 === e.length && (i[0] = 0);
return this._createEncoderBuffer(i);
}
if (e < 128) return this._createEncoderBuffer(e);
if (e < 256) return this._createEncoderBuffer([ 0, e ]);
let i = 1;
for (let t = e; t >= 256; t >>= 8) i++;
const r = new Array(i);
for (let t = r.length - 1; t >= 0; t--) {
r[t] = 255 & e;
e >>= 8;
}
128 & r[0] && r.unshift(0);
return this._createEncoderBuffer(n.from(r));
};
s.prototype._encodeBool = function(e) {
return this._createEncoderBuffer(e ? 255 : 0);
};
s.prototype._use = function(e, t) {
"function" == typeof e && (e = e(t));
return e._getEncoder("der").tree;
};
s.prototype._skipDefault = function(e, t, i) {
const n = this._baseState;
let r;
if (null === n.default) return !1;
const o = e.join();
void 0 === n.defaultBuffer && (n.defaultBuffer = this._encodeValue(n.default, t, i).join());
if (o.length !== n.defaultBuffer.length) return !1;
for (r = 0; r < o.length; r++) if (o[r] !== n.defaultBuffer[r]) return !1;
return !0;
};
function l(e, t, i, n) {
let r;
"seqof" === e ? e = "seq" : "setof" === e && (e = "set");
if (o.tagByName.hasOwnProperty(e)) r = o.tagByName[e]; else {
if ("number" != typeof e || (0 | e) !== e) return n.error("Unknown tag: " + e);
r = e;
}
if (r >= 31) return n.error("Multi-octet tag encoding unsupported");
t || (r |= 32);
return r | o.tagClassByName[i || "universal"] << 6;
}
}, {
"../base/node": 5,
"../constants/der": 7,
inherits: 138,
"safer-buffer": 183
} ],
13: [ function(e, t, i) {
"use strict";
const n = i;
n.der = e("./der");
n.pem = e("./pem");
}, {
"./der": 12,
"./pem": 14
} ],
14: [ function(e, t) {
"use strict";
const i = e("inherits"), n = e("./der");
function r(e) {
n.call(this, e);
this.enc = "pem";
}
i(r, n);
t.exports = r;
r.prototype.encode = function(e, t) {
const i = n.prototype.encode.call(this, e).toString("base64"), r = [ "-----BEGIN " + t.label + "-----" ];
for (let e = 0; e < i.length; e += 64) r.push(i.slice(e, e + 64));
r.push("-----END " + t.label + "-----");
return r.join("\n");
};
}, {
"./der": 12,
inherits: 138
} ],
15: [ function(e, t) {
(function(t, i) {
"use strict";
function n(e, t) {
if (!e) throw new Error(t || "Assertion failed");
}
function r(e, t) {
e.super_ = t;
var i = function() {};
i.prototype = t.prototype;
e.prototype = new i();
e.prototype.constructor = e;
}
function o(e, t, i) {
if (o.isBN(e)) return e;
this.negative = 0;
this.words = null;
this.length = 0;
this.red = null;
if (null !== e) {
if ("le" === t || "be" === t) {
i = t;
t = 10;
}
this._init(e || 0, t || 10, i || "be");
}
}
"object" == typeof t ? t.exports = o : i.BN = o;
o.BN = o;
o.wordSize = 26;
var a;
try {
a = "undefined" != typeof window && "undefined" != typeof window.Buffer ? window.Buffer : e("buffer").Buffer;
} catch (e) {}
o.isBN = function(e) {
return e instanceof o || null !== e && "object" == typeof e && e.constructor.wordSize === o.wordSize && Array.isArray(e.words);
};
o.max = function(e, t) {
return e.cmp(t) > 0 ? e : t;
};
o.min = function(e, t) {
return e.cmp(t) < 0 ? e : t;
};
o.prototype._init = function(e, t, i) {
if ("number" == typeof e) return this._initNumber(e, t, i);
if ("object" == typeof e) return this._initArray(e, t, i);
"hex" === t && (t = 16);
n(t === (0 | t) && t >= 2 && t <= 36);
var r = 0;
if ("-" === (e = e.toString().replace(/\s+/g, ""))[0]) {
r++;
this.negative = 1;
}
if (r < e.length) if (16 === t) this._parseHex(e, r, i); else {
this._parseBase(e, t, r);
"le" === i && this._initArray(this.toArray(), t, i);
}
};
o.prototype._initNumber = function(e, t, i) {
if (e < 0) {
this.negative = 1;
e = -e;
}
if (e < 67108864) {
this.words = [ 67108863 & e ];
this.length = 1;
} else if (e < 4503599627370496) {
this.words = [ 67108863 & e, e / 67108864 & 67108863 ];
this.length = 2;
} else {
n(e < 9007199254740992);
this.words = [ 67108863 & e, e / 67108864 & 67108863, 1 ];
this.length = 3;
}
"le" === i && this._initArray(this.toArray(), t, i);
};
o.prototype._initArray = function(e, t, i) {
n("number" == typeof e.length);
if (e.length <= 0) {
this.words = [ 0 ];
this.length = 1;
return this;
}
this.length = Math.ceil(e.length / 3);
this.words = new Array(this.length);
for (var r = 0; r < this.length; r++) this.words[r] = 0;
var o, a, s = 0;
if ("be" === i) for (r = e.length - 1, o = 0; r >= 0; r -= 3) {
a = e[r] | e[r - 1] << 8 | e[r - 2] << 16;
this.words[o] |= a << s & 67108863;
this.words[o + 1] = a >>> 26 - s & 67108863;
if ((s += 24) >= 26) {
s -= 26;
o++;
}
} else if ("le" === i) for (r = 0, o = 0; r < e.length; r += 3) {
a = e[r] | e[r + 1] << 8 | e[r + 2] << 16;
this.words[o] |= a << s & 67108863;
this.words[o + 1] = a >>> 26 - s & 67108863;
if ((s += 24) >= 26) {
s -= 26;
o++;
}
}
return this.strip();
};
function s(e, t) {
var i = e.charCodeAt(t);
return i >= 65 && i <= 70 ? i - 55 : i >= 97 && i <= 102 ? i - 87 : i - 48 & 15;
}
function c(e, t, i) {
var n = s(e, i);
i - 1 >= t && (n |= s(e, i - 1) << 4);
return n;
}
o.prototype._parseHex = function(e, t, i) {
this.length = Math.ceil((e.length - t) / 6);
this.words = new Array(this.length);
for (var n = 0; n < this.length; n++) this.words[n] = 0;
var r, o = 0, a = 0;
if ("be" === i) for (n = e.length - 1; n >= t; n -= 2) {
r = c(e, t, n) << o;
this.words[a] |= 67108863 & r;
if (o >= 18) {
o -= 18;
a += 1;
this.words[a] |= r >>> 26;
} else o += 8;
} else for (n = (e.length - t) % 2 == 0 ? t + 1 : t; n < e.length; n += 2) {
r = c(e, t, n) << o;
this.words[a] |= 67108863 & r;
if (o >= 18) {
o -= 18;
a += 1;
this.words[a] |= r >>> 26;
} else o += 8;
}
this.strip();
};
function l(e, t, i, n) {
for (var r = 0, o = Math.min(e.length, i), a = t; a < o; a++) {
var s = e.charCodeAt(a) - 48;
r *= n;
r += s >= 49 ? s - 49 + 10 : s >= 17 ? s - 17 + 10 : s;
}
return r;
}
o.prototype._parseBase = function(e, t, i) {
this.words = [ 0 ];
this.length = 1;
for (var n = 0, r = 1; r <= 67108863; r *= t) n++;
n--;
r = r / t | 0;
for (var o = e.length - i, a = o % n, s = Math.min(o, o - a) + i, c = 0, h = i; h < s; h += n) {
c = l(e, h, h + n, t);
this.imuln(r);
this.words[0] + c < 67108864 ? this.words[0] += c : this._iaddn(c);
}
if (0 !== a) {
var u = 1;
c = l(e, h, e.length, t);
for (h = 0; h < a; h++) u *= t;
this.imuln(u);
this.words[0] + c < 67108864 ? this.words[0] += c : this._iaddn(c);
}
this.strip();
};
o.prototype.copy = function(e) {
e.words = new Array(this.length);
for (var t = 0; t < this.length; t++) e.words[t] = this.words[t];
e.length = this.length;
e.negative = this.negative;
e.red = this.red;
};
o.prototype.clone = function() {
var e = new o(null);
this.copy(e);
return e;
};
o.prototype._expand = function(e) {
for (;this.length < e; ) this.words[this.length++] = 0;
return this;
};
o.prototype.strip = function() {
for (;this.length > 1 && 0 === this.words[this.length - 1]; ) this.length--;
return this._normSign();
};
o.prototype._normSign = function() {
1 === this.length && 0 === this.words[0] && (this.negative = 0);
return this;
};
o.prototype.inspect = function() {
return (this.red ? "<BN-R: " : "<BN: ") + this.toString(16) + ">";
};
var h = [ "", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000", "0000000000000000", "00000000000000000", "000000000000000000", "0000000000000000000", "00000000000000000000", "000000000000000000000", "0000000000000000000000", "00000000000000000000000", "000000000000000000000000", "0000000000000000000000000" ], u = [ 0, 0, 25, 16, 12, 11, 10, 9, 8, 8, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ], f = [ 0, 0, 33554432, 43046721, 16777216, 48828125, 60466176, 40353607, 16777216, 43046721, 1e7, 19487171, 35831808, 62748517, 7529536, 11390625, 16777216, 24137569, 34012224, 47045881, 64e6, 4084101, 5153632, 6436343, 7962624, 9765625, 11881376, 14348907, 17210368, 20511149, 243e5, 28629151, 33554432, 39135393, 45435424, 52521875, 60466176 ];
o.prototype.toString = function(e, t) {
t = 0 | t || 1;
var i;
if (16 === (e = e || 10) || "hex" === e) {
i = "";
for (var r = 0, o = 0, a = 0; a < this.length; a++) {
var s = this.words[a], c = (16777215 & (s << r | o)).toString(16);
i = 0 != (o = s >>> 24 - r & 16777215) || a !== this.length - 1 ? h[6 - c.length] + c + i : c + i;
if ((r += 2) >= 26) {
r -= 26;
a--;
}
}
0 !== o && (i = o.toString(16) + i);
for (;i.length % t != 0; ) i = "0" + i;
0 !== this.negative && (i = "-" + i);
return i;
}
if (e === (0 | e) && e >= 2 && e <= 36) {
var l = u[e], d = f[e];
i = "";
var p = this.clone();
p.negative = 0;
for (;!p.isZero(); ) {
var m = p.modn(d).toString(e);
i = (p = p.idivn(d)).isZero() ? m + i : h[l - m.length] + m + i;
}
this.isZero() && (i = "0" + i);
for (;i.length % t != 0; ) i = "0" + i;
0 !== this.negative && (i = "-" + i);
return i;
}
n(!1, "Base should be between 2 and 36");
};
o.prototype.toNumber = function() {
var e = this.words[0];
2 === this.length ? e += 67108864 * this.words[1] : 3 === this.length && 1 === this.words[2] ? e += 4503599627370496 + 67108864 * this.words[1] : this.length > 2 && n(!1, "Number can only safely store up to 53 bits");
return 0 !== this.negative ? -e : e;
};
o.prototype.toJSON = function() {
return this.toString(16);
};
o.prototype.toBuffer = function(e, t) {
n("undefined" != typeof a);
return this.toArrayLike(a, e, t);
};
o.prototype.toArray = function(e, t) {
return this.toArrayLike(Array, e, t);
};
o.prototype.toArrayLike = function(e, t, i) {
var r = this.byteLength(), o = i || Math.max(1, r);
n(r <= o, "byte array longer than desired length");
n(o > 0, "Requested array length <= 0");
this.strip();
var a, s, c = "le" === t, l = new e(o), h = this.clone();
if (c) {
for (s = 0; !h.isZero(); s++) {
a = h.andln(255);
h.iushrn(8);
l[s] = a;
}
for (;s < o; s++) l[s] = 0;
} else {
for (s = 0; s < o - r; s++) l[s] = 0;
for (s = 0; !h.isZero(); s++) {
a = h.andln(255);
h.iushrn(8);
l[o - s - 1] = a;
}
}
return l;
};
Math.clz32 ? o.prototype._countBits = function(e) {
return 32 - Math.clz32(e);
} : o.prototype._countBits = function(e) {
var t = e, i = 0;
if (t >= 4096) {
i += 13;
t >>>= 13;
}
if (t >= 64) {
i += 7;
t >>>= 7;
}
if (t >= 8) {
i += 4;
t >>>= 4;
}
if (t >= 2) {
i += 2;
t >>>= 2;
}
return i + t;
};
o.prototype._zeroBits = function(e) {
if (0 === e) return 26;
var t = e, i = 0;
if (0 == (8191 & t)) {
i += 13;
t >>>= 13;
}
if (0 == (127 & t)) {
i += 7;
t >>>= 7;
}
if (0 == (15 & t)) {
i += 4;
t >>>= 4;
}
if (0 == (3 & t)) {
i += 2;
t >>>= 2;
}
0 == (1 & t) && i++;
return i;
};
o.prototype.bitLength = function() {
var e = this.words[this.length - 1], t = this._countBits(e);
return 26 * (this.length - 1) + t;
};
function d(e) {
for (var t = new Array(e.bitLength()), i = 0; i < t.length; i++) {
var n = i / 26 | 0, r = i % 26;
t[i] = (e.words[n] & 1 << r) >>> r;
}
return t;
}
o.prototype.zeroBits = function() {
if (this.isZero()) return 0;
for (var e = 0, t = 0; t < this.length; t++) {
var i = this._zeroBits(this.words[t]);
e += i;
if (26 !== i) break;
}
return e;
};
o.prototype.byteLength = function() {
return Math.ceil(this.bitLength() / 8);
};
o.prototype.toTwos = function(e) {
return 0 !== this.negative ? this.abs().inotn(e).iaddn(1) : this.clone();
};
o.prototype.fromTwos = function(e) {
return this.testn(e - 1) ? this.notn(e).iaddn(1).ineg() : this.clone();
};
o.prototype.isNeg = function() {
return 0 !== this.negative;
};
o.prototype.neg = function() {
return this.clone().ineg();
};
o.prototype.ineg = function() {
this.isZero() || (this.negative ^= 1);
return this;
};
o.prototype.iuor = function(e) {
for (;this.length < e.length; ) this.words[this.length++] = 0;
for (var t = 0; t < e.length; t++) this.words[t] = this.words[t] | e.words[t];
return this.strip();
};
o.prototype.ior = function(e) {
n(0 == (this.negative | e.negative));
return this.iuor(e);
};
o.prototype.or = function(e) {
return this.length > e.length ? this.clone().ior(e) : e.clone().ior(this);
};
o.prototype.uor = function(e) {
return this.length > e.length ? this.clone().iuor(e) : e.clone().iuor(this);
};
o.prototype.iuand = function(e) {
var t;
t = this.length > e.length ? e : this;
for (var i = 0; i < t.length; i++) this.words[i] = this.words[i] & e.words[i];
this.length = t.length;
return this.strip();
};
o.prototype.iand = function(e) {
n(0 == (this.negative | e.negative));
return this.iuand(e);
};
o.prototype.and = function(e) {
return this.length > e.length ? this.clone().iand(e) : e.clone().iand(this);
};
o.prototype.uand = function(e) {
return this.length > e.length ? this.clone().iuand(e) : e.clone().iuand(this);
};
o.prototype.iuxor = function(e) {
var t, i;
if (this.length > e.length) {
t = this;
i = e;
} else {
t = e;
i = this;
}
for (var n = 0; n < i.length; n++) this.words[n] = t.words[n] ^ i.words[n];
if (this !== t) for (;n < t.length; n++) this.words[n] = t.words[n];
this.length = t.length;
return this.strip();
};
o.prototype.ixor = function(e) {
n(0 == (this.negative | e.negative));
return this.iuxor(e);
};
o.prototype.xor = function(e) {
return this.length > e.length ? this.clone().ixor(e) : e.clone().ixor(this);
};
o.prototype.uxor = function(e) {
return this.length > e.length ? this.clone().iuxor(e) : e.clone().iuxor(this);
};
o.prototype.inotn = function(e) {
n("number" == typeof e && e >= 0);
var t = 0 | Math.ceil(e / 26), i = e % 26;
this._expand(t);
i > 0 && t--;
for (var r = 0; r < t; r++) this.words[r] = 67108863 & ~this.words[r];
i > 0 && (this.words[r] = ~this.words[r] & 67108863 >> 26 - i);
return this.strip();
};
o.prototype.notn = function(e) {
return this.clone().inotn(e);
};
o.prototype.setn = function(e, t) {
n("number" == typeof e && e >= 0);
var i = e / 26 | 0, r = e % 26;
this._expand(i + 1);
this.words[i] = t ? this.words[i] | 1 << r : this.words[i] & ~(1 << r);
return this.strip();
};
o.prototype.iadd = function(e) {
var t, i, n;
if (0 !== this.negative && 0 === e.negative) {
this.negative = 0;
t = this.isub(e);
this.negative ^= 1;
return this._normSign();
}
if (0 === this.negative && 0 !== e.negative) {
e.negative = 0;
t = this.isub(e);
e.negative = 1;
return t._normSign();
}
if (this.length > e.length) {
i = this;
n = e;
} else {
i = e;
n = this;
}
for (var r = 0, o = 0; o < n.length; o++) {
t = (0 | i.words[o]) + (0 | n.words[o]) + r;
this.words[o] = 67108863 & t;
r = t >>> 26;
}
for (;0 !== r && o < i.length; o++) {
t = (0 | i.words[o]) + r;
this.words[o] = 67108863 & t;
r = t >>> 26;
}
this.length = i.length;
if (0 !== r) {
this.words[this.length] = r;
this.length++;
} else if (i !== this) for (;o < i.length; o++) this.words[o] = i.words[o];
return this;
};
o.prototype.add = function(e) {
var t;
if (0 !== e.negative && 0 === this.negative) {
e.negative = 0;
t = this.sub(e);
e.negative ^= 1;
return t;
}
if (0 === e.negative && 0 !== this.negative) {
this.negative = 0;
t = e.sub(this);
this.negative = 1;
return t;
}
return this.length > e.length ? this.clone().iadd(e) : e.clone().iadd(this);
};
o.prototype.isub = function(e) {
if (0 !== e.negative) {
e.negative = 0;
var t = this.iadd(e);
e.negative = 1;
return t._normSign();
}
if (0 !== this.negative) {
this.negative = 0;
this.iadd(e);
this.negative = 1;
return this._normSign();
}
var i, n, r = this.cmp(e);
if (0 === r) {
this.negative = 0;
this.length = 1;
this.words[0] = 0;
return this;
}
if (r > 0) {
i = this;
n = e;
} else {
i = e;
n = this;
}
for (var o = 0, a = 0; a < n.length; a++) {
o = (t = (0 | i.words[a]) - (0 | n.words[a]) + o) >> 26;
this.words[a] = 67108863 & t;
}
for (;0 !== o && a < i.length; a++) {
o = (t = (0 | i.words[a]) + o) >> 26;
this.words[a] = 67108863 & t;
}
if (0 === o && a < i.length && i !== this) for (;a < i.length; a++) this.words[a] = i.words[a];
this.length = Math.max(this.length, a);
i !== this && (this.negative = 1);
return this.strip();
};
o.prototype.sub = function(e) {
return this.clone().isub(e);
};
function p(e, t, i) {
i.negative = t.negative ^ e.negative;
var n = e.length + t.length | 0;
i.length = n;
n = n - 1 | 0;
var r = 0 | e.words[0], o = 0 | t.words[0], a = r * o, s = 67108863 & a, c = a / 67108864 | 0;
i.words[0] = s;
for (var l = 1; l < n; l++) {
for (var h = c >>> 26, u = 67108863 & c, f = Math.min(l, t.length - 1), d = Math.max(0, l - e.length + 1); d <= f; d++) {
var p = l - d | 0;
h += (a = (r = 0 | e.words[p]) * (o = 0 | t.words[d]) + u) / 67108864 | 0;
u = 67108863 & a;
}
i.words[l] = 0 | u;
c = 0 | h;
}
0 !== c ? i.words[l] = 0 | c : i.length--;
return i.strip();
}
var m = function(e, t, i) {
var n, r, o, a = e.words, s = t.words, c = i.words, l = 0, h = 0 | a[0], u = 8191 & h, f = h >>> 13, d = 0 | a[1], p = 8191 & d, m = d >>> 13, g = 0 | a[2], y = 8191 & g, b = g >>> 13, v = 0 | a[3], _ = 8191 & v, w = v >>> 13, C = 0 | a[4], S = 8191 & C, A = C >>> 13, M = 0 | a[5], B = 8191 & M, x = M >>> 13, k = 0 | a[6], T = 8191 & k, D = k >>> 13, E = 0 | a[7], L = 8191 & E, P = E >>> 13, R = 0 | a[8], I = 8191 & R, N = R >>> 13, O = 0 | a[9], U = 8191 & O, j = O >>> 13, F = 0 | s[0], G = 8191 & F, V = F >>> 13, z = 0 | s[1], H = 8191 & z, W = z >>> 13, q = 0 | s[2], X = 8191 & q, K = q >>> 13, J = 0 | s[3], Y = 8191 & J, Z = J >>> 13, Q = 0 | s[4], $ = 8191 & Q, ee = Q >>> 13, te = 0 | s[5], ie = 8191 & te, ne = te >>> 13, re = 0 | s[6], oe = 8191 & re, ae = re >>> 13, se = 0 | s[7], ce = 8191 & se, le = se >>> 13, he = 0 | s[8], ue = 8191 & he, fe = he >>> 13, de = 0 | s[9], pe = 8191 & de, me = de >>> 13;
i.negative = e.negative ^ t.negative;
i.length = 19;
var ge = (l + (n = Math.imul(u, G)) | 0) + ((8191 & (r = (r = Math.imul(u, V)) + Math.imul(f, G) | 0)) << 13) | 0;
l = ((o = Math.imul(f, V)) + (r >>> 13) | 0) + (ge >>> 26) | 0;
ge &= 67108863;
n = Math.imul(p, G);
r = (r = Math.imul(p, V)) + Math.imul(m, G) | 0;
o = Math.imul(m, V);
var ye = (l + (n = n + Math.imul(u, H) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, W) | 0) + Math.imul(f, H) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, W) | 0) + (r >>> 13) | 0) + (ye >>> 26) | 0;
ye &= 67108863;
n = Math.imul(y, G);
r = (r = Math.imul(y, V)) + Math.imul(b, G) | 0;
o = Math.imul(b, V);
n = n + Math.imul(p, H) | 0;
r = (r = r + Math.imul(p, W) | 0) + Math.imul(m, H) | 0;
o = o + Math.imul(m, W) | 0;
var be = (l + (n = n + Math.imul(u, X) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, K) | 0) + Math.imul(f, X) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, K) | 0) + (r >>> 13) | 0) + (be >>> 26) | 0;
be &= 67108863;
n = Math.imul(_, G);
r = (r = Math.imul(_, V)) + Math.imul(w, G) | 0;
o = Math.imul(w, V);
n = n + Math.imul(y, H) | 0;
r = (r = r + Math.imul(y, W) | 0) + Math.imul(b, H) | 0;
o = o + Math.imul(b, W) | 0;
n = n + Math.imul(p, X) | 0;
r = (r = r + Math.imul(p, K) | 0) + Math.imul(m, X) | 0;
o = o + Math.imul(m, K) | 0;
var ve = (l + (n = n + Math.imul(u, Y) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, Z) | 0) + Math.imul(f, Y) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, Z) | 0) + (r >>> 13) | 0) + (ve >>> 26) | 0;
ve &= 67108863;
n = Math.imul(S, G);
r = (r = Math.imul(S, V)) + Math.imul(A, G) | 0;
o = Math.imul(A, V);
n = n + Math.imul(_, H) | 0;
r = (r = r + Math.imul(_, W) | 0) + Math.imul(w, H) | 0;
o = o + Math.imul(w, W) | 0;
n = n + Math.imul(y, X) | 0;
r = (r = r + Math.imul(y, K) | 0) + Math.imul(b, X) | 0;
o = o + Math.imul(b, K) | 0;
n = n + Math.imul(p, Y) | 0;
r = (r = r + Math.imul(p, Z) | 0) + Math.imul(m, Y) | 0;
o = o + Math.imul(m, Z) | 0;
var _e = (l + (n = n + Math.imul(u, $) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, ee) | 0) + Math.imul(f, $) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, ee) | 0) + (r >>> 13) | 0) + (_e >>> 26) | 0;
_e &= 67108863;
n = Math.imul(B, G);
r = (r = Math.imul(B, V)) + Math.imul(x, G) | 0;
o = Math.imul(x, V);
n = n + Math.imul(S, H) | 0;
r = (r = r + Math.imul(S, W) | 0) + Math.imul(A, H) | 0;
o = o + Math.imul(A, W) | 0;
n = n + Math.imul(_, X) | 0;
r = (r = r + Math.imul(_, K) | 0) + Math.imul(w, X) | 0;
o = o + Math.imul(w, K) | 0;
n = n + Math.imul(y, Y) | 0;
r = (r = r + Math.imul(y, Z) | 0) + Math.imul(b, Y) | 0;
o = o + Math.imul(b, Z) | 0;
n = n + Math.imul(p, $) | 0;
r = (r = r + Math.imul(p, ee) | 0) + Math.imul(m, $) | 0;
o = o + Math.imul(m, ee) | 0;
var we = (l + (n = n + Math.imul(u, ie) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, ne) | 0) + Math.imul(f, ie) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, ne) | 0) + (r >>> 13) | 0) + (we >>> 26) | 0;
we &= 67108863;
n = Math.imul(T, G);
r = (r = Math.imul(T, V)) + Math.imul(D, G) | 0;
o = Math.imul(D, V);
n = n + Math.imul(B, H) | 0;
r = (r = r + Math.imul(B, W) | 0) + Math.imul(x, H) | 0;
o = o + Math.imul(x, W) | 0;
n = n + Math.imul(S, X) | 0;
r = (r = r + Math.imul(S, K) | 0) + Math.imul(A, X) | 0;
o = o + Math.imul(A, K) | 0;
n = n + Math.imul(_, Y) | 0;
r = (r = r + Math.imul(_, Z) | 0) + Math.imul(w, Y) | 0;
o = o + Math.imul(w, Z) | 0;
n = n + Math.imul(y, $) | 0;
r = (r = r + Math.imul(y, ee) | 0) + Math.imul(b, $) | 0;
o = o + Math.imul(b, ee) | 0;
n = n + Math.imul(p, ie) | 0;
r = (r = r + Math.imul(p, ne) | 0) + Math.imul(m, ie) | 0;
o = o + Math.imul(m, ne) | 0;
var Ce = (l + (n = n + Math.imul(u, oe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, ae) | 0) + Math.imul(f, oe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, ae) | 0) + (r >>> 13) | 0) + (Ce >>> 26) | 0;
Ce &= 67108863;
n = Math.imul(L, G);
r = (r = Math.imul(L, V)) + Math.imul(P, G) | 0;
o = Math.imul(P, V);
n = n + Math.imul(T, H) | 0;
r = (r = r + Math.imul(T, W) | 0) + Math.imul(D, H) | 0;
o = o + Math.imul(D, W) | 0;
n = n + Math.imul(B, X) | 0;
r = (r = r + Math.imul(B, K) | 0) + Math.imul(x, X) | 0;
o = o + Math.imul(x, K) | 0;
n = n + Math.imul(S, Y) | 0;
r = (r = r + Math.imul(S, Z) | 0) + Math.imul(A, Y) | 0;
o = o + Math.imul(A, Z) | 0;
n = n + Math.imul(_, $) | 0;
r = (r = r + Math.imul(_, ee) | 0) + Math.imul(w, $) | 0;
o = o + Math.imul(w, ee) | 0;
n = n + Math.imul(y, ie) | 0;
r = (r = r + Math.imul(y, ne) | 0) + Math.imul(b, ie) | 0;
o = o + Math.imul(b, ne) | 0;
n = n + Math.imul(p, oe) | 0;
r = (r = r + Math.imul(p, ae) | 0) + Math.imul(m, oe) | 0;
o = o + Math.imul(m, ae) | 0;
var Se = (l + (n = n + Math.imul(u, ce) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, le) | 0) + Math.imul(f, ce) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, le) | 0) + (r >>> 13) | 0) + (Se >>> 26) | 0;
Se &= 67108863;
n = Math.imul(I, G);
r = (r = Math.imul(I, V)) + Math.imul(N, G) | 0;
o = Math.imul(N, V);
n = n + Math.imul(L, H) | 0;
r = (r = r + Math.imul(L, W) | 0) + Math.imul(P, H) | 0;
o = o + Math.imul(P, W) | 0;
n = n + Math.imul(T, X) | 0;
r = (r = r + Math.imul(T, K) | 0) + Math.imul(D, X) | 0;
o = o + Math.imul(D, K) | 0;
n = n + Math.imul(B, Y) | 0;
r = (r = r + Math.imul(B, Z) | 0) + Math.imul(x, Y) | 0;
o = o + Math.imul(x, Z) | 0;
n = n + Math.imul(S, $) | 0;
r = (r = r + Math.imul(S, ee) | 0) + Math.imul(A, $) | 0;
o = o + Math.imul(A, ee) | 0;
n = n + Math.imul(_, ie) | 0;
r = (r = r + Math.imul(_, ne) | 0) + Math.imul(w, ie) | 0;
o = o + Math.imul(w, ne) | 0;
n = n + Math.imul(y, oe) | 0;
r = (r = r + Math.imul(y, ae) | 0) + Math.imul(b, oe) | 0;
o = o + Math.imul(b, ae) | 0;
n = n + Math.imul(p, ce) | 0;
r = (r = r + Math.imul(p, le) | 0) + Math.imul(m, ce) | 0;
o = o + Math.imul(m, le) | 0;
var Ae = (l + (n = n + Math.imul(u, ue) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, fe) | 0) + Math.imul(f, ue) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, fe) | 0) + (r >>> 13) | 0) + (Ae >>> 26) | 0;
Ae &= 67108863;
n = Math.imul(U, G);
r = (r = Math.imul(U, V)) + Math.imul(j, G) | 0;
o = Math.imul(j, V);
n = n + Math.imul(I, H) | 0;
r = (r = r + Math.imul(I, W) | 0) + Math.imul(N, H) | 0;
o = o + Math.imul(N, W) | 0;
n = n + Math.imul(L, X) | 0;
r = (r = r + Math.imul(L, K) | 0) + Math.imul(P, X) | 0;
o = o + Math.imul(P, K) | 0;
n = n + Math.imul(T, Y) | 0;
r = (r = r + Math.imul(T, Z) | 0) + Math.imul(D, Y) | 0;
o = o + Math.imul(D, Z) | 0;
n = n + Math.imul(B, $) | 0;
r = (r = r + Math.imul(B, ee) | 0) + Math.imul(x, $) | 0;
o = o + Math.imul(x, ee) | 0;
n = n + Math.imul(S, ie) | 0;
r = (r = r + Math.imul(S, ne) | 0) + Math.imul(A, ie) | 0;
o = o + Math.imul(A, ne) | 0;
n = n + Math.imul(_, oe) | 0;
r = (r = r + Math.imul(_, ae) | 0) + Math.imul(w, oe) | 0;
o = o + Math.imul(w, ae) | 0;
n = n + Math.imul(y, ce) | 0;
r = (r = r + Math.imul(y, le) | 0) + Math.imul(b, ce) | 0;
o = o + Math.imul(b, le) | 0;
n = n + Math.imul(p, ue) | 0;
r = (r = r + Math.imul(p, fe) | 0) + Math.imul(m, ue) | 0;
o = o + Math.imul(m, fe) | 0;
var Me = (l + (n = n + Math.imul(u, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, me) | 0) + Math.imul(f, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, me) | 0) + (r >>> 13) | 0) + (Me >>> 26) | 0;
Me &= 67108863;
n = Math.imul(U, H);
r = (r = Math.imul(U, W)) + Math.imul(j, H) | 0;
o = Math.imul(j, W);
n = n + Math.imul(I, X) | 0;
r = (r = r + Math.imul(I, K) | 0) + Math.imul(N, X) | 0;
o = o + Math.imul(N, K) | 0;
n = n + Math.imul(L, Y) | 0;
r = (r = r + Math.imul(L, Z) | 0) + Math.imul(P, Y) | 0;
o = o + Math.imul(P, Z) | 0;
n = n + Math.imul(T, $) | 0;
r = (r = r + Math.imul(T, ee) | 0) + Math.imul(D, $) | 0;
o = o + Math.imul(D, ee) | 0;
n = n + Math.imul(B, ie) | 0;
r = (r = r + Math.imul(B, ne) | 0) + Math.imul(x, ie) | 0;
o = o + Math.imul(x, ne) | 0;
n = n + Math.imul(S, oe) | 0;
r = (r = r + Math.imul(S, ae) | 0) + Math.imul(A, oe) | 0;
o = o + Math.imul(A, ae) | 0;
n = n + Math.imul(_, ce) | 0;
r = (r = r + Math.imul(_, le) | 0) + Math.imul(w, ce) | 0;
o = o + Math.imul(w, le) | 0;
n = n + Math.imul(y, ue) | 0;
r = (r = r + Math.imul(y, fe) | 0) + Math.imul(b, ue) | 0;
o = o + Math.imul(b, fe) | 0;
var Be = (l + (n = n + Math.imul(p, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(p, me) | 0) + Math.imul(m, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(m, me) | 0) + (r >>> 13) | 0) + (Be >>> 26) | 0;
Be &= 67108863;
n = Math.imul(U, X);
r = (r = Math.imul(U, K)) + Math.imul(j, X) | 0;
o = Math.imul(j, K);
n = n + Math.imul(I, Y) | 0;
r = (r = r + Math.imul(I, Z) | 0) + Math.imul(N, Y) | 0;
o = o + Math.imul(N, Z) | 0;
n = n + Math.imul(L, $) | 0;
r = (r = r + Math.imul(L, ee) | 0) + Math.imul(P, $) | 0;
o = o + Math.imul(P, ee) | 0;
n = n + Math.imul(T, ie) | 0;
r = (r = r + Math.imul(T, ne) | 0) + Math.imul(D, ie) | 0;
o = o + Math.imul(D, ne) | 0;
n = n + Math.imul(B, oe) | 0;
r = (r = r + Math.imul(B, ae) | 0) + Math.imul(x, oe) | 0;
o = o + Math.imul(x, ae) | 0;
n = n + Math.imul(S, ce) | 0;
r = (r = r + Math.imul(S, le) | 0) + Math.imul(A, ce) | 0;
o = o + Math.imul(A, le) | 0;
n = n + Math.imul(_, ue) | 0;
r = (r = r + Math.imul(_, fe) | 0) + Math.imul(w, ue) | 0;
o = o + Math.imul(w, fe) | 0;
var xe = (l + (n = n + Math.imul(y, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(y, me) | 0) + Math.imul(b, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(b, me) | 0) + (r >>> 13) | 0) + (xe >>> 26) | 0;
xe &= 67108863;
n = Math.imul(U, Y);
r = (r = Math.imul(U, Z)) + Math.imul(j, Y) | 0;
o = Math.imul(j, Z);
n = n + Math.imul(I, $) | 0;
r = (r = r + Math.imul(I, ee) | 0) + Math.imul(N, $) | 0;
o = o + Math.imul(N, ee) | 0;
n = n + Math.imul(L, ie) | 0;
r = (r = r + Math.imul(L, ne) | 0) + Math.imul(P, ie) | 0;
o = o + Math.imul(P, ne) | 0;
n = n + Math.imul(T, oe) | 0;
r = (r = r + Math.imul(T, ae) | 0) + Math.imul(D, oe) | 0;
o = o + Math.imul(D, ae) | 0;
n = n + Math.imul(B, ce) | 0;
r = (r = r + Math.imul(B, le) | 0) + Math.imul(x, ce) | 0;
o = o + Math.imul(x, le) | 0;
n = n + Math.imul(S, ue) | 0;
r = (r = r + Math.imul(S, fe) | 0) + Math.imul(A, ue) | 0;
o = o + Math.imul(A, fe) | 0;
var ke = (l + (n = n + Math.imul(_, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(_, me) | 0) + Math.imul(w, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(w, me) | 0) + (r >>> 13) | 0) + (ke >>> 26) | 0;
ke &= 67108863;
n = Math.imul(U, $);
r = (r = Math.imul(U, ee)) + Math.imul(j, $) | 0;
o = Math.imul(j, ee);
n = n + Math.imul(I, ie) | 0;
r = (r = r + Math.imul(I, ne) | 0) + Math.imul(N, ie) | 0;
o = o + Math.imul(N, ne) | 0;
n = n + Math.imul(L, oe) | 0;
r = (r = r + Math.imul(L, ae) | 0) + Math.imul(P, oe) | 0;
o = o + Math.imul(P, ae) | 0;
n = n + Math.imul(T, ce) | 0;
r = (r = r + Math.imul(T, le) | 0) + Math.imul(D, ce) | 0;
o = o + Math.imul(D, le) | 0;
n = n + Math.imul(B, ue) | 0;
r = (r = r + Math.imul(B, fe) | 0) + Math.imul(x, ue) | 0;
o = o + Math.imul(x, fe) | 0;
var Te = (l + (n = n + Math.imul(S, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(S, me) | 0) + Math.imul(A, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(A, me) | 0) + (r >>> 13) | 0) + (Te >>> 26) | 0;
Te &= 67108863;
n = Math.imul(U, ie);
r = (r = Math.imul(U, ne)) + Math.imul(j, ie) | 0;
o = Math.imul(j, ne);
n = n + Math.imul(I, oe) | 0;
r = (r = r + Math.imul(I, ae) | 0) + Math.imul(N, oe) | 0;
o = o + Math.imul(N, ae) | 0;
n = n + Math.imul(L, ce) | 0;
r = (r = r + Math.imul(L, le) | 0) + Math.imul(P, ce) | 0;
o = o + Math.imul(P, le) | 0;
n = n + Math.imul(T, ue) | 0;
r = (r = r + Math.imul(T, fe) | 0) + Math.imul(D, ue) | 0;
o = o + Math.imul(D, fe) | 0;
var De = (l + (n = n + Math.imul(B, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(B, me) | 0) + Math.imul(x, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(x, me) | 0) + (r >>> 13) | 0) + (De >>> 26) | 0;
De &= 67108863;
n = Math.imul(U, oe);
r = (r = Math.imul(U, ae)) + Math.imul(j, oe) | 0;
o = Math.imul(j, ae);
n = n + Math.imul(I, ce) | 0;
r = (r = r + Math.imul(I, le) | 0) + Math.imul(N, ce) | 0;
o = o + Math.imul(N, le) | 0;
n = n + Math.imul(L, ue) | 0;
r = (r = r + Math.imul(L, fe) | 0) + Math.imul(P, ue) | 0;
o = o + Math.imul(P, fe) | 0;
var Ee = (l + (n = n + Math.imul(T, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(T, me) | 0) + Math.imul(D, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(D, me) | 0) + (r >>> 13) | 0) + (Ee >>> 26) | 0;
Ee &= 67108863;
n = Math.imul(U, ce);
r = (r = Math.imul(U, le)) + Math.imul(j, ce) | 0;
o = Math.imul(j, le);
n = n + Math.imul(I, ue) | 0;
r = (r = r + Math.imul(I, fe) | 0) + Math.imul(N, ue) | 0;
o = o + Math.imul(N, fe) | 0;
var Le = (l + (n = n + Math.imul(L, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(L, me) | 0) + Math.imul(P, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(P, me) | 0) + (r >>> 13) | 0) + (Le >>> 26) | 0;
Le &= 67108863;
n = Math.imul(U, ue);
r = (r = Math.imul(U, fe)) + Math.imul(j, ue) | 0;
o = Math.imul(j, fe);
var Pe = (l + (n = n + Math.imul(I, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(I, me) | 0) + Math.imul(N, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(N, me) | 0) + (r >>> 13) | 0) + (Pe >>> 26) | 0;
Pe &= 67108863;
var Re = (l + (n = Math.imul(U, pe)) | 0) + ((8191 & (r = (r = Math.imul(U, me)) + Math.imul(j, pe) | 0)) << 13) | 0;
l = ((o = Math.imul(j, me)) + (r >>> 13) | 0) + (Re >>> 26) | 0;
Re &= 67108863;
c[0] = ge;
c[1] = ye;
c[2] = be;
c[3] = ve;
c[4] = _e;
c[5] = we;
c[6] = Ce;
c[7] = Se;
c[8] = Ae;
c[9] = Me;
c[10] = Be;
c[11] = xe;
c[12] = ke;
c[13] = Te;
c[14] = De;
c[15] = Ee;
c[16] = Le;
c[17] = Pe;
c[18] = Re;
if (0 !== l) {
c[19] = l;
i.length++;
}
return i;
};
Math.imul || (m = p);
function g(e, t, i) {
i.negative = t.negative ^ e.negative;
i.length = e.length + t.length;
for (var n = 0, r = 0, o = 0; o < i.length - 1; o++) {
var a = r;
r = 0;
for (var s = 67108863 & n, c = Math.min(o, t.length - 1), l = Math.max(0, o - e.length + 1); l <= c; l++) {
var h = o - l, u = (0 | e.words[h]) * (0 | t.words[l]), f = 67108863 & u;
s = 67108863 & (f = f + s | 0);
r += (a = (a = a + (u / 67108864 | 0) | 0) + (f >>> 26) | 0) >>> 26;
a &= 67108863;
}
i.words[o] = s;
n = a;
a = r;
}
0 !== n ? i.words[o] = n : i.length--;
return i.strip();
}
function y(e, t, i) {
return new b().mulp(e, t, i);
}
o.prototype.mulTo = function(e, t) {
var i = this.length + e.length;
return 10 === this.length && 10 === e.length ? m(this, e, t) : i < 63 ? p(this, e, t) : i < 1024 ? g(this, e, t) : y(this, e, t);
};
function b(e, t) {
this.x = e;
this.y = t;
}
b.prototype.makeRBT = function(e) {
for (var t = new Array(e), i = o.prototype._countBits(e) - 1, n = 0; n < e; n++) t[n] = this.revBin(n, i, e);
return t;
};
b.prototype.revBin = function(e, t, i) {
if (0 === e || e === i - 1) return e;
for (var n = 0, r = 0; r < t; r++) {
n |= (1 & e) << t - r - 1;
e >>= 1;
}
return n;
};
b.prototype.permute = function(e, t, i, n, r, o) {
for (var a = 0; a < o; a++) {
n[a] = t[e[a]];
r[a] = i[e[a]];
}
};
b.prototype.transform = function(e, t, i, n, r, o) {
this.permute(o, e, t, i, n, r);
for (var a = 1; a < r; a <<= 1) for (var s = a << 1, c = Math.cos(2 * Math.PI / s), l = Math.sin(2 * Math.PI / s), h = 0; h < r; h += s) for (var u = c, f = l, d = 0; d < a; d++) {
var p = i[h + d], m = n[h + d], g = i[h + d + a], y = n[h + d + a], b = u * g - f * y;
y = u * y + f * g;
g = b;
i[h + d] = p + g;
n[h + d] = m + y;
i[h + d + a] = p - g;
n[h + d + a] = m - y;
if (d !== s) {
b = c * u - l * f;
f = c * f + l * u;
u = b;
}
}
};
b.prototype.guessLen13b = function(e, t) {
var i = 1 | Math.max(t, e), n = 1 & i, r = 0;
for (i = i / 2 | 0; i; i >>>= 1) r++;
return 1 << r + 1 + n;
};
b.prototype.conjugate = function(e, t, i) {
if (!(i <= 1)) for (var n = 0; n < i / 2; n++) {
var r = e[n];
e[n] = e[i - n - 1];
e[i - n - 1] = r;
r = t[n];
t[n] = -t[i - n - 1];
t[i - n - 1] = -r;
}
};
b.prototype.normalize13b = function(e, t) {
for (var i = 0, n = 0; n < t / 2; n++) {
var r = 8192 * Math.round(e[2 * n + 1] / t) + Math.round(e[2 * n] / t) + i;
e[n] = 67108863 & r;
i = r < 67108864 ? 0 : r / 67108864 | 0;
}
return e;
};
b.prototype.convert13b = function(e, t, i, r) {
for (var o = 0, a = 0; a < t; a++) {
o += 0 | e[a];
i[2 * a] = 8191 & o;
o >>>= 13;
i[2 * a + 1] = 8191 & o;
o >>>= 13;
}
for (a = 2 * t; a < r; ++a) i[a] = 0;
n(0 === o);
n(0 == (-8192 & o));
};
b.prototype.stub = function(e) {
for (var t = new Array(e), i = 0; i < e; i++) t[i] = 0;
return t;
};
b.prototype.mulp = function(e, t, i) {
var n = 2 * this.guessLen13b(e.length, t.length), r = this.makeRBT(n), o = this.stub(n), a = new Array(n), s = new Array(n), c = new Array(n), l = new Array(n), h = new Array(n), u = new Array(n), f = i.words;
f.length = n;
this.convert13b(e.words, e.length, a, n);
this.convert13b(t.words, t.length, l, n);
this.transform(a, o, s, c, n, r);
this.transform(l, o, h, u, n, r);
for (var d = 0; d < n; d++) {
var p = s[d] * h[d] - c[d] * u[d];
c[d] = s[d] * u[d] + c[d] * h[d];
s[d] = p;
}
this.conjugate(s, c, n);
this.transform(s, c, f, o, n, r);
this.conjugate(f, o, n);
this.normalize13b(f, n);
i.negative = e.negative ^ t.negative;
i.length = e.length + t.length;
return i.strip();
};
o.prototype.mul = function(e) {
var t = new o(null);
t.words = new Array(this.length + e.length);
return this.mulTo(e, t);
};
o.prototype.mulf = function(e) {
var t = new o(null);
t.words = new Array(this.length + e.length);
return y(this, e, t);
};
o.prototype.imul = function(e) {
return this.clone().mulTo(e, this);
};
o.prototype.imuln = function(e) {
n("number" == typeof e);
n(e < 67108864);
for (var t = 0, i = 0; i < this.length; i++) {
var r = (0 | this.words[i]) * e, o = (67108863 & r) + (67108863 & t);
t >>= 26;
t += r / 67108864 | 0;
t += o >>> 26;
this.words[i] = 67108863 & o;
}
if (0 !== t) {
this.words[i] = t;
this.length++;
}
return this;
};
o.prototype.muln = function(e) {
return this.clone().imuln(e);
};
o.prototype.sqr = function() {
return this.mul(this);
};
o.prototype.isqr = function() {
return this.imul(this.clone());
};
o.prototype.pow = function(e) {
var t = d(e);
if (0 === t.length) return new o(1);
for (var i = this, n = 0; n < t.length && 0 === t[n]; n++, i = i.sqr()) ;
if (++n < t.length) for (var r = i.sqr(); n < t.length; n++, r = r.sqr()) 0 !== t[n] && (i = i.mul(r));
return i;
};
o.prototype.iushln = function(e) {
n("number" == typeof e && e >= 0);
var t, i = e % 26, r = (e - i) / 26, o = 67108863 >>> 26 - i << 26 - i;
if (0 !== i) {
var a = 0;
for (t = 0; t < this.length; t++) {
var s = this.words[t] & o, c = (0 | this.words[t]) - s << i;
this.words[t] = c | a;
a = s >>> 26 - i;
}
if (a) {
this.words[t] = a;
this.length++;
}
}
if (0 !== r) {
for (t = this.length - 1; t >= 0; t--) this.words[t + r] = this.words[t];
for (t = 0; t < r; t++) this.words[t] = 0;
this.length += r;
}
return this.strip();
};
o.prototype.ishln = function(e) {
n(0 === this.negative);
return this.iushln(e);
};
o.prototype.iushrn = function(e, t, i) {
n("number" == typeof e && e >= 0);
var r;
r = t ? (t - t % 26) / 26 : 0;
var o = e % 26, a = Math.min((e - o) / 26, this.length), s = 67108863 ^ 67108863 >>> o << o, c = i;
r -= a;
r = Math.max(0, r);
if (c) {
for (var l = 0; l < a; l++) c.words[l] = this.words[l];
c.length = a;
}
if (0 === a) ; else if (this.length > a) {
this.length -= a;
for (l = 0; l < this.length; l++) this.words[l] = this.words[l + a];
} else {
this.words[0] = 0;
this.length = 1;
}
var h = 0;
for (l = this.length - 1; l >= 0 && (0 !== h || l >= r); l--) {
var u = 0 | this.words[l];
this.words[l] = h << 26 - o | u >>> o;
h = u & s;
}
c && 0 !== h && (c.words[c.length++] = h);
if (0 === this.length) {
this.words[0] = 0;
this.length = 1;
}
return this.strip();
};
o.prototype.ishrn = function(e, t, i) {
n(0 === this.negative);
return this.iushrn(e, t, i);
};
o.prototype.shln = function(e) {
return this.clone().ishln(e);
};
o.prototype.ushln = function(e) {
return this.clone().iushln(e);
};
o.prototype.shrn = function(e) {
return this.clone().ishrn(e);
};
o.prototype.ushrn = function(e) {
return this.clone().iushrn(e);
};
o.prototype.testn = function(e) {
n("number" == typeof e && e >= 0);
var t = e % 26, i = (e - t) / 26, r = 1 << t;
return !(this.length <= i || !(this.words[i] & r));
};
o.prototype.imaskn = function(e) {
n("number" == typeof e && e >= 0);
var t = e % 26, i = (e - t) / 26;
n(0 === this.negative, "imaskn works only with positive numbers");
if (this.length <= i) return this;
0 !== t && i++;
this.length = Math.min(i, this.length);
if (0 !== t) {
var r = 67108863 ^ 67108863 >>> t << t;
this.words[this.length - 1] &= r;
}
return this.strip();
};
o.prototype.maskn = function(e) {
return this.clone().imaskn(e);
};
o.prototype.iaddn = function(e) {
n("number" == typeof e);
n(e < 67108864);
if (e < 0) return this.isubn(-e);
if (0 !== this.negative) {
if (1 === this.length && (0 | this.words[0]) < e) {
this.words[0] = e - (0 | this.words[0]);
this.negative = 0;
return this;
}
this.negative = 0;
this.isubn(e);
this.negative = 1;
return this;
}
return this._iaddn(e);
};
o.prototype._iaddn = function(e) {
this.words[0] += e;
for (var t = 0; t < this.length && this.words[t] >= 67108864; t++) {
this.words[t] -= 67108864;
t === this.length - 1 ? this.words[t + 1] = 1 : this.words[t + 1]++;
}
this.length = Math.max(this.length, t + 1);
return this;
};
o.prototype.isubn = function(e) {
n("number" == typeof e);
n(e < 67108864);
if (e < 0) return this.iaddn(-e);
if (0 !== this.negative) {
this.negative = 0;
this.iaddn(e);
this.negative = 1;
return this;
}
this.words[0] -= e;
if (1 === this.length && this.words[0] < 0) {
this.words[0] = -this.words[0];
this.negative = 1;
} else for (var t = 0; t < this.length && this.words[t] < 0; t++) {
this.words[t] += 67108864;
this.words[t + 1] -= 1;
}
return this.strip();
};
o.prototype.addn = function(e) {
return this.clone().iaddn(e);
};
o.prototype.subn = function(e) {
return this.clone().isubn(e);
};
o.prototype.iabs = function() {
this.negative = 0;
return this;
};
o.prototype.abs = function() {
return this.clone().iabs();
};
o.prototype._ishlnsubmul = function(e, t, i) {
var r, o, a = e.length + i;
this._expand(a);
var s = 0;
for (r = 0; r < e.length; r++) {
o = (0 | this.words[r + i]) + s;
var c = (0 | e.words[r]) * t;
s = ((o -= 67108863 & c) >> 26) - (c / 67108864 | 0);
this.words[r + i] = 67108863 & o;
}
for (;r < this.length - i; r++) {
s = (o = (0 | this.words[r + i]) + s) >> 26;
this.words[r + i] = 67108863 & o;
}
if (0 === s) return this.strip();
n(-1 === s);
s = 0;
for (r = 0; r < this.length; r++) {
s = (o = -(0 | this.words[r]) + s) >> 26;
this.words[r] = 67108863 & o;
}
this.negative = 1;
return this.strip();
};
o.prototype._wordDiv = function(e, t) {
var i = (this.length, e.length), n = this.clone(), r = e, a = 0 | r.words[r.length - 1];
if (0 != (i = 26 - this._countBits(a))) {
r = r.ushln(i);
n.iushln(i);
a = 0 | r.words[r.length - 1];
}
var s, c = n.length - r.length;
if ("mod" !== t) {
(s = new o(null)).length = c + 1;
s.words = new Array(s.length);
for (var l = 0; l < s.length; l++) s.words[l] = 0;
}
var h = n.clone()._ishlnsubmul(r, 1, c);
if (0 === h.negative) {
n = h;
s && (s.words[c] = 1);
}
for (var u = c - 1; u >= 0; u--) {
var f = 67108864 * (0 | n.words[r.length + u]) + (0 | n.words[r.length + u - 1]);
f = Math.min(f / a | 0, 67108863);
n._ishlnsubmul(r, f, u);
for (;0 !== n.negative; ) {
f--;
n.negative = 0;
n._ishlnsubmul(r, 1, u);
n.isZero() || (n.negative ^= 1);
}
s && (s.words[u] = f);
}
s && s.strip();
n.strip();
"div" !== t && 0 !== i && n.iushrn(i);
return {
div: s || null,
mod: n
};
};
o.prototype.divmod = function(e, t, i) {
n(!e.isZero());
if (this.isZero()) return {
div: new o(0),
mod: new o(0)
};
var r, a, s;
if (0 !== this.negative && 0 === e.negative) {
s = this.neg().divmod(e, t);
"mod" !== t && (r = s.div.neg());
if ("div" !== t) {
a = s.mod.neg();
i && 0 !== a.negative && a.iadd(e);
}
return {
div: r,
mod: a
};
}
if (0 === this.negative && 0 !== e.negative) {
s = this.divmod(e.neg(), t);
"mod" !== t && (r = s.div.neg());
return {
div: r,
mod: s.mod
};
}
if (0 != (this.negative & e.negative)) {
s = this.neg().divmod(e.neg(), t);
if ("div" !== t) {
a = s.mod.neg();
i && 0 !== a.negative && a.isub(e);
}
return {
div: s.div,
mod: a
};
}
return e.length > this.length || this.cmp(e) < 0 ? {
div: new o(0),
mod: this
} : 1 === e.length ? "div" === t ? {
div: this.divn(e.words[0]),
mod: null
} : "mod" === t ? {
div: null,
mod: new o(this.modn(e.words[0]))
} : {
div: this.divn(e.words[0]),
mod: new o(this.modn(e.words[0]))
} : this._wordDiv(e, t);
};
o.prototype.div = function(e) {
return this.divmod(e, "div", !1).div;
};
o.prototype.mod = function(e) {
return this.divmod(e, "mod", !1).mod;
};
o.prototype.umod = function(e) {
return this.divmod(e, "mod", !0).mod;
};
o.prototype.divRound = function(e) {
var t = this.divmod(e);
if (t.mod.isZero()) return t.div;
var i = 0 !== t.div.negative ? t.mod.isub(e) : t.mod, n = e.ushrn(1), r = e.andln(1), o = i.cmp(n);
return o < 0 || 1 === r && 0 === o ? t.div : 0 !== t.div.negative ? t.div.isubn(1) : t.div.iaddn(1);
};
o.prototype.modn = function(e) {
n(e <= 67108863);
for (var t = (1 << 26) % e, i = 0, r = this.length - 1; r >= 0; r--) i = (t * i + (0 | this.words[r])) % e;
return i;
};
o.prototype.idivn = function(e) {
n(e <= 67108863);
for (var t = 0, i = this.length - 1; i >= 0; i--) {
var r = (0 | this.words[i]) + 67108864 * t;
this.words[i] = r / e | 0;
t = r % e;
}
return this.strip();
};
o.prototype.divn = function(e) {
return this.clone().idivn(e);
};
o.prototype.egcd = function(e) {
n(0 === e.negative);
n(!e.isZero());
var t = this, i = e.clone();
t = 0 !== t.negative ? t.umod(e) : t.clone();
for (var r = new o(1), a = new o(0), s = new o(0), c = new o(1), l = 0; t.isEven() && i.isEven(); ) {
t.iushrn(1);
i.iushrn(1);
++l;
}
for (var h = i.clone(), u = t.clone(); !t.isZero(); ) {
for (var f = 0, d = 1; 0 == (t.words[0] & d) && f < 26; ++f, d <<= 1) ;
if (f > 0) {
t.iushrn(f);
for (;f-- > 0; ) {
if (r.isOdd() || a.isOdd()) {
r.iadd(h);
a.isub(u);
}
r.iushrn(1);
a.iushrn(1);
}
}
for (var p = 0, m = 1; 0 == (i.words[0] & m) && p < 26; ++p, m <<= 1) ;
if (p > 0) {
i.iushrn(p);
for (;p-- > 0; ) {
if (s.isOdd() || c.isOdd()) {
s.iadd(h);
c.isub(u);
}
s.iushrn(1);
c.iushrn(1);
}
}
if (t.cmp(i) >= 0) {
t.isub(i);
r.isub(s);
a.isub(c);
} else {
i.isub(t);
s.isub(r);
c.isub(a);
}
}
return {
a: s,
b: c,
gcd: i.iushln(l)
};
};
o.prototype._invmp = function(e) {
n(0 === e.negative);
n(!e.isZero());
var t = this, i = e.clone();
t = 0 !== t.negative ? t.umod(e) : t.clone();
for (var r, a = new o(1), s = new o(0), c = i.clone(); t.cmpn(1) > 0 && i.cmpn(1) > 0; ) {
for (var l = 0, h = 1; 0 == (t.words[0] & h) && l < 26; ++l, h <<= 1) ;
if (l > 0) {
t.iushrn(l);
for (;l-- > 0; ) {
a.isOdd() && a.iadd(c);
a.iushrn(1);
}
}
for (var u = 0, f = 1; 0 == (i.words[0] & f) && u < 26; ++u, f <<= 1) ;
if (u > 0) {
i.iushrn(u);
for (;u-- > 0; ) {
s.isOdd() && s.iadd(c);
s.iushrn(1);
}
}
if (t.cmp(i) >= 0) {
t.isub(i);
a.isub(s);
} else {
i.isub(t);
s.isub(a);
}
}
(r = 0 === t.cmpn(1) ? a : s).cmpn(0) < 0 && r.iadd(e);
return r;
};
o.prototype.gcd = function(e) {
if (this.isZero()) return e.abs();
if (e.isZero()) return this.abs();
var t = this.clone(), i = e.clone();
t.negative = 0;
i.negative = 0;
for (var n = 0; t.isEven() && i.isEven(); n++) {
t.iushrn(1);
i.iushrn(1);
}
for (;;) {
for (;t.isEven(); ) t.iushrn(1);
for (;i.isEven(); ) i.iushrn(1);
var r = t.cmp(i);
if (r < 0) {
var o = t;
t = i;
i = o;
} else if (0 === r || 0 === i.cmpn(1)) break;
t.isub(i);
}
return i.iushln(n);
};
o.prototype.invm = function(e) {
return this.egcd(e).a.umod(e);
};
o.prototype.isEven = function() {
return 0 == (1 & this.words[0]);
};
o.prototype.isOdd = function() {
return 1 == (1 & this.words[0]);
};
o.prototype.andln = function(e) {
return this.words[0] & e;
};
o.prototype.bincn = function(e) {
n("number" == typeof e);
var t = e % 26, i = (e - t) / 26, r = 1 << t;
if (this.length <= i) {
this._expand(i + 1);
this.words[i] |= r;
return this;
}
for (var o = r, a = i; 0 !== o && a < this.length; a++) {
var s = 0 | this.words[a];
o = (s += o) >>> 26;
s &= 67108863;
this.words[a] = s;
}
if (0 !== o) {
this.words[a] = o;
this.length++;
}
return this;
};
o.prototype.isZero = function() {
return 1 === this.length && 0 === this.words[0];
};
o.prototype.cmpn = function(e) {
var t, i = e < 0;
if (0 !== this.negative && !i) return -1;
if (0 === this.negative && i) return 1;
this.strip();
if (this.length > 1) t = 1; else {
i && (e = -e);
n(e <= 67108863, "Number is too big");
var r = 0 | this.words[0];
t = r === e ? 0 : r < e ? -1 : 1;
}
return 0 !== this.negative ? 0 | -t : t;
};
o.prototype.cmp = function(e) {
if (0 !== this.negative && 0 === e.negative) return -1;
if (0 === this.negative && 0 !== e.negative) return 1;
var t = this.ucmp(e);
return 0 !== this.negative ? 0 | -t : t;
};
o.prototype.ucmp = function(e) {
if (this.length > e.length) return 1;
if (this.length < e.length) return -1;
for (var t = 0, i = this.length - 1; i >= 0; i--) {
var n = 0 | this.words[i], r = 0 | e.words[i];
if (n !== r) {
n < r ? t = -1 : n > r && (t = 1);
break;
}
}
return t;
};
o.prototype.gtn = function(e) {
return 1 === this.cmpn(e);
};
o.prototype.gt = function(e) {
return 1 === this.cmp(e);
};
o.prototype.gten = function(e) {
return this.cmpn(e) >= 0;
};
o.prototype.gte = function(e) {
return this.cmp(e) >= 0;
};
o.prototype.ltn = function(e) {
return -1 === this.cmpn(e);
};
o.prototype.lt = function(e) {
return -1 === this.cmp(e);
};
o.prototype.lten = function(e) {
return this.cmpn(e) <= 0;
};
o.prototype.lte = function(e) {
return this.cmp(e) <= 0;
};
o.prototype.eqn = function(e) {
return 0 === this.cmpn(e);
};
o.prototype.eq = function(e) {
return 0 === this.cmp(e);
};
o.red = function(e) {
return new M(e);
};
o.prototype.toRed = function(e) {
n(!this.red, "Already a number in reduction context");
n(0 === this.negative, "red works only with positives");
return e.convertTo(this)._forceRed(e);
};
o.prototype.fromRed = function() {
n(this.red, "fromRed works only with numbers in reduction context");
return this.red.convertFrom(this);
};
o.prototype._forceRed = function(e) {
this.red = e;
return this;
};
o.prototype.forceRed = function(e) {
n(!this.red, "Already a number in reduction context");
return this._forceRed(e);
};
o.prototype.redAdd = function(e) {
n(this.red, "redAdd works only with red numbers");
return this.red.add(this, e);
};
o.prototype.redIAdd = function(e) {
n(this.red, "redIAdd works only with red numbers");
return this.red.iadd(this, e);
};
o.prototype.redSub = function(e) {
n(this.red, "redSub works only with red numbers");
return this.red.sub(this, e);
};
o.prototype.redISub = function(e) {
n(this.red, "redISub works only with red numbers");
return this.red.isub(this, e);
};
o.prototype.redShl = function(e) {
n(this.red, "redShl works only with red numbers");
return this.red.shl(this, e);
};
o.prototype.redMul = function(e) {
n(this.red, "redMul works only with red numbers");
this.red._verify2(this, e);
return this.red.mul(this, e);
};
o.prototype.redIMul = function(e) {
n(this.red, "redMul works only with red numbers");
this.red._verify2(this, e);
return this.red.imul(this, e);
};
o.prototype.redSqr = function() {
n(this.red, "redSqr works only with red numbers");
this.red._verify1(this);
return this.red.sqr(this);
};
o.prototype.redISqr = function() {
n(this.red, "redISqr works only with red numbers");
this.red._verify1(this);
return this.red.isqr(this);
};
o.prototype.redSqrt = function() {
n(this.red, "redSqrt works only with red numbers");
this.red._verify1(this);
return this.red.sqrt(this);
};
o.prototype.redInvm = function() {
n(this.red, "redInvm works only with red numbers");
this.red._verify1(this);
return this.red.invm(this);
};
o.prototype.redNeg = function() {
n(this.red, "redNeg works only with red numbers");
this.red._verify1(this);
return this.red.neg(this);
};
o.prototype.redPow = function(e) {
n(this.red && !e.red, "redPow(normalNum)");
this.red._verify1(this);
return this.red.pow(this, e);
};
var v = {
k256: null,
p224: null,
p192: null,
p25519: null
};
function _(e, t) {
this.name = e;
this.p = new o(t, 16);
this.n = this.p.bitLength();
this.k = new o(1).iushln(this.n).isub(this.p);
this.tmp = this._tmp();
}
_.prototype._tmp = function() {
var e = new o(null);
e.words = new Array(Math.ceil(this.n / 13));
return e;
};
_.prototype.ireduce = function(e) {
var t, i = e;
do {
this.split(i, this.tmp);
t = (i = (i = this.imulK(i)).iadd(this.tmp)).bitLength();
} while (t > this.n);
var n = t < this.n ? -1 : i.ucmp(this.p);
if (0 === n) {
i.words[0] = 0;
i.length = 1;
} else n > 0 ? i.isub(this.p) : void 0 !== i.strip ? i.strip() : i._strip();
return i;
};
_.prototype.split = function(e, t) {
e.iushrn(this.n, 0, t);
};
_.prototype.imulK = function(e) {
return e.imul(this.k);
};
function w() {
_.call(this, "k256", "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe fffffc2f");
}
r(w, _);
w.prototype.split = function(e, t) {
for (var i = Math.min(e.length, 9), n = 0; n < i; n++) t.words[n] = e.words[n];
t.length = i;
if (e.length <= 9) {
e.words[0] = 0;
e.length = 1;
} else {
var r = e.words[9];
t.words[t.length++] = 4194303 & r;
for (n = 10; n < e.length; n++) {
var o = 0 | e.words[n];
e.words[n - 10] = (4194303 & o) << 4 | r >>> 22;
r = o;
}
r >>>= 22;
e.words[n - 10] = r;
0 === r && e.length > 10 ? e.length -= 10 : e.length -= 9;
}
};
w.prototype.imulK = function(e) {
e.words[e.length] = 0;
e.words[e.length + 1] = 0;
e.length += 2;
for (var t = 0, i = 0; i < e.length; i++) {
var n = 0 | e.words[i];
t += 977 * n;
e.words[i] = 67108863 & t;
t = 64 * n + (t / 67108864 | 0);
}
if (0 === e.words[e.length - 1]) {
e.length--;
0 === e.words[e.length - 1] && e.length--;
}
return e;
};
function C() {
_.call(this, "p224", "ffffffff ffffffff ffffffff ffffffff 00000000 00000000 00000001");
}
r(C, _);
function S() {
_.call(this, "p192", "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff");
}
r(S, _);
function A() {
_.call(this, "25519", "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed");
}
r(A, _);
A.prototype.imulK = function(e) {
for (var t = 0, i = 0; i < e.length; i++) {
var n = 19 * (0 | e.words[i]) + t, r = 67108863 & n;
n >>>= 26;
e.words[i] = r;
t = n;
}
0 !== t && (e.words[e.length++] = t);
return e;
};
o._prime = function(e) {
if (v[e]) return v[e];
var t;
if ("k256" === e) t = new w(); else if ("p224" === e) t = new C(); else if ("p192" === e) t = new S(); else {
if ("p25519" !== e) throw new Error("Unknown prime " + e);
t = new A();
}
v[e] = t;
return t;
};
function M(e) {
if ("string" == typeof e) {
var t = o._prime(e);
this.m = t.p;
this.prime = t;
} else {
n(e.gtn(1), "modulus must be greater than 1");
this.m = e;
this.prime = null;
}
}
M.prototype._verify1 = function(e) {
n(0 === e.negative, "red works only with positives");
n(e.red, "red works only with red numbers");
};
M.prototype._verify2 = function(e, t) {
n(0 == (e.negative | t.negative), "red works only with positives");
n(e.red && e.red === t.red, "red works only with red numbers");
};
M.prototype.imod = function(e) {
return this.prime ? this.prime.ireduce(e)._forceRed(this) : e.umod(this.m)._forceRed(this);
};
M.prototype.neg = function(e) {
return e.isZero() ? e.clone() : this.m.sub(e)._forceRed(this);
};
M.prototype.add = function(e, t) {
this._verify2(e, t);
var i = e.add(t);
i.cmp(this.m) >= 0 && i.isub(this.m);
return i._forceRed(this);
};
M.prototype.iadd = function(e, t) {
this._verify2(e, t);
var i = e.iadd(t);
i.cmp(this.m) >= 0 && i.isub(this.m);
return i;
};
M.prototype.sub = function(e, t) {
this._verify2(e, t);
var i = e.sub(t);
i.cmpn(0) < 0 && i.iadd(this.m);
return i._forceRed(this);
};
M.prototype.isub = function(e, t) {
this._verify2(e, t);
var i = e.isub(t);
i.cmpn(0) < 0 && i.iadd(this.m);
return i;
};
M.prototype.shl = function(e, t) {
this._verify1(e);
return this.imod(e.ushln(t));
};
M.prototype.imul = function(e, t) {
this._verify2(e, t);
return this.imod(e.imul(t));
};
M.prototype.mul = function(e, t) {
this._verify2(e, t);
return this.imod(e.mul(t));
};
M.prototype.isqr = function(e) {
return this.imul(e, e.clone());
};
M.prototype.sqr = function(e) {
return this.mul(e, e);
};
M.prototype.sqrt = function(e) {
if (e.isZero()) return e.clone();
var t = this.m.andln(3);
n(t % 2 == 1);
if (3 === t) {
var i = this.m.add(new o(1)).iushrn(2);
return this.pow(e, i);
}
for (var r = this.m.subn(1), a = 0; !r.isZero() && 0 === r.andln(1); ) {
a++;
r.iushrn(1);
}
n(!r.isZero());
var s = new o(1).toRed(this), c = s.redNeg(), l = this.m.subn(1).iushrn(1), h = this.m.bitLength();
h = new o(2 * h * h).toRed(this);
for (;0 !== this.pow(h, l).cmp(c); ) h.redIAdd(c);
for (var u = this.pow(h, r), f = this.pow(e, r.addn(1).iushrn(1)), d = this.pow(e, r), p = a; 0 !== d.cmp(s); ) {
for (var m = d, g = 0; 0 !== m.cmp(s); g++) m = m.redSqr();
n(g < p);
var y = this.pow(u, new o(1).iushln(p - g - 1));
f = f.redMul(y);
u = y.redSqr();
d = d.redMul(u);
p = g;
}
return f;
};
M.prototype.invm = function(e) {
var t = e._invmp(this.m);
if (0 !== t.negative) {
t.negative = 0;
return this.imod(t).redNeg();
}
return this.imod(t);
};
M.prototype.pow = function(e, t) {
if (t.isZero()) return new o(1).toRed(this);
if (0 === t.cmpn(1)) return e.clone();
var i = new Array(16);
i[0] = new o(1).toRed(this);
i[1] = e;
for (var n = 2; n < i.length; n++) i[n] = this.mul(i[n - 1], e);
var r = i[0], a = 0, s = 0, c = t.bitLength() % 26;
0 === c && (c = 26);
for (n = t.length - 1; n >= 0; n--) {
for (var l = t.words[n], h = c - 1; h >= 0; h--) {
var u = l >> h & 1;
r !== i[0] && (r = this.sqr(r));
if (0 !== u || 0 !== a) {
a <<= 1;
a |= u;
if (4 == ++s || 0 === n && 0 === h) {
r = this.mul(r, i[a]);
s = 0;
a = 0;
}
} else s = 0;
}
c = 26;
}
return r;
};
M.prototype.convertTo = function(e) {
var t = e.umod(this.m);
return t === e ? t.clone() : t;
};
M.prototype.convertFrom = function(e) {
var t = e.clone();
t.red = null;
return t;
};
o.mont = function(e) {
return new B(e);
};
function B(e) {
M.call(this, e);
this.shift = this.m.bitLength();
this.shift % 26 != 0 && (this.shift += 26 - this.shift % 26);
this.r = new o(1).iushln(this.shift);
this.r2 = this.imod(this.r.sqr());
this.rinv = this.r._invmp(this.m);
this.minv = this.rinv.mul(this.r).isubn(1).div(this.m);
this.minv = this.minv.umod(this.r);
this.minv = this.r.sub(this.minv);
}
r(B, M);
B.prototype.convertTo = function(e) {
return this.imod(e.ushln(this.shift));
};
B.prototype.convertFrom = function(e) {
var t = this.imod(e.mul(this.rinv));
t.red = null;
return t;
};
B.prototype.imul = function(e, t) {
if (e.isZero() || t.isZero()) {
e.words[0] = 0;
e.length = 1;
return e;
}
var i = e.imul(t), n = i.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m), r = i.isub(n).iushrn(this.shift), o = r;
r.cmp(this.m) >= 0 ? o = r.isub(this.m) : r.cmpn(0) < 0 && (o = r.iadd(this.m));
return o._forceRed(this);
};
B.prototype.mul = function(e, t) {
if (e.isZero() || t.isZero()) return new o(0)._forceRed(this);
var i = e.mul(t), n = i.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m), r = i.isub(n).iushrn(this.shift), a = r;
r.cmp(this.m) >= 0 ? a = r.isub(this.m) : r.cmpn(0) < 0 && (a = r.iadd(this.m));
return a._forceRed(this);
};
B.prototype.invm = function(e) {
return this.imod(e._invmp(this.m).mul(this.r2))._forceRed(this);
};
})("undefined" == typeof t || t, this);
}, {
buffer: 19
} ],
16: [ function(e, t, i) {
"use strict";
i.byteLength = function(e) {
var t = l(e), i = t[0], n = t[1];
return 3 * (i + n) / 4 - n;
};
i.toByteArray = function(e) {
var t, i, n = l(e), a = n[0], s = n[1], c = new o(h(0, a, s)), u = 0, f = s > 0 ? a - 4 : a;
for (i = 0; i < f; i += 4) {
t = r[e.charCodeAt(i)] << 18 | r[e.charCodeAt(i + 1)] << 12 | r[e.charCodeAt(i + 2)] << 6 | r[e.charCodeAt(i + 3)];
c[u++] = t >> 16 & 255;
c[u++] = t >> 8 & 255;
c[u++] = 255 & t;
}
if (2 === s) {
t = r[e.charCodeAt(i)] << 2 | r[e.charCodeAt(i + 1)] >> 4;
c[u++] = 255 & t;
}
if (1 === s) {
t = r[e.charCodeAt(i)] << 10 | r[e.charCodeAt(i + 1)] << 4 | r[e.charCodeAt(i + 2)] >> 2;
c[u++] = t >> 8 & 255;
c[u++] = 255 & t;
}
return c;
};
i.fromByteArray = function(e) {
for (var t, i = e.length, r = i % 3, o = [], a = 0, s = i - r; a < s; a += 16383) o.push(u(e, a, a + 16383 > s ? s : a + 16383));
if (1 === r) {
t = e[i - 1];
o.push(n[t >> 2] + n[t << 4 & 63] + "==");
} else if (2 === r) {
t = (e[i - 2] << 8) + e[i - 1];
o.push(n[t >> 10] + n[t >> 4 & 63] + n[t << 2 & 63] + "=");
}
return o.join("");
};
for (var n = [], r = [], o = "undefined" != typeof Uint8Array ? Uint8Array : Array, a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", s = 0, c = a.length; s < c; ++s) {
n[s] = a[s];
r[a.charCodeAt(s)] = s;
}
r["-".charCodeAt(0)] = 62;
r["_".charCodeAt(0)] = 63;
function l(e) {
var t = e.length;
if (t % 4 > 0) throw new Error("Invalid string. Length must be a multiple of 4");
var i = e.indexOf("=");
-1 === i && (i = t);
return [ i, i === t ? 0 : 4 - i % 4 ];
}
function h(e, t, i) {
return 3 * (t + i) / 4 - i;
}
function u(e, t, i) {
for (var r, o, a = [], s = t; s < i; s += 3) {
r = (e[s] << 16 & 16711680) + (e[s + 1] << 8 & 65280) + (255 & e[s + 2]);
a.push(n[(o = r) >> 18 & 63] + n[o >> 12 & 63] + n[o >> 6 & 63] + n[63 & o]);
}
return a.join("");
}
}, {} ],
17: [ function(e, t) {
(function(t, i) {
"use strict";
function n(e, t) {
if (!e) throw new Error(t || "Assertion failed");
}
function r(e, t) {
e.super_ = t;
var i = function() {};
i.prototype = t.prototype;
e.prototype = new i();
e.prototype.constructor = e;
}
function o(e, t, i) {
if (o.isBN(e)) return e;
this.negative = 0;
this.words = null;
this.length = 0;
this.red = null;
if (null !== e) {
if ("le" === t || "be" === t) {
i = t;
t = 10;
}
this._init(e || 0, t || 10, i || "be");
}
}
"object" == typeof t ? t.exports = o : i.BN = o;
o.BN = o;
o.wordSize = 26;
var a;
try {
a = "undefined" != typeof window && "undefined" != typeof window.Buffer ? window.Buffer : e("buffer").Buffer;
} catch (e) {}
o.isBN = function(e) {
return e instanceof o || null !== e && "object" == typeof e && e.constructor.wordSize === o.wordSize && Array.isArray(e.words);
};
o.max = function(e, t) {
return e.cmp(t) > 0 ? e : t;
};
o.min = function(e, t) {
return e.cmp(t) < 0 ? e : t;
};
o.prototype._init = function(e, t, i) {
if ("number" == typeof e) return this._initNumber(e, t, i);
if ("object" == typeof e) return this._initArray(e, t, i);
"hex" === t && (t = 16);
n(t === (0 | t) && t >= 2 && t <= 36);
var r = 0;
if ("-" === (e = e.toString().replace(/\s+/g, ""))[0]) {
r++;
this.negative = 1;
}
if (r < e.length) if (16 === t) this._parseHex(e, r, i); else {
this._parseBase(e, t, r);
"le" === i && this._initArray(this.toArray(), t, i);
}
};
o.prototype._initNumber = function(e, t, i) {
if (e < 0) {
this.negative = 1;
e = -e;
}
if (e < 67108864) {
this.words = [ 67108863 & e ];
this.length = 1;
} else if (e < 4503599627370496) {
this.words = [ 67108863 & e, e / 67108864 & 67108863 ];
this.length = 2;
} else {
n(e < 9007199254740992);
this.words = [ 67108863 & e, e / 67108864 & 67108863, 1 ];
this.length = 3;
}
"le" === i && this._initArray(this.toArray(), t, i);
};
o.prototype._initArray = function(e, t, i) {
n("number" == typeof e.length);
if (e.length <= 0) {
this.words = [ 0 ];
this.length = 1;
return this;
}
this.length = Math.ceil(e.length / 3);
this.words = new Array(this.length);
for (var r = 0; r < this.length; r++) this.words[r] = 0;
var o, a, s = 0;
if ("be" === i) for (r = e.length - 1, o = 0; r >= 0; r -= 3) {
a = e[r] | e[r - 1] << 8 | e[r - 2] << 16;
this.words[o] |= a << s & 67108863;
this.words[o + 1] = a >>> 26 - s & 67108863;
if ((s += 24) >= 26) {
s -= 26;
o++;
}
} else if ("le" === i) for (r = 0, o = 0; r < e.length; r += 3) {
a = e[r] | e[r + 1] << 8 | e[r + 2] << 16;
this.words[o] |= a << s & 67108863;
this.words[o + 1] = a >>> 26 - s & 67108863;
if ((s += 24) >= 26) {
s -= 26;
o++;
}
}
return this._strip();
};
function s(e, t) {
var i = e.charCodeAt(t);
if (i >= 48 && i <= 57) return i - 48;
if (i >= 65 && i <= 70) return i - 55;
if (i >= 97 && i <= 102) return i - 87;
n(!1, "Invalid character in " + e);
}
function c(e, t, i) {
var n = s(e, i);
i - 1 >= t && (n |= s(e, i - 1) << 4);
return n;
}
o.prototype._parseHex = function(e, t, i) {
this.length = Math.ceil((e.length - t) / 6);
this.words = new Array(this.length);
for (var n = 0; n < this.length; n++) this.words[n] = 0;
var r, o = 0, a = 0;
if ("be" === i) for (n = e.length - 1; n >= t; n -= 2) {
r = c(e, t, n) << o;
this.words[a] |= 67108863 & r;
if (o >= 18) {
o -= 18;
a += 1;
this.words[a] |= r >>> 26;
} else o += 8;
} else for (n = (e.length - t) % 2 == 0 ? t + 1 : t; n < e.length; n += 2) {
r = c(e, t, n) << o;
this.words[a] |= 67108863 & r;
if (o >= 18) {
o -= 18;
a += 1;
this.words[a] |= r >>> 26;
} else o += 8;
}
this._strip();
};
function l(e, t, i, r) {
for (var o = 0, a = 0, s = Math.min(e.length, i), c = t; c < s; c++) {
var l = e.charCodeAt(c) - 48;
o *= r;
a = l >= 49 ? l - 49 + 10 : l >= 17 ? l - 17 + 10 : l;
n(l >= 0 && a < r, "Invalid character");
o += a;
}
return o;
}
o.prototype._parseBase = function(e, t, i) {
this.words = [ 0 ];
this.length = 1;
for (var n = 0, r = 1; r <= 67108863; r *= t) n++;
n--;
r = r / t | 0;
for (var o = e.length - i, a = o % n, s = Math.min(o, o - a) + i, c = 0, h = i; h < s; h += n) {
c = l(e, h, h + n, t);
this.imuln(r);
this.words[0] + c < 67108864 ? this.words[0] += c : this._iaddn(c);
}
if (0 !== a) {
var u = 1;
c = l(e, h, e.length, t);
for (h = 0; h < a; h++) u *= t;
this.imuln(u);
this.words[0] + c < 67108864 ? this.words[0] += c : this._iaddn(c);
}
this._strip();
};
o.prototype.copy = function(e) {
e.words = new Array(this.length);
for (var t = 0; t < this.length; t++) e.words[t] = this.words[t];
e.length = this.length;
e.negative = this.negative;
e.red = this.red;
};
function h(e, t) {
e.words = t.words;
e.length = t.length;
e.negative = t.negative;
e.red = t.red;
}
o.prototype._move = function(e) {
h(e, this);
};
o.prototype.clone = function() {
var e = new o(null);
this.copy(e);
return e;
};
o.prototype._expand = function(e) {
for (;this.length < e; ) this.words[this.length++] = 0;
return this;
};
o.prototype._strip = function() {
for (;this.length > 1 && 0 === this.words[this.length - 1]; ) this.length--;
return this._normSign();
};
o.prototype._normSign = function() {
1 === this.length && 0 === this.words[0] && (this.negative = 0);
return this;
};
if ("undefined" != typeof Symbol && "function" == typeof Symbol.for) try {
o.prototype[Symbol.for("nodejs.util.inspect.custom")] = u;
} catch (e) {
o.prototype.inspect = u;
} else o.prototype.inspect = u;
function u() {
return (this.red ? "<BN-R: " : "<BN: ") + this.toString(16) + ">";
}
var f = [ "", "0", "00", "000", "0000", "00000", "000000", "0000000", "00000000", "000000000", "0000000000", "00000000000", "000000000000", "0000000000000", "00000000000000", "000000000000000", "0000000000000000", "00000000000000000", "000000000000000000", "0000000000000000000", "00000000000000000000", "000000000000000000000", "0000000000000000000000", "00000000000000000000000", "000000000000000000000000", "0000000000000000000000000" ], d = [ 0, 0, 25, 16, 12, 11, 10, 9, 8, 8, 7, 7, 7, 7, 6, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ], p = [ 0, 0, 33554432, 43046721, 16777216, 48828125, 60466176, 40353607, 16777216, 43046721, 1e7, 19487171, 35831808, 62748517, 7529536, 11390625, 16777216, 24137569, 34012224, 47045881, 64e6, 4084101, 5153632, 6436343, 7962624, 9765625, 11881376, 14348907, 17210368, 20511149, 243e5, 28629151, 33554432, 39135393, 45435424, 52521875, 60466176 ];
o.prototype.toString = function(e, t) {
t = 0 | t || 1;
var i;
if (16 === (e = e || 10) || "hex" === e) {
i = "";
for (var r = 0, o = 0, a = 0; a < this.length; a++) {
var s = this.words[a], c = (16777215 & (s << r | o)).toString(16);
o = s >>> 24 - r & 16777215;
if ((r += 2) >= 26) {
r -= 26;
a--;
}
i = 0 !== o || a !== this.length - 1 ? f[6 - c.length] + c + i : c + i;
}
0 !== o && (i = o.toString(16) + i);
for (;i.length % t != 0; ) i = "0" + i;
0 !== this.negative && (i = "-" + i);
return i;
}
if (e === (0 | e) && e >= 2 && e <= 36) {
var l = d[e], h = p[e];
i = "";
var u = this.clone();
u.negative = 0;
for (;!u.isZero(); ) {
var m = u.modrn(h).toString(e);
i = (u = u.idivn(h)).isZero() ? m + i : f[l - m.length] + m + i;
}
this.isZero() && (i = "0" + i);
for (;i.length % t != 0; ) i = "0" + i;
0 !== this.negative && (i = "-" + i);
return i;
}
n(!1, "Base should be between 2 and 36");
};
o.prototype.toNumber = function() {
var e = this.words[0];
2 === this.length ? e += 67108864 * this.words[1] : 3 === this.length && 1 === this.words[2] ? e += 4503599627370496 + 67108864 * this.words[1] : this.length > 2 && n(!1, "Number can only safely store up to 53 bits");
return 0 !== this.negative ? -e : e;
};
o.prototype.toJSON = function() {
return this.toString(16, 2);
};
a && (o.prototype.toBuffer = function(e, t) {
return this.toArrayLike(a, e, t);
});
o.prototype.toArray = function(e, t) {
return this.toArrayLike(Array, e, t);
};
var m = function(e, t) {
return e.allocUnsafe ? e.allocUnsafe(t) : new e(t);
};
o.prototype.toArrayLike = function(e, t, i) {
this._strip();
var r = this.byteLength(), o = i || Math.max(1, r);
n(r <= o, "byte array longer than desired length");
n(o > 0, "Requested array length <= 0");
var a = m(e, o);
this["_toArrayLike" + ("le" === t ? "LE" : "BE")](a, r);
return a;
};
o.prototype._toArrayLikeLE = function(e) {
for (var t = 0, i = 0, n = 0, r = 0; n < this.length; n++) {
var o = this.words[n] << r | i;
e[t++] = 255 & o;
t < e.length && (e[t++] = o >> 8 & 255);
t < e.length && (e[t++] = o >> 16 & 255);
if (6 === r) {
t < e.length && (e[t++] = o >> 24 & 255);
i = 0;
r = 0;
} else {
i = o >>> 24;
r += 2;
}
}
if (t < e.length) {
e[t++] = i;
for (;t < e.length; ) e[t++] = 0;
}
};
o.prototype._toArrayLikeBE = function(e) {
for (var t = e.length - 1, i = 0, n = 0, r = 0; n < this.length; n++) {
var o = this.words[n] << r | i;
e[t--] = 255 & o;
t >= 0 && (e[t--] = o >> 8 & 255);
t >= 0 && (e[t--] = o >> 16 & 255);
if (6 === r) {
t >= 0 && (e[t--] = o >> 24 & 255);
i = 0;
r = 0;
} else {
i = o >>> 24;
r += 2;
}
}
if (t >= 0) {
e[t--] = i;
for (;t >= 0; ) e[t--] = 0;
}
};
Math.clz32 ? o.prototype._countBits = function(e) {
return 32 - Math.clz32(e);
} : o.prototype._countBits = function(e) {
var t = e, i = 0;
if (t >= 4096) {
i += 13;
t >>>= 13;
}
if (t >= 64) {
i += 7;
t >>>= 7;
}
if (t >= 8) {
i += 4;
t >>>= 4;
}
if (t >= 2) {
i += 2;
t >>>= 2;
}
return i + t;
};
o.prototype._zeroBits = function(e) {
if (0 === e) return 26;
var t = e, i = 0;
if (0 == (8191 & t)) {
i += 13;
t >>>= 13;
}
if (0 == (127 & t)) {
i += 7;
t >>>= 7;
}
if (0 == (15 & t)) {
i += 4;
t >>>= 4;
}
if (0 == (3 & t)) {
i += 2;
t >>>= 2;
}
0 == (1 & t) && i++;
return i;
};
o.prototype.bitLength = function() {
var e = this.words[this.length - 1], t = this._countBits(e);
return 26 * (this.length - 1) + t;
};
function g(e) {
for (var t = new Array(e.bitLength()), i = 0; i < t.length; i++) {
var n = i / 26 | 0, r = i % 26;
t[i] = e.words[n] >>> r & 1;
}
return t;
}
o.prototype.zeroBits = function() {
if (this.isZero()) return 0;
for (var e = 0, t = 0; t < this.length; t++) {
var i = this._zeroBits(this.words[t]);
e += i;
if (26 !== i) break;
}
return e;
};
o.prototype.byteLength = function() {
return Math.ceil(this.bitLength() / 8);
};
o.prototype.toTwos = function(e) {
return 0 !== this.negative ? this.abs().inotn(e).iaddn(1) : this.clone();
};
o.prototype.fromTwos = function(e) {
return this.testn(e - 1) ? this.notn(e).iaddn(1).ineg() : this.clone();
};
o.prototype.isNeg = function() {
return 0 !== this.negative;
};
o.prototype.neg = function() {
return this.clone().ineg();
};
o.prototype.ineg = function() {
this.isZero() || (this.negative ^= 1);
return this;
};
o.prototype.iuor = function(e) {
for (;this.length < e.length; ) this.words[this.length++] = 0;
for (var t = 0; t < e.length; t++) this.words[t] = this.words[t] | e.words[t];
return this._strip();
};
o.prototype.ior = function(e) {
n(0 == (this.negative | e.negative));
return this.iuor(e);
};
o.prototype.or = function(e) {
return this.length > e.length ? this.clone().ior(e) : e.clone().ior(this);
};
o.prototype.uor = function(e) {
return this.length > e.length ? this.clone().iuor(e) : e.clone().iuor(this);
};
o.prototype.iuand = function(e) {
var t;
t = this.length > e.length ? e : this;
for (var i = 0; i < t.length; i++) this.words[i] = this.words[i] & e.words[i];
this.length = t.length;
return this._strip();
};
o.prototype.iand = function(e) {
n(0 == (this.negative | e.negative));
return this.iuand(e);
};
o.prototype.and = function(e) {
return this.length > e.length ? this.clone().iand(e) : e.clone().iand(this);
};
o.prototype.uand = function(e) {
return this.length > e.length ? this.clone().iuand(e) : e.clone().iuand(this);
};
o.prototype.iuxor = function(e) {
var t, i;
if (this.length > e.length) {
t = this;
i = e;
} else {
t = e;
i = this;
}
for (var n = 0; n < i.length; n++) this.words[n] = t.words[n] ^ i.words[n];
if (this !== t) for (;n < t.length; n++) this.words[n] = t.words[n];
this.length = t.length;
return this._strip();
};
o.prototype.ixor = function(e) {
n(0 == (this.negative | e.negative));
return this.iuxor(e);
};
o.prototype.xor = function(e) {
return this.length > e.length ? this.clone().ixor(e) : e.clone().ixor(this);
};
o.prototype.uxor = function(e) {
return this.length > e.length ? this.clone().iuxor(e) : e.clone().iuxor(this);
};
o.prototype.inotn = function(e) {
n("number" == typeof e && e >= 0);
var t = 0 | Math.ceil(e / 26), i = e % 26;
this._expand(t);
i > 0 && t--;
for (var r = 0; r < t; r++) this.words[r] = 67108863 & ~this.words[r];
i > 0 && (this.words[r] = ~this.words[r] & 67108863 >> 26 - i);
return this._strip();
};
o.prototype.notn = function(e) {
return this.clone().inotn(e);
};
o.prototype.setn = function(e, t) {
n("number" == typeof e && e >= 0);
var i = e / 26 | 0, r = e % 26;
this._expand(i + 1);
this.words[i] = t ? this.words[i] | 1 << r : this.words[i] & ~(1 << r);
return this._strip();
};
o.prototype.iadd = function(e) {
var t, i, n;
if (0 !== this.negative && 0 === e.negative) {
this.negative = 0;
t = this.isub(e);
this.negative ^= 1;
return this._normSign();
}
if (0 === this.negative && 0 !== e.negative) {
e.negative = 0;
t = this.isub(e);
e.negative = 1;
return t._normSign();
}
if (this.length > e.length) {
i = this;
n = e;
} else {
i = e;
n = this;
}
for (var r = 0, o = 0; o < n.length; o++) {
t = (0 | i.words[o]) + (0 | n.words[o]) + r;
this.words[o] = 67108863 & t;
r = t >>> 26;
}
for (;0 !== r && o < i.length; o++) {
t = (0 | i.words[o]) + r;
this.words[o] = 67108863 & t;
r = t >>> 26;
}
this.length = i.length;
if (0 !== r) {
this.words[this.length] = r;
this.length++;
} else if (i !== this) for (;o < i.length; o++) this.words[o] = i.words[o];
return this;
};
o.prototype.add = function(e) {
var t;
if (0 !== e.negative && 0 === this.negative) {
e.negative = 0;
t = this.sub(e);
e.negative ^= 1;
return t;
}
if (0 === e.negative && 0 !== this.negative) {
this.negative = 0;
t = e.sub(this);
this.negative = 1;
return t;
}
return this.length > e.length ? this.clone().iadd(e) : e.clone().iadd(this);
};
o.prototype.isub = function(e) {
if (0 !== e.negative) {
e.negative = 0;
var t = this.iadd(e);
e.negative = 1;
return t._normSign();
}
if (0 !== this.negative) {
this.negative = 0;
this.iadd(e);
this.negative = 1;
return this._normSign();
}
var i, n, r = this.cmp(e);
if (0 === r) {
this.negative = 0;
this.length = 1;
this.words[0] = 0;
return this;
}
if (r > 0) {
i = this;
n = e;
} else {
i = e;
n = this;
}
for (var o = 0, a = 0; a < n.length; a++) {
o = (t = (0 | i.words[a]) - (0 | n.words[a]) + o) >> 26;
this.words[a] = 67108863 & t;
}
for (;0 !== o && a < i.length; a++) {
o = (t = (0 | i.words[a]) + o) >> 26;
this.words[a] = 67108863 & t;
}
if (0 === o && a < i.length && i !== this) for (;a < i.length; a++) this.words[a] = i.words[a];
this.length = Math.max(this.length, a);
i !== this && (this.negative = 1);
return this._strip();
};
o.prototype.sub = function(e) {
return this.clone().isub(e);
};
function y(e, t, i) {
i.negative = t.negative ^ e.negative;
var n = e.length + t.length | 0;
i.length = n;
n = n - 1 | 0;
var r = 0 | e.words[0], o = 0 | t.words[0], a = r * o, s = 67108863 & a, c = a / 67108864 | 0;
i.words[0] = s;
for (var l = 1; l < n; l++) {
for (var h = c >>> 26, u = 67108863 & c, f = Math.min(l, t.length - 1), d = Math.max(0, l - e.length + 1); d <= f; d++) {
var p = l - d | 0;
h += (a = (r = 0 | e.words[p]) * (o = 0 | t.words[d]) + u) / 67108864 | 0;
u = 67108863 & a;
}
i.words[l] = 0 | u;
c = 0 | h;
}
0 !== c ? i.words[l] = 0 | c : i.length--;
return i._strip();
}
var b = function(e, t, i) {
var n, r, o, a = e.words, s = t.words, c = i.words, l = 0, h = 0 | a[0], u = 8191 & h, f = h >>> 13, d = 0 | a[1], p = 8191 & d, m = d >>> 13, g = 0 | a[2], y = 8191 & g, b = g >>> 13, v = 0 | a[3], _ = 8191 & v, w = v >>> 13, C = 0 | a[4], S = 8191 & C, A = C >>> 13, M = 0 | a[5], B = 8191 & M, x = M >>> 13, k = 0 | a[6], T = 8191 & k, D = k >>> 13, E = 0 | a[7], L = 8191 & E, P = E >>> 13, R = 0 | a[8], I = 8191 & R, N = R >>> 13, O = 0 | a[9], U = 8191 & O, j = O >>> 13, F = 0 | s[0], G = 8191 & F, V = F >>> 13, z = 0 | s[1], H = 8191 & z, W = z >>> 13, q = 0 | s[2], X = 8191 & q, K = q >>> 13, J = 0 | s[3], Y = 8191 & J, Z = J >>> 13, Q = 0 | s[4], $ = 8191 & Q, ee = Q >>> 13, te = 0 | s[5], ie = 8191 & te, ne = te >>> 13, re = 0 | s[6], oe = 8191 & re, ae = re >>> 13, se = 0 | s[7], ce = 8191 & se, le = se >>> 13, he = 0 | s[8], ue = 8191 & he, fe = he >>> 13, de = 0 | s[9], pe = 8191 & de, me = de >>> 13;
i.negative = e.negative ^ t.negative;
i.length = 19;
var ge = (l + (n = Math.imul(u, G)) | 0) + ((8191 & (r = (r = Math.imul(u, V)) + Math.imul(f, G) | 0)) << 13) | 0;
l = ((o = Math.imul(f, V)) + (r >>> 13) | 0) + (ge >>> 26) | 0;
ge &= 67108863;
n = Math.imul(p, G);
r = (r = Math.imul(p, V)) + Math.imul(m, G) | 0;
o = Math.imul(m, V);
var ye = (l + (n = n + Math.imul(u, H) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, W) | 0) + Math.imul(f, H) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, W) | 0) + (r >>> 13) | 0) + (ye >>> 26) | 0;
ye &= 67108863;
n = Math.imul(y, G);
r = (r = Math.imul(y, V)) + Math.imul(b, G) | 0;
o = Math.imul(b, V);
n = n + Math.imul(p, H) | 0;
r = (r = r + Math.imul(p, W) | 0) + Math.imul(m, H) | 0;
o = o + Math.imul(m, W) | 0;
var be = (l + (n = n + Math.imul(u, X) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, K) | 0) + Math.imul(f, X) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, K) | 0) + (r >>> 13) | 0) + (be >>> 26) | 0;
be &= 67108863;
n = Math.imul(_, G);
r = (r = Math.imul(_, V)) + Math.imul(w, G) | 0;
o = Math.imul(w, V);
n = n + Math.imul(y, H) | 0;
r = (r = r + Math.imul(y, W) | 0) + Math.imul(b, H) | 0;
o = o + Math.imul(b, W) | 0;
n = n + Math.imul(p, X) | 0;
r = (r = r + Math.imul(p, K) | 0) + Math.imul(m, X) | 0;
o = o + Math.imul(m, K) | 0;
var ve = (l + (n = n + Math.imul(u, Y) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, Z) | 0) + Math.imul(f, Y) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, Z) | 0) + (r >>> 13) | 0) + (ve >>> 26) | 0;
ve &= 67108863;
n = Math.imul(S, G);
r = (r = Math.imul(S, V)) + Math.imul(A, G) | 0;
o = Math.imul(A, V);
n = n + Math.imul(_, H) | 0;
r = (r = r + Math.imul(_, W) | 0) + Math.imul(w, H) | 0;
o = o + Math.imul(w, W) | 0;
n = n + Math.imul(y, X) | 0;
r = (r = r + Math.imul(y, K) | 0) + Math.imul(b, X) | 0;
o = o + Math.imul(b, K) | 0;
n = n + Math.imul(p, Y) | 0;
r = (r = r + Math.imul(p, Z) | 0) + Math.imul(m, Y) | 0;
o = o + Math.imul(m, Z) | 0;
var _e = (l + (n = n + Math.imul(u, $) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, ee) | 0) + Math.imul(f, $) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, ee) | 0) + (r >>> 13) | 0) + (_e >>> 26) | 0;
_e &= 67108863;
n = Math.imul(B, G);
r = (r = Math.imul(B, V)) + Math.imul(x, G) | 0;
o = Math.imul(x, V);
n = n + Math.imul(S, H) | 0;
r = (r = r + Math.imul(S, W) | 0) + Math.imul(A, H) | 0;
o = o + Math.imul(A, W) | 0;
n = n + Math.imul(_, X) | 0;
r = (r = r + Math.imul(_, K) | 0) + Math.imul(w, X) | 0;
o = o + Math.imul(w, K) | 0;
n = n + Math.imul(y, Y) | 0;
r = (r = r + Math.imul(y, Z) | 0) + Math.imul(b, Y) | 0;
o = o + Math.imul(b, Z) | 0;
n = n + Math.imul(p, $) | 0;
r = (r = r + Math.imul(p, ee) | 0) + Math.imul(m, $) | 0;
o = o + Math.imul(m, ee) | 0;
var we = (l + (n = n + Math.imul(u, ie) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, ne) | 0) + Math.imul(f, ie) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, ne) | 0) + (r >>> 13) | 0) + (we >>> 26) | 0;
we &= 67108863;
n = Math.imul(T, G);
r = (r = Math.imul(T, V)) + Math.imul(D, G) | 0;
o = Math.imul(D, V);
n = n + Math.imul(B, H) | 0;
r = (r = r + Math.imul(B, W) | 0) + Math.imul(x, H) | 0;
o = o + Math.imul(x, W) | 0;
n = n + Math.imul(S, X) | 0;
r = (r = r + Math.imul(S, K) | 0) + Math.imul(A, X) | 0;
o = o + Math.imul(A, K) | 0;
n = n + Math.imul(_, Y) | 0;
r = (r = r + Math.imul(_, Z) | 0) + Math.imul(w, Y) | 0;
o = o + Math.imul(w, Z) | 0;
n = n + Math.imul(y, $) | 0;
r = (r = r + Math.imul(y, ee) | 0) + Math.imul(b, $) | 0;
o = o + Math.imul(b, ee) | 0;
n = n + Math.imul(p, ie) | 0;
r = (r = r + Math.imul(p, ne) | 0) + Math.imul(m, ie) | 0;
o = o + Math.imul(m, ne) | 0;
var Ce = (l + (n = n + Math.imul(u, oe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, ae) | 0) + Math.imul(f, oe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, ae) | 0) + (r >>> 13) | 0) + (Ce >>> 26) | 0;
Ce &= 67108863;
n = Math.imul(L, G);
r = (r = Math.imul(L, V)) + Math.imul(P, G) | 0;
o = Math.imul(P, V);
n = n + Math.imul(T, H) | 0;
r = (r = r + Math.imul(T, W) | 0) + Math.imul(D, H) | 0;
o = o + Math.imul(D, W) | 0;
n = n + Math.imul(B, X) | 0;
r = (r = r + Math.imul(B, K) | 0) + Math.imul(x, X) | 0;
o = o + Math.imul(x, K) | 0;
n = n + Math.imul(S, Y) | 0;
r = (r = r + Math.imul(S, Z) | 0) + Math.imul(A, Y) | 0;
o = o + Math.imul(A, Z) | 0;
n = n + Math.imul(_, $) | 0;
r = (r = r + Math.imul(_, ee) | 0) + Math.imul(w, $) | 0;
o = o + Math.imul(w, ee) | 0;
n = n + Math.imul(y, ie) | 0;
r = (r = r + Math.imul(y, ne) | 0) + Math.imul(b, ie) | 0;
o = o + Math.imul(b, ne) | 0;
n = n + Math.imul(p, oe) | 0;
r = (r = r + Math.imul(p, ae) | 0) + Math.imul(m, oe) | 0;
o = o + Math.imul(m, ae) | 0;
var Se = (l + (n = n + Math.imul(u, ce) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, le) | 0) + Math.imul(f, ce) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, le) | 0) + (r >>> 13) | 0) + (Se >>> 26) | 0;
Se &= 67108863;
n = Math.imul(I, G);
r = (r = Math.imul(I, V)) + Math.imul(N, G) | 0;
o = Math.imul(N, V);
n = n + Math.imul(L, H) | 0;
r = (r = r + Math.imul(L, W) | 0) + Math.imul(P, H) | 0;
o = o + Math.imul(P, W) | 0;
n = n + Math.imul(T, X) | 0;
r = (r = r + Math.imul(T, K) | 0) + Math.imul(D, X) | 0;
o = o + Math.imul(D, K) | 0;
n = n + Math.imul(B, Y) | 0;
r = (r = r + Math.imul(B, Z) | 0) + Math.imul(x, Y) | 0;
o = o + Math.imul(x, Z) | 0;
n = n + Math.imul(S, $) | 0;
r = (r = r + Math.imul(S, ee) | 0) + Math.imul(A, $) | 0;
o = o + Math.imul(A, ee) | 0;
n = n + Math.imul(_, ie) | 0;
r = (r = r + Math.imul(_, ne) | 0) + Math.imul(w, ie) | 0;
o = o + Math.imul(w, ne) | 0;
n = n + Math.imul(y, oe) | 0;
r = (r = r + Math.imul(y, ae) | 0) + Math.imul(b, oe) | 0;
o = o + Math.imul(b, ae) | 0;
n = n + Math.imul(p, ce) | 0;
r = (r = r + Math.imul(p, le) | 0) + Math.imul(m, ce) | 0;
o = o + Math.imul(m, le) | 0;
var Ae = (l + (n = n + Math.imul(u, ue) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, fe) | 0) + Math.imul(f, ue) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, fe) | 0) + (r >>> 13) | 0) + (Ae >>> 26) | 0;
Ae &= 67108863;
n = Math.imul(U, G);
r = (r = Math.imul(U, V)) + Math.imul(j, G) | 0;
o = Math.imul(j, V);
n = n + Math.imul(I, H) | 0;
r = (r = r + Math.imul(I, W) | 0) + Math.imul(N, H) | 0;
o = o + Math.imul(N, W) | 0;
n = n + Math.imul(L, X) | 0;
r = (r = r + Math.imul(L, K) | 0) + Math.imul(P, X) | 0;
o = o + Math.imul(P, K) | 0;
n = n + Math.imul(T, Y) | 0;
r = (r = r + Math.imul(T, Z) | 0) + Math.imul(D, Y) | 0;
o = o + Math.imul(D, Z) | 0;
n = n + Math.imul(B, $) | 0;
r = (r = r + Math.imul(B, ee) | 0) + Math.imul(x, $) | 0;
o = o + Math.imul(x, ee) | 0;
n = n + Math.imul(S, ie) | 0;
r = (r = r + Math.imul(S, ne) | 0) + Math.imul(A, ie) | 0;
o = o + Math.imul(A, ne) | 0;
n = n + Math.imul(_, oe) | 0;
r = (r = r + Math.imul(_, ae) | 0) + Math.imul(w, oe) | 0;
o = o + Math.imul(w, ae) | 0;
n = n + Math.imul(y, ce) | 0;
r = (r = r + Math.imul(y, le) | 0) + Math.imul(b, ce) | 0;
o = o + Math.imul(b, le) | 0;
n = n + Math.imul(p, ue) | 0;
r = (r = r + Math.imul(p, fe) | 0) + Math.imul(m, ue) | 0;
o = o + Math.imul(m, fe) | 0;
var Me = (l + (n = n + Math.imul(u, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(u, me) | 0) + Math.imul(f, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(f, me) | 0) + (r >>> 13) | 0) + (Me >>> 26) | 0;
Me &= 67108863;
n = Math.imul(U, H);
r = (r = Math.imul(U, W)) + Math.imul(j, H) | 0;
o = Math.imul(j, W);
n = n + Math.imul(I, X) | 0;
r = (r = r + Math.imul(I, K) | 0) + Math.imul(N, X) | 0;
o = o + Math.imul(N, K) | 0;
n = n + Math.imul(L, Y) | 0;
r = (r = r + Math.imul(L, Z) | 0) + Math.imul(P, Y) | 0;
o = o + Math.imul(P, Z) | 0;
n = n + Math.imul(T, $) | 0;
r = (r = r + Math.imul(T, ee) | 0) + Math.imul(D, $) | 0;
o = o + Math.imul(D, ee) | 0;
n = n + Math.imul(B, ie) | 0;
r = (r = r + Math.imul(B, ne) | 0) + Math.imul(x, ie) | 0;
o = o + Math.imul(x, ne) | 0;
n = n + Math.imul(S, oe) | 0;
r = (r = r + Math.imul(S, ae) | 0) + Math.imul(A, oe) | 0;
o = o + Math.imul(A, ae) | 0;
n = n + Math.imul(_, ce) | 0;
r = (r = r + Math.imul(_, le) | 0) + Math.imul(w, ce) | 0;
o = o + Math.imul(w, le) | 0;
n = n + Math.imul(y, ue) | 0;
r = (r = r + Math.imul(y, fe) | 0) + Math.imul(b, ue) | 0;
o = o + Math.imul(b, fe) | 0;
var Be = (l + (n = n + Math.imul(p, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(p, me) | 0) + Math.imul(m, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(m, me) | 0) + (r >>> 13) | 0) + (Be >>> 26) | 0;
Be &= 67108863;
n = Math.imul(U, X);
r = (r = Math.imul(U, K)) + Math.imul(j, X) | 0;
o = Math.imul(j, K);
n = n + Math.imul(I, Y) | 0;
r = (r = r + Math.imul(I, Z) | 0) + Math.imul(N, Y) | 0;
o = o + Math.imul(N, Z) | 0;
n = n + Math.imul(L, $) | 0;
r = (r = r + Math.imul(L, ee) | 0) + Math.imul(P, $) | 0;
o = o + Math.imul(P, ee) | 0;
n = n + Math.imul(T, ie) | 0;
r = (r = r + Math.imul(T, ne) | 0) + Math.imul(D, ie) | 0;
o = o + Math.imul(D, ne) | 0;
n = n + Math.imul(B, oe) | 0;
r = (r = r + Math.imul(B, ae) | 0) + Math.imul(x, oe) | 0;
o = o + Math.imul(x, ae) | 0;
n = n + Math.imul(S, ce) | 0;
r = (r = r + Math.imul(S, le) | 0) + Math.imul(A, ce) | 0;
o = o + Math.imul(A, le) | 0;
n = n + Math.imul(_, ue) | 0;
r = (r = r + Math.imul(_, fe) | 0) + Math.imul(w, ue) | 0;
o = o + Math.imul(w, fe) | 0;
var xe = (l + (n = n + Math.imul(y, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(y, me) | 0) + Math.imul(b, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(b, me) | 0) + (r >>> 13) | 0) + (xe >>> 26) | 0;
xe &= 67108863;
n = Math.imul(U, Y);
r = (r = Math.imul(U, Z)) + Math.imul(j, Y) | 0;
o = Math.imul(j, Z);
n = n + Math.imul(I, $) | 0;
r = (r = r + Math.imul(I, ee) | 0) + Math.imul(N, $) | 0;
o = o + Math.imul(N, ee) | 0;
n = n + Math.imul(L, ie) | 0;
r = (r = r + Math.imul(L, ne) | 0) + Math.imul(P, ie) | 0;
o = o + Math.imul(P, ne) | 0;
n = n + Math.imul(T, oe) | 0;
r = (r = r + Math.imul(T, ae) | 0) + Math.imul(D, oe) | 0;
o = o + Math.imul(D, ae) | 0;
n = n + Math.imul(B, ce) | 0;
r = (r = r + Math.imul(B, le) | 0) + Math.imul(x, ce) | 0;
o = o + Math.imul(x, le) | 0;
n = n + Math.imul(S, ue) | 0;
r = (r = r + Math.imul(S, fe) | 0) + Math.imul(A, ue) | 0;
o = o + Math.imul(A, fe) | 0;
var ke = (l + (n = n + Math.imul(_, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(_, me) | 0) + Math.imul(w, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(w, me) | 0) + (r >>> 13) | 0) + (ke >>> 26) | 0;
ke &= 67108863;
n = Math.imul(U, $);
r = (r = Math.imul(U, ee)) + Math.imul(j, $) | 0;
o = Math.imul(j, ee);
n = n + Math.imul(I, ie) | 0;
r = (r = r + Math.imul(I, ne) | 0) + Math.imul(N, ie) | 0;
o = o + Math.imul(N, ne) | 0;
n = n + Math.imul(L, oe) | 0;
r = (r = r + Math.imul(L, ae) | 0) + Math.imul(P, oe) | 0;
o = o + Math.imul(P, ae) | 0;
n = n + Math.imul(T, ce) | 0;
r = (r = r + Math.imul(T, le) | 0) + Math.imul(D, ce) | 0;
o = o + Math.imul(D, le) | 0;
n = n + Math.imul(B, ue) | 0;
r = (r = r + Math.imul(B, fe) | 0) + Math.imul(x, ue) | 0;
o = o + Math.imul(x, fe) | 0;
var Te = (l + (n = n + Math.imul(S, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(S, me) | 0) + Math.imul(A, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(A, me) | 0) + (r >>> 13) | 0) + (Te >>> 26) | 0;
Te &= 67108863;
n = Math.imul(U, ie);
r = (r = Math.imul(U, ne)) + Math.imul(j, ie) | 0;
o = Math.imul(j, ne);
n = n + Math.imul(I, oe) | 0;
r = (r = r + Math.imul(I, ae) | 0) + Math.imul(N, oe) | 0;
o = o + Math.imul(N, ae) | 0;
n = n + Math.imul(L, ce) | 0;
r = (r = r + Math.imul(L, le) | 0) + Math.imul(P, ce) | 0;
o = o + Math.imul(P, le) | 0;
n = n + Math.imul(T, ue) | 0;
r = (r = r + Math.imul(T, fe) | 0) + Math.imul(D, ue) | 0;
o = o + Math.imul(D, fe) | 0;
var De = (l + (n = n + Math.imul(B, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(B, me) | 0) + Math.imul(x, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(x, me) | 0) + (r >>> 13) | 0) + (De >>> 26) | 0;
De &= 67108863;
n = Math.imul(U, oe);
r = (r = Math.imul(U, ae)) + Math.imul(j, oe) | 0;
o = Math.imul(j, ae);
n = n + Math.imul(I, ce) | 0;
r = (r = r + Math.imul(I, le) | 0) + Math.imul(N, ce) | 0;
o = o + Math.imul(N, le) | 0;
n = n + Math.imul(L, ue) | 0;
r = (r = r + Math.imul(L, fe) | 0) + Math.imul(P, ue) | 0;
o = o + Math.imul(P, fe) | 0;
var Ee = (l + (n = n + Math.imul(T, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(T, me) | 0) + Math.imul(D, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(D, me) | 0) + (r >>> 13) | 0) + (Ee >>> 26) | 0;
Ee &= 67108863;
n = Math.imul(U, ce);
r = (r = Math.imul(U, le)) + Math.imul(j, ce) | 0;
o = Math.imul(j, le);
n = n + Math.imul(I, ue) | 0;
r = (r = r + Math.imul(I, fe) | 0) + Math.imul(N, ue) | 0;
o = o + Math.imul(N, fe) | 0;
var Le = (l + (n = n + Math.imul(L, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(L, me) | 0) + Math.imul(P, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(P, me) | 0) + (r >>> 13) | 0) + (Le >>> 26) | 0;
Le &= 67108863;
n = Math.imul(U, ue);
r = (r = Math.imul(U, fe)) + Math.imul(j, ue) | 0;
o = Math.imul(j, fe);
var Pe = (l + (n = n + Math.imul(I, pe) | 0) | 0) + ((8191 & (r = (r = r + Math.imul(I, me) | 0) + Math.imul(N, pe) | 0)) << 13) | 0;
l = ((o = o + Math.imul(N, me) | 0) + (r >>> 13) | 0) + (Pe >>> 26) | 0;
Pe &= 67108863;
var Re = (l + (n = Math.imul(U, pe)) | 0) + ((8191 & (r = (r = Math.imul(U, me)) + Math.imul(j, pe) | 0)) << 13) | 0;
l = ((o = Math.imul(j, me)) + (r >>> 13) | 0) + (Re >>> 26) | 0;
Re &= 67108863;
c[0] = ge;
c[1] = ye;
c[2] = be;
c[3] = ve;
c[4] = _e;
c[5] = we;
c[6] = Ce;
c[7] = Se;
c[8] = Ae;
c[9] = Me;
c[10] = Be;
c[11] = xe;
c[12] = ke;
c[13] = Te;
c[14] = De;
c[15] = Ee;
c[16] = Le;
c[17] = Pe;
c[18] = Re;
if (0 !== l) {
c[19] = l;
i.length++;
}
return i;
};
Math.imul || (b = y);
function v(e, t, i) {
i.negative = t.negative ^ e.negative;
i.length = e.length + t.length;
for (var n = 0, r = 0, o = 0; o < i.length - 1; o++) {
var a = r;
r = 0;
for (var s = 67108863 & n, c = Math.min(o, t.length - 1), l = Math.max(0, o - e.length + 1); l <= c; l++) {
var h = o - l, u = (0 | e.words[h]) * (0 | t.words[l]), f = 67108863 & u;
s = 67108863 & (f = f + s | 0);
r += (a = (a = a + (u / 67108864 | 0) | 0) + (f >>> 26) | 0) >>> 26;
a &= 67108863;
}
i.words[o] = s;
n = a;
a = r;
}
0 !== n ? i.words[o] = n : i.length--;
return i._strip();
}
function _(e, t, i) {
return v(e, t, i);
}
o.prototype.mulTo = function(e, t) {
var i = this.length + e.length;
return 10 === this.length && 10 === e.length ? b(this, e, t) : i < 63 ? y(this, e, t) : i < 1024 ? v(this, e, t) : _(this, e, t);
};
function w(e, t) {
this.x = e;
this.y = t;
}
w.prototype.makeRBT = function(e) {
for (var t = new Array(e), i = o.prototype._countBits(e) - 1, n = 0; n < e; n++) t[n] = this.revBin(n, i, e);
return t;
};
w.prototype.revBin = function(e, t, i) {
if (0 === e || e === i - 1) return e;
for (var n = 0, r = 0; r < t; r++) {
n |= (1 & e) << t - r - 1;
e >>= 1;
}
return n;
};
w.prototype.permute = function(e, t, i, n, r, o) {
for (var a = 0; a < o; a++) {
n[a] = t[e[a]];
r[a] = i[e[a]];
}
};
w.prototype.transform = function(e, t, i, n, r, o) {
this.permute(o, e, t, i, n, r);
for (var a = 1; a < r; a <<= 1) for (var s = a << 1, c = Math.cos(2 * Math.PI / s), l = Math.sin(2 * Math.PI / s), h = 0; h < r; h += s) for (var u = c, f = l, d = 0; d < a; d++) {
var p = i[h + d], m = n[h + d], g = i[h + d + a], y = n[h + d + a], b = u * g - f * y;
y = u * y + f * g;
g = b;
i[h + d] = p + g;
n[h + d] = m + y;
i[h + d + a] = p - g;
n[h + d + a] = m - y;
if (d !== s) {
b = c * u - l * f;
f = c * f + l * u;
u = b;
}
}
};
w.prototype.guessLen13b = function(e, t) {
var i = 1 | Math.max(t, e), n = 1 & i, r = 0;
for (i = i / 2 | 0; i; i >>>= 1) r++;
return 1 << r + 1 + n;
};
w.prototype.conjugate = function(e, t, i) {
if (!(i <= 1)) for (var n = 0; n < i / 2; n++) {
var r = e[n];
e[n] = e[i - n - 1];
e[i - n - 1] = r;
r = t[n];
t[n] = -t[i - n - 1];
t[i - n - 1] = -r;
}
};
w.prototype.normalize13b = function(e, t) {
for (var i = 0, n = 0; n < t / 2; n++) {
var r = 8192 * Math.round(e[2 * n + 1] / t) + Math.round(e[2 * n] / t) + i;
e[n] = 67108863 & r;
i = r < 67108864 ? 0 : r / 67108864 | 0;
}
return e;
};
w.prototype.convert13b = function(e, t, i, r) {
for (var o = 0, a = 0; a < t; a++) {
o += 0 | e[a];
i[2 * a] = 8191 & o;
o >>>= 13;
i[2 * a + 1] = 8191 & o;
o >>>= 13;
}
for (a = 2 * t; a < r; ++a) i[a] = 0;
n(0 === o);
n(0 == (-8192 & o));
};
w.prototype.stub = function(e) {
for (var t = new Array(e), i = 0; i < e; i++) t[i] = 0;
return t;
};
w.prototype.mulp = function(e, t, i) {
var n = 2 * this.guessLen13b(e.length, t.length), r = this.makeRBT(n), o = this.stub(n), a = new Array(n), s = new Array(n), c = new Array(n), l = new Array(n), h = new Array(n), u = new Array(n), f = i.words;
f.length = n;
this.convert13b(e.words, e.length, a, n);
this.convert13b(t.words, t.length, l, n);
this.transform(a, o, s, c, n, r);
this.transform(l, o, h, u, n, r);
for (var d = 0; d < n; d++) {
var p = s[d] * h[d] - c[d] * u[d];
c[d] = s[d] * u[d] + c[d] * h[d];
s[d] = p;
}
this.conjugate(s, c, n);
this.transform(s, c, f, o, n, r);
this.conjugate(f, o, n);
this.normalize13b(f, n);
i.negative = e.negative ^ t.negative;
i.length = e.length + t.length;
return i._strip();
};
o.prototype.mul = function(e) {
var t = new o(null);
t.words = new Array(this.length + e.length);
return this.mulTo(e, t);
};
o.prototype.mulf = function(e) {
var t = new o(null);
t.words = new Array(this.length + e.length);
return _(this, e, t);
};
o.prototype.imul = function(e) {
return this.clone().mulTo(e, this);
};
o.prototype.imuln = function(e) {
var t = e < 0;
t && (e = -e);
n("number" == typeof e);
n(e < 67108864);
for (var i = 0, r = 0; r < this.length; r++) {
var o = (0 | this.words[r]) * e, a = (67108863 & o) + (67108863 & i);
i >>= 26;
i += o / 67108864 | 0;
i += a >>> 26;
this.words[r] = 67108863 & a;
}
if (0 !== i) {
this.words[r] = i;
this.length++;
}
return t ? this.ineg() : this;
};
o.prototype.muln = function(e) {
return this.clone().imuln(e);
};
o.prototype.sqr = function() {
return this.mul(this);
};
o.prototype.isqr = function() {
return this.imul(this.clone());
};
o.prototype.pow = function(e) {
var t = g(e);
if (0 === t.length) return new o(1);
for (var i = this, n = 0; n < t.length && 0 === t[n]; n++, i = i.sqr()) ;
if (++n < t.length) for (var r = i.sqr(); n < t.length; n++, r = r.sqr()) 0 !== t[n] && (i = i.mul(r));
return i;
};
o.prototype.iushln = function(e) {
n("number" == typeof e && e >= 0);
var t, i = e % 26, r = (e - i) / 26, o = 67108863 >>> 26 - i << 26 - i;
if (0 !== i) {
var a = 0;
for (t = 0; t < this.length; t++) {
var s = this.words[t] & o, c = (0 | this.words[t]) - s << i;
this.words[t] = c | a;
a = s >>> 26 - i;
}
if (a) {
this.words[t] = a;
this.length++;
}
}
if (0 !== r) {
for (t = this.length - 1; t >= 0; t--) this.words[t + r] = this.words[t];
for (t = 0; t < r; t++) this.words[t] = 0;
this.length += r;
}
return this._strip();
};
o.prototype.ishln = function(e) {
n(0 === this.negative);
return this.iushln(e);
};
o.prototype.iushrn = function(e, t, i) {
n("number" == typeof e && e >= 0);
var r;
r = t ? (t - t % 26) / 26 : 0;
var o = e % 26, a = Math.min((e - o) / 26, this.length), s = 67108863 ^ 67108863 >>> o << o, c = i;
r -= a;
r = Math.max(0, r);
if (c) {
for (var l = 0; l < a; l++) c.words[l] = this.words[l];
c.length = a;
}
if (0 === a) ; else if (this.length > a) {
this.length -= a;
for (l = 0; l < this.length; l++) this.words[l] = this.words[l + a];
} else {
this.words[0] = 0;
this.length = 1;
}
var h = 0;
for (l = this.length - 1; l >= 0 && (0 !== h || l >= r); l--) {
var u = 0 | this.words[l];
this.words[l] = h << 26 - o | u >>> o;
h = u & s;
}
c && 0 !== h && (c.words[c.length++] = h);
if (0 === this.length) {
this.words[0] = 0;
this.length = 1;
}
return this._strip();
};
o.prototype.ishrn = function(e, t, i) {
n(0 === this.negative);
return this.iushrn(e, t, i);
};
o.prototype.shln = function(e) {
return this.clone().ishln(e);
};
o.prototype.ushln = function(e) {
return this.clone().iushln(e);
};
o.prototype.shrn = function(e) {
return this.clone().ishrn(e);
};
o.prototype.ushrn = function(e) {
return this.clone().iushrn(e);
};
o.prototype.testn = function(e) {
n("number" == typeof e && e >= 0);
var t = e % 26, i = (e - t) / 26, r = 1 << t;
return !(this.length <= i || !(this.words[i] & r));
};
o.prototype.imaskn = function(e) {
n("number" == typeof e && e >= 0);
var t = e % 26, i = (e - t) / 26;
n(0 === this.negative, "imaskn works only with positive numbers");
if (this.length <= i) return this;
0 !== t && i++;
this.length = Math.min(i, this.length);
if (0 !== t) {
var r = 67108863 ^ 67108863 >>> t << t;
this.words[this.length - 1] &= r;
}
return this._strip();
};
o.prototype.maskn = function(e) {
return this.clone().imaskn(e);
};
o.prototype.iaddn = function(e) {
n("number" == typeof e);
n(e < 67108864);
if (e < 0) return this.isubn(-e);
if (0 !== this.negative) {
if (1 === this.length && (0 | this.words[0]) <= e) {
this.words[0] = e - (0 | this.words[0]);
this.negative = 0;
return this;
}
this.negative = 0;
this.isubn(e);
this.negative = 1;
return this;
}
return this._iaddn(e);
};
o.prototype._iaddn = function(e) {
this.words[0] += e;
for (var t = 0; t < this.length && this.words[t] >= 67108864; t++) {
this.words[t] -= 67108864;
t === this.length - 1 ? this.words[t + 1] = 1 : this.words[t + 1]++;
}
this.length = Math.max(this.length, t + 1);
return this;
};
o.prototype.isubn = function(e) {
n("number" == typeof e);
n(e < 67108864);
if (e < 0) return this.iaddn(-e);
if (0 !== this.negative) {
this.negative = 0;
this.iaddn(e);
this.negative = 1;
return this;
}
this.words[0] -= e;
if (1 === this.length && this.words[0] < 0) {
this.words[0] = -this.words[0];
this.negative = 1;
} else for (var t = 0; t < this.length && this.words[t] < 0; t++) {
this.words[t] += 67108864;
this.words[t + 1] -= 1;
}
return this._strip();
};
o.prototype.addn = function(e) {
return this.clone().iaddn(e);
};
o.prototype.subn = function(e) {
return this.clone().isubn(e);
};
o.prototype.iabs = function() {
this.negative = 0;
return this;
};
o.prototype.abs = function() {
return this.clone().iabs();
};
o.prototype._ishlnsubmul = function(e, t, i) {
var r, o, a = e.length + i;
this._expand(a);
var s = 0;
for (r = 0; r < e.length; r++) {
o = (0 | this.words[r + i]) + s;
var c = (0 | e.words[r]) * t;
s = ((o -= 67108863 & c) >> 26) - (c / 67108864 | 0);
this.words[r + i] = 67108863 & o;
}
for (;r < this.length - i; r++) {
s = (o = (0 | this.words[r + i]) + s) >> 26;
this.words[r + i] = 67108863 & o;
}
if (0 === s) return this._strip();
n(-1 === s);
s = 0;
for (r = 0; r < this.length; r++) {
s = (o = -(0 | this.words[r]) + s) >> 26;
this.words[r] = 67108863 & o;
}
this.negative = 1;
return this._strip();
};
o.prototype._wordDiv = function(e, t) {
var i = (this.length, e.length), n = this.clone(), r = e, a = 0 | r.words[r.length - 1];
if (0 != (i = 26 - this._countBits(a))) {
r = r.ushln(i);
n.iushln(i);
a = 0 | r.words[r.length - 1];
}
var s, c = n.length - r.length;
if ("mod" !== t) {
(s = new o(null)).length = c + 1;
s.words = new Array(s.length);
for (var l = 0; l < s.length; l++) s.words[l] = 0;
}
var h = n.clone()._ishlnsubmul(r, 1, c);
if (0 === h.negative) {
n = h;
s && (s.words[c] = 1);
}
for (var u = c - 1; u >= 0; u--) {
var f = 67108864 * (0 | n.words[r.length + u]) + (0 | n.words[r.length + u - 1]);
f = Math.min(f / a | 0, 67108863);
n._ishlnsubmul(r, f, u);
for (;0 !== n.negative; ) {
f--;
n.negative = 0;
n._ishlnsubmul(r, 1, u);
n.isZero() || (n.negative ^= 1);
}
s && (s.words[u] = f);
}
s && s._strip();
n._strip();
"div" !== t && 0 !== i && n.iushrn(i);
return {
div: s || null,
mod: n
};
};
o.prototype.divmod = function(e, t, i) {
n(!e.isZero());
if (this.isZero()) return {
div: new o(0),
mod: new o(0)
};
var r, a, s;
if (0 !== this.negative && 0 === e.negative) {
s = this.neg().divmod(e, t);
"mod" !== t && (r = s.div.neg());
if ("div" !== t) {
a = s.mod.neg();
i && 0 !== a.negative && a.iadd(e);
}
return {
div: r,
mod: a
};
}
if (0 === this.negative && 0 !== e.negative) {
s = this.divmod(e.neg(), t);
"mod" !== t && (r = s.div.neg());
return {
div: r,
mod: s.mod
};
}
if (0 != (this.negative & e.negative)) {
s = this.neg().divmod(e.neg(), t);
if ("div" !== t) {
a = s.mod.neg();
i && 0 !== a.negative && a.isub(e);
}
return {
div: s.div,
mod: a
};
}
return e.length > this.length || this.cmp(e) < 0 ? {
div: new o(0),
mod: this
} : 1 === e.length ? "div" === t ? {
div: this.divn(e.words[0]),
mod: null
} : "mod" === t ? {
div: null,
mod: new o(this.modrn(e.words[0]))
} : {
div: this.divn(e.words[0]),
mod: new o(this.modrn(e.words[0]))
} : this._wordDiv(e, t);
};
o.prototype.div = function(e) {
return this.divmod(e, "div", !1).div;
};
o.prototype.mod = function(e) {
return this.divmod(e, "mod", !1).mod;
};
o.prototype.umod = function(e) {
return this.divmod(e, "mod", !0).mod;
};
o.prototype.divRound = function(e) {
var t = this.divmod(e);
if (t.mod.isZero()) return t.div;
var i = 0 !== t.div.negative ? t.mod.isub(e) : t.mod, n = e.ushrn(1), r = e.andln(1), o = i.cmp(n);
return o < 0 || 1 === r && 0 === o ? t.div : 0 !== t.div.negative ? t.div.isubn(1) : t.div.iaddn(1);
};
o.prototype.modrn = function(e) {
var t = e < 0;
t && (e = -e);
n(e <= 67108863);
for (var i = (1 << 26) % e, r = 0, o = this.length - 1; o >= 0; o--) r = (i * r + (0 | this.words[o])) % e;
return t ? -r : r;
};
o.prototype.modn = function(e) {
return this.modrn(e);
};
o.prototype.idivn = function(e) {
var t = e < 0;
t && (e = -e);
n(e <= 67108863);
for (var i = 0, r = this.length - 1; r >= 0; r--) {
var o = (0 | this.words[r]) + 67108864 * i;
this.words[r] = o / e | 0;
i = o % e;
}
this._strip();
return t ? this.ineg() : this;
};
o.prototype.divn = function(e) {
return this.clone().idivn(e);
};
o.prototype.egcd = function(e) {
n(0 === e.negative);
n(!e.isZero());
var t = this, i = e.clone();
t = 0 !== t.negative ? t.umod(e) : t.clone();
for (var r = new o(1), a = new o(0), s = new o(0), c = new o(1), l = 0; t.isEven() && i.isEven(); ) {
t.iushrn(1);
i.iushrn(1);
++l;
}
for (var h = i.clone(), u = t.clone(); !t.isZero(); ) {
for (var f = 0, d = 1; 0 == (t.words[0] & d) && f < 26; ++f, d <<= 1) ;
if (f > 0) {
t.iushrn(f);
for (;f-- > 0; ) {
if (r.isOdd() || a.isOdd()) {
r.iadd(h);
a.isub(u);
}
r.iushrn(1);
a.iushrn(1);
}
}
for (var p = 0, m = 1; 0 == (i.words[0] & m) && p < 26; ++p, m <<= 1) ;
if (p > 0) {
i.iushrn(p);
for (;p-- > 0; ) {
if (s.isOdd() || c.isOdd()) {
s.iadd(h);
c.isub(u);
}
s.iushrn(1);
c.iushrn(1);
}
}
if (t.cmp(i) >= 0) {
t.isub(i);
r.isub(s);
a.isub(c);
} else {
i.isub(t);
s.isub(r);
c.isub(a);
}
}
return {
a: s,
b: c,
gcd: i.iushln(l)
};
};
o.prototype._invmp = function(e) {
n(0 === e.negative);
n(!e.isZero());
var t = this, i = e.clone();
t = 0 !== t.negative ? t.umod(e) : t.clone();
for (var r, a = new o(1), s = new o(0), c = i.clone(); t.cmpn(1) > 0 && i.cmpn(1) > 0; ) {
for (var l = 0, h = 1; 0 == (t.words[0] & h) && l < 26; ++l, h <<= 1) ;
if (l > 0) {
t.iushrn(l);
for (;l-- > 0; ) {
a.isOdd() && a.iadd(c);
a.iushrn(1);
}
}
for (var u = 0, f = 1; 0 == (i.words[0] & f) && u < 26; ++u, f <<= 1) ;
if (u > 0) {
i.iushrn(u);
for (;u-- > 0; ) {
s.isOdd() && s.iadd(c);
s.iushrn(1);
}
}
if (t.cmp(i) >= 0) {
t.isub(i);
a.isub(s);
} else {
i.isub(t);
s.isub(a);
}
}
(r = 0 === t.cmpn(1) ? a : s).cmpn(0) < 0 && r.iadd(e);
return r;
};
o.prototype.gcd = function(e) {
if (this.isZero()) return e.abs();
if (e.isZero()) return this.abs();
var t = this.clone(), i = e.clone();
t.negative = 0;
i.negative = 0;
for (var n = 0; t.isEven() && i.isEven(); n++) {
t.iushrn(1);
i.iushrn(1);
}
for (;;) {
for (;t.isEven(); ) t.iushrn(1);
for (;i.isEven(); ) i.iushrn(1);
var r = t.cmp(i);
if (r < 0) {
var o = t;
t = i;
i = o;
} else if (0 === r || 0 === i.cmpn(1)) break;
t.isub(i);
}
return i.iushln(n);
};
o.prototype.invm = function(e) {
return this.egcd(e).a.umod(e);
};
o.prototype.isEven = function() {
return 0 == (1 & this.words[0]);
};
o.prototype.isOdd = function() {
return 1 == (1 & this.words[0]);
};
o.prototype.andln = function(e) {
return this.words[0] & e;
};
o.prototype.bincn = function(e) {
n("number" == typeof e);
var t = e % 26, i = (e - t) / 26, r = 1 << t;
if (this.length <= i) {
this._expand(i + 1);
this.words[i] |= r;
return this;
}
for (var o = r, a = i; 0 !== o && a < this.length; a++) {
var s = 0 | this.words[a];
o = (s += o) >>> 26;
s &= 67108863;
this.words[a] = s;
}
if (0 !== o) {
this.words[a] = o;
this.length++;
}
return this;
};
o.prototype.isZero = function() {
return 1 === this.length && 0 === this.words[0];
};
o.prototype.cmpn = function(e) {
var t, i = e < 0;
if (0 !== this.negative && !i) return -1;
if (0 === this.negative && i) return 1;
this._strip();
if (this.length > 1) t = 1; else {
i && (e = -e);
n(e <= 67108863, "Number is too big");
var r = 0 | this.words[0];
t = r === e ? 0 : r < e ? -1 : 1;
}
return 0 !== this.negative ? 0 | -t : t;
};
o.prototype.cmp = function(e) {
if (0 !== this.negative && 0 === e.negative) return -1;
if (0 === this.negative && 0 !== e.negative) return 1;
var t = this.ucmp(e);
return 0 !== this.negative ? 0 | -t : t;
};
o.prototype.ucmp = function(e) {
if (this.length > e.length) return 1;
if (this.length < e.length) return -1;
for (var t = 0, i = this.length - 1; i >= 0; i--) {
var n = 0 | this.words[i], r = 0 | e.words[i];
if (n !== r) {
n < r ? t = -1 : n > r && (t = 1);
break;
}
}
return t;
};
o.prototype.gtn = function(e) {
return 1 === this.cmpn(e);
};
o.prototype.gt = function(e) {
return 1 === this.cmp(e);
};
o.prototype.gten = function(e) {
return this.cmpn(e) >= 0;
};
o.prototype.gte = function(e) {
return this.cmp(e) >= 0;
};
o.prototype.ltn = function(e) {
return -1 === this.cmpn(e);
};
o.prototype.lt = function(e) {
return -1 === this.cmp(e);
};
o.prototype.lten = function(e) {
return this.cmpn(e) <= 0;
};
o.prototype.lte = function(e) {
return this.cmp(e) <= 0;
};
o.prototype.eqn = function(e) {
return 0 === this.cmpn(e);
};
o.prototype.eq = function(e) {
return 0 === this.cmp(e);
};
o.red = function(e) {
return new k(e);
};
o.prototype.toRed = function(e) {
n(!this.red, "Already a number in reduction context");
n(0 === this.negative, "red works only with positives");
return e.convertTo(this)._forceRed(e);
};
o.prototype.fromRed = function() {
n(this.red, "fromRed works only with numbers in reduction context");
return this.red.convertFrom(this);
};
o.prototype._forceRed = function(e) {
this.red = e;
return this;
};
o.prototype.forceRed = function(e) {
n(!this.red, "Already a number in reduction context");
return this._forceRed(e);
};
o.prototype.redAdd = function(e) {
n(this.red, "redAdd works only with red numbers");
return this.red.add(this, e);
};
o.prototype.redIAdd = function(e) {
n(this.red, "redIAdd works only with red numbers");
return this.red.iadd(this, e);
};
o.prototype.redSub = function(e) {
n(this.red, "redSub works only with red numbers");
return this.red.sub(this, e);
};
o.prototype.redISub = function(e) {
n(this.red, "redISub works only with red numbers");
return this.red.isub(this, e);
};
o.prototype.redShl = function(e) {
n(this.red, "redShl works only with red numbers");
return this.red.shl(this, e);
};
o.prototype.redMul = function(e) {
n(this.red, "redMul works only with red numbers");
this.red._verify2(this, e);
return this.red.mul(this, e);
};
o.prototype.redIMul = function(e) {
n(this.red, "redMul works only with red numbers");
this.red._verify2(this, e);
return this.red.imul(this, e);
};
o.prototype.redSqr = function() {
n(this.red, "redSqr works only with red numbers");
this.red._verify1(this);
return this.red.sqr(this);
};
o.prototype.redISqr = function() {
n(this.red, "redISqr works only with red numbers");
this.red._verify1(this);
return this.red.isqr(this);
};
o.prototype.redSqrt = function() {
n(this.red, "redSqrt works only with red numbers");
this.red._verify1(this);
return this.red.sqrt(this);
};
o.prototype.redInvm = function() {
n(this.red, "redInvm works only with red numbers");
this.red._verify1(this);
return this.red.invm(this);
};
o.prototype.redNeg = function() {
n(this.red, "redNeg works only with red numbers");
this.red._verify1(this);
return this.red.neg(this);
};
o.prototype.redPow = function(e) {
n(this.red && !e.red, "redPow(normalNum)");
this.red._verify1(this);
return this.red.pow(this, e);
};
var C = {
k256: null,
p224: null,
p192: null,
p25519: null
};
function S(e, t) {
this.name = e;
this.p = new o(t, 16);
this.n = this.p.bitLength();
this.k = new o(1).iushln(this.n).isub(this.p);
this.tmp = this._tmp();
}
S.prototype._tmp = function() {
var e = new o(null);
e.words = new Array(Math.ceil(this.n / 13));
return e;
};
S.prototype.ireduce = function(e) {
var t, i = e;
do {
this.split(i, this.tmp);
t = (i = (i = this.imulK(i)).iadd(this.tmp)).bitLength();
} while (t > this.n);
var n = t < this.n ? -1 : i.ucmp(this.p);
if (0 === n) {
i.words[0] = 0;
i.length = 1;
} else n > 0 ? i.isub(this.p) : void 0 !== i.strip ? i.strip() : i._strip();
return i;
};
S.prototype.split = function(e, t) {
e.iushrn(this.n, 0, t);
};
S.prototype.imulK = function(e) {
return e.imul(this.k);
};
function A() {
S.call(this, "k256", "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe fffffc2f");
}
r(A, S);
A.prototype.split = function(e, t) {
for (var i = Math.min(e.length, 9), n = 0; n < i; n++) t.words[n] = e.words[n];
t.length = i;
if (e.length <= 9) {
e.words[0] = 0;
e.length = 1;
} else {
var r = e.words[9];
t.words[t.length++] = 4194303 & r;
for (n = 10; n < e.length; n++) {
var o = 0 | e.words[n];
e.words[n - 10] = (4194303 & o) << 4 | r >>> 22;
r = o;
}
r >>>= 22;
e.words[n - 10] = r;
0 === r && e.length > 10 ? e.length -= 10 : e.length -= 9;
}
};
A.prototype.imulK = function(e) {
e.words[e.length] = 0;
e.words[e.length + 1] = 0;
e.length += 2;
for (var t = 0, i = 0; i < e.length; i++) {
var n = 0 | e.words[i];
t += 977 * n;
e.words[i] = 67108863 & t;
t = 64 * n + (t / 67108864 | 0);
}
if (0 === e.words[e.length - 1]) {
e.length--;
0 === e.words[e.length - 1] && e.length--;
}
return e;
};
function M() {
S.call(this, "p224", "ffffffff ffffffff ffffffff ffffffff 00000000 00000000 00000001");
}
r(M, S);
function B() {
S.call(this, "p192", "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff");
}
r(B, S);
function x() {
S.call(this, "25519", "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed");
}
r(x, S);
x.prototype.imulK = function(e) {
for (var t = 0, i = 0; i < e.length; i++) {
var n = 19 * (0 | e.words[i]) + t, r = 67108863 & n;
n >>>= 26;
e.words[i] = r;
t = n;
}
0 !== t && (e.words[e.length++] = t);
return e;
};
o._prime = function(e) {
if (C[e]) return C[e];
var t;
if ("k256" === e) t = new A(); else if ("p224" === e) t = new M(); else if ("p192" === e) t = new B(); else {
if ("p25519" !== e) throw new Error("Unknown prime " + e);
t = new x();
}
C[e] = t;
return t;
};
function k(e) {
if ("string" == typeof e) {
var t = o._prime(e);
this.m = t.p;
this.prime = t;
} else {
n(e.gtn(1), "modulus must be greater than 1");
this.m = e;
this.prime = null;
}
}
k.prototype._verify1 = function(e) {
n(0 === e.negative, "red works only with positives");
n(e.red, "red works only with red numbers");
};
k.prototype._verify2 = function(e, t) {
n(0 == (e.negative | t.negative), "red works only with positives");
n(e.red && e.red === t.red, "red works only with red numbers");
};
k.prototype.imod = function(e) {
if (this.prime) return this.prime.ireduce(e)._forceRed(this);
h(e, e.umod(this.m)._forceRed(this));
return e;
};
k.prototype.neg = function(e) {
return e.isZero() ? e.clone() : this.m.sub(e)._forceRed(this);
};
k.prototype.add = function(e, t) {
this._verify2(e, t);
var i = e.add(t);
i.cmp(this.m) >= 0 && i.isub(this.m);
return i._forceRed(this);
};
k.prototype.iadd = function(e, t) {
this._verify2(e, t);
var i = e.iadd(t);
i.cmp(this.m) >= 0 && i.isub(this.m);
return i;
};
k.prototype.sub = function(e, t) {
this._verify2(e, t);
var i = e.sub(t);
i.cmpn(0) < 0 && i.iadd(this.m);
return i._forceRed(this);
};
k.prototype.isub = function(e, t) {
this._verify2(e, t);
var i = e.isub(t);
i.cmpn(0) < 0 && i.iadd(this.m);
return i;
};
k.prototype.shl = function(e, t) {
this._verify1(e);
return this.imod(e.ushln(t));
};
k.prototype.imul = function(e, t) {
this._verify2(e, t);
return this.imod(e.imul(t));
};
k.prototype.mul = function(e, t) {
this._verify2(e, t);
return this.imod(e.mul(t));
};
k.prototype.isqr = function(e) {
return this.imul(e, e.clone());
};
k.prototype.sqr = function(e) {
return this.mul(e, e);
};
k.prototype.sqrt = function(e) {
if (e.isZero()) return e.clone();
var t = this.m.andln(3);
n(t % 2 == 1);
if (3 === t) {
var i = this.m.add(new o(1)).iushrn(2);
return this.pow(e, i);
}
for (var r = this.m.subn(1), a = 0; !r.isZero() && 0 === r.andln(1); ) {
a++;
r.iushrn(1);
}
n(!r.isZero());
var s = new o(1).toRed(this), c = s.redNeg(), l = this.m.subn(1).iushrn(1), h = this.m.bitLength();
h = new o(2 * h * h).toRed(this);
for (;0 !== this.pow(h, l).cmp(c); ) h.redIAdd(c);
for (var u = this.pow(h, r), f = this.pow(e, r.addn(1).iushrn(1)), d = this.pow(e, r), p = a; 0 !== d.cmp(s); ) {
for (var m = d, g = 0; 0 !== m.cmp(s); g++) m = m.redSqr();
n(g < p);
var y = this.pow(u, new o(1).iushln(p - g - 1));
f = f.redMul(y);
u = y.redSqr();
d = d.redMul(u);
p = g;
}
return f;
};
k.prototype.invm = function(e) {
var t = e._invmp(this.m);
if (0 !== t.negative) {
t.negative = 0;
return this.imod(t).redNeg();
}
return this.imod(t);
};
k.prototype.pow = function(e, t) {
if (t.isZero()) return new o(1).toRed(this);
if (0 === t.cmpn(1)) return e.clone();
var i = new Array(16);
i[0] = new o(1).toRed(this);
i[1] = e;
for (var n = 2; n < i.length; n++) i[n] = this.mul(i[n - 1], e);
var r = i[0], a = 0, s = 0, c = t.bitLength() % 26;
0 === c && (c = 26);
for (n = t.length - 1; n >= 0; n--) {
for (var l = t.words[n], h = c - 1; h >= 0; h--) {
var u = l >> h & 1;
r !== i[0] && (r = this.sqr(r));
if (0 !== u || 0 !== a) {
a <<= 1;
a |= u;
if (4 == ++s || 0 === n && 0 === h) {
r = this.mul(r, i[a]);
s = 0;
a = 0;
}
} else s = 0;
}
c = 26;
}
return r;
};
k.prototype.convertTo = function(e) {
var t = e.umod(this.m);
return t === e ? t.clone() : t;
};
k.prototype.convertFrom = function(e) {
var t = e.clone();
t.red = null;
return t;
};
o.mont = function(e) {
return new T(e);
};
function T(e) {
k.call(this, e);
this.shift = this.m.bitLength();
this.shift % 26 != 0 && (this.shift += 26 - this.shift % 26);
this.r = new o(1).iushln(this.shift);
this.r2 = this.imod(this.r.sqr());
this.rinv = this.r._invmp(this.m);
this.minv = this.rinv.mul(this.r).isubn(1).div(this.m);
this.minv = this.minv.umod(this.r);
this.minv = this.r.sub(this.minv);
}
r(T, k);
T.prototype.convertTo = function(e) {
return this.imod(e.ushln(this.shift));
};
T.prototype.convertFrom = function(e) {
var t = this.imod(e.mul(this.rinv));
t.red = null;
return t;
};
T.prototype.imul = function(e, t) {
if (e.isZero() || t.isZero()) {
e.words[0] = 0;
e.length = 1;
return e;
}
var i = e.imul(t), n = i.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m), r = i.isub(n).iushrn(this.shift), o = r;
r.cmp(this.m) >= 0 ? o = r.isub(this.m) : r.cmpn(0) < 0 && (o = r.iadd(this.m));
return o._forceRed(this);
};
T.prototype.mul = function(e, t) {
if (e.isZero() || t.isZero()) return new o(0)._forceRed(this);
var i = e.mul(t), n = i.maskn(this.shift).mul(this.minv).imaskn(this.shift).mul(this.m), r = i.isub(n).iushrn(this.shift), a = r;
r.cmp(this.m) >= 0 ? a = r.isub(this.m) : r.cmpn(0) < 0 && (a = r.iadd(this.m));
return a._forceRed(this);
};
T.prototype.invm = function(e) {
return this.imod(e._invmp(this.m).mul(this.r2))._forceRed(this);
};
})("undefined" == typeof t || t, this);
}, {
buffer: 19
} ],
18: [ function(e, t) {
var i;
t.exports = function(e) {
i || (i = new n(null));
return i.generate(e);
};
function n(e) {
this.rand = e;
}
t.exports.Rand = n;
n.prototype.generate = function(e) {
return this._rand(e);
};
n.prototype._rand = function(e) {
if (this.rand.getBytes) return this.rand.getBytes(e);
for (var t = new Uint8Array(e), i = 0; i < t.length; i++) t[i] = this.rand.getByte();
return t;
};
if ("object" == typeof self) self.crypto && self.crypto.getRandomValues ? n.prototype._rand = function(e) {
var t = new Uint8Array(e);
self.crypto.getRandomValues(t);
return t;
} : self.msCrypto && self.msCrypto.getRandomValues ? n.prototype._rand = function(e) {
var t = new Uint8Array(e);
self.msCrypto.getRandomValues(t);
return t;
} : "object" == typeof window && (n.prototype._rand = function() {
throw new Error("Not implemented yet");
}); else try {
var r = e("crypto");
if ("function" != typeof r.randomBytes) throw new Error("Not supported");
n.prototype._rand = function(e) {
return r.randomBytes(e);
};
} catch (e) {}
}, {
crypto: 19
} ],
19: [ function() {}, {} ],
20: [ function(e, t) {
var i = e("safe-buffer").Buffer;
function n(e) {
i.isBuffer(e) || (e = i.from(e));
for (var t = e.length / 4 | 0, n = new Array(t), r = 0; r < t; r++) n[r] = e.readUInt32BE(4 * r);
return n;
}
function r(e) {
for (;0 < e.length; e++) e[0] = 0;
}
function o(e, t, i, n, r) {
for (var o, a, s, c, l = i[0], h = i[1], u = i[2], f = i[3], d = e[0] ^ t[0], p = e[1] ^ t[1], m = e[2] ^ t[2], g = e[3] ^ t[3], y = 4, b = 1; b < r; b++) {
o = l[d >>> 24] ^ h[p >>> 16 & 255] ^ u[m >>> 8 & 255] ^ f[255 & g] ^ t[y++];
a = l[p >>> 24] ^ h[m >>> 16 & 255] ^ u[g >>> 8 & 255] ^ f[255 & d] ^ t[y++];
s = l[m >>> 24] ^ h[g >>> 16 & 255] ^ u[d >>> 8 & 255] ^ f[255 & p] ^ t[y++];
c = l[g >>> 24] ^ h[d >>> 16 & 255] ^ u[p >>> 8 & 255] ^ f[255 & m] ^ t[y++];
d = o;
p = a;
m = s;
g = c;
}
o = (n[d >>> 24] << 24 | n[p >>> 16 & 255] << 16 | n[m >>> 8 & 255] << 8 | n[255 & g]) ^ t[y++];
a = (n[p >>> 24] << 24 | n[m >>> 16 & 255] << 16 | n[g >>> 8 & 255] << 8 | n[255 & d]) ^ t[y++];
s = (n[m >>> 24] << 24 | n[g >>> 16 & 255] << 16 | n[d >>> 8 & 255] << 8 | n[255 & p]) ^ t[y++];
c = (n[g >>> 24] << 24 | n[d >>> 16 & 255] << 16 | n[p >>> 8 & 255] << 8 | n[255 & m]) ^ t[y++];
return [ o >>>= 0, a >>>= 0, s >>>= 0, c >>>= 0 ];
}
var a = [ 0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54 ], s = function() {
for (var e = new Array(256), t = 0; t < 256; t++) e[t] = t < 128 ? t << 1 : t << 1 ^ 283;
for (var i = [], n = [], r = [ [], [], [], [] ], o = [ [], [], [], [] ], a = 0, s = 0, c = 0; c < 256; ++c) {
var l = s ^ s << 1 ^ s << 2 ^ s << 3 ^ s << 4;
l = l >>> 8 ^ 255 & l ^ 99;
i[a] = l;
n[l] = a;
var h = e[a], u = e[h], f = e[u], d = 257 * e[l] ^ 16843008 * l;
r[0][a] = d << 24 | d >>> 8;
r[1][a] = d << 16 | d >>> 16;
r[2][a] = d << 8 | d >>> 24;
r[3][a] = d;
d = 16843009 * f ^ 65537 * u ^ 257 * h ^ 16843008 * a;
o[0][l] = d << 24 | d >>> 8;
o[1][l] = d << 16 | d >>> 16;
o[2][l] = d << 8 | d >>> 24;
o[3][l] = d;
if (0 === a) a = s = 1; else {
a = h ^ e[e[e[f ^ h]]];
s ^= e[e[s]];
}
}
return {
SBOX: i,
INV_SBOX: n,
SUB_MIX: r,
INV_SUB_MIX: o
};
}();
function c(e) {
this._key = n(e);
this._reset();
}
c.blockSize = 16;
c.keySize = 32;
c.prototype.blockSize = c.blockSize;
c.prototype.keySize = c.keySize;
c.prototype._reset = function() {
for (var e = this._key, t = e.length, i = t + 6, n = 4 * (i + 1), r = [], o = 0; o < t; o++) r[o] = e[o];
for (o = t; o < n; o++) {
var c = r[o - 1];
if (o % t == 0) {
c = c << 8 | c >>> 24;
c = s.SBOX[c >>> 24] << 24 | s.SBOX[c >>> 16 & 255] << 16 | s.SBOX[c >>> 8 & 255] << 8 | s.SBOX[255 & c];
c ^= a[o / t | 0] << 24;
} else t > 6 && o % t == 4 && (c = s.SBOX[c >>> 24] << 24 | s.SBOX[c >>> 16 & 255] << 16 | s.SBOX[c >>> 8 & 255] << 8 | s.SBOX[255 & c]);
r[o] = r[o - t] ^ c;
}
for (var l = [], h = 0; h < n; h++) {
var u = n - h, f = r[u - (h % 4 ? 0 : 4)];
l[h] = h < 4 || u <= 4 ? f : s.INV_SUB_MIX[0][s.SBOX[f >>> 24]] ^ s.INV_SUB_MIX[1][s.SBOX[f >>> 16 & 255]] ^ s.INV_SUB_MIX[2][s.SBOX[f >>> 8 & 255]] ^ s.INV_SUB_MIX[3][s.SBOX[255 & f]];
}
this._nRounds = i;
this._keySchedule = r;
this._invKeySchedule = l;
};
c.prototype.encryptBlockRaw = function(e) {
return o(e = n(e), this._keySchedule, s.SUB_MIX, s.SBOX, this._nRounds);
};
c.prototype.encryptBlock = function(e) {
var t = this.encryptBlockRaw(e), n = i.allocUnsafe(16);
n.writeUInt32BE(t[0], 0);
n.writeUInt32BE(t[1], 4);
n.writeUInt32BE(t[2], 8);
n.writeUInt32BE(t[3], 12);
return n;
};
c.prototype.decryptBlock = function(e) {
var t = (e = n(e))[1];
e[1] = e[3];
e[3] = t;
var r = o(e, this._invKeySchedule, s.INV_SUB_MIX, s.INV_SBOX, this._nRounds), a = i.allocUnsafe(16);
a.writeUInt32BE(r[0], 0);
a.writeUInt32BE(r[3], 4);
a.writeUInt32BE(r[2], 8);
a.writeUInt32BE(r[1], 12);
return a;
};
c.prototype.scrub = function() {
r(this._keySchedule);
r(this._invKeySchedule);
r(this._key);
};
t.exports.AES = c;
}, {
"safe-buffer": 182
} ],
21: [ function(e, t) {
var i = e("./aes"), n = e("safe-buffer").Buffer, r = e("cipher-base"), o = e("inherits"), a = e("./ghash"), s = e("buffer-xor"), c = e("./incr32");
function l(e, t) {
var i = 0;
e.length !== t.length && i++;
for (var n = Math.min(e.length, t.length), r = 0; r < n; ++r) i += e[r] ^ t[r];
return i;
}
function h(e, t, i) {
if (12 === t.length) {
e._finID = n.concat([ t, n.from([ 0, 0, 0, 1 ]) ]);
return n.concat([ t, n.from([ 0, 0, 0, 2 ]) ]);
}
var r = new a(i), o = t.length, s = o % 16;
r.update(t);
if (s) {
s = 16 - s;
r.update(n.alloc(s, 0));
}
r.update(n.alloc(8, 0));
var l = 8 * o, h = n.alloc(8);
h.writeUIntBE(l, 0, 8);
r.update(h);
e._finID = r.state;
var u = n.from(e._finID);
c(u);
return u;
}
function u(e, t, o, s) {
r.call(this);
var c = n.alloc(4, 0);
this._cipher = new i.AES(t);
var l = this._cipher.encryptBlock(c);
this._ghash = new a(l);
o = h(this, o, l);
this._prev = n.from(o);
this._cache = n.allocUnsafe(0);
this._secCache = n.allocUnsafe(0);
this._decrypt = s;
this._alen = 0;
this._len = 0;
this._mode = e;
this._authTag = null;
this._called = !1;
}
o(u, r);
u.prototype._update = function(e) {
if (!this._called && this._alen) {
var t = 16 - this._alen % 16;
if (t < 16) {
t = n.alloc(t, 0);
this._ghash.update(t);
}
}
this._called = !0;
var i = this._mode.encrypt(this, e);
this._decrypt ? this._ghash.update(e) : this._ghash.update(i);
this._len += e.length;
return i;
};
u.prototype._final = function() {
if (this._decrypt && !this._authTag) throw new Error("Unsupported state or unable to authenticate data");
var e = s(this._ghash.final(8 * this._alen, 8 * this._len), this._cipher.encryptBlock(this._finID));
if (this._decrypt && l(e, this._authTag)) throw new Error("Unsupported state or unable to authenticate data");
this._authTag = e;
this._cipher.scrub();
};
u.prototype.getAuthTag = function() {
if (this._decrypt || !n.isBuffer(this._authTag)) throw new Error("Attempting to get auth tag in unsupported state");
return this._authTag;
};
u.prototype.setAuthTag = function(e) {
if (!this._decrypt) throw new Error("Attempting to set auth tag in unsupported state");
this._authTag = e;
};
u.prototype.setAAD = function(e) {
if (this._called) throw new Error("Attempting to set AAD in unsupported state");
this._ghash.update(e);
this._alen += e.length;
};
t.exports = u;
}, {
"./aes": 20,
"./ghash": 25,
"./incr32": 26,
"buffer-xor": 64,
"cipher-base": 67,
inherits: 138,
"safe-buffer": 182
} ],
22: [ function(e, t, i) {
var n = e("./encrypter"), r = e("./decrypter"), o = e("./modes/list.json");
i.createCipher = i.Cipher = n.createCipher;
i.createCipheriv = i.Cipheriv = n.createCipheriv;
i.createDecipher = i.Decipher = r.createDecipher;
i.createDecipheriv = i.Decipheriv = r.createDecipheriv;
i.listCiphers = i.getCiphers = function() {
return Object.keys(o);
};
}, {
"./decrypter": 23,
"./encrypter": 24,
"./modes/list.json": 34
} ],
23: [ function(e, t, i) {
var n = e("./authCipher"), r = e("safe-buffer").Buffer, o = e("./modes"), a = e("./streamCipher"), s = e("cipher-base"), c = e("./aes"), l = e("evp_bytestokey");
function h(e, t, i) {
s.call(this);
this._cache = new u();
this._last = void 0;
this._cipher = new c.AES(t);
this._prev = r.from(i);
this._mode = e;
this._autopadding = !0;
}
e("inherits")(h, s);
h.prototype._update = function(e) {
this._cache.add(e);
for (var t, i, n = []; t = this._cache.get(this._autopadding); ) {
i = this._mode.decrypt(this, t);
n.push(i);
}
return r.concat(n);
};
h.prototype._final = function() {
var e = this._cache.flush();
if (this._autopadding) return f(this._mode.decrypt(this, e));
if (e) throw new Error("data not multiple of block length");
};
h.prototype.setAutoPadding = function(e) {
this._autopadding = !!e;
return this;
};
function u() {
this.cache = r.allocUnsafe(0);
}
u.prototype.add = function(e) {
this.cache = r.concat([ this.cache, e ]);
};
u.prototype.get = function(e) {
var t;
if (e) {
if (this.cache.length > 16) {
t = this.cache.slice(0, 16);
this.cache = this.cache.slice(16);
return t;
}
} else if (this.cache.length >= 16) {
t = this.cache.slice(0, 16);
this.cache = this.cache.slice(16);
return t;
}
return null;
};
u.prototype.flush = function() {
if (this.cache.length) return this.cache;
};
function f(e) {
var t = e[15];
if (t < 1 || t > 16) throw new Error("unable to decrypt data");
for (var i = -1; ++i < t; ) if (e[i + (16 - t)] !== t) throw new Error("unable to decrypt data");
if (16 !== t) return e.slice(0, 16 - t);
}
function d(e, t, i) {
var s = o[e.toLowerCase()];
if (!s) throw new TypeError("invalid suite type");
"string" == typeof i && (i = r.from(i));
if ("GCM" !== s.mode && i.length !== s.iv) throw new TypeError("invalid iv length " + i.length);
"string" == typeof t && (t = r.from(t));
if (t.length !== s.key / 8) throw new TypeError("invalid key length " + t.length);
return "stream" === s.type ? new a(s.module, t, i, !0) : "auth" === s.type ? new n(s.module, t, i, !0) : new h(s.module, t, i);
}
i.createDecipher = function(e, t) {
var i = o[e.toLowerCase()];
if (!i) throw new TypeError("invalid suite type");
var n = l(t, !1, i.key, i.iv);
return d(e, n.key, n.iv);
};
i.createDecipheriv = d;
}, {
"./aes": 20,
"./authCipher": 21,
"./modes": 33,
"./streamCipher": 36,
"cipher-base": 67,
evp_bytestokey: 105,
inherits: 138,
"safe-buffer": 182
} ],
24: [ function(e, t, i) {
var n = e("./modes"), r = e("./authCipher"), o = e("safe-buffer").Buffer, a = e("./streamCipher"), s = e("cipher-base"), c = e("./aes"), l = e("evp_bytestokey");
function h(e, t, i) {
s.call(this);
this._cache = new f();
this._cipher = new c.AES(t);
this._prev = o.from(i);
this._mode = e;
this._autopadding = !0;
}
e("inherits")(h, s);
h.prototype._update = function(e) {
this._cache.add(e);
for (var t, i, n = []; t = this._cache.get(); ) {
i = this._mode.encrypt(this, t);
n.push(i);
}
return o.concat(n);
};
var u = o.alloc(16, 16);
h.prototype._final = function() {
var e = this._cache.flush();
if (this._autopadding) {
e = this._mode.encrypt(this, e);
this._cipher.scrub();
return e;
}
if (!e.equals(u)) {
this._cipher.scrub();
throw new Error("data not multiple of block length");
}
};
h.prototype.setAutoPadding = function(e) {
this._autopadding = !!e;
return this;
};
function f() {
this.cache = o.allocUnsafe(0);
}
f.prototype.add = function(e) {
this.cache = o.concat([ this.cache, e ]);
};
f.prototype.get = function() {
if (this.cache.length > 15) {
var e = this.cache.slice(0, 16);
this.cache = this.cache.slice(16);
return e;
}
return null;
};
f.prototype.flush = function() {
for (var e = 16 - this.cache.length, t = o.allocUnsafe(e), i = -1; ++i < e; ) t.writeUInt8(e, i);
return o.concat([ this.cache, t ]);
};
function d(e, t, i) {
var s = n[e.toLowerCase()];
if (!s) throw new TypeError("invalid suite type");
"string" == typeof t && (t = o.from(t));
if (t.length !== s.key / 8) throw new TypeError("invalid key length " + t.length);
"string" == typeof i && (i = o.from(i));
if ("GCM" !== s.mode && i.length !== s.iv) throw new TypeError("invalid iv length " + i.length);
return "stream" === s.type ? new a(s.module, t, i) : "auth" === s.type ? new r(s.module, t, i) : new h(s.module, t, i);
}
i.createCipheriv = d;
i.createCipher = function(e, t) {
var i = n[e.toLowerCase()];
if (!i) throw new TypeError("invalid suite type");
var r = l(t, !1, i.key, i.iv);
return d(e, r.key, r.iv);
};
}, {
"./aes": 20,
"./authCipher": 21,
"./modes": 33,
"./streamCipher": 36,
"cipher-base": 67,
evp_bytestokey: 105,
inherits: 138,
"safe-buffer": 182
} ],
25: [ function(e, t) {
var i = e("safe-buffer").Buffer, n = i.alloc(16, 0);
function r(e) {
var t = i.allocUnsafe(16);
t.writeUInt32BE(e[0] >>> 0, 0);
t.writeUInt32BE(e[1] >>> 0, 4);
t.writeUInt32BE(e[2] >>> 0, 8);
t.writeUInt32BE(e[3] >>> 0, 12);
return t;
}
function o(e) {
this.h = e;
this.state = i.alloc(16, 0);
this.cache = i.allocUnsafe(0);
}
o.prototype.ghash = function(e) {
for (var t = -1; ++t < e.length; ) this.state[t] ^= e[t];
this._multiply();
};
o.prototype._multiply = function() {
for (var e, t, i, n = [ (e = this.h).readUInt32BE(0), e.readUInt32BE(4), e.readUInt32BE(8), e.readUInt32BE(12) ], o = [ 0, 0, 0, 0 ], a = -1; ++a < 128; ) {
if (0 != (this.state[~~(a / 8)] & 1 << 7 - a % 8)) {
o[0] ^= n[0];
o[1] ^= n[1];
o[2] ^= n[2];
o[3] ^= n[3];
}
i = 0 != (1 & n[3]);
for (t = 3; t > 0; t--) n[t] = n[t] >>> 1 | (1 & n[t - 1]) << 31;
n[0] = n[0] >>> 1;
i && (n[0] = n[0] ^ 225 << 24);
}
this.state = r(o);
};
o.prototype.update = function(e) {
this.cache = i.concat([ this.cache, e ]);
for (var t; this.cache.length >= 16; ) {
t = this.cache.slice(0, 16);
this.cache = this.cache.slice(16);
this.ghash(t);
}
};
o.prototype.final = function(e, t) {
this.cache.length && this.ghash(i.concat([ this.cache, n ], 16));
this.ghash(r([ 0, e, 0, t ]));
return this.state;
};
t.exports = o;
}, {
"safe-buffer": 182
} ],
26: [ function(e, t) {
t.exports = function(e) {
for (var t, i = e.length; i--; ) {
if (255 !== (t = e.readUInt8(i))) {
t++;
e.writeUInt8(t, i);
break;
}
e.writeUInt8(0, i);
}
};
}, {} ],
27: [ function(e, t, i) {
var n = e("buffer-xor");
i.encrypt = function(e, t) {
var i = n(t, e._prev);
e._prev = e._cipher.encryptBlock(i);
return e._prev;
};
i.decrypt = function(e, t) {
var i = e._prev;
e._prev = t;
var r = e._cipher.decryptBlock(t);
return n(r, i);
};
}, {
"buffer-xor": 64
} ],
28: [ function(e, t, i) {
var n = e("safe-buffer").Buffer, r = e("buffer-xor");
function o(e, t, i) {
var o = t.length, a = r(t, e._cache);
e._cache = e._cache.slice(o);
e._prev = n.concat([ e._prev, i ? t : a ]);
return a;
}
i.encrypt = function(e, t, i) {
for (var r, a = n.allocUnsafe(0); t.length; ) {
if (0 === e._cache.length) {
e._cache = e._cipher.encryptBlock(e._prev);
e._prev = n.allocUnsafe(0);
}
if (!(e._cache.length <= t.length)) {
a = n.concat([ a, o(e, t, i) ]);
break;
}
r = e._cache.length;
a = n.concat([ a, o(e, t.slice(0, r), i) ]);
t = t.slice(r);
}
return a;
};
}, {
"buffer-xor": 64,
"safe-buffer": 182
} ],
29: [ function(e, t, i) {
var n = e("safe-buffer").Buffer;
function r(e, t, i) {
for (var n, r, a = -1, s = 0; ++a < 8; ) {
n = t & 1 << 7 - a ? 128 : 0;
s += (128 & (r = e._cipher.encryptBlock(e._prev)[0] ^ n)) >> a % 8;
e._prev = o(e._prev, i ? n : r);
}
return s;
}
function o(e, t) {
var i = e.length, r = -1, o = n.allocUnsafe(e.length);
e = n.concat([ e, n.from([ t ]) ]);
for (;++r < i; ) o[r] = e[r] << 1 | e[r + 1] >> 7;
return o;
}
i.encrypt = function(e, t, i) {
for (var o = t.length, a = n.allocUnsafe(o), s = -1; ++s < o; ) a[s] = r(e, t[s], i);
return a;
};
}, {
"safe-buffer": 182
} ],
30: [ function(e, t, i) {
var n = e("safe-buffer").Buffer;
function r(e, t, i) {
var r = e._cipher.encryptBlock(e._prev)[0] ^ t;
e._prev = n.concat([ e._prev.slice(1), n.from([ i ? t : r ]) ]);
return r;
}
i.encrypt = function(e, t, i) {
for (var o = t.length, a = n.allocUnsafe(o), s = -1; ++s < o; ) a[s] = r(e, t[s], i);
return a;
};
}, {
"safe-buffer": 182
} ],
31: [ function(e, t, i) {
var n = e("buffer-xor"), r = e("safe-buffer").Buffer, o = e("../incr32");
function a(e) {
var t = e._cipher.encryptBlockRaw(e._prev);
o(e._prev);
return t;
}
i.encrypt = function(e, t) {
var i = Math.ceil(t.length / 16), o = e._cache.length;
e._cache = r.concat([ e._cache, r.allocUnsafe(16 * i) ]);
for (var s = 0; s < i; s++) {
var c = a(e), l = o + 16 * s;
e._cache.writeUInt32BE(c[0], l + 0);
e._cache.writeUInt32BE(c[1], l + 4);
e._cache.writeUInt32BE(c[2], l + 8);
e._cache.writeUInt32BE(c[3], l + 12);
}
var h = e._cache.slice(0, t.length);
e._cache = e._cache.slice(t.length);
return n(t, h);
};
}, {
"../incr32": 26,
"buffer-xor": 64,
"safe-buffer": 182
} ],
32: [ function(e, t, i) {
i.encrypt = function(e, t) {
return e._cipher.encryptBlock(t);
};
i.decrypt = function(e, t) {
return e._cipher.decryptBlock(t);
};
}, {} ],
33: [ function(e, t) {
var i = {
ECB: e("./ecb"),
CBC: e("./cbc"),
CFB: e("./cfb"),
CFB8: e("./cfb8"),
CFB1: e("./cfb1"),
OFB: e("./ofb"),
CTR: e("./ctr"),
GCM: e("./ctr")
}, n = e("./list.json");
for (var r in n) n[r].module = i[n[r].mode];
t.exports = n;
}, {
"./cbc": 27,
"./cfb": 28,
"./cfb1": 29,
"./cfb8": 30,
"./ctr": 31,
"./ecb": 32,
"./list.json": 34,
"./ofb": 35
} ],
34: [ function(e, t) {
t.exports = {
"aes-128-ecb": {
cipher: "AES",
key: 128,
iv: 0,
mode: "ECB",
type: "block"
},
"aes-192-ecb": {
cipher: "AES",
key: 192,
iv: 0,
mode: "ECB",
type: "block"
},
"aes-256-ecb": {
cipher: "AES",
key: 256,
iv: 0,
mode: "ECB",
type: "block"
},
"aes-128-cbc": {
cipher: "AES",
key: 128,
iv: 16,
mode: "CBC",
type: "block"
},
"aes-192-cbc": {
cipher: "AES",
key: 192,
iv: 16,
mode: "CBC",
type: "block"
},
"aes-256-cbc": {
cipher: "AES",
key: 256,
iv: 16,
mode: "CBC",
type: "block"
},
aes128: {
cipher: "AES",
key: 128,
iv: 16,
mode: "CBC",
type: "block"
},
aes192: {
cipher: "AES",
key: 192,
iv: 16,
mode: "CBC",
type: "block"
},
aes256: {
cipher: "AES",
key: 256,
iv: 16,
mode: "CBC",
type: "block"
},
"aes-128-cfb": {
cipher: "AES",
key: 128,
iv: 16,
mode: "CFB",
type: "stream"
},
"aes-192-cfb": {
cipher: "AES",
key: 192,
iv: 16,
mode: "CFB",
type: "stream"
},
"aes-256-cfb": {
cipher: "AES",
key: 256,
iv: 16,
mode: "CFB",
type: "stream"
},
"aes-128-cfb8": {
cipher: "AES",
key: 128,
iv: 16,
mode: "CFB8",
type: "stream"
},
"aes-192-cfb8": {
cipher: "AES",
key: 192,
iv: 16,
mode: "CFB8",
type: "stream"
},
"aes-256-cfb8": {
cipher: "AES",
key: 256,
iv: 16,
mode: "CFB8",
type: "stream"
},
"aes-128-cfb1": {
cipher: "AES",
key: 128,
iv: 16,
mode: "CFB1",
type: "stream"
},
"aes-192-cfb1": {
cipher: "AES",
key: 192,
iv: 16,
mode: "CFB1",
type: "stream"
},
"aes-256-cfb1": {
cipher: "AES",
key: 256,
iv: 16,
mode: "CFB1",
type: "stream"
},
"aes-128-ofb": {
cipher: "AES",
key: 128,
iv: 16,
mode: "OFB",
type: "stream"
},
"aes-192-ofb": {
cipher: "AES",
key: 192,
iv: 16,
mode: "OFB",
type: "stream"
},
"aes-256-ofb": {
cipher: "AES",
key: 256,
iv: 16,
mode: "OFB",
type: "stream"
},
"aes-128-ctr": {
cipher: "AES",
key: 128,
iv: 16,
mode: "CTR",
type: "stream"
},
"aes-192-ctr": {
cipher: "AES",
key: 192,
iv: 16,
mode: "CTR",
type: "stream"
},
"aes-256-ctr": {
cipher: "AES",
key: 256,
iv: 16,
mode: "CTR",
type: "stream"
},
"aes-128-gcm": {
cipher: "AES",
key: 128,
iv: 12,
mode: "GCM",
type: "auth"
},
"aes-192-gcm": {
cipher: "AES",
key: 192,
iv: 12,
mode: "GCM",
type: "auth"
},
"aes-256-gcm": {
cipher: "AES",
key: 256,
iv: 12,
mode: "GCM",
type: "auth"
}
};
}, {} ],
35: [ function(e, t, i) {
(function(t) {
var n = e("buffer-xor");
function r(e) {
e._prev = e._cipher.encryptBlock(e._prev);
return e._prev;
}
i.encrypt = function(e, i) {
for (;e._cache.length < i.length; ) e._cache = t.concat([ e._cache, r(e) ]);
var o = e._cache.slice(0, i.length);
e._cache = e._cache.slice(i.length);
return n(i, o);
};
}).call(this, e("buffer").Buffer);
}, {
buffer: 65,
"buffer-xor": 64
} ],
36: [ function(e, t) {
var i = e("./aes"), n = e("safe-buffer").Buffer, r = e("cipher-base");
function o(e, t, o, a) {
r.call(this);
this._cipher = new i.AES(t);
this._prev = n.from(o);
this._cache = n.allocUnsafe(0);
this._secCache = n.allocUnsafe(0);
this._decrypt = a;
this._mode = e;
}
e("inherits")(o, r);
o.prototype._update = function(e) {
return this._mode.encrypt(this, e, this._decrypt);
};
o.prototype._final = function() {
this._cipher.scrub();
};
t.exports = o;
}, {
"./aes": 20,
"cipher-base": 67,
inherits: 138,
"safe-buffer": 182
} ],
37: [ function(e, t, i) {
var n = e("browserify-des"), r = e("browserify-aes/browser"), o = e("browserify-aes/modes"), a = e("browserify-des/modes"), s = e("evp_bytestokey");
function c(e, t, i) {
e = e.toLowerCase();
if (o[e]) return r.createCipheriv(e, t, i);
if (a[e]) return new n({
key: t,
iv: i,
mode: e
});
throw new TypeError("invalid suite type");
}
function l(e, t, i) {
e = e.toLowerCase();
if (o[e]) return r.createDecipheriv(e, t, i);
if (a[e]) return new n({
key: t,
iv: i,
mode: e,
decrypt: !0
});
throw new TypeError("invalid suite type");
}
i.createCipher = i.Cipher = function(e, t) {
e = e.toLowerCase();
var i, n;
if (o[e]) {
i = o[e].key;
n = o[e].iv;
} else {
if (!a[e]) throw new TypeError("invalid suite type");
i = 8 * a[e].key;
n = a[e].iv;
}
var r = s(t, !1, i, n);
return c(e, r.key, r.iv);
};
i.createCipheriv = i.Cipheriv = c;
i.createDecipher = i.Decipher = function(e, t) {
e = e.toLowerCase();
var i, n;
if (o[e]) {
i = o[e].key;
n = o[e].iv;
} else {
if (!a[e]) throw new TypeError("invalid suite type");
i = 8 * a[e].key;
n = a[e].iv;
}
var r = s(t, !1, i, n);
return l(e, r.key, r.iv);
};
i.createDecipheriv = i.Decipheriv = l;
i.listCiphers = i.getCiphers = function() {
return Object.keys(a).concat(r.getCiphers());
};
}, {
"browserify-aes/browser": 22,
"browserify-aes/modes": 33,
"browserify-des": 38,
"browserify-des/modes": 39,
evp_bytestokey: 105
} ],
38: [ function(e, t) {
var i = e("cipher-base"), n = e("des.js"), r = e("inherits"), o = e("safe-buffer").Buffer, a = {
"des-ede3-cbc": n.CBC.instantiate(n.EDE),
"des-ede3": n.EDE,
"des-ede-cbc": n.CBC.instantiate(n.EDE),
"des-ede": n.EDE,
"des-cbc": n.CBC.instantiate(n.DES),
"des-ecb": n.DES
};
a.des = a["des-cbc"];
a.des3 = a["des-ede3-cbc"];
t.exports = s;
r(s, i);
function s(e) {
i.call(this);
var t, n = e.mode.toLowerCase(), r = a[n];
t = e.decrypt ? "decrypt" : "encrypt";
var s = e.key;
o.isBuffer(s) || (s = o.from(s));
"des-ede" !== n && "des-ede-cbc" !== n || (s = o.concat([ s, s.slice(0, 8) ]));
var c = e.iv;
o.isBuffer(c) || (c = o.from(c));
this._des = r.create({
key: s,
iv: c,
type: t
});
}
s.prototype._update = function(e) {
return o.from(this._des.update(e));
};
s.prototype._final = function() {
return o.from(this._des.final());
};
}, {
"cipher-base": 67,
"des.js": 76,
inherits: 138,
"safe-buffer": 182
} ],
39: [ function(e, t, i) {
i["des-ecb"] = {
key: 8,
iv: 0
};
i["des-cbc"] = i.des = {
key: 8,
iv: 8
};
i["des-ede3-cbc"] = i.des3 = {
key: 24,
iv: 8
};
i["des-ede3"] = {
key: 24,
iv: 0
};
i["des-ede-cbc"] = {
key: 16,
iv: 8
};
i["des-ede"] = {
key: 16,
iv: 0
};
}, {} ],
40: [ function(e, t) {
(function(i) {
var n = e("bn.js"), r = e("randombytes");
function o(e) {
var t = a(e);
return {
blinder: t.toRed(n.mont(e.modulus)).redPow(new n(e.publicExponent)).fromRed(),
unblinder: t.invm(e.modulus)
};
}
function a(e) {
var t, i = e.modulus.byteLength();
do {
t = new n(r(i));
} while (t.cmp(e.modulus) >= 0 || !t.umod(e.prime1) || !t.umod(e.prime2));
return t;
}
function s(e, t) {
var r = o(t), a = t.modulus.byteLength(), s = new n(e).mul(r.blinder).umod(t.modulus), c = s.toRed(n.mont(t.prime1)), l = s.toRed(n.mont(t.prime2)), h = t.coefficient, u = t.prime1, f = t.prime2, d = c.redPow(t.exponent1).fromRed(), p = l.redPow(t.exponent2).fromRed(), m = d.isub(p).imul(h).umod(u).imul(f);
return p.iadd(m).imul(r.unblinder).umod(t.modulus).toArrayLike(i, "be", a);
}
s.getr = a;
t.exports = s;
}).call(this, e("buffer").Buffer);
}, {
"bn.js": 17,
buffer: 65,
randombytes: 164
} ],
41: [ function(e, t) {
t.exports = e("./browser/algorithms.json");
}, {
"./browser/algorithms.json": 42
} ],
42: [ function(e, t) {
t.exports = {
sha224WithRSAEncryption: {
sign: "rsa",
hash: "sha224",
id: "302d300d06096086480165030402040500041c"
},
"RSA-SHA224": {
sign: "ecdsa/rsa",
hash: "sha224",
id: "302d300d06096086480165030402040500041c"
},
sha256WithRSAEncryption: {
sign: "rsa",
hash: "sha256",
id: "3031300d060960864801650304020105000420"
},
"RSA-SHA256": {
sign: "ecdsa/rsa",
hash: "sha256",
id: "3031300d060960864801650304020105000420"
},
sha384WithRSAEncryption: {
sign: "rsa",
hash: "sha384",
id: "3041300d060960864801650304020205000430"
},
"RSA-SHA384": {
sign: "ecdsa/rsa",
hash: "sha384",
id: "3041300d060960864801650304020205000430"
},
sha512WithRSAEncryption: {
sign: "rsa",
hash: "sha512",
id: "3051300d060960864801650304020305000440"
},
"RSA-SHA512": {
sign: "ecdsa/rsa",
hash: "sha512",
id: "3051300d060960864801650304020305000440"
},
"RSA-SHA1": {
sign: "rsa",
hash: "sha1",
id: "3021300906052b0e03021a05000414"
},
"ecdsa-with-SHA1": {
sign: "ecdsa",
hash: "sha1",
id: ""
},
sha256: {
sign: "ecdsa",
hash: "sha256",
id: ""
},
sha224: {
sign: "ecdsa",
hash: "sha224",
id: ""
},
sha384: {
sign: "ecdsa",
hash: "sha384",
id: ""
},
sha512: {
sign: "ecdsa",
hash: "sha512",
id: ""
},
"DSA-SHA": {
sign: "dsa",
hash: "sha1",
id: ""
},
"DSA-SHA1": {
sign: "dsa",
hash: "sha1",
id: ""
},
DSA: {
sign: "dsa",
hash: "sha1",
id: ""
},
"DSA-WITH-SHA224": {
sign: "dsa",
hash: "sha224",
id: ""
},
"DSA-SHA224": {
sign: "dsa",
hash: "sha224",
id: ""
},
"DSA-WITH-SHA256": {
sign: "dsa",
hash: "sha256",
id: ""
},
"DSA-SHA256": {
sign: "dsa",
hash: "sha256",
id: ""
},
"DSA-WITH-SHA384": {
sign: "dsa",
hash: "sha384",
id: ""
},
"DSA-SHA384": {
sign: "dsa",
hash: "sha384",
id: ""
},
"DSA-WITH-SHA512": {
sign: "dsa",
hash: "sha512",
id: ""
},
"DSA-SHA512": {
sign: "dsa",
hash: "sha512",
id: ""
},
"DSA-RIPEMD160": {
sign: "dsa",
hash: "rmd160",
id: ""
},
ripemd160WithRSA: {
sign: "rsa",
hash: "rmd160",
id: "3021300906052b2403020105000414"
},
"RSA-RIPEMD160": {
sign: "rsa",
hash: "rmd160",
id: "3021300906052b2403020105000414"
},
md5WithRSAEncryption: {
sign: "rsa",
hash: "md5",
id: "3020300c06082a864886f70d020505000410"
},
"RSA-MD5": {
sign: "rsa",
hash: "md5",
id: "3020300c06082a864886f70d020505000410"
}
};
}, {} ],
43: [ function(e, t) {
t.exports = {
"1.3.132.0.10": "secp256k1",
"1.3.132.0.33": "p224",
"1.2.840.10045.3.1.1": "p192",
"1.2.840.10045.3.1.7": "p256",
"1.3.132.0.34": "p384",
"1.3.132.0.35": "p521"
};
}, {} ],
44: [ function(e, t) {
var i = e("safe-buffer").Buffer, n = e("create-hash"), r = e("readable-stream"), o = e("inherits"), a = e("./sign"), s = e("./verify"), c = e("./algorithms.json");
Object.keys(c).forEach(function(e) {
c[e].id = i.from(c[e].id, "hex");
c[e.toLowerCase()] = c[e];
});
function l(e) {
r.Writable.call(this);
var t = c[e];
if (!t) throw new Error("Unknown message digest");
this._hashType = t.hash;
this._hash = n(t.hash);
this._tag = t.id;
this._signType = t.sign;
}
o(l, r.Writable);
l.prototype._write = function(e, t, i) {
this._hash.update(e);
i();
};
l.prototype.update = function(e, t) {
"string" == typeof e && (e = i.from(e, t));
this._hash.update(e);
return this;
};
l.prototype.sign = function(e, t) {
this.end();
var i = this._hash.digest(), n = a(i, e, this._hashType, this._signType, this._tag);
return t ? n.toString(t) : n;
};
function h(e) {
r.Writable.call(this);
var t = c[e];
if (!t) throw new Error("Unknown message digest");
this._hash = n(t.hash);
this._tag = t.id;
this._signType = t.sign;
}
o(h, r.Writable);
h.prototype._write = function(e, t, i) {
this._hash.update(e);
i();
};
h.prototype.update = function(e, t) {
"string" == typeof e && (e = i.from(e, t));
this._hash.update(e);
return this;
};
h.prototype.verify = function(e, t, n) {
"string" == typeof t && (t = i.from(t, n));
this.end();
var r = this._hash.digest();
return s(t, r, e, this._signType, this._tag);
};
function u(e) {
return new l(e);
}
function f(e) {
return new h(e);
}
t.exports = {
Sign: u,
Verify: f,
createSign: u,
createVerify: f
};
}, {
"./algorithms.json": 42,
"./sign": 45,
"./verify": 46,
"create-hash": 71,
inherits: 138,
"readable-stream": 61,
"safe-buffer": 62
} ],
45: [ function(e, t) {
var i = e("safe-buffer").Buffer, n = e("create-hmac"), r = e("browserify-rsa"), o = e("elliptic").ec, a = e("bn.js"), s = e("parse-asn1"), c = e("./curves.json");
function l(e, t) {
var n = c[t.curve.join(".")];
if (!n) throw new Error("unknown curve " + t.curve.join("."));
var r = new o(n).keyFromPrivate(t.privateKey).sign(e);
return i.from(r.toDER());
}
function h(e, t, i) {
for (var n, r = t.params.priv_key, o = t.params.p, s = t.params.q, c = t.params.g, l = new a(0), h = d(e, s).mod(s), p = !1, y = f(r, s, e, i); !1 === p; ) {
l = g(c, n = m(s, y, i), o, s);
if (0 === (p = n.invm(s).imul(h.add(r.mul(l))).mod(s)).cmpn(0)) {
p = !1;
l = new a(0);
}
}
return u(l, p);
}
function u(e, t) {
e = e.toArray();
t = t.toArray();
128 & e[0] && (e = [ 0 ].concat(e));
128 & t[0] && (t = [ 0 ].concat(t));
var n = [ 48, e.length + t.length + 4, 2, e.length ];
n = n.concat(e, [ 2, t.length ], t);
return i.from(n);
}
function f(e, t, r, o) {
if ((e = i.from(e.toArray())).length < t.byteLength()) {
var a = i.alloc(t.byteLength() - e.length);
e = i.concat([ a, e ]);
}
var s = r.length, c = p(r, t), l = i.alloc(s);
l.fill(1);
var h = i.alloc(s);
h = n(o, h).update(l).update(i.from([ 0 ])).update(e).update(c).digest();
l = n(o, h).update(l).digest();
return {
k: h = n(o, h).update(l).update(i.from([ 1 ])).update(e).update(c).digest(),
v: l = n(o, h).update(l).digest()
};
}
function d(e, t) {
var i = new a(e), n = (e.length << 3) - t.bitLength();
n > 0 && i.ishrn(n);
return i;
}
function p(e, t) {
e = (e = d(e, t)).mod(t);
var n = i.from(e.toArray());
if (n.length < t.byteLength()) {
var r = i.alloc(t.byteLength() - n.length);
n = i.concat([ r, n ]);
}
return n;
}
function m(e, t, r) {
var o, a;
do {
o = i.alloc(0);
for (;8 * o.length < e.bitLength(); ) {
t.v = n(r, t.k).update(t.v).digest();
o = i.concat([ o, t.v ]);
}
a = d(o, e);
t.k = n(r, t.k).update(t.v).update(i.from([ 0 ])).digest();
t.v = n(r, t.k).update(t.v).digest();
} while (-1 !== a.cmp(e));
return a;
}
function g(e, t, i, n) {
return e.toRed(a.mont(i)).redPow(t).fromRed().mod(n);
}
t.exports = function(e, t, n, o, a) {
var c = s(t);
if (c.curve) {
if ("ecdsa" !== o && "ecdsa/rsa" !== o) throw new Error("wrong private key type");
return l(e, c);
}
if ("dsa" === c.type) {
if ("dsa" !== o) throw new Error("wrong private key type");
return h(e, c, n);
}
if ("rsa" !== o && "ecdsa/rsa" !== o) throw new Error("wrong private key type");
e = i.concat([ a, e ]);
for (var u = c.modulus.byteLength(), f = [ 0, 1 ]; e.length + f.length + 1 < u; ) f.push(255);
f.push(0);
for (var d = -1; ++d < e.length; ) f.push(e[d]);
return r(f, c);
};
t.exports.getKey = f;
t.exports.makeKey = m;
}, {
"./curves.json": 43,
"bn.js": 17,
"browserify-rsa": 40,
"create-hmac": 73,
elliptic: 87,
"parse-asn1": 148,
"safe-buffer": 62
} ],
46: [ function(e, t) {
var i = e("safe-buffer").Buffer, n = e("bn.js"), r = e("elliptic").ec, o = e("parse-asn1"), a = e("./curves.json");
function s(e, t, i) {
var n = a[i.data.algorithm.curve.join(".")];
if (!n) throw new Error("unknown curve " + i.data.algorithm.curve.join("."));
var o = new r(n), s = i.data.subjectPrivateKey.data;
return o.verify(t, e, s);
}
function c(e, t, i) {
var r = i.data.p, a = i.data.q, s = i.data.g, c = i.data.pub_key, h = o.signature.decode(e, "der"), u = h.s, f = h.r;
l(u, a);
l(f, a);
var d = n.mont(r), p = u.invm(a);
return 0 === s.toRed(d).redPow(new n(t).mul(p).mod(a)).fromRed().mul(c.toRed(d).redPow(f.mul(p).mod(a)).fromRed()).mod(r).mod(a).cmp(f);
}
function l(e, t) {
if (e.cmpn(0) <= 0) throw new Error("invalid sig");
if (e.cmp(t) >= t) throw new Error("invalid sig");
}
t.exports = function(e, t, r, a, l) {
var h = o(r);
if ("ec" === h.type) {
if ("ecdsa" !== a && "ecdsa/rsa" !== a) throw new Error("wrong public key type");
return s(e, t, h);
}
if ("dsa" === h.type) {
if ("dsa" !== a) throw new Error("wrong public key type");
return c(e, t, h);
}
if ("rsa" !== a && "ecdsa/rsa" !== a) throw new Error("wrong public key type");
t = i.concat([ l, t ]);
for (var u = h.modulus.byteLength(), f = [ 1 ], d = 0; t.length + f.length + 2 < u; ) {
f.push(255);
d++;
}
f.push(0);
for (var p = -1; ++p < t.length; ) f.push(t[p]);
f = i.from(f);
var m = n.mont(h.modulus);
e = (e = new n(e).toRed(m)).redPow(new n(h.publicExponent));
e = i.from(e.fromRed().toArray());
var g = d < 8 ? 1 : 0;
u = Math.min(e.length, f.length);
e.length !== f.length && (g = 1);
p = -1;
for (;++p < u; ) g |= e[p] ^ f[p];
return 0 === g;
};
}, {
"./curves.json": 43,
"bn.js": 17,
elliptic: 87,
"parse-asn1": 148,
"safe-buffer": 62
} ],
47: [ function(e, t) {
"use strict";
function i(e, t) {
e.prototype = Object.create(t.prototype);
e.prototype.constructor = e;
e.__proto__ = t;
}
var n = {};
function r(e, t, r) {
r || (r = Error);
function o(e, i, n) {
return "string" == typeof t ? t : t(e, i, n);
}
var a = function(e) {
i(t, e);
function t(t, i, n) {
return e.call(this, o(t, i, n)) || this;
}
return t;
}(r);
a.prototype.name = r.name;
a.prototype.code = e;
n[e] = a;
}
function o(e, t) {
if (Array.isArray(e)) {
var i = e.length;
e = e.map(function(e) {
return String(e);
});
return i > 2 ? "one of ".concat(t, " ").concat(e.slice(0, i - 1).join(", "), ", or ") + e[i - 1] : 2 === i ? "one of ".concat(t, " ").concat(e[0], " or ").concat(e[1]) : "of ".concat(t, " ").concat(e[0]);
}
return "of ".concat(t, " ").concat(String(e));
}
function a(e, t, i) {
(void 0 === i || i > e.length) && (i = e.length);
return e.substring(i - t.length, i) === t;
}
function s(e, t, i) {
"number" != typeof i && (i = 0);
return !(i + t.length > e.length) && -1 !== e.indexOf(t, i);
}
r("ERR_INVALID_OPT_VALUE", function(e, t) {
return 'The value "' + t + '" is invalid for option "' + e + '"';
}, TypeError);
r("ERR_INVALID_ARG_TYPE", function(e, t, i) {
var n, r;
if ("string" == typeof t && ("not ", "not " === t.substr(0, "not ".length))) {
n = "must not be";
t = t.replace(/^not /, "");
} else n = "must be";
if (a(e, " argument")) r = "The ".concat(e, " ").concat(n, " ").concat(o(t, "type")); else {
var c = s(e, ".") ? "property" : "argument";
r = 'The "'.concat(e, '" ').concat(c, " ").concat(n, " ").concat(o(t, "type"));
}
return r + ". Received type ".concat(typeof i);
}, TypeError);
r("ERR_STREAM_PUSH_AFTER_EOF", "stream.push() after EOF");
r("ERR_METHOD_NOT_IMPLEMENTED", function(e) {
return "The " + e + " method is not implemented";
});
r("ERR_STREAM_PREMATURE_CLOSE", "Premature close");
r("ERR_STREAM_DESTROYED", function(e) {
return "Cannot call " + e + " after a stream was destroyed";
});
r("ERR_MULTIPLE_CALLBACK", "Callback called multiple times");
r("ERR_STREAM_CANNOT_PIPE", "Cannot pipe, not readable");
r("ERR_STREAM_WRITE_AFTER_END", "write after end");
r("ERR_STREAM_NULL_VALUES", "May not write null values to stream", TypeError);
r("ERR_UNKNOWN_ENCODING", function(e) {
return "Unknown encoding: " + e;
}, TypeError);
r("ERR_STREAM_UNSHIFT_AFTER_END_EVENT", "stream.unshift() after end event");
t.exports.codes = n;
}, {} ],
48: [ function(e, t) {
(function(i) {
"use strict";
var n = Object.keys || function(e) {
var t = [];
for (var i in e) t.push(i);
return t;
};
t.exports = l;
var r = e("./_stream_readable"), o = e("./_stream_writable");
e("inherits")(l, r);
for (var a = n(o.prototype), s = 0; s < a.length; s++) {
var c = a[s];
l.prototype[c] || (l.prototype[c] = o.prototype[c]);
}
function l(e) {
if (!(this instanceof l)) return new l(e);
r.call(this, e);
o.call(this, e);
this.allowHalfOpen = !0;
if (e) {
!1 === e.readable && (this.readable = !1);
!1 === e.writable && (this.writable = !1);
if (!1 === e.allowHalfOpen) {
this.allowHalfOpen = !1;
this.once("end", h);
}
}
}
Object.defineProperty(l.prototype, "writableHighWaterMark", {
enumerable: !1,
get: function() {
return this._writableState.highWaterMark;
}
});
Object.defineProperty(l.prototype, "writableBuffer", {
enumerable: !1,
get: function() {
return this._writableState && this._writableState.getBuffer();
}
});
Object.defineProperty(l.prototype, "writableLength", {
enumerable: !1,
get: function() {
return this._writableState.length;
}
});
function h() {
this._writableState.ended || i.nextTick(u, this);
}
function u(e) {
e.end();
}
Object.defineProperty(l.prototype, "destroyed", {
enumerable: !1,
get: function() {
return void 0 !== this._readableState && void 0 !== this._writableState && this._readableState.destroyed && this._writableState.destroyed;
},
set: function(e) {
if (void 0 !== this._readableState && void 0 !== this._writableState) {
this._readableState.destroyed = e;
this._writableState.destroyed = e;
}
}
});
}).call(this, e("_process"));
}, {
"./_stream_readable": 50,
"./_stream_writable": 52,
_process: 156,
inherits: 138
} ],
49: [ function(e, t) {
"use strict";
t.exports = n;
var i = e("./_stream_transform");
e("inherits")(n, i);
function n(e) {
if (!(this instanceof n)) return new n(e);
i.call(this, e);
}
n.prototype._transform = function(e, t, i) {
i(null, e);
};
}, {
"./_stream_transform": 51,
inherits: 138
} ],
50: [ function(e, t) {
(function(i, n) {
"use strict";
t.exports = x;
var r;
x.ReadableState = B;
e("events").EventEmitter;
var o = function(e, t) {
return e.listeners(t).length;
}, a = e("./internal/streams/stream"), s = e("buffer").Buffer, c = n.Uint8Array || function() {};
function l(e) {
return s.from(e);
}
var h, u = e("util");
h = u && u.debuglog ? u.debuglog("stream") : function() {};
var f, d, p, m = e("./internal/streams/buffer_list"), g = e("./internal/streams/destroy"), y = e("./internal/streams/state").getHighWaterMark, b = e("../errors").codes, v = b.ERR_INVALID_ARG_TYPE, _ = b.ERR_STREAM_PUSH_AFTER_EOF, w = b.ERR_METHOD_NOT_IMPLEMENTED, C = b.ERR_STREAM_UNSHIFT_AFTER_END_EVENT;
e("inherits")(x, a);
var S = g.errorOrDestroy, A = [ "error", "close", "destroy", "pause", "resume" ];
function M(e, t, i) {
if ("function" == typeof e.prependListener) return e.prependListener(t, i);
e._events && e._events[t] ? Array.isArray(e._events[t]) ? e._events[t].unshift(i) : e._events[t] = [ i, e._events[t] ] : e.on(t, i);
}
function B(t, i, n) {
r = r || e("./_stream_duplex");
t = t || {};
"boolean" != typeof n && (n = i instanceof r);
this.objectMode = !!t.objectMode;
n && (this.objectMode = this.objectMode || !!t.readableObjectMode);
this.highWaterMark = y(this, t, "readableHighWaterMark", n);
this.buffer = new m();
this.length = 0;
this.pipes = null;
this.pipesCount = 0;
this.flowing = null;
this.ended = !1;
this.endEmitted = !1;
this.reading = !1;
this.sync = !0;
this.needReadable = !1;
this.emittedReadable = !1;
this.readableListening = !1;
this.resumeScheduled = !1;
this.paused = !0;
this.emitClose = !1 !== t.emitClose;
this.autoDestroy = !!t.autoDestroy;
this.destroyed = !1;
this.defaultEncoding = t.defaultEncoding || "utf8";
this.awaitDrain = 0;
this.readingMore = !1;
this.decoder = null;
this.encoding = null;
if (t.encoding) {
f || (f = e("string_decoder/").StringDecoder);
this.decoder = new f(t.encoding);
this.encoding = t.encoding;
}
}
function x(t) {
r = r || e("./_stream_duplex");
if (!(this instanceof x)) return new x(t);
var i = this instanceof r;
this._readableState = new B(t, this, i);
this.readable = !0;
if (t) {
"function" == typeof t.read && (this._read = t.read);
"function" == typeof t.destroy && (this._destroy = t.destroy);
}
a.call(this);
}
Object.defineProperty(x.prototype, "destroyed", {
enumerable: !1,
get: function() {
return void 0 !== this._readableState && this._readableState.destroyed;
},
set: function(e) {
this._readableState && (this._readableState.destroyed = e);
}
});
x.prototype.destroy = g.destroy;
x.prototype._undestroy = g.undestroy;
x.prototype._destroy = function(e, t) {
t(e);
};
x.prototype.push = function(e, t) {
var i, n = this._readableState;
if (n.objectMode) i = !0; else if ("string" == typeof e) {
if ((t = t || n.defaultEncoding) !== n.encoding) {
e = s.from(e, t);
t = "";
}
i = !0;
}
return k(this, e, t, !1, i);
};
x.prototype.unshift = function(e) {
return k(this, e, null, !0, !1);
};
function k(e, t, i, n, r) {
h("readableAddChunk", t);
var o = e._readableState;
if (null === t) {
o.reading = !1;
R(e, o);
} else {
var a;
r || (a = D(o, t));
if (a) S(e, a); else if (o.objectMode || t && t.length > 0) {
"string" == typeof t || o.objectMode || Object.getPrototypeOf(t) === s.prototype || (t = l(t));
if (n) o.endEmitted ? S(e, new C()) : T(e, o, t, !0); else if (o.ended) S(e, new _()); else {
if (o.destroyed) return !1;
o.reading = !1;
if (o.decoder && !i) {
t = o.decoder.write(t);
o.objectMode || 0 !== t.length ? T(e, o, t, !1) : O(e, o);
} else T(e, o, t, !1);
}
} else if (!n) {
o.reading = !1;
O(e, o);
}
}
return !o.ended && (o.length < o.highWaterMark || 0 === o.length);
}
function T(e, t, i, n) {
if (t.flowing && 0 === t.length && !t.sync) {
t.awaitDrain = 0;
e.emit("data", i);
} else {
t.length += t.objectMode ? 1 : i.length;
n ? t.buffer.unshift(i) : t.buffer.push(i);
t.needReadable && I(e);
}
O(e, t);
}
function D(e, t) {
var i, n;
(n = t, s.isBuffer(n) || n instanceof c) || "string" == typeof t || void 0 === t || e.objectMode || (i = new v("chunk", [ "string", "Buffer", "Uint8Array" ], t));
return i;
}
x.prototype.isPaused = function() {
return !1 === this._readableState.flowing;
};
x.prototype.setEncoding = function(t) {
f || (f = e("string_decoder/").StringDecoder);
var i = new f(t);
this._readableState.decoder = i;
this._readableState.encoding = this._readableState.decoder.encoding;
for (var n = this._readableState.buffer.head, r = ""; null !== n; ) {
r += i.write(n.data);
n = n.next;
}
this._readableState.buffer.clear();
"" !== r && this._readableState.buffer.push(r);
this._readableState.length = r.length;
return this;
};
var E = 1073741824;
function L(e) {
if (e >= E) e = E; else {
e--;
e |= e >>> 1;
e |= e >>> 2;
e |= e >>> 4;
e |= e >>> 8;
e |= e >>> 16;
e++;
}
return e;
}
function P(e, t) {
if (e <= 0 || 0 === t.length && t.ended) return 0;
if (t.objectMode) return 1;
if (e != e) return t.flowing && t.length ? t.buffer.head.data.length : t.length;
e > t.highWaterMark && (t.highWaterMark = L(e));
if (e <= t.length) return e;
if (!t.ended) {
t.needReadable = !0;
return 0;
}
return t.length;
}
x.prototype.read = function(e) {
h("read", e);
e = parseInt(e, 10);
var t = this._readableState, i = e;
0 !== e && (t.emittedReadable = !1);
if (0 === e && t.needReadable && ((0 !== t.highWaterMark ? t.length >= t.highWaterMark : t.length > 0) || t.ended)) {
h("read: emitReadable", t.length, t.ended);
0 === t.length && t.ended ? q(this) : I(this);
return null;
}
if (0 === (e = P(e, t)) && t.ended) {
0 === t.length && q(this);
return null;
}
var n, r = t.needReadable;
h("need readable", r);
(0 === t.length || t.length - e < t.highWaterMark) && h("length less than watermark", r = !0);
if (t.ended || t.reading) h("reading or ended", r = !1); else if (r) {
h("do read");
t.reading = !0;
t.sync = !0;
0 === t.length && (t.needReadable = !0);
this._read(t.highWaterMark);
t.sync = !1;
t.reading || (e = P(i, t));
}
if (null === (n = e > 0 ? W(e, t) : null)) {
t.needReadable = t.length <= t.highWaterMark;
e = 0;
} else {
t.length -= e;
t.awaitDrain = 0;
}
if (0 === t.length) {
t.ended || (t.needReadable = !0);
i !== e && t.ended && q(this);
}
null !== n && this.emit("data", n);
return n;
};
function R(e, t) {
h("onEofChunk");
if (!t.ended) {
if (t.decoder) {
var i = t.decoder.end();
if (i && i.length) {
t.buffer.push(i);
t.length += t.objectMode ? 1 : i.length;
}
}
t.ended = !0;
if (t.sync) I(e); else {
t.needReadable = !1;
if (!t.emittedReadable) {
t.emittedReadable = !0;
N(e);
}
}
}
}
function I(e) {
var t = e._readableState;
h("emitReadable", t.needReadable, t.emittedReadable);
t.needReadable = !1;
if (!t.emittedReadable) {
h("emitReadable", t.flowing);
t.emittedReadable = !0;
i.nextTick(N, e);
}
}
function N(e) {
var t = e._readableState;
h("emitReadable_", t.destroyed, t.length, t.ended);
if (!t.destroyed && (t.length || t.ended)) {
e.emit("readable");
t.emittedReadable = !1;
}
t.needReadable = !t.flowing && !t.ended && t.length <= t.highWaterMark;
H(e);
}
function O(e, t) {
if (!t.readingMore) {
t.readingMore = !0;
i.nextTick(U, e, t);
}
}
function U(e, t) {
for (;!t.reading && !t.ended && (t.length < t.highWaterMark || t.flowing && 0 === t.length); ) {
var i = t.length;
h("maybeReadMore read 0");
e.read(0);
if (i === t.length) break;
}
t.readingMore = !1;
}
x.prototype._read = function() {
S(this, new w("_read()"));
};
x.prototype.pipe = function(e, t) {
var n = this, r = this._readableState;
switch (r.pipesCount) {
case 0:
r.pipes = e;
break;

case 1:
r.pipes = [ r.pipes, e ];
break;

default:
r.pipes.push(e);
}
r.pipesCount += 1;
h("pipe count=%d opts=%j", r.pipesCount, t);
var a = t && !1 === t.end || e === i.stdout || e === i.stderr ? y : c;
r.endEmitted ? i.nextTick(a) : n.once("end", a);
e.on("unpipe", s);
function s(e, t) {
h("onunpipe");
if (e === n && t && !1 === t.hasUnpiped) {
t.hasUnpiped = !0;
f();
}
}
function c() {
h("onend");
e.end();
}
var l = j(n);
e.on("drain", l);
var u = !1;
function f() {
h("cleanup");
e.removeListener("close", m);
e.removeListener("finish", g);
e.removeListener("drain", l);
e.removeListener("error", p);
e.removeListener("unpipe", s);
n.removeListener("end", c);
n.removeListener("end", y);
n.removeListener("data", d);
u = !0;
!r.awaitDrain || e._writableState && !e._writableState.needDrain || l();
}
n.on("data", d);
function d(t) {
h("ondata");
var i = e.write(t);
h("dest.write", i);
if (!1 === i) {
if ((1 === r.pipesCount && r.pipes === e || r.pipesCount > 1 && -1 !== K(r.pipes, e)) && !u) {
h("false write response, pause", r.awaitDrain);
r.awaitDrain++;
}
n.pause();
}
}
function p(t) {
h("onerror", t);
y();
e.removeListener("error", p);
0 === o(e, "error") && S(e, t);
}
M(e, "error", p);
function m() {
e.removeListener("finish", g);
y();
}
e.once("close", m);
function g() {
h("onfinish");
e.removeListener("close", m);
y();
}
e.once("finish", g);
function y() {
h("unpipe");
n.unpipe(e);
}
e.emit("pipe", n);
if (!r.flowing) {
h("pipe resume");
n.resume();
}
return e;
};
function j(e) {
return function() {
var t = e._readableState;
h("pipeOnDrain", t.awaitDrain);
t.awaitDrain && t.awaitDrain--;
if (0 === t.awaitDrain && o(e, "data")) {
t.flowing = !0;
H(e);
}
};
}
x.prototype.unpipe = function(e) {
var t = this._readableState, i = {
hasUnpiped: !1
};
if (0 === t.pipesCount) return this;
if (1 === t.pipesCount) {
if (e && e !== t.pipes) return this;
e || (e = t.pipes);
t.pipes = null;
t.pipesCount = 0;
t.flowing = !1;
e && e.emit("unpipe", this, i);
return this;
}
if (!e) {
var n = t.pipes, r = t.pipesCount;
t.pipes = null;
t.pipesCount = 0;
t.flowing = !1;
for (var o = 0; o < r; o++) n[o].emit("unpipe", this, {
hasUnpiped: !1
});
return this;
}
var a = K(t.pipes, e);
if (-1 === a) return this;
t.pipes.splice(a, 1);
t.pipesCount -= 1;
1 === t.pipesCount && (t.pipes = t.pipes[0]);
e.emit("unpipe", this, i);
return this;
};
x.prototype.on = function(e, t) {
var n = a.prototype.on.call(this, e, t), r = this._readableState;
if ("data" === e) {
r.readableListening = this.listenerCount("readable") > 0;
!1 !== r.flowing && this.resume();
} else if ("readable" === e && !r.endEmitted && !r.readableListening) {
r.readableListening = r.needReadable = !0;
r.flowing = !1;
r.emittedReadable = !1;
h("on readable", r.length, r.reading);
r.length ? I(this) : r.reading || i.nextTick(G, this);
}
return n;
};
x.prototype.addListener = x.prototype.on;
x.prototype.removeListener = function(e, t) {
var n = a.prototype.removeListener.call(this, e, t);
"readable" === e && i.nextTick(F, this);
return n;
};
x.prototype.removeAllListeners = function(e) {
var t = a.prototype.removeAllListeners.apply(this, arguments);
"readable" !== e && void 0 !== e || i.nextTick(F, this);
return t;
};
function F(e) {
var t = e._readableState;
t.readableListening = e.listenerCount("readable") > 0;
t.resumeScheduled && !t.paused ? t.flowing = !0 : e.listenerCount("data") > 0 && e.resume();
}
function G(e) {
h("readable nexttick read 0");
e.read(0);
}
x.prototype.resume = function() {
var e = this._readableState;
if (!e.flowing) {
h("resume");
e.flowing = !e.readableListening;
V(this, e);
}
e.paused = !1;
return this;
};
function V(e, t) {
if (!t.resumeScheduled) {
t.resumeScheduled = !0;
i.nextTick(z, e, t);
}
}
function z(e, t) {
h("resume", t.reading);
t.reading || e.read(0);
t.resumeScheduled = !1;
e.emit("resume");
H(e);
t.flowing && !t.reading && e.read(0);
}
x.prototype.pause = function() {
h("call pause flowing=%j", this._readableState.flowing);
if (!1 !== this._readableState.flowing) {
h("pause");
this._readableState.flowing = !1;
this.emit("pause");
}
this._readableState.paused = !0;
return this;
};
function H(e) {
var t = e._readableState;
h("flow", t.flowing);
for (;t.flowing && null !== e.read(); ) ;
}
x.prototype.wrap = function(e) {
var t = this, i = this._readableState, n = !1;
e.on("end", function() {
h("wrapped end");
if (i.decoder && !i.ended) {
var e = i.decoder.end();
e && e.length && t.push(e);
}
t.push(null);
});
e.on("data", function(r) {
h("wrapped data");
i.decoder && (r = i.decoder.write(r));
if ((!i.objectMode || null != r) && (i.objectMode || r && r.length) && !t.push(r)) {
n = !0;
e.pause();
}
});
for (var r in e) void 0 === this[r] && "function" == typeof e[r] && (this[r] = function(t) {
return function() {
return e[t].apply(e, arguments);
};
}(r));
for (var o = 0; o < A.length; o++) e.on(A[o], this.emit.bind(this, A[o]));
this._read = function(t) {
h("wrapped _read", t);
if (n) {
n = !1;
e.resume();
}
};
return this;
};
"function" == typeof Symbol && (x.prototype[Symbol.asyncIterator] = function() {
void 0 === d && (d = e("./internal/streams/async_iterator"));
return d(this);
});
Object.defineProperty(x.prototype, "readableHighWaterMark", {
enumerable: !1,
get: function() {
return this._readableState.highWaterMark;
}
});
Object.defineProperty(x.prototype, "readableBuffer", {
enumerable: !1,
get: function() {
return this._readableState && this._readableState.buffer;
}
});
Object.defineProperty(x.prototype, "readableFlowing", {
enumerable: !1,
get: function() {
return this._readableState.flowing;
},
set: function(e) {
this._readableState && (this._readableState.flowing = e);
}
});
x._fromList = W;
Object.defineProperty(x.prototype, "readableLength", {
enumerable: !1,
get: function() {
return this._readableState.length;
}
});
function W(e, t) {
if (0 === t.length) return null;
var i;
if (t.objectMode) i = t.buffer.shift(); else if (!e || e >= t.length) {
i = t.decoder ? t.buffer.join("") : 1 === t.buffer.length ? t.buffer.first() : t.buffer.concat(t.length);
t.buffer.clear();
} else i = t.buffer.consume(e, t.decoder);
return i;
}
function q(e) {
var t = e._readableState;
h("endReadable", t.endEmitted);
if (!t.endEmitted) {
t.ended = !0;
i.nextTick(X, t, e);
}
}
function X(e, t) {
h("endReadableNT", e.endEmitted, e.length);
if (!e.endEmitted && 0 === e.length) {
e.endEmitted = !0;
t.readable = !1;
t.emit("end");
if (e.autoDestroy) {
var i = t._writableState;
(!i || i.autoDestroy && i.finished) && t.destroy();
}
}
}
"function" == typeof Symbol && (x.from = function(t, i) {
void 0 === p && (p = e("./internal/streams/from"));
return p(x, t, i);
});
function K(e, t) {
for (var i = 0, n = e.length; i < n; i++) if (e[i] === t) return i;
return -1;
}
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"../errors": 47,
"./_stream_duplex": 48,
"./internal/streams/async_iterator": 53,
"./internal/streams/buffer_list": 54,
"./internal/streams/destroy": 55,
"./internal/streams/from": 57,
"./internal/streams/state": 59,
"./internal/streams/stream": 60,
_process: 156,
buffer: 65,
events: 104,
inherits: 138,
"string_decoder/": 63,
util: 19
} ],
51: [ function(e, t) {
"use strict";
t.exports = l;
var i = e("../errors").codes, n = i.ERR_METHOD_NOT_IMPLEMENTED, r = i.ERR_MULTIPLE_CALLBACK, o = i.ERR_TRANSFORM_ALREADY_TRANSFORMING, a = i.ERR_TRANSFORM_WITH_LENGTH_0, s = e("./_stream_duplex");
e("inherits")(l, s);
function c(e, t) {
var i = this._transformState;
i.transforming = !1;
var n = i.writecb;
if (null === n) return this.emit("error", new r());
i.writechunk = null;
i.writecb = null;
null != t && this.push(t);
n(e);
var o = this._readableState;
o.reading = !1;
(o.needReadable || o.length < o.highWaterMark) && this._read(o.highWaterMark);
}
function l(e) {
if (!(this instanceof l)) return new l(e);
s.call(this, e);
this._transformState = {
afterTransform: c.bind(this),
needTransform: !1,
transforming: !1,
writecb: null,
writechunk: null,
writeencoding: null
};
this._readableState.needReadable = !0;
this._readableState.sync = !1;
if (e) {
"function" == typeof e.transform && (this._transform = e.transform);
"function" == typeof e.flush && (this._flush = e.flush);
}
this.on("prefinish", h);
}
function h() {
var e = this;
"function" != typeof this._flush || this._readableState.destroyed ? u(this, null, null) : this._flush(function(t, i) {
u(e, t, i);
});
}
l.prototype.push = function(e, t) {
this._transformState.needTransform = !1;
return s.prototype.push.call(this, e, t);
};
l.prototype._transform = function(e, t, i) {
i(new n("_transform()"));
};
l.prototype._write = function(e, t, i) {
var n = this._transformState;
n.writecb = i;
n.writechunk = e;
n.writeencoding = t;
if (!n.transforming) {
var r = this._readableState;
(n.needTransform || r.needReadable || r.length < r.highWaterMark) && this._read(r.highWaterMark);
}
};
l.prototype._read = function() {
var e = this._transformState;
if (null === e.writechunk || e.transforming) e.needTransform = !0; else {
e.transforming = !0;
this._transform(e.writechunk, e.writeencoding, e.afterTransform);
}
};
l.prototype._destroy = function(e, t) {
s.prototype._destroy.call(this, e, function(e) {
t(e);
});
};
function u(e, t, i) {
if (t) return e.emit("error", t);
null != i && e.push(i);
if (e._writableState.length) throw new a();
if (e._transformState.transforming) throw new o();
return e.push(null);
}
}, {
"../errors": 47,
"./_stream_duplex": 48,
inherits: 138
} ],
52: [ function(e, t) {
(function(i, n) {
"use strict";
t.exports = B;
function r(e) {
var t = this;
this.next = null;
this.entry = null;
this.finish = function() {
z(t, e);
};
}
var o;
B.WritableState = M;
var a = {
deprecate: e("util-deprecate")
}, s = e("./internal/streams/stream"), c = e("buffer").Buffer, l = n.Uint8Array || function() {};
function h(e) {
return c.from(e);
}
var u, f = e("./internal/streams/destroy"), d = e("./internal/streams/state").getHighWaterMark, p = e("../errors").codes, m = p.ERR_INVALID_ARG_TYPE, g = p.ERR_METHOD_NOT_IMPLEMENTED, y = p.ERR_MULTIPLE_CALLBACK, b = p.ERR_STREAM_CANNOT_PIPE, v = p.ERR_STREAM_DESTROYED, _ = p.ERR_STREAM_NULL_VALUES, w = p.ERR_STREAM_WRITE_AFTER_END, C = p.ERR_UNKNOWN_ENCODING, S = f.errorOrDestroy;
e("inherits")(B, s);
function A() {}
function M(t, i, n) {
o = o || e("./_stream_duplex");
t = t || {};
"boolean" != typeof n && (n = i instanceof o);
this.objectMode = !!t.objectMode;
n && (this.objectMode = this.objectMode || !!t.writableObjectMode);
this.highWaterMark = d(this, t, "writableHighWaterMark", n);
this.finalCalled = !1;
this.needDrain = !1;
this.ending = !1;
this.ended = !1;
this.finished = !1;
this.destroyed = !1;
var a = !1 === t.decodeStrings;
this.decodeStrings = !a;
this.defaultEncoding = t.defaultEncoding || "utf8";
this.length = 0;
this.writing = !1;
this.corked = 0;
this.sync = !0;
this.bufferProcessing = !1;
this.onwrite = function(e) {
R(i, e);
};
this.writecb = null;
this.writelen = 0;
this.bufferedRequest = null;
this.lastBufferedRequest = null;
this.pendingcb = 0;
this.prefinished = !1;
this.errorEmitted = !1;
this.emitClose = !1 !== t.emitClose;
this.autoDestroy = !!t.autoDestroy;
this.bufferedRequestCount = 0;
this.corkedRequestsFree = new r(this);
}
M.prototype.getBuffer = function() {
for (var e = this.bufferedRequest, t = []; e; ) {
t.push(e);
e = e.next;
}
return t;
};
(function() {
try {
Object.defineProperty(M.prototype, "buffer", {
get: a.deprecate(function() {
return this.getBuffer();
}, "_writableState.buffer is deprecated. Use _writableState.getBuffer instead.", "DEP0003")
});
} catch (e) {}
})();
if ("function" == typeof Symbol && Symbol.hasInstance && "function" == typeof Function.prototype[Symbol.hasInstance]) {
u = Function.prototype[Symbol.hasInstance];
Object.defineProperty(B, Symbol.hasInstance, {
value: function(e) {
return !!u.call(this, e) || this === B && e && e._writableState instanceof M;
}
});
} else u = function(e) {
return e instanceof this;
};
function B(t) {
var i = this instanceof (o = o || e("./_stream_duplex"));
if (!i && !u.call(B, this)) return new B(t);
this._writableState = new M(t, this, i);
this.writable = !0;
if (t) {
"function" == typeof t.write && (this._write = t.write);
"function" == typeof t.writev && (this._writev = t.writev);
"function" == typeof t.destroy && (this._destroy = t.destroy);
"function" == typeof t.final && (this._final = t.final);
}
s.call(this);
}
B.prototype.pipe = function() {
S(this, new b());
};
function x(e, t) {
var n = new w();
S(e, n);
i.nextTick(t, n);
}
function k(e, t, n, r) {
var o;
null === n ? o = new _() : "string" == typeof n || t.objectMode || (o = new m("chunk", [ "string", "Buffer" ], n));
if (o) {
S(e, o);
i.nextTick(r, o);
return !1;
}
return !0;
}
B.prototype.write = function(e, t, i) {
var n, r = this._writableState, o = !1, a = !r.objectMode && (n = e, c.isBuffer(n) || n instanceof l);
a && !c.isBuffer(e) && (e = h(e));
if ("function" == typeof t) {
i = t;
t = null;
}
a ? t = "buffer" : t || (t = r.defaultEncoding);
"function" != typeof i && (i = A);
if (r.ending) x(this, i); else if (a || k(this, r, e, i)) {
r.pendingcb++;
o = D(this, r, a, e, t, i);
}
return o;
};
B.prototype.cork = function() {
this._writableState.corked++;
};
B.prototype.uncork = function() {
var e = this._writableState;
if (e.corked) {
e.corked--;
e.writing || e.corked || e.bufferProcessing || !e.bufferedRequest || O(this, e);
}
};
B.prototype.setDefaultEncoding = function(e) {
"string" == typeof e && (e = e.toLowerCase());
if (!([ "hex", "utf8", "utf-8", "ascii", "binary", "base64", "ucs2", "ucs-2", "utf16le", "utf-16le", "raw" ].indexOf((e + "").toLowerCase()) > -1)) throw new C(e);
this._writableState.defaultEncoding = e;
return this;
};
Object.defineProperty(B.prototype, "writableBuffer", {
enumerable: !1,
get: function() {
return this._writableState && this._writableState.getBuffer();
}
});
function T(e, t, i) {
e.objectMode || !1 === e.decodeStrings || "string" != typeof t || (t = c.from(t, i));
return t;
}
Object.defineProperty(B.prototype, "writableHighWaterMark", {
enumerable: !1,
get: function() {
return this._writableState.highWaterMark;
}
});
function D(e, t, i, n, r, o) {
if (!i) {
var a = T(t, n, r);
if (n !== a) {
i = !0;
r = "buffer";
n = a;
}
}
var s = t.objectMode ? 1 : n.length;
t.length += s;
var c = t.length < t.highWaterMark;
c || (t.needDrain = !0);
if (t.writing || t.corked) {
var l = t.lastBufferedRequest;
t.lastBufferedRequest = {
chunk: n,
encoding: r,
isBuf: i,
callback: o,
next: null
};
l ? l.next = t.lastBufferedRequest : t.bufferedRequest = t.lastBufferedRequest;
t.bufferedRequestCount += 1;
} else E(e, t, !1, s, n, r, o);
return c;
}
function E(e, t, i, n, r, o, a) {
t.writelen = n;
t.writecb = a;
t.writing = !0;
t.sync = !0;
t.destroyed ? t.onwrite(new v("write")) : i ? e._writev(r, t.onwrite) : e._write(r, o, t.onwrite);
t.sync = !1;
}
function L(e, t, n, r, o) {
--t.pendingcb;
if (n) {
i.nextTick(o, r);
i.nextTick(G, e, t);
e._writableState.errorEmitted = !0;
S(e, r);
} else {
o(r);
e._writableState.errorEmitted = !0;
S(e, r);
G(e, t);
}
}
function P(e) {
e.writing = !1;
e.writecb = null;
e.length -= e.writelen;
e.writelen = 0;
}
function R(e, t) {
var n = e._writableState, r = n.sync, o = n.writecb;
if ("function" != typeof o) throw new y();
P(n);
if (t) L(e, n, r, t, o); else {
var a = U(n) || e.destroyed;
a || n.corked || n.bufferProcessing || !n.bufferedRequest || O(e, n);
r ? i.nextTick(I, e, n, a, o) : I(e, n, a, o);
}
}
function I(e, t, i, n) {
i || N(e, t);
t.pendingcb--;
n();
G(e, t);
}
function N(e, t) {
if (0 === t.length && t.needDrain) {
t.needDrain = !1;
e.emit("drain");
}
}
function O(e, t) {
t.bufferProcessing = !0;
var i = t.bufferedRequest;
if (e._writev && i && i.next) {
var n = t.bufferedRequestCount, o = new Array(n), a = t.corkedRequestsFree;
a.entry = i;
for (var s = 0, c = !0; i; ) {
o[s] = i;
i.isBuf || (c = !1);
i = i.next;
s += 1;
}
o.allBuffers = c;
E(e, t, !0, t.length, o, "", a.finish);
t.pendingcb++;
t.lastBufferedRequest = null;
if (a.next) {
t.corkedRequestsFree = a.next;
a.next = null;
} else t.corkedRequestsFree = new r(t);
t.bufferedRequestCount = 0;
} else {
for (;i; ) {
var l = i.chunk, h = i.encoding, u = i.callback;
E(e, t, !1, t.objectMode ? 1 : l.length, l, h, u);
i = i.next;
t.bufferedRequestCount--;
if (t.writing) break;
}
null === i && (t.lastBufferedRequest = null);
}
t.bufferedRequest = i;
t.bufferProcessing = !1;
}
B.prototype._write = function(e, t, i) {
i(new g("_write()"));
};
B.prototype._writev = null;
B.prototype.end = function(e, t, i) {
var n = this._writableState;
if ("function" == typeof e) {
i = e;
e = null;
t = null;
} else if ("function" == typeof t) {
i = t;
t = null;
}
null != e && this.write(e, t);
if (n.corked) {
n.corked = 1;
this.uncork();
}
n.ending || V(this, n, i);
return this;
};
Object.defineProperty(B.prototype, "writableLength", {
enumerable: !1,
get: function() {
return this._writableState.length;
}
});
function U(e) {
return e.ending && 0 === e.length && null === e.bufferedRequest && !e.finished && !e.writing;
}
function j(e, t) {
e._final(function(i) {
t.pendingcb--;
i && S(e, i);
t.prefinished = !0;
e.emit("prefinish");
G(e, t);
});
}
function F(e, t) {
if (!t.prefinished && !t.finalCalled) if ("function" != typeof e._final || t.destroyed) {
t.prefinished = !0;
e.emit("prefinish");
} else {
t.pendingcb++;
t.finalCalled = !0;
i.nextTick(j, e, t);
}
}
function G(e, t) {
var i = U(t);
if (i) {
F(e, t);
if (0 === t.pendingcb) {
t.finished = !0;
e.emit("finish");
if (t.autoDestroy) {
var n = e._readableState;
(!n || n.autoDestroy && n.endEmitted) && e.destroy();
}
}
}
return i;
}
function V(e, t, n) {
t.ending = !0;
G(e, t);
n && (t.finished ? i.nextTick(n) : e.once("finish", n));
t.ended = !0;
e.writable = !1;
}
function z(e, t, i) {
var n = e.entry;
e.entry = null;
for (;n; ) {
var r = n.callback;
t.pendingcb--;
r(i);
n = n.next;
}
t.corkedRequestsFree.next = e;
}
Object.defineProperty(B.prototype, "destroyed", {
enumerable: !1,
get: function() {
return void 0 !== this._writableState && this._writableState.destroyed;
},
set: function(e) {
this._writableState && (this._writableState.destroyed = e);
}
});
B.prototype.destroy = f.destroy;
B.prototype._undestroy = f.undestroy;
B.prototype._destroy = function(e, t) {
t(e);
};
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"../errors": 47,
"./_stream_duplex": 48,
"./internal/streams/destroy": 55,
"./internal/streams/state": 59,
"./internal/streams/stream": 60,
_process: 156,
buffer: 65,
inherits: 138,
"util-deprecate": 194
} ],
53: [ function(e, t) {
(function(i) {
"use strict";
var n;
function r(e, t, i) {
t in e ? Object.defineProperty(e, t, {
value: i,
enumerable: !0,
configurable: !0,
writable: !0
}) : e[t] = i;
return e;
}
var o = e("./end-of-stream"), a = Symbol("lastResolve"), s = Symbol("lastReject"), c = Symbol("error"), l = Symbol("ended"), h = Symbol("lastPromise"), u = Symbol("handlePromise"), f = Symbol("stream");
function d(e, t) {
return {
value: e,
done: t
};
}
function p(e) {
var t = e[a];
if (null !== t) {
var i = e[f].read();
if (null !== i) {
e[h] = null;
e[a] = null;
e[s] = null;
t(d(i, !1));
}
}
}
function m(e) {
i.nextTick(p, e);
}
function g(e, t) {
return function(i, n) {
e.then(function() {
t[l] ? i(d(void 0, !0)) : t[u](i, n);
}, n);
};
}
var y = Object.getPrototypeOf(function() {}), b = Object.setPrototypeOf((r(n = {
get stream() {
return this[f];
},
next: function() {
var e = this, t = this[c];
if (null !== t) return Promise.reject(t);
if (this[l]) return Promise.resolve(d(void 0, !0));
if (this[f].destroyed) return new Promise(function(t, n) {
i.nextTick(function() {
e[c] ? n(e[c]) : t(d(void 0, !0));
});
});
var n, r = this[h];
if (r) n = new Promise(g(r, this)); else {
var o = this[f].read();
if (null !== o) return Promise.resolve(d(o, !1));
n = new Promise(this[u]);
}
this[h] = n;
return n;
}
}, Symbol.asyncIterator, function() {
return this;
}), r(n, "return", function() {
var e = this;
return new Promise(function(t, i) {
e[f].destroy(null, function(e) {
e ? i(e) : t(d(void 0, !0));
});
});
}), n), y);
t.exports = function(e) {
var t, i = Object.create(b, (r(t = {}, f, {
value: e,
writable: !0
}), r(t, a, {
value: null,
writable: !0
}), r(t, s, {
value: null,
writable: !0
}), r(t, c, {
value: null,
writable: !0
}), r(t, l, {
value: e._readableState.endEmitted,
writable: !0
}), r(t, u, {
value: function(e, t) {
var n = i[f].read();
if (n) {
i[h] = null;
i[a] = null;
i[s] = null;
e(d(n, !1));
} else {
i[a] = e;
i[s] = t;
}
},
writable: !0
}), t));
i[h] = null;
o(e, function(e) {
if (e && "ERR_STREAM_PREMATURE_CLOSE" !== e.code) {
var t = i[s];
if (null !== t) {
i[h] = null;
i[a] = null;
i[s] = null;
t(e);
}
i[c] = e;
} else {
var n = i[a];
if (null !== n) {
i[h] = null;
i[a] = null;
i[s] = null;
n(d(void 0, !0));
}
i[l] = !0;
}
});
e.on("readable", m.bind(null, i));
return i;
};
}).call(this, e("_process"));
}, {
"./end-of-stream": 56,
_process: 156
} ],
54: [ function(e, t) {
"use strict";
function i(e, t) {
var i = Object.keys(e);
if (Object.getOwnPropertySymbols) {
var n = Object.getOwnPropertySymbols(e);
t && (n = n.filter(function(t) {
return Object.getOwnPropertyDescriptor(e, t).enumerable;
}));
i.push.apply(i, n);
}
return i;
}
function n(e) {
for (var t = 1; t < arguments.length; t++) {
var n = null != arguments[t] ? arguments[t] : {};
t % 2 ? i(Object(n), !0).forEach(function(t) {
r(e, t, n[t]);
}) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : i(Object(n)).forEach(function(t) {
Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t));
});
}
return e;
}
function r(e, t, i) {
t in e ? Object.defineProperty(e, t, {
value: i,
enumerable: !0,
configurable: !0,
writable: !0
}) : e[t] = i;
return e;
}
function o(e, t) {
if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}
function a(e, t) {
for (var i = 0; i < t.length; i++) {
var n = t[i];
n.enumerable = n.enumerable || !1;
n.configurable = !0;
"value" in n && (n.writable = !0);
Object.defineProperty(e, n.key, n);
}
}
function s(e, t, i) {
t && a(e.prototype, t);
i && a(e, i);
return e;
}
var c = e("buffer").Buffer, l = e("util").inspect, h = l && l.custom || "inspect";
t.exports = function() {
function e() {
o(this, e);
this.head = null;
this.tail = null;
this.length = 0;
}
s(e, [ {
key: "push",
value: function(e) {
var t = {
data: e,
next: null
};
this.length > 0 ? this.tail.next = t : this.head = t;
this.tail = t;
++this.length;
}
}, {
key: "unshift",
value: function(e) {
var t = {
data: e,
next: this.head
};
0 === this.length && (this.tail = t);
this.head = t;
++this.length;
}
}, {
key: "shift",
value: function() {
if (0 !== this.length) {
var e = this.head.data;
1 === this.length ? this.head = this.tail = null : this.head = this.head.next;
--this.length;
return e;
}
}
}, {
key: "clear",
value: function() {
this.head = this.tail = null;
this.length = 0;
}
}, {
key: "join",
value: function(e) {
if (0 === this.length) return "";
for (var t = this.head, i = "" + t.data; t = t.next; ) i += e + t.data;
return i;
}
}, {
key: "concat",
value: function(e) {
if (0 === this.length) return c.alloc(0);
for (var t, i, n, r = c.allocUnsafe(e >>> 0), o = this.head, a = 0; o; ) {
t = o.data, i = r, n = a, c.prototype.copy.call(t, i, n);
a += o.data.length;
o = o.next;
}
return r;
}
}, {
key: "consume",
value: function(e, t) {
var i;
if (e < this.head.data.length) {
i = this.head.data.slice(0, e);
this.head.data = this.head.data.slice(e);
} else i = e === this.head.data.length ? this.shift() : t ? this._getString(e) : this._getBuffer(e);
return i;
}
}, {
key: "first",
value: function() {
return this.head.data;
}
}, {
key: "_getString",
value: function(e) {
var t = this.head, i = 1, n = t.data;
e -= n.length;
for (;t = t.next; ) {
var r = t.data, o = e > r.length ? r.length : e;
o === r.length ? n += r : n += r.slice(0, e);
if (0 == (e -= o)) {
if (o === r.length) {
++i;
t.next ? this.head = t.next : this.head = this.tail = null;
} else {
this.head = t;
t.data = r.slice(o);
}
break;
}
++i;
}
this.length -= i;
return n;
}
}, {
key: "_getBuffer",
value: function(e) {
var t = c.allocUnsafe(e), i = this.head, n = 1;
i.data.copy(t);
e -= i.data.length;
for (;i = i.next; ) {
var r = i.data, o = e > r.length ? r.length : e;
r.copy(t, t.length - e, 0, o);
if (0 == (e -= o)) {
if (o === r.length) {
++n;
i.next ? this.head = i.next : this.head = this.tail = null;
} else {
this.head = i;
i.data = r.slice(o);
}
break;
}
++n;
}
this.length -= n;
return t;
}
}, {
key: h,
value: function(e, t) {
return l(this, n({}, t, {
depth: 0,
customInspect: !1
}));
}
} ]);
return e;
}();
}, {
buffer: 65,
util: 19
} ],
55: [ function(e, t) {
(function(e) {
"use strict";
function i(e, t) {
r(e, t);
n(e);
}
function n(e) {
e._writableState && !e._writableState.emitClose || e._readableState && !e._readableState.emitClose || e.emit("close");
}
function r(e, t) {
e.emit("error", t);
}
t.exports = {
destroy: function(t, o) {
var a = this, s = this._readableState && this._readableState.destroyed, c = this._writableState && this._writableState.destroyed;
if (s || c) {
if (o) o(t); else if (t) if (this._writableState) {
if (!this._writableState.errorEmitted) {
this._writableState.errorEmitted = !0;
e.nextTick(r, this, t);
}
} else e.nextTick(r, this, t);
return this;
}
this._readableState && (this._readableState.destroyed = !0);
this._writableState && (this._writableState.destroyed = !0);
this._destroy(t || null, function(t) {
if (!o && t) if (a._writableState) if (a._writableState.errorEmitted) e.nextTick(n, a); else {
a._writableState.errorEmitted = !0;
e.nextTick(i, a, t);
} else e.nextTick(i, a, t); else if (o) {
e.nextTick(n, a);
o(t);
} else e.nextTick(n, a);
});
return this;
},
undestroy: function() {
if (this._readableState) {
this._readableState.destroyed = !1;
this._readableState.reading = !1;
this._readableState.ended = !1;
this._readableState.endEmitted = !1;
}
if (this._writableState) {
this._writableState.destroyed = !1;
this._writableState.ended = !1;
this._writableState.ending = !1;
this._writableState.finalCalled = !1;
this._writableState.prefinished = !1;
this._writableState.finished = !1;
this._writableState.errorEmitted = !1;
}
},
errorOrDestroy: function(e, t) {
var i = e._readableState, n = e._writableState;
i && i.autoDestroy || n && n.autoDestroy ? e.destroy(t) : e.emit("error", t);
}
};
}).call(this, e("_process"));
}, {
_process: 156
} ],
56: [ function(e, t) {
"use strict";
var i = e("../../../errors").codes.ERR_STREAM_PREMATURE_CLOSE;
function n(e) {
var t = !1;
return function() {
if (!t) {
t = !0;
for (var i = arguments.length, n = new Array(i), r = 0; r < i; r++) n[r] = arguments[r];
e.apply(this, n);
}
};
}
function r() {}
function o(e) {
return e.setHeader && "function" == typeof e.abort;
}
t.exports = function e(t, a, s) {
if ("function" == typeof a) return e(t, null, a);
a || (a = {});
s = n(s || r);
var c = a.readable || !1 !== a.readable && t.readable, l = a.writable || !1 !== a.writable && t.writable, h = function() {
t.writable || f();
}, u = t._writableState && t._writableState.finished, f = function() {
l = !1;
u = !0;
c || s.call(t);
}, d = t._readableState && t._readableState.endEmitted, p = function() {
c = !1;
d = !0;
l || s.call(t);
}, m = function(e) {
s.call(t, e);
}, g = function() {
var e;
if (c && !d) {
t._readableState && t._readableState.ended || (e = new i());
return s.call(t, e);
}
if (l && !u) {
t._writableState && t._writableState.ended || (e = new i());
return s.call(t, e);
}
}, y = function() {
t.req.on("finish", f);
};
if (o(t)) {
t.on("complete", f);
t.on("abort", g);
t.req ? y() : t.on("request", y);
} else if (l && !t._writableState) {
t.on("end", h);
t.on("close", h);
}
t.on("end", p);
t.on("finish", f);
!1 !== a.error && t.on("error", m);
t.on("close", g);
return function() {
t.removeListener("complete", f);
t.removeListener("abort", g);
t.removeListener("request", y);
t.req && t.req.removeListener("finish", f);
t.removeListener("end", h);
t.removeListener("close", h);
t.removeListener("finish", f);
t.removeListener("end", p);
t.removeListener("error", m);
t.removeListener("close", g);
};
};
}, {
"../../../errors": 47
} ],
57: [ function(e, t) {
t.exports = function() {
throw new Error("Readable.from is not available in the browser");
};
}, {} ],
58: [ function(e, t) {
"use strict";
var i;
function n(e) {
var t = !1;
return function() {
if (!t) {
t = !0;
e.apply(void 0, arguments);
}
};
}
var r = e("../../../errors").codes, o = r.ERR_MISSING_ARGS, a = r.ERR_STREAM_DESTROYED;
function s(e) {
if (e) throw e;
}
function c(e) {
return e.setHeader && "function" == typeof e.abort;
}
function l(t, r, o, s) {
s = n(s);
var l = !1;
t.on("close", function() {
l = !0;
});
void 0 === i && (i = e("./end-of-stream"));
i(t, {
readable: r,
writable: o
}, function(e) {
if (e) return s(e);
l = !0;
s();
});
var h = !1;
return function(e) {
if (!l && !h) {
h = !0;
if (c(t)) return t.abort();
if ("function" == typeof t.destroy) return t.destroy();
s(e || new a("pipe"));
}
};
}
function h(e) {
e();
}
function u(e, t) {
return e.pipe(t);
}
function f(e) {
return e.length ? "function" != typeof e[e.length - 1] ? s : e.pop() : s;
}
t.exports = function() {
for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
var n, r = f(t);
Array.isArray(t[0]) && (t = t[0]);
if (t.length < 2) throw new o("streams");
var a = t.map(function(e, i) {
var o = i < t.length - 1;
return l(e, o, i > 0, function(e) {
n || (n = e);
e && a.forEach(h);
if (!o) {
a.forEach(h);
r(n);
}
});
});
return t.reduce(u);
};
}, {
"../../../errors": 47,
"./end-of-stream": 56
} ],
59: [ function(e, t) {
"use strict";
var i = e("../../../errors").codes.ERR_INVALID_OPT_VALUE;
function n(e, t, i) {
return null != e.highWaterMark ? e.highWaterMark : t ? e[i] : null;
}
t.exports = {
getHighWaterMark: function(e, t, r, o) {
var a = n(t, o, r);
if (null != a) {
if (!isFinite(a) || Math.floor(a) !== a || a < 0) throw new i(o ? r : "highWaterMark", a);
return Math.floor(a);
}
return e.objectMode ? 16 : 16384;
}
};
}, {
"../../../errors": 47
} ],
60: [ function(e, t) {
t.exports = e("events").EventEmitter;
}, {
events: 104
} ],
61: [ function(e, t, i) {
(i = t.exports = e("./lib/_stream_readable.js")).Stream = i;
i.Readable = i;
i.Writable = e("./lib/_stream_writable.js");
i.Duplex = e("./lib/_stream_duplex.js");
i.Transform = e("./lib/_stream_transform.js");
i.PassThrough = e("./lib/_stream_passthrough.js");
i.finished = e("./lib/internal/streams/end-of-stream.js");
i.pipeline = e("./lib/internal/streams/pipeline.js");
}, {
"./lib/_stream_duplex.js": 48,
"./lib/_stream_passthrough.js": 49,
"./lib/_stream_readable.js": 50,
"./lib/_stream_transform.js": 51,
"./lib/_stream_writable.js": 52,
"./lib/internal/streams/end-of-stream.js": 56,
"./lib/internal/streams/pipeline.js": 58
} ],
62: [ function(e, t, i) {
var n = e("buffer"), r = n.Buffer;
function o(e, t) {
for (var i in e) t[i] = e[i];
}
if (r.from && r.alloc && r.allocUnsafe && r.allocUnsafeSlow) t.exports = n; else {
o(n, i);
i.Buffer = a;
}
function a(e, t, i) {
return r(e, t, i);
}
a.prototype = Object.create(r.prototype);
o(r, a);
a.from = function(e, t, i) {
if ("number" == typeof e) throw new TypeError("Argument must not be a number");
return r(e, t, i);
};
a.alloc = function(e, t, i) {
if ("number" != typeof e) throw new TypeError("Argument must be a number");
var n = r(e);
void 0 !== t ? "string" == typeof i ? n.fill(t, i) : n.fill(t) : n.fill(0);
return n;
};
a.allocUnsafe = function(e) {
if ("number" != typeof e) throw new TypeError("Argument must be a number");
return r(e);
};
a.allocUnsafeSlow = function(e) {
if ("number" != typeof e) throw new TypeError("Argument must be a number");
return n.SlowBuffer(e);
};
}, {
buffer: 65
} ],
63: [ function(e, t, i) {
"use strict";
var n = e("safe-buffer").Buffer, r = n.isEncoding || function(e) {
switch ((e = "" + e) && e.toLowerCase()) {
case "hex":
case "utf8":
case "utf-8":
case "ascii":
case "binary":
case "base64":
case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
case "raw":
return !0;

default:
return !1;
}
};
function o(e) {
if (!e) return "utf8";
for (var t; ;) switch (e) {
case "utf8":
case "utf-8":
return "utf8";

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return "utf16le";

case "latin1":
case "binary":
return "latin1";

case "base64":
case "ascii":
case "hex":
return e;

default:
if (t) return;
e = ("" + e).toLowerCase();
t = !0;
}
}
function a(e) {
var t = o(e);
if ("string" != typeof t && (n.isEncoding === r || !r(e))) throw new Error("Unknown encoding: " + e);
return t || e;
}
i.StringDecoder = s;
function s(e) {
this.encoding = a(e);
var t;
switch (this.encoding) {
case "utf16le":
this.text = f;
this.end = d;
t = 4;
break;

case "utf8":
this.fillLast = u;
t = 4;
break;

case "base64":
this.text = p;
this.end = m;
t = 3;
break;

default:
this.write = g;
this.end = y;
return;
}
this.lastNeed = 0;
this.lastTotal = 0;
this.lastChar = n.allocUnsafe(t);
}
s.prototype.write = function(e) {
if (0 === e.length) return "";
var t, i;
if (this.lastNeed) {
if (void 0 === (t = this.fillLast(e))) return "";
i = this.lastNeed;
this.lastNeed = 0;
} else i = 0;
return i < e.length ? t ? t + this.text(e, i) : this.text(e, i) : t || "";
};
s.prototype.end = function(e) {
var t = e && e.length ? this.write(e) : "";
return this.lastNeed ? t + "�" : t;
};
s.prototype.text = function(e, t) {
var i = l(this, e, t);
if (!this.lastNeed) return e.toString("utf8", t);
this.lastTotal = i;
var n = e.length - (i - this.lastNeed);
e.copy(this.lastChar, 0, n);
return e.toString("utf8", t, n);
};
s.prototype.fillLast = function(e) {
if (this.lastNeed <= e.length) {
e.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, this.lastNeed);
return this.lastChar.toString(this.encoding, 0, this.lastTotal);
}
e.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, e.length);
this.lastNeed -= e.length;
};
function c(e) {
return e <= 127 ? 0 : e >> 5 == 6 ? 2 : e >> 4 == 14 ? 3 : e >> 3 == 30 ? 4 : e >> 6 == 2 ? -1 : -2;
}
function l(e, t, i) {
var n = t.length - 1;
if (n < i) return 0;
var r = c(t[n]);
if (r >= 0) {
r > 0 && (e.lastNeed = r - 1);
return r;
}
if (--n < i || -2 === r) return 0;
if ((r = c(t[n])) >= 0) {
r > 0 && (e.lastNeed = r - 2);
return r;
}
if (--n < i || -2 === r) return 0;
if ((r = c(t[n])) >= 0) {
r > 0 && (2 === r ? r = 0 : e.lastNeed = r - 3);
return r;
}
return 0;
}
function h(e, t) {
if (128 != (192 & t[0])) {
e.lastNeed = 0;
return "�";
}
if (e.lastNeed > 1 && t.length > 1) {
if (128 != (192 & t[1])) {
e.lastNeed = 1;
return "�";
}
if (e.lastNeed > 2 && t.length > 2 && 128 != (192 & t[2])) {
e.lastNeed = 2;
return "�";
}
}
}
function u(e) {
var t = this.lastTotal - this.lastNeed, i = h(this, e);
if (void 0 !== i) return i;
if (this.lastNeed <= e.length) {
e.copy(this.lastChar, t, 0, this.lastNeed);
return this.lastChar.toString(this.encoding, 0, this.lastTotal);
}
e.copy(this.lastChar, t, 0, e.length);
this.lastNeed -= e.length;
}
function f(e, t) {
if ((e.length - t) % 2 == 0) {
var i = e.toString("utf16le", t);
if (i) {
var n = i.charCodeAt(i.length - 1);
if (n >= 55296 && n <= 56319) {
this.lastNeed = 2;
this.lastTotal = 4;
this.lastChar[0] = e[e.length - 2];
this.lastChar[1] = e[e.length - 1];
return i.slice(0, -1);
}
}
return i;
}
this.lastNeed = 1;
this.lastTotal = 2;
this.lastChar[0] = e[e.length - 1];
return e.toString("utf16le", t, e.length - 1);
}
function d(e) {
var t = e && e.length ? this.write(e) : "";
if (this.lastNeed) {
var i = this.lastTotal - this.lastNeed;
return t + this.lastChar.toString("utf16le", 0, i);
}
return t;
}
function p(e, t) {
var i = (e.length - t) % 3;
if (0 === i) return e.toString("base64", t);
this.lastNeed = 3 - i;
this.lastTotal = 3;
if (1 === i) this.lastChar[0] = e[e.length - 1]; else {
this.lastChar[0] = e[e.length - 2];
this.lastChar[1] = e[e.length - 1];
}
return e.toString("base64", t, e.length - i);
}
function m(e) {
var t = e && e.length ? this.write(e) : "";
return this.lastNeed ? t + this.lastChar.toString("base64", 0, 3 - this.lastNeed) : t;
}
function g(e) {
return e.toString(this.encoding);
}
function y(e) {
return e && e.length ? this.write(e) : "";
}
}, {
"safe-buffer": 62
} ],
64: [ function(e, t) {
(function(e) {
t.exports = function(t, i) {
for (var n = Math.min(t.length, i.length), r = new e(n), o = 0; o < n; ++o) r[o] = t[o] ^ i[o];
return r;
};
}).call(this, e("buffer").Buffer);
}, {
buffer: 65
} ],
65: [ function(e, t, i) {
(function(t) {
"use strict";
var n = e("base64-js"), r = e("ieee754"), o = e("isarray");
i.Buffer = c;
i.SlowBuffer = function(e) {
+e != e && (e = 0);
return c.alloc(+e);
};
i.INSPECT_MAX_BYTES = 50;
c.TYPED_ARRAY_SUPPORT = void 0 !== t.TYPED_ARRAY_SUPPORT ? t.TYPED_ARRAY_SUPPORT : function() {
try {
var e = new Uint8Array(1);
e.__proto__ = {
__proto__: Uint8Array.prototype,
foo: function() {
return 42;
}
};
return 42 === e.foo() && "function" == typeof e.subarray && 0 === e.subarray(1, 1).byteLength;
} catch (e) {
return !1;
}
}();
i.kMaxLength = a();
function a() {
return c.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823;
}
function s(e, t) {
if (a() < t) throw new RangeError("Invalid typed array length");
if (c.TYPED_ARRAY_SUPPORT) (e = new Uint8Array(t)).__proto__ = c.prototype; else {
null === e && (e = new c(t));
e.length = t;
}
return e;
}
function c(e, t, i) {
if (!(c.TYPED_ARRAY_SUPPORT || this instanceof c)) return new c(e, t, i);
if ("number" == typeof e) {
if ("string" == typeof t) throw new Error("If encoding is specified then the first argument must be a string");
return f(this, e);
}
return l(this, e, t, i);
}
c.poolSize = 8192;
c._augment = function(e) {
e.__proto__ = c.prototype;
return e;
};
function l(e, t, i, n) {
if ("number" == typeof t) throw new TypeError('"value" argument must not be a number');
return "undefined" != typeof ArrayBuffer && t instanceof ArrayBuffer ? m(e, t, i, n) : "string" == typeof t ? d(e, t, i) : g(e, t);
}
c.from = function(e, t, i) {
return l(null, e, t, i);
};
if (c.TYPED_ARRAY_SUPPORT) {
c.prototype.__proto__ = Uint8Array.prototype;
c.__proto__ = Uint8Array;
"undefined" != typeof Symbol && Symbol.species && c[Symbol.species] === c && Object.defineProperty(c, Symbol.species, {
value: null,
configurable: !0
});
}
function h(e) {
if ("number" != typeof e) throw new TypeError('"size" argument must be a number');
if (e < 0) throw new RangeError('"size" argument must not be negative');
}
function u(e, t, i, n) {
h(t);
return t <= 0 ? s(e, t) : void 0 !== i ? "string" == typeof n ? s(e, t).fill(i, n) : s(e, t).fill(i) : s(e, t);
}
c.alloc = function(e, t, i) {
return u(null, e, t, i);
};
function f(e, t) {
h(t);
e = s(e, t < 0 ? 0 : 0 | y(t));
if (!c.TYPED_ARRAY_SUPPORT) for (var i = 0; i < t; ++i) e[i] = 0;
return e;
}
c.allocUnsafe = function(e) {
return f(null, e);
};
c.allocUnsafeSlow = function(e) {
return f(null, e);
};
function d(e, t, i) {
"string" == typeof i && "" !== i || (i = "utf8");
if (!c.isEncoding(i)) throw new TypeError('"encoding" must be a valid string encoding');
var n = 0 | b(t, i), r = (e = s(e, n)).write(t, i);
r !== n && (e = e.slice(0, r));
return e;
}
function p(e, t) {
var i = t.length < 0 ? 0 : 0 | y(t.length);
e = s(e, i);
for (var n = 0; n < i; n += 1) e[n] = 255 & t[n];
return e;
}
function m(e, t, i, n) {
t.byteLength;
if (i < 0 || t.byteLength < i) throw new RangeError("'offset' is out of bounds");
if (t.byteLength < i + (n || 0)) throw new RangeError("'length' is out of bounds");
t = void 0 === i && void 0 === n ? new Uint8Array(t) : void 0 === n ? new Uint8Array(t, i) : new Uint8Array(t, i, n);
c.TYPED_ARRAY_SUPPORT ? (e = t).__proto__ = c.prototype : e = p(e, t);
return e;
}
function g(e, t) {
if (c.isBuffer(t)) {
var i = 0 | y(t.length);
if (0 === (e = s(e, i)).length) return e;
t.copy(e, 0, 0, i);
return e;
}
if (t) {
if ("undefined" != typeof ArrayBuffer && t.buffer instanceof ArrayBuffer || "length" in t) return "number" != typeof t.length || (n = t.length) != n ? s(e, 0) : p(e, t);
if ("Buffer" === t.type && o(t.data)) return p(e, t.data);
}
var n;
throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
}
function y(e) {
if (e >= a()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + a().toString(16) + " bytes");
return 0 | e;
}
c.isBuffer = function(e) {
return !(null == e || !e._isBuffer);
};
c.compare = function(e, t) {
if (!c.isBuffer(e) || !c.isBuffer(t)) throw new TypeError("Arguments must be Buffers");
if (e === t) return 0;
for (var i = e.length, n = t.length, r = 0, o = Math.min(i, n); r < o; ++r) if (e[r] !== t[r]) {
i = e[r];
n = t[r];
break;
}
return i < n ? -1 : n < i ? 1 : 0;
};
c.isEncoding = function(e) {
switch (String(e).toLowerCase()) {
case "hex":
case "utf8":
case "utf-8":
case "ascii":
case "latin1":
case "binary":
case "base64":
case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return !0;

default:
return !1;
}
};
c.concat = function(e, t) {
if (!o(e)) throw new TypeError('"list" argument must be an Array of Buffers');
if (0 === e.length) return c.alloc(0);
var i;
if (void 0 === t) {
t = 0;
for (i = 0; i < e.length; ++i) t += e[i].length;
}
var n = c.allocUnsafe(t), r = 0;
for (i = 0; i < e.length; ++i) {
var a = e[i];
if (!c.isBuffer(a)) throw new TypeError('"list" argument must be an Array of Buffers');
a.copy(n, r);
r += a.length;
}
return n;
};
function b(e, t) {
if (c.isBuffer(e)) return e.length;
if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(e) || e instanceof ArrayBuffer)) return e.byteLength;
"string" != typeof e && (e = "" + e);
var i = e.length;
if (0 === i) return 0;
for (var n = !1; ;) switch (t) {
case "ascii":
case "latin1":
case "binary":
return i;

case "utf8":
case "utf-8":
case void 0:
return X(e).length;

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return 2 * i;

case "hex":
return i >>> 1;

case "base64":
return Y(e).length;

default:
if (n) return X(e).length;
t = ("" + t).toLowerCase();
n = !0;
}
}
c.byteLength = b;
function v(e, t, i) {
var n = !1;
(void 0 === t || t < 0) && (t = 0);
if (t > this.length) return "";
(void 0 === i || i > this.length) && (i = this.length);
if (i <= 0) return "";
if ((i >>>= 0) <= (t >>>= 0)) return "";
e || (e = "utf8");
for (;;) switch (e) {
case "hex":
return I(this, t, i);

case "utf8":
case "utf-8":
return D(this, t, i);

case "ascii":
return P(this, t, i);

case "latin1":
case "binary":
return R(this, t, i);

case "base64":
return T(this, t, i);

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return N(this, t, i);

default:
if (n) throw new TypeError("Unknown encoding: " + e);
e = (e + "").toLowerCase();
n = !0;
}
}
c.prototype._isBuffer = !0;
function _(e, t, i) {
var n = e[t];
e[t] = e[i];
e[i] = n;
}
c.prototype.swap16 = function() {
var e = this.length;
if (e % 2 != 0) throw new RangeError("Buffer size must be a multiple of 16-bits");
for (var t = 0; t < e; t += 2) _(this, t, t + 1);
return this;
};
c.prototype.swap32 = function() {
var e = this.length;
if (e % 4 != 0) throw new RangeError("Buffer size must be a multiple of 32-bits");
for (var t = 0; t < e; t += 4) {
_(this, t, t + 3);
_(this, t + 1, t + 2);
}
return this;
};
c.prototype.swap64 = function() {
var e = this.length;
if (e % 8 != 0) throw new RangeError("Buffer size must be a multiple of 64-bits");
for (var t = 0; t < e; t += 8) {
_(this, t, t + 7);
_(this, t + 1, t + 6);
_(this, t + 2, t + 5);
_(this, t + 3, t + 4);
}
return this;
};
c.prototype.toString = function() {
var e = 0 | this.length;
return 0 === e ? "" : 0 === arguments.length ? D(this, 0, e) : v.apply(this, arguments);
};
c.prototype.equals = function(e) {
if (!c.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
return this === e || 0 === c.compare(this, e);
};
c.prototype.inspect = function() {
var e = "", t = i.INSPECT_MAX_BYTES;
if (this.length > 0) {
e = this.toString("hex", 0, t).match(/.{2}/g).join(" ");
this.length > t && (e += " ... ");
}
return "<Buffer " + e + ">";
};
c.prototype.compare = function(e, t, i, n, r) {
if (!c.isBuffer(e)) throw new TypeError("Argument must be a Buffer");
void 0 === t && (t = 0);
void 0 === i && (i = e ? e.length : 0);
void 0 === n && (n = 0);
void 0 === r && (r = this.length);
if (t < 0 || i > e.length || n < 0 || r > this.length) throw new RangeError("out of range index");
if (n >= r && t >= i) return 0;
if (n >= r) return -1;
if (t >= i) return 1;
if (this === e) return 0;
for (var o = (r >>>= 0) - (n >>>= 0), a = (i >>>= 0) - (t >>>= 0), s = Math.min(o, a), l = this.slice(n, r), h = e.slice(t, i), u = 0; u < s; ++u) if (l[u] !== h[u]) {
o = l[u];
a = h[u];
break;
}
return o < a ? -1 : a < o ? 1 : 0;
};
function w(e, t, i, n, r) {
if (0 === e.length) return -1;
if ("string" == typeof i) {
n = i;
i = 0;
} else i > 2147483647 ? i = 2147483647 : i < -2147483648 && (i = -2147483648);
i = +i;
isNaN(i) && (i = r ? 0 : e.length - 1);
i < 0 && (i = e.length + i);
if (i >= e.length) {
if (r) return -1;
i = e.length - 1;
} else if (i < 0) {
if (!r) return -1;
i = 0;
}
"string" == typeof t && (t = c.from(t, n));
if (c.isBuffer(t)) return 0 === t.length ? -1 : C(e, t, i, n, r);
if ("number" == typeof t) {
t &= 255;
return c.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? r ? Uint8Array.prototype.indexOf.call(e, t, i) : Uint8Array.prototype.lastIndexOf.call(e, t, i) : C(e, [ t ], i, n, r);
}
throw new TypeError("val must be string, number or Buffer");
}
function C(e, t, i, n, r) {
var o, a = 1, s = e.length, c = t.length;
if (void 0 !== n && ("ucs2" === (n = String(n).toLowerCase()) || "ucs-2" === n || "utf16le" === n || "utf-16le" === n)) {
if (e.length < 2 || t.length < 2) return -1;
a = 2;
s /= 2;
c /= 2;
i /= 2;
}
function l(e, t) {
return 1 === a ? e[t] : e.readUInt16BE(t * a);
}
if (r) {
var h = -1;
for (o = i; o < s; o++) if (l(e, o) === l(t, -1 === h ? 0 : o - h)) {
-1 === h && (h = o);
if (o - h + 1 === c) return h * a;
} else {
-1 !== h && (o -= o - h);
h = -1;
}
} else {
i + c > s && (i = s - c);
for (o = i; o >= 0; o--) {
for (var u = !0, f = 0; f < c; f++) if (l(e, o + f) !== l(t, f)) {
u = !1;
break;
}
if (u) return o;
}
}
return -1;
}
c.prototype.includes = function(e, t, i) {
return -1 !== this.indexOf(e, t, i);
};
c.prototype.indexOf = function(e, t, i) {
return w(this, e, t, i, !0);
};
c.prototype.lastIndexOf = function(e, t, i) {
return w(this, e, t, i, !1);
};
function S(e, t, i, n) {
i = Number(i) || 0;
var r = e.length - i;
n ? (n = Number(n)) > r && (n = r) : n = r;
var o = t.length;
if (o % 2 != 0) throw new TypeError("Invalid hex string");
n > o / 2 && (n = o / 2);
for (var a = 0; a < n; ++a) {
var s = parseInt(t.substr(2 * a, 2), 16);
if (isNaN(s)) return a;
e[i + a] = s;
}
return a;
}
function A(e, t, i, n) {
return Z(X(t, e.length - i), e, i, n);
}
function M(e, t, i, n) {
return Z(K(t), e, i, n);
}
function B(e, t, i, n) {
return M(e, t, i, n);
}
function x(e, t, i, n) {
return Z(Y(t), e, i, n);
}
function k(e, t, i, n) {
return Z(J(t, e.length - i), e, i, n);
}
c.prototype.write = function(e, t, i, n) {
if (void 0 === t) {
n = "utf8";
i = this.length;
t = 0;
} else if (void 0 === i && "string" == typeof t) {
n = t;
i = this.length;
t = 0;
} else {
if (!isFinite(t)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
t |= 0;
if (isFinite(i)) {
i |= 0;
void 0 === n && (n = "utf8");
} else {
n = i;
i = void 0;
}
}
var r = this.length - t;
(void 0 === i || i > r) && (i = r);
if (e.length > 0 && (i < 0 || t < 0) || t > this.length) throw new RangeError("Attempt to write outside buffer bounds");
n || (n = "utf8");
for (var o = !1; ;) switch (n) {
case "hex":
return S(this, e, t, i);

case "utf8":
case "utf-8":
return A(this, e, t, i);

case "ascii":
return M(this, e, t, i);

case "latin1":
case "binary":
return B(this, e, t, i);

case "base64":
return x(this, e, t, i);

case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
return k(this, e, t, i);

default:
if (o) throw new TypeError("Unknown encoding: " + n);
n = ("" + n).toLowerCase();
o = !0;
}
};
c.prototype.toJSON = function() {
return {
type: "Buffer",
data: Array.prototype.slice.call(this._arr || this, 0)
};
};
function T(e, t, i) {
return 0 === t && i === e.length ? n.fromByteArray(e) : n.fromByteArray(e.slice(t, i));
}
function D(e, t, i) {
i = Math.min(e.length, i);
for (var n = [], r = t; r < i; ) {
var o = e[r], a = null, s = o > 239 ? 4 : o > 223 ? 3 : o > 191 ? 2 : 1;
if (r + s <= i) {
var c, l, h, u;
switch (s) {
case 1:
o < 128 && (a = o);
break;

case 2:
128 == (192 & (c = e[r + 1])) && (u = (31 & o) << 6 | 63 & c) > 127 && (a = u);
break;

case 3:
c = e[r + 1];
l = e[r + 2];
128 == (192 & c) && 128 == (192 & l) && (u = (15 & o) << 12 | (63 & c) << 6 | 63 & l) > 2047 && (u < 55296 || u > 57343) && (a = u);
break;

case 4:
c = e[r + 1];
l = e[r + 2];
h = e[r + 3];
128 == (192 & c) && 128 == (192 & l) && 128 == (192 & h) && (u = (15 & o) << 18 | (63 & c) << 12 | (63 & l) << 6 | 63 & h) > 65535 && u < 1114112 && (a = u);
}
}
if (null === a) {
a = 65533;
s = 1;
} else if (a > 65535) {
a -= 65536;
n.push(a >>> 10 & 1023 | 55296);
a = 56320 | 1023 & a;
}
n.push(a);
r += s;
}
return L(n);
}
var E = 4096;
function L(e) {
var t = e.length;
if (t <= E) return String.fromCharCode.apply(String, e);
for (var i = "", n = 0; n < t; ) i += String.fromCharCode.apply(String, e.slice(n, n += E));
return i;
}
function P(e, t, i) {
var n = "";
i = Math.min(e.length, i);
for (var r = t; r < i; ++r) n += String.fromCharCode(127 & e[r]);
return n;
}
function R(e, t, i) {
var n = "";
i = Math.min(e.length, i);
for (var r = t; r < i; ++r) n += String.fromCharCode(e[r]);
return n;
}
function I(e, t, i) {
var n, r = e.length;
(!t || t < 0) && (t = 0);
(!i || i < 0 || i > r) && (i = r);
for (var o = "", a = t; a < i; ++a) o += (n = e[a]) < 16 ? "0" + n.toString(16) : n.toString(16);
return o;
}
function N(e, t, i) {
for (var n = e.slice(t, i), r = "", o = 0; o < n.length; o += 2) r += String.fromCharCode(n[o] + 256 * n[o + 1]);
return r;
}
c.prototype.slice = function(e, t) {
var i, n = this.length;
(e = ~~e) < 0 ? (e += n) < 0 && (e = 0) : e > n && (e = n);
(t = void 0 === t ? n : ~~t) < 0 ? (t += n) < 0 && (t = 0) : t > n && (t = n);
t < e && (t = e);
if (c.TYPED_ARRAY_SUPPORT) (i = this.subarray(e, t)).__proto__ = c.prototype; else {
var r = t - e;
i = new c(r, void 0);
for (var o = 0; o < r; ++o) i[o] = this[o + e];
}
return i;
};
function O(e, t, i) {
if (e % 1 != 0 || e < 0) throw new RangeError("offset is not uint");
if (e + t > i) throw new RangeError("Trying to access beyond buffer length");
}
c.prototype.readUIntLE = function(e, t, i) {
e |= 0;
t |= 0;
i || O(e, t, this.length);
for (var n = this[e], r = 1, o = 0; ++o < t && (r *= 256); ) n += this[e + o] * r;
return n;
};
c.prototype.readUIntBE = function(e, t, i) {
e |= 0;
t |= 0;
i || O(e, t, this.length);
for (var n = this[e + --t], r = 1; t > 0 && (r *= 256); ) n += this[e + --t] * r;
return n;
};
c.prototype.readUInt8 = function(e, t) {
t || O(e, 1, this.length);
return this[e];
};
c.prototype.readUInt16LE = function(e, t) {
t || O(e, 2, this.length);
return this[e] | this[e + 1] << 8;
};
c.prototype.readUInt16BE = function(e, t) {
t || O(e, 2, this.length);
return this[e] << 8 | this[e + 1];
};
c.prototype.readUInt32LE = function(e, t) {
t || O(e, 4, this.length);
return (this[e] | this[e + 1] << 8 | this[e + 2] << 16) + 16777216 * this[e + 3];
};
c.prototype.readUInt32BE = function(e, t) {
t || O(e, 4, this.length);
return 16777216 * this[e] + (this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3]);
};
c.prototype.readIntLE = function(e, t, i) {
e |= 0;
t |= 0;
i || O(e, t, this.length);
for (var n = this[e], r = 1, o = 0; ++o < t && (r *= 256); ) n += this[e + o] * r;
n >= (r *= 128) && (n -= Math.pow(2, 8 * t));
return n;
};
c.prototype.readIntBE = function(e, t, i) {
e |= 0;
t |= 0;
i || O(e, t, this.length);
for (var n = t, r = 1, o = this[e + --n]; n > 0 && (r *= 256); ) o += this[e + --n] * r;
o >= (r *= 128) && (o -= Math.pow(2, 8 * t));
return o;
};
c.prototype.readInt8 = function(e, t) {
t || O(e, 1, this.length);
return 128 & this[e] ? -1 * (255 - this[e] + 1) : this[e];
};
c.prototype.readInt16LE = function(e, t) {
t || O(e, 2, this.length);
var i = this[e] | this[e + 1] << 8;
return 32768 & i ? 4294901760 | i : i;
};
c.prototype.readInt16BE = function(e, t) {
t || O(e, 2, this.length);
var i = this[e + 1] | this[e] << 8;
return 32768 & i ? 4294901760 | i : i;
};
c.prototype.readInt32LE = function(e, t) {
t || O(e, 4, this.length);
return this[e] | this[e + 1] << 8 | this[e + 2] << 16 | this[e + 3] << 24;
};
c.prototype.readInt32BE = function(e, t) {
t || O(e, 4, this.length);
return this[e] << 24 | this[e + 1] << 16 | this[e + 2] << 8 | this[e + 3];
};
c.prototype.readFloatLE = function(e, t) {
t || O(e, 4, this.length);
return r.read(this, e, !0, 23, 4);
};
c.prototype.readFloatBE = function(e, t) {
t || O(e, 4, this.length);
return r.read(this, e, !1, 23, 4);
};
c.prototype.readDoubleLE = function(e, t) {
t || O(e, 8, this.length);
return r.read(this, e, !0, 52, 8);
};
c.prototype.readDoubleBE = function(e, t) {
t || O(e, 8, this.length);
return r.read(this, e, !1, 52, 8);
};
function U(e, t, i, n, r, o) {
if (!c.isBuffer(e)) throw new TypeError('"buffer" argument must be a Buffer instance');
if (t > r || t < o) throw new RangeError('"value" argument is out of bounds');
if (i + n > e.length) throw new RangeError("Index out of range");
}
c.prototype.writeUIntLE = function(e, t, i, n) {
e = +e;
t |= 0;
i |= 0;
n || U(this, e, t, i, Math.pow(2, 8 * i) - 1, 0);
var r = 1, o = 0;
this[t] = 255 & e;
for (;++o < i && (r *= 256); ) this[t + o] = e / r & 255;
return t + i;
};
c.prototype.writeUIntBE = function(e, t, i, n) {
e = +e;
t |= 0;
i |= 0;
n || U(this, e, t, i, Math.pow(2, 8 * i) - 1, 0);
var r = i - 1, o = 1;
this[t + r] = 255 & e;
for (;--r >= 0 && (o *= 256); ) this[t + r] = e / o & 255;
return t + i;
};
c.prototype.writeUInt8 = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 1, 255, 0);
c.TYPED_ARRAY_SUPPORT || (e = Math.floor(e));
this[t] = 255 & e;
return t + 1;
};
function j(e, t, i, n) {
t < 0 && (t = 65535 + t + 1);
for (var r = 0, o = Math.min(e.length - i, 2); r < o; ++r) e[i + r] = (t & 255 << 8 * (n ? r : 1 - r)) >>> 8 * (n ? r : 1 - r);
}
c.prototype.writeUInt16LE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 2, 65535, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = 255 & e;
this[t + 1] = e >>> 8;
} else j(this, e, t, !0);
return t + 2;
};
c.prototype.writeUInt16BE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 2, 65535, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 8;
this[t + 1] = 255 & e;
} else j(this, e, t, !1);
return t + 2;
};
function F(e, t, i, n) {
t < 0 && (t = 4294967295 + t + 1);
for (var r = 0, o = Math.min(e.length - i, 4); r < o; ++r) e[i + r] = t >>> 8 * (n ? r : 3 - r) & 255;
}
c.prototype.writeUInt32LE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 4, 4294967295, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t + 3] = e >>> 24;
this[t + 2] = e >>> 16;
this[t + 1] = e >>> 8;
this[t] = 255 & e;
} else F(this, e, t, !0);
return t + 4;
};
c.prototype.writeUInt32BE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 4, 4294967295, 0);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 24;
this[t + 1] = e >>> 16;
this[t + 2] = e >>> 8;
this[t + 3] = 255 & e;
} else F(this, e, t, !1);
return t + 4;
};
c.prototype.writeIntLE = function(e, t, i, n) {
e = +e;
t |= 0;
if (!n) {
var r = Math.pow(2, 8 * i - 1);
U(this, e, t, i, r - 1, -r);
}
var o = 0, a = 1, s = 0;
this[t] = 255 & e;
for (;++o < i && (a *= 256); ) {
e < 0 && 0 === s && 0 !== this[t + o - 1] && (s = 1);
this[t + o] = (e / a >> 0) - s & 255;
}
return t + i;
};
c.prototype.writeIntBE = function(e, t, i, n) {
e = +e;
t |= 0;
if (!n) {
var r = Math.pow(2, 8 * i - 1);
U(this, e, t, i, r - 1, -r);
}
var o = i - 1, a = 1, s = 0;
this[t + o] = 255 & e;
for (;--o >= 0 && (a *= 256); ) {
e < 0 && 0 === s && 0 !== this[t + o + 1] && (s = 1);
this[t + o] = (e / a >> 0) - s & 255;
}
return t + i;
};
c.prototype.writeInt8 = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 1, 127, -128);
c.TYPED_ARRAY_SUPPORT || (e = Math.floor(e));
e < 0 && (e = 255 + e + 1);
this[t] = 255 & e;
return t + 1;
};
c.prototype.writeInt16LE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 2, 32767, -32768);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = 255 & e;
this[t + 1] = e >>> 8;
} else j(this, e, t, !0);
return t + 2;
};
c.prototype.writeInt16BE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 2, 32767, -32768);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 8;
this[t + 1] = 255 & e;
} else j(this, e, t, !1);
return t + 2;
};
c.prototype.writeInt32LE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 4, 2147483647, -2147483648);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = 255 & e;
this[t + 1] = e >>> 8;
this[t + 2] = e >>> 16;
this[t + 3] = e >>> 24;
} else F(this, e, t, !0);
return t + 4;
};
c.prototype.writeInt32BE = function(e, t, i) {
e = +e;
t |= 0;
i || U(this, e, t, 4, 2147483647, -2147483648);
e < 0 && (e = 4294967295 + e + 1);
if (c.TYPED_ARRAY_SUPPORT) {
this[t] = e >>> 24;
this[t + 1] = e >>> 16;
this[t + 2] = e >>> 8;
this[t + 3] = 255 & e;
} else F(this, e, t, !1);
return t + 4;
};
function G(e, t, i, n) {
if (i + n > e.length) throw new RangeError("Index out of range");
if (i < 0) throw new RangeError("Index out of range");
}
function V(e, t, i, n, o) {
o || G(e, 0, i, 4);
r.write(e, t, i, n, 23, 4);
return i + 4;
}
c.prototype.writeFloatLE = function(e, t, i) {
return V(this, e, t, !0, i);
};
c.prototype.writeFloatBE = function(e, t, i) {
return V(this, e, t, !1, i);
};
function z(e, t, i, n, o) {
o || G(e, 0, i, 8);
r.write(e, t, i, n, 52, 8);
return i + 8;
}
c.prototype.writeDoubleLE = function(e, t, i) {
return z(this, e, t, !0, i);
};
c.prototype.writeDoubleBE = function(e, t, i) {
return z(this, e, t, !1, i);
};
c.prototype.copy = function(e, t, i, n) {
i || (i = 0);
n || 0 === n || (n = this.length);
t >= e.length && (t = e.length);
t || (t = 0);
n > 0 && n < i && (n = i);
if (n === i) return 0;
if (0 === e.length || 0 === this.length) return 0;
if (t < 0) throw new RangeError("targetStart out of bounds");
if (i < 0 || i >= this.length) throw new RangeError("sourceStart out of bounds");
if (n < 0) throw new RangeError("sourceEnd out of bounds");
n > this.length && (n = this.length);
e.length - t < n - i && (n = e.length - t + i);
var r, o = n - i;
if (this === e && i < t && t < n) for (r = o - 1; r >= 0; --r) e[r + t] = this[r + i]; else if (o < 1e3 || !c.TYPED_ARRAY_SUPPORT) for (r = 0; r < o; ++r) e[r + t] = this[r + i]; else Uint8Array.prototype.set.call(e, this.subarray(i, i + o), t);
return o;
};
c.prototype.fill = function(e, t, i, n) {
if ("string" == typeof e) {
if ("string" == typeof t) {
n = t;
t = 0;
i = this.length;
} else if ("string" == typeof i) {
n = i;
i = this.length;
}
if (1 === e.length) {
var r = e.charCodeAt(0);
r < 256 && (e = r);
}
if (void 0 !== n && "string" != typeof n) throw new TypeError("encoding must be a string");
if ("string" == typeof n && !c.isEncoding(n)) throw new TypeError("Unknown encoding: " + n);
} else "number" == typeof e && (e &= 255);
if (t < 0 || this.length < t || this.length < i) throw new RangeError("Out of range index");
if (i <= t) return this;
t >>>= 0;
i = void 0 === i ? this.length : i >>> 0;
e || (e = 0);
var o;
if ("number" == typeof e) for (o = t; o < i; ++o) this[o] = e; else {
var a = c.isBuffer(e) ? e : X(new c(e, n).toString()), s = a.length;
for (o = 0; o < i - t; ++o) this[o + t] = a[o % s];
}
return this;
};
var H = /[^+\/0-9A-Za-z-_]/g;
function W(e) {
if ((e = q(e).replace(H, "")).length < 2) return "";
for (;e.length % 4 != 0; ) e += "=";
return e;
}
function q(e) {
return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "");
}
function X(e, t) {
t = t || Infinity;
for (var i, n = e.length, r = null, o = [], a = 0; a < n; ++a) {
if ((i = e.charCodeAt(a)) > 55295 && i < 57344) {
if (!r) {
if (i > 56319) {
(t -= 3) > -1 && o.push(239, 191, 189);
continue;
}
if (a + 1 === n) {
(t -= 3) > -1 && o.push(239, 191, 189);
continue;
}
r = i;
continue;
}
if (i < 56320) {
(t -= 3) > -1 && o.push(239, 191, 189);
r = i;
continue;
}
i = 65536 + (r - 55296 << 10 | i - 56320);
} else r && (t -= 3) > -1 && o.push(239, 191, 189);
r = null;
if (i < 128) {
if ((t -= 1) < 0) break;
o.push(i);
} else if (i < 2048) {
if ((t -= 2) < 0) break;
o.push(i >> 6 | 192, 63 & i | 128);
} else if (i < 65536) {
if ((t -= 3) < 0) break;
o.push(i >> 12 | 224, i >> 6 & 63 | 128, 63 & i | 128);
} else {
if (!(i < 1114112)) throw new Error("Invalid code point");
if ((t -= 4) < 0) break;
o.push(i >> 18 | 240, i >> 12 & 63 | 128, i >> 6 & 63 | 128, 63 & i | 128);
}
}
return o;
}
function K(e) {
for (var t = [], i = 0; i < e.length; ++i) t.push(255 & e.charCodeAt(i));
return t;
}
function J(e, t) {
for (var i, n, r, o = [], a = 0; a < e.length && !((t -= 2) < 0); ++a) {
n = (i = e.charCodeAt(a)) >> 8;
r = i % 256;
o.push(r);
o.push(n);
}
return o;
}
function Y(e) {
return n.toByteArray(W(e));
}
function Z(e, t, i, n) {
for (var r = 0; r < n && !(r + i >= t.length || r >= e.length); ++r) t[r + i] = e[r];
return r;
}
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"base64-js": 16,
ieee754: 137,
isarray: 66
} ],
66: [ function(e, t) {
var i = {}.toString;
t.exports = Array.isArray || function(e) {
return "[object Array]" == i.call(e);
};
}, {} ],
67: [ function(e, t) {
var i = e("safe-buffer").Buffer, n = e("stream").Transform, r = e("string_decoder").StringDecoder;
function o(e) {
n.call(this);
this.hashMode = "string" == typeof e;
this.hashMode ? this[e] = this._finalOrDigest : this.final = this._finalOrDigest;
if (this._final) {
this.__final = this._final;
this._final = null;
}
this._decoder = null;
this._encoding = null;
}
e("inherits")(o, n);
o.prototype.update = function(e, t, n) {
"string" == typeof e && (e = i.from(e, t));
var r = this._update(e);
if (this.hashMode) return this;
n && (r = this._toString(r, n));
return r;
};
o.prototype.setAutoPadding = function() {};
o.prototype.getAuthTag = function() {
throw new Error("trying to get auth tag in unsupported state");
};
o.prototype.setAuthTag = function() {
throw new Error("trying to set auth tag in unsupported state");
};
o.prototype.setAAD = function() {
throw new Error("trying to set aad in unsupported state");
};
o.prototype._transform = function(e, t, i) {
var n;
try {
this.hashMode ? this._update(e) : this.push(this._update(e));
} catch (e) {
n = e;
} finally {
i(n);
}
};
o.prototype._flush = function(e) {
var t;
try {
this.push(this.__final());
} catch (e) {
t = e;
}
e(t);
};
o.prototype._finalOrDigest = function(e) {
var t = this.__final() || i.alloc(0);
e && (t = this._toString(t, e, !0));
return t;
};
o.prototype._toString = function(e, t, i) {
if (!this._decoder) {
this._decoder = new r(t);
this._encoding = t;
}
if (this._encoding !== t) throw new Error("can't switch encodings");
var n = this._decoder.write(e);
i && (n += this._decoder.end());
return n;
};
t.exports = o;
}, {
inherits: 138,
"safe-buffer": 182,
stream: 192,
string_decoder: 193
} ],
68: [ function(e, t, i) {
i.isArray = function(e) {
return Array.isArray ? Array.isArray(e) : "[object Array]" === n(e);
};
i.isBoolean = function(e) {
return "boolean" == typeof e;
};
i.isNull = function(e) {
return null === e;
};
i.isNullOrUndefined = function(e) {
return null == e;
};
i.isNumber = function(e) {
return "number" == typeof e;
};
i.isString = function(e) {
return "string" == typeof e;
};
i.isSymbol = function(e) {
return "symbol" == typeof e;
};
i.isUndefined = function(e) {
return void 0 === e;
};
i.isRegExp = function(e) {
return "[object RegExp]" === n(e);
};
i.isObject = function(e) {
return "object" == typeof e && null !== e;
};
i.isDate = function(e) {
return "[object Date]" === n(e);
};
i.isError = function(e) {
return "[object Error]" === n(e) || e instanceof Error;
};
i.isFunction = function(e) {
return "function" == typeof e;
};
i.isPrimitive = function(e) {
return null === e || "boolean" == typeof e || "number" == typeof e || "string" == typeof e || "symbol" == typeof e || "undefined" == typeof e;
};
i.isBuffer = e("buffer").Buffer.isBuffer;
function n(e) {
return Object.prototype.toString.call(e);
}
}, {
buffer: 65
} ],
69: [ function(e, t) {
(function(i) {
var n = e("elliptic"), r = e("bn.js");
t.exports = function(e) {
return new a(e);
};
var o = {
secp256k1: {
name: "secp256k1",
byteLength: 32
},
secp224r1: {
name: "p224",
byteLength: 28
},
prime256v1: {
name: "p256",
byteLength: 32
},
prime192v1: {
name: "p192",
byteLength: 24
},
ed25519: {
name: "ed25519",
byteLength: 32
},
secp384r1: {
name: "p384",
byteLength: 48
},
secp521r1: {
name: "p521",
byteLength: 66
}
};
o.p224 = o.secp224r1;
o.p256 = o.secp256r1 = o.prime256v1;
o.p192 = o.secp192r1 = o.prime192v1;
o.p384 = o.secp384r1;
o.p521 = o.secp521r1;
function a(e) {
this.curveType = o[e];
this.curveType || (this.curveType = {
name: e
});
this.curve = new n.ec(this.curveType.name);
this.keys = void 0;
}
a.prototype.generateKeys = function(e, t) {
this.keys = this.curve.genKeyPair();
return this.getPublicKey(e, t);
};
a.prototype.computeSecret = function(e, t, n) {
t = t || "utf8";
i.isBuffer(e) || (e = new i(e, t));
return s(this.curve.keyFromPublic(e).getPublic().mul(this.keys.getPrivate()).getX(), n, this.curveType.byteLength);
};
a.prototype.getPublicKey = function(e, t) {
var i = this.keys.getPublic("compressed" === t, !0);
"hybrid" === t && (i[i.length - 1] % 2 ? i[0] = 7 : i[0] = 6);
return s(i, e);
};
a.prototype.getPrivateKey = function(e) {
return s(this.keys.getPrivate(), e);
};
a.prototype.setPublicKey = function(e, t) {
t = t || "utf8";
i.isBuffer(e) || (e = new i(e, t));
this.keys._importPublic(e);
return this;
};
a.prototype.setPrivateKey = function(e, t) {
t = t || "utf8";
i.isBuffer(e) || (e = new i(e, t));
var n = new r(e);
n = n.toString(16);
this.keys = this.curve.genKeyPair();
this.keys._importPrivate(n);
return this;
};
function s(e, t, n) {
Array.isArray(e) || (e = e.toArray());
var r = new i(e);
if (n && r.length < n) {
var o = new i(n - r.length);
o.fill(0);
r = i.concat([ o, r ]);
}
return t ? r.toString(t) : r;
}
}).call(this, e("buffer").Buffer);
}, {
"bn.js": 70,
buffer: 65,
elliptic: 87
} ],
70: [ function(e, t, i) {
arguments[4][15][0].apply(i, arguments);
}, {
buffer: 19,
dup: 15
} ],
71: [ function(e, t) {
"use strict";
var i = e("inherits"), n = e("md5.js"), r = e("ripemd160"), o = e("sha.js"), a = e("cipher-base");
function s(e) {
a.call(this, "digest");
this._hash = e;
}
i(s, a);
s.prototype._update = function(e) {
this._hash.update(e);
};
s.prototype._final = function() {
return this._hash.digest();
};
t.exports = function(e) {
return "md5" === (e = e.toLowerCase()) ? new n() : "rmd160" === e || "ripemd160" === e ? new r() : new s(o(e));
};
}, {
"cipher-base": 67,
inherits: 138,
"md5.js": 139,
ripemd160: 181,
"sha.js": 185
} ],
72: [ function(e, t) {
var i = e("md5.js");
t.exports = function(e) {
return new i().update(e).digest();
};
}, {
"md5.js": 139
} ],
73: [ function(e, t) {
"use strict";
var i = e("inherits"), n = e("./legacy"), r = e("cipher-base"), o = e("safe-buffer").Buffer, a = e("create-hash/md5"), s = e("ripemd160"), c = e("sha.js"), l = o.alloc(128);
function h(e, t) {
r.call(this, "digest");
"string" == typeof t && (t = o.from(t));
var i = "sha512" === e || "sha384" === e ? 128 : 64;
this._alg = e;
this._key = t;
t.length > i ? t = ("rmd160" === e ? new s() : c(e)).update(t).digest() : t.length < i && (t = o.concat([ t, l ], i));
for (var n = this._ipad = o.allocUnsafe(i), a = this._opad = o.allocUnsafe(i), h = 0; h < i; h++) {
n[h] = 54 ^ t[h];
a[h] = 92 ^ t[h];
}
this._hash = "rmd160" === e ? new s() : c(e);
this._hash.update(n);
}
i(h, r);
h.prototype._update = function(e) {
this._hash.update(e);
};
h.prototype._final = function() {
var e = this._hash.digest();
return ("rmd160" === this._alg ? new s() : c(this._alg)).update(this._opad).update(e).digest();
};
t.exports = function(e, t) {
return "rmd160" === (e = e.toLowerCase()) || "ripemd160" === e ? new h("rmd160", t) : "md5" === e ? new n(a, t) : new h(e, t);
};
}, {
"./legacy": 74,
"cipher-base": 67,
"create-hash/md5": 72,
inherits: 138,
ripemd160: 181,
"safe-buffer": 182,
"sha.js": 185
} ],
74: [ function(e, t) {
"use strict";
var i = e("inherits"), n = e("safe-buffer").Buffer, r = e("cipher-base"), o = n.alloc(128), a = 64;
function s(e, t) {
r.call(this, "digest");
"string" == typeof t && (t = n.from(t));
this._alg = e;
this._key = t;
t.length > a ? t = e(t) : t.length < a && (t = n.concat([ t, o ], a));
for (var i = this._ipad = n.allocUnsafe(a), s = this._opad = n.allocUnsafe(a), c = 0; c < a; c++) {
i[c] = 54 ^ t[c];
s[c] = 92 ^ t[c];
}
this._hash = [ i ];
}
i(s, r);
s.prototype._update = function(e) {
this._hash.push(e);
};
s.prototype._final = function() {
var e = this._alg(n.concat(this._hash));
return this._alg(n.concat([ this._opad, e ]));
};
t.exports = s;
}, {
"cipher-base": 67,
inherits: 138,
"safe-buffer": 182
} ],
75: [ function(e, t, i) {
"use strict";
i.randomBytes = i.rng = i.pseudoRandomBytes = i.prng = e("randombytes");
i.createHash = i.Hash = e("create-hash");
i.createHmac = i.Hmac = e("create-hmac");
var n = e("browserify-sign/algos"), r = Object.keys(n), o = [ "sha1", "sha224", "sha256", "sha384", "sha512", "md5", "rmd160" ].concat(r);
i.getHashes = function() {
return o;
};
var a = e("pbkdf2");
i.pbkdf2 = a.pbkdf2;
i.pbkdf2Sync = a.pbkdf2Sync;
var s = e("browserify-cipher");
i.Cipher = s.Cipher;
i.createCipher = s.createCipher;
i.Cipheriv = s.Cipheriv;
i.createCipheriv = s.createCipheriv;
i.Decipher = s.Decipher;
i.createDecipher = s.createDecipher;
i.Decipheriv = s.Decipheriv;
i.createDecipheriv = s.createDecipheriv;
i.getCiphers = s.getCiphers;
i.listCiphers = s.listCiphers;
var c = e("diffie-hellman");
i.DiffieHellmanGroup = c.DiffieHellmanGroup;
i.createDiffieHellmanGroup = c.createDiffieHellmanGroup;
i.getDiffieHellman = c.getDiffieHellman;
i.createDiffieHellman = c.createDiffieHellman;
i.DiffieHellman = c.DiffieHellman;
var l = e("browserify-sign");
i.createSign = l.createSign;
i.Sign = l.Sign;
i.createVerify = l.createVerify;
i.Verify = l.Verify;
i.createECDH = e("create-ecdh");
var h = e("public-encrypt");
i.publicEncrypt = h.publicEncrypt;
i.privateEncrypt = h.privateEncrypt;
i.publicDecrypt = h.publicDecrypt;
i.privateDecrypt = h.privateDecrypt;
var u = e("randomfill");
i.randomFill = u.randomFill;
i.randomFillSync = u.randomFillSync;
i.createCredentials = function() {
throw new Error([ "sorry, createCredentials is not implemented yet", "we accept pull requests", "https://github.com/crypto-browserify/crypto-browserify" ].join("\n"));
};
i.constants = {
DH_CHECK_P_NOT_SAFE_PRIME: 2,
DH_CHECK_P_NOT_PRIME: 1,
DH_UNABLE_TO_CHECK_GENERATOR: 4,
DH_NOT_SUITABLE_GENERATOR: 8,
NPN_ENABLED: 1,
ALPN_ENABLED: 1,
RSA_PKCS1_PADDING: 1,
RSA_SSLV23_PADDING: 2,
RSA_NO_PADDING: 3,
RSA_PKCS1_OAEP_PADDING: 4,
RSA_X931_PADDING: 5,
RSA_PKCS1_PSS_PADDING: 6,
POINT_CONVERSION_COMPRESSED: 2,
POINT_CONVERSION_UNCOMPRESSED: 4,
POINT_CONVERSION_HYBRID: 6
};
}, {
"browserify-cipher": 37,
"browserify-sign": 44,
"browserify-sign/algos": 41,
"create-ecdh": 69,
"create-hash": 71,
"create-hmac": 73,
"diffie-hellman": 82,
pbkdf2: 149,
"public-encrypt": 157,
randombytes: 164,
randomfill: 165
} ],
76: [ function(e, t, i) {
"use strict";
i.utils = e("./des/utils");
i.Cipher = e("./des/cipher");
i.DES = e("./des/des");
i.CBC = e("./des/cbc");
i.EDE = e("./des/ede");
}, {
"./des/cbc": 77,
"./des/cipher": 78,
"./des/des": 79,
"./des/ede": 80,
"./des/utils": 81
} ],
77: [ function(e, t, i) {
"use strict";
var n = e("minimalistic-assert"), r = e("inherits"), o = {};
function a(e) {
n.equal(e.length, 8, "Invalid IV length");
this.iv = new Array(8);
for (var t = 0; t < this.iv.length; t++) this.iv[t] = e[t];
}
i.instantiate = function(e) {
function t(t) {
e.call(this, t);
this._cbcInit();
}
r(t, e);
for (var i = Object.keys(o), n = 0; n < i.length; n++) {
var a = i[n];
t.prototype[a] = o[a];
}
t.create = function(e) {
return new t(e);
};
return t;
};
o._cbcInit = function() {
var e = new a(this.options.iv);
this._cbcState = e;
};
o._update = function(e, t, i, n) {
var r = this._cbcState, o = this.constructor.super_.prototype, a = r.iv;
if ("encrypt" === this.type) {
for (var s = 0; s < this.blockSize; s++) a[s] ^= e[t + s];
o._update.call(this, a, 0, i, n);
for (s = 0; s < this.blockSize; s++) a[s] = i[n + s];
} else {
o._update.call(this, e, t, i, n);
for (s = 0; s < this.blockSize; s++) i[n + s] ^= a[s];
for (s = 0; s < this.blockSize; s++) a[s] = e[t + s];
}
};
}, {
inherits: 138,
"minimalistic-assert": 142
} ],
78: [ function(e, t) {
"use strict";
var i = e("minimalistic-assert");
function n(e) {
this.options = e;
this.type = this.options.type;
this.blockSize = 8;
this._init();
this.buffer = new Array(this.blockSize);
this.bufferOff = 0;
}
t.exports = n;
n.prototype._init = function() {};
n.prototype.update = function(e) {
return 0 === e.length ? [] : "decrypt" === this.type ? this._updateDecrypt(e) : this._updateEncrypt(e);
};
n.prototype._buffer = function(e, t) {
for (var i = Math.min(this.buffer.length - this.bufferOff, e.length - t), n = 0; n < i; n++) this.buffer[this.bufferOff + n] = e[t + n];
this.bufferOff += i;
return i;
};
n.prototype._flushBuffer = function(e, t) {
this._update(this.buffer, 0, e, t);
this.bufferOff = 0;
return this.blockSize;
};
n.prototype._updateEncrypt = function(e) {
var t = 0, i = 0, n = (this.bufferOff + e.length) / this.blockSize | 0, r = new Array(n * this.blockSize);
if (0 !== this.bufferOff) {
t += this._buffer(e, t);
this.bufferOff === this.buffer.length && (i += this._flushBuffer(r, i));
}
for (var o = e.length - (e.length - t) % this.blockSize; t < o; t += this.blockSize) {
this._update(e, t, r, i);
i += this.blockSize;
}
for (;t < e.length; t++, this.bufferOff++) this.buffer[this.bufferOff] = e[t];
return r;
};
n.prototype._updateDecrypt = function(e) {
for (var t = 0, i = 0, n = Math.ceil((this.bufferOff + e.length) / this.blockSize) - 1, r = new Array(n * this.blockSize); n > 0; n--) {
t += this._buffer(e, t);
i += this._flushBuffer(r, i);
}
t += this._buffer(e, t);
return r;
};
n.prototype.final = function(e) {
var t, i;
e && (t = this.update(e));
i = "encrypt" === this.type ? this._finalEncrypt() : this._finalDecrypt();
return t ? t.concat(i) : i;
};
n.prototype._pad = function(e, t) {
if (0 === t) return !1;
for (;t < e.length; ) e[t++] = 0;
return !0;
};
n.prototype._finalEncrypt = function() {
if (!this._pad(this.buffer, this.bufferOff)) return [];
var e = new Array(this.blockSize);
this._update(this.buffer, 0, e, 0);
return e;
};
n.prototype._unpad = function(e) {
return e;
};
n.prototype._finalDecrypt = function() {
i.equal(this.bufferOff, this.blockSize, "Not enough data to decrypt");
var e = new Array(this.blockSize);
this._flushBuffer(e, 0);
return this._unpad(e);
};
}, {
"minimalistic-assert": 142
} ],
79: [ function(e, t) {
"use strict";
var i = e("minimalistic-assert"), n = e("inherits"), r = e("./utils"), o = e("./cipher");
function a() {
this.tmp = new Array(2);
this.keys = null;
}
function s(e) {
o.call(this, e);
var t = new a();
this._desState = t;
this.deriveKeys(t, e.key);
}
n(s, o);
t.exports = s;
s.create = function(e) {
return new s(e);
};
var c = [ 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 ];
s.prototype.deriveKeys = function(e, t) {
e.keys = new Array(32);
i.equal(t.length, this.blockSize, "Invalid key length");
var n = r.readUInt32BE(t, 0), o = r.readUInt32BE(t, 4);
r.pc1(n, o, e.tmp, 0);
n = e.tmp[0];
o = e.tmp[1];
for (var a = 0; a < e.keys.length; a += 2) {
var s = c[a >>> 1];
n = r.r28shl(n, s);
o = r.r28shl(o, s);
r.pc2(n, o, e.keys, a);
}
};
s.prototype._update = function(e, t, i, n) {
var o = this._desState, a = r.readUInt32BE(e, t), s = r.readUInt32BE(e, t + 4);
r.ip(a, s, o.tmp, 0);
a = o.tmp[0];
s = o.tmp[1];
"encrypt" === this.type ? this._encrypt(o, a, s, o.tmp, 0) : this._decrypt(o, a, s, o.tmp, 0);
a = o.tmp[0];
s = o.tmp[1];
r.writeUInt32BE(i, a, n);
r.writeUInt32BE(i, s, n + 4);
};
s.prototype._pad = function(e, t) {
for (var i = e.length - t, n = t; n < e.length; n++) e[n] = i;
return !0;
};
s.prototype._unpad = function(e) {
for (var t = e[e.length - 1], n = e.length - t; n < e.length; n++) i.equal(e[n], t);
return e.slice(0, e.length - t);
};
s.prototype._encrypt = function(e, t, i, n, o) {
for (var a = t, s = i, c = 0; c < e.keys.length; c += 2) {
var l = e.keys[c], h = e.keys[c + 1];
r.expand(s, e.tmp, 0);
l ^= e.tmp[0];
h ^= e.tmp[1];
var u = r.substitute(l, h), f = s;
s = (a ^ r.permute(u)) >>> 0;
a = f;
}
r.rip(s, a, n, o);
};
s.prototype._decrypt = function(e, t, i, n, o) {
for (var a = i, s = t, c = e.keys.length - 2; c >= 0; c -= 2) {
var l = e.keys[c], h = e.keys[c + 1];
r.expand(a, e.tmp, 0);
l ^= e.tmp[0];
h ^= e.tmp[1];
var u = r.substitute(l, h), f = a;
a = (s ^ r.permute(u)) >>> 0;
s = f;
}
r.rip(a, s, n, o);
};
}, {
"./cipher": 78,
"./utils": 81,
inherits: 138,
"minimalistic-assert": 142
} ],
80: [ function(e, t) {
"use strict";
var i = e("minimalistic-assert"), n = e("inherits"), r = e("./cipher"), o = e("./des");
function a(e, t) {
i.equal(t.length, 24, "Invalid key length");
var n = t.slice(0, 8), r = t.slice(8, 16), a = t.slice(16, 24);
this.ciphers = "encrypt" === e ? [ o.create({
type: "encrypt",
key: n
}), o.create({
type: "decrypt",
key: r
}), o.create({
type: "encrypt",
key: a
}) ] : [ o.create({
type: "decrypt",
key: a
}), o.create({
type: "encrypt",
key: r
}), o.create({
type: "decrypt",
key: n
}) ];
}
function s(e) {
r.call(this, e);
var t = new a(this.type, this.options.key);
this._edeState = t;
}
n(s, r);
t.exports = s;
s.create = function(e) {
return new s(e);
};
s.prototype._update = function(e, t, i, n) {
var r = this._edeState;
r.ciphers[0]._update(e, t, i, n);
r.ciphers[1]._update(i, n, i, n);
r.ciphers[2]._update(i, n, i, n);
};
s.prototype._pad = o.prototype._pad;
s.prototype._unpad = o.prototype._unpad;
}, {
"./cipher": 78,
"./des": 79,
inherits: 138,
"minimalistic-assert": 142
} ],
81: [ function(e, t, i) {
"use strict";
i.readUInt32BE = function(e, t) {
return (e[0 + t] << 24 | e[1 + t] << 16 | e[2 + t] << 8 | e[3 + t]) >>> 0;
};
i.writeUInt32BE = function(e, t, i) {
e[0 + i] = t >>> 24;
e[1 + i] = t >>> 16 & 255;
e[2 + i] = t >>> 8 & 255;
e[3 + i] = 255 & t;
};
i.ip = function(e, t, i, n) {
for (var r = 0, o = 0, a = 6; a >= 0; a -= 2) {
for (var s = 0; s <= 24; s += 8) {
r <<= 1;
r |= t >>> s + a & 1;
}
for (s = 0; s <= 24; s += 8) {
r <<= 1;
r |= e >>> s + a & 1;
}
}
for (a = 6; a >= 0; a -= 2) {
for (s = 1; s <= 25; s += 8) {
o <<= 1;
o |= t >>> s + a & 1;
}
for (s = 1; s <= 25; s += 8) {
o <<= 1;
o |= e >>> s + a & 1;
}
}
i[n + 0] = r >>> 0;
i[n + 1] = o >>> 0;
};
i.rip = function(e, t, i, n) {
for (var r = 0, o = 0, a = 0; a < 4; a++) for (var s = 24; s >= 0; s -= 8) {
r <<= 1;
r |= t >>> s + a & 1;
r <<= 1;
r |= e >>> s + a & 1;
}
for (a = 4; a < 8; a++) for (s = 24; s >= 0; s -= 8) {
o <<= 1;
o |= t >>> s + a & 1;
o <<= 1;
o |= e >>> s + a & 1;
}
i[n + 0] = r >>> 0;
i[n + 1] = o >>> 0;
};
i.pc1 = function(e, t, i, n) {
for (var r = 0, o = 0, a = 7; a >= 5; a--) {
for (var s = 0; s <= 24; s += 8) {
r <<= 1;
r |= t >> s + a & 1;
}
for (s = 0; s <= 24; s += 8) {
r <<= 1;
r |= e >> s + a & 1;
}
}
for (s = 0; s <= 24; s += 8) {
r <<= 1;
r |= t >> s + a & 1;
}
for (a = 1; a <= 3; a++) {
for (s = 0; s <= 24; s += 8) {
o <<= 1;
o |= t >> s + a & 1;
}
for (s = 0; s <= 24; s += 8) {
o <<= 1;
o |= e >> s + a & 1;
}
}
for (s = 0; s <= 24; s += 8) {
o <<= 1;
o |= e >> s + a & 1;
}
i[n + 0] = r >>> 0;
i[n + 1] = o >>> 0;
};
i.r28shl = function(e, t) {
return e << t & 268435455 | e >>> 28 - t;
};
var n = [ 14, 11, 17, 4, 27, 23, 25, 0, 13, 22, 7, 18, 5, 9, 16, 24, 2, 20, 12, 21, 1, 8, 15, 26, 15, 4, 25, 19, 9, 1, 26, 16, 5, 11, 23, 8, 12, 7, 17, 0, 22, 3, 10, 14, 6, 20, 27, 24 ];
i.pc2 = function(e, t, i, r) {
for (var o = 0, a = 0, s = n.length >>> 1, c = 0; c < s; c++) {
o <<= 1;
o |= e >>> n[c] & 1;
}
for (c = s; c < n.length; c++) {
a <<= 1;
a |= t >>> n[c] & 1;
}
i[r + 0] = o >>> 0;
i[r + 1] = a >>> 0;
};
i.expand = function(e, t, i) {
var n = 0, r = 0;
n = (1 & e) << 5 | e >>> 27;
for (var o = 23; o >= 15; o -= 4) {
n <<= 6;
n |= e >>> o & 63;
}
for (o = 11; o >= 3; o -= 4) {
r |= e >>> o & 63;
r <<= 6;
}
r |= (31 & e) << 1 | e >>> 31;
t[i + 0] = n >>> 0;
t[i + 1] = r >>> 0;
};
var r = [ 14, 0, 4, 15, 13, 7, 1, 4, 2, 14, 15, 2, 11, 13, 8, 1, 3, 10, 10, 6, 6, 12, 12, 11, 5, 9, 9, 5, 0, 3, 7, 8, 4, 15, 1, 12, 14, 8, 8, 2, 13, 4, 6, 9, 2, 1, 11, 7, 15, 5, 12, 11, 9, 3, 7, 14, 3, 10, 10, 0, 5, 6, 0, 13, 15, 3, 1, 13, 8, 4, 14, 7, 6, 15, 11, 2, 3, 8, 4, 14, 9, 12, 7, 0, 2, 1, 13, 10, 12, 6, 0, 9, 5, 11, 10, 5, 0, 13, 14, 8, 7, 10, 11, 1, 10, 3, 4, 15, 13, 4, 1, 2, 5, 11, 8, 6, 12, 7, 6, 12, 9, 0, 3, 5, 2, 14, 15, 9, 10, 13, 0, 7, 9, 0, 14, 9, 6, 3, 3, 4, 15, 6, 5, 10, 1, 2, 13, 8, 12, 5, 7, 14, 11, 12, 4, 11, 2, 15, 8, 1, 13, 1, 6, 10, 4, 13, 9, 0, 8, 6, 15, 9, 3, 8, 0, 7, 11, 4, 1, 15, 2, 14, 12, 3, 5, 11, 10, 5, 14, 2, 7, 12, 7, 13, 13, 8, 14, 11, 3, 5, 0, 6, 6, 15, 9, 0, 10, 3, 1, 4, 2, 7, 8, 2, 5, 12, 11, 1, 12, 10, 4, 14, 15, 9, 10, 3, 6, 15, 9, 0, 0, 6, 12, 10, 11, 1, 7, 13, 13, 8, 15, 9, 1, 4, 3, 5, 14, 11, 5, 12, 2, 7, 8, 2, 4, 14, 2, 14, 12, 11, 4, 2, 1, 12, 7, 4, 10, 7, 11, 13, 6, 1, 8, 5, 5, 0, 3, 15, 15, 10, 13, 3, 0, 9, 14, 8, 9, 6, 4, 11, 2, 8, 1, 12, 11, 7, 10, 1, 13, 14, 7, 2, 8, 13, 15, 6, 9, 15, 12, 0, 5, 9, 6, 10, 3, 4, 0, 5, 14, 3, 12, 10, 1, 15, 10, 4, 15, 2, 9, 7, 2, 12, 6, 9, 8, 5, 0, 6, 13, 1, 3, 13, 4, 14, 14, 0, 7, 11, 5, 3, 11, 8, 9, 4, 14, 3, 15, 2, 5, 12, 2, 9, 8, 5, 12, 15, 3, 10, 7, 11, 0, 14, 4, 1, 10, 7, 1, 6, 13, 0, 11, 8, 6, 13, 4, 13, 11, 0, 2, 11, 14, 7, 15, 4, 0, 9, 8, 1, 13, 10, 3, 14, 12, 3, 9, 5, 7, 12, 5, 2, 10, 15, 6, 8, 1, 6, 1, 6, 4, 11, 11, 13, 13, 8, 12, 1, 3, 4, 7, 10, 14, 7, 10, 9, 15, 5, 6, 0, 8, 15, 0, 14, 5, 2, 9, 3, 2, 12, 13, 1, 2, 15, 8, 13, 4, 8, 6, 10, 15, 3, 11, 7, 1, 4, 10, 12, 9, 5, 3, 6, 14, 11, 5, 0, 0, 14, 12, 9, 7, 2, 7, 2, 11, 1, 4, 14, 1, 7, 9, 4, 12, 10, 14, 8, 2, 13, 0, 15, 6, 12, 10, 9, 13, 0, 15, 3, 3, 5, 5, 6, 8, 11 ];
i.substitute = function(e, t) {
for (var i = 0, n = 0; n < 4; n++) {
i <<= 4;
i |= r[64 * n + (e >>> 18 - 6 * n & 63)];
}
for (n = 0; n < 4; n++) {
i <<= 4;
i |= r[256 + 64 * n + (t >>> 18 - 6 * n & 63)];
}
return i >>> 0;
};
var o = [ 16, 25, 12, 11, 3, 20, 4, 15, 31, 17, 9, 6, 27, 14, 1, 22, 30, 24, 8, 18, 0, 5, 29, 23, 13, 19, 2, 26, 10, 21, 28, 7 ];
i.permute = function(e) {
for (var t = 0, i = 0; i < o.length; i++) {
t <<= 1;
t |= e >>> o[i] & 1;
}
return t >>> 0;
};
i.padSplit = function(e, t, i) {
for (var n = e.toString(2); n.length < t; ) n = "0" + n;
for (var r = [], o = 0; o < t; o += i) r.push(n.slice(o, o + i));
return r.join(" ");
};
}, {} ],
82: [ function(e, t, i) {
(function(t) {
var n = e("./lib/generatePrime"), r = e("./lib/primes.json"), o = e("./lib/dh"), a = {
binary: !0,
hex: !0,
base64: !0
};
i.DiffieHellmanGroup = i.createDiffieHellmanGroup = i.getDiffieHellman = function(e) {
var i = new t(r[e].prime, "hex"), n = new t(r[e].gen, "hex");
return new o(i, n);
};
i.createDiffieHellman = i.DiffieHellman = function e(i, r, s, c) {
if (t.isBuffer(r) || void 0 === a[r]) return e(i, "binary", r, s);
r = r || "binary";
c = c || "binary";
s = s || new t([ 2 ]);
t.isBuffer(s) || (s = new t(s, c));
if ("number" == typeof i) return new o(n(i, s), s, !0);
t.isBuffer(i) || (i = new t(i, r));
return new o(i, s, !0);
};
}).call(this, e("buffer").Buffer);
}, {
"./lib/dh": 83,
"./lib/generatePrime": 84,
"./lib/primes.json": 85,
buffer: 65
} ],
83: [ function(e, t) {
(function(i) {
var n = e("bn.js"), r = new (e("miller-rabin"))(), o = new n(24), a = new n(11), s = new n(10), c = new n(3), l = new n(7), h = e("./generatePrime"), u = e("randombytes");
t.exports = g;
function f(e, t) {
t = t || "utf8";
i.isBuffer(e) || (e = new i(e, t));
this._pub = new n(e);
return this;
}
function d(e, t) {
t = t || "utf8";
i.isBuffer(e) || (e = new i(e, t));
this._priv = new n(e);
return this;
}
var p = {};
function m(e, t) {
var i = t.toString("hex"), n = [ i, e.toString(16) ].join("_");
if (n in p) return p[n];
var u, f = 0;
if (e.isEven() || !h.simpleSieve || !h.fermatTest(e) || !r.test(e)) {
f += 1;
f += "02" === i || "05" === i ? 8 : 4;
p[n] = f;
return f;
}
r.test(e.shrn(1)) || (f += 2);
switch (i) {
case "02":
e.mod(o).cmp(a) && (f += 8);
break;

case "05":
(u = e.mod(s)).cmp(c) && u.cmp(l) && (f += 8);
break;

default:
f += 4;
}
p[n] = f;
return f;
}
function g(e, t, i) {
this.setGenerator(t);
this.__prime = new n(e);
this._prime = n.mont(this.__prime);
this._primeLen = e.length;
this._pub = void 0;
this._priv = void 0;
this._primeCode = void 0;
if (i) {
this.setPublicKey = f;
this.setPrivateKey = d;
} else this._primeCode = 8;
}
Object.defineProperty(g.prototype, "verifyError", {
enumerable: !0,
get: function() {
"number" != typeof this._primeCode && (this._primeCode = m(this.__prime, this.__gen));
return this._primeCode;
}
});
g.prototype.generateKeys = function() {
this._priv || (this._priv = new n(u(this._primeLen)));
this._pub = this._gen.toRed(this._prime).redPow(this._priv).fromRed();
return this.getPublicKey();
};
g.prototype.computeSecret = function(e) {
var t = (e = (e = new n(e)).toRed(this._prime)).redPow(this._priv).fromRed(), r = new i(t.toArray()), o = this.getPrime();
if (r.length < o.length) {
var a = new i(o.length - r.length);
a.fill(0);
r = i.concat([ a, r ]);
}
return r;
};
g.prototype.getPublicKey = function(e) {
return y(this._pub, e);
};
g.prototype.getPrivateKey = function(e) {
return y(this._priv, e);
};
g.prototype.getPrime = function(e) {
return y(this.__prime, e);
};
g.prototype.getGenerator = function(e) {
return y(this._gen, e);
};
g.prototype.setGenerator = function(e, t) {
t = t || "utf8";
i.isBuffer(e) || (e = new i(e, t));
this.__gen = e;
this._gen = new n(e);
return this;
};
function y(e, t) {
var n = new i(e.toArray());
return t ? n.toString(t) : n;
}
}).call(this, e("buffer").Buffer);
}, {
"./generatePrime": 84,
"bn.js": 86,
buffer: 65,
"miller-rabin": 140,
randombytes: 164
} ],
84: [ function(e, t) {
var i = e("randombytes");
t.exports = y;
y.simpleSieve = m;
y.fermatTest = g;
var n = e("bn.js"), r = new n(24), o = new (e("miller-rabin"))(), a = new n(1), s = new n(2), c = new n(5), l = (new n(16), 
new n(8), new n(10)), h = new n(3), u = (new n(7), new n(11)), f = new n(4), d = (new n(12), 
null);
function p() {
if (null !== d) return d;
var e = [];
e[0] = 2;
for (var t = 1, i = 3; i < 1048576; i += 2) {
for (var n = Math.ceil(Math.sqrt(i)), r = 0; r < t && e[r] <= n && i % e[r] != 0; r++) ;
t !== r && e[r] <= n || (e[t++] = i);
}
d = e;
return e;
}
function m(e) {
for (var t = p(), i = 0; i < t.length; i++) if (0 === e.modn(t[i])) return 0 === e.cmpn(t[i]);
return !0;
}
function g(e) {
var t = n.mont(e);
return 0 === s.toRed(t).redPow(e.subn(1)).fromRed().cmpn(1);
}
function y(e, t) {
if (e < 16) return new n(2 === t || 5 === t ? [ 140, 123 ] : [ 140, 39 ]);
t = new n(t);
for (var d, p; ;) {
d = new n(i(Math.ceil(e / 8)));
for (;d.bitLength() > e; ) d.ishrn(1);
d.isEven() && d.iadd(a);
d.testn(1) || d.iadd(s);
if (t.cmp(s)) {
if (!t.cmp(c)) for (;d.mod(l).cmp(h); ) d.iadd(f);
} else for (;d.mod(r).cmp(u); ) d.iadd(f);
if (m(p = d.shrn(1)) && m(d) && g(p) && g(d) && o.test(p) && o.test(d)) return d;
}
}
}, {
"bn.js": 86,
"miller-rabin": 140,
randombytes: 164
} ],
85: [ function(e, t) {
t.exports = {
modp1: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a63a3620ffffffffffffffff"
},
modp2: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece65381ffffffffffffffff"
},
modp5: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff"
},
modp14: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca18217c32905e462e36ce3be39e772c180e86039b2783a2ec07a28fb5c55df06f4c52c9de2bcbf6955817183995497cea956ae515d2261898fa051015728e5a8aacaa68ffffffffffffffff"
},
modp15: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca18217c32905e462e36ce3be39e772c180e86039b2783a2ec07a28fb5c55df06f4c52c9de2bcbf6955817183995497cea956ae515d2261898fa051015728e5a8aaac42dad33170d04507a33a85521abdf1cba64ecfb850458dbef0a8aea71575d060c7db3970f85a6e1e4c7abf5ae8cdb0933d71e8c94e04a25619dcee3d2261ad2ee6bf12ffa06d98a0864d87602733ec86a64521f2b18177b200cbbe117577a615d6c770988c0bad946e208e24fa074e5ab3143db5bfce0fd108e4b82d120a93ad2caffffffffffffffff"
},
modp16: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca18217c32905e462e36ce3be39e772c180e86039b2783a2ec07a28fb5c55df06f4c52c9de2bcbf6955817183995497cea956ae515d2261898fa051015728e5a8aaac42dad33170d04507a33a85521abdf1cba64ecfb850458dbef0a8aea71575d060c7db3970f85a6e1e4c7abf5ae8cdb0933d71e8c94e04a25619dcee3d2261ad2ee6bf12ffa06d98a0864d87602733ec86a64521f2b18177b200cbbe117577a615d6c770988c0bad946e208e24fa074e5ab3143db5bfce0fd108e4b82d120a92108011a723c12a787e6d788719a10bdba5b2699c327186af4e23c1a946834b6150bda2583e9ca2ad44ce8dbbbc2db04de8ef92e8efc141fbecaa6287c59474e6bc05d99b2964fa090c3a2233ba186515be7ed1f612970cee2d7afb81bdd762170481cd0069127d5b05aa993b4ea988d8fddc186ffb7dc90a6c08f4df435c934063199ffffffffffffffff"
},
modp17: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca18217c32905e462e36ce3be39e772c180e86039b2783a2ec07a28fb5c55df06f4c52c9de2bcbf6955817183995497cea956ae515d2261898fa051015728e5a8aaac42dad33170d04507a33a85521abdf1cba64ecfb850458dbef0a8aea71575d060c7db3970f85a6e1e4c7abf5ae8cdb0933d71e8c94e04a25619dcee3d2261ad2ee6bf12ffa06d98a0864d87602733ec86a64521f2b18177b200cbbe117577a615d6c770988c0bad946e208e24fa074e5ab3143db5bfce0fd108e4b82d120a92108011a723c12a787e6d788719a10bdba5b2699c327186af4e23c1a946834b6150bda2583e9ca2ad44ce8dbbbc2db04de8ef92e8efc141fbecaa6287c59474e6bc05d99b2964fa090c3a2233ba186515be7ed1f612970cee2d7afb81bdd762170481cd0069127d5b05aa993b4ea988d8fddc186ffb7dc90a6c08f4df435c93402849236c3fab4d27c7026c1d4dcb2602646dec9751e763dba37bdf8ff9406ad9e530ee5db382f413001aeb06a53ed9027d831179727b0865a8918da3edbebcf9b14ed44ce6cbaced4bb1bdb7f1447e6cc254b332051512bd7af426fb8f401378cd2bf5983ca01c64b92ecf032ea15d1721d03f482d7ce6e74fef6d55e702f46980c82b5a84031900b1c9e59e7c97fbec7e8f323a97a7e36cc88be0f1d45b7ff585ac54bd407b22b4154aacc8f6d7ebf48e1d814cc5ed20f8037e0a79715eef29be32806a1d58bb7c5da76f550aa3d8a1fbff0eb19ccb1a313d55cda56c9ec2ef29632387fe8d76e3c0468043e8f663f4860ee12bf2d5b0b7474d6e694f91e6dcc4024ffffffffffffffff"
},
modp18: {
gen: "02",
prime: "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca18217c32905e462e36ce3be39e772c180e86039b2783a2ec07a28fb5c55df06f4c52c9de2bcbf6955817183995497cea956ae515d2261898fa051015728e5a8aaac42dad33170d04507a33a85521abdf1cba64ecfb850458dbef0a8aea71575d060c7db3970f85a6e1e4c7abf5ae8cdb0933d71e8c94e04a25619dcee3d2261ad2ee6bf12ffa06d98a0864d87602733ec86a64521f2b18177b200cbbe117577a615d6c770988c0bad946e208e24fa074e5ab3143db5bfce0fd108e4b82d120a92108011a723c12a787e6d788719a10bdba5b2699c327186af4e23c1a946834b6150bda2583e9ca2ad44ce8dbbbc2db04de8ef92e8efc141fbecaa6287c59474e6bc05d99b2964fa090c3a2233ba186515be7ed1f612970cee2d7afb81bdd762170481cd0069127d5b05aa993b4ea988d8fddc186ffb7dc90a6c08f4df435c93402849236c3fab4d27c7026c1d4dcb2602646dec9751e763dba37bdf8ff9406ad9e530ee5db382f413001aeb06a53ed9027d831179727b0865a8918da3edbebcf9b14ed44ce6cbaced4bb1bdb7f1447e6cc254b332051512bd7af426fb8f401378cd2bf5983ca01c64b92ecf032ea15d1721d03f482d7ce6e74fef6d55e702f46980c82b5a84031900b1c9e59e7c97fbec7e8f323a97a7e36cc88be0f1d45b7ff585ac54bd407b22b4154aacc8f6d7ebf48e1d814cc5ed20f8037e0a79715eef29be32806a1d58bb7c5da76f550aa3d8a1fbff0eb19ccb1a313d55cda56c9ec2ef29632387fe8d76e3c0468043e8f663f4860ee12bf2d5b0b7474d6e694f91e6dbe115974a3926f12fee5e438777cb6a932df8cd8bec4d073b931ba3bc832b68d9dd300741fa7bf8afc47ed2576f6936ba424663aab639c5ae4f5683423b4742bf1c978238f16cbe39d652de3fdb8befc848ad922222e04a4037c0713eb57a81a23f0c73473fc646cea306b4bcbc8862f8385ddfa9d4b7fa2c087e879683303ed5bdd3a062b3cf5b3a278a66d2a13f83f44f82ddf310ee074ab6a364597e899a0255dc164f31cc50846851df9ab48195ded7ea1b1d510bd7ee74d73faf36bc31ecfa268359046f4eb879f924009438b481c6cd7889a002ed5ee382bc9190da6fc026e479558e4475677e9aa9e3050e2765694dfc81f56e880b96e7160c980dd98edd3dfffffffffffffffff"
}
};
}, {} ],
86: [ function(e, t, i) {
arguments[4][15][0].apply(i, arguments);
}, {
buffer: 19,
dup: 15
} ],
87: [ function(e, t, i) {
"use strict";
var n = i;
n.version = e("../package.json").version;
n.utils = e("./elliptic/utils");
n.rand = e("brorand");
n.curve = e("./elliptic/curve");
n.curves = e("./elliptic/curves");
n.ec = e("./elliptic/ec");
n.eddsa = e("./elliptic/eddsa");
}, {
"../package.json": 103,
"./elliptic/curve": 90,
"./elliptic/curves": 93,
"./elliptic/ec": 94,
"./elliptic/eddsa": 97,
"./elliptic/utils": 101,
brorand: 18
} ],
88: [ function(e, t) {
"use strict";
var i = e("bn.js"), n = e("../utils"), r = n.getNAF, o = n.getJSF, a = n.assert;
function s(e, t) {
this.type = e;
this.p = new i(t.p, 16);
this.red = t.prime ? i.red(t.prime) : i.mont(this.p);
this.zero = new i(0).toRed(this.red);
this.one = new i(1).toRed(this.red);
this.two = new i(2).toRed(this.red);
this.n = t.n && new i(t.n, 16);
this.g = t.g && this.pointFromJSON(t.g, t.gRed);
this._wnafT1 = new Array(4);
this._wnafT2 = new Array(4);
this._wnafT3 = new Array(4);
this._wnafT4 = new Array(4);
this._bitLength = this.n ? this.n.bitLength() : 0;
var n = this.n && this.p.div(this.n);
if (!n || n.cmpn(100) > 0) this.redN = null; else {
this._maxwellTrick = !0;
this.redN = this.n.toRed(this.red);
}
}
t.exports = s;
s.prototype.point = function() {
throw new Error("Not implemented");
};
s.prototype.validate = function() {
throw new Error("Not implemented");
};
s.prototype._fixedNafMul = function(e, t) {
a(e.precomputed);
var i = e._getDoubles(), n = r(t, 1, this._bitLength), o = (1 << i.step + 1) - (i.step % 2 == 0 ? 2 : 1);
o /= 3;
var s, c, l = [];
for (s = 0; s < n.length; s += i.step) {
c = 0;
for (var h = s + i.step - 1; h >= s; h--) c = (c << 1) + n[h];
l.push(c);
}
for (var u = this.jpoint(null, null, null), f = this.jpoint(null, null, null), d = o; d > 0; d--) {
for (s = 0; s < l.length; s++) (c = l[s]) === d ? f = f.mixedAdd(i.points[s]) : c === -d && (f = f.mixedAdd(i.points[s].neg()));
u = u.add(f);
}
return u.toP();
};
s.prototype._wnafMul = function(e, t) {
var i = 4, n = e._getNAFPoints(i);
i = n.wnd;
for (var o = n.points, s = r(t, i, this._bitLength), c = this.jpoint(null, null, null), l = s.length - 1; l >= 0; l--) {
for (var h = 0; l >= 0 && 0 === s[l]; l--) h++;
l >= 0 && h++;
c = c.dblp(h);
if (l < 0) break;
var u = s[l];
a(0 !== u);
c = "affine" === e.type ? u > 0 ? c.mixedAdd(o[u - 1 >> 1]) : c.mixedAdd(o[-u - 1 >> 1].neg()) : u > 0 ? c.add(o[u - 1 >> 1]) : c.add(o[-u - 1 >> 1].neg());
}
return "affine" === e.type ? c.toP() : c;
};
s.prototype._wnafMulAdd = function(e, t, i, n, a) {
var s, c, l, h = this._wnafT1, u = this._wnafT2, f = this._wnafT3, d = 0;
for (s = 0; s < n; s++) {
var p = (l = t[s])._getNAFPoints(e);
h[s] = p.wnd;
u[s] = p.points;
}
for (s = n - 1; s >= 1; s -= 2) {
var m = s - 1, g = s;
if (1 === h[m] && 1 === h[g]) {
var y = [ t[m], null, null, t[g] ];
if (0 === t[m].y.cmp(t[g].y)) {
y[1] = t[m].add(t[g]);
y[2] = t[m].toJ().mixedAdd(t[g].neg());
} else if (0 === t[m].y.cmp(t[g].y.redNeg())) {
y[1] = t[m].toJ().mixedAdd(t[g]);
y[2] = t[m].add(t[g].neg());
} else {
y[1] = t[m].toJ().mixedAdd(t[g]);
y[2] = t[m].toJ().mixedAdd(t[g].neg());
}
var b = [ -3, -1, -5, -7, 0, 7, 5, 1, 3 ], v = o(i[m], i[g]);
d = Math.max(v[0].length, d);
f[m] = new Array(d);
f[g] = new Array(d);
for (c = 0; c < d; c++) {
var _ = 0 | v[0][c], w = 0 | v[1][c];
f[m][c] = b[3 * (_ + 1) + (w + 1)];
f[g][c] = 0;
u[m] = y;
}
} else {
f[m] = r(i[m], h[m], this._bitLength);
f[g] = r(i[g], h[g], this._bitLength);
d = Math.max(f[m].length, d);
d = Math.max(f[g].length, d);
}
}
var C = this.jpoint(null, null, null), S = this._wnafT4;
for (s = d; s >= 0; s--) {
for (var A = 0; s >= 0; ) {
var M = !0;
for (c = 0; c < n; c++) {
S[c] = 0 | f[c][s];
0 !== S[c] && (M = !1);
}
if (!M) break;
A++;
s--;
}
s >= 0 && A++;
C = C.dblp(A);
if (s < 0) break;
for (c = 0; c < n; c++) {
var B = S[c];
if (0 !== B) {
B > 0 ? l = u[c][B - 1 >> 1] : B < 0 && (l = u[c][-B - 1 >> 1].neg());
C = "affine" === l.type ? C.mixedAdd(l) : C.add(l);
}
}
}
for (s = 0; s < n; s++) u[s] = null;
return a ? C : C.toP();
};
function c(e, t) {
this.curve = e;
this.type = t;
this.precomputed = null;
}
s.BasePoint = c;
c.prototype.eq = function() {
throw new Error("Not implemented");
};
c.prototype.validate = function() {
return this.curve.validate(this);
};
s.prototype.decodePoint = function(e, t) {
e = n.toArray(e, t);
var i = this.p.byteLength();
if ((4 === e[0] || 6 === e[0] || 7 === e[0]) && e.length - 1 == 2 * i) {
6 === e[0] ? a(e[e.length - 1] % 2 == 0) : 7 === e[0] && a(e[e.length - 1] % 2 == 1);
return this.point(e.slice(1, 1 + i), e.slice(1 + i, 1 + 2 * i));
}
if ((2 === e[0] || 3 === e[0]) && e.length - 1 === i) return this.pointFromX(e.slice(1, 1 + i), 3 === e[0]);
throw new Error("Unknown point format");
};
c.prototype.encodeCompressed = function(e) {
return this.encode(e, !0);
};
c.prototype._encode = function(e) {
var t = this.curve.p.byteLength(), i = this.getX().toArray("be", t);
return e ? [ this.getY().isEven() ? 2 : 3 ].concat(i) : [ 4 ].concat(i, this.getY().toArray("be", t));
};
c.prototype.encode = function(e, t) {
return n.encode(this._encode(t), e);
};
c.prototype.precompute = function(e) {
if (this.precomputed) return this;
var t = {
doubles: null,
naf: null,
beta: null
};
t.naf = this._getNAFPoints(8);
t.doubles = this._getDoubles(4, e);
t.beta = this._getBeta();
this.precomputed = t;
return this;
};
c.prototype._hasDoubles = function(e) {
if (!this.precomputed) return !1;
var t = this.precomputed.doubles;
return !!t && t.points.length >= Math.ceil((e.bitLength() + 1) / t.step);
};
c.prototype._getDoubles = function(e, t) {
if (this.precomputed && this.precomputed.doubles) return this.precomputed.doubles;
for (var i = [ this ], n = this, r = 0; r < t; r += e) {
for (var o = 0; o < e; o++) n = n.dbl();
i.push(n);
}
return {
step: e,
points: i
};
};
c.prototype._getNAFPoints = function(e) {
if (this.precomputed && this.precomputed.naf) return this.precomputed.naf;
for (var t = [ this ], i = (1 << e) - 1, n = 1 === i ? null : this.dbl(), r = 1; r < i; r++) t[r] = t[r - 1].add(n);
return {
wnd: e,
points: t
};
};
c.prototype._getBeta = function() {
return null;
};
c.prototype.dblp = function(e) {
for (var t = this, i = 0; i < e; i++) t = t.dbl();
return t;
};
}, {
"../utils": 101,
"bn.js": 102
} ],
89: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("bn.js"), r = e("inherits"), o = e("./base"), a = i.assert;
function s(e) {
this.twisted = 1 != (0 | e.a);
this.mOneA = this.twisted && -1 == (0 | e.a);
this.extended = this.mOneA;
o.call(this, "edwards", e);
this.a = new n(e.a, 16).umod(this.red.m);
this.a = this.a.toRed(this.red);
this.c = new n(e.c, 16).toRed(this.red);
this.c2 = this.c.redSqr();
this.d = new n(e.d, 16).toRed(this.red);
this.dd = this.d.redAdd(this.d);
a(!this.twisted || 0 === this.c.fromRed().cmpn(1));
this.oneC = 1 == (0 | e.c);
}
r(s, o);
t.exports = s;
s.prototype._mulA = function(e) {
return this.mOneA ? e.redNeg() : this.a.redMul(e);
};
s.prototype._mulC = function(e) {
return this.oneC ? e : this.c.redMul(e);
};
s.prototype.jpoint = function(e, t, i, n) {
return this.point(e, t, i, n);
};
s.prototype.pointFromX = function(e, t) {
(e = new n(e, 16)).red || (e = e.toRed(this.red));
var i = e.redSqr(), r = this.c2.redSub(this.a.redMul(i)), o = this.one.redSub(this.c2.redMul(this.d).redMul(i)), a = r.redMul(o.redInvm()), s = a.redSqrt();
if (0 !== s.redSqr().redSub(a).cmp(this.zero)) throw new Error("invalid point");
var c = s.fromRed().isOdd();
(t && !c || !t && c) && (s = s.redNeg());
return this.point(e, s);
};
s.prototype.pointFromY = function(e, t) {
(e = new n(e, 16)).red || (e = e.toRed(this.red));
var i = e.redSqr(), r = i.redSub(this.c2), o = i.redMul(this.d).redMul(this.c2).redSub(this.a), a = r.redMul(o.redInvm());
if (0 === a.cmp(this.zero)) {
if (t) throw new Error("invalid point");
return this.point(this.zero, e);
}
var s = a.redSqrt();
if (0 !== s.redSqr().redSub(a).cmp(this.zero)) throw new Error("invalid point");
s.fromRed().isOdd() !== t && (s = s.redNeg());
return this.point(s, e);
};
s.prototype.validate = function(e) {
if (e.isInfinity()) return !0;
e.normalize();
var t = e.x.redSqr(), i = e.y.redSqr(), n = t.redMul(this.a).redAdd(i), r = this.c2.redMul(this.one.redAdd(this.d.redMul(t).redMul(i)));
return 0 === n.cmp(r);
};
function c(e, t, i, r, a) {
o.BasePoint.call(this, e, "projective");
if (null === t && null === i && null === r) {
this.x = this.curve.zero;
this.y = this.curve.one;
this.z = this.curve.one;
this.t = this.curve.zero;
this.zOne = !0;
} else {
this.x = new n(t, 16);
this.y = new n(i, 16);
this.z = r ? new n(r, 16) : this.curve.one;
this.t = a && new n(a, 16);
this.x.red || (this.x = this.x.toRed(this.curve.red));
this.y.red || (this.y = this.y.toRed(this.curve.red));
this.z.red || (this.z = this.z.toRed(this.curve.red));
this.t && !this.t.red && (this.t = this.t.toRed(this.curve.red));
this.zOne = this.z === this.curve.one;
if (this.curve.extended && !this.t) {
this.t = this.x.redMul(this.y);
this.zOne || (this.t = this.t.redMul(this.z.redInvm()));
}
}
}
r(c, o.BasePoint);
s.prototype.pointFromJSON = function(e) {
return c.fromJSON(this, e);
};
s.prototype.point = function(e, t, i, n) {
return new c(this, e, t, i, n);
};
c.fromJSON = function(e, t) {
return new c(e, t[0], t[1], t[2]);
};
c.prototype.inspect = function() {
return this.isInfinity() ? "<EC Point Infinity>" : "<EC Point x: " + this.x.fromRed().toString(16, 2) + " y: " + this.y.fromRed().toString(16, 2) + " z: " + this.z.fromRed().toString(16, 2) + ">";
};
c.prototype.isInfinity = function() {
return 0 === this.x.cmpn(0) && (0 === this.y.cmp(this.z) || this.zOne && 0 === this.y.cmp(this.curve.c));
};
c.prototype._extDbl = function() {
var e = this.x.redSqr(), t = this.y.redSqr(), i = this.z.redSqr();
i = i.redIAdd(i);
var n = this.curve._mulA(e), r = this.x.redAdd(this.y).redSqr().redISub(e).redISub(t), o = n.redAdd(t), a = o.redSub(i), s = n.redSub(t), c = r.redMul(a), l = o.redMul(s), h = r.redMul(s), u = a.redMul(o);
return this.curve.point(c, l, u, h);
};
c.prototype._projDbl = function() {
var e, t, i, n, r, o, a = this.x.redAdd(this.y).redSqr(), s = this.x.redSqr(), c = this.y.redSqr();
if (this.curve.twisted) {
var l = (n = this.curve._mulA(s)).redAdd(c);
if (this.zOne) {
e = a.redSub(s).redSub(c).redMul(l.redSub(this.curve.two));
t = l.redMul(n.redSub(c));
i = l.redSqr().redSub(l).redSub(l);
} else {
r = this.z.redSqr();
o = l.redSub(r).redISub(r);
e = a.redSub(s).redISub(c).redMul(o);
t = l.redMul(n.redSub(c));
i = l.redMul(o);
}
} else {
n = s.redAdd(c);
r = this.curve._mulC(this.z).redSqr();
o = n.redSub(r).redSub(r);
e = this.curve._mulC(a.redISub(n)).redMul(o);
t = this.curve._mulC(n).redMul(s.redISub(c));
i = n.redMul(o);
}
return this.curve.point(e, t, i);
};
c.prototype.dbl = function() {
return this.isInfinity() ? this : this.curve.extended ? this._extDbl() : this._projDbl();
};
c.prototype._extAdd = function(e) {
var t = this.y.redSub(this.x).redMul(e.y.redSub(e.x)), i = this.y.redAdd(this.x).redMul(e.y.redAdd(e.x)), n = this.t.redMul(this.curve.dd).redMul(e.t), r = this.z.redMul(e.z.redAdd(e.z)), o = i.redSub(t), a = r.redSub(n), s = r.redAdd(n), c = i.redAdd(t), l = o.redMul(a), h = s.redMul(c), u = o.redMul(c), f = a.redMul(s);
return this.curve.point(l, h, f, u);
};
c.prototype._projAdd = function(e) {
var t, i, n = this.z.redMul(e.z), r = n.redSqr(), o = this.x.redMul(e.x), a = this.y.redMul(e.y), s = this.curve.d.redMul(o).redMul(a), c = r.redSub(s), l = r.redAdd(s), h = this.x.redAdd(this.y).redMul(e.x.redAdd(e.y)).redISub(o).redISub(a), u = n.redMul(c).redMul(h);
if (this.curve.twisted) {
t = n.redMul(l).redMul(a.redSub(this.curve._mulA(o)));
i = c.redMul(l);
} else {
t = n.redMul(l).redMul(a.redSub(o));
i = this.curve._mulC(c).redMul(l);
}
return this.curve.point(u, t, i);
};
c.prototype.add = function(e) {
return this.isInfinity() ? e : e.isInfinity() ? this : this.curve.extended ? this._extAdd(e) : this._projAdd(e);
};
c.prototype.mul = function(e) {
return this._hasDoubles(e) ? this.curve._fixedNafMul(this, e) : this.curve._wnafMul(this, e);
};
c.prototype.mulAdd = function(e, t, i) {
return this.curve._wnafMulAdd(1, [ this, t ], [ e, i ], 2, !1);
};
c.prototype.jmulAdd = function(e, t, i) {
return this.curve._wnafMulAdd(1, [ this, t ], [ e, i ], 2, !0);
};
c.prototype.normalize = function() {
if (this.zOne) return this;
var e = this.z.redInvm();
this.x = this.x.redMul(e);
this.y = this.y.redMul(e);
this.t && (this.t = this.t.redMul(e));
this.z = this.curve.one;
this.zOne = !0;
return this;
};
c.prototype.neg = function() {
return this.curve.point(this.x.redNeg(), this.y, this.z, this.t && this.t.redNeg());
};
c.prototype.getX = function() {
this.normalize();
return this.x.fromRed();
};
c.prototype.getY = function() {
this.normalize();
return this.y.fromRed();
};
c.prototype.eq = function(e) {
return this === e || 0 === this.getX().cmp(e.getX()) && 0 === this.getY().cmp(e.getY());
};
c.prototype.eqXToP = function(e) {
var t = e.toRed(this.curve.red).redMul(this.z);
if (0 === this.x.cmp(t)) return !0;
for (var i = e.clone(), n = this.curve.redN.redMul(this.z); ;) {
i.iadd(this.curve.n);
if (i.cmp(this.curve.p) >= 0) return !1;
t.redIAdd(n);
if (0 === this.x.cmp(t)) return !0;
}
};
c.prototype.toP = c.prototype.normalize;
c.prototype.mixedAdd = c.prototype.add;
}, {
"../utils": 101,
"./base": 88,
"bn.js": 102,
inherits: 138
} ],
90: [ function(e, t, i) {
"use strict";
var n = i;
n.base = e("./base");
n.short = e("./short");
n.mont = e("./mont");
n.edwards = e("./edwards");
}, {
"./base": 88,
"./edwards": 89,
"./mont": 91,
"./short": 92
} ],
91: [ function(e, t) {
"use strict";
var i = e("bn.js"), n = e("inherits"), r = e("./base"), o = e("../utils");
function a(e) {
r.call(this, "mont", e);
this.a = new i(e.a, 16).toRed(this.red);
this.b = new i(e.b, 16).toRed(this.red);
this.i4 = new i(4).toRed(this.red).redInvm();
this.two = new i(2).toRed(this.red);
this.a24 = this.i4.redMul(this.a.redAdd(this.two));
}
n(a, r);
t.exports = a;
a.prototype.validate = function(e) {
var t = e.normalize().x, i = t.redSqr(), n = i.redMul(t).redAdd(i.redMul(this.a)).redAdd(t);
return 0 === n.redSqrt().redSqr().cmp(n);
};
function s(e, t, n) {
r.BasePoint.call(this, e, "projective");
if (null === t && null === n) {
this.x = this.curve.one;
this.z = this.curve.zero;
} else {
this.x = new i(t, 16);
this.z = new i(n, 16);
this.x.red || (this.x = this.x.toRed(this.curve.red));
this.z.red || (this.z = this.z.toRed(this.curve.red));
}
}
n(s, r.BasePoint);
a.prototype.decodePoint = function(e, t) {
return this.point(o.toArray(e, t), 1);
};
a.prototype.point = function(e, t) {
return new s(this, e, t);
};
a.prototype.pointFromJSON = function(e) {
return s.fromJSON(this, e);
};
s.prototype.precompute = function() {};
s.prototype._encode = function() {
return this.getX().toArray("be", this.curve.p.byteLength());
};
s.fromJSON = function(e, t) {
return new s(e, t[0], t[1] || e.one);
};
s.prototype.inspect = function() {
return this.isInfinity() ? "<EC Point Infinity>" : "<EC Point x: " + this.x.fromRed().toString(16, 2) + " z: " + this.z.fromRed().toString(16, 2) + ">";
};
s.prototype.isInfinity = function() {
return 0 === this.z.cmpn(0);
};
s.prototype.dbl = function() {
var e = this.x.redAdd(this.z).redSqr(), t = this.x.redSub(this.z).redSqr(), i = e.redSub(t), n = e.redMul(t), r = i.redMul(t.redAdd(this.curve.a24.redMul(i)));
return this.curve.point(n, r);
};
s.prototype.add = function() {
throw new Error("Not supported on Montgomery curve");
};
s.prototype.diffAdd = function(e, t) {
var i = this.x.redAdd(this.z), n = this.x.redSub(this.z), r = e.x.redAdd(e.z), o = e.x.redSub(e.z).redMul(i), a = r.redMul(n), s = t.z.redMul(o.redAdd(a).redSqr()), c = t.x.redMul(o.redISub(a).redSqr());
return this.curve.point(s, c);
};
s.prototype.mul = function(e) {
for (var t = e.clone(), i = this, n = this.curve.point(null, null), r = []; 0 !== t.cmpn(0); t.iushrn(1)) r.push(t.andln(1));
for (var o = r.length - 1; o >= 0; o--) if (0 === r[o]) {
i = i.diffAdd(n, this);
n = n.dbl();
} else {
n = i.diffAdd(n, this);
i = i.dbl();
}
return n;
};
s.prototype.mulAdd = function() {
throw new Error("Not supported on Montgomery curve");
};
s.prototype.jumlAdd = function() {
throw new Error("Not supported on Montgomery curve");
};
s.prototype.eq = function(e) {
return 0 === this.getX().cmp(e.getX());
};
s.prototype.normalize = function() {
this.x = this.x.redMul(this.z.redInvm());
this.z = this.curve.one;
return this;
};
s.prototype.getX = function() {
this.normalize();
return this.x.fromRed();
};
}, {
"../utils": 101,
"./base": 88,
"bn.js": 102,
inherits: 138
} ],
92: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("bn.js"), r = e("inherits"), o = e("./base"), a = i.assert;
function s(e) {
o.call(this, "short", e);
this.a = new n(e.a, 16).toRed(this.red);
this.b = new n(e.b, 16).toRed(this.red);
this.tinv = this.two.redInvm();
this.zeroA = 0 === this.a.fromRed().cmpn(0);
this.threeA = 0 === this.a.fromRed().sub(this.p).cmpn(-3);
this.endo = this._getEndomorphism(e);
this._endoWnafT1 = new Array(4);
this._endoWnafT2 = new Array(4);
}
r(s, o);
t.exports = s;
s.prototype._getEndomorphism = function(e) {
if (this.zeroA && this.g && this.n && 1 === this.p.modn(3)) {
var t, i;
if (e.beta) t = new n(e.beta, 16).toRed(this.red); else {
var r = this._getEndoRoots(this.p);
t = (t = r[0].cmp(r[1]) < 0 ? r[0] : r[1]).toRed(this.red);
}
if (e.lambda) i = new n(e.lambda, 16); else {
var o = this._getEndoRoots(this.n);
if (0 === this.g.mul(o[0]).x.cmp(this.g.x.redMul(t))) i = o[0]; else {
i = o[1];
a(0 === this.g.mul(i).x.cmp(this.g.x.redMul(t)));
}
}
return {
beta: t,
lambda: i,
basis: e.basis ? e.basis.map(function(e) {
return {
a: new n(e.a, 16),
b: new n(e.b, 16)
};
}) : this._getEndoBasis(i)
};
}
};
s.prototype._getEndoRoots = function(e) {
var t = e === this.p ? this.red : n.mont(e), i = new n(2).toRed(t).redInvm(), r = i.redNeg(), o = new n(3).toRed(t).redNeg().redSqrt().redMul(i);
return [ r.redAdd(o).fromRed(), r.redSub(o).fromRed() ];
};
s.prototype._getEndoBasis = function(e) {
for (var t, i, r, o, a, s, c, l, h, u = this.n.ushrn(Math.floor(this.n.bitLength() / 2)), f = e, d = this.n.clone(), p = new n(1), m = new n(0), g = new n(0), y = new n(1), b = 0; 0 !== f.cmpn(0); ) {
var v = d.div(f);
l = d.sub(v.mul(f));
h = g.sub(v.mul(p));
var _ = y.sub(v.mul(m));
if (!r && l.cmp(u) < 0) {
t = c.neg();
i = p;
r = l.neg();
o = h;
} else if (r && 2 == ++b) break;
c = l;
d = f;
f = l;
g = p;
p = h;
y = m;
m = _;
}
a = l.neg();
s = h;
var w = r.sqr().add(o.sqr());
if (a.sqr().add(s.sqr()).cmp(w) >= 0) {
a = t;
s = i;
}
if (r.negative) {
r = r.neg();
o = o.neg();
}
if (a.negative) {
a = a.neg();
s = s.neg();
}
return [ {
a: r,
b: o
}, {
a: a,
b: s
} ];
};
s.prototype._endoSplit = function(e) {
var t = this.endo.basis, i = t[0], n = t[1], r = n.b.mul(e).divRound(this.n), o = i.b.neg().mul(e).divRound(this.n), a = r.mul(i.a), s = o.mul(n.a), c = r.mul(i.b), l = o.mul(n.b);
return {
k1: e.sub(a).sub(s),
k2: c.add(l).neg()
};
};
s.prototype.pointFromX = function(e, t) {
(e = new n(e, 16)).red || (e = e.toRed(this.red));
var i = e.redSqr().redMul(e).redIAdd(e.redMul(this.a)).redIAdd(this.b), r = i.redSqrt();
if (0 !== r.redSqr().redSub(i).cmp(this.zero)) throw new Error("invalid point");
var o = r.fromRed().isOdd();
(t && !o || !t && o) && (r = r.redNeg());
return this.point(e, r);
};
s.prototype.validate = function(e) {
if (e.inf) return !0;
var t = e.x, i = e.y, n = this.a.redMul(t), r = t.redSqr().redMul(t).redIAdd(n).redIAdd(this.b);
return 0 === i.redSqr().redISub(r).cmpn(0);
};
s.prototype._endoWnafMulAdd = function(e, t, i) {
for (var n = this._endoWnafT1, r = this._endoWnafT2, o = 0; o < e.length; o++) {
var a = this._endoSplit(t[o]), s = e[o], c = s._getBeta();
if (a.k1.negative) {
a.k1.ineg();
s = s.neg(!0);
}
if (a.k2.negative) {
a.k2.ineg();
c = c.neg(!0);
}
n[2 * o] = s;
n[2 * o + 1] = c;
r[2 * o] = a.k1;
r[2 * o + 1] = a.k2;
}
for (var l = this._wnafMulAdd(1, n, r, 2 * o, i), h = 0; h < 2 * o; h++) {
n[h] = null;
r[h] = null;
}
return l;
};
function c(e, t, i, r) {
o.BasePoint.call(this, e, "affine");
if (null === t && null === i) {
this.x = null;
this.y = null;
this.inf = !0;
} else {
this.x = new n(t, 16);
this.y = new n(i, 16);
if (r) {
this.x.forceRed(this.curve.red);
this.y.forceRed(this.curve.red);
}
this.x.red || (this.x = this.x.toRed(this.curve.red));
this.y.red || (this.y = this.y.toRed(this.curve.red));
this.inf = !1;
}
}
r(c, o.BasePoint);
s.prototype.point = function(e, t, i) {
return new c(this, e, t, i);
};
s.prototype.pointFromJSON = function(e, t) {
return c.fromJSON(this, e, t);
};
c.prototype._getBeta = function() {
if (this.curve.endo) {
var e = this.precomputed;
if (e && e.beta) return e.beta;
var t = this.curve.point(this.x.redMul(this.curve.endo.beta), this.y);
if (e) {
var i = this.curve, n = function(e) {
return i.point(e.x.redMul(i.endo.beta), e.y);
};
e.beta = t;
t.precomputed = {
beta: null,
naf: e.naf && {
wnd: e.naf.wnd,
points: e.naf.points.map(n)
},
doubles: e.doubles && {
step: e.doubles.step,
points: e.doubles.points.map(n)
}
};
}
return t;
}
};
c.prototype.toJSON = function() {
return this.precomputed ? [ this.x, this.y, this.precomputed && {
doubles: this.precomputed.doubles && {
step: this.precomputed.doubles.step,
points: this.precomputed.doubles.points.slice(1)
},
naf: this.precomputed.naf && {
wnd: this.precomputed.naf.wnd,
points: this.precomputed.naf.points.slice(1)
}
} ] : [ this.x, this.y ];
};
c.fromJSON = function(e, t, i) {
"string" == typeof t && (t = JSON.parse(t));
var n = e.point(t[0], t[1], i);
if (!t[2]) return n;
function r(t) {
return e.point(t[0], t[1], i);
}
var o = t[2];
n.precomputed = {
beta: null,
doubles: o.doubles && {
step: o.doubles.step,
points: [ n ].concat(o.doubles.points.map(r))
},
naf: o.naf && {
wnd: o.naf.wnd,
points: [ n ].concat(o.naf.points.map(r))
}
};
return n;
};
c.prototype.inspect = function() {
return this.isInfinity() ? "<EC Point Infinity>" : "<EC Point x: " + this.x.fromRed().toString(16, 2) + " y: " + this.y.fromRed().toString(16, 2) + ">";
};
c.prototype.isInfinity = function() {
return this.inf;
};
c.prototype.add = function(e) {
if (this.inf) return e;
if (e.inf) return this;
if (this.eq(e)) return this.dbl();
if (this.neg().eq(e)) return this.curve.point(null, null);
if (0 === this.x.cmp(e.x)) return this.curve.point(null, null);
var t = this.y.redSub(e.y);
0 !== t.cmpn(0) && (t = t.redMul(this.x.redSub(e.x).redInvm()));
var i = t.redSqr().redISub(this.x).redISub(e.x), n = t.redMul(this.x.redSub(i)).redISub(this.y);
return this.curve.point(i, n);
};
c.prototype.dbl = function() {
if (this.inf) return this;
var e = this.y.redAdd(this.y);
if (0 === e.cmpn(0)) return this.curve.point(null, null);
var t = this.curve.a, i = this.x.redSqr(), n = e.redInvm(), r = i.redAdd(i).redIAdd(i).redIAdd(t).redMul(n), o = r.redSqr().redISub(this.x.redAdd(this.x)), a = r.redMul(this.x.redSub(o)).redISub(this.y);
return this.curve.point(o, a);
};
c.prototype.getX = function() {
return this.x.fromRed();
};
c.prototype.getY = function() {
return this.y.fromRed();
};
c.prototype.mul = function(e) {
e = new n(e, 16);
return this.isInfinity() ? this : this._hasDoubles(e) ? this.curve._fixedNafMul(this, e) : this.curve.endo ? this.curve._endoWnafMulAdd([ this ], [ e ]) : this.curve._wnafMul(this, e);
};
c.prototype.mulAdd = function(e, t, i) {
var n = [ this, t ], r = [ e, i ];
return this.curve.endo ? this.curve._endoWnafMulAdd(n, r) : this.curve._wnafMulAdd(1, n, r, 2);
};
c.prototype.jmulAdd = function(e, t, i) {
var n = [ this, t ], r = [ e, i ];
return this.curve.endo ? this.curve._endoWnafMulAdd(n, r, !0) : this.curve._wnafMulAdd(1, n, r, 2, !0);
};
c.prototype.eq = function(e) {
return this === e || this.inf === e.inf && (this.inf || 0 === this.x.cmp(e.x) && 0 === this.y.cmp(e.y));
};
c.prototype.neg = function(e) {
if (this.inf) return this;
var t = this.curve.point(this.x, this.y.redNeg());
if (e && this.precomputed) {
var i = this.precomputed, n = function(e) {
return e.neg();
};
t.precomputed = {
naf: i.naf && {
wnd: i.naf.wnd,
points: i.naf.points.map(n)
},
doubles: i.doubles && {
step: i.doubles.step,
points: i.doubles.points.map(n)
}
};
}
return t;
};
c.prototype.toJ = function() {
return this.inf ? this.curve.jpoint(null, null, null) : this.curve.jpoint(this.x, this.y, this.curve.one);
};
function l(e, t, i, r) {
o.BasePoint.call(this, e, "jacobian");
if (null === t && null === i && null === r) {
this.x = this.curve.one;
this.y = this.curve.one;
this.z = new n(0);
} else {
this.x = new n(t, 16);
this.y = new n(i, 16);
this.z = new n(r, 16);
}
this.x.red || (this.x = this.x.toRed(this.curve.red));
this.y.red || (this.y = this.y.toRed(this.curve.red));
this.z.red || (this.z = this.z.toRed(this.curve.red));
this.zOne = this.z === this.curve.one;
}
r(l, o.BasePoint);
s.prototype.jpoint = function(e, t, i) {
return new l(this, e, t, i);
};
l.prototype.toP = function() {
if (this.isInfinity()) return this.curve.point(null, null);
var e = this.z.redInvm(), t = e.redSqr(), i = this.x.redMul(t), n = this.y.redMul(t).redMul(e);
return this.curve.point(i, n);
};
l.prototype.neg = function() {
return this.curve.jpoint(this.x, this.y.redNeg(), this.z);
};
l.prototype.add = function(e) {
if (this.isInfinity()) return e;
if (e.isInfinity()) return this;
var t = e.z.redSqr(), i = this.z.redSqr(), n = this.x.redMul(t), r = e.x.redMul(i), o = this.y.redMul(t.redMul(e.z)), a = e.y.redMul(i.redMul(this.z)), s = n.redSub(r), c = o.redSub(a);
if (0 === s.cmpn(0)) return 0 !== c.cmpn(0) ? this.curve.jpoint(null, null, null) : this.dbl();
var l = s.redSqr(), h = l.redMul(s), u = n.redMul(l), f = c.redSqr().redIAdd(h).redISub(u).redISub(u), d = c.redMul(u.redISub(f)).redISub(o.redMul(h)), p = this.z.redMul(e.z).redMul(s);
return this.curve.jpoint(f, d, p);
};
l.prototype.mixedAdd = function(e) {
if (this.isInfinity()) return e.toJ();
if (e.isInfinity()) return this;
var t = this.z.redSqr(), i = this.x, n = e.x.redMul(t), r = this.y, o = e.y.redMul(t).redMul(this.z), a = i.redSub(n), s = r.redSub(o);
if (0 === a.cmpn(0)) return 0 !== s.cmpn(0) ? this.curve.jpoint(null, null, null) : this.dbl();
var c = a.redSqr(), l = c.redMul(a), h = i.redMul(c), u = s.redSqr().redIAdd(l).redISub(h).redISub(h), f = s.redMul(h.redISub(u)).redISub(r.redMul(l)), d = this.z.redMul(a);
return this.curve.jpoint(u, f, d);
};
l.prototype.dblp = function(e) {
if (0 === e) return this;
if (this.isInfinity()) return this;
if (!e) return this.dbl();
var t;
if (this.curve.zeroA || this.curve.threeA) {
var i = this;
for (t = 0; t < e; t++) i = i.dbl();
return i;
}
var n = this.curve.a, r = this.curve.tinv, o = this.x, a = this.y, s = this.z, c = s.redSqr().redSqr(), l = a.redAdd(a);
for (t = 0; t < e; t++) {
var h = o.redSqr(), u = l.redSqr(), f = u.redSqr(), d = h.redAdd(h).redIAdd(h).redIAdd(n.redMul(c)), p = o.redMul(u), m = d.redSqr().redISub(p.redAdd(p)), g = p.redISub(m), y = d.redMul(g);
y = y.redIAdd(y).redISub(f);
var b = l.redMul(s);
t + 1 < e && (c = c.redMul(f));
o = m;
s = b;
l = y;
}
return this.curve.jpoint(o, l.redMul(r), s);
};
l.prototype.dbl = function() {
return this.isInfinity() ? this : this.curve.zeroA ? this._zeroDbl() : this.curve.threeA ? this._threeDbl() : this._dbl();
};
l.prototype._zeroDbl = function() {
var e, t, i;
if (this.zOne) {
var n = this.x.redSqr(), r = this.y.redSqr(), o = r.redSqr(), a = this.x.redAdd(r).redSqr().redISub(n).redISub(o);
a = a.redIAdd(a);
var s = n.redAdd(n).redIAdd(n), c = s.redSqr().redISub(a).redISub(a), l = o.redIAdd(o);
l = (l = l.redIAdd(l)).redIAdd(l);
e = c;
t = s.redMul(a.redISub(c)).redISub(l);
i = this.y.redAdd(this.y);
} else {
var h = this.x.redSqr(), u = this.y.redSqr(), f = u.redSqr(), d = this.x.redAdd(u).redSqr().redISub(h).redISub(f);
d = d.redIAdd(d);
var p = h.redAdd(h).redIAdd(h), m = p.redSqr(), g = f.redIAdd(f);
g = (g = g.redIAdd(g)).redIAdd(g);
e = m.redISub(d).redISub(d);
t = p.redMul(d.redISub(e)).redISub(g);
i = (i = this.y.redMul(this.z)).redIAdd(i);
}
return this.curve.jpoint(e, t, i);
};
l.prototype._threeDbl = function() {
var e, t, i;
if (this.zOne) {
var n = this.x.redSqr(), r = this.y.redSqr(), o = r.redSqr(), a = this.x.redAdd(r).redSqr().redISub(n).redISub(o);
a = a.redIAdd(a);
var s = n.redAdd(n).redIAdd(n).redIAdd(this.curve.a), c = s.redSqr().redISub(a).redISub(a);
e = c;
var l = o.redIAdd(o);
l = (l = l.redIAdd(l)).redIAdd(l);
t = s.redMul(a.redISub(c)).redISub(l);
i = this.y.redAdd(this.y);
} else {
var h = this.z.redSqr(), u = this.y.redSqr(), f = this.x.redMul(u), d = this.x.redSub(h).redMul(this.x.redAdd(h));
d = d.redAdd(d).redIAdd(d);
var p = f.redIAdd(f), m = (p = p.redIAdd(p)).redAdd(p);
e = d.redSqr().redISub(m);
i = this.y.redAdd(this.z).redSqr().redISub(u).redISub(h);
var g = u.redSqr();
g = (g = (g = g.redIAdd(g)).redIAdd(g)).redIAdd(g);
t = d.redMul(p.redISub(e)).redISub(g);
}
return this.curve.jpoint(e, t, i);
};
l.prototype._dbl = function() {
var e = this.curve.a, t = this.x, i = this.y, n = this.z, r = n.redSqr().redSqr(), o = t.redSqr(), a = i.redSqr(), s = o.redAdd(o).redIAdd(o).redIAdd(e.redMul(r)), c = t.redAdd(t), l = (c = c.redIAdd(c)).redMul(a), h = s.redSqr().redISub(l.redAdd(l)), u = l.redISub(h), f = a.redSqr();
f = (f = (f = f.redIAdd(f)).redIAdd(f)).redIAdd(f);
var d = s.redMul(u).redISub(f), p = i.redAdd(i).redMul(n);
return this.curve.jpoint(h, d, p);
};
l.prototype.trpl = function() {
if (!this.curve.zeroA) return this.dbl().add(this);
var e = this.x.redSqr(), t = this.y.redSqr(), i = this.z.redSqr(), n = t.redSqr(), r = e.redAdd(e).redIAdd(e), o = r.redSqr(), a = this.x.redAdd(t).redSqr().redISub(e).redISub(n), s = (a = (a = (a = a.redIAdd(a)).redAdd(a).redIAdd(a)).redISub(o)).redSqr(), c = n.redIAdd(n);
c = (c = (c = c.redIAdd(c)).redIAdd(c)).redIAdd(c);
var l = r.redIAdd(a).redSqr().redISub(o).redISub(s).redISub(c), h = t.redMul(l);
h = (h = h.redIAdd(h)).redIAdd(h);
var u = this.x.redMul(s).redISub(h);
u = (u = u.redIAdd(u)).redIAdd(u);
var f = this.y.redMul(l.redMul(c.redISub(l)).redISub(a.redMul(s)));
f = (f = (f = f.redIAdd(f)).redIAdd(f)).redIAdd(f);
var d = this.z.redAdd(a).redSqr().redISub(i).redISub(s);
return this.curve.jpoint(u, f, d);
};
l.prototype.mul = function(e, t) {
e = new n(e, t);
return this.curve._wnafMul(this, e);
};
l.prototype.eq = function(e) {
if ("affine" === e.type) return this.eq(e.toJ());
if (this === e) return !0;
var t = this.z.redSqr(), i = e.z.redSqr();
if (0 !== this.x.redMul(i).redISub(e.x.redMul(t)).cmpn(0)) return !1;
var n = t.redMul(this.z), r = i.redMul(e.z);
return 0 === this.y.redMul(r).redISub(e.y.redMul(n)).cmpn(0);
};
l.prototype.eqXToP = function(e) {
var t = this.z.redSqr(), i = e.toRed(this.curve.red).redMul(t);
if (0 === this.x.cmp(i)) return !0;
for (var n = e.clone(), r = this.curve.redN.redMul(t); ;) {
n.iadd(this.curve.n);
if (n.cmp(this.curve.p) >= 0) return !1;
i.redIAdd(r);
if (0 === this.x.cmp(i)) return !0;
}
};
l.prototype.inspect = function() {
return this.isInfinity() ? "<EC JPoint Infinity>" : "<EC JPoint x: " + this.x.toString(16, 2) + " y: " + this.y.toString(16, 2) + " z: " + this.z.toString(16, 2) + ">";
};
l.prototype.isInfinity = function() {
return 0 === this.z.cmpn(0);
};
}, {
"../utils": 101,
"./base": 88,
"bn.js": 102,
inherits: 138
} ],
93: [ function(e, t, i) {
"use strict";
var n, r = i, o = e("hash.js"), a = e("./curve"), s = e("./utils").assert;
function c(e) {
"short" === e.type ? this.curve = new a.short(e) : "edwards" === e.type ? this.curve = new a.edwards(e) : this.curve = new a.mont(e);
this.g = this.curve.g;
this.n = this.curve.n;
this.hash = e.hash;
s(this.g.validate(), "Invalid curve");
s(this.g.mul(this.n).isInfinity(), "Invalid curve, G*N != O");
}
r.PresetCurve = c;
function l(e, t) {
Object.defineProperty(r, e, {
configurable: !0,
enumerable: !0,
get: function() {
var i = new c(t);
Object.defineProperty(r, e, {
configurable: !0,
enumerable: !0,
value: i
});
return i;
}
});
}
l("p192", {
type: "short",
prime: "p192",
p: "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff",
a: "ffffffff ffffffff ffffffff fffffffe ffffffff fffffffc",
b: "64210519 e59c80e7 0fa7e9ab 72243049 feb8deec c146b9b1",
n: "ffffffff ffffffff ffffffff 99def836 146bc9b1 b4d22831",
hash: o.sha256,
gRed: !1,
g: [ "188da80e b03090f6 7cbf20eb 43a18800 f4ff0afd 82ff1012", "07192b95 ffc8da78 631011ed 6b24cdd5 73f977a1 1e794811" ]
});
l("p224", {
type: "short",
prime: "p224",
p: "ffffffff ffffffff ffffffff ffffffff 00000000 00000000 00000001",
a: "ffffffff ffffffff ffffffff fffffffe ffffffff ffffffff fffffffe",
b: "b4050a85 0c04b3ab f5413256 5044b0b7 d7bfd8ba 270b3943 2355ffb4",
n: "ffffffff ffffffff ffffffff ffff16a2 e0b8f03e 13dd2945 5c5c2a3d",
hash: o.sha256,
gRed: !1,
g: [ "b70e0cbd 6bb4bf7f 321390b9 4a03c1d3 56c21122 343280d6 115c1d21", "bd376388 b5f723fb 4c22dfe6 cd4375a0 5a074764 44d58199 85007e34" ]
});
l("p256", {
type: "short",
prime: null,
p: "ffffffff 00000001 00000000 00000000 00000000 ffffffff ffffffff ffffffff",
a: "ffffffff 00000001 00000000 00000000 00000000 ffffffff ffffffff fffffffc",
b: "5ac635d8 aa3a93e7 b3ebbd55 769886bc 651d06b0 cc53b0f6 3bce3c3e 27d2604b",
n: "ffffffff 00000000 ffffffff ffffffff bce6faad a7179e84 f3b9cac2 fc632551",
hash: o.sha256,
gRed: !1,
g: [ "6b17d1f2 e12c4247 f8bce6e5 63a440f2 77037d81 2deb33a0 f4a13945 d898c296", "4fe342e2 fe1a7f9b 8ee7eb4a 7c0f9e16 2bce3357 6b315ece cbb64068 37bf51f5" ]
});
l("p384", {
type: "short",
prime: null,
p: "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe ffffffff 00000000 00000000 ffffffff",
a: "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe ffffffff 00000000 00000000 fffffffc",
b: "b3312fa7 e23ee7e4 988e056b e3f82d19 181d9c6e fe814112 0314088f 5013875a c656398d 8a2ed19d 2a85c8ed d3ec2aef",
n: "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff c7634d81 f4372ddf 581a0db2 48b0a77a ecec196a ccc52973",
hash: o.sha384,
gRed: !1,
g: [ "aa87ca22 be8b0537 8eb1c71e f320ad74 6e1d3b62 8ba79b98 59f741e0 82542a38 5502f25d bf55296c 3a545e38 72760ab7", "3617de4a 96262c6f 5d9e98bf 9292dc29 f8f41dbd 289a147c e9da3113 b5f0b8c0 0a60b1ce 1d7e819d 7a431d7c 90ea0e5f" ]
});
l("p521", {
type: "short",
prime: null,
p: "000001ff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff",
a: "000001ff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffc",
b: "00000051 953eb961 8e1c9a1f 929a21a0 b68540ee a2da725b 99b315f3 b8b48991 8ef109e1 56193951 ec7e937b 1652c0bd 3bb1bf07 3573df88 3d2c34f1 ef451fd4 6b503f00",
n: "000001ff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffa 51868783 bf2f966b 7fcc0148 f709a5d0 3bb5c9b8 899c47ae bb6fb71e 91386409",
hash: o.sha512,
gRed: !1,
g: [ "000000c6 858e06b7 0404e9cd 9e3ecb66 2395b442 9c648139 053fb521 f828af60 6b4d3dba a14b5e77 efe75928 fe1dc127 a2ffa8de 3348b3c1 856a429b f97e7e31 c2e5bd66", "00000118 39296a78 9a3bc004 5c8a5fb4 2c7d1bd9 98f54449 579b4468 17afbd17 273e662c 97ee7299 5ef42640 c550b901 3fad0761 353c7086 a272c240 88be9476 9fd16650" ]
});
l("curve25519", {
type: "mont",
prime: "p25519",
p: "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed",
a: "76d06",
b: "1",
n: "1000000000000000 0000000000000000 14def9dea2f79cd6 5812631a5cf5d3ed",
hash: o.sha256,
gRed: !1,
g: [ "9" ]
});
l("ed25519", {
type: "edwards",
prime: "p25519",
p: "7fffffffffffffff ffffffffffffffff ffffffffffffffff ffffffffffffffed",
a: "-1",
c: "1",
d: "52036cee2b6ffe73 8cc740797779e898 00700a4d4141d8ab 75eb4dca135978a3",
n: "1000000000000000 0000000000000000 14def9dea2f79cd6 5812631a5cf5d3ed",
hash: o.sha256,
gRed: !1,
g: [ "216936d3cd6e53fec0a4e231fdd6dc5c692cc7609525a7b2c9562d608f25d51a", "6666666666666666666666666666666666666666666666666666666666666658" ]
});
try {
n = e("./precomputed/secp256k1");
} catch (e) {
n = void 0;
}
l("secp256k1", {
type: "short",
prime: "k256",
p: "ffffffff ffffffff ffffffff ffffffff ffffffff ffffffff fffffffe fffffc2f",
a: "0",
b: "7",
n: "ffffffff ffffffff ffffffff fffffffe baaedce6 af48a03b bfd25e8c d0364141",
h: "1",
hash: o.sha256,
beta: "7ae96a2b657c07106e64479eac3434e99cf0497512f58995c1396c28719501ee",
lambda: "5363ad4cc05c30e0a5261c028812645a122e22ea20816678df02967c1b23bd72",
basis: [ {
a: "3086d221a7d46bcde86c90e49284eb15",
b: "-e4437ed6010e88286f547fa90abfe4c3"
}, {
a: "114ca50f7a8e2f3f657c1108d9d44cfd8",
b: "3086d221a7d46bcde86c90e49284eb15"
} ],
gRed: !1,
g: [ "79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798", "483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8", n ]
});
}, {
"./curve": 90,
"./precomputed/secp256k1": 100,
"./utils": 101,
"hash.js": 124
} ],
94: [ function(e, t) {
"use strict";
var i = e("bn.js"), n = e("hmac-drbg"), r = e("../utils"), o = e("../curves"), a = e("brorand"), s = r.assert, c = e("./key"), l = e("./signature");
function h(e) {
if (!(this instanceof h)) return new h(e);
if ("string" == typeof e) {
s(Object.prototype.hasOwnProperty.call(o, e), "Unknown curve " + e);
e = o[e];
}
e instanceof o.PresetCurve && (e = {
curve: e
});
this.curve = e.curve.curve;
this.n = this.curve.n;
this.nh = this.n.ushrn(1);
this.g = this.curve.g;
this.g = e.curve.g;
this.g.precompute(e.curve.n.bitLength() + 1);
this.hash = e.hash || e.curve.hash;
}
t.exports = h;
h.prototype.keyPair = function(e) {
return new c(this, e);
};
h.prototype.keyFromPrivate = function(e, t) {
return c.fromPrivate(this, e, t);
};
h.prototype.keyFromPublic = function(e, t) {
return c.fromPublic(this, e, t);
};
h.prototype.genKeyPair = function(e) {
e || (e = {});
for (var t = new n({
hash: this.hash,
pers: e.pers,
persEnc: e.persEnc || "utf8",
entropy: e.entropy || a(this.hash.hmacStrength),
entropyEnc: e.entropy && e.entropyEnc || "utf8",
nonce: this.n.toArray()
}), r = this.n.byteLength(), o = this.n.sub(new i(2)); ;) {
var s = new i(t.generate(r));
if (!(s.cmp(o) > 0)) {
s.iaddn(1);
return this.keyFromPrivate(s);
}
}
};
h.prototype._truncateToN = function(e, t) {
var i = 8 * e.byteLength() - this.n.bitLength();
i > 0 && (e = e.ushrn(i));
return !t && e.cmp(this.n) >= 0 ? e.sub(this.n) : e;
};
h.prototype.sign = function(e, t, r, o) {
if ("object" == typeof r) {
o = r;
r = null;
}
o || (o = {});
t = this.keyFromPrivate(t, r);
e = this._truncateToN(new i(e, 16));
for (var a = this.n.byteLength(), s = t.getPrivate().toArray("be", a), c = e.toArray("be", a), h = new n({
hash: this.hash,
entropy: s,
nonce: c,
pers: o.pers,
persEnc: o.persEnc || "utf8"
}), u = this.n.sub(new i(1)), f = 0; ;f++) {
var d = o.k ? o.k(f) : new i(h.generate(this.n.byteLength()));
if (!((d = this._truncateToN(d, !0)).cmpn(1) <= 0 || d.cmp(u) >= 0)) {
var p = this.g.mul(d);
if (!p.isInfinity()) {
var m = p.getX(), g = m.umod(this.n);
if (0 !== g.cmpn(0)) {
var y = d.invm(this.n).mul(g.mul(t.getPrivate()).iadd(e));
if (0 !== (y = y.umod(this.n)).cmpn(0)) {
var b = (p.getY().isOdd() ? 1 : 0) | (0 !== m.cmp(g) ? 2 : 0);
if (o.canonical && y.cmp(this.nh) > 0) {
y = this.n.sub(y);
b ^= 1;
}
return new l({
r: g,
s: y,
recoveryParam: b
});
}
}
}
}
}
};
h.prototype.verify = function(e, t, n, r) {
e = this._truncateToN(new i(e, 16));
n = this.keyFromPublic(n, r);
var o = (t = new l(t, "hex")).r, a = t.s;
if (o.cmpn(1) < 0 || o.cmp(this.n) >= 0) return !1;
if (a.cmpn(1) < 0 || a.cmp(this.n) >= 0) return !1;
var s, c = a.invm(this.n), h = c.mul(e).umod(this.n), u = c.mul(o).umod(this.n);
return this.curve._maxwellTrick ? !(s = this.g.jmulAdd(h, n.getPublic(), u)).isInfinity() && s.eqXToP(o) : !(s = this.g.mulAdd(h, n.getPublic(), u)).isInfinity() && 0 === s.getX().umod(this.n).cmp(o);
};
h.prototype.recoverPubKey = function(e, t, n, r) {
s((3 & n) === n, "The recovery param is more than two bits");
t = new l(t, r);
var o = this.n, a = new i(e), c = t.r, h = t.s, u = 1 & n, f = n >> 1;
if (c.cmp(this.curve.p.umod(this.curve.n)) >= 0 && f) throw new Error("Unable to find sencond key candinate");
c = f ? this.curve.pointFromX(c.add(this.curve.n), u) : this.curve.pointFromX(c, u);
var d = t.r.invm(o), p = o.sub(a).mul(d).umod(o), m = h.mul(d).umod(o);
return this.g.mulAdd(p, c, m);
};
h.prototype.getKeyRecoveryParam = function(e, t, i, n) {
if (null !== (t = new l(t, n)).recoveryParam) return t.recoveryParam;
for (var r = 0; r < 4; r++) {
var o;
try {
o = this.recoverPubKey(e, t, r);
} catch (e) {
continue;
}
if (o.eq(i)) return r;
}
throw new Error("Unable to find valid recovery factor");
};
}, {
"../curves": 93,
"../utils": 101,
"./key": 95,
"./signature": 96,
"bn.js": 102,
brorand: 18,
"hmac-drbg": 136
} ],
95: [ function(e, t) {
"use strict";
var i = e("bn.js"), n = e("../utils").assert;
function r(e, t) {
this.ec = e;
this.priv = null;
this.pub = null;
t.priv && this._importPrivate(t.priv, t.privEnc);
t.pub && this._importPublic(t.pub, t.pubEnc);
}
t.exports = r;
r.fromPublic = function(e, t, i) {
return t instanceof r ? t : new r(e, {
pub: t,
pubEnc: i
});
};
r.fromPrivate = function(e, t, i) {
return t instanceof r ? t : new r(e, {
priv: t,
privEnc: i
});
};
r.prototype.validate = function() {
var e = this.getPublic();
return e.isInfinity() ? {
result: !1,
reason: "Invalid public key"
} : e.validate() ? e.mul(this.ec.curve.n).isInfinity() ? {
result: !0,
reason: null
} : {
result: !1,
reason: "Public key * N != O"
} : {
result: !1,
reason: "Public key is not a point"
};
};
r.prototype.getPublic = function(e, t) {
if ("string" == typeof e) {
t = e;
e = null;
}
this.pub || (this.pub = this.ec.g.mul(this.priv));
return t ? this.pub.encode(t, e) : this.pub;
};
r.prototype.getPrivate = function(e) {
return "hex" === e ? this.priv.toString(16, 2) : this.priv;
};
r.prototype._importPrivate = function(e, t) {
this.priv = new i(e, t || 16);
this.priv = this.priv.umod(this.ec.curve.n);
};
r.prototype._importPublic = function(e, t) {
if (e.x || e.y) {
"mont" === this.ec.curve.type ? n(e.x, "Need x coordinate") : "short" !== this.ec.curve.type && "edwards" !== this.ec.curve.type || n(e.x && e.y, "Need both x and y coordinate");
this.pub = this.ec.curve.point(e.x, e.y);
} else this.pub = this.ec.curve.decodePoint(e, t);
};
r.prototype.derive = function(e) {
e.validate() || n(e.validate(), "public point not validated");
return e.mul(this.priv).getX();
};
r.prototype.sign = function(e, t, i) {
return this.ec.sign(e, this, t, i);
};
r.prototype.verify = function(e, t) {
return this.ec.verify(e, t, this);
};
r.prototype.inspect = function() {
return "<Key priv: " + (this.priv && this.priv.toString(16, 2)) + " pub: " + (this.pub && this.pub.inspect()) + " >";
};
}, {
"../utils": 101,
"bn.js": 102
} ],
96: [ function(e, t) {
"use strict";
var i = e("bn.js"), n = e("../utils"), r = n.assert;
function o(e, t) {
if (e instanceof o) return e;
if (!this._importDER(e, t)) {
r(e.r && e.s, "Signature without r or s");
this.r = new i(e.r, 16);
this.s = new i(e.s, 16);
void 0 === e.recoveryParam ? this.recoveryParam = null : this.recoveryParam = e.recoveryParam;
}
}
t.exports = o;
function a() {
this.place = 0;
}
function s(e, t) {
var i = e[t.place++];
if (!(128 & i)) return i;
var n = 15 & i;
if (0 === n || n > 4) return !1;
for (var r = 0, o = 0, a = t.place; o < n; o++, a++) {
r <<= 8;
r |= e[a];
r >>>= 0;
}
if (r <= 127) return !1;
t.place = a;
return r;
}
function c(e) {
for (var t = 0, i = e.length - 1; !e[t] && !(128 & e[t + 1]) && t < i; ) t++;
return 0 === t ? e : e.slice(t);
}
o.prototype._importDER = function(e, t) {
e = n.toArray(e, t);
var r = new a();
if (48 !== e[r.place++]) return !1;
var o = s(e, r);
if (!1 === o) return !1;
if (o + r.place !== e.length) return !1;
if (2 !== e[r.place++]) return !1;
var c = s(e, r);
if (!1 === c) return !1;
var l = e.slice(r.place, c + r.place);
r.place += c;
if (2 !== e[r.place++]) return !1;
var h = s(e, r);
if (!1 === h) return !1;
if (e.length !== h + r.place) return !1;
var u = e.slice(r.place, h + r.place);
if (0 === l[0]) {
if (!(128 & l[1])) return !1;
l = l.slice(1);
}
if (0 === u[0]) {
if (!(128 & u[1])) return !1;
u = u.slice(1);
}
this.r = new i(l);
this.s = new i(u);
this.recoveryParam = null;
return !0;
};
function l(e, t) {
if (t < 128) e.push(t); else {
var i = 1 + (Math.log(t) / Math.LN2 >>> 3);
e.push(128 | i);
for (;--i; ) e.push(t >>> (i << 3) & 255);
e.push(t);
}
}
o.prototype.toDER = function(e) {
var t = this.r.toArray(), i = this.s.toArray();
128 & t[0] && (t = [ 0 ].concat(t));
128 & i[0] && (i = [ 0 ].concat(i));
t = c(t);
i = c(i);
for (;!(i[0] || 128 & i[1]); ) i = i.slice(1);
var r = [ 2 ];
l(r, t.length);
(r = r.concat(t)).push(2);
l(r, i.length);
var o = r.concat(i), a = [ 48 ];
l(a, o.length);
a = a.concat(o);
return n.encode(a, e);
};
}, {
"../utils": 101,
"bn.js": 102
} ],
97: [ function(e, t) {
"use strict";
var i = e("hash.js"), n = e("../curves"), r = e("../utils"), o = r.assert, a = r.parseBytes, s = e("./key"), c = e("./signature");
function l(e) {
o("ed25519" === e, "only tested with ed25519 so far");
if (!(this instanceof l)) return new l(e);
e = n[e].curve;
this.curve = e;
this.g = e.g;
this.g.precompute(e.n.bitLength() + 1);
this.pointClass = e.point().constructor;
this.encodingLength = Math.ceil(e.n.bitLength() / 8);
this.hash = i.sha512;
}
t.exports = l;
l.prototype.sign = function(e, t) {
e = a(e);
var i = this.keyFromSecret(t), n = this.hashInt(i.messagePrefix(), e), r = this.g.mul(n), o = this.encodePoint(r), s = this.hashInt(o, i.pubBytes(), e).mul(i.priv()), c = n.add(s).umod(this.curve.n);
return this.makeSignature({
R: r,
S: c,
Rencoded: o
});
};
l.prototype.verify = function(e, t, i) {
e = a(e);
t = this.makeSignature(t);
var n = this.keyFromPublic(i), r = this.hashInt(t.Rencoded(), n.pubBytes(), e), o = this.g.mul(t.S());
return t.R().add(n.pub().mul(r)).eq(o);
};
l.prototype.hashInt = function() {
for (var e = this.hash(), t = 0; t < arguments.length; t++) e.update(arguments[t]);
return r.intFromLE(e.digest()).umod(this.curve.n);
};
l.prototype.keyFromPublic = function(e) {
return s.fromPublic(this, e);
};
l.prototype.keyFromSecret = function(e) {
return s.fromSecret(this, e);
};
l.prototype.makeSignature = function(e) {
return e instanceof c ? e : new c(this, e);
};
l.prototype.encodePoint = function(e) {
var t = e.getY().toArray("le", this.encodingLength);
t[this.encodingLength - 1] |= e.getX().isOdd() ? 128 : 0;
return t;
};
l.prototype.decodePoint = function(e) {
var t = (e = r.parseBytes(e)).length - 1, i = e.slice(0, t).concat(-129 & e[t]), n = 0 != (128 & e[t]), o = r.intFromLE(i);
return this.curve.pointFromY(o, n);
};
l.prototype.encodeInt = function(e) {
return e.toArray("le", this.encodingLength);
};
l.prototype.decodeInt = function(e) {
return r.intFromLE(e);
};
l.prototype.isPoint = function(e) {
return e instanceof this.pointClass;
};
}, {
"../curves": 93,
"../utils": 101,
"./key": 98,
"./signature": 99,
"hash.js": 124
} ],
98: [ function(e, t) {
"use strict";
var i = e("../utils"), n = i.assert, r = i.parseBytes, o = i.cachedProperty;
function a(e, t) {
this.eddsa = e;
this._secret = r(t.secret);
e.isPoint(t.pub) ? this._pub = t.pub : this._pubBytes = r(t.pub);
}
a.fromPublic = function(e, t) {
return t instanceof a ? t : new a(e, {
pub: t
});
};
a.fromSecret = function(e, t) {
return t instanceof a ? t : new a(e, {
secret: t
});
};
a.prototype.secret = function() {
return this._secret;
};
o(a, "pubBytes", function() {
return this.eddsa.encodePoint(this.pub());
});
o(a, "pub", function() {
return this._pubBytes ? this.eddsa.decodePoint(this._pubBytes) : this.eddsa.g.mul(this.priv());
});
o(a, "privBytes", function() {
var e = this.eddsa, t = this.hash(), i = e.encodingLength - 1, n = t.slice(0, e.encodingLength);
n[0] &= 248;
n[i] &= 127;
n[i] |= 64;
return n;
});
o(a, "priv", function() {
return this.eddsa.decodeInt(this.privBytes());
});
o(a, "hash", function() {
return this.eddsa.hash().update(this.secret()).digest();
});
o(a, "messagePrefix", function() {
return this.hash().slice(this.eddsa.encodingLength);
});
a.prototype.sign = function(e) {
n(this._secret, "KeyPair can only verify");
return this.eddsa.sign(e, this);
};
a.prototype.verify = function(e, t) {
return this.eddsa.verify(e, t, this);
};
a.prototype.getSecret = function(e) {
n(this._secret, "KeyPair is public only");
return i.encode(this.secret(), e);
};
a.prototype.getPublic = function(e) {
return i.encode(this.pubBytes(), e);
};
t.exports = a;
}, {
"../utils": 101
} ],
99: [ function(e, t) {
"use strict";
var i = e("bn.js"), n = e("../utils"), r = n.assert, o = n.cachedProperty, a = n.parseBytes;
function s(e, t) {
this.eddsa = e;
"object" != typeof t && (t = a(t));
Array.isArray(t) && (t = {
R: t.slice(0, e.encodingLength),
S: t.slice(e.encodingLength)
});
r(t.R && t.S, "Signature without R or S");
e.isPoint(t.R) && (this._R = t.R);
t.S instanceof i && (this._S = t.S);
this._Rencoded = Array.isArray(t.R) ? t.R : t.Rencoded;
this._Sencoded = Array.isArray(t.S) ? t.S : t.Sencoded;
}
o(s, "S", function() {
return this.eddsa.decodeInt(this.Sencoded());
});
o(s, "R", function() {
return this.eddsa.decodePoint(this.Rencoded());
});
o(s, "Rencoded", function() {
return this.eddsa.encodePoint(this.R());
});
o(s, "Sencoded", function() {
return this.eddsa.encodeInt(this.S());
});
s.prototype.toBytes = function() {
return this.Rencoded().concat(this.Sencoded());
};
s.prototype.toHex = function() {
return n.encode(this.toBytes(), "hex").toUpperCase();
};
t.exports = s;
}, {
"../utils": 101,
"bn.js": 102
} ],
100: [ function(e, t) {
t.exports = {
doubles: {
step: 4,
points: [ [ "e60fce93b59e9ec53011aabc21c23e97b2a31369b87a5ae9c44ee89e2a6dec0a", "f7e3507399e595929db99f34f57937101296891e44d23f0be1f32cce69616821" ], [ "8282263212c609d9ea2a6e3e172de238d8c39cabd5ac1ca10646e23fd5f51508", "11f8a8098557dfe45e8256e830b60ace62d613ac2f7b17bed31b6eaff6e26caf" ], [ "175e159f728b865a72f99cc6c6fc846de0b93833fd2222ed73fce5b551e5b739", "d3506e0d9e3c79eba4ef97a51ff71f5eacb5955add24345c6efa6ffee9fed695" ], [ "363d90d447b00c9c99ceac05b6262ee053441c7e55552ffe526bad8f83ff4640", "4e273adfc732221953b445397f3363145b9a89008199ecb62003c7f3bee9de9" ], [ "8b4b5f165df3c2be8c6244b5b745638843e4a781a15bcd1b69f79a55dffdf80c", "4aad0a6f68d308b4b3fbd7813ab0da04f9e336546162ee56b3eff0c65fd4fd36" ], [ "723cbaa6e5db996d6bf771c00bd548c7b700dbffa6c0e77bcb6115925232fcda", "96e867b5595cc498a921137488824d6e2660a0653779494801dc069d9eb39f5f" ], [ "eebfa4d493bebf98ba5feec812c2d3b50947961237a919839a533eca0e7dd7fa", "5d9a8ca3970ef0f269ee7edaf178089d9ae4cdc3a711f712ddfd4fdae1de8999" ], [ "100f44da696e71672791d0a09b7bde459f1215a29b3c03bfefd7835b39a48db0", "cdd9e13192a00b772ec8f3300c090666b7ff4a18ff5195ac0fbd5cd62bc65a09" ], [ "e1031be262c7ed1b1dc9227a4a04c017a77f8d4464f3b3852c8acde6e534fd2d", "9d7061928940405e6bb6a4176597535af292dd419e1ced79a44f18f29456a00d" ], [ "feea6cae46d55b530ac2839f143bd7ec5cf8b266a41d6af52d5e688d9094696d", "e57c6b6c97dce1bab06e4e12bf3ecd5c981c8957cc41442d3155debf18090088" ], [ "da67a91d91049cdcb367be4be6ffca3cfeed657d808583de33fa978bc1ec6cb1", "9bacaa35481642bc41f463f7ec9780e5dec7adc508f740a17e9ea8e27a68be1d" ], [ "53904faa0b334cdda6e000935ef22151ec08d0f7bb11069f57545ccc1a37b7c0", "5bc087d0bc80106d88c9eccac20d3c1c13999981e14434699dcb096b022771c8" ], [ "8e7bcd0bd35983a7719cca7764ca906779b53a043a9b8bcaeff959f43ad86047", "10b7770b2a3da4b3940310420ca9514579e88e2e47fd68b3ea10047e8460372a" ], [ "385eed34c1cdff21e6d0818689b81bde71a7f4f18397e6690a841e1599c43862", "283bebc3e8ea23f56701de19e9ebf4576b304eec2086dc8cc0458fe5542e5453" ], [ "6f9d9b803ecf191637c73a4413dfa180fddf84a5947fbc9c606ed86c3fac3a7", "7c80c68e603059ba69b8e2a30e45c4d47ea4dd2f5c281002d86890603a842160" ], [ "3322d401243c4e2582a2147c104d6ecbf774d163db0f5e5313b7e0e742d0e6bd", "56e70797e9664ef5bfb019bc4ddaf9b72805f63ea2873af624f3a2e96c28b2a0" ], [ "85672c7d2de0b7da2bd1770d89665868741b3f9af7643397721d74d28134ab83", "7c481b9b5b43b2eb6374049bfa62c2e5e77f17fcc5298f44c8e3094f790313a6" ], [ "948bf809b1988a46b06c9f1919413b10f9226c60f668832ffd959af60c82a0a", "53a562856dcb6646dc6b74c5d1c3418c6d4dff08c97cd2bed4cb7f88d8c8e589" ], [ "6260ce7f461801c34f067ce0f02873a8f1b0e44dfc69752accecd819f38fd8e8", "bc2da82b6fa5b571a7f09049776a1ef7ecd292238051c198c1a84e95b2b4ae17" ], [ "e5037de0afc1d8d43d8348414bbf4103043ec8f575bfdc432953cc8d2037fa2d", "4571534baa94d3b5f9f98d09fb990bddbd5f5b03ec481f10e0e5dc841d755bda" ], [ "e06372b0f4a207adf5ea905e8f1771b4e7e8dbd1c6a6c5b725866a0ae4fce725", "7a908974bce18cfe12a27bb2ad5a488cd7484a7787104870b27034f94eee31dd" ], [ "213c7a715cd5d45358d0bbf9dc0ce02204b10bdde2a3f58540ad6908d0559754", "4b6dad0b5ae462507013ad06245ba190bb4850f5f36a7eeddff2c27534b458f2" ], [ "4e7c272a7af4b34e8dbb9352a5419a87e2838c70adc62cddf0cc3a3b08fbd53c", "17749c766c9d0b18e16fd09f6def681b530b9614bff7dd33e0b3941817dcaae6" ], [ "fea74e3dbe778b1b10f238ad61686aa5c76e3db2be43057632427e2840fb27b6", "6e0568db9b0b13297cf674deccb6af93126b596b973f7b77701d3db7f23cb96f" ], [ "76e64113f677cf0e10a2570d599968d31544e179b760432952c02a4417bdde39", "c90ddf8dee4e95cf577066d70681f0d35e2a33d2b56d2032b4b1752d1901ac01" ], [ "c738c56b03b2abe1e8281baa743f8f9a8f7cc643df26cbee3ab150242bcbb891", "893fb578951ad2537f718f2eacbfbbbb82314eef7880cfe917e735d9699a84c3" ], [ "d895626548b65b81e264c7637c972877d1d72e5f3a925014372e9f6588f6c14b", "febfaa38f2bc7eae728ec60818c340eb03428d632bb067e179363ed75d7d991f" ], [ "b8da94032a957518eb0f6433571e8761ceffc73693e84edd49150a564f676e03", "2804dfa44805a1e4d7c99cc9762808b092cc584d95ff3b511488e4e74efdf6e7" ], [ "e80fea14441fb33a7d8adab9475d7fab2019effb5156a792f1a11778e3c0df5d", "eed1de7f638e00771e89768ca3ca94472d155e80af322ea9fcb4291b6ac9ec78" ], [ "a301697bdfcd704313ba48e51d567543f2a182031efd6915ddc07bbcc4e16070", "7370f91cfb67e4f5081809fa25d40f9b1735dbf7c0a11a130c0d1a041e177ea1" ], [ "90ad85b389d6b936463f9d0512678de208cc330b11307fffab7ac63e3fb04ed4", "e507a3620a38261affdcbd9427222b839aefabe1582894d991d4d48cb6ef150" ], [ "8f68b9d2f63b5f339239c1ad981f162ee88c5678723ea3351b7b444c9ec4c0da", "662a9f2dba063986de1d90c2b6be215dbbea2cfe95510bfdf23cbf79501fff82" ], [ "e4f3fb0176af85d65ff99ff9198c36091f48e86503681e3e6686fd5053231e11", "1e63633ad0ef4f1c1661a6d0ea02b7286cc7e74ec951d1c9822c38576feb73bc" ], [ "8c00fa9b18ebf331eb961537a45a4266c7034f2f0d4e1d0716fb6eae20eae29e", "efa47267fea521a1a9dc343a3736c974c2fadafa81e36c54e7d2a4c66702414b" ], [ "e7a26ce69dd4829f3e10cec0a9e98ed3143d084f308b92c0997fddfc60cb3e41", "2a758e300fa7984b471b006a1aafbb18d0a6b2c0420e83e20e8a9421cf2cfd51" ], [ "b6459e0ee3662ec8d23540c223bcbdc571cbcb967d79424f3cf29eb3de6b80ef", "67c876d06f3e06de1dadf16e5661db3c4b3ae6d48e35b2ff30bf0b61a71ba45" ], [ "d68a80c8280bb840793234aa118f06231d6f1fc67e73c5a5deda0f5b496943e8", "db8ba9fff4b586d00c4b1f9177b0e28b5b0e7b8f7845295a294c84266b133120" ], [ "324aed7df65c804252dc0270907a30b09612aeb973449cea4095980fc28d3d5d", "648a365774b61f2ff130c0c35aec1f4f19213b0c7e332843967224af96ab7c84" ], [ "4df9c14919cde61f6d51dfdbe5fee5dceec4143ba8d1ca888e8bd373fd054c96", "35ec51092d8728050974c23a1d85d4b5d506cdc288490192ebac06cad10d5d" ], [ "9c3919a84a474870faed8a9c1cc66021523489054d7f0308cbfc99c8ac1f98cd", "ddb84f0f4a4ddd57584f044bf260e641905326f76c64c8e6be7e5e03d4fc599d" ], [ "6057170b1dd12fdf8de05f281d8e06bb91e1493a8b91d4cc5a21382120a959e5", "9a1af0b26a6a4807add9a2daf71df262465152bc3ee24c65e899be932385a2a8" ], [ "a576df8e23a08411421439a4518da31880cef0fba7d4df12b1a6973eecb94266", "40a6bf20e76640b2c92b97afe58cd82c432e10a7f514d9f3ee8be11ae1b28ec8" ], [ "7778a78c28dec3e30a05fe9629de8c38bb30d1f5cf9a3a208f763889be58ad71", "34626d9ab5a5b22ff7098e12f2ff580087b38411ff24ac563b513fc1fd9f43ac" ], [ "928955ee637a84463729fd30e7afd2ed5f96274e5ad7e5cb09eda9c06d903ac", "c25621003d3f42a827b78a13093a95eeac3d26efa8a8d83fc5180e935bcd091f" ], [ "85d0fef3ec6db109399064f3a0e3b2855645b4a907ad354527aae75163d82751", "1f03648413a38c0be29d496e582cf5663e8751e96877331582c237a24eb1f962" ], [ "ff2b0dce97eece97c1c9b6041798b85dfdfb6d8882da20308f5404824526087e", "493d13fef524ba188af4c4dc54d07936c7b7ed6fb90e2ceb2c951e01f0c29907" ], [ "827fbbe4b1e880ea9ed2b2e6301b212b57f1ee148cd6dd28780e5e2cf856e241", "c60f9c923c727b0b71bef2c67d1d12687ff7a63186903166d605b68baec293ec" ], [ "eaa649f21f51bdbae7be4ae34ce6e5217a58fdce7f47f9aa7f3b58fa2120e2b3", "be3279ed5bbbb03ac69a80f89879aa5a01a6b965f13f7e59d47a5305ba5ad93d" ], [ "e4a42d43c5cf169d9391df6decf42ee541b6d8f0c9a137401e23632dda34d24f", "4d9f92e716d1c73526fc99ccfb8ad34ce886eedfa8d8e4f13a7f7131deba9414" ], [ "1ec80fef360cbdd954160fadab352b6b92b53576a88fea4947173b9d4300bf19", "aeefe93756b5340d2f3a4958a7abbf5e0146e77f6295a07b671cdc1cc107cefd" ], [ "146a778c04670c2f91b00af4680dfa8bce3490717d58ba889ddb5928366642be", "b318e0ec3354028add669827f9d4b2870aaa971d2f7e5ed1d0b297483d83efd0" ], [ "fa50c0f61d22e5f07e3acebb1aa07b128d0012209a28b9776d76a8793180eef9", "6b84c6922397eba9b72cd2872281a68a5e683293a57a213b38cd8d7d3f4f2811" ], [ "da1d61d0ca721a11b1a5bf6b7d88e8421a288ab5d5bba5220e53d32b5f067ec2", "8157f55a7c99306c79c0766161c91e2966a73899d279b48a655fba0f1ad836f1" ], [ "a8e282ff0c9706907215ff98e8fd416615311de0446f1e062a73b0610d064e13", "7f97355b8db81c09abfb7f3c5b2515888b679a3e50dd6bd6cef7c73111f4cc0c" ], [ "174a53b9c9a285872d39e56e6913cab15d59b1fa512508c022f382de8319497c", "ccc9dc37abfc9c1657b4155f2c47f9e6646b3a1d8cb9854383da13ac079afa73" ], [ "959396981943785c3d3e57edf5018cdbe039e730e4918b3d884fdff09475b7ba", "2e7e552888c331dd8ba0386a4b9cd6849c653f64c8709385e9b8abf87524f2fd" ], [ "d2a63a50ae401e56d645a1153b109a8fcca0a43d561fba2dbb51340c9d82b151", "e82d86fb6443fcb7565aee58b2948220a70f750af484ca52d4142174dcf89405" ], [ "64587e2335471eb890ee7896d7cfdc866bacbdbd3839317b3436f9b45617e073", "d99fcdd5bf6902e2ae96dd6447c299a185b90a39133aeab358299e5e9faf6589" ], [ "8481bde0e4e4d885b3a546d3e549de042f0aa6cea250e7fd358d6c86dd45e458", "38ee7b8cba5404dd84a25bf39cecb2ca900a79c42b262e556d64b1b59779057e" ], [ "13464a57a78102aa62b6979ae817f4637ffcfed3c4b1ce30bcd6303f6caf666b", "69be159004614580ef7e433453ccb0ca48f300a81d0942e13f495a907f6ecc27" ], [ "bc4a9df5b713fe2e9aef430bcc1dc97a0cd9ccede2f28588cada3a0d2d83f366", "d3a81ca6e785c06383937adf4b798caa6e8a9fbfa547b16d758d666581f33c1" ], [ "8c28a97bf8298bc0d23d8c749452a32e694b65e30a9472a3954ab30fe5324caa", "40a30463a3305193378fedf31f7cc0eb7ae784f0451cb9459e71dc73cbef9482" ], [ "8ea9666139527a8c1dd94ce4f071fd23c8b350c5a4bb33748c4ba111faccae0", "620efabbc8ee2782e24e7c0cfb95c5d735b783be9cf0f8e955af34a30e62b945" ], [ "dd3625faef5ba06074669716bbd3788d89bdde815959968092f76cc4eb9a9787", "7a188fa3520e30d461da2501045731ca941461982883395937f68d00c644a573" ], [ "f710d79d9eb962297e4f6232b40e8f7feb2bc63814614d692c12de752408221e", "ea98e67232d3b3295d3b535532115ccac8612c721851617526ae47a9c77bfc82" ] ]
},
naf: {
wnd: 7,
points: [ [ "f9308a019258c31049344f85f89d5229b531c845836f99b08601f113bce036f9", "388f7b0f632de8140fe337e62a37f3566500a99934c2231b6cb9fd7584b8e672" ], [ "2f8bde4d1a07209355b4a7250a5c5128e88b84bddc619ab7cba8d569b240efe4", "d8ac222636e5e3d6d4dba9dda6c9c426f788271bab0d6840dca87d3aa6ac62d6" ], [ "5cbdf0646e5db4eaa398f365f2ea7a0e3d419b7e0330e39ce92bddedcac4f9bc", "6aebca40ba255960a3178d6d861a54dba813d0b813fde7b5a5082628087264da" ], [ "acd484e2f0c7f65309ad178a9f559abde09796974c57e714c35f110dfc27ccbe", "cc338921b0a7d9fd64380971763b61e9add888a4375f8e0f05cc262ac64f9c37" ], [ "774ae7f858a9411e5ef4246b70c65aac5649980be5c17891bbec17895da008cb", "d984a032eb6b5e190243dd56d7b7b365372db1e2dff9d6a8301d74c9c953c61b" ], [ "f28773c2d975288bc7d1d205c3748651b075fbc6610e58cddeeddf8f19405aa8", "ab0902e8d880a89758212eb65cdaf473a1a06da521fa91f29b5cb52db03ed81" ], [ "d7924d4f7d43ea965a465ae3095ff41131e5946f3c85f79e44adbcf8e27e080e", "581e2872a86c72a683842ec228cc6defea40af2bd896d3a5c504dc9ff6a26b58" ], [ "defdea4cdb677750a420fee807eacf21eb9898ae79b9768766e4faa04a2d4a34", "4211ab0694635168e997b0ead2a93daeced1f4a04a95c0f6cfb199f69e56eb77" ], [ "2b4ea0a797a443d293ef5cff444f4979f06acfebd7e86d277475656138385b6c", "85e89bc037945d93b343083b5a1c86131a01f60c50269763b570c854e5c09b7a" ], [ "352bbf4a4cdd12564f93fa332ce333301d9ad40271f8107181340aef25be59d5", "321eb4075348f534d59c18259dda3e1f4a1b3b2e71b1039c67bd3d8bcf81998c" ], [ "2fa2104d6b38d11b0230010559879124e42ab8dfeff5ff29dc9cdadd4ecacc3f", "2de1068295dd865b64569335bd5dd80181d70ecfc882648423ba76b532b7d67" ], [ "9248279b09b4d68dab21a9b066edda83263c3d84e09572e269ca0cd7f5453714", "73016f7bf234aade5d1aa71bdea2b1ff3fc0de2a887912ffe54a32ce97cb3402" ], [ "daed4f2be3a8bf278e70132fb0beb7522f570e144bf615c07e996d443dee8729", "a69dce4a7d6c98e8d4a1aca87ef8d7003f83c230f3afa726ab40e52290be1c55" ], [ "c44d12c7065d812e8acf28d7cbb19f9011ecd9e9fdf281b0e6a3b5e87d22e7db", "2119a460ce326cdc76c45926c982fdac0e106e861edf61c5a039063f0e0e6482" ], [ "6a245bf6dc698504c89a20cfded60853152b695336c28063b61c65cbd269e6b4", "e022cf42c2bd4a708b3f5126f16a24ad8b33ba48d0423b6efd5e6348100d8a82" ], [ "1697ffa6fd9de627c077e3d2fe541084ce13300b0bec1146f95ae57f0d0bd6a5", "b9c398f186806f5d27561506e4557433a2cf15009e498ae7adee9d63d01b2396" ], [ "605bdb019981718b986d0f07e834cb0d9deb8360ffb7f61df982345ef27a7479", "2972d2de4f8d20681a78d93ec96fe23c26bfae84fb14db43b01e1e9056b8c49" ], [ "62d14dab4150bf497402fdc45a215e10dcb01c354959b10cfe31c7e9d87ff33d", "80fc06bd8cc5b01098088a1950eed0db01aa132967ab472235f5642483b25eaf" ], [ "80c60ad0040f27dade5b4b06c408e56b2c50e9f56b9b8b425e555c2f86308b6f", "1c38303f1cc5c30f26e66bad7fe72f70a65eed4cbe7024eb1aa01f56430bd57a" ], [ "7a9375ad6167ad54aa74c6348cc54d344cc5dc9487d847049d5eabb0fa03c8fb", "d0e3fa9eca8726909559e0d79269046bdc59ea10c70ce2b02d499ec224dc7f7" ], [ "d528ecd9b696b54c907a9ed045447a79bb408ec39b68df504bb51f459bc3ffc9", "eecf41253136e5f99966f21881fd656ebc4345405c520dbc063465b521409933" ], [ "49370a4b5f43412ea25f514e8ecdad05266115e4a7ecb1387231808f8b45963", "758f3f41afd6ed428b3081b0512fd62a54c3f3afbb5b6764b653052a12949c9a" ], [ "77f230936ee88cbbd73df930d64702ef881d811e0e1498e2f1c13eb1fc345d74", "958ef42a7886b6400a08266e9ba1b37896c95330d97077cbbe8eb3c7671c60d6" ], [ "f2dac991cc4ce4b9ea44887e5c7c0bce58c80074ab9d4dbaeb28531b7739f530", "e0dedc9b3b2f8dad4da1f32dec2531df9eb5fbeb0598e4fd1a117dba703a3c37" ], [ "463b3d9f662621fb1b4be8fbbe2520125a216cdfc9dae3debcba4850c690d45b", "5ed430d78c296c3543114306dd8622d7c622e27c970a1de31cb377b01af7307e" ], [ "f16f804244e46e2a09232d4aff3b59976b98fac14328a2d1a32496b49998f247", "cedabd9b82203f7e13d206fcdf4e33d92a6c53c26e5cce26d6579962c4e31df6" ], [ "caf754272dc84563b0352b7a14311af55d245315ace27c65369e15f7151d41d1", "cb474660ef35f5f2a41b643fa5e460575f4fa9b7962232a5c32f908318a04476" ], [ "2600ca4b282cb986f85d0f1709979d8b44a09c07cb86d7c124497bc86f082120", "4119b88753c15bd6a693b03fcddbb45d5ac6be74ab5f0ef44b0be9475a7e4b40" ], [ "7635ca72d7e8432c338ec53cd12220bc01c48685e24f7dc8c602a7746998e435", "91b649609489d613d1d5e590f78e6d74ecfc061d57048bad9e76f302c5b9c61" ], [ "754e3239f325570cdbbf4a87deee8a66b7f2b33479d468fbc1a50743bf56cc18", "673fb86e5bda30fb3cd0ed304ea49a023ee33d0197a695d0c5d98093c536683" ], [ "e3e6bd1071a1e96aff57859c82d570f0330800661d1c952f9fe2694691d9b9e8", "59c9e0bba394e76f40c0aa58379a3cb6a5a2283993e90c4167002af4920e37f5" ], [ "186b483d056a033826ae73d88f732985c4ccb1f32ba35f4b4cc47fdcf04aa6eb", "3b952d32c67cf77e2e17446e204180ab21fb8090895138b4a4a797f86e80888b" ], [ "df9d70a6b9876ce544c98561f4be4f725442e6d2b737d9c91a8321724ce0963f", "55eb2dafd84d6ccd5f862b785dc39d4ab157222720ef9da217b8c45cf2ba2417" ], [ "5edd5cc23c51e87a497ca815d5dce0f8ab52554f849ed8995de64c5f34ce7143", "efae9c8dbc14130661e8cec030c89ad0c13c66c0d17a2905cdc706ab7399a868" ], [ "290798c2b6476830da12fe02287e9e777aa3fba1c355b17a722d362f84614fba", "e38da76dcd440621988d00bcf79af25d5b29c094db2a23146d003afd41943e7a" ], [ "af3c423a95d9f5b3054754efa150ac39cd29552fe360257362dfdecef4053b45", "f98a3fd831eb2b749a93b0e6f35cfb40c8cd5aa667a15581bc2feded498fd9c6" ], [ "766dbb24d134e745cccaa28c99bf274906bb66b26dcf98df8d2fed50d884249a", "744b1152eacbe5e38dcc887980da38b897584a65fa06cedd2c924f97cbac5996" ], [ "59dbf46f8c94759ba21277c33784f41645f7b44f6c596a58ce92e666191abe3e", "c534ad44175fbc300f4ea6ce648309a042ce739a7919798cd85e216c4a307f6e" ], [ "f13ada95103c4537305e691e74e9a4a8dd647e711a95e73cb62dc6018cfd87b8", "e13817b44ee14de663bf4bc808341f326949e21a6a75c2570778419bdaf5733d" ], [ "7754b4fa0e8aced06d4167a2c59cca4cda1869c06ebadfb6488550015a88522c", "30e93e864e669d82224b967c3020b8fa8d1e4e350b6cbcc537a48b57841163a2" ], [ "948dcadf5990e048aa3874d46abef9d701858f95de8041d2a6828c99e2262519", "e491a42537f6e597d5d28a3224b1bc25df9154efbd2ef1d2cbba2cae5347d57e" ], [ "7962414450c76c1689c7b48f8202ec37fb224cf5ac0bfa1570328a8a3d7c77ab", "100b610ec4ffb4760d5c1fc133ef6f6b12507a051f04ac5760afa5b29db83437" ], [ "3514087834964b54b15b160644d915485a16977225b8847bb0dd085137ec47ca", "ef0afbb2056205448e1652c48e8127fc6039e77c15c2378b7e7d15a0de293311" ], [ "d3cc30ad6b483e4bc79ce2c9dd8bc54993e947eb8df787b442943d3f7b527eaf", "8b378a22d827278d89c5e9be8f9508ae3c2ad46290358630afb34db04eede0a4" ], [ "1624d84780732860ce1c78fcbfefe08b2b29823db913f6493975ba0ff4847610", "68651cf9b6da903e0914448c6cd9d4ca896878f5282be4c8cc06e2a404078575" ], [ "733ce80da955a8a26902c95633e62a985192474b5af207da6df7b4fd5fc61cd4", "f5435a2bd2badf7d485a4d8b8db9fcce3e1ef8e0201e4578c54673bc1dc5ea1d" ], [ "15d9441254945064cf1a1c33bbd3b49f8966c5092171e699ef258dfab81c045c", "d56eb30b69463e7234f5137b73b84177434800bacebfc685fc37bbe9efe4070d" ], [ "a1d0fcf2ec9de675b612136e5ce70d271c21417c9d2b8aaaac138599d0717940", "edd77f50bcb5a3cab2e90737309667f2641462a54070f3d519212d39c197a629" ], [ "e22fbe15c0af8ccc5780c0735f84dbe9a790badee8245c06c7ca37331cb36980", "a855babad5cd60c88b430a69f53a1a7a38289154964799be43d06d77d31da06" ], [ "311091dd9860e8e20ee13473c1155f5f69635e394704eaa74009452246cfa9b3", "66db656f87d1f04fffd1f04788c06830871ec5a64feee685bd80f0b1286d8374" ], [ "34c1fd04d301be89b31c0442d3e6ac24883928b45a9340781867d4232ec2dbdf", "9414685e97b1b5954bd46f730174136d57f1ceeb487443dc5321857ba73abee" ], [ "f219ea5d6b54701c1c14de5b557eb42a8d13f3abbcd08affcc2a5e6b049b8d63", "4cb95957e83d40b0f73af4544cccf6b1f4b08d3c07b27fb8d8c2962a400766d1" ], [ "d7b8740f74a8fbaab1f683db8f45de26543a5490bca627087236912469a0b448", "fa77968128d9c92ee1010f337ad4717eff15db5ed3c049b3411e0315eaa4593b" ], [ "32d31c222f8f6f0ef86f7c98d3a3335ead5bcd32abdd94289fe4d3091aa824bf", "5f3032f5892156e39ccd3d7915b9e1da2e6dac9e6f26e961118d14b8462e1661" ], [ "7461f371914ab32671045a155d9831ea8793d77cd59592c4340f86cbc18347b5", "8ec0ba238b96bec0cbdddcae0aa442542eee1ff50c986ea6b39847b3cc092ff6" ], [ "ee079adb1df1860074356a25aa38206a6d716b2c3e67453d287698bad7b2b2d6", "8dc2412aafe3be5c4c5f37e0ecc5f9f6a446989af04c4e25ebaac479ec1c8c1e" ], [ "16ec93e447ec83f0467b18302ee620f7e65de331874c9dc72bfd8616ba9da6b5", "5e4631150e62fb40d0e8c2a7ca5804a39d58186a50e497139626778e25b0674d" ], [ "eaa5f980c245f6f038978290afa70b6bd8855897f98b6aa485b96065d537bd99", "f65f5d3e292c2e0819a528391c994624d784869d7e6ea67fb18041024edc07dc" ], [ "78c9407544ac132692ee1910a02439958ae04877151342ea96c4b6b35a49f51", "f3e0319169eb9b85d5404795539a5e68fa1fbd583c064d2462b675f194a3ddb4" ], [ "494f4be219a1a77016dcd838431aea0001cdc8ae7a6fc688726578d9702857a5", "42242a969283a5f339ba7f075e36ba2af925ce30d767ed6e55f4b031880d562c" ], [ "a598a8030da6d86c6bc7f2f5144ea549d28211ea58faa70ebf4c1e665c1fe9b5", "204b5d6f84822c307e4b4a7140737aec23fc63b65b35f86a10026dbd2d864e6b" ], [ "c41916365abb2b5d09192f5f2dbeafec208f020f12570a184dbadc3e58595997", "4f14351d0087efa49d245b328984989d5caf9450f34bfc0ed16e96b58fa9913" ], [ "841d6063a586fa475a724604da03bc5b92a2e0d2e0a36acfe4c73a5514742881", "73867f59c0659e81904f9a1c7543698e62562d6744c169ce7a36de01a8d6154" ], [ "5e95bb399a6971d376026947f89bde2f282b33810928be4ded112ac4d70e20d5", "39f23f366809085beebfc71181313775a99c9aed7d8ba38b161384c746012865" ], [ "36e4641a53948fd476c39f8a99fd974e5ec07564b5315d8bf99471bca0ef2f66", "d2424b1b1abe4eb8164227b085c9aa9456ea13493fd563e06fd51cf5694c78fc" ], [ "336581ea7bfbbb290c191a2f507a41cf5643842170e914faeab27c2c579f726", "ead12168595fe1be99252129b6e56b3391f7ab1410cd1e0ef3dcdcabd2fda224" ], [ "8ab89816dadfd6b6a1f2634fcf00ec8403781025ed6890c4849742706bd43ede", "6fdcef09f2f6d0a044e654aef624136f503d459c3e89845858a47a9129cdd24e" ], [ "1e33f1a746c9c5778133344d9299fcaa20b0938e8acff2544bb40284b8c5fb94", "60660257dd11b3aa9c8ed618d24edff2306d320f1d03010e33a7d2057f3b3b6" ], [ "85b7c1dcb3cec1b7ee7f30ded79dd20a0ed1f4cc18cbcfcfa410361fd8f08f31", "3d98a9cdd026dd43f39048f25a8847f4fcafad1895d7a633c6fed3c35e999511" ], [ "29df9fbd8d9e46509275f4b125d6d45d7fbe9a3b878a7af872a2800661ac5f51", "b4c4fe99c775a606e2d8862179139ffda61dc861c019e55cd2876eb2a27d84b" ], [ "a0b1cae06b0a847a3fea6e671aaf8adfdfe58ca2f768105c8082b2e449fce252", "ae434102edde0958ec4b19d917a6a28e6b72da1834aff0e650f049503a296cf2" ], [ "4e8ceafb9b3e9a136dc7ff67e840295b499dfb3b2133e4ba113f2e4c0e121e5", "cf2174118c8b6d7a4b48f6d534ce5c79422c086a63460502b827ce62a326683c" ], [ "d24a44e047e19b6f5afb81c7ca2f69080a5076689a010919f42725c2b789a33b", "6fb8d5591b466f8fc63db50f1c0f1c69013f996887b8244d2cdec417afea8fa3" ], [ "ea01606a7a6c9cdd249fdfcfacb99584001edd28abbab77b5104e98e8e3b35d4", "322af4908c7312b0cfbfe369f7a7b3cdb7d4494bc2823700cfd652188a3ea98d" ], [ "af8addbf2b661c8a6c6328655eb96651252007d8c5ea31be4ad196de8ce2131f", "6749e67c029b85f52a034eafd096836b2520818680e26ac8f3dfbcdb71749700" ], [ "e3ae1974566ca06cc516d47e0fb165a674a3dabcfca15e722f0e3450f45889", "2aeabe7e4531510116217f07bf4d07300de97e4874f81f533420a72eeb0bd6a4" ], [ "591ee355313d99721cf6993ffed1e3e301993ff3ed258802075ea8ced397e246", "b0ea558a113c30bea60fc4775460c7901ff0b053d25ca2bdeee98f1a4be5d196" ], [ "11396d55fda54c49f19aa97318d8da61fa8584e47b084945077cf03255b52984", "998c74a8cd45ac01289d5833a7beb4744ff536b01b257be4c5767bea93ea57a4" ], [ "3c5d2a1ba39c5a1790000738c9e0c40b8dcdfd5468754b6405540157e017aa7a", "b2284279995a34e2f9d4de7396fc18b80f9b8b9fdd270f6661f79ca4c81bd257" ], [ "cc8704b8a60a0defa3a99a7299f2e9c3fbc395afb04ac078425ef8a1793cc030", "bdd46039feed17881d1e0862db347f8cf395b74fc4bcdc4e940b74e3ac1f1b13" ], [ "c533e4f7ea8555aacd9777ac5cad29b97dd4defccc53ee7ea204119b2889b197", "6f0a256bc5efdf429a2fb6242f1a43a2d9b925bb4a4b3a26bb8e0f45eb596096" ], [ "c14f8f2ccb27d6f109f6d08d03cc96a69ba8c34eec07bbcf566d48e33da6593", "c359d6923bb398f7fd4473e16fe1c28475b740dd098075e6c0e8649113dc3a38" ], [ "a6cbc3046bc6a450bac24789fa17115a4c9739ed75f8f21ce441f72e0b90e6ef", "21ae7f4680e889bb130619e2c0f95a360ceb573c70603139862afd617fa9b9f" ], [ "347d6d9a02c48927ebfb86c1359b1caf130a3c0267d11ce6344b39f99d43cc38", "60ea7f61a353524d1c987f6ecec92f086d565ab687870cb12689ff1e31c74448" ], [ "da6545d2181db8d983f7dcb375ef5866d47c67b1bf31c8cf855ef7437b72656a", "49b96715ab6878a79e78f07ce5680c5d6673051b4935bd897fea824b77dc208a" ], [ "c40747cc9d012cb1a13b8148309c6de7ec25d6945d657146b9d5994b8feb1111", "5ca560753be2a12fc6de6caf2cb489565db936156b9514e1bb5e83037e0fa2d4" ], [ "4e42c8ec82c99798ccf3a610be870e78338c7f713348bd34c8203ef4037f3502", "7571d74ee5e0fb92a7a8b33a07783341a5492144cc54bcc40a94473693606437" ], [ "3775ab7089bc6af823aba2e1af70b236d251cadb0c86743287522a1b3b0dedea", "be52d107bcfa09d8bcb9736a828cfa7fac8db17bf7a76a2c42ad961409018cf7" ], [ "cee31cbf7e34ec379d94fb814d3d775ad954595d1314ba8846959e3e82f74e26", "8fd64a14c06b589c26b947ae2bcf6bfa0149ef0be14ed4d80f448a01c43b1c6d" ], [ "b4f9eaea09b6917619f6ea6a4eb5464efddb58fd45b1ebefcdc1a01d08b47986", "39e5c9925b5a54b07433a4f18c61726f8bb131c012ca542eb24a8ac07200682a" ], [ "d4263dfc3d2df923a0179a48966d30ce84e2515afc3dccc1b77907792ebcc60e", "62dfaf07a0f78feb30e30d6295853ce189e127760ad6cf7fae164e122a208d54" ], [ "48457524820fa65a4f8d35eb6930857c0032acc0a4a2de422233eeda897612c4", "25a748ab367979d98733c38a1fa1c2e7dc6cc07db2d60a9ae7a76aaa49bd0f77" ], [ "dfeeef1881101f2cb11644f3a2afdfc2045e19919152923f367a1767c11cceda", "ecfb7056cf1de042f9420bab396793c0c390bde74b4bbdff16a83ae09a9a7517" ], [ "6d7ef6b17543f8373c573f44e1f389835d89bcbc6062ced36c82df83b8fae859", "cd450ec335438986dfefa10c57fea9bcc521a0959b2d80bbf74b190dca712d10" ], [ "e75605d59102a5a2684500d3b991f2e3f3c88b93225547035af25af66e04541f", "f5c54754a8f71ee540b9b48728473e314f729ac5308b06938360990e2bfad125" ], [ "eb98660f4c4dfaa06a2be453d5020bc99a0c2e60abe388457dd43fefb1ed620c", "6cb9a8876d9cb8520609af3add26cd20a0a7cd8a9411131ce85f44100099223e" ], [ "13e87b027d8514d35939f2e6892b19922154596941888336dc3563e3b8dba942", "fef5a3c68059a6dec5d624114bf1e91aac2b9da568d6abeb2570d55646b8adf1" ], [ "ee163026e9fd6fe017c38f06a5be6fc125424b371ce2708e7bf4491691e5764a", "1acb250f255dd61c43d94ccc670d0f58f49ae3fa15b96623e5430da0ad6c62b2" ], [ "b268f5ef9ad51e4d78de3a750c2dc89b1e626d43505867999932e5db33af3d80", "5f310d4b3c99b9ebb19f77d41c1dee018cf0d34fd4191614003e945a1216e423" ], [ "ff07f3118a9df035e9fad85eb6c7bfe42b02f01ca99ceea3bf7ffdba93c4750d", "438136d603e858a3a5c440c38eccbaddc1d2942114e2eddd4740d098ced1f0d8" ], [ "8d8b9855c7c052a34146fd20ffb658bea4b9f69e0d825ebec16e8c3ce2b526a1", "cdb559eedc2d79f926baf44fb84ea4d44bcf50fee51d7ceb30e2e7f463036758" ], [ "52db0b5384dfbf05bfa9d472d7ae26dfe4b851ceca91b1eba54263180da32b63", "c3b997d050ee5d423ebaf66a6db9f57b3180c902875679de924b69d84a7b375" ], [ "e62f9490d3d51da6395efd24e80919cc7d0f29c3f3fa48c6fff543becbd43352", "6d89ad7ba4876b0b22c2ca280c682862f342c8591f1daf5170e07bfd9ccafa7d" ], [ "7f30ea2476b399b4957509c88f77d0191afa2ff5cb7b14fd6d8e7d65aaab1193", "ca5ef7d4b231c94c3b15389a5f6311e9daff7bb67b103e9880ef4bff637acaec" ], [ "5098ff1e1d9f14fb46a210fada6c903fef0fb7b4a1dd1d9ac60a0361800b7a00", "9731141d81fc8f8084d37c6e7542006b3ee1b40d60dfe5362a5b132fd17ddc0" ], [ "32b78c7de9ee512a72895be6b9cbefa6e2f3c4ccce445c96b9f2c81e2778ad58", "ee1849f513df71e32efc3896ee28260c73bb80547ae2275ba497237794c8753c" ], [ "e2cb74fddc8e9fbcd076eef2a7c72b0ce37d50f08269dfc074b581550547a4f7", "d3aa2ed71c9dd2247a62df062736eb0baddea9e36122d2be8641abcb005cc4a4" ], [ "8438447566d4d7bedadc299496ab357426009a35f235cb141be0d99cd10ae3a8", "c4e1020916980a4da5d01ac5e6ad330734ef0d7906631c4f2390426b2edd791f" ], [ "4162d488b89402039b584c6fc6c308870587d9c46f660b878ab65c82c711d67e", "67163e903236289f776f22c25fb8a3afc1732f2b84b4e95dbda47ae5a0852649" ], [ "3fad3fa84caf0f34f0f89bfd2dcf54fc175d767aec3e50684f3ba4a4bf5f683d", "cd1bc7cb6cc407bb2f0ca647c718a730cf71872e7d0d2a53fa20efcdfe61826" ], [ "674f2600a3007a00568c1a7ce05d0816c1fb84bf1370798f1c69532faeb1a86b", "299d21f9413f33b3edf43b257004580b70db57da0b182259e09eecc69e0d38a5" ], [ "d32f4da54ade74abb81b815ad1fb3b263d82d6c692714bcff87d29bd5ee9f08f", "f9429e738b8e53b968e99016c059707782e14f4535359d582fc416910b3eea87" ], [ "30e4e670435385556e593657135845d36fbb6931f72b08cb1ed954f1e3ce3ff6", "462f9bce619898638499350113bbc9b10a878d35da70740dc695a559eb88db7b" ], [ "be2062003c51cc3004682904330e4dee7f3dcd10b01e580bf1971b04d4cad297", "62188bc49d61e5428573d48a74e1c655b1c61090905682a0d5558ed72dccb9bc" ], [ "93144423ace3451ed29e0fb9ac2af211cb6e84a601df5993c419859fff5df04a", "7c10dfb164c3425f5c71a3f9d7992038f1065224f72bb9d1d902a6d13037b47c" ], [ "b015f8044f5fcbdcf21ca26d6c34fb8197829205c7b7d2a7cb66418c157b112c", "ab8c1e086d04e813744a655b2df8d5f83b3cdc6faa3088c1d3aea1454e3a1d5f" ], [ "d5e9e1da649d97d89e4868117a465a3a4f8a18de57a140d36b3f2af341a21b52", "4cb04437f391ed73111a13cc1d4dd0db1693465c2240480d8955e8592f27447a" ], [ "d3ae41047dd7ca065dbf8ed77b992439983005cd72e16d6f996a5316d36966bb", "bd1aeb21ad22ebb22a10f0303417c6d964f8cdd7df0aca614b10dc14d125ac46" ], [ "463e2763d885f958fc66cdd22800f0a487197d0a82e377b49f80af87c897b065", "bfefacdb0e5d0fd7df3a311a94de062b26b80c61fbc97508b79992671ef7ca7f" ], [ "7985fdfd127c0567c6f53ec1bb63ec3158e597c40bfe747c83cddfc910641917", "603c12daf3d9862ef2b25fe1de289aed24ed291e0ec6708703a5bd567f32ed03" ], [ "74a1ad6b5f76e39db2dd249410eac7f99e74c59cb83d2d0ed5ff1543da7703e9", "cc6157ef18c9c63cd6193d83631bbea0093e0968942e8c33d5737fd790e0db08" ], [ "30682a50703375f602d416664ba19b7fc9bab42c72747463a71d0896b22f6da3", "553e04f6b018b4fa6c8f39e7f311d3176290d0e0f19ca73f17714d9977a22ff8" ], [ "9e2158f0d7c0d5f26c3791efefa79597654e7a2b2464f52b1ee6c1347769ef57", "712fcdd1b9053f09003a3481fa7762e9ffd7c8ef35a38509e2fbf2629008373" ], [ "176e26989a43c9cfeba4029c202538c28172e566e3c4fce7322857f3be327d66", "ed8cc9d04b29eb877d270b4878dc43c19aefd31f4eee09ee7b47834c1fa4b1c3" ], [ "75d46efea3771e6e68abb89a13ad747ecf1892393dfc4f1b7004788c50374da8", "9852390a99507679fd0b86fd2b39a868d7efc22151346e1a3ca4726586a6bed8" ], [ "809a20c67d64900ffb698c4c825f6d5f2310fb0451c869345b7319f645605721", "9e994980d9917e22b76b061927fa04143d096ccc54963e6a5ebfa5f3f8e286c1" ], [ "1b38903a43f7f114ed4500b4eac7083fdefece1cf29c63528d563446f972c180", "4036edc931a60ae889353f77fd53de4a2708b26b6f5da72ad3394119daf408f9" ] ]
}
};
}, {} ],
101: [ function(e, t, i) {
"use strict";
var n = i, r = e("bn.js"), o = e("minimalistic-assert"), a = e("minimalistic-crypto-utils");
n.assert = o;
n.toArray = a.toArray;
n.zero2 = a.zero2;
n.toHex = a.toHex;
n.encode = a.encode;
n.getNAF = function(e, t, i) {
var n = new Array(Math.max(e.bitLength(), i) + 1);
n.fill(0);
for (var r = 1 << t + 1, o = e.clone(), a = 0; a < n.length; a++) {
var s, c = o.andln(r - 1);
if (o.isOdd()) {
s = c > (r >> 1) - 1 ? (r >> 1) - c : c;
o.isubn(s);
} else s = 0;
n[a] = s;
o.iushrn(1);
}
return n;
};
n.getJSF = function(e, t) {
var i = [ [], [] ];
e = e.clone();
t = t.clone();
for (var n, r = 0, o = 0; e.cmpn(-r) > 0 || t.cmpn(-o) > 0; ) {
var a, s, c = e.andln(3) + r & 3, l = t.andln(3) + o & 3;
3 === c && (c = -1);
3 === l && (l = -1);
a = 0 == (1 & c) ? 0 : 3 != (n = e.andln(7) + r & 7) && 5 !== n || 2 !== l ? c : -c;
i[0].push(a);
s = 0 == (1 & l) ? 0 : 3 != (n = t.andln(7) + o & 7) && 5 !== n || 2 !== c ? l : -l;
i[1].push(s);
2 * r === a + 1 && (r = 1 - r);
2 * o === s + 1 && (o = 1 - o);
e.iushrn(1);
t.iushrn(1);
}
return i;
};
n.cachedProperty = function(e, t, i) {
var n = "_" + t;
e.prototype[t] = function() {
return void 0 !== this[n] ? this[n] : this[n] = i.call(this);
};
};
n.parseBytes = function(e) {
return "string" == typeof e ? n.toArray(e, "hex") : e;
};
n.intFromLE = function(e) {
return new r(e, "hex", "le");
};
}, {
"bn.js": 102,
"minimalistic-assert": 142,
"minimalistic-crypto-utils": 143
} ],
102: [ function(e, t, i) {
arguments[4][15][0].apply(i, arguments);
}, {
buffer: 19,
dup: 15
} ],
103: [ function(e, t) {
t.exports = {
_from: "elliptic@^6.5.3",
_id: "elliptic@6.5.4",
_inBundle: !1,
_integrity: "sha512-iLhC6ULemrljPZb+QutR5TQGB+pdW6KGD5RSegS+8sorOZT+rdQFbsQFJgvN3eRqNALqJer4oQ16YvJHlU8hzQ==",
_location: "/elliptic",
_phantomChildren: {},
_requested: {
type: "range",
registry: !0,
raw: "elliptic@^6.5.3",
name: "elliptic",
escapedName: "elliptic",
rawSpec: "^6.5.3",
saveSpec: null,
fetchSpec: "^6.5.3"
},
_requiredBy: [ "/browserify-sign", "/create-ecdh" ],
_resolved: "https://registry.npmjs.org/elliptic/-/elliptic-6.5.4.tgz",
_shasum: "da37cebd31e79a1367e941b592ed1fbebd58abbb",
_spec: "elliptic@^6.5.3",
_where: "/Users/nantas/fireball-x/fireball_2.4.11/dist/CocosCreator.app/Contents/Resources/app/node_modules/browserify-sign",
author: {
name: "Fedor Indutny",
email: "fedor@indutny.com"
},
bugs: {
url: "https://github.com/indutny/elliptic/issues"
},
bundleDependencies: !1,
dependencies: {
"bn.js": "^4.11.9",
brorand: "^1.1.0",
"hash.js": "^1.0.0",
"hmac-drbg": "^1.0.1",
inherits: "^2.0.4",
"minimalistic-assert": "^1.0.1",
"minimalistic-crypto-utils": "^1.0.1"
},
deprecated: !1,
description: "EC cryptography",
devDependencies: {
brfs: "^2.0.2",
coveralls: "^3.1.0",
eslint: "^7.6.0",
grunt: "^1.2.1",
"grunt-browserify": "^5.3.0",
"grunt-cli": "^1.3.2",
"grunt-contrib-connect": "^3.0.0",
"grunt-contrib-copy": "^1.0.0",
"grunt-contrib-uglify": "^5.0.0",
"grunt-mocha-istanbul": "^5.0.2",
"grunt-saucelabs": "^9.0.1",
istanbul: "^0.4.5",
mocha: "^8.0.1"
},
files: [ "lib" ],
homepage: "https://github.com/indutny/elliptic",
keywords: [ "EC", "Elliptic", "curve", "Cryptography" ],
license: "MIT",
main: "lib/elliptic.js",
name: "elliptic",
repository: {
type: "git",
url: "git+ssh://git@github.com/indutny/elliptic.git"
},
scripts: {
lint: "eslint lib test",
"lint:fix": "npm run lint -- --fix",
test: "npm run lint && npm run unit",
unit: "istanbul test _mocha --reporter=spec test/index.js",
version: "grunt dist && git add dist/"
},
version: "6.5.4"
};
}, {} ],
104: [ function(e, t) {
function i() {
this._events = this._events || {};
this._maxListeners = this._maxListeners || void 0;
}
t.exports = i;
i.EventEmitter = i;
i.prototype._events = void 0;
i.prototype._maxListeners = void 0;
i.defaultMaxListeners = 10;
i.prototype.setMaxListeners = function(e) {
if (!(t = e, "number" == typeof t) || e < 0 || isNaN(e)) throw TypeError("n must be a positive number");
var t;
this._maxListeners = e;
return this;
};
i.prototype.emit = function(e) {
var t, i, a, s, c, l;
this._events || (this._events = {});
if ("error" === e && (!this._events.error || r(this._events.error) && !this._events.error.length)) {
if ((t = arguments[1]) instanceof Error) throw t;
var h = new Error('Uncaught, unspecified "error" event. (' + t + ")");
h.context = t;
throw h;
}
if (o(i = this._events[e])) return !1;
if (n(i)) switch (arguments.length) {
case 1:
i.call(this);
break;

case 2:
i.call(this, arguments[1]);
break;

case 3:
i.call(this, arguments[1], arguments[2]);
break;

default:
s = Array.prototype.slice.call(arguments, 1);
i.apply(this, s);
} else if (r(i)) {
s = Array.prototype.slice.call(arguments, 1);
a = (l = i.slice()).length;
for (c = 0; c < a; c++) l[c].apply(this, s);
}
return !0;
};
i.prototype.addListener = function(e, t) {
var a;
if (!n(t)) throw TypeError("listener must be a function");
this._events || (this._events = {});
this._events.newListener && this.emit("newListener", e, n(t.listener) ? t.listener : t);
this._events[e] ? r(this._events[e]) ? this._events[e].push(t) : this._events[e] = [ this._events[e], t ] : this._events[e] = t;
if (r(this._events[e]) && !this._events[e].warned && (a = o(this._maxListeners) ? i.defaultMaxListeners : this._maxListeners) && a > 0 && this._events[e].length > a) {
this._events[e].warned = !0;
console.error("(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.", this._events[e].length);
"function" == typeof console.trace && console.trace();
}
return this;
};
i.prototype.on = i.prototype.addListener;
i.prototype.once = function(e, t) {
if (!n(t)) throw TypeError("listener must be a function");
var i = !1;
function r() {
this.removeListener(e, r);
if (!i) {
i = !0;
t.apply(this, arguments);
}
}
r.listener = t;
this.on(e, r);
return this;
};
i.prototype.removeListener = function(e, t) {
var i, o, a, s;
if (!n(t)) throw TypeError("listener must be a function");
if (!this._events || !this._events[e]) return this;
a = (i = this._events[e]).length;
o = -1;
if (i === t || n(i.listener) && i.listener === t) {
delete this._events[e];
this._events.removeListener && this.emit("removeListener", e, t);
} else if (r(i)) {
for (s = a; s-- > 0; ) if (i[s] === t || i[s].listener && i[s].listener === t) {
o = s;
break;
}
if (o < 0) return this;
if (1 === i.length) {
i.length = 0;
delete this._events[e];
} else i.splice(o, 1);
this._events.removeListener && this.emit("removeListener", e, t);
}
return this;
};
i.prototype.removeAllListeners = function(e) {
var t, i;
if (!this._events) return this;
if (!this._events.removeListener) {
0 === arguments.length ? this._events = {} : this._events[e] && delete this._events[e];
return this;
}
if (0 === arguments.length) {
for (t in this._events) "removeListener" !== t && this.removeAllListeners(t);
this.removeAllListeners("removeListener");
this._events = {};
return this;
}
if (n(i = this._events[e])) this.removeListener(e, i); else if (i) for (;i.length; ) this.removeListener(e, i[i.length - 1]);
delete this._events[e];
return this;
};
i.prototype.listeners = function(e) {
return this._events && this._events[e] ? n(this._events[e]) ? [ this._events[e] ] : this._events[e].slice() : [];
};
i.prototype.listenerCount = function(e) {
if (this._events) {
var t = this._events[e];
if (n(t)) return 1;
if (t) return t.length;
}
return 0;
};
i.listenerCount = function(e, t) {
return e.listenerCount(t);
};
function n(e) {
return "function" == typeof e;
}
function r(e) {
return "object" == typeof e && null !== e;
}
function o(e) {
return void 0 === e;
}
}, {} ],
105: [ function(e, t) {
var i = e("safe-buffer").Buffer, n = e("md5.js");
t.exports = function(e, t, r, o) {
i.isBuffer(e) || (e = i.from(e, "binary"));
if (t) {
i.isBuffer(t) || (t = i.from(t, "binary"));
if (8 !== t.length) throw new RangeError("salt should be Buffer with 8 byte length");
}
for (var a = r / 8, s = i.alloc(a), c = i.alloc(o || 0), l = i.alloc(0); a > 0 || o > 0; ) {
var h = new n();
h.update(l);
h.update(e);
t && h.update(t);
l = h.digest();
var u = 0;
if (a > 0) {
var f = s.length - a;
u = Math.min(a, l.length);
l.copy(s, f, 0, u);
a -= u;
}
if (u < l.length && o > 0) {
var d = c.length - o, p = Math.min(o, l.length - u);
l.copy(c, d, u, u + p);
o -= p;
}
}
l.fill(0);
return {
key: s,
iv: c
};
};
}, {
"md5.js": 139,
"safe-buffer": 182
} ],
106: [ function(e, t) {
"use strict";
var i = e("safe-buffer").Buffer, n = e("readable-stream").Transform;
function r(e, t) {
if (!i.isBuffer(e) && "string" != typeof e) throw new TypeError(t + " must be a string or a buffer");
}
function o(e) {
n.call(this);
this._block = i.allocUnsafe(e);
this._blockSize = e;
this._blockOffset = 0;
this._length = [ 0, 0, 0, 0 ];
this._finalized = !1;
}
e("inherits")(o, n);
o.prototype._transform = function(e, t, i) {
var n = null;
try {
this.update(e, t);
} catch (e) {
n = e;
}
i(n);
};
o.prototype._flush = function(e) {
var t = null;
try {
this.push(this.digest());
} catch (e) {
t = e;
}
e(t);
};
o.prototype.update = function(e, t) {
r(e, "Data");
if (this._finalized) throw new Error("Digest already called");
i.isBuffer(e) || (e = i.from(e, t));
for (var n = this._block, o = 0; this._blockOffset + e.length - o >= this._blockSize; ) {
for (var a = this._blockOffset; a < this._blockSize; ) n[a++] = e[o++];
this._update();
this._blockOffset = 0;
}
for (;o < e.length; ) n[this._blockOffset++] = e[o++];
for (var s = 0, c = 8 * e.length; c > 0; ++s) {
this._length[s] += c;
(c = this._length[s] / 4294967296 | 0) > 0 && (this._length[s] -= 4294967296 * c);
}
return this;
};
o.prototype._update = function() {
throw new Error("_update is not implemented");
};
o.prototype.digest = function(e) {
if (this._finalized) throw new Error("Digest already called");
this._finalized = !0;
var t = this._digest();
void 0 !== e && (t = t.toString(e));
this._block.fill(0);
this._blockOffset = 0;
for (var i = 0; i < 4; ++i) this._length[i] = 0;
return t;
};
o.prototype._digest = function() {
throw new Error("_digest is not implemented");
};
t.exports = o;
}, {
inherits: 138,
"readable-stream": 121,
"safe-buffer": 122
} ],
107: [ function(e, t, i) {
arguments[4][47][0].apply(i, arguments);
}, {
dup: 47
} ],
108: [ function(e, t, i) {
arguments[4][48][0].apply(i, arguments);
}, {
"./_stream_readable": 110,
"./_stream_writable": 112,
_process: 156,
dup: 48,
inherits: 138
} ],
109: [ function(e, t, i) {
arguments[4][49][0].apply(i, arguments);
}, {
"./_stream_transform": 111,
dup: 49,
inherits: 138
} ],
110: [ function(e, t, i) {
arguments[4][50][0].apply(i, arguments);
}, {
"../errors": 107,
"./_stream_duplex": 108,
"./internal/streams/async_iterator": 113,
"./internal/streams/buffer_list": 114,
"./internal/streams/destroy": 115,
"./internal/streams/from": 117,
"./internal/streams/state": 119,
"./internal/streams/stream": 120,
_process: 156,
buffer: 65,
dup: 50,
events: 104,
inherits: 138,
"string_decoder/": 123,
util: 19
} ],
111: [ function(e, t, i) {
arguments[4][51][0].apply(i, arguments);
}, {
"../errors": 107,
"./_stream_duplex": 108,
dup: 51,
inherits: 138
} ],
112: [ function(e, t, i) {
arguments[4][52][0].apply(i, arguments);
}, {
"../errors": 107,
"./_stream_duplex": 108,
"./internal/streams/destroy": 115,
"./internal/streams/state": 119,
"./internal/streams/stream": 120,
_process: 156,
buffer: 65,
dup: 52,
inherits: 138,
"util-deprecate": 194
} ],
113: [ function(e, t, i) {
arguments[4][53][0].apply(i, arguments);
}, {
"./end-of-stream": 116,
_process: 156,
dup: 53
} ],
114: [ function(e, t, i) {
arguments[4][54][0].apply(i, arguments);
}, {
buffer: 65,
dup: 54,
util: 19
} ],
115: [ function(e, t, i) {
arguments[4][55][0].apply(i, arguments);
}, {
_process: 156,
dup: 55
} ],
116: [ function(e, t, i) {
arguments[4][56][0].apply(i, arguments);
}, {
"../../../errors": 107,
dup: 56
} ],
117: [ function(e, t, i) {
arguments[4][57][0].apply(i, arguments);
}, {
dup: 57
} ],
118: [ function(e, t, i) {
arguments[4][58][0].apply(i, arguments);
}, {
"../../../errors": 107,
"./end-of-stream": 116,
dup: 58
} ],
119: [ function(e, t, i) {
arguments[4][59][0].apply(i, arguments);
}, {
"../../../errors": 107,
dup: 59
} ],
120: [ function(e, t, i) {
arguments[4][60][0].apply(i, arguments);
}, {
dup: 60,
events: 104
} ],
121: [ function(e, t, i) {
arguments[4][61][0].apply(i, arguments);
}, {
"./lib/_stream_duplex.js": 108,
"./lib/_stream_passthrough.js": 109,
"./lib/_stream_readable.js": 110,
"./lib/_stream_transform.js": 111,
"./lib/_stream_writable.js": 112,
"./lib/internal/streams/end-of-stream.js": 116,
"./lib/internal/streams/pipeline.js": 118,
dup: 61
} ],
122: [ function(e, t, i) {
arguments[4][62][0].apply(i, arguments);
}, {
buffer: 65,
dup: 62
} ],
123: [ function(e, t, i) {
arguments[4][63][0].apply(i, arguments);
}, {
dup: 63,
"safe-buffer": 122
} ],
124: [ function(e, t, i) {
var n = i;
n.utils = e("./hash/utils");
n.common = e("./hash/common");
n.sha = e("./hash/sha");
n.ripemd = e("./hash/ripemd");
n.hmac = e("./hash/hmac");
n.sha1 = n.sha.sha1;
n.sha256 = n.sha.sha256;
n.sha224 = n.sha.sha224;
n.sha384 = n.sha.sha384;
n.sha512 = n.sha.sha512;
n.ripemd160 = n.ripemd.ripemd160;
}, {
"./hash/common": 125,
"./hash/hmac": 126,
"./hash/ripemd": 127,
"./hash/sha": 128,
"./hash/utils": 135
} ],
125: [ function(e, t, i) {
"use strict";
var n = e("./utils"), r = e("minimalistic-assert");
function o() {
this.pending = null;
this.pendingTotal = 0;
this.blockSize = this.constructor.blockSize;
this.outSize = this.constructor.outSize;
this.hmacStrength = this.constructor.hmacStrength;
this.padLength = this.constructor.padLength / 8;
this.endian = "big";
this._delta8 = this.blockSize / 8;
this._delta32 = this.blockSize / 32;
}
i.BlockHash = o;
o.prototype.update = function(e, t) {
e = n.toArray(e, t);
this.pending ? this.pending = this.pending.concat(e) : this.pending = e;
this.pendingTotal += e.length;
if (this.pending.length >= this._delta8) {
var i = (e = this.pending).length % this._delta8;
this.pending = e.slice(e.length - i, e.length);
0 === this.pending.length && (this.pending = null);
e = n.join32(e, 0, e.length - i, this.endian);
for (var r = 0; r < e.length; r += this._delta32) this._update(e, r, r + this._delta32);
}
return this;
};
o.prototype.digest = function(e) {
this.update(this._pad());
r(null === this.pending);
return this._digest(e);
};
o.prototype._pad = function() {
var e = this.pendingTotal, t = this._delta8, i = t - (e + this.padLength) % t, n = new Array(i + this.padLength);
n[0] = 128;
for (var r = 1; r < i; r++) n[r] = 0;
e <<= 3;
if ("big" === this.endian) {
for (var o = 8; o < this.padLength; o++) n[r++] = 0;
n[r++] = 0;
n[r++] = 0;
n[r++] = 0;
n[r++] = 0;
n[r++] = e >>> 24 & 255;
n[r++] = e >>> 16 & 255;
n[r++] = e >>> 8 & 255;
n[r++] = 255 & e;
} else {
n[r++] = 255 & e;
n[r++] = e >>> 8 & 255;
n[r++] = e >>> 16 & 255;
n[r++] = e >>> 24 & 255;
n[r++] = 0;
n[r++] = 0;
n[r++] = 0;
n[r++] = 0;
for (o = 8; o < this.padLength; o++) n[r++] = 0;
}
return n;
};
}, {
"./utils": 135,
"minimalistic-assert": 142
} ],
126: [ function(e, t) {
"use strict";
var i = e("./utils"), n = e("minimalistic-assert");
function r(e, t, n) {
if (!(this instanceof r)) return new r(e, t, n);
this.Hash = e;
this.blockSize = e.blockSize / 8;
this.outSize = e.outSize / 8;
this.inner = null;
this.outer = null;
this._init(i.toArray(t, n));
}
t.exports = r;
r.prototype._init = function(e) {
e.length > this.blockSize && (e = new this.Hash().update(e).digest());
n(e.length <= this.blockSize);
for (var t = e.length; t < this.blockSize; t++) e.push(0);
for (t = 0; t < e.length; t++) e[t] ^= 54;
this.inner = new this.Hash().update(e);
for (t = 0; t < e.length; t++) e[t] ^= 106;
this.outer = new this.Hash().update(e);
};
r.prototype.update = function(e, t) {
this.inner.update(e, t);
return this;
};
r.prototype.digest = function(e) {
this.outer.update(this.inner.digest());
return this.outer.digest(e);
};
}, {
"./utils": 135,
"minimalistic-assert": 142
} ],
127: [ function(e, t, i) {
"use strict";
var n = e("./utils"), r = e("./common"), o = n.rotl32, a = n.sum32, s = n.sum32_3, c = n.sum32_4, l = r.BlockHash;
function h() {
if (!(this instanceof h)) return new h();
l.call(this);
this.h = [ 1732584193, 4023233417, 2562383102, 271733878, 3285377520 ];
this.endian = "little";
}
n.inherits(h, l);
i.ripemd160 = h;
h.blockSize = 512;
h.outSize = 160;
h.hmacStrength = 192;
h.padLength = 64;
h.prototype._update = function(e, t) {
for (var i = this.h[0], n = this.h[1], r = this.h[2], l = this.h[3], h = this.h[4], b = i, v = n, _ = r, w = l, C = h, S = 0; S < 80; S++) {
var A = a(o(c(i, u(S, n, r, l), e[p[S] + t], f(S)), g[S]), h);
i = h;
h = l;
l = o(r, 10);
r = n;
n = A;
A = a(o(c(b, u(79 - S, v, _, w), e[m[S] + t], d(S)), y[S]), C);
b = C;
C = w;
w = o(_, 10);
_ = v;
v = A;
}
A = s(this.h[1], r, w);
this.h[1] = s(this.h[2], l, C);
this.h[2] = s(this.h[3], h, b);
this.h[3] = s(this.h[4], i, v);
this.h[4] = s(this.h[0], n, _);
this.h[0] = A;
};
h.prototype._digest = function(e) {
return "hex" === e ? n.toHex32(this.h, "little") : n.split32(this.h, "little");
};
function u(e, t, i, n) {
return e <= 15 ? t ^ i ^ n : e <= 31 ? t & i | ~t & n : e <= 47 ? (t | ~i) ^ n : e <= 63 ? t & n | i & ~n : t ^ (i | ~n);
}
function f(e) {
return e <= 15 ? 0 : e <= 31 ? 1518500249 : e <= 47 ? 1859775393 : e <= 63 ? 2400959708 : 2840853838;
}
function d(e) {
return e <= 15 ? 1352829926 : e <= 31 ? 1548603684 : e <= 47 ? 1836072691 : e <= 63 ? 2053994217 : 0;
}
var p = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13 ], m = [ 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11 ], g = [ 11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6 ], y = [ 8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11 ];
}, {
"./common": 125,
"./utils": 135
} ],
128: [ function(e, t, i) {
"use strict";
i.sha1 = e("./sha/1");
i.sha224 = e("./sha/224");
i.sha256 = e("./sha/256");
i.sha384 = e("./sha/384");
i.sha512 = e("./sha/512");
}, {
"./sha/1": 129,
"./sha/224": 130,
"./sha/256": 131,
"./sha/384": 132,
"./sha/512": 133
} ],
129: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("../common"), r = e("./common"), o = i.rotl32, a = i.sum32, s = i.sum32_5, c = r.ft_1, l = n.BlockHash, h = [ 1518500249, 1859775393, 2400959708, 3395469782 ];
function u() {
if (!(this instanceof u)) return new u();
l.call(this);
this.h = [ 1732584193, 4023233417, 2562383102, 271733878, 3285377520 ];
this.W = new Array(80);
}
i.inherits(u, l);
t.exports = u;
u.blockSize = 512;
u.outSize = 160;
u.hmacStrength = 80;
u.padLength = 64;
u.prototype._update = function(e, t) {
for (var i = this.W, n = 0; n < 16; n++) i[n] = e[t + n];
for (;n < i.length; n++) i[n] = o(i[n - 3] ^ i[n - 8] ^ i[n - 14] ^ i[n - 16], 1);
var r = this.h[0], l = this.h[1], u = this.h[2], f = this.h[3], d = this.h[4];
for (n = 0; n < i.length; n++) {
var p = ~~(n / 20), m = s(o(r, 5), c(p, l, u, f), d, i[n], h[p]);
d = f;
f = u;
u = o(l, 30);
l = r;
r = m;
}
this.h[0] = a(this.h[0], r);
this.h[1] = a(this.h[1], l);
this.h[2] = a(this.h[2], u);
this.h[3] = a(this.h[3], f);
this.h[4] = a(this.h[4], d);
};
u.prototype._digest = function(e) {
return "hex" === e ? i.toHex32(this.h, "big") : i.split32(this.h, "big");
};
}, {
"../common": 125,
"../utils": 135,
"./common": 134
} ],
130: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("./256");
function r() {
if (!(this instanceof r)) return new r();
n.call(this);
this.h = [ 3238371032, 914150663, 812702999, 4144912697, 4290775857, 1750603025, 1694076839, 3204075428 ];
}
i.inherits(r, n);
t.exports = r;
r.blockSize = 512;
r.outSize = 224;
r.hmacStrength = 192;
r.padLength = 64;
r.prototype._digest = function(e) {
return "hex" === e ? i.toHex32(this.h.slice(0, 7), "big") : i.split32(this.h.slice(0, 7), "big");
};
}, {
"../utils": 135,
"./256": 131
} ],
131: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("../common"), r = e("./common"), o = e("minimalistic-assert"), a = i.sum32, s = i.sum32_4, c = i.sum32_5, l = r.ch32, h = r.maj32, u = r.s0_256, f = r.s1_256, d = r.g0_256, p = r.g1_256, m = n.BlockHash, g = [ 1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298 ];
function y() {
if (!(this instanceof y)) return new y();
m.call(this);
this.h = [ 1779033703, 3144134277, 1013904242, 2773480762, 1359893119, 2600822924, 528734635, 1541459225 ];
this.k = g;
this.W = new Array(64);
}
i.inherits(y, m);
t.exports = y;
y.blockSize = 512;
y.outSize = 256;
y.hmacStrength = 192;
y.padLength = 64;
y.prototype._update = function(e, t) {
for (var i = this.W, n = 0; n < 16; n++) i[n] = e[t + n];
for (;n < i.length; n++) i[n] = s(p(i[n - 2]), i[n - 7], d(i[n - 15]), i[n - 16]);
var r = this.h[0], m = this.h[1], g = this.h[2], y = this.h[3], b = this.h[4], v = this.h[5], _ = this.h[6], w = this.h[7];
o(this.k.length === i.length);
for (n = 0; n < i.length; n++) {
var C = c(w, f(b), l(b, v, _), this.k[n], i[n]), S = a(u(r), h(r, m, g));
w = _;
_ = v;
v = b;
b = a(y, C);
y = g;
g = m;
m = r;
r = a(C, S);
}
this.h[0] = a(this.h[0], r);
this.h[1] = a(this.h[1], m);
this.h[2] = a(this.h[2], g);
this.h[3] = a(this.h[3], y);
this.h[4] = a(this.h[4], b);
this.h[5] = a(this.h[5], v);
this.h[6] = a(this.h[6], _);
this.h[7] = a(this.h[7], w);
};
y.prototype._digest = function(e) {
return "hex" === e ? i.toHex32(this.h, "big") : i.split32(this.h, "big");
};
}, {
"../common": 125,
"../utils": 135,
"./common": 134,
"minimalistic-assert": 142
} ],
132: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("./512");
function r() {
if (!(this instanceof r)) return new r();
n.call(this);
this.h = [ 3418070365, 3238371032, 1654270250, 914150663, 2438529370, 812702999, 355462360, 4144912697, 1731405415, 4290775857, 2394180231, 1750603025, 3675008525, 1694076839, 1203062813, 3204075428 ];
}
i.inherits(r, n);
t.exports = r;
r.blockSize = 1024;
r.outSize = 384;
r.hmacStrength = 192;
r.padLength = 128;
r.prototype._digest = function(e) {
return "hex" === e ? i.toHex32(this.h.slice(0, 12), "big") : i.split32(this.h.slice(0, 12), "big");
};
}, {
"../utils": 135,
"./512": 133
} ],
133: [ function(e, t) {
"use strict";
var i = e("../utils"), n = e("../common"), r = e("minimalistic-assert"), o = i.rotr64_hi, a = i.rotr64_lo, s = i.shr64_hi, c = i.shr64_lo, l = i.sum64, h = i.sum64_hi, u = i.sum64_lo, f = i.sum64_4_hi, d = i.sum64_4_lo, p = i.sum64_5_hi, m = i.sum64_5_lo, g = n.BlockHash, y = [ 1116352408, 3609767458, 1899447441, 602891725, 3049323471, 3964484399, 3921009573, 2173295548, 961987163, 4081628472, 1508970993, 3053834265, 2453635748, 2937671579, 2870763221, 3664609560, 3624381080, 2734883394, 310598401, 1164996542, 607225278, 1323610764, 1426881987, 3590304994, 1925078388, 4068182383, 2162078206, 991336113, 2614888103, 633803317, 3248222580, 3479774868, 3835390401, 2666613458, 4022224774, 944711139, 264347078, 2341262773, 604807628, 2007800933, 770255983, 1495990901, 1249150122, 1856431235, 1555081692, 3175218132, 1996064986, 2198950837, 2554220882, 3999719339, 2821834349, 766784016, 2952996808, 2566594879, 3210313671, 3203337956, 3336571891, 1034457026, 3584528711, 2466948901, 113926993, 3758326383, 338241895, 168717936, 666307205, 1188179964, 773529912, 1546045734, 1294757372, 1522805485, 1396182291, 2643833823, 1695183700, 2343527390, 1986661051, 1014477480, 2177026350, 1206759142, 2456956037, 344077627, 2730485921, 1290863460, 2820302411, 3158454273, 3259730800, 3505952657, 3345764771, 106217008, 3516065817, 3606008344, 3600352804, 1432725776, 4094571909, 1467031594, 275423344, 851169720, 430227734, 3100823752, 506948616, 1363258195, 659060556, 3750685593, 883997877, 3785050280, 958139571, 3318307427, 1322822218, 3812723403, 1537002063, 2003034995, 1747873779, 3602036899, 1955562222, 1575990012, 2024104815, 1125592928, 2227730452, 2716904306, 2361852424, 442776044, 2428436474, 593698344, 2756734187, 3733110249, 3204031479, 2999351573, 3329325298, 3815920427, 3391569614, 3928383900, 3515267271, 566280711, 3940187606, 3454069534, 4118630271, 4000239992, 116418474, 1914138554, 174292421, 2731055270, 289380356, 3203993006, 460393269, 320620315, 685471733, 587496836, 852142971, 1086792851, 1017036298, 365543100, 1126000580, 2618297676, 1288033470, 3409855158, 1501505948, 4234509866, 1607167915, 987167468, 1816402316, 1246189591 ];
function b() {
if (!(this instanceof b)) return new b();
g.call(this);
this.h = [ 1779033703, 4089235720, 3144134277, 2227873595, 1013904242, 4271175723, 2773480762, 1595750129, 1359893119, 2917565137, 2600822924, 725511199, 528734635, 4215389547, 1541459225, 327033209 ];
this.k = y;
this.W = new Array(160);
}
i.inherits(b, g);
t.exports = b;
b.blockSize = 1024;
b.outSize = 512;
b.hmacStrength = 192;
b.padLength = 128;
b.prototype._prepareBlock = function(e, t) {
for (var i = this.W, n = 0; n < 32; n++) i[n] = e[t + n];
for (;n < i.length; n += 2) {
var r = T(i[n - 4], i[n - 3]), o = D(i[n - 4], i[n - 3]), a = i[n - 14], s = i[n - 13], c = x(i[n - 30], i[n - 29]), l = k(i[n - 30], i[n - 29]), h = i[n - 32], u = i[n - 31];
i[n] = f(r, o, a, s, c, l, h, u);
i[n + 1] = d(r, o, a, s, c, l, h, u);
}
};
b.prototype._update = function(e, t) {
this._prepareBlock(e, t);
var i = this.W, n = this.h[0], o = this.h[1], a = this.h[2], s = this.h[3], c = this.h[4], f = this.h[5], d = this.h[6], g = this.h[7], y = this.h[8], b = this.h[9], x = this.h[10], k = this.h[11], T = this.h[12], D = this.h[13], E = this.h[14], L = this.h[15];
r(this.k.length === i.length);
for (var P = 0; P < i.length; P += 2) {
var R = E, I = L, N = M(y, b), O = B(y, b), U = v(y, 0, x, 0, T), j = _(0, b, 0, k, 0, D), F = this.k[P], G = this.k[P + 1], V = i[P], z = i[P + 1], H = p(R, I, N, O, U, j, F, G, V, z), W = m(R, I, N, O, U, j, F, G, V, z);
R = S(n, o);
I = A(n, o);
N = w(n, 0, a, 0, c);
O = C(0, o, 0, s, 0, f);
var q = h(R, I, N, O), X = u(R, I, N, O);
E = T;
L = D;
T = x;
D = k;
x = y;
k = b;
y = h(d, g, H, W);
b = u(g, g, H, W);
d = c;
g = f;
c = a;
f = s;
a = n;
s = o;
n = h(H, W, q, X);
o = u(H, W, q, X);
}
l(this.h, 0, n, o);
l(this.h, 2, a, s);
l(this.h, 4, c, f);
l(this.h, 6, d, g);
l(this.h, 8, y, b);
l(this.h, 10, x, k);
l(this.h, 12, T, D);
l(this.h, 14, E, L);
};
b.prototype._digest = function(e) {
return "hex" === e ? i.toHex32(this.h, "big") : i.split32(this.h, "big");
};
function v(e, t, i, n, r) {
var o = e & i ^ ~e & r;
o < 0 && (o += 4294967296);
return o;
}
function _(e, t, i, n, r, o) {
var a = t & n ^ ~t & o;
a < 0 && (a += 4294967296);
return a;
}
function w(e, t, i, n, r) {
var o = e & i ^ e & r ^ i & r;
o < 0 && (o += 4294967296);
return o;
}
function C(e, t, i, n, r, o) {
var a = t & n ^ t & o ^ n & o;
a < 0 && (a += 4294967296);
return a;
}
function S(e, t) {
var i = o(e, t, 28) ^ o(t, e, 2) ^ o(t, e, 7);
i < 0 && (i += 4294967296);
return i;
}
function A(e, t) {
var i = a(e, t, 28) ^ a(t, e, 2) ^ a(t, e, 7);
i < 0 && (i += 4294967296);
return i;
}
function M(e, t) {
var i = o(e, t, 14) ^ o(e, t, 18) ^ o(t, e, 9);
i < 0 && (i += 4294967296);
return i;
}
function B(e, t) {
var i = a(e, t, 14) ^ a(e, t, 18) ^ a(t, e, 9);
i < 0 && (i += 4294967296);
return i;
}
function x(e, t) {
var i = o(e, t, 1) ^ o(e, t, 8) ^ s(e, t, 7);
i < 0 && (i += 4294967296);
return i;
}
function k(e, t) {
var i = a(e, t, 1) ^ a(e, t, 8) ^ c(e, t, 7);
i < 0 && (i += 4294967296);
return i;
}
function T(e, t) {
var i = o(e, t, 19) ^ o(t, e, 29) ^ s(e, t, 6);
i < 0 && (i += 4294967296);
return i;
}
function D(e, t) {
var i = a(e, t, 19) ^ a(t, e, 29) ^ c(e, t, 6);
i < 0 && (i += 4294967296);
return i;
}
}, {
"../common": 125,
"../utils": 135,
"minimalistic-assert": 142
} ],
134: [ function(e, t, i) {
"use strict";
var n = e("../utils").rotr32;
i.ft_1 = function(e, t, i, n) {
return 0 === e ? r(t, i, n) : 1 === e || 3 === e ? a(t, i, n) : 2 === e ? o(t, i, n) : void 0;
};
function r(e, t, i) {
return e & t ^ ~e & i;
}
i.ch32 = r;
function o(e, t, i) {
return e & t ^ e & i ^ t & i;
}
i.maj32 = o;
function a(e, t, i) {
return e ^ t ^ i;
}
i.p32 = a;
i.s0_256 = function(e) {
return n(e, 2) ^ n(e, 13) ^ n(e, 22);
};
i.s1_256 = function(e) {
return n(e, 6) ^ n(e, 11) ^ n(e, 25);
};
i.g0_256 = function(e) {
return n(e, 7) ^ n(e, 18) ^ e >>> 3;
};
i.g1_256 = function(e) {
return n(e, 17) ^ n(e, 19) ^ e >>> 10;
};
}, {
"../utils": 135
} ],
135: [ function(e, t, i) {
"use strict";
var n = e("minimalistic-assert"), r = e("inherits");
i.inherits = r;
function o(e, t) {
return 55296 == (64512 & e.charCodeAt(t)) && !(t < 0 || t + 1 >= e.length) && 56320 == (64512 & e.charCodeAt(t + 1));
}
i.toArray = function(e, t) {
if (Array.isArray(e)) return e.slice();
if (!e) return [];
var i = [];
if ("string" == typeof e) if (t) {
if ("hex" === t) {
(e = e.replace(/[^a-z0-9]+/gi, "")).length % 2 != 0 && (e = "0" + e);
for (r = 0; r < e.length; r += 2) i.push(parseInt(e[r] + e[r + 1], 16));
}
} else for (var n = 0, r = 0; r < e.length; r++) {
var a = e.charCodeAt(r);
if (a < 128) i[n++] = a; else if (a < 2048) {
i[n++] = a >> 6 | 192;
i[n++] = 63 & a | 128;
} else if (o(e, r)) {
a = 65536 + ((1023 & a) << 10) + (1023 & e.charCodeAt(++r));
i[n++] = a >> 18 | 240;
i[n++] = a >> 12 & 63 | 128;
i[n++] = a >> 6 & 63 | 128;
i[n++] = 63 & a | 128;
} else {
i[n++] = a >> 12 | 224;
i[n++] = a >> 6 & 63 | 128;
i[n++] = 63 & a | 128;
}
} else for (r = 0; r < e.length; r++) i[r] = 0 | e[r];
return i;
};
i.toHex = function(e) {
for (var t = "", i = 0; i < e.length; i++) t += s(e[i].toString(16));
return t;
};
function a(e) {
return (e >>> 24 | e >>> 8 & 65280 | e << 8 & 16711680 | (255 & e) << 24) >>> 0;
}
i.htonl = a;
i.toHex32 = function(e, t) {
for (var i = "", n = 0; n < e.length; n++) {
var r = e[n];
"little" === t && (r = a(r));
i += c(r.toString(16));
}
return i;
};
function s(e) {
return 1 === e.length ? "0" + e : e;
}
i.zero2 = s;
function c(e) {
return 7 === e.length ? "0" + e : 6 === e.length ? "00" + e : 5 === e.length ? "000" + e : 4 === e.length ? "0000" + e : 3 === e.length ? "00000" + e : 2 === e.length ? "000000" + e : 1 === e.length ? "0000000" + e : e;
}
i.zero8 = c;
i.join32 = function(e, t, i, r) {
var o = i - t;
n(o % 4 == 0);
for (var a = new Array(o / 4), s = 0, c = t; s < a.length; s++, c += 4) {
var l;
l = "big" === r ? e[c] << 24 | e[c + 1] << 16 | e[c + 2] << 8 | e[c + 3] : e[c + 3] << 24 | e[c + 2] << 16 | e[c + 1] << 8 | e[c];
a[s] = l >>> 0;
}
return a;
};
i.split32 = function(e, t) {
for (var i = new Array(4 * e.length), n = 0, r = 0; n < e.length; n++, r += 4) {
var o = e[n];
if ("big" === t) {
i[r] = o >>> 24;
i[r + 1] = o >>> 16 & 255;
i[r + 2] = o >>> 8 & 255;
i[r + 3] = 255 & o;
} else {
i[r + 3] = o >>> 24;
i[r + 2] = o >>> 16 & 255;
i[r + 1] = o >>> 8 & 255;
i[r] = 255 & o;
}
}
return i;
};
i.rotr32 = function(e, t) {
return e >>> t | e << 32 - t;
};
i.rotl32 = function(e, t) {
return e << t | e >>> 32 - t;
};
i.sum32 = function(e, t) {
return e + t >>> 0;
};
i.sum32_3 = function(e, t, i) {
return e + t + i >>> 0;
};
i.sum32_4 = function(e, t, i, n) {
return e + t + i + n >>> 0;
};
i.sum32_5 = function(e, t, i, n, r) {
return e + t + i + n + r >>> 0;
};
i.sum64 = function(e, t, i, n) {
var r = e[t], o = n + e[t + 1] >>> 0, a = (o < n ? 1 : 0) + i + r;
e[t] = a >>> 0;
e[t + 1] = o;
};
i.sum64_hi = function(e, t, i, n) {
return (t + n >>> 0 < t ? 1 : 0) + e + i >>> 0;
};
i.sum64_lo = function(e, t, i, n) {
return t + n >>> 0;
};
i.sum64_4_hi = function(e, t, i, n, r, o, a, s) {
var c = 0, l = t;
c += (l = l + n >>> 0) < t ? 1 : 0;
c += (l = l + o >>> 0) < o ? 1 : 0;
return e + i + r + a + (c += (l = l + s >>> 0) < s ? 1 : 0) >>> 0;
};
i.sum64_4_lo = function(e, t, i, n, r, o, a, s) {
return t + n + o + s >>> 0;
};
i.sum64_5_hi = function(e, t, i, n, r, o, a, s, c, l) {
var h = 0, u = t;
h += (u = u + n >>> 0) < t ? 1 : 0;
h += (u = u + o >>> 0) < o ? 1 : 0;
h += (u = u + s >>> 0) < s ? 1 : 0;
return e + i + r + a + c + (h += (u = u + l >>> 0) < l ? 1 : 0) >>> 0;
};
i.sum64_5_lo = function(e, t, i, n, r, o, a, s, c, l) {
return t + n + o + s + l >>> 0;
};
i.rotr64_hi = function(e, t, i) {
return (t << 32 - i | e >>> i) >>> 0;
};
i.rotr64_lo = function(e, t, i) {
return (e << 32 - i | t >>> i) >>> 0;
};
i.shr64_hi = function(e, t, i) {
return e >>> i;
};
i.shr64_lo = function(e, t, i) {
return (e << 32 - i | t >>> i) >>> 0;
};
}, {
inherits: 138,
"minimalistic-assert": 142
} ],
136: [ function(e, t) {
"use strict";
var i = e("hash.js"), n = e("minimalistic-crypto-utils"), r = e("minimalistic-assert");
function o(e) {
if (!(this instanceof o)) return new o(e);
this.hash = e.hash;
this.predResist = !!e.predResist;
this.outLen = this.hash.outSize;
this.minEntropy = e.minEntropy || this.hash.hmacStrength;
this._reseed = null;
this.reseedInterval = null;
this.K = null;
this.V = null;
var t = n.toArray(e.entropy, e.entropyEnc || "hex"), i = n.toArray(e.nonce, e.nonceEnc || "hex"), a = n.toArray(e.pers, e.persEnc || "hex");
r(t.length >= this.minEntropy / 8, "Not enough entropy. Minimum is: " + this.minEntropy + " bits");
this._init(t, i, a);
}
t.exports = o;
o.prototype._init = function(e, t, i) {
var n = e.concat(t).concat(i);
this.K = new Array(this.outLen / 8);
this.V = new Array(this.outLen / 8);
for (var r = 0; r < this.V.length; r++) {
this.K[r] = 0;
this.V[r] = 1;
}
this._update(n);
this._reseed = 1;
this.reseedInterval = 281474976710656;
};
o.prototype._hmac = function() {
return new i.hmac(this.hash, this.K);
};
o.prototype._update = function(e) {
var t = this._hmac().update(this.V).update([ 0 ]);
e && (t = t.update(e));
this.K = t.digest();
this.V = this._hmac().update(this.V).digest();
if (e) {
this.K = this._hmac().update(this.V).update([ 1 ]).update(e).digest();
this.V = this._hmac().update(this.V).digest();
}
};
o.prototype.reseed = function(e, t, i, o) {
if ("string" != typeof t) {
o = i;
i = t;
t = null;
}
e = n.toArray(e, t);
i = n.toArray(i, o);
r(e.length >= this.minEntropy / 8, "Not enough entropy. Minimum is: " + this.minEntropy + " bits");
this._update(e.concat(i || []));
this._reseed = 1;
};
o.prototype.generate = function(e, t, i, r) {
if (this._reseed > this.reseedInterval) throw new Error("Reseed is required");
if ("string" != typeof t) {
r = i;
i = t;
t = null;
}
if (i) {
i = n.toArray(i, r || "hex");
this._update(i);
}
for (var o = []; o.length < e; ) {
this.V = this._hmac().update(this.V).digest();
o = o.concat(this.V);
}
var a = o.slice(0, e);
this._update(i);
this._reseed++;
return n.encode(a, t);
};
}, {
"hash.js": 124,
"minimalistic-assert": 142,
"minimalistic-crypto-utils": 143
} ],
137: [ function(e, t, i) {
i.read = function(e, t, i, n, r) {
var o, a, s = 8 * r - n - 1, c = (1 << s) - 1, l = c >> 1, h = -7, u = i ? r - 1 : 0, f = i ? -1 : 1, d = e[t + u];
u += f;
o = d & (1 << -h) - 1;
d >>= -h;
h += s;
for (;h > 0; o = 256 * o + e[t + u], u += f, h -= 8) ;
a = o & (1 << -h) - 1;
o >>= -h;
h += n;
for (;h > 0; a = 256 * a + e[t + u], u += f, h -= 8) ;
if (0 === o) o = 1 - l; else {
if (o === c) return a ? NaN : Infinity * (d ? -1 : 1);
a += Math.pow(2, n);
o -= l;
}
return (d ? -1 : 1) * a * Math.pow(2, o - n);
};
i.write = function(e, t, i, n, r, o) {
var a, s, c, l = 8 * o - r - 1, h = (1 << l) - 1, u = h >> 1, f = 23 === r ? Math.pow(2, -24) - Math.pow(2, -77) : 0, d = n ? 0 : o - 1, p = n ? 1 : -1, m = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;
t = Math.abs(t);
if (isNaN(t) || Infinity === t) {
s = isNaN(t) ? 1 : 0;
a = h;
} else {
a = Math.floor(Math.log(t) / Math.LN2);
if (t * (c = Math.pow(2, -a)) < 1) {
a--;
c *= 2;
}
if ((t += a + u >= 1 ? f / c : f * Math.pow(2, 1 - u)) * c >= 2) {
a++;
c /= 2;
}
if (a + u >= h) {
s = 0;
a = h;
} else if (a + u >= 1) {
s = (t * c - 1) * Math.pow(2, r);
a += u;
} else {
s = t * Math.pow(2, u - 1) * Math.pow(2, r);
a = 0;
}
}
for (;r >= 8; e[i + d] = 255 & s, d += p, s /= 256, r -= 8) ;
a = a << r | s;
l += r;
for (;l > 0; e[i + d] = 255 & a, d += p, a /= 256, l -= 8) ;
e[i + d - p] |= 128 * m;
};
}, {} ],
138: [ function(e, t) {
"function" == typeof Object.create ? t.exports = function(e, t) {
if (t) {
e.super_ = t;
e.prototype = Object.create(t.prototype, {
constructor: {
value: e,
enumerable: !1,
writable: !0,
configurable: !0
}
});
}
} : t.exports = function(e, t) {
if (t) {
e.super_ = t;
var i = function() {};
i.prototype = t.prototype;
e.prototype = new i();
e.prototype.constructor = e;
}
};
}, {} ],
139: [ function(e, t) {
"use strict";
var i = e("inherits"), n = e("hash-base"), r = e("safe-buffer").Buffer, o = new Array(16);
function a() {
n.call(this, 64);
this._a = 1732584193;
this._b = 4023233417;
this._c = 2562383102;
this._d = 271733878;
}
i(a, n);
a.prototype._update = function() {
for (var e = o, t = 0; t < 16; ++t) e[t] = this._block.readInt32LE(4 * t);
var i = this._a, n = this._b, r = this._c, a = this._d;
i = c(i, n, r, a, e[0], 3614090360, 7);
a = c(a, i, n, r, e[1], 3905402710, 12);
r = c(r, a, i, n, e[2], 606105819, 17);
n = c(n, r, a, i, e[3], 3250441966, 22);
i = c(i, n, r, a, e[4], 4118548399, 7);
a = c(a, i, n, r, e[5], 1200080426, 12);
r = c(r, a, i, n, e[6], 2821735955, 17);
n = c(n, r, a, i, e[7], 4249261313, 22);
i = c(i, n, r, a, e[8], 1770035416, 7);
a = c(a, i, n, r, e[9], 2336552879, 12);
r = c(r, a, i, n, e[10], 4294925233, 17);
n = c(n, r, a, i, e[11], 2304563134, 22);
i = c(i, n, r, a, e[12], 1804603682, 7);
a = c(a, i, n, r, e[13], 4254626195, 12);
r = c(r, a, i, n, e[14], 2792965006, 17);
i = l(i, n = c(n, r, a, i, e[15], 1236535329, 22), r, a, e[1], 4129170786, 5);
a = l(a, i, n, r, e[6], 3225465664, 9);
r = l(r, a, i, n, e[11], 643717713, 14);
n = l(n, r, a, i, e[0], 3921069994, 20);
i = l(i, n, r, a, e[5], 3593408605, 5);
a = l(a, i, n, r, e[10], 38016083, 9);
r = l(r, a, i, n, e[15], 3634488961, 14);
n = l(n, r, a, i, e[4], 3889429448, 20);
i = l(i, n, r, a, e[9], 568446438, 5);
a = l(a, i, n, r, e[14], 3275163606, 9);
r = l(r, a, i, n, e[3], 4107603335, 14);
n = l(n, r, a, i, e[8], 1163531501, 20);
i = l(i, n, r, a, e[13], 2850285829, 5);
a = l(a, i, n, r, e[2], 4243563512, 9);
r = l(r, a, i, n, e[7], 1735328473, 14);
i = h(i, n = l(n, r, a, i, e[12], 2368359562, 20), r, a, e[5], 4294588738, 4);
a = h(a, i, n, r, e[8], 2272392833, 11);
r = h(r, a, i, n, e[11], 1839030562, 16);
n = h(n, r, a, i, e[14], 4259657740, 23);
i = h(i, n, r, a, e[1], 2763975236, 4);
a = h(a, i, n, r, e[4], 1272893353, 11);
r = h(r, a, i, n, e[7], 4139469664, 16);
n = h(n, r, a, i, e[10], 3200236656, 23);
i = h(i, n, r, a, e[13], 681279174, 4);
a = h(a, i, n, r, e[0], 3936430074, 11);
r = h(r, a, i, n, e[3], 3572445317, 16);
n = h(n, r, a, i, e[6], 76029189, 23);
i = h(i, n, r, a, e[9], 3654602809, 4);
a = h(a, i, n, r, e[12], 3873151461, 11);
r = h(r, a, i, n, e[15], 530742520, 16);
i = u(i, n = h(n, r, a, i, e[2], 3299628645, 23), r, a, e[0], 4096336452, 6);
a = u(a, i, n, r, e[7], 1126891415, 10);
r = u(r, a, i, n, e[14], 2878612391, 15);
n = u(n, r, a, i, e[5], 4237533241, 21);
i = u(i, n, r, a, e[12], 1700485571, 6);
a = u(a, i, n, r, e[3], 2399980690, 10);
r = u(r, a, i, n, e[10], 4293915773, 15);
n = u(n, r, a, i, e[1], 2240044497, 21);
i = u(i, n, r, a, e[8], 1873313359, 6);
a = u(a, i, n, r, e[15], 4264355552, 10);
r = u(r, a, i, n, e[6], 2734768916, 15);
n = u(n, r, a, i, e[13], 1309151649, 21);
i = u(i, n, r, a, e[4], 4149444226, 6);
a = u(a, i, n, r, e[11], 3174756917, 10);
r = u(r, a, i, n, e[2], 718787259, 15);
n = u(n, r, a, i, e[9], 3951481745, 21);
this._a = this._a + i | 0;
this._b = this._b + n | 0;
this._c = this._c + r | 0;
this._d = this._d + a | 0;
};
a.prototype._digest = function() {
this._block[this._blockOffset++] = 128;
if (this._blockOffset > 56) {
this._block.fill(0, this._blockOffset, 64);
this._update();
this._blockOffset = 0;
}
this._block.fill(0, this._blockOffset, 56);
this._block.writeUInt32LE(this._length[0], 56);
this._block.writeUInt32LE(this._length[1], 60);
this._update();
var e = r.allocUnsafe(16);
e.writeInt32LE(this._a, 0);
e.writeInt32LE(this._b, 4);
e.writeInt32LE(this._c, 8);
e.writeInt32LE(this._d, 12);
return e;
};
function s(e, t) {
return e << t | e >>> 32 - t;
}
function c(e, t, i, n, r, o, a) {
return s(e + (t & i | ~t & n) + r + o | 0, a) + t | 0;
}
function l(e, t, i, n, r, o, a) {
return s(e + (t & n | i & ~n) + r + o | 0, a) + t | 0;
}
function h(e, t, i, n, r, o, a) {
return s(e + (t ^ i ^ n) + r + o | 0, a) + t | 0;
}
function u(e, t, i, n, r, o, a) {
return s(e + (i ^ (t | ~n)) + r + o | 0, a) + t | 0;
}
t.exports = a;
}, {
"hash-base": 106,
inherits: 138,
"safe-buffer": 182
} ],
140: [ function(e, t) {
var i = e("bn.js"), n = e("brorand");
function r(e) {
this.rand = e || new n.Rand();
}
t.exports = r;
r.create = function(e) {
return new r(e);
};
r.prototype._randbelow = function(e) {
var t = e.bitLength(), n = Math.ceil(t / 8);
do {
var r = new i(this.rand.generate(n));
} while (r.cmp(e) >= 0);
return r;
};
r.prototype._randrange = function(e, t) {
var i = t.sub(e);
return e.add(this._randbelow(i));
};
r.prototype.test = function(e, t, n) {
var r = e.bitLength(), o = i.mont(e), a = new i(1).toRed(o);
t || (t = Math.max(1, r / 48 | 0));
for (var s = e.subn(1), c = 0; !s.testn(c); c++) ;
for (var l = e.shrn(c), h = s.toRed(o); t > 0; t--) {
var u = this._randrange(new i(2), s);
n && n(u);
var f = u.toRed(o).redPow(l);
if (0 !== f.cmp(a) && 0 !== f.cmp(h)) {
for (var d = 1; d < c; d++) {
if (0 === (f = f.redSqr()).cmp(a)) return !1;
if (0 === f.cmp(h)) break;
}
if (d === c) return !1;
}
}
return !0;
};
r.prototype.getDivisor = function(e, t) {
var n = e.bitLength(), r = i.mont(e), o = new i(1).toRed(r);
t || (t = Math.max(1, n / 48 | 0));
for (var a = e.subn(1), s = 0; !a.testn(s); s++) ;
for (var c = e.shrn(s), l = a.toRed(r); t > 0; t--) {
var h = this._randrange(new i(2), a), u = e.gcd(h);
if (0 !== u.cmpn(1)) return u;
var f = h.toRed(r).redPow(c);
if (0 !== f.cmp(o) && 0 !== f.cmp(l)) {
for (var d = 1; d < s; d++) {
if (0 === (f = f.redSqr()).cmp(o)) return f.fromRed().subn(1).gcd(e);
if (0 === f.cmp(l)) break;
}
if (d === s) return (f = f.redSqr()).fromRed().subn(1).gcd(e);
}
}
return !1;
};
}, {
"bn.js": 141,
brorand: 18
} ],
141: [ function(e, t, i) {
arguments[4][15][0].apply(i, arguments);
}, {
buffer: 19,
dup: 15
} ],
142: [ function(e, t) {
t.exports = i;
function i(e, t) {
if (!e) throw new Error(t || "Assertion failed");
}
i.equal = function(e, t, i) {
if (e != t) throw new Error(i || "Assertion failed: " + e + " != " + t);
};
}, {} ],
143: [ function(e, t, i) {
"use strict";
var n = i;
n.toArray = function(e, t) {
if (Array.isArray(e)) return e.slice();
if (!e) return [];
var i = [];
if ("string" != typeof e) {
for (var n = 0; n < e.length; n++) i[n] = 0 | e[n];
return i;
}
if ("hex" === t) {
(e = e.replace(/[^a-z0-9]+/gi, "")).length % 2 != 0 && (e = "0" + e);
for (n = 0; n < e.length; n += 2) i.push(parseInt(e[n] + e[n + 1], 16));
} else for (n = 0; n < e.length; n++) {
var r = e.charCodeAt(n), o = r >> 8, a = 255 & r;
o ? i.push(o, a) : i.push(a);
}
return i;
};
function r(e) {
return 1 === e.length ? "0" + e : e;
}
n.zero2 = r;
function o(e) {
for (var t = "", i = 0; i < e.length; i++) t += r(e[i].toString(16));
return t;
}
n.toHex = o;
n.encode = function(e, t) {
return "hex" === t ? o(e) : e;
};
}, {} ],
144: [ function(e, t) {
t.exports = {
"2.16.840.1.101.3.4.1.1": "aes-128-ecb",
"2.16.840.1.101.3.4.1.2": "aes-128-cbc",
"2.16.840.1.101.3.4.1.3": "aes-128-ofb",
"2.16.840.1.101.3.4.1.4": "aes-128-cfb",
"2.16.840.1.101.3.4.1.21": "aes-192-ecb",
"2.16.840.1.101.3.4.1.22": "aes-192-cbc",
"2.16.840.1.101.3.4.1.23": "aes-192-ofb",
"2.16.840.1.101.3.4.1.24": "aes-192-cfb",
"2.16.840.1.101.3.4.1.41": "aes-256-ecb",
"2.16.840.1.101.3.4.1.42": "aes-256-cbc",
"2.16.840.1.101.3.4.1.43": "aes-256-ofb",
"2.16.840.1.101.3.4.1.44": "aes-256-cfb"
};
}, {} ],
145: [ function(e, t, i) {
"use strict";
var n = e("asn1.js");
i.certificate = e("./certificate");
var r = n.define("RSAPrivateKey", function() {
this.seq().obj(this.key("version").int(), this.key("modulus").int(), this.key("publicExponent").int(), this.key("privateExponent").int(), this.key("prime1").int(), this.key("prime2").int(), this.key("exponent1").int(), this.key("exponent2").int(), this.key("coefficient").int());
});
i.RSAPrivateKey = r;
var o = n.define("RSAPublicKey", function() {
this.seq().obj(this.key("modulus").int(), this.key("publicExponent").int());
});
i.RSAPublicKey = o;
var a = n.define("SubjectPublicKeyInfo", function() {
this.seq().obj(this.key("algorithm").use(s), this.key("subjectPublicKey").bitstr());
});
i.PublicKey = a;
var s = n.define("AlgorithmIdentifier", function() {
this.seq().obj(this.key("algorithm").objid(), this.key("none").null_().optional(), this.key("curve").objid().optional(), this.key("params").seq().obj(this.key("p").int(), this.key("q").int(), this.key("g").int()).optional());
}), c = n.define("PrivateKeyInfo", function() {
this.seq().obj(this.key("version").int(), this.key("algorithm").use(s), this.key("subjectPrivateKey").octstr());
});
i.PrivateKey = c;
var l = n.define("EncryptedPrivateKeyInfo", function() {
this.seq().obj(this.key("algorithm").seq().obj(this.key("id").objid(), this.key("decrypt").seq().obj(this.key("kde").seq().obj(this.key("id").objid(), this.key("kdeparams").seq().obj(this.key("salt").octstr(), this.key("iters").int())), this.key("cipher").seq().obj(this.key("algo").objid(), this.key("iv").octstr()))), this.key("subjectPrivateKey").octstr());
});
i.EncryptedPrivateKey = l;
var h = n.define("DSAPrivateKey", function() {
this.seq().obj(this.key("version").int(), this.key("p").int(), this.key("q").int(), this.key("g").int(), this.key("pub_key").int(), this.key("priv_key").int());
});
i.DSAPrivateKey = h;
i.DSAparam = n.define("DSAparam", function() {
this.int();
});
var u = n.define("ECPrivateKey", function() {
this.seq().obj(this.key("version").int(), this.key("privateKey").octstr(), this.key("parameters").optional().explicit(0).use(f), this.key("publicKey").optional().explicit(1).bitstr());
});
i.ECPrivateKey = u;
var f = n.define("ECParameters", function() {
this.choice({
namedCurve: this.objid()
});
});
i.signature = n.define("signature", function() {
this.seq().obj(this.key("r").int(), this.key("s").int());
});
}, {
"./certificate": 146,
"asn1.js": 1
} ],
146: [ function(e, t) {
"use strict";
var i = e("asn1.js"), n = i.define("Time", function() {
this.choice({
utcTime: this.utctime(),
generalTime: this.gentime()
});
}), r = i.define("AttributeTypeValue", function() {
this.seq().obj(this.key("type").objid(), this.key("value").any());
}), o = i.define("AlgorithmIdentifier", function() {
this.seq().obj(this.key("algorithm").objid(), this.key("parameters").optional(), this.key("curve").objid().optional());
}), a = i.define("SubjectPublicKeyInfo", function() {
this.seq().obj(this.key("algorithm").use(o), this.key("subjectPublicKey").bitstr());
}), s = i.define("RelativeDistinguishedName", function() {
this.setof(r);
}), c = i.define("RDNSequence", function() {
this.seqof(s);
}), l = i.define("Name", function() {
this.choice({
rdnSequence: this.use(c)
});
}), h = i.define("Validity", function() {
this.seq().obj(this.key("notBefore").use(n), this.key("notAfter").use(n));
}), u = i.define("Extension", function() {
this.seq().obj(this.key("extnID").objid(), this.key("critical").bool().def(!1), this.key("extnValue").octstr());
}), f = i.define("TBSCertificate", function() {
this.seq().obj(this.key("version").explicit(0).int().optional(), this.key("serialNumber").int(), this.key("signature").use(o), this.key("issuer").use(l), this.key("validity").use(h), this.key("subject").use(l), this.key("subjectPublicKeyInfo").use(a), this.key("issuerUniqueID").implicit(1).bitstr().optional(), this.key("subjectUniqueID").implicit(2).bitstr().optional(), this.key("extensions").explicit(3).seqof(u).optional());
}), d = i.define("X509Certificate", function() {
this.seq().obj(this.key("tbsCertificate").use(f), this.key("signatureAlgorithm").use(o), this.key("signatureValue").bitstr());
});
t.exports = d;
}, {
"asn1.js": 1
} ],
147: [ function(e, t) {
var i = /Proc-Type: 4,ENCRYPTED[\n\r]+DEK-Info: AES-((?:128)|(?:192)|(?:256))-CBC,([0-9A-H]+)[\n\r]+([0-9A-z\n\r+/=]+)[\n\r]+/m, n = /^-----BEGIN ((?:.*? KEY)|CERTIFICATE)-----/m, r = /^-----BEGIN ((?:.*? KEY)|CERTIFICATE)-----([0-9A-z\n\r+/=]+)-----END \1-----$/m, o = e("evp_bytestokey"), a = e("browserify-aes"), s = e("safe-buffer").Buffer;
t.exports = function(e, t) {
var c, l = e.toString(), h = l.match(i);
if (h) {
var u = "aes" + h[1], f = s.from(h[2], "hex"), d = s.from(h[3].replace(/[\r\n]/g, ""), "base64"), p = o(t, f.slice(0, 8), parseInt(h[1], 10)).key, m = [], g = a.createDecipheriv(u, p, f);
m.push(g.update(d));
m.push(g.final());
c = s.concat(m);
} else {
var y = l.match(r);
c = s.from(y[2].replace(/[\r\n]/g, ""), "base64");
}
return {
tag: l.match(n)[1],
data: c
};
};
}, {
"browserify-aes": 22,
evp_bytestokey: 105,
"safe-buffer": 182
} ],
148: [ function(e, t) {
var i = e("./asn1"), n = e("./aesid.json"), r = e("./fixProc"), o = e("browserify-aes"), a = e("pbkdf2"), s = e("safe-buffer").Buffer;
t.exports = c;
function c(e) {
var t;
if ("object" == typeof e && !s.isBuffer(e)) {
t = e.passphrase;
e = e.key;
}
"string" == typeof e && (e = s.from(e));
var n, o, a = r(e, t), c = a.tag, h = a.data;
switch (c) {
case "CERTIFICATE":
o = i.certificate.decode(h, "der").tbsCertificate.subjectPublicKeyInfo;

case "PUBLIC KEY":
o || (o = i.PublicKey.decode(h, "der"));
switch (n = o.algorithm.algorithm.join(".")) {
case "1.2.840.113549.1.1.1":
return i.RSAPublicKey.decode(o.subjectPublicKey.data, "der");

case "1.2.840.10045.2.1":
o.subjectPrivateKey = o.subjectPublicKey;
return {
type: "ec",
data: o
};

case "1.2.840.10040.4.1":
o.algorithm.params.pub_key = i.DSAparam.decode(o.subjectPublicKey.data, "der");
return {
type: "dsa",
data: o.algorithm.params
};

default:
throw new Error("unknown key id " + n);
}

case "ENCRYPTED PRIVATE KEY":
h = l(h = i.EncryptedPrivateKey.decode(h, "der"), t);

case "PRIVATE KEY":
switch (n = (o = i.PrivateKey.decode(h, "der")).algorithm.algorithm.join(".")) {
case "1.2.840.113549.1.1.1":
return i.RSAPrivateKey.decode(o.subjectPrivateKey, "der");

case "1.2.840.10045.2.1":
return {
curve: o.algorithm.curve,
privateKey: i.ECPrivateKey.decode(o.subjectPrivateKey, "der").privateKey
};

case "1.2.840.10040.4.1":
o.algorithm.params.priv_key = i.DSAparam.decode(o.subjectPrivateKey, "der");
return {
type: "dsa",
params: o.algorithm.params
};

default:
throw new Error("unknown key id " + n);
}

case "RSA PUBLIC KEY":
return i.RSAPublicKey.decode(h, "der");

case "RSA PRIVATE KEY":
return i.RSAPrivateKey.decode(h, "der");

case "DSA PRIVATE KEY":
return {
type: "dsa",
params: i.DSAPrivateKey.decode(h, "der")
};

case "EC PRIVATE KEY":
return {
curve: (h = i.ECPrivateKey.decode(h, "der")).parameters.value,
privateKey: h.privateKey
};

default:
throw new Error("unknown key type " + c);
}
}
c.signature = i.signature;
function l(e, t) {
var i = e.algorithm.decrypt.kde.kdeparams.salt, r = parseInt(e.algorithm.decrypt.kde.kdeparams.iters.toString(), 10), c = n[e.algorithm.decrypt.cipher.algo.join(".")], l = e.algorithm.decrypt.cipher.iv, h = e.subjectPrivateKey, u = parseInt(c.split("-")[1], 10) / 8, f = a.pbkdf2Sync(t, i, r, u, "sha1"), d = o.createDecipheriv(c, f, l), p = [];
p.push(d.update(h));
p.push(d.final());
return s.concat(p);
}
}, {
"./aesid.json": 144,
"./asn1": 145,
"./fixProc": 147,
"browserify-aes": 22,
pbkdf2: 149,
"safe-buffer": 182
} ],
149: [ function(e, t, i) {
i.pbkdf2 = e("./lib/async");
i.pbkdf2Sync = e("./lib/sync");
}, {
"./lib/async": 150,
"./lib/sync": 153
} ],
150: [ function(e, t) {
(function(i) {
var n, r, o = e("safe-buffer").Buffer, a = e("./precondition"), s = e("./default-encoding"), c = e("./sync"), l = e("./to-buffer"), h = i.crypto && i.crypto.subtle, u = {
sha: "SHA-1",
"sha-1": "SHA-1",
sha1: "SHA-1",
sha256: "SHA-256",
"sha-256": "SHA-256",
sha384: "SHA-384",
"sha-384": "SHA-384",
"sha-512": "SHA-512",
sha512: "SHA-512"
}, f = [];
function d(e) {
if (i.process && !i.process.browser) return Promise.resolve(!1);
if (!h || !h.importKey || !h.deriveBits) return Promise.resolve(!1);
if (void 0 !== f[e]) return f[e];
var t = m(n = n || o.alloc(8), n, 10, 128, e).then(function() {
return !0;
}).catch(function() {
return !1;
});
f[e] = t;
return t;
}
function p() {
return r || (r = i.process && i.process.nextTick ? i.process.nextTick : i.queueMicrotask ? i.queueMicrotask : i.setImmediate ? i.setImmediate : i.setTimeout);
}
function m(e, t, i, n, r) {
return h.importKey("raw", e, {
name: "PBKDF2"
}, !1, [ "deriveBits" ]).then(function(e) {
return h.deriveBits({
name: "PBKDF2",
salt: t,
iterations: i,
hash: {
name: r
}
}, e, n << 3);
}).then(function(e) {
return o.from(e);
});
}
function g(e, t) {
e.then(function(e) {
p()(function() {
t(null, e);
});
}, function(e) {
p()(function() {
t(e);
});
});
}
t.exports = function(e, t, n, r, o, h) {
if ("function" == typeof o) {
h = o;
o = void 0;
}
var f = u[(o = o || "sha1").toLowerCase()];
if (f && "function" == typeof i.Promise) {
a(n, r);
e = l(e, s, "Password");
t = l(t, s, "Salt");
if ("function" != typeof h) throw new Error("No callback provided to pbkdf2");
g(d(f).then(function(i) {
return i ? m(e, t, n, r, f) : c(e, t, n, r, o);
}), h);
} else p()(function() {
var i;
try {
i = c(e, t, n, r, o);
} catch (e) {
return h(e);
}
h(null, i);
});
};
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"./default-encoding": 151,
"./precondition": 152,
"./sync": 153,
"./to-buffer": 154,
"safe-buffer": 182
} ],
151: [ function(e, t) {
(function(e, i) {
var n;
n = i.process && i.process.browser ? "utf-8" : i.process && i.process.version ? parseInt(e.version.split(".")[0].slice(1), 10) >= 6 ? "utf-8" : "binary" : "utf-8";
t.exports = n;
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
_process: 156
} ],
152: [ function(e, t) {
var i = Math.pow(2, 30) - 1;
t.exports = function(e, t) {
if ("number" != typeof e) throw new TypeError("Iterations not a number");
if (e < 0) throw new TypeError("Bad iterations");
if ("number" != typeof t) throw new TypeError("Key length not a number");
if (t < 0 || t > i || t != t) throw new TypeError("Bad key length");
};
}, {} ],
153: [ function(e, t) {
var i = e("create-hash/md5"), n = e("ripemd160"), r = e("sha.js"), o = e("safe-buffer").Buffer, a = e("./precondition"), s = e("./default-encoding"), c = e("./to-buffer"), l = o.alloc(128), h = {
md5: 16,
sha1: 20,
sha224: 28,
sha256: 32,
sha384: 48,
sha512: 64,
rmd160: 20,
ripemd160: 20
};
function u(e, t, i) {
var n = f(e), r = "sha512" === e || "sha384" === e ? 128 : 64;
t.length > r ? t = n(t) : t.length < r && (t = o.concat([ t, l ], r));
for (var a = o.allocUnsafe(r + h[e]), s = o.allocUnsafe(r + h[e]), c = 0; c < r; c++) {
a[c] = 54 ^ t[c];
s[c] = 92 ^ t[c];
}
var u = o.allocUnsafe(r + i + 4);
a.copy(u, 0, 0, r);
this.ipad1 = u;
this.ipad2 = a;
this.opad = s;
this.alg = e;
this.blocksize = r;
this.hash = n;
this.size = h[e];
}
u.prototype.run = function(e, t) {
e.copy(t, this.blocksize);
this.hash(t).copy(this.opad, this.blocksize);
return this.hash(this.opad);
};
function f(e) {
return "rmd160" === e || "ripemd160" === e ? function(e) {
return new n().update(e).digest();
} : "md5" === e ? i : function(t) {
return r(e).update(t).digest();
};
}
t.exports = function(e, t, i, n, r) {
a(i, n);
var l = new u(r = r || "sha1", e = c(e, s, "Password"), (t = c(t, s, "Salt")).length), f = o.allocUnsafe(n), d = o.allocUnsafe(t.length + 4);
t.copy(d, 0, 0, t.length);
for (var p = 0, m = h[r], g = Math.ceil(n / m), y = 1; y <= g; y++) {
d.writeUInt32BE(y, t.length);
for (var b = l.run(d, l.ipad1), v = b, _ = 1; _ < i; _++) {
v = l.run(v, l.ipad2);
for (var w = 0; w < m; w++) b[w] ^= v[w];
}
b.copy(f, p);
p += m;
}
return f;
};
}, {
"./default-encoding": 151,
"./precondition": 152,
"./to-buffer": 154,
"create-hash/md5": 72,
ripemd160: 181,
"safe-buffer": 182,
"sha.js": 185
} ],
154: [ function(e, t) {
var i = e("safe-buffer").Buffer;
t.exports = function(e, t, n) {
if (i.isBuffer(e)) return e;
if ("string" == typeof e) return i.from(e, t);
if (ArrayBuffer.isView(e)) return i.from(e.buffer);
throw new TypeError(n + " must be a string, a Buffer, a typed array or a DataView");
};
}, {
"safe-buffer": 182
} ],
155: [ function(e, t) {
(function(e) {
"use strict";
"undefined" == typeof e || !e.version || 0 === e.version.indexOf("v0.") || 0 === e.version.indexOf("v1.") && 0 !== e.version.indexOf("v1.8.") ? t.exports = {
nextTick: function(t, i, n, r) {
if ("function" != typeof t) throw new TypeError('"callback" argument must be a function');
var o, a, s = arguments.length;
switch (s) {
case 0:
case 1:
return e.nextTick(t);

case 2:
return e.nextTick(function() {
t.call(null, i);
});

case 3:
return e.nextTick(function() {
t.call(null, i, n);
});

case 4:
return e.nextTick(function() {
t.call(null, i, n, r);
});

default:
o = new Array(s - 1);
a = 0;
for (;a < o.length; ) o[a++] = arguments[a];
return e.nextTick(function() {
t.apply(null, o);
});
}
}
} : t.exports = e;
}).call(this, e("_process"));
}, {
_process: 156
} ],
156: [ function(e, t) {
var i, n, r = t.exports = {};
function o() {
throw new Error("setTimeout has not been defined");
}
function a() {
throw new Error("clearTimeout has not been defined");
}
(function() {
try {
i = "function" == typeof setTimeout ? setTimeout : o;
} catch (e) {
i = o;
}
try {
n = "function" == typeof clearTimeout ? clearTimeout : a;
} catch (e) {
n = a;
}
})();
function s(e) {
if (i === setTimeout) return setTimeout(e, 0);
if ((i === o || !i) && setTimeout) {
i = setTimeout;
return setTimeout(e, 0);
}
try {
return i(e, 0);
} catch (t) {
try {
return i.call(null, e, 0);
} catch (t) {
return i.call(this, e, 0);
}
}
}
function c(e) {
if (n === clearTimeout) return clearTimeout(e);
if ((n === a || !n) && clearTimeout) {
n = clearTimeout;
return clearTimeout(e);
}
try {
return n(e);
} catch (t) {
try {
return n.call(null, e);
} catch (t) {
return n.call(this, e);
}
}
}
var l, h = [], u = !1, f = -1;
function d() {
if (u && l) {
u = !1;
l.length ? h = l.concat(h) : f = -1;
h.length && p();
}
}
function p() {
if (!u) {
var e = s(d);
u = !0;
for (var t = h.length; t; ) {
l = h;
h = [];
for (;++f < t; ) l && l[f].run();
f = -1;
t = h.length;
}
l = null;
u = !1;
c(e);
}
}
r.nextTick = function(e) {
var t = new Array(arguments.length - 1);
if (arguments.length > 1) for (var i = 1; i < arguments.length; i++) t[i - 1] = arguments[i];
h.push(new m(e, t));
1 !== h.length || u || s(p);
};
function m(e, t) {
this.fun = e;
this.array = t;
}
m.prototype.run = function() {
this.fun.apply(null, this.array);
};
r.title = "browser";
r.browser = !0;
r.env = {};
r.argv = [];
r.version = "";
r.versions = {};
function g() {}
r.on = g;
r.addListener = g;
r.once = g;
r.off = g;
r.removeListener = g;
r.removeAllListeners = g;
r.emit = g;
r.prependListener = g;
r.prependOnceListener = g;
r.listeners = function() {
return [];
};
r.binding = function() {
throw new Error("process.binding is not supported");
};
r.cwd = function() {
return "/";
};
r.chdir = function() {
throw new Error("process.chdir is not supported");
};
r.umask = function() {
return 0;
};
}, {} ],
157: [ function(e, t, i) {
i.publicEncrypt = e("./publicEncrypt");
i.privateDecrypt = e("./privateDecrypt");
i.privateEncrypt = function(e, t) {
return i.publicEncrypt(e, t, !0);
};
i.publicDecrypt = function(e, t) {
return i.privateDecrypt(e, t, !0);
};
}, {
"./privateDecrypt": 160,
"./publicEncrypt": 161
} ],
158: [ function(e, t) {
var i = e("create-hash"), n = e("safe-buffer").Buffer;
t.exports = function(e, t) {
for (var o, a = n.alloc(0), s = 0; a.length < t; ) {
o = r(s++);
a = n.concat([ a, i("sha1").update(e).update(o).digest() ]);
}
return a.slice(0, t);
};
function r(e) {
var t = n.allocUnsafe(4);
t.writeUInt32BE(e, 0);
return t;
}
}, {
"create-hash": 71,
"safe-buffer": 182
} ],
159: [ function(e, t, i) {
arguments[4][15][0].apply(i, arguments);
}, {
buffer: 19,
dup: 15
} ],
160: [ function(e, t) {
var i = e("parse-asn1"), n = e("./mgf"), r = e("./xor"), o = e("bn.js"), a = e("browserify-rsa"), s = e("create-hash"), c = e("./withPublic"), l = e("safe-buffer").Buffer;
t.exports = function(e, t, n) {
var r;
r = e.padding ? e.padding : n ? 1 : 4;
var s, f = i(e), d = f.modulus.byteLength();
if (t.length > d || new o(t).cmp(f.modulus) >= 0) throw new Error("decryption error");
s = n ? c(new o(t), f) : a(t, f);
var p = l.alloc(d - s.length);
s = l.concat([ p, s ], d);
if (4 === r) return h(f, s);
if (1 === r) return u(0, s, n);
if (3 === r) return s;
throw new Error("unknown padding");
};
function h(e, t) {
var i = e.modulus.byteLength(), o = s("sha1").update(l.alloc(0)).digest(), a = o.length;
if (0 !== t[0]) throw new Error("decryption error");
var c = t.slice(1, a + 1), h = t.slice(a + 1), u = r(c, n(h, a)), d = r(h, n(u, i - a - 1));
if (f(o, d.slice(0, a))) throw new Error("decryption error");
for (var p = a; 0 === d[p]; ) p++;
if (1 !== d[p++]) throw new Error("decryption error");
return d.slice(p);
}
function u(e, t, i) {
for (var n = t.slice(0, 2), r = 2, o = 0; 0 !== t[r++]; ) if (r >= t.length) {
o++;
break;
}
var a = t.slice(2, r - 1);
("0002" !== n.toString("hex") && !i || "0001" !== n.toString("hex") && i) && o++;
a.length < 8 && o++;
if (o) throw new Error("decryption error");
return t.slice(r);
}
function f(e, t) {
e = l.from(e);
t = l.from(t);
var i = 0, n = e.length;
if (e.length !== t.length) {
i++;
n = Math.min(e.length, t.length);
}
for (var r = -1; ++r < n; ) i += e[r] ^ t[r];
return i;
}
}, {
"./mgf": 158,
"./withPublic": 162,
"./xor": 163,
"bn.js": 159,
"browserify-rsa": 40,
"create-hash": 71,
"parse-asn1": 148,
"safe-buffer": 182
} ],
161: [ function(e, t) {
var i = e("parse-asn1"), n = e("randombytes"), r = e("create-hash"), o = e("./mgf"), a = e("./xor"), s = e("bn.js"), c = e("./withPublic"), l = e("browserify-rsa"), h = e("safe-buffer").Buffer;
t.exports = function(e, t, n) {
var r;
r = e.padding ? e.padding : n ? 1 : 4;
var o, a = i(e);
if (4 === r) o = u(a, t); else if (1 === r) o = f(a, t, n); else {
if (3 !== r) throw new Error("unknown padding");
if ((o = new s(t)).cmp(a.modulus) >= 0) throw new Error("data too long for modulus");
}
return n ? l(o, a) : c(o, a);
};
function u(e, t) {
var i = e.modulus.byteLength(), c = t.length, l = r("sha1").update(h.alloc(0)).digest(), u = l.length, f = 2 * u;
if (c > i - f - 2) throw new Error("message too long");
var d = h.alloc(i - c - f - 2), p = i - u - 1, m = n(u), g = a(h.concat([ l, d, h.alloc(1, 1), t ], p), o(m, p)), y = a(m, o(g, u));
return new s(h.concat([ h.alloc(1), y, g ], i));
}
function f(e, t, i) {
var n, r = t.length, o = e.modulus.byteLength();
if (r > o - 11) throw new Error("message too long");
n = i ? h.alloc(o - r - 3, 255) : d(o - r - 3);
return new s(h.concat([ h.from([ 0, i ? 1 : 2 ]), n, h.alloc(1), t ], o));
}
function d(e) {
for (var t, i = h.allocUnsafe(e), r = 0, o = n(2 * e), a = 0; r < e; ) {
if (a === o.length) {
o = n(2 * e);
a = 0;
}
(t = o[a++]) && (i[r++] = t);
}
return i;
}
}, {
"./mgf": 158,
"./withPublic": 162,
"./xor": 163,
"bn.js": 159,
"browserify-rsa": 40,
"create-hash": 71,
"parse-asn1": 148,
randombytes: 164,
"safe-buffer": 182
} ],
162: [ function(e, t) {
var i = e("bn.js"), n = e("safe-buffer").Buffer;
t.exports = function(e, t) {
return n.from(e.toRed(i.mont(t.modulus)).redPow(new i(t.publicExponent)).fromRed().toArray());
};
}, {
"bn.js": 159,
"safe-buffer": 182
} ],
163: [ function(e, t) {
t.exports = function(e, t) {
for (var i = e.length, n = -1; ++n < i; ) e[n] ^= t[n];
return e;
};
}, {} ],
164: [ function(e, t) {
(function(i, n) {
"use strict";
var r = e("safe-buffer").Buffer, o = n.crypto || n.msCrypto;
o && o.getRandomValues ? t.exports = function(e, t) {
if (e > 4294967295) throw new RangeError("requested too many random bytes");
var n = r.allocUnsafe(e);
if (e > 0) if (e > 65536) for (var a = 0; a < e; a += 65536) o.getRandomValues(n.slice(a, a + 65536)); else o.getRandomValues(n);
return "function" == typeof t ? i.nextTick(function() {
t(null, n);
}) : n;
} : t.exports = function() {
throw new Error("Secure random number generation is not supported by this browser.\nUse Chrome, Firefox or Internet Explorer 11");
};
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
_process: 156,
"safe-buffer": 182
} ],
165: [ function(e, t, i) {
(function(t, n) {
"use strict";
function r() {
throw new Error("secure random number generation not supported by this browser\nuse chrome, FireFox or Internet Explorer 11");
}
var o = e("safe-buffer"), a = e("randombytes"), s = o.Buffer, c = o.kMaxLength, l = n.crypto || n.msCrypto, h = Math.pow(2, 32) - 1;
function u(e, t) {
if ("number" != typeof e || e != e) throw new TypeError("offset must be a number");
if (e > h || e < 0) throw new TypeError("offset must be a uint32");
if (e > c || e > t) throw new RangeError("offset out of range");
}
function f(e, t, i) {
if ("number" != typeof e || e != e) throw new TypeError("size must be a number");
if (e > h || e < 0) throw new TypeError("size must be a uint32");
if (e + t > i || e > c) throw new RangeError("buffer too small");
}
if (l && l.getRandomValues || !t.browser) {
i.randomFill = function(e, t, i, r) {
if (!(s.isBuffer(e) || e instanceof n.Uint8Array)) throw new TypeError('"buf" argument must be a Buffer or Uint8Array');
if ("function" == typeof t) {
r = t;
t = 0;
i = e.length;
} else if ("function" == typeof i) {
r = i;
i = e.length - t;
} else if ("function" != typeof r) throw new TypeError('"cb" argument must be a function');
u(t, e.length);
f(i, t, e.length);
return d(e, t, i, r);
};
i.randomFillSync = function(e, t, i) {
"undefined" == typeof t && (t = 0);
if (!(s.isBuffer(e) || e instanceof n.Uint8Array)) throw new TypeError('"buf" argument must be a Buffer or Uint8Array');
u(t, e.length);
void 0 === i && (i = e.length - t);
f(i, t, e.length);
return d(e, t, i);
};
} else {
i.randomFill = r;
i.randomFillSync = r;
}
function d(e, i, n, r) {
if (t.browser) {
var o = e.buffer, s = new Uint8Array(o, i, n);
l.getRandomValues(s);
if (r) {
t.nextTick(function() {
r(null, e);
});
return;
}
return e;
}
if (!r) {
a(n).copy(e, i);
return e;
}
a(n, function(t, n) {
if (t) return r(t);
n.copy(e, i);
r(null, e);
});
}
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
_process: 156,
randombytes: 164,
"safe-buffer": 182
} ],
166: [ function(e, t) {
t.exports = e("./lib/_stream_duplex.js");
}, {
"./lib/_stream_duplex.js": 167
} ],
167: [ function(e, t) {
"use strict";
var i = e("process-nextick-args"), n = Object.keys || function(e) {
var t = [];
for (var i in e) t.push(i);
return t;
};
t.exports = h;
var r = Object.create(e("core-util-is"));
r.inherits = e("inherits");
var o = e("./_stream_readable"), a = e("./_stream_writable");
r.inherits(h, o);
for (var s = n(a.prototype), c = 0; c < s.length; c++) {
var l = s[c];
h.prototype[l] || (h.prototype[l] = a.prototype[l]);
}
function h(e) {
if (!(this instanceof h)) return new h(e);
o.call(this, e);
a.call(this, e);
e && !1 === e.readable && (this.readable = !1);
e && !1 === e.writable && (this.writable = !1);
this.allowHalfOpen = !0;
e && !1 === e.allowHalfOpen && (this.allowHalfOpen = !1);
this.once("end", u);
}
Object.defineProperty(h.prototype, "writableHighWaterMark", {
enumerable: !1,
get: function() {
return this._writableState.highWaterMark;
}
});
function u() {
this.allowHalfOpen || this._writableState.ended || i.nextTick(f, this);
}
function f(e) {
e.end();
}
Object.defineProperty(h.prototype, "destroyed", {
get: function() {
return void 0 !== this._readableState && void 0 !== this._writableState && this._readableState.destroyed && this._writableState.destroyed;
},
set: function(e) {
if (void 0 !== this._readableState && void 0 !== this._writableState) {
this._readableState.destroyed = e;
this._writableState.destroyed = e;
}
}
});
h.prototype._destroy = function(e, t) {
this.push(null);
this.end();
i.nextTick(t, e);
};
}, {
"./_stream_readable": 169,
"./_stream_writable": 171,
"core-util-is": 68,
inherits: 138,
"process-nextick-args": 155
} ],
168: [ function(e, t) {
"use strict";
t.exports = r;
var i = e("./_stream_transform"), n = Object.create(e("core-util-is"));
n.inherits = e("inherits");
n.inherits(r, i);
function r(e) {
if (!(this instanceof r)) return new r(e);
i.call(this, e);
}
r.prototype._transform = function(e, t, i) {
i(null, e);
};
}, {
"./_stream_transform": 170,
"core-util-is": 68,
inherits: 138
} ],
169: [ function(e, t) {
(function(i, n) {
"use strict";
var r = e("process-nextick-args");
t.exports = w;
var o, a = e("isarray");
w.ReadableState = _;
e("events").EventEmitter;
var s = function(e, t) {
return e.listeners(t).length;
}, c = e("./internal/streams/stream"), l = e("safe-buffer").Buffer, h = n.Uint8Array || function() {};
function u(e) {
return l.from(e);
}
var f = Object.create(e("core-util-is"));
f.inherits = e("inherits");
var d = e("util"), p = void 0;
p = d && d.debuglog ? d.debuglog("stream") : function() {};
var m, g = e("./internal/streams/BufferList"), y = e("./internal/streams/destroy");
f.inherits(w, c);
var b = [ "error", "close", "destroy", "pause", "resume" ];
function v(e, t, i) {
if ("function" == typeof e.prependListener) return e.prependListener(t, i);
e._events && e._events[t] ? a(e._events[t]) ? e._events[t].unshift(i) : e._events[t] = [ i, e._events[t] ] : e.on(t, i);
}
function _(t, i) {
t = t || {};
var n = i instanceof (o = o || e("./_stream_duplex"));
this.objectMode = !!t.objectMode;
n && (this.objectMode = this.objectMode || !!t.readableObjectMode);
var r = t.highWaterMark, a = t.readableHighWaterMark, s = this.objectMode ? 16 : 16384;
this.highWaterMark = r || 0 === r ? r : n && (a || 0 === a) ? a : s;
this.highWaterMark = Math.floor(this.highWaterMark);
this.buffer = new g();
this.length = 0;
this.pipes = null;
this.pipesCount = 0;
this.flowing = null;
this.ended = !1;
this.endEmitted = !1;
this.reading = !1;
this.sync = !0;
this.needReadable = !1;
this.emittedReadable = !1;
this.readableListening = !1;
this.resumeScheduled = !1;
this.destroyed = !1;
this.defaultEncoding = t.defaultEncoding || "utf8";
this.awaitDrain = 0;
this.readingMore = !1;
this.decoder = null;
this.encoding = null;
if (t.encoding) {
m || (m = e("string_decoder/").StringDecoder);
this.decoder = new m(t.encoding);
this.encoding = t.encoding;
}
}
function w(t) {
o = o || e("./_stream_duplex");
if (!(this instanceof w)) return new w(t);
this._readableState = new _(t, this);
this.readable = !0;
if (t) {
"function" == typeof t.read && (this._read = t.read);
"function" == typeof t.destroy && (this._destroy = t.destroy);
}
c.call(this);
}
Object.defineProperty(w.prototype, "destroyed", {
get: function() {
return void 0 !== this._readableState && this._readableState.destroyed;
},
set: function(e) {
this._readableState && (this._readableState.destroyed = e);
}
});
w.prototype.destroy = y.destroy;
w.prototype._undestroy = y.undestroy;
w.prototype._destroy = function(e, t) {
this.push(null);
t(e);
};
w.prototype.push = function(e, t) {
var i, n = this._readableState;
if (n.objectMode) i = !0; else if ("string" == typeof e) {
if ((t = t || n.defaultEncoding) !== n.encoding) {
e = l.from(e, t);
t = "";
}
i = !0;
}
return C(this, e, t, !1, i);
};
w.prototype.unshift = function(e) {
return C(this, e, null, !0, !1);
};
function C(e, t, i, n, r) {
var o = e._readableState;
if (null === t) {
o.reading = !1;
T(e, o);
} else {
var a;
r || (a = A(o, t));
if (a) e.emit("error", a); else if (o.objectMode || t && t.length > 0) {
"string" == typeof t || o.objectMode || Object.getPrototypeOf(t) === l.prototype || (t = u(t));
if (n) o.endEmitted ? e.emit("error", new Error("stream.unshift() after end event")) : S(e, o, t, !0); else if (o.ended) e.emit("error", new Error("stream.push() after EOF")); else {
o.reading = !1;
if (o.decoder && !i) {
t = o.decoder.write(t);
o.objectMode || 0 !== t.length ? S(e, o, t, !1) : L(e, o);
} else S(e, o, t, !1);
}
} else n || (o.reading = !1);
}
return M(o);
}
function S(e, t, i, n) {
if (t.flowing && 0 === t.length && !t.sync) {
e.emit("data", i);
e.read(0);
} else {
t.length += t.objectMode ? 1 : i.length;
n ? t.buffer.unshift(i) : t.buffer.push(i);
t.needReadable && D(e);
}
L(e, t);
}
function A(e, t) {
var i, n;
(n = t, l.isBuffer(n) || n instanceof h) || "string" == typeof t || void 0 === t || e.objectMode || (i = new TypeError("Invalid non-string/buffer chunk"));
return i;
}
function M(e) {
return !e.ended && (e.needReadable || e.length < e.highWaterMark || 0 === e.length);
}
w.prototype.isPaused = function() {
return !1 === this._readableState.flowing;
};
w.prototype.setEncoding = function(t) {
m || (m = e("string_decoder/").StringDecoder);
this._readableState.decoder = new m(t);
this._readableState.encoding = t;
return this;
};
var B = 8388608;
function x(e) {
if (e >= B) e = B; else {
e--;
e |= e >>> 1;
e |= e >>> 2;
e |= e >>> 4;
e |= e >>> 8;
e |= e >>> 16;
e++;
}
return e;
}
function k(e, t) {
if (e <= 0 || 0 === t.length && t.ended) return 0;
if (t.objectMode) return 1;
if (e != e) return t.flowing && t.length ? t.buffer.head.data.length : t.length;
e > t.highWaterMark && (t.highWaterMark = x(e));
if (e <= t.length) return e;
if (!t.ended) {
t.needReadable = !0;
return 0;
}
return t.length;
}
w.prototype.read = function(e) {
p("read", e);
e = parseInt(e, 10);
var t = this._readableState, i = e;
0 !== e && (t.emittedReadable = !1);
if (0 === e && t.needReadable && (t.length >= t.highWaterMark || t.ended)) {
p("read: emitReadable", t.length, t.ended);
0 === t.length && t.ended ? z(this) : D(this);
return null;
}
if (0 === (e = k(e, t)) && t.ended) {
0 === t.length && z(this);
return null;
}
var n, r = t.needReadable;
p("need readable", r);
(0 === t.length || t.length - e < t.highWaterMark) && p("length less than watermark", r = !0);
if (t.ended || t.reading) p("reading or ended", r = !1); else if (r) {
p("do read");
t.reading = !0;
t.sync = !0;
0 === t.length && (t.needReadable = !0);
this._read(t.highWaterMark);
t.sync = !1;
t.reading || (e = k(i, t));
}
if (null === (n = e > 0 ? j(e, t) : null)) {
t.needReadable = !0;
e = 0;
} else t.length -= e;
if (0 === t.length) {
t.ended || (t.needReadable = !0);
i !== e && t.ended && z(this);
}
null !== n && this.emit("data", n);
return n;
};
function T(e, t) {
if (!t.ended) {
if (t.decoder) {
var i = t.decoder.end();
if (i && i.length) {
t.buffer.push(i);
t.length += t.objectMode ? 1 : i.length;
}
}
t.ended = !0;
D(e);
}
}
function D(e) {
var t = e._readableState;
t.needReadable = !1;
if (!t.emittedReadable) {
p("emitReadable", t.flowing);
t.emittedReadable = !0;
t.sync ? r.nextTick(E, e) : E(e);
}
}
function E(e) {
p("emit readable");
e.emit("readable");
U(e);
}
function L(e, t) {
if (!t.readingMore) {
t.readingMore = !0;
r.nextTick(P, e, t);
}
}
function P(e, t) {
for (var i = t.length; !t.reading && !t.flowing && !t.ended && t.length < t.highWaterMark; ) {
p("maybeReadMore read 0");
e.read(0);
if (i === t.length) break;
i = t.length;
}
t.readingMore = !1;
}
w.prototype._read = function() {
this.emit("error", new Error("_read() is not implemented"));
};
w.prototype.pipe = function(e, t) {
var n = this, o = this._readableState;
switch (o.pipesCount) {
case 0:
o.pipes = e;
break;

case 1:
o.pipes = [ o.pipes, e ];
break;

default:
o.pipes.push(e);
}
o.pipesCount += 1;
p("pipe count=%d opts=%j", o.pipesCount, t);
var a = t && !1 === t.end || e === i.stdout || e === i.stderr ? _ : l;
o.endEmitted ? r.nextTick(a) : n.once("end", a);
e.on("unpipe", c);
function c(e, t) {
p("onunpipe");
if (e === n && t && !1 === t.hasUnpiped) {
t.hasUnpiped = !0;
f();
}
}
function l() {
p("onend");
e.end();
}
var h = R(n);
e.on("drain", h);
var u = !1;
function f() {
p("cleanup");
e.removeListener("close", y);
e.removeListener("finish", b);
e.removeListener("drain", h);
e.removeListener("error", g);
e.removeListener("unpipe", c);
n.removeListener("end", l);
n.removeListener("end", _);
n.removeListener("data", m);
u = !0;
!o.awaitDrain || e._writableState && !e._writableState.needDrain || h();
}
var d = !1;
n.on("data", m);
function m(t) {
p("ondata");
d = !1;
if (!1 === e.write(t) && !d) {
if ((1 === o.pipesCount && o.pipes === e || o.pipesCount > 1 && -1 !== W(o.pipes, e)) && !u) {
p("false write response, pause", n._readableState.awaitDrain);
n._readableState.awaitDrain++;
d = !0;
}
n.pause();
}
}
function g(t) {
p("onerror", t);
_();
e.removeListener("error", g);
0 === s(e, "error") && e.emit("error", t);
}
v(e, "error", g);
function y() {
e.removeListener("finish", b);
_();
}
e.once("close", y);
function b() {
p("onfinish");
e.removeListener("close", y);
_();
}
e.once("finish", b);
function _() {
p("unpipe");
n.unpipe(e);
}
e.emit("pipe", n);
if (!o.flowing) {
p("pipe resume");
n.resume();
}
return e;
};
function R(e) {
return function() {
var t = e._readableState;
p("pipeOnDrain", t.awaitDrain);
t.awaitDrain && t.awaitDrain--;
if (0 === t.awaitDrain && s(e, "data")) {
t.flowing = !0;
U(e);
}
};
}
w.prototype.unpipe = function(e) {
var t = this._readableState, i = {
hasUnpiped: !1
};
if (0 === t.pipesCount) return this;
if (1 === t.pipesCount) {
if (e && e !== t.pipes) return this;
e || (e = t.pipes);
t.pipes = null;
t.pipesCount = 0;
t.flowing = !1;
e && e.emit("unpipe", this, i);
return this;
}
if (!e) {
var n = t.pipes, r = t.pipesCount;
t.pipes = null;
t.pipesCount = 0;
t.flowing = !1;
for (var o = 0; o < r; o++) n[o].emit("unpipe", this, i);
return this;
}
var a = W(t.pipes, e);
if (-1 === a) return this;
t.pipes.splice(a, 1);
t.pipesCount -= 1;
1 === t.pipesCount && (t.pipes = t.pipes[0]);
e.emit("unpipe", this, i);
return this;
};
w.prototype.on = function(e, t) {
var i = c.prototype.on.call(this, e, t);
if ("data" === e) !1 !== this._readableState.flowing && this.resume(); else if ("readable" === e) {
var n = this._readableState;
if (!n.endEmitted && !n.readableListening) {
n.readableListening = n.needReadable = !0;
n.emittedReadable = !1;
n.reading ? n.length && D(this) : r.nextTick(I, this);
}
}
return i;
};
w.prototype.addListener = w.prototype.on;
function I(e) {
p("readable nexttick read 0");
e.read(0);
}
w.prototype.resume = function() {
var e = this._readableState;
if (!e.flowing) {
p("resume");
e.flowing = !0;
N(this, e);
}
return this;
};
function N(e, t) {
if (!t.resumeScheduled) {
t.resumeScheduled = !0;
r.nextTick(O, e, t);
}
}
function O(e, t) {
if (!t.reading) {
p("resume read 0");
e.read(0);
}
t.resumeScheduled = !1;
t.awaitDrain = 0;
e.emit("resume");
U(e);
t.flowing && !t.reading && e.read(0);
}
w.prototype.pause = function() {
p("call pause flowing=%j", this._readableState.flowing);
if (!1 !== this._readableState.flowing) {
p("pause");
this._readableState.flowing = !1;
this.emit("pause");
}
return this;
};
function U(e) {
var t = e._readableState;
p("flow", t.flowing);
for (;t.flowing && null !== e.read(); ) ;
}
w.prototype.wrap = function(e) {
var t = this, i = this._readableState, n = !1;
e.on("end", function() {
p("wrapped end");
if (i.decoder && !i.ended) {
var e = i.decoder.end();
e && e.length && t.push(e);
}
t.push(null);
});
e.on("data", function(r) {
p("wrapped data");
i.decoder && (r = i.decoder.write(r));
if ((!i.objectMode || null != r) && (i.objectMode || r && r.length) && !t.push(r)) {
n = !0;
e.pause();
}
});
for (var r in e) void 0 === this[r] && "function" == typeof e[r] && (this[r] = function(t) {
return function() {
return e[t].apply(e, arguments);
};
}(r));
for (var o = 0; o < b.length; o++) e.on(b[o], this.emit.bind(this, b[o]));
this._read = function(t) {
p("wrapped _read", t);
if (n) {
n = !1;
e.resume();
}
};
return this;
};
Object.defineProperty(w.prototype, "readableHighWaterMark", {
enumerable: !1,
get: function() {
return this._readableState.highWaterMark;
}
});
w._fromList = j;
function j(e, t) {
if (0 === t.length) return null;
var i;
if (t.objectMode) i = t.buffer.shift(); else if (!e || e >= t.length) {
i = t.decoder ? t.buffer.join("") : 1 === t.buffer.length ? t.buffer.head.data : t.buffer.concat(t.length);
t.buffer.clear();
} else i = F(e, t.buffer, t.decoder);
return i;
}
function F(e, t, i) {
var n;
if (e < t.head.data.length) {
n = t.head.data.slice(0, e);
t.head.data = t.head.data.slice(e);
} else n = e === t.head.data.length ? t.shift() : i ? G(e, t) : V(e, t);
return n;
}
function G(e, t) {
var i = t.head, n = 1, r = i.data;
e -= r.length;
for (;i = i.next; ) {
var o = i.data, a = e > o.length ? o.length : e;
a === o.length ? r += o : r += o.slice(0, e);
if (0 == (e -= a)) {
if (a === o.length) {
++n;
i.next ? t.head = i.next : t.head = t.tail = null;
} else {
t.head = i;
i.data = o.slice(a);
}
break;
}
++n;
}
t.length -= n;
return r;
}
function V(e, t) {
var i = l.allocUnsafe(e), n = t.head, r = 1;
n.data.copy(i);
e -= n.data.length;
for (;n = n.next; ) {
var o = n.data, a = e > o.length ? o.length : e;
o.copy(i, i.length - e, 0, a);
if (0 == (e -= a)) {
if (a === o.length) {
++r;
n.next ? t.head = n.next : t.head = t.tail = null;
} else {
t.head = n;
n.data = o.slice(a);
}
break;
}
++r;
}
t.length -= r;
return i;
}
function z(e) {
var t = e._readableState;
if (t.length > 0) throw new Error('"endReadable()" called on non-empty stream');
if (!t.endEmitted) {
t.ended = !0;
r.nextTick(H, t, e);
}
}
function H(e, t) {
if (!e.endEmitted && 0 === e.length) {
e.endEmitted = !0;
t.readable = !1;
t.emit("end");
}
}
function W(e, t) {
for (var i = 0, n = e.length; i < n; i++) if (e[i] === t) return i;
return -1;
}
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"./_stream_duplex": 167,
"./internal/streams/BufferList": 172,
"./internal/streams/destroy": 173,
"./internal/streams/stream": 174,
_process: 156,
"core-util-is": 68,
events: 104,
inherits: 138,
isarray: 175,
"process-nextick-args": 155,
"safe-buffer": 182,
"string_decoder/": 176,
util: 19
} ],
170: [ function(e, t) {
"use strict";
t.exports = o;
var i = e("./_stream_duplex"), n = Object.create(e("core-util-is"));
n.inherits = e("inherits");
n.inherits(o, i);
function r(e, t) {
var i = this._transformState;
i.transforming = !1;
var n = i.writecb;
if (!n) return this.emit("error", new Error("write callback called multiple times"));
i.writechunk = null;
i.writecb = null;
null != t && this.push(t);
n(e);
var r = this._readableState;
r.reading = !1;
(r.needReadable || r.length < r.highWaterMark) && this._read(r.highWaterMark);
}
function o(e) {
if (!(this instanceof o)) return new o(e);
i.call(this, e);
this._transformState = {
afterTransform: r.bind(this),
needTransform: !1,
transforming: !1,
writecb: null,
writechunk: null,
writeencoding: null
};
this._readableState.needReadable = !0;
this._readableState.sync = !1;
if (e) {
"function" == typeof e.transform && (this._transform = e.transform);
"function" == typeof e.flush && (this._flush = e.flush);
}
this.on("prefinish", a);
}
function a() {
var e = this;
"function" == typeof this._flush ? this._flush(function(t, i) {
s(e, t, i);
}) : s(this, null, null);
}
o.prototype.push = function(e, t) {
this._transformState.needTransform = !1;
return i.prototype.push.call(this, e, t);
};
o.prototype._transform = function() {
throw new Error("_transform() is not implemented");
};
o.prototype._write = function(e, t, i) {
var n = this._transformState;
n.writecb = i;
n.writechunk = e;
n.writeencoding = t;
if (!n.transforming) {
var r = this._readableState;
(n.needTransform || r.needReadable || r.length < r.highWaterMark) && this._read(r.highWaterMark);
}
};
o.prototype._read = function() {
var e = this._transformState;
if (null !== e.writechunk && e.writecb && !e.transforming) {
e.transforming = !0;
this._transform(e.writechunk, e.writeencoding, e.afterTransform);
} else e.needTransform = !0;
};
o.prototype._destroy = function(e, t) {
var n = this;
i.prototype._destroy.call(this, e, function(e) {
t(e);
n.emit("close");
});
};
function s(e, t, i) {
if (t) return e.emit("error", t);
null != i && e.push(i);
if (e._writableState.length) throw new Error("Calling transform done when ws.length != 0");
if (e._transformState.transforming) throw new Error("Calling transform done when still transforming");
return e.push(null);
}
}, {
"./_stream_duplex": 167,
"core-util-is": 68,
inherits: 138
} ],
171: [ function(e, t) {
(function(i, n) {
"use strict";
var r = e("process-nextick-args");
t.exports = b;
function o(e) {
var t = this;
this.next = null;
this.entry = null;
this.finish = function() {
I(t, e);
};
}
var a, s = !i.browser && [ "v0.10", "v0.9." ].indexOf(i.version.slice(0, 5)) > -1 ? setImmediate : r.nextTick;
b.WritableState = y;
var c = Object.create(e("core-util-is"));
c.inherits = e("inherits");
var l = {
deprecate: e("util-deprecate")
}, h = e("./internal/streams/stream"), u = e("safe-buffer").Buffer, f = n.Uint8Array || function() {};
function d(e) {
return u.from(e);
}
var p, m = e("./internal/streams/destroy");
c.inherits(b, h);
function g() {}
function y(t, i) {
a = a || e("./_stream_duplex");
t = t || {};
var n = i instanceof a;
this.objectMode = !!t.objectMode;
n && (this.objectMode = this.objectMode || !!t.writableObjectMode);
var r = t.highWaterMark, s = t.writableHighWaterMark, c = this.objectMode ? 16 : 16384;
this.highWaterMark = r || 0 === r ? r : n && (s || 0 === s) ? s : c;
this.highWaterMark = Math.floor(this.highWaterMark);
this.finalCalled = !1;
this.needDrain = !1;
this.ending = !1;
this.ended = !1;
this.finished = !1;
this.destroyed = !1;
var l = !1 === t.decodeStrings;
this.decodeStrings = !l;
this.defaultEncoding = t.defaultEncoding || "utf8";
this.length = 0;
this.writing = !1;
this.corked = 0;
this.sync = !0;
this.bufferProcessing = !1;
this.onwrite = function(e) {
B(i, e);
};
this.writecb = null;
this.writelen = 0;
this.bufferedRequest = null;
this.lastBufferedRequest = null;
this.pendingcb = 0;
this.prefinished = !1;
this.errorEmitted = !1;
this.bufferedRequestCount = 0;
this.corkedRequestsFree = new o(this);
}
y.prototype.getBuffer = function() {
for (var e = this.bufferedRequest, t = []; e; ) {
t.push(e);
e = e.next;
}
return t;
};
(function() {
try {
Object.defineProperty(y.prototype, "buffer", {
get: l.deprecate(function() {
return this.getBuffer();
}, "_writableState.buffer is deprecated. Use _writableState.getBuffer instead.", "DEP0003")
});
} catch (e) {}
})();
if ("function" == typeof Symbol && Symbol.hasInstance && "function" == typeof Function.prototype[Symbol.hasInstance]) {
p = Function.prototype[Symbol.hasInstance];
Object.defineProperty(b, Symbol.hasInstance, {
value: function(e) {
return !!p.call(this, e) || this === b && e && e._writableState instanceof y;
}
});
} else p = function(e) {
return e instanceof this;
};
function b(t) {
a = a || e("./_stream_duplex");
if (!(p.call(b, this) || this instanceof a)) return new b(t);
this._writableState = new y(t, this);
this.writable = !0;
if (t) {
"function" == typeof t.write && (this._write = t.write);
"function" == typeof t.writev && (this._writev = t.writev);
"function" == typeof t.destroy && (this._destroy = t.destroy);
"function" == typeof t.final && (this._final = t.final);
}
h.call(this);
}
b.prototype.pipe = function() {
this.emit("error", new Error("Cannot pipe, not readable"));
};
function v(e, t) {
var i = new Error("write after end");
e.emit("error", i);
r.nextTick(t, i);
}
function _(e, t, i, n) {
var o = !0, a = !1;
null === i ? a = new TypeError("May not write null values to stream") : "string" == typeof i || void 0 === i || t.objectMode || (a = new TypeError("Invalid non-string/buffer chunk"));
if (a) {
e.emit("error", a);
r.nextTick(n, a);
o = !1;
}
return o;
}
b.prototype.write = function(e, t, i) {
var n, r = this._writableState, o = !1, a = !r.objectMode && (n = e, u.isBuffer(n) || n instanceof f);
a && !u.isBuffer(e) && (e = d(e));
if ("function" == typeof t) {
i = t;
t = null;
}
a ? t = "buffer" : t || (t = r.defaultEncoding);
"function" != typeof i && (i = g);
if (r.ended) v(this, i); else if (a || _(this, r, e, i)) {
r.pendingcb++;
o = C(this, r, a, e, t, i);
}
return o;
};
b.prototype.cork = function() {
this._writableState.corked++;
};
b.prototype.uncork = function() {
var e = this._writableState;
if (e.corked) {
e.corked--;
e.writing || e.corked || e.finished || e.bufferProcessing || !e.bufferedRequest || T(this, e);
}
};
b.prototype.setDefaultEncoding = function(e) {
"string" == typeof e && (e = e.toLowerCase());
if (!([ "hex", "utf8", "utf-8", "ascii", "binary", "base64", "ucs2", "ucs-2", "utf16le", "utf-16le", "raw" ].indexOf((e + "").toLowerCase()) > -1)) throw new TypeError("Unknown encoding: " + e);
this._writableState.defaultEncoding = e;
return this;
};
function w(e, t, i) {
e.objectMode || !1 === e.decodeStrings || "string" != typeof t || (t = u.from(t, i));
return t;
}
Object.defineProperty(b.prototype, "writableHighWaterMark", {
enumerable: !1,
get: function() {
return this._writableState.highWaterMark;
}
});
function C(e, t, i, n, r, o) {
if (!i) {
var a = w(t, n, r);
if (n !== a) {
i = !0;
r = "buffer";
n = a;
}
}
var s = t.objectMode ? 1 : n.length;
t.length += s;
var c = t.length < t.highWaterMark;
c || (t.needDrain = !0);
if (t.writing || t.corked) {
var l = t.lastBufferedRequest;
t.lastBufferedRequest = {
chunk: n,
encoding: r,
isBuf: i,
callback: o,
next: null
};
l ? l.next = t.lastBufferedRequest : t.bufferedRequest = t.lastBufferedRequest;
t.bufferedRequestCount += 1;
} else S(e, t, !1, s, n, r, o);
return c;
}
function S(e, t, i, n, r, o, a) {
t.writelen = n;
t.writecb = a;
t.writing = !0;
t.sync = !0;
i ? e._writev(r, t.onwrite) : e._write(r, o, t.onwrite);
t.sync = !1;
}
function A(e, t, i, n, o) {
--t.pendingcb;
if (i) {
r.nextTick(o, n);
r.nextTick(P, e, t);
e._writableState.errorEmitted = !0;
e.emit("error", n);
} else {
o(n);
e._writableState.errorEmitted = !0;
e.emit("error", n);
P(e, t);
}
}
function M(e) {
e.writing = !1;
e.writecb = null;
e.length -= e.writelen;
e.writelen = 0;
}
function B(e, t) {
var i = e._writableState, n = i.sync, r = i.writecb;
M(i);
if (t) A(e, i, n, t, r); else {
var o = D(i);
o || i.corked || i.bufferProcessing || !i.bufferedRequest || T(e, i);
n ? s(x, e, i, o, r) : x(e, i, o, r);
}
}
function x(e, t, i, n) {
i || k(e, t);
t.pendingcb--;
n();
P(e, t);
}
function k(e, t) {
if (0 === t.length && t.needDrain) {
t.needDrain = !1;
e.emit("drain");
}
}
function T(e, t) {
t.bufferProcessing = !0;
var i = t.bufferedRequest;
if (e._writev && i && i.next) {
var n = t.bufferedRequestCount, r = new Array(n), a = t.corkedRequestsFree;
a.entry = i;
for (var s = 0, c = !0; i; ) {
r[s] = i;
i.isBuf || (c = !1);
i = i.next;
s += 1;
}
r.allBuffers = c;
S(e, t, !0, t.length, r, "", a.finish);
t.pendingcb++;
t.lastBufferedRequest = null;
if (a.next) {
t.corkedRequestsFree = a.next;
a.next = null;
} else t.corkedRequestsFree = new o(t);
t.bufferedRequestCount = 0;
} else {
for (;i; ) {
var l = i.chunk, h = i.encoding, u = i.callback;
S(e, t, !1, t.objectMode ? 1 : l.length, l, h, u);
i = i.next;
t.bufferedRequestCount--;
if (t.writing) break;
}
null === i && (t.lastBufferedRequest = null);
}
t.bufferedRequest = i;
t.bufferProcessing = !1;
}
b.prototype._write = function(e, t, i) {
i(new Error("_write() is not implemented"));
};
b.prototype._writev = null;
b.prototype.end = function(e, t, i) {
var n = this._writableState;
if ("function" == typeof e) {
i = e;
e = null;
t = null;
} else if ("function" == typeof t) {
i = t;
t = null;
}
null != e && this.write(e, t);
if (n.corked) {
n.corked = 1;
this.uncork();
}
n.ending || n.finished || R(this, n, i);
};
function D(e) {
return e.ending && 0 === e.length && null === e.bufferedRequest && !e.finished && !e.writing;
}
function E(e, t) {
e._final(function(i) {
t.pendingcb--;
i && e.emit("error", i);
t.prefinished = !0;
e.emit("prefinish");
P(e, t);
});
}
function L(e, t) {
if (!t.prefinished && !t.finalCalled) if ("function" == typeof e._final) {
t.pendingcb++;
t.finalCalled = !0;
r.nextTick(E, e, t);
} else {
t.prefinished = !0;
e.emit("prefinish");
}
}
function P(e, t) {
var i = D(t);
if (i) {
L(e, t);
if (0 === t.pendingcb) {
t.finished = !0;
e.emit("finish");
}
}
return i;
}
function R(e, t, i) {
t.ending = !0;
P(e, t);
i && (t.finished ? r.nextTick(i) : e.once("finish", i));
t.ended = !0;
e.writable = !1;
}
function I(e, t, i) {
var n = e.entry;
e.entry = null;
for (;n; ) {
var r = n.callback;
t.pendingcb--;
r(i);
n = n.next;
}
t.corkedRequestsFree ? t.corkedRequestsFree.next = e : t.corkedRequestsFree = e;
}
Object.defineProperty(b.prototype, "destroyed", {
get: function() {
return void 0 !== this._writableState && this._writableState.destroyed;
},
set: function(e) {
this._writableState && (this._writableState.destroyed = e);
}
});
b.prototype.destroy = m.destroy;
b.prototype._undestroy = m.undestroy;
b.prototype._destroy = function(e, t) {
this.end();
t(e);
};
}).call(this, e("_process"), "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
"./_stream_duplex": 167,
"./internal/streams/destroy": 173,
"./internal/streams/stream": 174,
_process: 156,
"core-util-is": 68,
inherits: 138,
"process-nextick-args": 155,
"safe-buffer": 182,
"util-deprecate": 194
} ],
172: [ function(e, t) {
"use strict";
function i(e, t) {
if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function");
}
var n = e("safe-buffer").Buffer, r = e("util");
t.exports = function() {
function e() {
i(this, e);
this.head = null;
this.tail = null;
this.length = 0;
}
e.prototype.push = function(e) {
var t = {
data: e,
next: null
};
this.length > 0 ? this.tail.next = t : this.head = t;
this.tail = t;
++this.length;
};
e.prototype.unshift = function(e) {
var t = {
data: e,
next: this.head
};
0 === this.length && (this.tail = t);
this.head = t;
++this.length;
};
e.prototype.shift = function() {
if (0 !== this.length) {
var e = this.head.data;
1 === this.length ? this.head = this.tail = null : this.head = this.head.next;
--this.length;
return e;
}
};
e.prototype.clear = function() {
this.head = this.tail = null;
this.length = 0;
};
e.prototype.join = function(e) {
if (0 === this.length) return "";
for (var t = this.head, i = "" + t.data; t = t.next; ) i += e + t.data;
return i;
};
e.prototype.concat = function(e) {
if (0 === this.length) return n.alloc(0);
if (1 === this.length) return this.head.data;
for (var t, i, r = n.allocUnsafe(e >>> 0), o = this.head, a = 0; o; ) {
t = r, i = a, o.data.copy(t, i);
a += o.data.length;
o = o.next;
}
return r;
};
return e;
}();
r && r.inspect && r.inspect.custom && (t.exports.prototype[r.inspect.custom] = function() {
var e = r.inspect({
length: this.length
});
return this.constructor.name + " " + e;
});
}, {
"safe-buffer": 182,
util: 19
} ],
173: [ function(e, t) {
"use strict";
var i = e("process-nextick-args");
function n(e, t) {
e.emit("error", t);
}
t.exports = {
destroy: function(e, t) {
var r = this, o = this._readableState && this._readableState.destroyed, a = this._writableState && this._writableState.destroyed;
if (o || a) {
t ? t(e) : !e || this._writableState && this._writableState.errorEmitted || i.nextTick(n, this, e);
return this;
}
this._readableState && (this._readableState.destroyed = !0);
this._writableState && (this._writableState.destroyed = !0);
this._destroy(e || null, function(e) {
if (!t && e) {
i.nextTick(n, r, e);
r._writableState && (r._writableState.errorEmitted = !0);
} else t && t(e);
});
return this;
},
undestroy: function() {
if (this._readableState) {
this._readableState.destroyed = !1;
this._readableState.reading = !1;
this._readableState.ended = !1;
this._readableState.endEmitted = !1;
}
if (this._writableState) {
this._writableState.destroyed = !1;
this._writableState.ended = !1;
this._writableState.ending = !1;
this._writableState.finished = !1;
this._writableState.errorEmitted = !1;
}
}
};
}, {
"process-nextick-args": 155
} ],
174: [ function(e, t, i) {
arguments[4][60][0].apply(i, arguments);
}, {
dup: 60,
events: 104
} ],
175: [ function(e, t, i) {
arguments[4][66][0].apply(i, arguments);
}, {
dup: 66
} ],
176: [ function(e, t, i) {
arguments[4][63][0].apply(i, arguments);
}, {
dup: 63,
"safe-buffer": 182
} ],
177: [ function(e, t) {
t.exports = e("./readable").PassThrough;
}, {
"./readable": 178
} ],
178: [ function(e, t, i) {
(i = t.exports = e("./lib/_stream_readable.js")).Stream = i;
i.Readable = i;
i.Writable = e("./lib/_stream_writable.js");
i.Duplex = e("./lib/_stream_duplex.js");
i.Transform = e("./lib/_stream_transform.js");
i.PassThrough = e("./lib/_stream_passthrough.js");
}, {
"./lib/_stream_duplex.js": 167,
"./lib/_stream_passthrough.js": 168,
"./lib/_stream_readable.js": 169,
"./lib/_stream_transform.js": 170,
"./lib/_stream_writable.js": 171
} ],
179: [ function(e, t) {
t.exports = e("./readable").Transform;
}, {
"./readable": 178
} ],
180: [ function(e, t) {
t.exports = e("./lib/_stream_writable.js");
}, {
"./lib/_stream_writable.js": 171
} ],
181: [ function(e, t) {
"use strict";
var i = e("buffer").Buffer, n = e("inherits"), r = e("hash-base"), o = new Array(16), a = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13 ], s = [ 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11 ], c = [ 11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6 ], l = [ 8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11 ], h = [ 0, 1518500249, 1859775393, 2400959708, 2840853838 ], u = [ 1352829926, 1548603684, 1836072691, 2053994217, 0 ];
function f() {
r.call(this, 64);
this._a = 1732584193;
this._b = 4023233417;
this._c = 2562383102;
this._d = 271733878;
this._e = 3285377520;
}
n(f, r);
f.prototype._update = function() {
for (var e = o, t = 0; t < 16; ++t) e[t] = this._block.readInt32LE(4 * t);
for (var i = 0 | this._a, n = 0 | this._b, r = 0 | this._c, f = 0 | this._d, v = 0 | this._e, _ = 0 | this._a, w = 0 | this._b, C = 0 | this._c, S = 0 | this._d, A = 0 | this._e, M = 0; M < 80; M += 1) {
var B, x;
if (M < 16) {
B = p(i, n, r, f, v, e[a[M]], h[0], c[M]);
x = b(_, w, C, S, A, e[s[M]], u[0], l[M]);
} else if (M < 32) {
B = m(i, n, r, f, v, e[a[M]], h[1], c[M]);
x = y(_, w, C, S, A, e[s[M]], u[1], l[M]);
} else if (M < 48) {
B = g(i, n, r, f, v, e[a[M]], h[2], c[M]);
x = g(_, w, C, S, A, e[s[M]], u[2], l[M]);
} else if (M < 64) {
B = y(i, n, r, f, v, e[a[M]], h[3], c[M]);
x = m(_, w, C, S, A, e[s[M]], u[3], l[M]);
} else {
B = b(i, n, r, f, v, e[a[M]], h[4], c[M]);
x = p(_, w, C, S, A, e[s[M]], u[4], l[M]);
}
i = v;
v = f;
f = d(r, 10);
r = n;
n = B;
_ = A;
A = S;
S = d(C, 10);
C = w;
w = x;
}
var k = this._b + r + S | 0;
this._b = this._c + f + A | 0;
this._c = this._d + v + _ | 0;
this._d = this._e + i + w | 0;
this._e = this._a + n + C | 0;
this._a = k;
};
f.prototype._digest = function() {
this._block[this._blockOffset++] = 128;
if (this._blockOffset > 56) {
this._block.fill(0, this._blockOffset, 64);
this._update();
this._blockOffset = 0;
}
this._block.fill(0, this._blockOffset, 56);
this._block.writeUInt32LE(this._length[0], 56);
this._block.writeUInt32LE(this._length[1], 60);
this._update();
var e = i.alloc ? i.alloc(20) : new i(20);
e.writeInt32LE(this._a, 0);
e.writeInt32LE(this._b, 4);
e.writeInt32LE(this._c, 8);
e.writeInt32LE(this._d, 12);
e.writeInt32LE(this._e, 16);
return e;
};
function d(e, t) {
return e << t | e >>> 32 - t;
}
function p(e, t, i, n, r, o, a, s) {
return d(e + (t ^ i ^ n) + o + a | 0, s) + r | 0;
}
function m(e, t, i, n, r, o, a, s) {
return d(e + (t & i | ~t & n) + o + a | 0, s) + r | 0;
}
function g(e, t, i, n, r, o, a, s) {
return d(e + ((t | ~i) ^ n) + o + a | 0, s) + r | 0;
}
function y(e, t, i, n, r, o, a, s) {
return d(e + (t & n | i & ~n) + o + a | 0, s) + r | 0;
}
function b(e, t, i, n, r, o, a, s) {
return d(e + (t ^ (i | ~n)) + o + a | 0, s) + r | 0;
}
t.exports = f;
}, {
buffer: 65,
"hash-base": 106,
inherits: 138
} ],
182: [ function(e, t, i) {
var n = e("buffer"), r = n.Buffer;
function o(e, t) {
for (var i in e) t[i] = e[i];
}
if (r.from && r.alloc && r.allocUnsafe && r.allocUnsafeSlow) t.exports = n; else {
o(n, i);
i.Buffer = a;
}
function a(e, t, i) {
return r(e, t, i);
}
o(r, a);
a.from = function(e, t, i) {
if ("number" == typeof e) throw new TypeError("Argument must not be a number");
return r(e, t, i);
};
a.alloc = function(e, t, i) {
if ("number" != typeof e) throw new TypeError("Argument must be a number");
var n = r(e);
void 0 !== t ? "string" == typeof i ? n.fill(t, i) : n.fill(t) : n.fill(0);
return n;
};
a.allocUnsafe = function(e) {
if ("number" != typeof e) throw new TypeError("Argument must be a number");
return r(e);
};
a.allocUnsafeSlow = function(e) {
if ("number" != typeof e) throw new TypeError("Argument must be a number");
return n.SlowBuffer(e);
};
}, {
buffer: 65
} ],
183: [ function(e, t) {
(function(i) {
"use strict";
var n, r = e("buffer"), o = r.Buffer, a = {};
for (n in r) r.hasOwnProperty(n) && "SlowBuffer" !== n && "Buffer" !== n && (a[n] = r[n]);
var s = a.Buffer = {};
for (n in o) o.hasOwnProperty(n) && "allocUnsafe" !== n && "allocUnsafeSlow" !== n && (s[n] = o[n]);
a.Buffer.prototype = o.prototype;
s.from && s.from !== Uint8Array.from || (s.from = function(e, t, i) {
if ("number" == typeof e) throw new TypeError('The "value" argument must not be of type number. Received type ' + typeof e);
if (e && "undefined" == typeof e.length) throw new TypeError("The first argument must be one of type string, Buffer, ArrayBuffer, Array, or Array-like Object. Received type " + typeof e);
return o(e, t, i);
});
s.alloc || (s.alloc = function(e, t, i) {
if ("number" != typeof e) throw new TypeError('The "size" argument must be of type number. Received type ' + typeof e);
if (e < 0 || e >= 2 * (1 << 30)) throw new RangeError('The value "' + e + '" is invalid for option "size"');
var n = o(e);
t && 0 !== t.length ? "string" == typeof i ? n.fill(t, i) : n.fill(t) : n.fill(0);
return n;
});
if (!a.kStringMaxLength) try {
a.kStringMaxLength = i.binding("buffer").kStringMaxLength;
} catch (e) {}
if (!a.constants) {
a.constants = {
MAX_LENGTH: a.kMaxLength
};
a.kStringMaxLength && (a.constants.MAX_STRING_LENGTH = a.kStringMaxLength);
}
t.exports = a;
}).call(this, e("_process"));
}, {
_process: 156,
buffer: 65
} ],
184: [ function(e, t) {
var i = e("safe-buffer").Buffer;
function n(e, t) {
this._block = i.alloc(e);
this._finalSize = t;
this._blockSize = e;
this._len = 0;
}
n.prototype.update = function(e, t) {
if ("string" == typeof e) {
t = t || "utf8";
e = i.from(e, t);
}
for (var n = this._block, r = this._blockSize, o = e.length, a = this._len, s = 0; s < o; ) {
for (var c = a % r, l = Math.min(o - s, r - c), h = 0; h < l; h++) n[c + h] = e[s + h];
s += l;
(a += l) % r == 0 && this._update(n);
}
this._len += o;
return this;
};
n.prototype.digest = function(e) {
var t = this._len % this._blockSize;
this._block[t] = 128;
this._block.fill(0, t + 1);
if (t >= this._finalSize) {
this._update(this._block);
this._block.fill(0);
}
var i = 8 * this._len;
if (i <= 4294967295) this._block.writeUInt32BE(i, this._blockSize - 4); else {
var n = (4294967295 & i) >>> 0, r = (i - n) / 4294967296;
this._block.writeUInt32BE(r, this._blockSize - 8);
this._block.writeUInt32BE(n, this._blockSize - 4);
}
this._update(this._block);
var o = this._hash();
return e ? o.toString(e) : o;
};
n.prototype._update = function() {
throw new Error("_update must be implemented by subclass");
};
t.exports = n;
}, {
"safe-buffer": 182
} ],
185: [ function(e, t, i) {
(i = t.exports = function(e) {
e = e.toLowerCase();
var t = i[e];
if (!t) throw new Error(e + " is not supported (we accept pull requests)");
return new t();
}).sha = e("./sha");
i.sha1 = e("./sha1");
i.sha224 = e("./sha224");
i.sha256 = e("./sha256");
i.sha384 = e("./sha384");
i.sha512 = e("./sha512");
}, {
"./sha": 186,
"./sha1": 187,
"./sha224": 188,
"./sha256": 189,
"./sha384": 190,
"./sha512": 191
} ],
186: [ function(e, t) {
var i = e("inherits"), n = e("./hash"), r = e("safe-buffer").Buffer, o = [ 1518500249, 1859775393, -1894007588, -899497514 ], a = new Array(80);
function s() {
this.init();
this._w = a;
n.call(this, 64, 56);
}
i(s, n);
s.prototype.init = function() {
this._a = 1732584193;
this._b = 4023233417;
this._c = 2562383102;
this._d = 271733878;
this._e = 3285377520;
return this;
};
function c(e) {
return e << 30 | e >>> 2;
}
function l(e, t, i, n) {
return 0 === e ? t & i | ~t & n : 2 === e ? t & i | t & n | i & n : t ^ i ^ n;
}
s.prototype._update = function(e) {
for (var t, i = this._w, n = 0 | this._a, r = 0 | this._b, a = 0 | this._c, s = 0 | this._d, h = 0 | this._e, u = 0; u < 16; ++u) i[u] = e.readInt32BE(4 * u);
for (;u < 80; ++u) i[u] = i[u - 3] ^ i[u - 8] ^ i[u - 14] ^ i[u - 16];
for (var f = 0; f < 80; ++f) {
var d = ~~(f / 20), p = ((t = n) << 5 | t >>> 27) + l(d, r, a, s) + h + i[f] + o[d] | 0;
h = s;
s = a;
a = c(r);
r = n;
n = p;
}
this._a = n + this._a | 0;
this._b = r + this._b | 0;
this._c = a + this._c | 0;
this._d = s + this._d | 0;
this._e = h + this._e | 0;
};
s.prototype._hash = function() {
var e = r.allocUnsafe(20);
e.writeInt32BE(0 | this._a, 0);
e.writeInt32BE(0 | this._b, 4);
e.writeInt32BE(0 | this._c, 8);
e.writeInt32BE(0 | this._d, 12);
e.writeInt32BE(0 | this._e, 16);
return e;
};
t.exports = s;
}, {
"./hash": 184,
inherits: 138,
"safe-buffer": 182
} ],
187: [ function(e, t) {
var i = e("inherits"), n = e("./hash"), r = e("safe-buffer").Buffer, o = [ 1518500249, 1859775393, -1894007588, -899497514 ], a = new Array(80);
function s() {
this.init();
this._w = a;
n.call(this, 64, 56);
}
i(s, n);
s.prototype.init = function() {
this._a = 1732584193;
this._b = 4023233417;
this._c = 2562383102;
this._d = 271733878;
this._e = 3285377520;
return this;
};
function c(e) {
return e << 5 | e >>> 27;
}
function l(e) {
return e << 30 | e >>> 2;
}
function h(e, t, i, n) {
return 0 === e ? t & i | ~t & n : 2 === e ? t & i | t & n | i & n : t ^ i ^ n;
}
s.prototype._update = function(e) {
for (var t, i = this._w, n = 0 | this._a, r = 0 | this._b, a = 0 | this._c, s = 0 | this._d, u = 0 | this._e, f = 0; f < 16; ++f) i[f] = e.readInt32BE(4 * f);
for (;f < 80; ++f) i[f] = (t = i[f - 3] ^ i[f - 8] ^ i[f - 14] ^ i[f - 16]) << 1 | t >>> 31;
for (var d = 0; d < 80; ++d) {
var p = ~~(d / 20), m = c(n) + h(p, r, a, s) + u + i[d] + o[p] | 0;
u = s;
s = a;
a = l(r);
r = n;
n = m;
}
this._a = n + this._a | 0;
this._b = r + this._b | 0;
this._c = a + this._c | 0;
this._d = s + this._d | 0;
this._e = u + this._e | 0;
};
s.prototype._hash = function() {
var e = r.allocUnsafe(20);
e.writeInt32BE(0 | this._a, 0);
e.writeInt32BE(0 | this._b, 4);
e.writeInt32BE(0 | this._c, 8);
e.writeInt32BE(0 | this._d, 12);
e.writeInt32BE(0 | this._e, 16);
return e;
};
t.exports = s;
}, {
"./hash": 184,
inherits: 138,
"safe-buffer": 182
} ],
188: [ function(e, t) {
var i = e("inherits"), n = e("./sha256"), r = e("./hash"), o = e("safe-buffer").Buffer, a = new Array(64);
function s() {
this.init();
this._w = a;
r.call(this, 64, 56);
}
i(s, n);
s.prototype.init = function() {
this._a = 3238371032;
this._b = 914150663;
this._c = 812702999;
this._d = 4144912697;
this._e = 4290775857;
this._f = 1750603025;
this._g = 1694076839;
this._h = 3204075428;
return this;
};
s.prototype._hash = function() {
var e = o.allocUnsafe(28);
e.writeInt32BE(this._a, 0);
e.writeInt32BE(this._b, 4);
e.writeInt32BE(this._c, 8);
e.writeInt32BE(this._d, 12);
e.writeInt32BE(this._e, 16);
e.writeInt32BE(this._f, 20);
e.writeInt32BE(this._g, 24);
return e;
};
t.exports = s;
}, {
"./hash": 184,
"./sha256": 189,
inherits: 138,
"safe-buffer": 182
} ],
189: [ function(e, t) {
var i = e("inherits"), n = e("./hash"), r = e("safe-buffer").Buffer, o = [ 1116352408, 1899447441, 3049323471, 3921009573, 961987163, 1508970993, 2453635748, 2870763221, 3624381080, 310598401, 607225278, 1426881987, 1925078388, 2162078206, 2614888103, 3248222580, 3835390401, 4022224774, 264347078, 604807628, 770255983, 1249150122, 1555081692, 1996064986, 2554220882, 2821834349, 2952996808, 3210313671, 3336571891, 3584528711, 113926993, 338241895, 666307205, 773529912, 1294757372, 1396182291, 1695183700, 1986661051, 2177026350, 2456956037, 2730485921, 2820302411, 3259730800, 3345764771, 3516065817, 3600352804, 4094571909, 275423344, 430227734, 506948616, 659060556, 883997877, 958139571, 1322822218, 1537002063, 1747873779, 1955562222, 2024104815, 2227730452, 2361852424, 2428436474, 2756734187, 3204031479, 3329325298 ], a = new Array(64);
function s() {
this.init();
this._w = a;
n.call(this, 64, 56);
}
i(s, n);
s.prototype.init = function() {
this._a = 1779033703;
this._b = 3144134277;
this._c = 1013904242;
this._d = 2773480762;
this._e = 1359893119;
this._f = 2600822924;
this._g = 528734635;
this._h = 1541459225;
return this;
};
function c(e, t, i) {
return i ^ e & (t ^ i);
}
function l(e, t, i) {
return e & t | i & (e | t);
}
function h(e) {
return (e >>> 2 | e << 30) ^ (e >>> 13 | e << 19) ^ (e >>> 22 | e << 10);
}
function u(e) {
return (e >>> 6 | e << 26) ^ (e >>> 11 | e << 21) ^ (e >>> 25 | e << 7);
}
function f(e) {
return (e >>> 7 | e << 25) ^ (e >>> 18 | e << 14) ^ e >>> 3;
}
s.prototype._update = function(e) {
for (var t, i = this._w, n = 0 | this._a, r = 0 | this._b, a = 0 | this._c, s = 0 | this._d, d = 0 | this._e, p = 0 | this._f, m = 0 | this._g, g = 0 | this._h, y = 0; y < 16; ++y) i[y] = e.readInt32BE(4 * y);
for (;y < 64; ++y) i[y] = (((t = i[y - 2]) >>> 17 | t << 15) ^ (t >>> 19 | t << 13) ^ t >>> 10) + i[y - 7] + f(i[y - 15]) + i[y - 16] | 0;
for (var b = 0; b < 64; ++b) {
var v = g + u(d) + c(d, p, m) + o[b] + i[b] | 0, _ = h(n) + l(n, r, a) | 0;
g = m;
m = p;
p = d;
d = s + v | 0;
s = a;
a = r;
r = n;
n = v + _ | 0;
}
this._a = n + this._a | 0;
this._b = r + this._b | 0;
this._c = a + this._c | 0;
this._d = s + this._d | 0;
this._e = d + this._e | 0;
this._f = p + this._f | 0;
this._g = m + this._g | 0;
this._h = g + this._h | 0;
};
s.prototype._hash = function() {
var e = r.allocUnsafe(32);
e.writeInt32BE(this._a, 0);
e.writeInt32BE(this._b, 4);
e.writeInt32BE(this._c, 8);
e.writeInt32BE(this._d, 12);
e.writeInt32BE(this._e, 16);
e.writeInt32BE(this._f, 20);
e.writeInt32BE(this._g, 24);
e.writeInt32BE(this._h, 28);
return e;
};
t.exports = s;
}, {
"./hash": 184,
inherits: 138,
"safe-buffer": 182
} ],
190: [ function(e, t) {
var i = e("inherits"), n = e("./sha512"), r = e("./hash"), o = e("safe-buffer").Buffer, a = new Array(160);
function s() {
this.init();
this._w = a;
r.call(this, 128, 112);
}
i(s, n);
s.prototype.init = function() {
this._ah = 3418070365;
this._bh = 1654270250;
this._ch = 2438529370;
this._dh = 355462360;
this._eh = 1731405415;
this._fh = 2394180231;
this._gh = 3675008525;
this._hh = 1203062813;
this._al = 3238371032;
this._bl = 914150663;
this._cl = 812702999;
this._dl = 4144912697;
this._el = 4290775857;
this._fl = 1750603025;
this._gl = 1694076839;
this._hl = 3204075428;
return this;
};
s.prototype._hash = function() {
var e = o.allocUnsafe(48);
function t(t, i, n) {
e.writeInt32BE(t, n);
e.writeInt32BE(i, n + 4);
}
t(this._ah, this._al, 0);
t(this._bh, this._bl, 8);
t(this._ch, this._cl, 16);
t(this._dh, this._dl, 24);
t(this._eh, this._el, 32);
t(this._fh, this._fl, 40);
return e;
};
t.exports = s;
}, {
"./hash": 184,
"./sha512": 191,
inherits: 138,
"safe-buffer": 182
} ],
191: [ function(e, t) {
var i = e("inherits"), n = e("./hash"), r = e("safe-buffer").Buffer, o = [ 1116352408, 3609767458, 1899447441, 602891725, 3049323471, 3964484399, 3921009573, 2173295548, 961987163, 4081628472, 1508970993, 3053834265, 2453635748, 2937671579, 2870763221, 3664609560, 3624381080, 2734883394, 310598401, 1164996542, 607225278, 1323610764, 1426881987, 3590304994, 1925078388, 4068182383, 2162078206, 991336113, 2614888103, 633803317, 3248222580, 3479774868, 3835390401, 2666613458, 4022224774, 944711139, 264347078, 2341262773, 604807628, 2007800933, 770255983, 1495990901, 1249150122, 1856431235, 1555081692, 3175218132, 1996064986, 2198950837, 2554220882, 3999719339, 2821834349, 766784016, 2952996808, 2566594879, 3210313671, 3203337956, 3336571891, 1034457026, 3584528711, 2466948901, 113926993, 3758326383, 338241895, 168717936, 666307205, 1188179964, 773529912, 1546045734, 1294757372, 1522805485, 1396182291, 2643833823, 1695183700, 2343527390, 1986661051, 1014477480, 2177026350, 1206759142, 2456956037, 344077627, 2730485921, 1290863460, 2820302411, 3158454273, 3259730800, 3505952657, 3345764771, 106217008, 3516065817, 3606008344, 3600352804, 1432725776, 4094571909, 1467031594, 275423344, 851169720, 430227734, 3100823752, 506948616, 1363258195, 659060556, 3750685593, 883997877, 3785050280, 958139571, 3318307427, 1322822218, 3812723403, 1537002063, 2003034995, 1747873779, 3602036899, 1955562222, 1575990012, 2024104815, 1125592928, 2227730452, 2716904306, 2361852424, 442776044, 2428436474, 593698344, 2756734187, 3733110249, 3204031479, 2999351573, 3329325298, 3815920427, 3391569614, 3928383900, 3515267271, 566280711, 3940187606, 3454069534, 4118630271, 4000239992, 116418474, 1914138554, 174292421, 2731055270, 289380356, 3203993006, 460393269, 320620315, 685471733, 587496836, 852142971, 1086792851, 1017036298, 365543100, 1126000580, 2618297676, 1288033470, 3409855158, 1501505948, 4234509866, 1607167915, 987167468, 1816402316, 1246189591 ], a = new Array(160);
function s() {
this.init();
this._w = a;
n.call(this, 128, 112);
}
i(s, n);
s.prototype.init = function() {
this._ah = 1779033703;
this._bh = 3144134277;
this._ch = 1013904242;
this._dh = 2773480762;
this._eh = 1359893119;
this._fh = 2600822924;
this._gh = 528734635;
this._hh = 1541459225;
this._al = 4089235720;
this._bl = 2227873595;
this._cl = 4271175723;
this._dl = 1595750129;
this._el = 2917565137;
this._fl = 725511199;
this._gl = 4215389547;
this._hl = 327033209;
return this;
};
function c(e, t, i) {
return i ^ e & (t ^ i);
}
function l(e, t, i) {
return e & t | i & (e | t);
}
function h(e, t) {
return (e >>> 28 | t << 4) ^ (t >>> 2 | e << 30) ^ (t >>> 7 | e << 25);
}
function u(e, t) {
return (e >>> 14 | t << 18) ^ (e >>> 18 | t << 14) ^ (t >>> 9 | e << 23);
}
function f(e, t) {
return (e >>> 1 | t << 31) ^ (e >>> 8 | t << 24) ^ e >>> 7;
}
function d(e, t) {
return (e >>> 1 | t << 31) ^ (e >>> 8 | t << 24) ^ (e >>> 7 | t << 25);
}
function p(e, t) {
return (e >>> 19 | t << 13) ^ (t >>> 29 | e << 3) ^ e >>> 6;
}
function m(e, t) {
return (e >>> 19 | t << 13) ^ (t >>> 29 | e << 3) ^ (e >>> 6 | t << 26);
}
function g(e, t) {
return e >>> 0 < t >>> 0 ? 1 : 0;
}
s.prototype._update = function(e) {
for (var t = this._w, i = 0 | this._ah, n = 0 | this._bh, r = 0 | this._ch, a = 0 | this._dh, s = 0 | this._eh, y = 0 | this._fh, b = 0 | this._gh, v = 0 | this._hh, _ = 0 | this._al, w = 0 | this._bl, C = 0 | this._cl, S = 0 | this._dl, A = 0 | this._el, M = 0 | this._fl, B = 0 | this._gl, x = 0 | this._hl, k = 0; k < 32; k += 2) {
t[k] = e.readInt32BE(4 * k);
t[k + 1] = e.readInt32BE(4 * k + 4);
}
for (;k < 160; k += 2) {
var T = t[k - 30], D = t[k - 30 + 1], E = f(T, D), L = d(D, T), P = p(T = t[k - 4], D = t[k - 4 + 1]), R = m(D, T), I = t[k - 14], N = t[k - 14 + 1], O = t[k - 32], U = t[k - 32 + 1], j = L + N | 0, F = E + I + g(j, L) | 0;
F = (F = F + P + g(j = j + R | 0, R) | 0) + O + g(j = j + U | 0, U) | 0;
t[k] = F;
t[k + 1] = j;
}
for (var G = 0; G < 160; G += 2) {
F = t[G];
j = t[G + 1];
var V = l(i, n, r), z = l(_, w, C), H = h(i, _), W = h(_, i), q = u(s, A), X = u(A, s), K = o[G], J = o[G + 1], Y = c(s, y, b), Z = c(A, M, B), Q = x + X | 0, $ = v + q + g(Q, x) | 0;
$ = ($ = ($ = $ + Y + g(Q = Q + Z | 0, Z) | 0) + K + g(Q = Q + J | 0, J) | 0) + F + g(Q = Q + j | 0, j) | 0;
var ee = W + z | 0, te = H + V + g(ee, W) | 0;
v = b;
x = B;
b = y;
B = M;
y = s;
M = A;
s = a + $ + g(A = S + Q | 0, S) | 0;
a = r;
S = C;
r = n;
C = w;
n = i;
w = _;
i = $ + te + g(_ = Q + ee | 0, Q) | 0;
}
this._al = this._al + _ | 0;
this._bl = this._bl + w | 0;
this._cl = this._cl + C | 0;
this._dl = this._dl + S | 0;
this._el = this._el + A | 0;
this._fl = this._fl + M | 0;
this._gl = this._gl + B | 0;
this._hl = this._hl + x | 0;
this._ah = this._ah + i + g(this._al, _) | 0;
this._bh = this._bh + n + g(this._bl, w) | 0;
this._ch = this._ch + r + g(this._cl, C) | 0;
this._dh = this._dh + a + g(this._dl, S) | 0;
this._eh = this._eh + s + g(this._el, A) | 0;
this._fh = this._fh + y + g(this._fl, M) | 0;
this._gh = this._gh + b + g(this._gl, B) | 0;
this._hh = this._hh + v + g(this._hl, x) | 0;
};
s.prototype._hash = function() {
var e = r.allocUnsafe(64);
function t(t, i, n) {
e.writeInt32BE(t, n);
e.writeInt32BE(i, n + 4);
}
t(this._ah, this._al, 0);
t(this._bh, this._bl, 8);
t(this._ch, this._cl, 16);
t(this._dh, this._dl, 24);
t(this._eh, this._el, 32);
t(this._fh, this._fl, 40);
t(this._gh, this._gl, 48);
t(this._hh, this._hl, 56);
return e;
};
t.exports = s;
}, {
"./hash": 184,
inherits: 138,
"safe-buffer": 182
} ],
192: [ function(e, t) {
t.exports = n;
var i = e("events").EventEmitter;
e("inherits")(n, i);
n.Readable = e("readable-stream/readable.js");
n.Writable = e("readable-stream/writable.js");
n.Duplex = e("readable-stream/duplex.js");
n.Transform = e("readable-stream/transform.js");
n.PassThrough = e("readable-stream/passthrough.js");
n.Stream = n;
function n() {
i.call(this);
}
n.prototype.pipe = function(e, t) {
var n = this;
function r(t) {
e.writable && !1 === e.write(t) && n.pause && n.pause();
}
n.on("data", r);
function o() {
n.readable && n.resume && n.resume();
}
e.on("drain", o);
if (!(e._isStdio || t && !1 === t.end)) {
n.on("end", s);
n.on("close", c);
}
var a = !1;
function s() {
if (!a) {
a = !0;
e.end();
}
}
function c() {
if (!a) {
a = !0;
"function" == typeof e.destroy && e.destroy();
}
}
function l(e) {
h();
if (0 === i.listenerCount(this, "error")) throw e;
}
n.on("error", l);
e.on("error", l);
function h() {
n.removeListener("data", r);
e.removeListener("drain", o);
n.removeListener("end", s);
n.removeListener("close", c);
n.removeListener("error", l);
e.removeListener("error", l);
n.removeListener("end", h);
n.removeListener("close", h);
e.removeListener("close", h);
}
n.on("end", h);
n.on("close", h);
e.on("close", h);
e.emit("pipe", n);
return e;
};
}, {
events: 104,
inherits: 138,
"readable-stream/duplex.js": 166,
"readable-stream/passthrough.js": 177,
"readable-stream/readable.js": 178,
"readable-stream/transform.js": 179,
"readable-stream/writable.js": 180
} ],
193: [ function(e, t, i) {
var n = e("buffer").Buffer, r = n.isEncoding || function(e) {
switch (e && e.toLowerCase()) {
case "hex":
case "utf8":
case "utf-8":
case "ascii":
case "binary":
case "base64":
case "ucs2":
case "ucs-2":
case "utf16le":
case "utf-16le":
case "raw":
return !0;

default:
return !1;
}
};
function o(e) {
if (e && !r(e)) throw new Error("Unknown encoding: " + e);
}
var a = i.StringDecoder = function(e) {
this.encoding = (e || "utf8").toLowerCase().replace(/[-_]/, "");
o(e);
switch (this.encoding) {
case "utf8":
this.surrogateSize = 3;
break;

case "ucs2":
case "utf16le":
this.surrogateSize = 2;
this.detectIncompleteChar = c;
break;

case "base64":
this.surrogateSize = 3;
this.detectIncompleteChar = l;
break;

default:
this.write = s;
return;
}
this.charBuffer = new n(6);
this.charReceived = 0;
this.charLength = 0;
};
a.prototype.write = function(e) {
for (var t = ""; this.charLength; ) {
var i = e.length >= this.charLength - this.charReceived ? this.charLength - this.charReceived : e.length;
e.copy(this.charBuffer, this.charReceived, 0, i);
this.charReceived += i;
if (this.charReceived < this.charLength) return "";
e = e.slice(i, e.length);
if (!((n = (t = this.charBuffer.slice(0, this.charLength).toString(this.encoding)).charCodeAt(t.length - 1)) >= 55296 && n <= 56319)) {
this.charReceived = this.charLength = 0;
if (0 === e.length) return t;
break;
}
this.charLength += this.surrogateSize;
t = "";
}
this.detectIncompleteChar(e);
var n, r = e.length;
if (this.charLength) {
e.copy(this.charBuffer, 0, e.length - this.charReceived, r);
r -= this.charReceived;
}
r = (t += e.toString(this.encoding, 0, r)).length - 1;
if ((n = t.charCodeAt(r)) >= 55296 && n <= 56319) {
var o = this.surrogateSize;
this.charLength += o;
this.charReceived += o;
this.charBuffer.copy(this.charBuffer, o, 0, o);
e.copy(this.charBuffer, 0, 0, o);
return t.substring(0, r);
}
return t;
};
a.prototype.detectIncompleteChar = function(e) {
for (var t = e.length >= 3 ? 3 : e.length; t > 0; t--) {
var i = e[e.length - t];
if (1 == t && i >> 5 == 6) {
this.charLength = 2;
break;
}
if (t <= 2 && i >> 4 == 14) {
this.charLength = 3;
break;
}
if (t <= 3 && i >> 3 == 30) {
this.charLength = 4;
break;
}
}
this.charReceived = t;
};
a.prototype.end = function(e) {
var t = "";
e && e.length && (t = this.write(e));
if (this.charReceived) {
var i = this.charReceived, n = this.charBuffer, r = this.encoding;
t += n.slice(0, i).toString(r);
}
return t;
};
function s(e) {
return e.toString(this.encoding);
}
function c(e) {
this.charReceived = e.length % 2;
this.charLength = this.charReceived ? 2 : 0;
}
function l(e) {
this.charReceived = e.length % 3;
this.charLength = this.charReceived ? 3 : 0;
}
}, {
buffer: 65
} ],
194: [ function(e, t) {
(function(e) {
t.exports = function(e, t) {
if (i("noDeprecation")) return e;
var n = !1;
return function() {
if (!n) {
if (i("throwDeprecation")) throw new Error(t);
i("traceDeprecation") ? console.trace(t) : console.warn(t);
n = !0;
}
return e.apply(this, arguments);
};
};
function i(t) {
try {
if (!e.localStorage) return !1;
} catch (e) {
return !1;
}
var i = e.localStorage[t];
return null != i && "true" === String(i).toLowerCase();
}
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {} ],
AnalyticsUtilities: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "0c725qUcwxF0JReYR3VnDwb", "AnalyticsUtilities");
var n = this && this.__awaiter || function(e, t, i, n) {
return new (i || (i = Promise))(function(r, o) {
function a(e) {
try {
c(n.next(e));
} catch (e) {
o(e);
}
}
function s(e) {
try {
c(n.throw(e));
} catch (e) {
o(e);
}
}
function c(e) {
e.done ? r(e.value) : (t = e.value, t instanceof i ? t : new i(function(e) {
e(t);
})).then(a, s);
var t;
}
c((n = n.apply(e, t || [])).next());
});
}, r = this && this.__generator || function(e, t) {
var i, n, r, o, a = {
label: 0,
sent: function() {
if (1 & r[0]) throw r[1];
return r[1];
},
trys: [],
ops: []
};
return o = {
next: s(0),
throw: s(1),
return: s(2)
}, "function" == typeof Symbol && (o[Symbol.iterator] = function() {
return this;
}), o;
function s(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(o) {
if (i) throw new TypeError("Generator is already executing.");
for (;a; ) try {
if (i = 1, n && (r = 2 & o[0] ? n.return : o[0] ? n.throw || ((r = n.return) && r.call(n), 
0) : n.next) && !(r = r.call(n, o[1])).done) return r;
(n = 0, r) && (o = [ 2 & o[0], r.value ]);
switch (o[0]) {
case 0:
case 1:
r = o;
break;

case 4:
a.label++;
return {
value: o[1],
done: !1
};

case 5:
a.label++;
n = o[1];
o = [ 0 ];
continue;

case 7:
o = a.ops.pop();
a.trys.pop();
continue;

default:
if (!(r = a.trys, r = r.length > 0 && r[r.length - 1]) && (6 === o[0] || 2 === o[0])) {
a = 0;
continue;
}
if (3 === o[0] && (!r || o[1] > r[0] && o[1] < r[3])) {
a.label = o[1];
break;
}
if (6 === o[0] && a.label < r[1]) {
a.label = r[1];
r = o;
break;
}
if (r && a.label < r[2]) {
a.label = r[2];
a.ops.push(o);
break;
}
r[2] && a.ops.pop();
a.trys.pop();
continue;
}
o = t.call(e, a);
} catch (e) {
o = [ 6, e ];
n = 0;
} finally {
i = r = 0;
}
if (5 & o[0]) throw o[1];
return {
value: o[0] ? o[1] : void 0,
done: !0
};
}
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var o = function() {
function e() {}
e.logEvent = function(e, t) {
void 0 === t && (t = null);
return n(this, void 0, void 0, function() {
var i;
return r(this, function() {
i = null;
if ("undefined" != typeof gmbox && gmbox.logEvent) {
null != t ? t.event = e : t = {
event: e
};
gmbox.logEvent(t);
i = "gmbox";
}
if ("undefined" != typeof FBInstant && FBInstant.logEvent) {
FBInstant.logEvent(e, 1, t);
i = "FB";
}
if ("undefined" != typeof wx && wx.aldSendEvent) {
wx.aldSendEvent(e, t);
i = "wx";
}
console.info("打点" + i + ": event = " + e + ", bundle = " + JSON.stringify(t));
return [ 2 ];
});
});
};
return e;
}();
i.default = o;
cc._RF.pop();
}, {} ],
AudioController: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "7a669O++lRBF5n0W9Yyz+qn", "AudioController");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("../i18n/i18nMgr"), s = cc._decorator, c = s.ccclass, l = s.property, h = (window.wx, 
function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.openAudio = null;
t.openAudioClick = null;
t.closeAudio = null;
t.closeAudioClick = null;
t.audioClickButton = null;
t.audioCountDown = null;
t.audioGetCoins = null;
t.audioGamePass = null;
t.audioArrowNotify = null;
t.audioMove = null;
t.audioCoinClick = null;
t.audioDialogShow = null;
t.audioMiao = null;
t.startAudio = null;
t.moveEditAudio = null;
t.gameStartAudio = null;
t.bgAudio = [];
t.helpAudio = [];
t.endAudio = [];
t.adNode = null;
t.isOpenAudio = !0;
return t;
}
t.prototype.onLoad = function() {
var e = cc.sys.localStorage.getItem("is_audio_open");
if (null == e || this.isNull(e)) {
e = "1";
cc.sys.localStorage.setItem("is_audio_open", e);
}
1 == Number(e) ? this.isOpenAudio = !0 : this.isOpenAudio = !1;
};
t.prototype.isNull = function(e) {
return "undefined" == typeof e || "" == e;
};
t.prototype.playCountDownAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioCountDown, !1, 1);
};
t.prototype.playGetCoinsAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioGetCoins, !1, 1);
};
t.prototype.playClickButtonAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioClickButton, !1, 1);
};
t.prototype.playGamePassAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioGamePass, !1, 1);
};
t.prototype.playArrowNotifyAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioArrowNotify, !1, 1);
};
t.prototype.playMoveAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioMove, !1, 1);
};
t.prototype.playCoinClickAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioCoinClick, !1, 1);
};
t.prototype.playDialogShowAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.audioDialogShow, !1, 1);
};
t.prototype.playCatAudio = function() {
this.isOpenAudio;
};
t.prototype.playStartAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.startAudio, !1, 1);
};
t.prototype.playMoveEditAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.moveEditAudio, !1, 1);
};
t.prototype.playGameStartAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.gameStartAudio, !1, 1);
};
t.prototype.playGameHelpAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.helpAudio["pt" == a.i18nMgr.getLanguage() ? 1 : 0], !1, 1);
};
t.prototype.playGameEndAudio = function() {
this.isOpenAudio && cc.audioEngine.play(this.endAudio["pt" == a.i18nMgr.getLanguage() ? 1 : 0], !1, 1);
};
t.prototype.playBgMusic = function(e) {
cc.log(e, "id");
this.isOpenAudio && cc.audioEngine.playMusic(this.bgAudio[e], !0);
};
t.prototype.stopMusic = function() {
cc.audioEngine.stopMusic();
};
t.prototype.soundBtnClick = function() {
this.playClickButtonAudio();
if (this.isOpenAudio) {
this.isOpenAudio = !1;
this.adNode.getComponent(cc.Sprite).spriteFrame = this.closeAudio;
} else {
this.isOpenAudio = !0;
this.adNode.getComponent(cc.Sprite).spriteFrame = this.openAudio;
}
cc.sys.localStorage.setItem("is_audio_open", this.isOpenAudio ? 1 : 0);
};
t.prototype.showUI = function() {
this.isOpenAudio ? this.adNode.getComponent(cc.Sprite).spriteFrame = this.openAudio : this.adNode.getComponent(cc.Sprite).spriteFrame = this.closeAudio;
};
t.prototype.start = function() {};
o([ l(cc.SpriteFrame) ], t.prototype, "openAudio", void 0);
o([ l(cc.SpriteFrame) ], t.prototype, "openAudioClick", void 0);
o([ l(cc.SpriteFrame) ], t.prototype, "closeAudio", void 0);
o([ l(cc.SpriteFrame) ], t.prototype, "closeAudioClick", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioClickButton", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioCountDown", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioGetCoins", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioGamePass", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioArrowNotify", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioMove", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioCoinClick", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioDialogShow", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "audioMiao", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "startAudio", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "moveEditAudio", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "gameStartAudio", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "bgAudio", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "helpAudio", void 0);
o([ l({
type: cc.AudioClip
}) ], t.prototype, "endAudio", void 0);
o([ l({
type: cc.Node
}) ], t.prototype, "adNode", void 0);
return o([ c ], t);
}(cc.Component));
i.default = h;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr"
} ],
AwardCoin: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "0ba09gihZtPoqPeizx+8mQQ", "AwardCoin");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.awardControl = null;
t.pickAnim = null;
t.coinBg = null;
t.moneyLabel = null;
return t;
}
t.prototype.onLoad = function() {
this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchEvent, this);
};
t.prototype.start = function() {};
t.prototype.setAwardControl = function(e) {
this.awardControl = e;
};
t.prototype.onTouchEvent = function() {
if (!this.isClicked()) {
var e = this.getRandom(1, 3);
this.awardControl.addScore(e);
this.moneyLabel.string = "+" + e;
this.coinBg.opacity = 0;
this.pickAnim.active = !0;
cc.find("Canvas").getComponent("MainControl").getVolumeControl().playCoinClickAudio();
this.labelAnim();
this.pickAnim.getComponent(dragonBones.ArmatureDisplay).playAnimation("newAnimation", 1);
this.pickAnim.getComponent(dragonBones.ArmatureDisplay).addEventListener(dragonBones.EventObject.COMPLETE, this.animEventHandler, this);
}
};
t.prototype.animEventHandler = function(e) {
if (e.type === dragonBones.EventObject.COMPLETE) {
this.node.active = !1;
this.coinBg.opacity = 255;
this.pickAnim.active = !1;
}
};
t.prototype.isClicked = function() {
return 0 == this.coinBg.opacity;
};
t.prototype.labelAnim = function() {
this.moneyLabel.node.opacity = 255;
this.moneyLabel.node.y = 0;
var e = cc.spawn(cc.moveTo(1, cc.v2(this.moneyLabel.node.x, 20)), cc.fadeOut(1));
this.moneyLabel.node.runAction(e);
};
t.prototype.getRandom = function(e, t) {
return Math.floor(Math.random() * (t - e + 1) + e);
};
o([ c(cc.Node) ], t.prototype, "pickAnim", void 0);
o([ c(cc.Node) ], t.prototype, "coinBg", void 0);
o([ c(cc.Label) ], t.prototype, "moneyLabel", void 0);
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {} ],
AwardViewControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "5b963/T6idIUYhs5LWr+RIC", "AwardViewControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./GlobalConst"), h = e("./SettleUtil"), u = e("./Common/StorageUtil"), f = e("./Bean/UserData"), d = e("./Common/WXCommon"), p = window.wx, m = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.countDownAnim = null;
t.countDownTime = null;
t.coinsView = null;
t.settleDialog = null;
t.gainedCoinsLabel = null;
t.gameView = null;
t.startBtn = null;
t.startView = null;
t.mainView = null;
t.coinTargetNode = null;
t.doubleGainToggle = null;
t.notifyLabel = null;
t.countDownPlayView = null;
t.checkBoxNode = null;
t.gainCoinBtn = null;
t.speed = 600;
t.coinsArray = [];
t.globalConsts = new l.default();
t.poolMng = null;
t.settleUtil = new h.default();
t.startCountDown = !1;
t.costTime = 0;
t.score = 0;
t.coinsNum = 60;
t.storageUtil = new u.default();
t.doubleCheckBoxChecked = !0;
t.wxCommon = new d.default();
t.scheduleId = 0;
t.isHide = !1;
return t;
}
t.prototype.onLoad = function() {
this.poolMng = this.node.getComponent("PoolMng");
this.poolMng.init();
cc.game.on(cc.game.EVENT_HIDE, function() {
this.isHide = !0;
}.bind(this));
cc.game.on(cc.game.EVENT_SHOW, function() {
this.isHide = !1;
}.bind(this));
};
t.prototype.showStartView = function() {
p.aldSendEvent("闯关赛奖励引导界面曝光");
this.node.zIndex = 999;
this.node.setPosition(0, 0);
this.node.active = !0;
this.countDownPlayView.active = !0;
this.notifyLabel.active = !1;
this.countDownAnim.active = !1;
this.startView.active = !0;
this.countDownTime.string = "00:" + this.globalConsts.awardTime;
this.countDownTime.node.color = new cc.Color().fromHEX("#FFFFFF");
this.gameView.getComponent("GameControl").startCountDown = !1;
};
t.prototype.startAwardFromRedPack = function() {
this.node.zIndex = 999;
this.node.setPosition(0, 0);
this.node.active = !0;
this.countDownPlayView.active = !0;
this.node.active = !0;
this.notifyLabel.active = !1;
this.countDownAnim.active = !1;
this.startView.active = !1;
this.countDownTime.string = "00:" + this.globalConsts.awardTime;
this.countDownTime.node.color = new cc.Color().fromHEX("#FFFFFF");
this.gameView.getComponent("GameControl").startCountDown = !1;
this.playAwardGame();
};
t.prototype.playAwardGame = function() {
p.aldSendEvent("闯关赛奖励引导界面开始点击");
this.notifyLabel.active = !0;
var e = new Date().getTime();
this.storageUtil.saveData("awardTime", e);
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.node.active = !0;
this.startView.active = !1;
var t = 0;
this.schedule(function() {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playCountDownAudio();
if (0 == t) {
this.countDownAnim.active = !0;
this.countDownAnim.getComponent(dragonBones.ArmatureDisplay).playAnimation("newAnimation", 1);
this.scheduleOnce(function() {
this.score = 0;
this.countDownAnim.active = !1;
this.startCountDown = !0;
this.settleDialog.active = !1;
this.costTime = 0;
this.generateCoins();
}.bind(this), 3);
}
t++;
}, 1, 2, .01);
};
t.prototype.update = function(e) {
if (this.startCountDown) {
this.costTime = this.costTime + e;
var t = Math.ceil(this.globalConsts.awardTime - this.costTime);
if (t < 10) {
this.countDownTime.node.color = new cc.Color().fromHEX("#DD0606");
this.countDownTime.string = "00:0" + t;
} else {
this.countDownTime.node.color = new cc.Color().fromHEX("#FFFFFF");
this.countDownTime.string = "00:" + t;
}
if (t <= 0) {
this.startCountDown = !1;
this.notifyLabel.active = !1;
this.doubleGainToggle.getComponent(cc.Toggle).isChecked = !0;
this.doubleGainToggle.getComponent(cc.Toggle).check();
this.countDownPlayView.active = !1;
this.settleDialog.active = !0;
p.aldSendEvent("闯关赛奖励结算曝光", {
"获得猫币数量": this.score
});
cc.find("Canvas").getComponent("MainControl").getVolumeControl().playDialogShowAudio();
var i = this.mainView.getComponent("MainControl");
this.resetBannerPos();
i.showBanner(i.AwardBanner, function(e) {
this.moveBanner(e);
}.bind(this));
this.checkBoxNode.opacity = 0;
var n = cc.fadeIn(1), r = cc.delayTime(2);
this.checkBoxNode.runAction(cc.sequence(r, n));
this.gainedCoinsLabel.string = this.score + "";
for (;this.coinsArray.length > 0; ) this.removeCoin(this.coinsArray[0]);
clearInterval(this.scheduleId);
}
for (var o = 0; o < this.coinsArray.length; o++) if (!this.coinsArray[o].getComponent("AwardCoin").isClicked()) {
this.coinsArray[o].y = this.coinsArray[o].y - e * this.speed;
this.coinsArray[o].y < -this.coinsView.height / 2 - 20 && this.removeCoin(this.coinsArray[o]);
}
}
};
t.prototype.generateCoins = function() {
this.coinsNum;
this.scheduleId = setInterval(this.generateCoin.bind(this), 500);
};
t.prototype.generateCoin = function() {
if (this.startCountDown && !this.isHide) {
var e = this.poolMng.spawnCoin(), t = this.settleUtil.getRandom(-this.coinsView.width / 2 + e.width / 2, this.coinsView.width / 2 - e.width / 2), i = this.coinsView.height / 2 + 20;
e.active = !0;
e.x = t;
e.y = i;
e.getComponent("AwardCoin").setAwardControl(this);
this.coinsArray.push(e);
this.coinsView.addChild(e);
}
};
t.prototype.removeCoin = function(e) {
e.active = !1;
var t = this.coinsArray.indexOf(e);
this.coinsArray.splice(t, 1);
this.poolMng.despwanCoin(0, e);
this.coinsView.removeChild(e);
};
t.prototype.addScore = function(e) {
this.score = e + this.score;
};
t.prototype.onGetAwardClick = function() {
this.gainAwardCoins();
};
t.prototype.gainAwardCoins = function() {
this.mainView.getComponent("MainControl").hideBanner();
var e = new Date().getTime();
this.storageUtil.saveData("awardClick", e);
this.gameView.getComponent("GameControl").scheduleAwardBtn();
var t = this.storageUtil.getData("userData");
null == t && (t = new f.default());
this.doubleCheckBoxChecked ? t.gold += 2 * this.score : t.gold += this.score;
var i = this.coinTargetNode.parent.convertToWorldSpaceAR(this.coinTargetNode.getPosition()), n = this.node.convertToNodeSpaceAR(i);
this.storageUtil.createGoldAnim(cc.v2(0, 0), n, 150, 20, !0, this.coinTargetNode, this.mainView, this.poolMng);
this.mainView.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
this.storageUtil.saveData("userData", t);
this.settleDialog.active = !1;
this.node.active = !1;
this.gameView.getComponent("GameControl").startCountDown = !0;
this.gameView.getComponent("GameControl").updateCoinsLabel(t.gold + "");
this.doubleCheckBoxChecked = !0;
};
t.prototype.setDoubleAwardState = function(e) {
this.doubleCheckBoxChecked = e.isChecked;
};
t.prototype.resetBannerPos = function() {};
t.prototype.moveBanner = function() {};
o([ c(cc.Node) ], t.prototype, "countDownAnim", void 0);
o([ c(cc.Label) ], t.prototype, "countDownTime", void 0);
o([ c(cc.Node) ], t.prototype, "coinsView", void 0);
o([ c(cc.Node) ], t.prototype, "settleDialog", void 0);
o([ c(cc.Label) ], t.prototype, "gainedCoinsLabel", void 0);
o([ c(cc.Node) ], t.prototype, "gameView", void 0);
o([ c(cc.Node) ], t.prototype, "startBtn", void 0);
o([ c(cc.Node) ], t.prototype, "startView", void 0);
o([ c(cc.Node) ], t.prototype, "mainView", void 0);
o([ c(cc.Node) ], t.prototype, "coinTargetNode", void 0);
o([ c(cc.Node) ], t.prototype, "doubleGainToggle", void 0);
o([ c(cc.Node) ], t.prototype, "notifyLabel", void 0);
o([ c(cc.Node) ], t.prototype, "countDownPlayView", void 0);
o([ c(cc.Node) ], t.prototype, "checkBoxNode", void 0);
o([ c(cc.Node) ], t.prototype, "gainCoinBtn", void 0);
return o([ s ], t);
}(cc.Component);
i.default = m;
cc._RF.pop();
}, {
"./Bean/UserData": "UserData",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./GlobalConst": "GlobalConst",
"./SettleUtil": "SettleUtil"
} ],
BackgroundAdapter: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "23f0bz6UbRHRqMHW7wD8JXb", "BackgroundAdapter");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, function(e) {
r(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {};
t.prototype.start = function() {};
return o([ s ], t);
}(cc.Component));
i.default = c;
cc._RF.pop();
}, {} ],
BaseConfig: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "5008dLYnahB4oaaUjskDJXZ", "BaseConfig");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.BaseConfig = void 0;
var n = function() {
function e() {}
e.IsLoading = !1;
e.Debug = !1;
e.TestInfo = !1;
e.Global = {
Game_Name: "",
Game_Bind_App_List: [ "wx5b7716ae421f9b60" ],
Game_Query: {},
Game_Is_Review: !0,
gane_is_open_break: !1,
Game_Is_privacy: !1,
NetRoot: "https://cathome8.com",
NetRes: "/zjkj_h5/gongfu/gongfu_wx/res_1001/json/",
BoxMenuNetRes: "/zjkj_boxmenu/",
BoxMenuVersion: "zy_game_ml/res_lqwz_20200407/",
TgSDK_ServerUrl: "/zjserver/tgsdk/tgsdk/protocol/",
Zjrank_ServerUrl: "/zjserver/zjrank/zjrank/protocol/",
TgSDK_gameID: "wgfth",
Zjrank_AppID: "fnxqc",
Zjrank_Channel: "weixin",
Zjrank_RankName: "fnxqc_rank_weixin_20191114_1",
game_window_height_num: 640,
game_banner_is_show: !1,
Quiet: 0
};
e.currentPages = [];
e.hideType = "";
e.game_global = null;
e.WeChatGlobal = {
WxAppType: "zy",
WxAppId: "wx1cc825ee4274a726",
BannerAdUnitId: "adunit-33bf016591b22189",
RewardedVideoAdUnitId: "adunit-c8e7051beb53cef2",
InterstitialAdUnitId: "adunit-a3c388a689286227"
};
e.WxSubPackageName = "subpackage";
e.GlobalJson = {
Game_Level_Json: "game_level.json",
Game_Skin_Json: "game_skin.json",
Game_Sigin_Json: "game_sigin.json"
};
e.GameResMusics = {
Sound_btn: "normal_btn_click",
Sound_main_bgm: "bgm",
Sound_props: "props",
downTimes: "downTimes",
rise: "rise",
No1: "No1",
timeSd: "timeSd",
wait: "wait",
Sound_right_answer: "right_answer",
Sound_wrong_answer: "wrong_answer",
throwEgg: "throwEgg",
excludeError: "excludeError",
match: "match",
loss: "loss"
};
e.IndexTextData = {
ShareText1: "没有人比我更懂",
ShareText2: "俏丽哇",
ShareText3: "埋汰王轻松到手",
ShareText4: "头好痒要长脑子了",
ShareText5: "你是什么垃圾？",
ShareText6: "我们简直就是萌妹的代表作",
ShareText7: "我不李姐",
ShareText8: "我蚌埠住了",
ShareText9: "盖亚！",
ShareText10: "你是对的猪",
ShareText11: "不愧是我",
ShareText12: "你有freestyle吗？",
ShareText13: "大聪明",
ShareText14: "阿瑟请坐",
ShareText15: "秀一波操作",
ShareText16: "冲鸭！",
ShareText17: "小趴菜",
ShareText18: "大肠包小肠",
ShareText19: "鼠鼠我呀",
ShareText20: "我可能上了假大学",
ShareText21: "脑子已被格式化",
ShareText22: "我没惹你们任何人",
ShareText23: "乌拉！",
ShareText24: "一整个懵住",
ShareText25: "请开始你的表演",
ShareText26: "会答题你就多答点",
ShareText27: "咱就是说"
};
e.ShareTextData = {
ShareText1: "分享答题",
ShareText2: "分享答题",
ShareText3: "分享答题"
};
e.ShareImgData = {
ShareImg1: "wx_share_1.png",
ShareImg2: "wx_share_1.png",
ShareImg3: "wx_share_1.png"
};
e.TmplIds = {
TmplId1: "VC0_fHOW3wchZdJDKLsi9gvUgWVsuZ9xvB7O4iG3-_k",
TmplId3: "9BXaBXm-XNXv5YcVe60BQzdLj4BxZPu86W4MGq729iQ",
TmplId6: "VMn0LEmmV1E3owX_vINPHX20L6eRXSCCT5Y8k-dRV34",
TmplId2: "m2TBkD9iEqrecgvv8QMlrWY_kgAx9q2NZLjLeykwGn0",
TmplId4: "_Yy5gFSUjWLH1gdEmLhNTrd7JIC6UKjgBFgEJdQct2I",
TmplId5: "9BXaBXm-XNXv5YcVe60BQ8wPb7hkb5-J_48KM_0s28Q"
};
e.MenuRes = {
login: "prefab/loginPrefab/login",
userInfo: "prefab/loginPrefab/userInfo",
mainMenu: "prefab/mainMenu/mainMenu",
planFight: "prefab/mainMenu/planFight",
planFight3Result: "prefab/mainMenu/planFight3Result",
ranking: "prefab/mainMenu/ranking",
shareNode: "prefab/mainMenu/shareNode",
classify: "prefab/mainMenu/classify",
classLv: "prefab/mainMenu/classLv",
classLvCompe: "prefab/mainMenu/classLvCompe",
classRank: "prefab/mainMenu/classRank",
classifyCompe: "prefab/mainMenu/classifyCompe",
review: "prefab/mainMenu/review",
friendPk: "prefab/mainMenu/friendPk",
friendPkResult: "prefab/mainMenu/friendpkResult",
friendpkWx: "prefab/mainMenu/friendpkWx",
mine: "prefab/mainMenu/mine",
mineCate: "prefab/mainMenu/mineCate",
topic: "prefab/mainMenu/topic",
priagree: "prefab/mainMenu/priagree",
webPage: "prefab/mainMenu/webPage",
photoSelect: "prefab/mainMenu/photoSelect",
fightSelect: "prefab/mainMenu/fightSelect",
userVideo: "prefab/mainMenu/userVideo",
netError: "prefab/mainMenu/netError",
matching: "prefab/mainMenu/matching",
topticList: "prefab/mainMenu/topticList2",
topticUnload: "prefab/mainMenu/topticUnload",
topicRank: "prefab/mainMenu/topicRank",
topicStar: "prefab/mainMenu/topicStar",
topicMine: "prefab/mainMenu/topicMine",
topicSearch: "prefab/mainMenu/topicSearch",
fightTip: "prefab/mainMenu/fightTip",
honor: "prefab/mainMenu/honor",
videoCenter: "prefab/mainMenu/videoCenter",
rewardRes: "prefab/component/rewardRes",
rewardType: "prefab/component/rewardType",
lackCoin: "prefab/component/lackCoin",
chooseReward: "prefab/component/chooseReward",
ageTips: "prefab/component/ageTips",
propStore: "prefab/mainMenu/propStore",
loginReward: "prefab/mainMenu/loginReward",
welfare: "prefab/mainMenu/welfare",
setting: "prefab/mainMenu/setting",
PkSelect: "prefab/mainMenu/PkSelect",
PkSelecDetail: "prefab/mainMenu/PkSelecDetail",
PkWait: "prefab/mainMenu/PkWait",
toast: "prefab/zjadbar/toast",
ZjadBar_break: "script/zjadbar/ZjadBar_break",
ZjadBar_full: "script/zjadbar/ZjadBar_full",
ZjadBar_pop: "script/zjadbar/ZjadBar_pop",
EggMenu: "script/zjadbar/EggMenu",
subContext: "script/zjadbar/subContext"
};
e.SceneRes = {
GameMain: "GameMain"
};
e.NodePoolKey = {
Npc: "cccnpc"
};
e.UserDataTime = "lookPkTimeTest1";
e.UserDataRed = "lookPkRedTest1";
e.UserDataPlayTime = "UserDataPlayTime0";
e.UserDataTopicRed = "UserDataTopicRed12";
e.UserDataRankClick = "UserDataRankClick";
e.UserDataLoginClick = "UserDataLoginClick";
e.UserDataTopicHotClick = "UserDataTopicHotClick";
e.UserDataSub = "UserDataSub";
e.UserDataKey = "lanqiu_gameData";
e.UserUUIDKey = "lwdx_2019_11_25_00";
e.UserUUID = "";
e.UserADState = !0;
e.isShare = !1;
e.Channel = "";
e.Version = "149";
e.mediaList = [ "路人粉", "铁粉", "忠粉", "代言人" ];
e.BaseConfig = {
realNameAuth: 0,
PutOlineInterval: 60,
CheckConfig: {
Channel: "",
Version: 0,
List: "",
AdSelect: 1,
ChannelStatus: "0"
}
};
e.UserData = {
Authorization: "",
nickName: "游客",
avatarUrl: "",
gender: 0,
lvData: {
Grade: 1,
Score: 0
},
Trophy: [ 0, 0 ],
classLvData: null,
province: "",
user_id: 0,
lookPkRed: 0,
lookPkTime: 0,
playTime: [],
topicRed: {},
newToday: !1,
medalConfig: [ 0, 30, 70, 100 ],
itemsConfig: [],
goldConfig: null,
raceConfig: null,
shareVideoReward: {
max: 3,
countdown: 0,
coins: 500
},
medal: null,
topicConfig: {
HomeRefresh: 300,
EachPlayTime: 20,
ABPrice: 500,
LocalTest: !1,
Price: 0
},
topicGetTimes: 1,
shareText: null,
lossVideo: "",
lossVideoNoAudio: "",
victoryVideo: "",
victoryVideoNoAudio: "",
imageVideo: "",
wxAvatarUrl: "",
idCheck: !0,
property: {
balance: 0,
items: [ {
ItemID: 2,
Name: "去除错误答案",
Icon: "",
Description: "去除错误答案",
Amount: 200,
Total: 0
}, {
ItemID: 3,
Name: "显示正确答案",
Icon: "",
Description: "显示正确答案",
Amount: 200,
Total: 0
}, {
ItemID: 4,
Name: "重新答题",
Icon: "",
Description: "重新答题",
Amount: 200,
Total: 0
}, {
ItemID: 5,
Name: "对对手进行减分",
Icon: "",
Description: "对对手进行减分",
Amount: 200,
Total: 0
} ]
},
store: [ {
ItemID: 2,
Name: "去除错误答案",
Icon: "",
Description: "去除错误答案",
Amount: 200,
Total: 0
}, {
ItemID: 3,
Name: "显示正确答案",
Icon: "",
Description: "显示正确答案",
Amount: 200,
Total: 0
}, {
ItemID: 4,
Name: "重新答题",
Icon: "",
Description: "重新答题",
Amount: 200,
Total: 0
}, {
ItemID: 5,
Name: "对对手进行减分",
Icon: "",
Description: "对对手进行减分",
Amount: 200,
Total: 0
} ]
};
e.GameAiVideo = {
url: null
};
e.Query = {
type: "",
matchId: "",
userId: "",
url: ""
};
e.Scene = "";
e.UserDataList = {
highScore: 0,
money: 9999,
ballType: 0,
ballTypeList: [ 0 ]
};
e.lvData = [ {
lv: 1,
Score: 0
}, {
lv: 2,
Score: 10
}, {
lv: 3,
Score: 30
}, {
lv: 4,
Score: 50
}, {
lv: 5,
Score: 100
}, {
lv: 6,
Score: 150
}, {
lv: 7,
Score: 200
}, {
lv: 8,
Score: 250
}, {
lv: 9,
Score: 300
}, {
lv: 10,
Score: 350
}, {
lv: 11,
Score: 400
}, {
lv: 12,
Score: 450
}, {
lv: 13,
Score: 500
}, {
lv: 14,
Score: 550
}, {
lv: 15,
Score: 600
}, {
lv: 16,
Score: 700
}, {
lv: 17,
Score: 800
}, {
lv: 18,
Score: 900
}, {
lv: 19,
Score: 1e3
}, {
lv: 20,
Score: 1100
}, {
lv: 21,
Score: 1300
}, {
lv: 22,
Score: 1500
}, {
lv: 23,
Score: 1700
}, {
lv: 24,
Score: 1900
}, {
lv: 25,
Score: 2100
}, {
lv: 26,
Score: 2600
}, {
lv: 27,
Score: 3100
}, {
lv: 28,
Score: 3600
}, {
lv: 29,
Score: 4100
}, {
lv: 30,
Score: 4600
} ];
e.Game = {
ballCount: 0,
ballScore: 0,
ballFaultCount: 0,
pop: 0,
ballADType: !1,
ballADTypeCount: !1,
moneyCount: 0
};
e.GameZJadBar = {
ZjadBar_pop: null,
ZjadBar_alone: null,
ZjadBar_bottom: null
};
e.OpenDataMessageType = {
GetCloudStorage: "getcloudstorage",
ShowEndlessRank: "showendlessrank",
ShowLimitTimeRank: "showLimittimerank",
ShowGoodTimeRank: "showgoodtimerank"
};
e.OpenDataMessageData = {
MessageType: "",
KVDataList: [],
Data: "",
DataKey: ""
};
e.CloudData = {
UUID: "",
Score: 0,
UpdateTime: 0
};
e.AdMap = {
"adunit-fa207acad4e9a1e2": "CoinsPopupBanner",
"adunit-fd5318a90d6f52a0": "RewardPopupBanner",
"adunit-a9994f3e7072ec1a": "LoginRewardBanner",
"adunit-c8e7051beb53cef2": "RewardVideo"
};
e.NativeData = {
nativeTopicImg: ""
};
e.DeviceInfo = {
deviceId: "",
mac: "",
version: "",
uuid: "",
user_id: "",
channel: 0,
app_code: "",
android_id: "",
os: 1,
oaid: "",
vendor: "",
model: "",
os_version: ""
};
return e;
}();
i.BaseConfig = n;
cc._RF.pop();
}, {} ],
BaseLayer: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "c81f4MlKaJDTrC4g31R1JG9", "BaseLayer");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
});
Object.defineProperty(i, "__esModule", {
value: !0
});
i.BaseLayer = void 0;
var o = e("./BaseUtils"), a = e("./PropManager"), s = function(e) {
r(t, e);
function t() {
return e.call(this) || this;
}
t.prototype.SetSprite = function(e, t, i, n, r, o) {
void 0 === i && (i = 0);
void 0 === n && (n = null);
void 0 === o && (o = null);
e ? i ? this.LoadNetImg(t, function(t) {
t.packable = !1;
var i = new cc.SpriteFrame(t);
if (e.isValid) {
var r = e.getComponent(cc.Sprite);
r ? r.spriteFrame = i : e.addComponent(cc.Sprite).spriteFrame = i;
n && n();
}
}, r, o) : this.LoadResImg(t, function(t) {
if (e.isValid) {
var i = e.getComponent(cc.Sprite);
i ? i.spriteFrame = t : e.addComponent(cc.Sprite).spriteFrame = t;
n && n();
}
}) : console.error("SetSprite node is null");
};
t.prototype.LoadResImg = function(e, t) {
void 0 === t && (t = null);
cc.assetManager.loadBundle("resBundle", function(i, n) {
n.load(e, cc.SpriteFrame, function(e, i) {
e && console.error(e);
t && t(i);
});
});
};
t.prototype.LoadNetImg = function(e, t, i, n) {
void 0 === t && (t = null);
cc.assetManager.loadRemote(e, i, function(e, i) {
if (e) {
console.error(e);
n && n(e);
} else t && t(i);
});
};
t.prototype.LoadPrefab = function(e, i, n, r) {
void 0 === n && (n = null);
void 0 === r && (r = null);
e && cc.assetManager.loadBundle("resBundle", function(o, a) {
a.load(i, cc.Prefab, function(i, o) {
if (i) console.error(i); else if (e) {
var a = cc.instantiate(o);
a.getComponent(t).Init(n);
e.addChild(a);
r && r(a);
}
});
});
};
t.prototype.ActMoveBy = function(e, t, i, n, r, o) {
void 0 === r && (r = 0);
void 0 === o && (o = this.ActCallFun);
if (r > 0) {
var a = cc.callFunc(o, this, r), s = cc.moveBy(t, cc.v2(i, n));
s.setTag(r);
e.runAction(cc.sequence(s, a));
} else e.runAction(cc.moveBy(t, cc.v2(i, n)));
};
t.prototype.ActMoveTo = function(e, t, i, n, r, o) {
void 0 === r && (r = 0);
void 0 === o && (o = this.ActCallFun);
if (r > 0) {
var a = cc.callFunc(o, this, r), s = cc.moveTo(t, cc.v2(i, n));
s.setTag(r);
e.runAction(cc.sequence(s, a));
} else e.runAction(cc.moveTo(t, cc.v2(i, n)));
};
t.prototype.ActDelayTime = function(e, t, i, n) {
void 0 === i && (i = 0);
void 0 === n && (n = this.ActCallFun);
if (i > 0) {
var r = cc.callFunc(n, this, i), o = cc.delayTime(t);
o.setTag(i);
e.runAction(cc.sequence(o, r));
} else e.runAction(cc.delayTime(t));
};
t.prototype.ActCallFun = function() {};
t.prototype.PlayAnimtion = function(e, t, i, n, r, a, s) {
void 0 === t && (t = null);
void 0 === i && (i = null);
void 0 === n && (n = null);
void 0 === r && (r = null);
void 0 === a && (a = null);
void 0 === s && (s = null);
var c = e.getComponent(cc.Animation);
if (c) {
t ? c.play(t) : c.play();
i && c.on("play", i, this);
s && c.on("lastframe", s, this);
n && c.on("finished", n, this);
r && c.on("pause", r, this);
a && c.on("resume", a, this);
} else o.Utils.CCLog("PlayAnimtion nodename:" + e.name + "PlayAnimtion not Animation");
};
t.prototype.PlaySpineAnimtion = function(e, t, i, n, r, a, s) {
void 0 === t && (t = null);
void 0 === i && (i = !1);
void 0 === n && (n = null);
void 0 === r && (r = null);
void 0 === a && (a = null);
void 0 === s && (s = null);
var c = e.getComponent(sp.Skeleton);
if (c) {
n && c.setStartListener(n);
r && c.setEndListener(r);
a && c.setDisposeListener(a);
s && c.setInterruptListener(s);
c.loop = i;
c.animation = t;
} else o.Utils.CCLog("PlaySpineAnimtion node is not sp.Skeleton component");
};
t.prototype.SetSpineSkin = function(e, t) {
var i = e.getComponent(sp.Skeleton);
i && i.setSkin(t);
};
t.prototype.LoadNetSpineSkeleton = function(e, t, i, n) {
void 0 === n && (n = null);
null != e ? cc.loader.load(t + i + ".png", function(r, o) {
cc.loader.load({
url: t + i + ".atlas",
type: "txt"
}, function(r, a) {
cc.loader.load({
url: t + i + ".json",
type: "txt"
}, function(t, r) {
var s = e.getComponent(sp.Skeleton);
s || (s = e.addComponent(sp.Skeleton));
var c = new sp.SkeletonData();
c.skeletonJson = r;
c.atlasText = a;
c.textures = [ o ];
c.textureNames = [ i + ".png" ];
s.skeletonData = c;
n && n();
});
});
}) : o.Utils.CCLog("_node is null");
};
t.prototype.GetWorldBoundingBox = function(e) {
if (e) {
var t = e.parent.convertToWorldSpaceAR(cc.v2(e.x, e.y));
return cc.Rect.fromMinMax(cc.v2(t.x - e.width * e.anchorX, t.y - e.height * e.anchorY), cc.v2(t.x + e.width * (1 - e.anchorX), t.y + e.height * (1 - e.anchorY)));
}
t = this.node.convertToWorldSpaceAR(cc.Vec2.ZERO);
return cc.Rect.fromMinMax(cc.v2(t.x - this.node.width * this.node.anchorX, t.y - this.node.height * this.node.anchorY), cc.v2(t.x + this.node.width * (1 - this.node.anchorX), t.y + this.node.height * (1 - this.node.anchorY)));
};
t.prototype.loaderSpriteFrameToNode = function(e, t, i, n) {
var r = this;
void 0 === n && (n = .2);
var o = i.indexOf("@");
if (-1 == o) cc.loader.load({
url: t + i,
type: "png"
}, function(t, i) {
var n = new cc.SpriteFrame(i);
e.getComponent(cc.Sprite).spriteFrame = n;
}); else {
var a = i.indexOf("."), s = i.substring(o + 1, a).split("_");
cc.loader.load({
url: t + i,
type: "png"
}, function(t, i) {
e.m_texture = i;
r.ActFpriteFrame(e, s[0], s[1], n, 0);
});
}
};
t.prototype.ActFpriteFrame = function(e, t, i, n, r) {
var o = this;
void 0 === r && (r = 0);
r >= t * i && (r = 0);
var a = r % t, s = Math.floor(r / i), c = e.m_texture.width / t, l = e.m_texture.height / i, h = new cc.SpriteFrame(e.m_texture), u = {
width: c,
height: l,
x: a * c,
y: s * l
};
h.setRect(u);
e.getComponent(cc.Sprite).spriteFrame = h;
r++;
var f = cc.delayTime(n), d = cc.callFunc(function() {
o.ActFpriteFrame(e, t, i, n, r);
}), p = cc.sequence(f, d);
e.runAction(p);
};
t.prototype.findProp = function(e) {
for (var t = a.PropManager.get().items, i = 0; i < t.length; i++) {
var n = t[i];
if (e == n.ItemID) return n;
}
return null;
};
t.prototype.Init = function() {};
t.prototype.Free = function() {};
t.prototype.OnClick = function() {};
t.prototype.OnToggle = function() {};
return t;
}(cc.Component);
i.BaseLayer = s;
cc._RF.pop();
}, {
"./BaseUtils": "BaseUtils",
"./PropManager": "PropManager"
} ],
BaseUtils: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "3bf7fswUtRI6r622KvG+6Y3", "BaseUtils");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.MenuType = i.Utils = void 0;
var n = e("./BaseConfig"), r = e("../Common/HttpRequestNew"), o = function() {
function e() {}
e.CCLog = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.log.apply(console, e);
};
e.CCLogCount = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.count.apply(console, e);
};
e.CCLogResetCount = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.countReset.apply(console, e);
};
e.CCLogDir = function(e) {
n.BaseConfig.Debug && console.dir(e);
};
e.CCLogTrace = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.trace.apply(console, e);
};
e.CCLogGroup = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.group.apply(console, e);
};
e.CCLogGroupEnd = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.groupEnd.apply(console, e);
};
e.CCLogTime = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.time.apply(console, e);
};
e.nowTime = function() {
return new Date().getTime();
};
e.nowSTime = function() {
return Math.floor(new Date().getTime() / 1e3);
};
e.CCLogTimeEnd = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.timeEnd.apply(console, e);
};
e.CCLogError = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.error.apply(console, e);
};
e.CCLogWarn = function() {
for (var e = [], t = 0; t < arguments.length; t++) e[t] = arguments[t];
n.BaseConfig.Debug && console.warn.apply(console, e);
};
e.CCLogClear = function() {
n.BaseConfig.Debug && console.clear();
};
e.RandNum = function(e, t) {
return t < e ? Math.round(t) + Math.round(Math.random() * (e - t)) : t === e ? Math.round(e) : Math.round(e) + Math.round(Math.random() * (t - e));
};
e.RandFloat = function(e, t) {
return t < e ? t + Math.random() * (e - t) : t === e ? e : e + Math.random() * (t - e);
};
e.RandRadian = function() {
return Math.random() > .5 ? Math.random() % Math.PI : -Math.random() % Math.PI;
};
e.IsNull = function(e) {
return null === e || "" === e || "null" === e || "Null" === e || "NULL" === e || void 0 === e;
};
e.CloneObj = function(e) {
var t = {};
e instanceof Array && (t = []);
for (var i in e) {
var n = e[i];
t[i] = null === n ? null : "object" == typeof n ? this.CloneObj(n) : n;
}
return t;
};
e.GetUUID = function() {
var e = new Date().getTime();
return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(t) {
var i = (e + 16 * Math.random()) % 16 | 0;
e = Math.floor(e / 16);
return ("x" == t ? i : 3 & i | 8).toString(16);
});
};
e.GetStringCutOut = function(e, t) {
void 0 === t && (t = 10);
var i = "";
if (null == e) return i;
for (var n = 0, r = 0; r < e.length; r++) {
e.charCodeAt(r) > 127 || 94 == e.charCodeAt(r) ? n += 2 : n++;
n <= t && (i += e.charAt(r));
}
i.length < e.length && (i += "...");
return i;
};
e.GetStringToArray = function(t, i) {
void 0 === i && (i = ";");
return e.IsNull(t) ? [] : t.split(i);
};
e.RadianToVec2 = function(e, t) {
void 0 === t && (t = 1);
return cc.v2(Math.cos(e) * t, Math.sin(e) * t);
};
e.AngleToRadian = function(e) {
return e / 180 * Math.PI;
};
e.RadianToAngle = function(e) {
return e / Math.PI * 180;
};
e.Vec2Distance = function(e, t) {
var i = cc.v2(0, 0);
i.x = e.x - t.x;
i.y = e.y - t.y;
return Math.sqrt(i.x * i.x + i.y * i.y);
};
e.Vec2Sub = function(e, t) {
var i = cc.v2(0, 0);
i.x = e.x - t.x;
i.y = e.y - t.y;
return i;
};
e.Vec2Radian = function(e) {
return Math.atan2(e.y, e.x);
};
e.Vec2Lenght = function(e) {
return Math.sqrt(e.x * e.x + e.y * e.y);
};
e.Vec2Normalize = function(e) {
var t = cc.v2(0, 0), i = e.x * e.x + e.y * e.y;
if (1 === i) return e;
if (0 === i) return e;
var n = 1 / Math.sqrt(i);
t.x = e.x * n;
t.y = e.y * n;
return t;
};
e.Vec2Mul = function(e, t) {
var i = cc.v2(0, 0);
i.x = e.x * t;
i.y = e.y * t;
return i;
};
e.GetTimeFormat = function(e) {
var t = new Date(e), i = t.getFullYear(), n = t.getMonth() + 1, r = t.getDate();
return i + "-" + (n < 10 ? "0" + n : n) + "-" + (r < 10 ? "0" + r : r) + " " + t.toTimeString().substr(0, 8);
};
e.GetTimeTimesMinutesSeconds = function(e) {
var t = e;
return (Math.floor(t / 3600) < 10 ? "0" + Math.floor(t / 3600) : Math.floor(t / 3600)) + ":" + (Math.floor(t / 60 % 60) < 10 ? "0" + Math.floor(t / 60 % 60) : Math.floor(t / 60 % 60)) + ":" + (Math.floor(t % 60) < 10 ? "0" + Math.floor(t % 60) : Math.floor(t % 60));
};
e.timestampToTime = function(e) {
var t = new Date(1e3 * e);
return t.getFullYear() + "-" + (t.getMonth() + 1 < 10 ? "0" + (t.getMonth() + 1) : t.getMonth() + 1) + "-" + (t.getDate() < 10 ? "0" + t.getDate() : t.getDate());
};
e.GetTimeMinutesSeconds = function(e) {
var t = Math.floor(e / 60), i = e - 60 * t;
return (t < 10 ? "0" + t : t) + ":" + (i < 10 ? "0" + i : i);
};
e.GetTimeMinutesSecondsCh = function(e) {
var t = Math.floor(e / 60);
return t + "分" + (e - 60 * t) + "秒";
};
e.GetTimeMinutes = function(e) {
return Math.ceil(e / 60);
};
e.GetTimeDay = function(e) {
return Math.ceil(e / 1e3 / 60 / 60 / 24);
};
e.GetTimeHour = function(e) {
return Math.ceil(e / 1e3 / 60 / 60);
};
e.StringCalculateSum = function(e, t) {
"string" != typeof e && (e += "");
"string" != typeof t && (t += "");
for (var i = e.split(""), n = t.split(""), r = "", o = 0; i.length || n.length || o; ) {
r = (o += ~~i.pop() + ~~n.pop()) % 10 + r;
o = o > 9;
}
return r;
};
e.StringCalculateSub = function(t, i) {
"string" != typeof t && (t += "");
"string" != typeof i && (i += "");
var n = t.split(""), r = i.split(""), o = "", a = 0, s = 0;
if (parseInt(t) < parseInt(i)) {
var c = n;
n = r;
r = c;
s = -1;
} else if (t == i) return 0;
e.CCLog("StringCalculateAddition  raList", n, "rbList", r);
for (;n.length || r.length || a; ) {
e.CCLogCount("StringCalculateSub whiel");
var l = ~~n.pop() - ~~r.pop() - a;
e.CCLog("ad", l, "count", a);
o = (l < 0 ? 10 + l : l) + o;
a = l < 0;
}
for (var h = o.split(""), u = 0; u < h.length; u++) {
if (0 != h[u]) {
h.splice(0, u);
break;
}
if (u == h.length - 1) {
h.splice(0, h.length - 1);
break;
}
}
-1 == s && h.splice(0, 0, "-");
e.CCLog("StringCalculateSub  raList", n, "rbList", r, "result", o, "relist.join", h.join(""));
return h.join("");
};
e.StringGetConversionUnit = function(e) {
"string" != typeof e && (e += "");
var t = [ "", "K", "B", "M", "T", "P", "E", "Z", "Y", "S", "L", "X", "D" ], i = 0, n = e.replace(/(^|\s)\d+/g, function(e) {
return e.replace(/(?=(?!\b)(\d{3})+$)/g, ",");
}).split(",");
if (n.length > t.length) {
n.splice(n.length - t.length, t.length - 1);
i = t[t.length - 1];
} else {
i = t[n.length - 1];
n.splice(1, n.length - 1);
}
return n.join("") + i;
};
e.Str_To_Unicode = function(e) {
for (var t = "\\u", i = 0, n = e.length; i < n; i++) i < n - 1 ? t += e.charCodeAt(i).toString(16) + "\\u" : i === n - 1 && (t += e.charCodeAt(i).toString(16));
return t;
};
e.Unicode_To_Str = function(e) {
for (var t = [], i = e.split("\\u"), n = 2, r = i.length - 1; n < r; n++) i[n] && t.push(String.fromCharCode(parseInt(i[n], 16)));
return t.join("");
};
e.toChinese = function(e) {
e = Math.floor(e);
var t = "", i = Math.floor(Math.log10(e)) + 1, n = [ "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" ], r = [ "", "十", "百", "千", "万" ];
if (i >= 9) {
var o = Math.floor(e / Math.pow(10, 8)), a = e % Math.pow(10, 8), s = Math.floor(Math.log10(a)) + 1;
return this.toChinese(o) + "亿" + (s < 8 ? "零" : "") + (a > 0 ? this.toChinese(a) : "");
}
if (1 == i) return n[e];
if (2 == i) {
(o = Math.floor(e / 10)) > 1 && (t = n[o]);
t += r[1];
(a = e % 10) > 0 && (t += n[a]);
return t;
}
if (i > 5) {
o = e / Math.pow(10, 4), a = e % Math.pow(10, 4), s = Math.floor(Math.log10(a)) + 1;
return this.toChinese(o) + "万" + (s < 4 ? "零" : "") + (a > 0 ? this.toChinese(a) : "");
}
for (var c = i; c >= 1; c--) {
var l = Math.floor(e / Math.pow(10, c - 1) % 10);
l > 0 ? t = t + n[l] + r[c - 1] : c > 1 && Math.floor(e / Math.pow(10, c - 2) % 10) > 0 && c > 1 && (t += n[l]);
}
return t;
};
e.toRome = function(e) {
return e < 0 || e > 5 ? "" : [ "", "Ⅰ", "Ⅱ", "Ⅲ", "Ⅳ", "Ⅴ" ][e];
};
e.fillWithRandom = function(e, t, i) {
var n = new Array(i), r = 0, o = function() {
for (var o = 0; o < i; o++) n[o] = Math.random();
r = n.reduce(function(e, t) {
return e + t;
}, 0);
var a = (t - i) / r;
n = n.map(function(t) {
return Math.min(e, Math.round(t * a) + 1);
});
r = n.reduce(function(e, t) {
return e + t;
}, 0);
};
do {
o();
} while (r - t);
return n;
};
e.getRandomArrayElements = function(e, t) {
for (var i, n, r = e.slice(0), o = e.length, a = o - t; o-- > a; ) {
i = r[n = Math.floor((o + 1) * Math.random())];
r[n] = r[o];
r[o] = i;
}
return r.slice(a);
};
e.getTopPage = function() {
return n.BaseConfig.currentPages[n.BaseConfig.currentPages.length - 1];
};
e.backPage = function() {
MenuManage.instance.RmoveMenu(this.getTopPage());
};
e.getLocalUrl = function(e) {
return "https://answer-1304723711.cos.ap-beijing.myqcloud.com/cocos/canvas/" + e;
};
e.getLocaltionPath = function(e) {
e = "resBundle/images/canvas/" + e;
var t = cc.path.changeExtname(e.substr(10)), i = cc.path.extname(e);
cc.AssetManager.BuiltinBundleName.RESOURCES;
return cc.AssetManager.prototype._transform({
path: t,
bundle: "resBundle",
__isNative__: !0,
ext: i
});
};
e.topicMedia = function(e, t) {
if (e >= t) return 4;
var i = e / t * 100;
return i > n.BaseConfig.UserData.medalConfig[2] ? 3 : i > n.BaseConfig.UserData.medalConfig[1] ? 2 : 1;
};
e.setNodeGray = function(t, i) {
if (!e.IsNull(t)) {
var n = t.getComponentsInChildren(cc.Sprite);
if (n) for (var r = 0; r < n.length; r++) i ? n[r].setMaterial(0, cc.Material.getBuiltinMaterial("2d-gray-sprite")) : n[r].setMaterial(0, cc.Material.getBuiltinMaterial("2d-sprite"));
}
};
e.todayJudge = function(e, t) {
void 0 === t && (t = "timestamp");
"string" == typeof e && "timestamp" == t && (e = Number(e));
"datetime" == t && (e = e.replace(/-/g, "/"));
return new Date().setHours(0, 0, 0, 0) == new Date(e).setHours(0, 0, 0, 0);
};
e.shuffle = function(e) {
for (var t, i = [], n = e.length; n; ) if ((t = Math.floor(Math.random() * e.length)) in e) {
i.push(e[t]);
delete e[t];
n--;
}
return i;
};
e.event = function(t, i) {
i.event = t;
if (!e.IsNull(i.deviceId)) {
e.IsNull(i.adNetworkPlatformName) || (i.media = i.adNetworkPlatformName);
this.requet.event("api/v1/log?logType=Ad&Ver=v1.0", i, function() {});
}
};
e.requet = new r.default();
return e;
}();
i.Utils = o;
var a = function() {
function e() {}
e.TransitionType = {
None: 1,
FadeIn: 2,
FadeOut: 3,
PopUp: 4,
LeftIn: 5,
LeftOut: 6,
RightIn: 7,
RightOut: 8,
ScaleFadeIn: 9,
ScaleFadeOut: 10
};
return e;
}();
i.MenuType = a;
cc._RF.pop();
}, {
"../Common/HttpRequestNew": "HttpRequestNew",
"./BaseConfig": "BaseConfig"
} ],
Box: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "d2b41ySfv9PJ751R/GPddl7", "Box");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./GlobalConst"), h = e("./LevelUtils"), u = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.label = null;
t.value = 0;
t.scale = 0;
t.originWidth = 0;
t.gameControl = null;
t.m_Oritation = "Bottom";
t.drawedFrame = null;
t.undrawFrame = null;
t.bodyPrefab = null;
t.arrowNode = null;
t.arrowSprite = null;
t.arrowEndSprite = null;
t.bgNode = null;
t.bodyNode = null;
t.move2Vec = cc.v2();
t.isMovingForward = !1;
t.isMovingBackward = !1;
t.scale2X = 1;
t.scale2Y = 1;
t.originScale = 1;
t.scale2Width = 0;
t.scale2Height = 0;
t.move2X = 0;
t.move2Y = 0;
t.forwardOri = "";
t.forwardAnimTime = 0;
t.backwardAnimTime = 0;
t.forwardStartTime = 0;
t.backwardStartTime = 0;
t.drawedColor = new cc.Color();
t.defaultColor = new cc.Color().fromHEX("#418D90");
t.arrowColor = new cc.Color();
t.globalConst = new l.default();
t.levelUtil = new h.default();
return t;
}
t.prototype.onLoad = function() {
this.gameControl = this.node.getParent().getComponent("GameControl");
};
t.prototype.processTouch = function() {
-1 != this.value && null != this.gameControl && (0 == this.value ? this.gameControl.hasAroundNode(this.x_Index, this.y_Index) : this.gameControl.popPathArrayList(this.node));
};
t.prototype.setValue = function(e, t) {
this.value = e;
this.scale = t;
null != this.bodyNode && this.initBoxWidth();
this.changeThemeColor();
};
t.prototype.reset = function(e) {
this.value = 0;
this.m_Oritation = "null";
this.node.getComponent(cc.Sprite).spriteFrame = this.undrawFrame;
this.node.color = this.defaultColor;
null != this.bodyNode && (this.bodyNode.opacity = 0);
e && (this.arrowNode.opacity = 0);
this.node.scale = 1;
this.bgNode.opacity = 0;
};
t.prototype.generateBodyNode = function() {
this.bodyNode = cc.instantiate(this.bodyPrefab);
this.node.getParent().getParent().getChildByName("body").addChild(this.bodyNode);
this.initBoxWidth();
var e = this.node.getParent().convertToWorldSpaceAR(this.node.getPosition()), t = this.node.getParent().getParent().convertToNodeSpaceAR(e);
this.bodyNode.setPosition(t);
this.bodyNodeOriginVec = t;
this.bodyNode.opacity = 0;
this.bodyNode.color = this.drawedColor;
};
t.prototype.playForward = function(e, t, i) {
null == this.bodyNode && this.generateBodyNode();
this.forwardAnimTime = t;
this.bodyNode.opacity = 255;
this.setBodyNodeState(e, i);
this.bodyNode.width = this.originWidth;
this.bodyNode.height = this.originWidth;
this.bodyNode.setPosition(this.bodyNodeOriginVec);
this.isMovingBackward = !1;
this.bodyNode.stopAllActions();
this.isMovingForward = !0;
this.forwardStartTime = Date.now();
var n = cc.sequence(cc.delayTime(t), cc.callFunc(function() {
this.isMovingForward = !1;
this.bodyNode.width = this.scale2Width;
this.bodyNode.height = this.scale2Height;
this.bodyNode.setPosition(this.move2Vec);
}.bind(this)));
this.bodyNode.runAction(n);
};
t.prototype.playBackward = function(e, t) {
null == this.bodyNode && this.generateBodyNode();
this.backwardAnimTime = e;
this.bodyNode.opacity = 255;
var i = Math.max(this.scale2Width, this.scale2Height) * this.scale;
this.setBodyNodeState(t, i);
this.bodyNode.width = this.scale2Width;
this.bodyNode.height = this.scale2Height;
this.bodyNode.setPosition(this.move2Vec);
this.isMovingForward = !1;
this.bodyNode.stopAllActions();
this.isMovingBackward = !0;
this.forwardStartTime = Date.now();
var n = cc.sequence(cc.delayTime(e), cc.callFunc(function() {
this.bodyNode.opacity = 0;
this.isMovingBackward = !1;
this.scale2Width = this.originWidth;
this.scale2Height = this.originWidth;
this.move2Vec = cc.v2(this.bodyNodeOriginVec);
this.bodyNode.width = this.originWidth;
this.bodyNode.height = this.originWidth;
this.bodyNode.setPosition(this.move2Vec);
}.bind(this)));
this.bodyNode.runAction(n);
};
t.prototype.setBodyNodeState = function(e, t) {
this.forwardOri = e;
switch (e) {
case "Left":
this.move2Vec.x = this.bodyNodeOriginVec.x + this.node.width / 2 - t / 2;
this.move2Vec.y = this.bodyNodeOriginVec.y;
this.scale2Width = t / this.scale;
this.scale2Height = this.originWidth;
break;

case "Top":
this.move2Vec.y = this.bodyNodeOriginVec.y - this.node.height / 2 + t / 2;
this.move2Vec.x = this.bodyNodeOriginVec.x;
this.scale2Width = this.originWidth;
this.scale2Height = t / this.scale;
break;

case "Right":
this.move2Vec.x = this.bodyNodeOriginVec.x - this.node.width / 2 + t / 2;
this.move2Vec.y = this.bodyNodeOriginVec.y;
this.scale2Width = t / this.scale;
this.scale2Height = this.originWidth;
break;

case "Bottom":
this.move2Vec.y = this.bodyNodeOriginVec.y + this.node.height / 2 - t / 2;
this.move2Vec.x = this.bodyNodeOriginVec.x;
this.scale2Width = this.originWidth;
this.scale2Height = t / this.scale;
}
};
t.prototype.update = function() {
var e = (Date.now() - this.forwardStartTime) / 1e3;
if (this.isMovingForward) {
var t = this.originWidth + (this.scale2Width - this.originWidth) / this.forwardAnimTime * e, i = this.bodyNodeOriginVec.x + (this.move2Vec.x - this.bodyNodeOriginVec.x) / this.forwardAnimTime * e, n = this.originWidth + (this.scale2Height - this.originWidth) / this.forwardAnimTime * e, r = this.bodyNodeOriginVec.y + (this.move2Vec.y - this.bodyNodeOriginVec.y) / this.forwardAnimTime * e;
switch (this.forwardOri) {
case "Left":
this.bodyNode.height = this.originWidth;
t > this.scale2Width && (t = this.scale2Width);
this.bodyNode.width = t;
i < this.move2Vec.x && (i = this.move2Vec.x);
this.bodyNode.x = i;
break;

case "Right":
this.bodyNode.height = this.originWidth;
t > this.scale2Width && (t = this.scale2Width);
this.bodyNode.width = t;
i > this.move2Vec.x && (i = this.move2Vec.x);
this.bodyNode.x = i;
break;

case "Top":
this.bodyNode.width = this.originWidth;
n > this.scale2Height && (n = this.scale2Height);
this.bodyNode.height = n;
r > this.move2Vec.y && (r = this.move2Vec.y);
this.bodyNode.y = r;
break;

case "Bottom":
this.bodyNode.width = this.originWidth;
n > this.scale2Height && (n = this.scale2Height);
this.bodyNode.height = n;
r < this.move2Vec.y && (r = this.move2Vec.y);
this.bodyNode.y = r;
}
}
if (this.isMovingBackward) {
t = this.scale2Width - (this.scale2Width - this.originWidth) / this.forwardAnimTime * e, 
i = this.move2Vec.x - (this.move2Vec.x - this.bodyNodeOriginVec.x) / this.forwardAnimTime * e, 
n = this.scale2Height - (this.scale2Height - this.originWidth) / this.forwardAnimTime * e, 
r = this.move2Vec.y - (this.move2Vec.y - this.bodyNodeOriginVec.y) / this.forwardAnimTime * e;
switch (this.forwardOri) {
case "Left":
this.bodyNode.height = this.originWidth;
t < this.node.width && (t = this.node.width);
this.bodyNode.width = t;
i > this.bodyNodeOriginVec.x && (i = this.bodyNodeOriginVec.x);
this.bodyNode.x = i;
break;

case "Right":
this.bodyNode.height = this.originWidth;
t < this.node.width && (t = this.node.width);
this.bodyNode.width = t;
i < this.bodyNodeOriginVec.x && (i = this.bodyNodeOriginVec.x);
this.bodyNode.x = i;
break;

case "Top":
this.bodyNode.width = this.originWidth;
n < this.node.height && (n = this.node.height);
this.bodyNode.height = n;
r < this.bodyNodeOriginVec.y && (r = this.bodyNodeOriginVec.y);
this.bodyNode.y = r;
break;

case "Bottom":
this.bodyNode.width = this.originWidth;
n < this.node.height && (n = this.node.height);
this.bodyNode.height = n;
r > this.bodyNodeOriginVec.y && (r = this.bodyNodeOriginVec.y);
this.bodyNode.y = r;
}
}
};
t.prototype.notifyClicked = function(e, t) {
this.arrowNode.width = this.node.width * (1 - this.arrowNode.getComponent(cc.Widget).left - this.arrowNode.getComponent(cc.Widget).right);
this.arrowNode.height = this.node.width * (1 - this.arrowNode.getComponent(cc.Widget).top - this.arrowNode.getComponent(cc.Widget).bottom);
this.arrowNode.getComponent(cc.Sprite).spriteFrame = t ? this.arrowEndSprite : this.arrowSprite;
var i = cc.fadeIn(.1);
cc.find("Canvas").getComponent("MainControl").getVolumeControl().playMoveAudio();
this.arrowNode.runAction(i);
switch (e) {
case "Left":
this.arrowNode.angle = 90;
break;

case "Top":
this.arrowNode.angle = 0;
break;

case "Right":
this.arrowNode.angle = -90;
break;

case "Bottom":
this.arrowNode.angle = -180;
}
};
t.prototype.removeBodyNode = function() {
this.node.getParent().getParent().getChildByName("body").removeChild(this.bodyNode);
this.bodyNode = null;
this.isMovingBackward = !1;
this.isMovingForward = !1;
this.arrowNode.opacity = 0;
this.node.scale = 1;
this.bgNode.opacity = 0;
this.node.getComponent(cc.Sprite).spriteFrame = this.undrawFrame;
this.node.color = this.defaultColor;
};
t.prototype.showNotifyAnim = function() {
var e = cc.scaleTo(.1, .8);
this.node.runAction(e);
this.bgNode.runAction(cc.fadeIn(.1));
};
t.prototype.cancelNotifyAnim = function() {
var e = cc.scaleTo(.1, 1);
this.node.runAction(e);
this.bgNode.runAction(cc.fadeOut(.1));
};
t.prototype.changeThemeColor = function() {
null == this.gameControl && (this.gameControl = this.node.getParent().getComponent("GameControl"));
var e = this.gameControl.currentLevel;
if (this.gameControl.isLimitGame) {
this.drawedColor = new cc.Color().fromHEX("#FCAE00");
this.defaultColor = new cc.Color().fromHEX("#FFFFFF");
this.arrowColor = new cc.Color().fromHEX("#6D2CFF");
this.node.color = this.defaultColor;
if (this.value > 0) {
this.node.getComponent(cc.Sprite).spriteFrame = this.drawedFrame;
this.node.color = this.drawedColor;
}
} else {
var t = Math.ceil(e / this.globalConst.chapterCount / this.globalConst.levelCount);
this.levelUtil.loadThemeJson(function(e) {
this.drawedColor = new cc.Color().fromHEX("#0BF9ED");
this.defaultColor = new cc.Color().fromHEX("#FFFFFF");
this.arrowColor = new cc.Color().fromHEX(e[t].arrow);
this.node.color = this.defaultColor;
if (this.value > 0) {
this.node.getComponent(cc.Sprite).spriteFrame = this.drawedFrame;
this.node.color = this.drawedColor;
}
}.bind(this));
}
};
t.prototype.setOritation = function(e) {
this.m_Oritation = e;
};
t.prototype.initBoxWidth = function() {
this.bodyNode.scale = this.scale;
this.bodyNode.width = this.node.width / this.scale;
this.bodyNode.height = this.node.width / this.scale;
this.originWidth = this.bodyNode.width;
};
o([ c(cc.Label) ], t.prototype, "label", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "drawedFrame", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "undrawFrame", void 0);
o([ c(cc.Prefab) ], t.prototype, "bodyPrefab", void 0);
o([ c(cc.Node) ], t.prototype, "arrowNode", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "arrowSprite", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "arrowEndSprite", void 0);
o([ c(cc.Node) ], t.prototype, "bgNode", void 0);
return o([ s ], t);
}(cc.Component);
i.default = u;
cc._RF.pop();
}, {
"./GlobalConst": "GlobalConst",
"./LevelUtils": "LevelUtils"
} ],
BtnSound: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "d3c337sp3tHpYmcD12jn2J9", "BtnSound");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.ButtonScaler = void 0;
var a = cc._decorator, s = a.ccclass, c = (a.property, function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.safeTime = .5;
t.clickEvents = [];
return t;
}
t.prototype.onLoad = function() {
var e = this, t = this.node.getComponent(cc.Button);
if (t) {
this.clickEvents = t.clickEvents;
this.node.on("click", function() {
if (0 != t.clickEvents.length) {
t.clickEvents = [];
var i = setTimeout(function() {
cc.log(i, "delayFun++++++++");
clearTimeout(i);
e && e.node && t && (t.clickEvents = e.clickEvents);
}, 1e3);
}
}, this);
}
};
return o([ s ], t);
}(cc.Component));
i.ButtonScaler = c;
cc._RF.pop();
}, {} ],
ButtonScaler: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "39453IUyv5GVZgccpUcVCg0", "ButtonScaler");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.ButtonScaler = void 0;
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.scaleTo = new cc.Vec3(1.2, 1.2, 1.2);
t.transDuration = .2;
t.button = null;
t._scale = new cc.Vec3(1, 1, 1);
t._lastScale = new cc.Vec3();
t._start = new cc.Vec3();
return t;
}
t.prototype.onLoad = function() {
this.initScale = this.node.scale;
this.button = this.getComponent(cc.Button);
var e = cc.tween(this._scale), t = cc.tween(this._scale);
this.node.getScale(this._start);
e.to(this.transDuration, this.scaleTo, {
easing: "cubicInOut"
});
t.to(this.transDuration, this._start, {
easing: "cubicInOut"
});
this._lastScale.set(this._scale);
function i() {
e.stop();
t.start();
}
this.node.on(cc.Node.EventType.TOUCH_START, function() {
e.start();
}, this);
this.node.on(cc.Node.EventType.TOUCH_END, i, this);
this.node.on(cc.Node.EventType.TOUCH_CANCEL, i, this);
};
t.prototype.update = function() {
if (!this._scale.equals(this._lastScale)) {
this.node.setScale(this._scale);
this._lastScale.set(this._scale);
}
};
o([ c ], t.prototype, "scaleTo", void 0);
o([ c ], t.prototype, "transDuration", void 0);
return o([ s ], t);
}(cc.Component);
i.ButtonScaler = l;
cc._RF.pop();
}, {} ],
CallNative: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "a2dd5kAYA1CG7BIwaNdD+eO", "CallNative");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.CallNative = void 0;
var n = e("./MethodManager"), r = e("./JsBridge"), o = function() {
function e() {}
e.callNative = function(e) {
console.log(this.TAG, "to..." + e);
return r.JsBridge.isAndroidPlatform() ? jsb.reflection.callStaticMethod("com/wll/common/JsBridge", "toNative", "(Ljava/lang/String;)V", e) : r.JsBridge.isIosPlatform() ? jsb.reflection.callStaticMethod("JsBridge", "toNative:", e) : void 0;
};
e.callCocos = function(e) {
console.log(this.TAG, "back..." + e);
var t = JSON.parse(e);
null != t && n.MethodManager.instance.hasMethod(t.cmd) && n.MethodManager.instance.applyMethod(t.cmd, t.msg);
};
e.TAG = "CallNative";
return e;
}();
i.CallNative = o;
cc._RF.pop();
}, {
"./JsBridge": "JsBridge",
"./MethodManager": "MethodManager"
} ],
CatFace: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "fd508Ke3UpOwZoBXc7pa4LJ", "CatFace");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.m_EyeOpen = null;
t.m_EyeClose = null;
return t;
}
t.prototype.openEye = function() {
this.m_EyeOpen.active = !0;
this.m_EyeClose.active = !1;
};
t.prototype.closeEye = function() {
this.m_EyeOpen.active = !1;
this.m_EyeClose.active = !0;
};
t.prototype.start = function() {};
o([ c(cc.Node) ], t.prototype, "m_EyeOpen", void 0);
o([ c(cc.Node) ], t.prototype, "m_EyeClose", void 0);
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {} ],
CoinsUtil: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "7d91fHGBr9DUI2/eZgtotcJ", "CoinsUtil");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, e("./Common/StorageUtil")), s = e("./Bean/UserData"), c = (window.wx, 
function() {
function e() {
this.storageUtil = new a.default();
this.userData = null;
}
e.prototype.addCoins = function(e) {
this.userData = this.storageUtil.getData("userData");
null == this.userData && (this.userData = new s.default());
this.userData.gold += e;
this.storageUtil.saveData("userData", this.userData);
return this.userData.gold;
};
e.prototype.costCoins = function(e) {
this.userData = this.storageUtil.getData("userData");
null == this.userData && (this.userData = new s.default());
var t = this.userData.gold;
if ((t -= e) < 0) return !1;
this.userData.gold = t;
this.storageUtil.saveData("userData", this.userData);
return !0;
};
return n([ o ], e);
}());
i.default = c;
cc._RF.pop();
}, {
"./Bean/UserData": "UserData",
"./Common/StorageUtil": "StorageUtil"
} ],
ContentAdapter: [ function(e, t) {
"use strict";
cc._RF.push(t, "0ee51ZStVlCXbYk6CvMXgU/", "ContentAdapter");
cc.Class({
extends: cc.Component,
properties: {
isLabel: !1,
isWidgetTop: !1,
isWidgetLeft: !1,
isWidgetRight: !1,
isWidgetBottom: !1,
isVerticalCenter: !1,
isHorizontalCenter: !1,
isScale: !1
},
onLoad: function() {},
start: function() {}
});
cc._RF.pop();
}, {} ],
DialogAnimation: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "8faaf3RJUtNdYpJfoxsfioe", "DialogAnimation");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.originScale = 1;
t.needUpdate = !1;
return t;
}
t.prototype.start = function() {};
t.prototype.onEnable = function() {
this.originScale = this.node.scale;
this.needUpdate = !0;
this.node.scale = 0;
};
t.prototype.lateUpdate = function() {
if (this.needUpdate) {
this.needUpdate = !1;
this.show(!0);
}
};
t.prototype.show = function() {
var e = this.originScale;
this.node.y = this.node.getParent().height / 2 - this.node.getComponent(cc.Widget).top * this.node.getParent().height - this.node.height / 2;
var t = cc.scaleTo(1, e);
t.easing(cc.easeElasticOut(1));
this.node.stopAllActions();
this.node.runAction(t);
};
t.prototype.showRank = function() {
var e = this.node.scale, t = cc.winSize.height;
this.node.scale = 0;
this.node.y = t / 2 - .126 * t - this.node.height / 2;
var i = cc.scaleTo(1, e, e);
i.easing(cc.easeElasticOut(1));
var n = cc.sequence(i, cc.callFunc(function() {
this.node.y = t / 2 - .126 * t - this.node.height / 2;
}.bind(this)));
this.node.stopAllActions();
this.node.runAction(n);
};
return o([ s ], t);
}(cc.Component));
i.default = c;
cc._RF.pop();
}, {} ],
DualBlur: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "9dc80rabGFIHJ6+8yAPdf0q", "DualBlur");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.spriteSrc = null;
t.spriteDst = null;
t.lbOffset = null;
t.lbIteration = null;
t.lbScale = null;
t.materialDown = null;
t.materialUp = null;
t.renderTexture = null;
t._offset = 8;
t._iteration = 3;
t._scale = 2;
return t;
}
t.prototype.start = function() {
this.blur(this._offset, this._iteration, this._scale);
};
t.prototype.init = function() {
this.blur(this._offset, this._iteration, this._scale);
};
t.prototype.blur = function(e, t, i) {
var n, r;
void 0 === i && (i = .5);
var o = this.spriteDst, a = this.spriteSrc.node;
this.materialDown;
this.materialDown.setProperty("resolution", cc.v2(a.width, a.height));
this.materialDown.setProperty("offset", e);
this.materialUp.setProperty("resolution", cc.v2(a.width, a.height));
this.materialUp.setProperty("offset", e);
var s = new cc.RenderTexture(), c = new cc.RenderTexture();
this.getRenderTexture(a, c);
for (var l = [], h = c.width, u = c.height, f = 0; f < t; f++) {
l.push([ h, u ]);
c = (n = [ s, c ])[0], s = n[1];
h = Math.max(h * i, 1), u = Math.max(u * i, 1);
this.renderWithMaterial(s, c, this.materialDown, cc.size(h, u));
}
for (f = t - 1; f >= 0; f--) {
c = (r = [ s, c ])[0], s = r[1];
this.renderWithMaterial(s, c, this.materialUp, cc.size(l[f][0], l[f][1]));
}
this.renderTexture = c;
o.spriteFrame = new cc.SpriteFrame(this.renderTexture);
o.spriteFrame.setFlipY(!0);
cc.log(o.node.height);
s.destroy();
};
t.prototype.getRenderTexture = function(e, t) {
if (!cc.isValid(e)) return null;
t && t instanceof cc.RenderTexture || (t = new cc.RenderTexture());
var i = Math.floor(e.width), n = Math.floor(e.height);
t.initWithSize(i, n);
var r = new cc.Node();
r.parent = e;
var o = r.addComponent(cc.Camera);
o.clearFlags |= cc.Camera.ClearFlags.COLOR;
o.backgroundColor = cc.color(0, 0, 0, 0);
o.zoomRatio = 1 / e.scale;
o.zoomRatio = cc.winSize.height / n / e.scale;
o.targetTexture = t;
o.render(e);
r.destroy();
return t;
};
t.prototype.renderWithMaterial = function(e, t, i, n) {
if (t instanceof cc.Material) {
i = t;
t = new cc.RenderTexture();
}
var r = new cc.Node();
r.setParent(cc.Canvas.instance.node);
var o = r.addComponent(cc.Sprite);
o.sizeMode = cc.Sprite.SizeMode.RAW;
o.trim = !1;
o.spriteFrame = new cc.SpriteFrame(e);
var a = null != n ? n : {
width: e.width,
height: e.height
}, s = a.width, c = a.height;
t.initWithSize(s, c);
i instanceof cc.Material && o.setMaterial(0, i);
var l = new cc.Node();
l.setParent(r);
var h = l.addComponent(cc.Camera);
h.clearFlags |= cc.Camera.ClearFlags.COLOR;
h.backgroundColor = cc.color(0, 0, 0, 0);
h.zoomRatio = cc.winSize.height / e.height;
h.targetTexture = t;
h.render(r);
l.destroy();
r.destroy();
return t;
};
t.prototype.onSliderOffsetEvent = function(e) {
var t = Math.round(1e3 * e.progress) / 100;
if (this._offset != t) {
this._offset = t;
this.lbOffset.string = "offset:" + this._offset;
cc.log("offset: " + this._offset);
this.blur(this._offset, this._iteration, this._scale);
}
};
t.prototype.onSliderIterationEvent = function(e) {
var t = Math.round(5 * e.progress);
if (this._iteration != t) {
this._iteration = t;
this.lbIteration.string = "iteration:" + this._iteration;
cc.log("iteration: " + this._iteration);
this.blur(this._offset, this._iteration, this._scale);
}
};
t.prototype.onSliderScaleEvent = function(e) {
var t = Math.max(Math.round(10 * e.progress) / 10, .1);
if (this._scale != t) {
this._scale = t;
this.lbScale.string = "scale:" + this._scale;
cc.log("scale: " + this._scale);
this.blur(this._offset, this._iteration, this._scale);
}
};
o([ c(cc.Sprite) ], t.prototype, "spriteSrc", void 0);
o([ c(cc.Sprite) ], t.prototype, "spriteDst", void 0);
o([ c(cc.Label) ], t.prototype, "lbOffset", void 0);
o([ c(cc.Label) ], t.prototype, "lbIteration", void 0);
o([ c(cc.Label) ], t.prototype, "lbScale", void 0);
o([ c(cc.Material) ], t.prototype, "materialDown", void 0);
o([ c(cc.Material) ], t.prototype, "materialUp", void 0);
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {} ],
EditorAsset: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "80b13KtfMtFG4kyhQvKJ2Gz", "EditorAsset");
Object.defineProperty(i, "__esModule", {
value: !0
});
var n = function() {
function e() {}
e.load = function() {
return new Promise(function(e) {
e(null);
cc.warn("[EditorAsset]", "该函数只在编辑器环境内有效！");
});
};
return e;
}();
i.default = n;
cc._RF.pop();
}, {} ],
Facade: [ function(e, t) {
"use strict";
cc._RF.push(t, "756dcTaSfROyLUbYIQqPrgF", "Facade");
cc.Class({
extends: cc.Component,
properties: {},
onLoad: function() {
this.node.reEnter = !1;
this.node.VERSION = 1001;
this.node.isShareOut = !1;
this.node.lastSessionid = "0";
this.node.canShowShareGroup = !0;
this.node.FloatOrder = 11e3;
this.node.PopOrder = 9999;
this.node.isEnteredGame = !1;
this.node.SAVE_MODE = !1;
this.node.FOR_DEVELOP = !1;
this.node.token = "";
this.node.wxAppId = "wxfd9901848f148530";
}
});
cc._RF.pop();
}, {} ],
FluxayTexture: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "9233b7gPhJPzpAtGrjzD3IQ", "FluxayTexture");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t._speed = 1;
t._time = 0;
return t;
}
t.prototype.onLoad = function() {
this._material = this.node.getComponent(cc.Sprite).getMaterial(0);
};
t.prototype.update = function(e) {
this._time > 2 && (this._time = 0);
this._material.setProperty("u_time", this._time);
this._time += e * this._speed;
};
return o([ s ], t);
}(cc.Component));
i.default = c;
cc._RF.pop();
}, {} ],
GameControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "61599VN3nBD1YTBlb/qp3hy", "GameControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("./native/JsBridge"), s = e("./native/CallNative"), c = cc._decorator, l = c.ccclass, h = c.property, u = e("./LevelUtils"), f = e("./Bean/UserData"), d = e("./LevelData"), p = e("./Bean/PassLevelBean"), m = e("./Common/StorageUtil"), g = e("./GlobalConst"), y = e("./SettleUtil"), b = e("./Common/WXCommon"), v = e("./SubDomainControl"), _ = e("./CoinsUtil"), w = e("../i18n/i18nMgr"), C = e("./DualBlur/DualBlur"), S = e("./base/BaseLayer"), A = window.wx, M = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.label = null;
t.m_WinLayout = null;
t.m_CatFace = null;
t.m_InnerLayout = null;
t.settleLevelLabel = null;
t.settlePercentLabel = null;
t.settlePlayerLabel = null;
t.countDownNode = null;
t.countDownNodeBg = null;
t.countDownLabel = null;
t.limitFailDialog = null;
t.mainCanvas = null;
t.m_CatTail = null;
t.m_NextPlayer = null;
t.m_NextPlayerMask = null;
t.m_ChallengeBg = null;
t.m_LimitBg = null;
t.m_Tool = null;
t.m_ToolBg = null;
t.m_GoldLayout = null;
t.m_CoinLabel = null;
t.m_PressureProgress = null;
t.m_PressureLabel = null;
t.m_AwardView = null;
t.m_DialogRank = null;
t.m_BottomView = null;
t.m_Bg = null;
t.m_DoubleSettleView = null;
t.m_SingleSettleView = null;
t.m_AnswerSolvedRewardView = null;
t.getWxInfoDialog = null;
t.coinTargetNode = null;
t.settilePassAnim = null;
t.notifyBtn = null;
t.awardBtn = null;
t.awardBtnBg = null;
t.awardBtnMask = null;
t.awardCountDownLabel = null;
t.limitBackDialog = null;
t.fingerGuide = null;
t.turnTablePrefab = null;
t.turnTableNode = null;
t.shareLabel = null;
t.shareIcon = null;
t.nextLabel = null;
t.videoIcon = null;
t.turnIcon = null;
t.goldIconLabel = null;
t.notifyCostLabel = null;
t.notifyCostVedio = null;
t.gold50Icon = null;
t.pressureBtn = null;
t.pressureCheckBox = null;
t.answerSolvedCheckBox = null;
t.gameGridPreb = null;
t.gamePagePrefab = null;
t.doubleLayout = null;
t.videoNextBtn = null;
t.giftAnim = null;
t.BgSprite = null;
t.gamePageNode = null;
t.poolMng = null;
t.m_RowNum = 0;
t.m_ColNum = 0;
t.m_BoxWidth = 0;
t.m_BoxHeight = 0;
t.m_MaxBoxWidth = 120;
t.m_BoxArray = new Array();
t.m_LevelConfigArray = new Array();
t.m_PathArray = new Array();
t.m_AnswerPathArray = new Array();
t.m_StartTouchMode = !1;
t.m_TotalItemCount = 0;
t.levelUtils = new u.default();
t.storageUtil = new m.default();
t.coinsUtil = new _.default();
t.playAnimTime = .08;
t.notifyStep = 4;
t.currentStep = 1;
t.currentLevel = 1;
t.globalConst = new g.default();
t.costTime = 0;
t.helpTime = 0;
t.lastRestTime = t.globalConst.countDownTime;
t.isLimitGame = !1;
t.settleUtil = new y.default();
t.limitLevel = 1;
t.limitLevelArray = new Array();
t.startCountDown = !1;
t.awardStep = 3;
t.gold = 0;
t.wxCommon = new b.default();
t.isHelperGame = !1;
t.isAnswerGame = !1;
t.isShareNotify = !1;
t.requestHelperInfo = null;
t.subDomainControl = new v.default();
t.doubleCheckBoxChecked = !1;
t.bodyNodeColor = "";
t.awardClickTime = 0;
t.awardScheduleId = 0;
t.catAudioScheduleID = 0;
t.boxArrayLoaded = !1;
t.theme = 1;
t.chapter = 1;
t.shareLabelIndex = -1;
t.iconAd = null;
t.gameAdshow = !0;
t.notifyDialogShow = !1;
t.notifyDialogPref = null;
t.notifyDialogNode = null;
t.settlePopupPriorityArray = new Array();
t.settleLevel = 0;
t.passData = null;
t.nextLevel = -1;
return t;
}
t.prototype.onLoad = function() {
this.originHeight = .616 * cc.winSize.height;
this.poolMng = this.node.getComponent("PoolMng");
this.poolMng.init();
this.node.on(cc.Node.EventType.TOUCH_START, this.onTouchEvent, this);
this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouchEvent, this);
this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEvent, this);
this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchEvent, this);
a.JsBridge.isMobile() && (cc.CallNative = s.CallNative);
};
t.prototype.initBoxArray = function() {
this.helpTime = 0;
this.node.parent.getChildByName("Tools").opacity = 255;
this.node.parent.getChildByName("Topbar").opacity = 255;
this.m_CatFace.opacity = 255;
this.node.opacity = 255;
this.node.parent.getChildByName("body").opacity = 255;
var e = this.node.getComponent(cc.Layout);
this.node.removeAllChildren();
this.node.height = this.originHeight;
this.m_BoxWidth = this.node.width / (1.1 * this.m_ColNum + .9);
this.m_BoxWidth = Math.min(this.m_BoxWidth, this.node.height / (1.1 * this.m_RowNum + .9));
this.m_BoxWidth = Math.min(this.m_BoxWidth, this.m_MaxBoxWidth);
e.spacingX = .1 * this.m_BoxWidth;
e.spacingY = e.spacingX;
e.paddingLeft = (this.node.width - this.m_BoxWidth * this.m_ColNum - (this.m_ColNum - 1) * e.spacingX) / 2;
e.paddingRight = e.paddingLeft;
this.m_BoxHeight = this.m_BoxWidth;
for (var t = 0; t < this.m_RowNum; t++) {
this.m_BoxArray.push([]);
for (var i = 0; i < this.m_ColNum; i++) {
var n = this.poolMng.spawnBox(), r = (this.m_BoxWidth - 1) / 95;
n.width = this.m_BoxWidth - 1;
n.height = this.m_BoxHeight - 1;
this.m_BoxArray[t][i] = n;
this.node.addChild(n);
var o = n.getComponent("Box");
o.setValue(this.m_LevelConfigArray[t][i], r);
if (-1 == this.m_LevelConfigArray[t][i]) n.opacity = 0; else {
this.m_TotalItemCount++;
1 == this.m_LevelConfigArray[t][i] && this.m_PathArray.push(this.m_BoxArray[t][i]);
n.opacity = 255;
}
o.x_Index = t;
o.y_Index = i;
}
}
e.updateLayout();
var a = this.m_BoxWidth / this.m_CatFace.width;
this.m_CatFace.setScale(a);
this.m_CatFace.zIndex = this.m_TotalItemCount;
this.drawCatFace(this.m_PathArray[0], this.m_PathArray[0].getComponent("Box").m_Oritation);
if (!this.isLimitGame) {
this.storageUtil.saveData("playLevel", this.theme);
this.scheduleOnce(function() {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playGameHelpAudio();
}, .2);
}
this.boxArrayLoaded = !0;
var s = cc.find("Canvas").getComponent("MainControl");
s.Membership ? this.notifyCostVedio.active = !1 : s.showBanner(s.PlayGameBanner, function() {}.bind(this));
this.mainCanvas.getComponent("MainControl").getVolumeControl().playBgMusic(this.theme - 1);
};
t.prototype.initTestLevel = function() {
for (var e = 0; e < this.m_RowNum; e++) {
this.m_LevelConfigArray.push([]);
for (var t = 0; t < this.m_ColNum; t++) this.m_LevelConfigArray[e][t] = -1;
}
this.m_LevelConfigArray[0][0] = 1;
this.m_LevelConfigArray[0][1] = 0;
this.m_LevelConfigArray[0][2] = 0;
this.m_LevelConfigArray[1][1] = 0;
this.m_LevelConfigArray[1][2] = 0;
this.m_LevelConfigArray[2][1] = 0;
};
t.prototype.hasAroundNode = function(e, t) {
var i;
if (t - 1 >= 0 && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[e][t - 1]) {
this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
i = "Right";
this.m_BoxArray[e][t].getComponent("Box").setOritation(i);
this.drawNextNode(e, t, this.m_BoxArray[e][t - 1], i);
} else if (e - 1 >= 0 && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[e - 1][t]) {
this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
i = "Bottom";
this.m_BoxArray[e][t].getComponent("Box").setOritation(i);
this.drawNextNode(e, t, this.m_BoxArray[e - 1][t], i);
} else if (t + 1 < this.m_ColNum && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[e][t + 1]) {
this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
i = "Left";
this.m_BoxArray[e][t].getComponent("Box").setOritation(i);
this.drawNextNode(e, t, this.m_BoxArray[e][t + 1], i);
} else if (e + 1 < this.m_RowNum && this.m_PathArray[this.m_PathArray.length - 1] == this.m_BoxArray[e + 1][t]) {
this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
i = "Top";
this.m_BoxArray[e][t].getComponent("Box").setOritation(i);
this.drawNextNode(e, t, this.m_BoxArray[e + 1][t], i);
} else this.m_BoxArray[e][t].getComponent("Box").setOritation("null");
};
t.prototype.onTouchEvent = function(e) {
1 == this.fingerGuide.active && (this.fingerGuide.active = !1);
if (0 != this.boxArrayLoaded) {
var t = e.getLocationX(), i = e.getLocationY(), n = this.node.convertToNodeSpaceAR(cc.v2(t, i)), r = this.node.getComponent(cc.Layout), o = Math.floor((n.x - r.paddingLeft + this.node.width / 2) / (this.m_BoxWidth + r.spacingX)), a = Math.floor((this.node.height / 2 - n.y - r.paddingTop) / (this.m_BoxHeight + r.spacingY));
if (null != this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box")) {
var s = this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box");
"touchstart" == e.type && s.y_Index == o && s.x_Index == a && this.mainCanvas.getComponent("MainControl").getVolumeControl().playStartAudio();
o < 0 || o >= this.m_ColNum ? this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]) : a < 0 || a >= this.m_RowNum ? this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]) : this.m_TotalItemCount <= this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box").value || this.m_BoxArray[a][o].getBoundingBox().contains(n) && (cc.Node.EventType.TOUCH_END == e.getType() || cc.Node.EventType.TOUCH_CANCEL == e.getType() ? this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]) : this.m_BoxArray[a][o].getComponent("Box").processTouch(e));
}
}
};
t.prototype.popPathArrayList = function(e) {
var t = this.m_PathArray.indexOf(e);
if (!(t < 0 || t >= this.m_PathArray.length - 1)) {
this.drawCatFace(this.m_PathArray[this.m_PathArray.length - 1], this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box").m_Oritation);
var i = this.m_PathArray.length - 1 - t, n = this.playAnimTime;
i * this.playAnimTime > .5 && (n = .5 / i);
this.moveBackward(this.m_PathArray[this.m_PathArray.length - 1], this.m_PathArray[this.m_PathArray.length - 2], t, n);
}
};
t.prototype.onTouchEventStart = function(e) {
this.onTouchEvent(e);
};
t.prototype.onTouchEventMove = function(e) {
this.onTouchEvent(e);
};
t.prototype.drawNextNode = function(e, t, i, n) {
var r = i.getComponent("Box").value;
this.m_BoxArray[e][t].getComponent("Box").value = r + 1;
this.m_PathArray.push(this.m_BoxArray[e][t]);
this.drawCatFace(i, n);
this.moveForward(i, this.m_BoxArray[e][t], n);
this.winOrNot();
};
t.prototype.winOrNot = function() {
if (!(this.m_TotalItemCount > this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box").value)) {
this.mainCanvas.getComponent("MainControl").getVolumeControl().stopMusic();
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playGameEndAudio();
this.scheduleOnce(function() {
if (this.isLimitGame) {
this.startCountDown = !1;
this.currentLevel++;
this.calculateLimitLevel();
this.passNextLevel();
} else {
this.isAnswerGame = !1;
if (!1 === this.m_WinLayout.active) {
var e = cc.find("Canvas").getComponent("MainControl");
e.hideBanner1();
e.Membership ? this.m_AwardView.active = !1 : this.m_AwardView.active = !0;
if (2 != this.storageUtil.getData("unLockLevel" + this.currentLevel)) {
this.storageUtil.saveData("unLockLevel" + this.currentLevel, 2);
var t = this.storageUtil.getData("passCount");
t ? t++ : t = 1;
this.storageUtil.saveData("passCount", t);
var i = t / (this.globalConst.levelCount * this.globalConst.themeCount * this.globalConst.chapterCount) * 100;
this.settlePlayerLabel.string = w.i18nMgr._getLabel("txt_118").replace("@", Math.ceil(i) + "%");
}
this.m_WinLayout.zIndex = this.m_CatFace.zIndex + 1;
this.m_NextPlayer.active = !1;
this.settleLevel = this.currentLevel;
this.endLevelEvent();
this.showSettleData();
this.levelData = new d.default();
this.saveLevelData();
this.currentLevel++;
if (this.userData.gameLevel < this.currentLevel) {
this.userData.gameLevel = this.currentLevel;
this.userData.gold = this.gold;
this.storageUtil.saveData("userData", this.userData);
}
this.passData = this.storageUtil.getData("passData");
null == this.passData && (this.passData = new p.default());
if (this.passData.date && this.passData.date === new Date().getMonth() + "_" + new Date().getDate()) {
this.passData.level = this.passData.level + 1;
this.storageUtil.saveData("passData", this.passData);
} else {
this.passData.level = 0;
this.passData.date = new Date().getMonth() + "_" + new Date().getDate();
this.storageUtil.saveData("passData", this.passData);
}
}
}
}.bind(this), .2);
}
};
t.prototype.onSettleShareClick = function() {
this.wxCommon.shareToFrends("", "", "", "闯关赛结算分享");
};
t.prototype.onSettleShare2Click = function() {
this.wxCommon.shareToFrends("", "", "", "闯关赛结算炫耀");
};
t.prototype.onSettleShare1Click = function() {
this.wxCommon.shareToFrends("", "", "", "闯关赛结算发到群");
};
t.prototype.onPassNextLevelClick = function() {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.isLimitGame || (this.currentLevel - 1 == this.globalConst.chapterCount * this.globalConst.levelCount * this.globalConst.themeCount ? this.backBtnClick() : this.m_PressureProgress.active ? this.onVideoNextClick() : this.passNextLevel());
};
t.prototype.saveBtn = function() {
var e = this.mainCanvas.getComponent("MainControl");
e.volumeControl.getComponent("AudioController").playClickButtonAudio();
e.Membership ? this.successAd() : e.loadVideoAd(e.download);
};
t.prototype.successAd = function() {
var e = new d.default().currentUrl + "/bg/id_" + (this.chapter + (this.theme - 1) * this.globalConst.chapterCount) + "/level_bg_" + this.level + ".jpg";
console.log(e, "bgPath+++++++++");
a.JsBridge.isMobile() && a.JsBridge.callWithCallback(a.JsBridge.SAVE, {
type: "img",
url: e
}, function() {});
};
t.prototype.passNextLevel = function() {
this.boxArrayLoaded = !1;
this.mainCanvas.getComponent("MainControl").hideBanner();
this.m_DialogRank.active = !1;
this.m_WinLayout.active = !1;
this.isLimitGame && 1 == this.currentLevel || (this.startCountDown = !0);
this.isLimitGame || this.turnTableNode && this.turnTableNode.getComponent("Turntable").close();
this.currentStep = 1;
this.costTime = 0;
this.notifyDialogShow = !1;
this.resetLevelInfo();
if (this.isLimitGame) {
this.initLimitBg();
this.initLevel(this.limitLevel);
} else {
this.initChallengeBg();
clearInterval(this.awardScheduleId);
this.scheduleAwardBtn();
this.initLevel(this.currentLevel);
this.initLevelEvent();
}
this.scheduleCatAudio();
};
t.prototype.drawCatFace = function(e, t) {
this.m_CatFace.active = !0;
var i = this.node.convertToWorldSpaceAR(e.getPosition()), n = this.node.getParent().convertToNodeSpaceAR(i);
this.m_CatFace.setPosition(n);
this.m_PathArray.length > 1 ? "null" == this.m_PathArray[1].getComponent("Box").m_Oritation ? this.drawCatTail(this.m_PathArray[0], t) : this.drawCatTail(this.m_PathArray[0], this.m_PathArray[1].getComponent("Box").m_Oritation) : this.drawCatTail(this.m_PathArray[0], this.m_PathArray[0].getComponent("Box").m_Oritation);
};
t.prototype.moveForward = function(e, t, i) {
var n = 2 * t.width + this.node.getComponent(cc.Layout).spacingX;
e.getComponent("Box").playForward(i, this.playAnimTime, n);
var r = this.node.convertToWorldSpaceAR(t.getPosition()), o = this.node.getParent().convertToNodeSpaceAR(r);
this.mainCanvas.getComponent("MainControl").getVolumeControl().playMoveAudio();
this.m_CatFace.stopAllActions();
this.m_CatFace.runAction(cc.moveTo(this.playAnimTime, o));
};
t.prototype.moveBackward = function(e, t, i, n) {
var r = e.getComponent("Box").m_Oritation, o = this.node.convertToWorldSpaceAR(t.getPosition()), a = this.node.getParent().convertToNodeSpaceAR(o);
this.cancelAroundBox(this.m_PathArray[this.m_PathArray.length - 1]);
this.mainCanvas.getComponent("MainControl").getVolumeControl().playMoveEditAudio();
"Right" == r ? a.x = a.x + t.width / 2 : "Left" == r ? a.x = a.x - t.width / 2 : a.y = "Top" == r ? a.y + t.height / 2 : a.y - t.height / 2;
this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box").reset(!1);
this.m_PathArray.splice(this.m_PathArray.length - 1, 1);
var s = cc.spawn(cc.callFunc(function() {
this.drawCatFace(e, r);
t.getComponent("Box").playBackward(n, r);
}.bind(this)), cc.moveTo(n, a)), c = cc.sequence(s, cc.callFunc(function() {
this.drawCatFace(t, t.getComponent("Box").m_Oritation);
this.m_PathArray.length - 2 >= i && this.moveBackward(this.m_PathArray[this.m_PathArray.length - 1], this.m_PathArray[this.m_PathArray.length - 2], i, n);
}.bind(this)));
this.m_CatFace.stopAllActions();
this.m_CatFace.runAction(c);
};
t.prototype.initLevel = function(e) {
this.userData = this.storageUtil.getData("userData");
null == this.userData && (this.userData = new f.default());
if (!this.isLimitGame) {
this.gold = this.userData.gold;
this.updateCoinsLabel(this.gold);
this.currentLevel >= this.userData.gameLevel && this.saveDataToWx(this.currentLevel);
}
var t = Math.ceil(e / this.globalConst.chapterCount / this.globalConst.levelCount), i = this.currentLevel - (t - 1) * this.globalConst.chapterCount * this.globalConst.levelCount, n = Math.ceil(i / this.globalConst.levelCount);
cc.log(t, i, n, "chapter++++");
var r = n;
this.levelUtils.loadLevelJson(t, r, function(i) {
var n = e - (t - 1) * this.globalConst.chapterCount * this.globalConst.levelCount - (r - 1) * this.globalConst.levelCount - 1;
console.log("levelData level jason  = ", i);
console.log("levelData levelIndex = ", n);
var o = this.levelUtils.hex_to_levelData(this.levelUtils.getBinString(i[n]));
console.log("levelData", o);
this.initRowColNum(o);
for (var a = 0; a < this.m_RowNum; a++) {
this.m_LevelConfigArray.push([]);
for (var s = 0; s < this.m_ColNum; s++) this.m_LevelConfigArray[a][s] = -1;
}
for (a = 0; a < o.length; a++) {
var c = o[a];
this.m_AnswerPathArray.push(c);
this.m_LevelConfigArray[c[1]][c[0]] = 0 == a ? 1 : 0;
}
o[0];
this.initBoxArray();
this.label.string = w.i18nMgr._getLabel("txt_06").replace("&", this.currentLevel);
this.isAnswerGame && !this.isLimitGame && this.onNotifyClick(null);
!this.isLimitGame && this.needGuideView() && this.showGuideAnim();
}.bind(this));
};
t.prototype.initRowColNum = function(e) {
this.m_RowNum = 0;
this.m_ColNum = 0;
for (var t = 0; t < e.length; t++) {
var i = e[t];
i[1] > this.m_RowNum && (this.m_RowNum = i[1]);
i[0] > this.m_ColNum && (this.m_ColNum = i[0]);
}
this.m_RowNum += 1;
this.m_ColNum += 1;
};
t.prototype.onNotifyBtnClick = function() {
if (this.currentStep != this.m_AnswerPathArray.length) {
var e = this.mainCanvas.getComponent("MainControl");
if (e.Membership) {
this.startCountDown = !1;
this.onNotifyClick();
} else {
this.startCountDown = !1;
e.loadVideoAd(e.Notify);
}
}
};
t.prototype.onNotifyClick = function(e) {
var t, i = this.notifyStep;
e && (i = e);
console.log("视频看完提示");
this.isLimitGame && (this.startCountDown = !1);
this.isAnswerGame ? i = this.m_AnswerPathArray.length : this.isShareNotify && (this.isShareNotify = !1);
this.currentStep + i > this.m_AnswerPathArray.length && (i = this.m_AnswerPathArray.length - this.currentStep);
t = 0 == this.currentStep ? this.m_AnswerPathArray[this.currentStep] : this.m_AnswerPathArray[this.currentStep - 1];
this.popPathArrayList(this.m_BoxArray[t[1]][t[0]]);
for (var n = 1, r = function(e) {
var t = o.m_AnswerPathArray[e], i = o.m_BoxArray[t[1]][t[0]], r = "Bottom";
if (e > 0) {
var a = o.m_AnswerPathArray[e - 1], s = t[1] - a[1], c = t[0] - a[0];
s < 0 && (r = "Top");
s > 0 && (r = "Bottom");
if (0 == s) {
c < 0 && (r = "Left");
c > 0 && (r = "Right");
}
o.scheduleOnce(function() {
var t = e == this.m_AnswerPathArray.length - 1;
i.getComponent("Box").notifyClicked(r, t);
}.bind(o), .1 * e);
}
n++;
}, o = this, a = this.currentStep; a < this.currentStep + i; a++) r(a);
this.scheduleOnce(function() {
this.notifyBtn.interactable = !0;
}.bind(this), .1 * n);
this.currentStep = this.currentStep + i;
};
t.prototype.resetLevelInfo = function() {
this.m_TotalItemCount = 0;
for (var e = 0; e < this.m_RowNum; e++) for (var t = 0; t < this.m_ColNum; t++) {
this.m_BoxArray[e][t].getComponent("Box").reset(!0);
this.m_BoxArray[e][t].getComponent("Box").removeBodyNode();
this.poolMng.despwanBox(0, this.m_BoxArray[e][t]);
}
this.m_BoxArray.splice(0, this.m_BoxArray.length);
this.m_LevelConfigArray.splice(0, this.m_LevelConfigArray.length);
this.m_PathArray.splice(0, this.m_PathArray.length);
this.m_AnswerPathArray.splice(0, this.m_AnswerPathArray.length);
};
t.prototype.notifyAroundBox = function(e) {
var t = e.getComponent("Box"), i = t.x_Index, n = t.y_Index, r = [];
n - 1 >= 0 && 0 == this.m_BoxArray[i][n - 1].getComponent("Box").value && r.push(this.m_BoxArray[i][n - 1]);
i - 1 >= 0 && 0 == this.m_BoxArray[i - 1][n].getComponent("Box").value && r.push(this.m_BoxArray[i - 1][n]);
n + 1 < this.m_ColNum && 0 == this.m_BoxArray[i][n + 1].getComponent("Box").value && r.push(this.m_BoxArray[i][n + 1]);
i + 1 < this.m_RowNum && 0 == this.m_BoxArray[i + 1][n].getComponent("Box").value && r.push(this.m_BoxArray[i + 1][n]);
return r;
};
t.prototype.notifyAroundBox1 = function(e) {
var t = e.getComponent("Box"), i = t.x_Index, n = t.y_Index, r = [];
n - 1 >= 0 && (0 == this.m_BoxArray[i][n - 1].getComponent("Box").value || 2 == this.m_BoxArray[i][n - 1].getComponent("Box").value) && r.push(this.m_BoxArray[i][n - 1]);
i - 1 >= 0 && (0 == this.m_BoxArray[i - 1][n].getComponent("Box").value || 2 == this.m_BoxArray[i - 1][n].getComponent("Box").value) && r.push(this.m_BoxArray[i - 1][n]);
n + 1 < this.m_ColNum && (0 == this.m_BoxArray[i][n + 1].getComponent("Box").value || 2 == this.m_BoxArray[i][n + 1].getComponent("Box").value) && r.push(this.m_BoxArray[i][n + 1]);
i + 1 < this.m_RowNum && (0 == this.m_BoxArray[i + 1][n].getComponent("Box").value || 2 == this.m_BoxArray[i + 1][n].getComponent("Box").value) && r.push(this.m_BoxArray[i + 1][n]);
return r;
};
t.prototype.cancelAroundBox = function(e) {
var t = e.getComponent("Box"), i = t.x_Index, n = t.y_Index;
n - 1 >= 0 && this.m_BoxArray[i][n - 1].getComponent("Box").cancelNotifyAnim();
i - 1 >= 0 && this.m_BoxArray[i - 1][n].getComponent("Box").cancelNotifyAnim();
n + 1 < this.m_ColNum && this.m_BoxArray[i][n + 1].getComponent("Box").cancelNotifyAnim();
i + 1 < this.m_RowNum && this.m_BoxArray[i + 1][n].getComponent("Box").cancelNotifyAnim();
};
t.prototype.resetClick = function() {
this.startCountDown = !0;
this.popPathArrayList(this.m_PathArray[0]);
};
t.prototype.onRestClick = function() {
A.aldSendEvent("闯关赛重玩点击", {
"关卡": this.currentLevel
});
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
var e = this.mainCanvas.getComponent("MainControl");
e.loadVideoAd(e.Reset);
this.startCountDown = !1;
};
t.prototype.saveLevelData = function() {
this.levelData.currentColor = this.bodyNodeColor;
this.levelData.perfect = !0;
this.levelData.colNum = this.m_ColNum;
this.levelData.rowNum = this.m_RowNum;
this.levelData.itemWidth = this.m_BoxWidth;
for (var e = 0; e < this.m_PathArray.length; e++) {
this.levelData.currentPathIndex.push([]);
this.levelData.currentPathIndex[e][0] = this.m_PathArray[e].getComponent("Box").x_Index;
this.levelData.currentPathIndex[e][1] = this.m_PathArray[e].getComponent("Box").y_Index;
}
this.storageUtil.saveData("levelData" + this.currentLevel, this.levelData);
};
t.prototype.playSelectedLevel = function(e, t) {
this.isLimitGame = !1;
this.isHelperGame = t;
this.countDownLabel.node.active = !1;
this.m_InnerLayout.active = !1;
this.currentLevel = e;
A.aldSendEvent("闯关赛界面曝光", {
"初始关卡": this.currentLevel
});
this.passNextLevel();
};
t.prototype.backBtnClick = function() {
this.mainCanvas.getComponent("MainControl").getVolumeControl().stopMusic();
var e = cc.find("Canvas").getComponent("MainControl");
e.hideBanner1();
e.volumeControl.getComponent("AudioController").playClickButtonAudio();
clearInterval(this.awardScheduleId);
clearInterval(this.catAudioScheduleID);
this.isLimitGame ? this.showLimitBackDialog() : this.back2LevelSelect();
};
t.prototype.back2LevelSelect = function() {
this.startCountDown = !1;
this.m_NextPlayer.active = !1;
this.m_InnerLayout.active = !0;
this.m_InnerLayout.getChildByName("GridLayout").getComponent("InnerLevelControl").initLevelData(this.theme, this.chapter);
};
t.prototype.update = function(e) {
if (this.isLimitGame) {
if (this.startCountDown) {
this.costTime = this.costTime + e;
var t = Math.ceil(this.globalConst.countDownTime - this.costTime);
if (t <= 0) {
t = 0;
this.limitFailDialog.active = !0;
this.limitFailDialog.zIndex = this.m_CatFace.zIndex + 1;
this.mainCanvas.getComponent("MainControl").Membership && (this.limitBackDialog.active = !1);
this.limitFailDialog.getComponent("LimitFailControl").showDialog(this.currentLevel);
A.aldSendEvent("挑战赛失败曝光", {
"关卡": this.currentLevel
});
this.startCountDown = !1;
this.lastRestTime = this.globalConst.countDownTime;
}
if (t < 10) {
if (t != this.lastRestTime) {
this.lastRestTime = t;
this.mainCanvas.getComponent("MainControl").getVolumeControl().playCountDownAudio();
}
this.countDownLabel.node.color = new cc.Color().fromHEX("#DD0606");
this.countDownLabel.string = "00:0" + t;
} else {
this.countDownLabel.node.color = new cc.Color().fromHEX("#FFFFFF");
this.countDownLabel.string = "00:" + t;
}
}
} else if (this.startCountDown) {
this.costTime = this.costTime + e;
this.costTime >= this.globalConst.costTimeThre && !this.notifyDialogShow && this.m_TotalItemCount > this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box").value && !this.isAnswerGame && this.showNotifyGuaidDialog();
}
if (this.startCountDown) {
this.helpTime = this.helpTime + e;
if (Math.floor(this.helpTime) == this.globalConst.helpMe && !this.notifyDialogShow && this.m_TotalItemCount > this.m_PathArray[this.m_PathArray.length - 1].getComponent("Box").value && !this.isAnswerGame) {
this.helpTime = 0;
cc.log(123321);
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playGameHelpAudio();
}
}
};
t.prototype.showSettleData = function() {
this.currentLevel >= this.userData.gameLevel && this.saveDataToWx(this.currentLevel);
var e = this.currentLevel % this.awardStep, t = this.currentLevel % this.globalConst.levelCount;
console.log("awardPercent = " + e);
this.isAnswerGame ? this.requestHelperInfo.uid == this.userData.uid && this.settleDialogControl(e, t) : this.settleDialogControl(e, t);
};
t.prototype.playLimitGame = function() {
this.isAnswerGame = !1;
this.isLimitGame = !0;
this.currentLevel = 1;
this.startCountDown = !1;
this.countDownLabel.string = "00:30";
this.resetLimitLevelArray();
this.calculateLimitLevel();
this.passNextLevel();
this.countDownNodeBg.active = !0;
this.countDownLabel.node.active = !0;
this.countDownNodeBg.zIndex = 999;
var e = 0;
this.schedule(function() {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playCountDownAudio();
if (0 == e) {
this.countDownNode.getComponent(dragonBones.ArmatureDisplay).playAnimation("newAnimation", 1);
this.scheduleOnce(function() {
this.countDownNodeBg.active = !1;
this.startCountDown = !0;
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playGameHelpAudio();
}.bind(this), 3);
}
e++;
}, 1, 2, .01);
this.scheduleOnce(function() {
this.countDownNodeBg.active = !1;
A.aldSendEvent("挑战赛界面曝光");
this.startCountDown = !0;
}.bind(this), 3);
};
t.prototype.calculateLimitLevel = function() {
Math.floor(this.currentLevel / this.globalConst.levelCount);
this.globalConst.levelCount;
if (-1 != this.nextLevel) this.limitLevel = this.nextLevel; else {
var e = Math.round(this.settleUtil.getRandom(0, this.limitLevelArray.length - 1));
this.limitLevel = this.limitLevelArray[e];
this.limitLevelArray.splice(e, 1);
}
if (this.limitLevelArray.length <= 0) {
this.nextLevel = -1;
this.resetLimitLevelArray();
} else {
var t = Math.round(this.settleUtil.getRandom(0, this.limitLevelArray.length - 1));
this.nextLevel = this.limitLevelArray[t];
this.limitLevelArray.splice(t, 1);
}
};
t.prototype.resetLimitLevelArray = function() {
for (var e = Math.floor(this.currentLevel / this.globalConst.levelCount) * this.globalConst.levelCount + 1, t = this.globalConst.levelCount + e - 1, i = e; i <= t; i++) this.limitLevelArray[i - e] = i;
};
t.prototype.quitLimitGame = function() {
this.limitFailDialog.active = !1;
this.mainCanvas.getComponent("MainControl").back2LaunchView();
A.aldSendEvent("挑战赛返回点击", {
"关卡": this.currentLevel,
"选择": "退出"
});
};
t.prototype.drawCatTail = function(e) {
var t = this.node.convertToWorldSpaceAR(e.getPosition());
this.node.getParent().convertToNodeSpaceAR(t);
};
t.prototype.saveDataToWx = function(e) {
var t = new Array();
t.push({
key: "level",
value: String(e)
});
console.log("ok saveDataToWx kvDataList =", t);
cc.sys.platform == cc.sys.WECHAT_GAME && A.setUserCloudStorage({
KVDataList: t,
success: function(e) {
console.log("ok success", e);
},
fail: function(e) {
console.log("fail " + e);
}
});
};
t.prototype.initLimitBg = function() {
this.m_LimitBg.active = !0;
this.m_GoldLayout.active = !1;
this.m_Tool.getComponent("ToolsControl").setLimitMode();
cc.log(this.limitLevel, this.currentLevel, "this.limitLevel+++++++++++++");
this.changeTheme(this.limitLevel);
};
t.prototype.initChallengeBg = function() {
this.m_LimitBg.active = !1;
this.m_ChallengeBg.active = !0;
this.m_GoldLayout.active = !0;
this.m_Tool.getComponent("ToolsControl").setChallengeMode();
this.changeTheme();
};
t.prototype.showSettleDialog = function() {
this.currentLevel;
this.scheduleOnce(function() {
var e = this;
this.node.opacity = 0;
this.node.opacity = 0;
this.node.opacity = 0;
this.node.opacity = 0;
this.m_CatFace.runAction(cc.fadeOut(.2));
var t = this.m_WinLayout.getChildByName("DialogBg").getChildByName("Bg");
t.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.m_Bg.node.parent.getChildByName("ChallengeBg").getComponent(cc.Sprite).spriteFrame;
var i = this.m_WinLayout.height / t.getChildByName("Sprite").height;
t.scale = i + .2;
t.stopAllActions();
cc.tween(t).delay(.2).call(function() {
e.m_WinLayout.active = !0;
e.m_DialogRank.active = !0;
t.runAction(cc.scaleTo(1, 1).easing(cc.easeBackOut()));
}).delay(0).call(function() {
t.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = e.m_Bg.node.parent.getChildByName("bg").getComponent(cc.Sprite).spriteFrame;
e.settilePassAnim.resetSystem();
}).start();
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGamePassAudio();
}.bind(this), .3);
A.aldSendEvent("闯关赛结算曝光", {
"关卡": this.currentLevel
});
this.settleLevelLabel.getComponent(cc.Label).string = w.i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString());
this.settleUtil.getOverPlayerPercent(this.costTime, this.m_PathArray.length, function(e) {
this.settlePercentLabel.getComponent(cc.Label).string = w.i18nMgr._getLabel("txt_106").replace("*", this.costTime.toFixed(1) + "s").replace("&", e);
}.bind(this));
};
t.prototype.showPressureDialog = function() {
A.aldSendEvent("闯关赛宝箱曝光", {
"关卡": this.currentLevel
});
this.scheduleOnce(function() {
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGamePassAudio();
var e = this.mainCanvas.getComponent("MainControl");
this.resetBannerPos(this.pressureBtn);
e.showBanner(e.PressureBanner, function(e) {
this.moveBanner(e, this.pressureBtn);
}.bind(this));
this.pressureCheckBox.getChildByName("New Toggle").getComponent(cc.Toggle).check();
this.pressureCheckBox.opacity = 0;
var t = cc.fadeIn(1), i = cc.delayTime(2);
this.pressureCheckBox.runAction(cc.sequence(i, t));
}.bind(this), .5);
};
t.prototype.onAwardClick = function() {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.m_AwardView.getComponent("AwardViewControl").showStartView();
};
t.prototype.updateCoinsLabel = function(e) {
var t = e + "";
e >= 1e4 && (t = Math.floor(e / 1e3 * 100) / 100 + "k");
console.log("updateCoinsLabel scoreTmp = ", t);
this.m_CoinLabel.string = t;
if (e < this.globalConst.notifyCost) {
this.notifyCostLabel.active = !1;
this.notifyCostVedio.active = !0;
} else {
this.notifyCostLabel.active = !0;
this.notifyCostVedio.active = !1;
}
};
t.prototype.onShareAnswerClick = function() {
A.aldSendEvent("求助分享答案点击", {
"关卡": this.currentLevel
});
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
var e = new Date().getTime();
this.storageUtil.saveData("shareTime", e);
this.storageUtil.saveData("shareType", this.wxCommon.SHARE_ANSWER);
this.wxCommon.shareAnswer("", "", this.currentLevel, this.requestHelperInfo);
};
t.prototype.setAnswerShare = function(e, t) {
this.isAnswerGame = e;
this.requestHelperInfo = t;
};
t.prototype.getAnswerFromHelper = function() {
var e, t = this.m_AnswerPathArray.length - 1;
this.currentStep + t > this.m_AnswerPathArray.length && (t = this.m_AnswerPathArray.length - this.currentStep);
e = 0 == this.currentStep ? this.m_AnswerPathArray[this.currentStep] : this.m_AnswerPathArray[this.currentStep - 1];
this.popPathArrayList(this.m_BoxArray[e[1]][e[0]]);
for (var i = this.currentStep; i < this.currentStep + t; i++) {
var n = this.m_AnswerPathArray[i], r = this.m_BoxArray[n[1]][n[0]], o = "Bottom";
if (i > 0) {
var a = this.m_AnswerPathArray[i - 1], s = n[1] - a[1], c = n[0] - a[0];
s < 0 && (o = "Top");
s > 0 && (o = "Bottom");
if (0 == s) {
c < 0 && (o = "Left");
c > 0 && (o = "Right");
}
r.getComponent("Box").notifyClicked(o);
}
}
this.currentStep = this.currentStep + t;
};
t.prototype.onReviveClick = function() {
A.aldSendEvent("挑战赛失败复活", {
"关卡": this.currentLevel
});
this.limitFailDialog.getComponent("LimitFailControl").justHideAd();
var e = this.mainCanvas.getComponent("MainControl");
e.loadVideoAd(e.Revive);
};
t.prototype.gainRevive = function() {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.limitFailDialog.getComponent("LimitFailControl").closeDialog();
this.limitFailDialog.active = !1;
this.startCountDown = !0;
this.costTime = 0;
};
t.prototype.changeTheme = function(e) {
var t = this;
this.theme = Math.ceil(this.currentLevel / this.globalConst.chapterCount / this.globalConst.levelCount);
var i = this.currentLevel - (this.theme - 1) * this.globalConst.chapterCount * this.globalConst.levelCount;
this.chapter = Math.ceil(i / this.globalConst.levelCount);
var n, r, o = i % this.globalConst.levelCount || 20;
if (e) {
n = Math.ceil(e / this.globalConst.chapterCount / this.globalConst.levelCount);
var a = e - (n - 1) * this.globalConst.chapterCount * this.globalConst.levelCount;
r = Math.ceil(a / this.globalConst.levelCount);
o = a % this.globalConst.levelCount || 20;
this.level = o;
this.theme = n;
this.chapter = r;
} else {
n = this.theme;
r = this.chapter;
this.level = o;
if (this.currentLevel > this.globalConst.themeCount * this.globalConst.chapterCount * this.globalConst.levelCount) {
this.levelUtils.plsWaitUpdate();
return;
}
}
var s = new d.default().currentUrl + "/bg/id_" + (this.chapter + (this.theme - 1) * this.globalConst.chapterCount) + "/level_bg_" + this.level + ".jpg", c = this.m_Bg.node.parent.getChildByName("bg");
this.currentLevel == c.level || this.isLimitGame || (this.m_Bg.node.getComponent(cc.Sprite).spriteFrame = this.BgSprite);
c.level = this.currentLevel;
this.LoadNetImg(s, function(e) {
if (t.currentLevel == h.level) {
var i = new cc.SpriteFrame(e);
cc.loader.setAutoReleaseRecursively(i, !0);
t.m_Bg.node.parent.getChildByName("bg").getComponent(cc.Sprite).spriteFrame = i;
t.m_Bg.node.getComponent(cc.Widget).updateAlignment();
t.m_Bg.node.width = t.m_Bg.node.height / e.height * e.width;
t.m_Bg.getComponent(C.default).init();
}
});
cc.log(s);
this.scheduleOnce(function() {
var i = "", n = "";
if (e) {
if (t.nextLevel > -1) {
var r = Math.ceil(t.nextLevel / t.globalConst.chapterCount / t.globalConst.levelCount), o = t.nextLevel - (r - 1) * t.globalConst.chapterCount * t.globalConst.levelCount, a = Math.ceil(o / t.globalConst.levelCount), s = o % t.globalConst.levelCount || 20;
if (20 == t.level) {
n = new d.default().currentUrl + "/icons/id_" + (a + r * t.globalConst.chapterCount) + "/level_icon_1.jpg";
i = new d.default().currentUrl + "/bg/id_" + (a + r * t.globalConst.chapterCount) + "/level_bg_1.jpg";
} else {
n = new d.default().currentUrl + "/icons/id_" + (a + (r - 1) * t.globalConst.chapterCount) + "/level_icon_" + s + ".jpg";
i = new d.default().currentUrl + "/bg/id_" + (a + (r - 1) * t.globalConst.chapterCount) + "/level_bg_" + s + ".jpg";
}
}
} else if (20 == t.level) {
var c = 1 + t.chapter + (t.theme - 1) * t.globalConst.chapterCount > 54 ? 1 : 1 + t.chapter + (t.theme - 1) * t.globalConst.chapterCount;
n = new d.default().currentUrl + "/icons/id_" + c + "/level_icon_1.jpg";
i = new d.default().currentUrl + "/bg/id_" + c + "/level_bg_1.jpg";
} else {
n = new d.default().currentUrl + "/icons/id_" + (t.chapter + (t.theme - 1) * t.globalConst.chapterCount) + "/level_icon_" + (t.level + 1) + ".jpg";
i = new d.default().currentUrl + "/bg/id_" + (t.chapter + (t.theme - 1) * t.globalConst.chapterCount) + "/level_bg_" + (t.level + 1) + ".jpg";
}
cc.log(i, "bgPath1++++++++++");
if ("" != i) {
t.LoadNetImg(i, function() {});
t.LoadNetImg(n, function() {});
}
}, .5);
this.levelUtils.loadThemeJson(function(e) {
console.log("game theme json", e);
this.bodyNodeColor = e[n].body;
if ("string" == typeof e[n].bottom) this.m_ToolBg.color = new cc.Color().fromHEX(e[n].bottom); else {
this.m_ToolBg.color = new cc.Color().fromHEX(e[n].bottom[r - 1]);
this.m_NextPlayerMask.color = this.m_ToolBg.color;
}
}.bind(this));
var l = new d.default().currentUrl + "/icons/id_" + (this.chapter + (this.theme - 1) * this.globalConst.chapterCount) + "/level_icon_" + o + ".jpg", h = this.m_CatFace.getChildByName("mask");
this.currentLevel == h.level || this.isLimitGame || (this.m_CatFace.getChildByName("mask").getChildByName("img").getComponent(cc.Sprite).spriteFrame = null);
h.level = this.currentLevel;
this.LoadNetImg(l, function(e) {
if (t.currentLevel == h.level) {
var i = new cc.SpriteFrame(e);
cc.loader.setAutoReleaseRecursively(i, !0);
t.m_CatFace.getChildByName("mask").active = !0;
t.m_CatFace.getChildByName("mask").getChildByName("img").getComponent(cc.Sprite).spriteFrame = i;
}
});
this.isLimitGame || (this.m_NextPlayer.active = !0);
};
t.prototype.onAddCoinsClick = function() {
A.aldSendEvent("闯关赛添加猫币点击", {
"关卡": this.currentLevel
});
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.wxCommon.shareToFrends("", "", "", "闯关赛添加猫币分享");
var e = new Date().getTime();
this.storageUtil.saveData("shareTime", e);
this.storageUtil.saveData("shareType", this.wxCommon.PICK_ADD_COIN);
};
t.prototype.gainAddCoins = function() {
this.gold = this.coinsUtil.addCoins(this.globalConst.adddCoinClick);
console.log("gainAddCoins this.gold = " + this.gold);
this.updateCoinsLabel(this.gold);
this.playGainCoinsAnim();
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
};
t.prototype.gainAddCoin = function(e) {
this.gold = this.coinsUtil.addCoins(e);
console.log("gainAddCoins this.gold = " + this.gold);
this.updateCoinsLabel(this.gold);
this.playGainCoinsAnim();
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
};
t.prototype.closeAllDialog = function() {
this.limitFailDialog.getComponent("LimitFailControl").hideGameAd();
this.limitFailDialog.active = !1;
};
t.prototype.onDoubleCoinsClick = function(e) {
A.aldSendEvent("闯关赛领取" + e + "猫币点击", {
"关卡": this.currentLevel
});
var t = this.mainCanvas.getComponent("MainControl");
50 == e ? t.loadVideoAd(t.Gold50) : t.loadVideoAd(t.Gold100);
};
t.prototype.gainDoubleCoins = function() {
this.playGainCoinsAnim();
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
1 == this.shareLabelIndex ? this.coinsUtil.addCoins(this.globalConst.doubleGetAward) : this.coinsUtil.addCoins(this.globalConst.doubleGetAward1);
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
this.passNextLevel();
};
t.prototype.setDoubleAwardState = function(e) {
console.log("toggle is checked  = " + e.isChecked);
this.doubleCheckBoxChecked = e.isChecked;
};
t.prototype.onPressureGetClick = function() {
if (this.doubleCheckBoxChecked) {
A.aldSendEvent("闯关赛宝箱领取点击", {
"关卡": this.currentLevel,
"领取方式": "双倍"
});
var e = this.mainCanvas.getComponent("MainControl");
e.loadVideoAd(e.PressureDoubleGold);
} else {
this.playGainCoinsAnim();
A.aldSendEvent("闯关赛宝箱领取点击", {
"关卡": this.currentLevel,
"领取方式": "单倍"
});
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
this.passNextLevel();
}
};
t.prototype.gainPressure = function() {
this.playGainCoinsAnim();
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
this.passNextLevel();
};
t.prototype.showUnlockChapter = function() {
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGamePassAudio();
this.storageUtil.saveData("unLockLevel", this.currentLevel + 1);
this.mainCanvas.getComponent("MainControl").back2ThemeView();
this.mainCanvas.getComponent("MainControl").closeAlbum();
var e = this.currentLevel + 1, t = Math.ceil(e / this.globalConst.levelCount), i = e % (this.globalConst.chapterCount * this.globalConst.levelCount);
Math.ceil(i / this.globalConst.levelCount);
this.mainCanvas.getComponent("MainControl").themeView.getComponent("ThemeViewControl").showUnlockView(t, e);
};
t.prototype.onShared2Friends = function() {
this.m_AnswerSolvedRewardView.active = !0;
this.answerSolvedCheckBox.getChildByName("New Toggle").getComponent(cc.Toggle).check();
A.aldSendEvent("求助分享答案奖励界面曝光", {
"关卡": this.currentLevel
});
};
t.prototype.settleDialogControl = function(e, t) {
cc.log(t, "unlockChapterPercent");
var i = this.mainCanvas.getComponent("MainControl"), n = this.storageUtil.getData("unLockLevel" + (this.currentLevel + 1));
cc.log(t % 5, "unlockChapterPercent % 5 ", n, this.currentLevel + 1);
if (0 == t) if (n || i.Membership) this.m_PressureProgress.active = !1; else {
if (this.currentLevel + 1 < this.globalConst.chapterCount * this.globalConst.levelCount * this.globalConst.themeCount) {
this.showUnlockChapter();
return;
}
this.m_PressureProgress.active = !1;
} else if (t % 5 != 4) {
if (!n) {
this.storageUtil.saveData("unLockLevel" + (this.currentLevel + 1), 1);
var r = Math.ceil(this.currentLevel / this.globalConst.levelCount);
this.storageUtil.saveData("unLockThemeMaxLevel" + r, this.currentLevel + 1);
}
this.m_PressureProgress.active = !1;
} else {
this.m_PressureProgress.active = !n && !i.Membership;
this.mainCanvas.getComponent("MainControl").themeView.getComponent("ThemeViewControl").unlockLevel = this.currentLevel + 1;
}
this.showSettleDialog(e);
};
t.prototype.playGainCoinsAnim = function() {
var e = this.coinTargetNode.parent.convertToWorldSpaceAR(this.coinTargetNode.getPosition()), t = this.node.convertToNodeSpaceAR(e);
this.storageUtil.createGoldAnim(cc.v2(0, 0), t, 150, 20, !0, this.coinTargetNode, this.mainCanvas, this.poolMng);
};
t.prototype.onAnswerSolvedClick = function() {
if (this.doubleCheckBoxChecked) {
A.aldSendEvent("求助分享答案奖励领取", {
"关卡": this.currentLevel,
"领取方式": "双倍"
});
this.wxCommon.shareToFrends("", "", "", "求助分享答案奖励领取分享");
var e = new Date().getTime();
this.storageUtil.saveData("shareTime", e);
this.storageUtil.saveData("shareType", this.wxCommon.PICK_ANSWER);
} else {
A.aldSendEvent("求助分享答案奖励领取", {
"关卡": this.currentLevel,
"领取方式": "单倍"
});
this.gainAnswerSolvedCoins();
}
};
t.prototype.gainAnswerSolvedCoins = function() {
var e = this.storageUtil.getData("userData");
null == e && (e = new f.default());
this.doubleCheckBoxChecked ? e.gold += 2 * this.globalConst.shareAnswerCoin : e.gold += this.globalConst.shareAnswerCoin;
this.m_AnswerSolvedRewardView.active = !1;
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
this.storageUtil.saveData("userData", e);
this.updateCoinsLabel(e.gold);
this.doubleCheckBoxChecked = !0;
this.mainCanvas.getComponent("MainControl").back2LaunchView();
};
t.prototype.updateAwardIcon = function() {
var e = new Date().getTime(), t = 0;
if (Number(0 != this.awardClickTime)) if ((t = 12e4 - (e - Number(this.awardClickTime))) > 0) {
var i = Math.floor(t / 1e3 / 60), n = Math.floor(t % 6e4 / 1e3), r = "0" + i + ":" + n;
n < 10 && (r = "0" + i + ":0" + n);
var o = t / 12e4;
this.awardBtnMask.fillRange = o;
this.awardCountDownLabel.string = r;
this.awardBtn.interactable = !1;
this.awardBtnBg.active = !0;
} else {
this.awardBtn.interactable = !0;
this.awardBtnBg.active = !1;
cc.sys.localStorage.removeItem("awardClick");
} else {
this.awardBtn.interactable = !0;
this.awardBtnBg.active = !1;
}
};
t.prototype.initAwardBtnState = function() {
this.awardClickTime = this.storageUtil.getNumberData("awardClick", 0);
};
t.prototype.scheduleAwardBtn = function() {
this.initAwardBtnState();
this.awardScheduleId = setInterval(this.updateAwardIcon.bind(this), 1e3);
};
t.prototype.scheduleCatAudio = function() {};
t.prototype.playCatAudio = function() {};
t.prototype.showLimitBackDialog = function() {
this.onQuitClick();
};
t.prototype.onQuitClick = function() {
this.quitLimitGame();
};
t.prototype.onContinueClick = function() {
A.aldSendEvent("挑战赛返回点击", {
"关卡": this.currentLevel,
"选择": "继续"
});
this.startCountDown = !0;
};
t.prototype.showGuideAnim = function() {
this.storageUtil.saveData("guaidLevel" + this.currentLevel, !0);
this.fingerGuide.active = !0;
this.fingerGuide.opacity = 255;
this.fingerGuide.zIndex = this.m_CatFace.zIndex + 1;
var e = new Array();
this.fingerGuide.stopAllActions();
for (var t = 0; t < 2; t++) {
for (var i = 0; i < this.m_AnswerPathArray.length; i++) {
var n = this.m_AnswerPathArray[i], r = this.m_BoxArray[n[1]][n[0]];
0 == i && (0 != t ? e.push(cc.sequence(cc.moveTo(.1, cc.v2(r.position)), cc.fadeIn(.3))) : this.fingerGuide.setPosition(cc.v2(r.position)));
if (i < this.m_AnswerPathArray.length - 1) {
var o = this.m_AnswerPathArray[i + 1], a = this.m_BoxArray[o[1]][o[0]], s = cc.moveTo(.4, cc.v2(a.position));
e.push(s);
}
}
e.push(cc.fadeOut(.4));
}
var c = cc.repeat(cc.sequence(e), 1);
this.fingerGuide.runAction(c);
};
t.prototype.needGuideView = function() {
var e = this.storageUtil.getData("guaidLevel" + this.currentLevel);
return (!e || null == e) && this.currentLevel <= 3;
};
t.prototype.onAwardGained = function() {
this.playGainCoinsAnim();
this.mainCanvas.getComponent("MainControl").getVolumeControl().playGetCoinsAudio();
0 == this.m_WinLayout.active && (this.startCountDown = !0);
this.gold = this.storageUtil.getData("userData").gold;
this.updateCoinsLabel(this.gold);
};
t.prototype.showNotifyGuaidDialog = function() {
this.notifyDialogShow = !0;
if (null == this.notifyDialogPref) cc.loader.loadRes("pfb/NotifyLayout", function(e, t) {
this.notifyDialogPref = t;
this.notifyDialogNode = cc.instantiate(this.notifyDialogPref);
this.node.getParent().addChild(this.notifyDialogNode);
this.notifyDialogNode.getComponent("NotifyLayout").setGameControl(this);
this.notifyDialogNode.getComponent("NotifyLayout").show();
this.notifyDialogNode.zIndex = this.m_CatFace.zIndex + 1;
}.bind(this)); else {
this.notifyDialogNode.getComponent("NotifyLayout").show();
this.notifyDialogNode.zIndex = this.m_CatFace.zIndex + 1;
}
};
t.prototype.onNotifyVideoClick = function() {
A.aldSendEvent("闯关赛提示弹框视频点击", {
"关卡": this.currentLevel
});
var e = this.mainCanvas.getComponent("MainControl");
e.loadVideoAd(e.NotifyDialog);
};
t.prototype.onDialogNotifyClick = function() {
this.runningLevelEvent("提示弹框");
this.notifyDialogNode.getComponent("NotifyLayout").close();
this.onNotifyClick(2 * this.notifyStep);
};
t.prototype.startRolling = function() {
this.turnTableNode.getComponent("Turntable").startChoujiang();
};
t.prototype.showSettleShare = function() {
this.settlePopupPriorityArray.splice(0, this.settlePopupPriorityArray.length);
var e = this.shareLabelIndex % (this.storageUtil.settleShareLabel.length - 1) + 1;
this.shareLabel.string = this.storageUtil.settleShareLabel[e];
console.log("this.shareLabelIndex =", this.shareLabelIndex, this.storageUtil.settleShareLabel);
this.m_SingleSettleView.getComponent(cc.Button).interactable = !0;
this.gamePageNode.active = !1;
switch (e) {
case 0:
this.shareIcon.active = !1;
this.videoIcon.active = !1;
this.turnIcon.active = !1;
this.nextLabel.opacity = 0;
this.m_SingleSettleView.getComponent(cc.Button).interactable = !1;
break;

case 1:
this.goldIconLabel.string = "50";
this.turnIcon.active = !1;
this.shareIcon.active = !0;
this.shareIcon.getComponent(cc.Sprite).spriteFrame = this.gold50Icon;
this.videoIcon.active = !0;
this.nextLabel.opacity = 255;
break;

case 3:
this.goldIconLabel.string = "100";
this.turnIcon.active = !1;
this.shareIcon.active = !0;
this.shareIcon.getComponent(cc.Sprite).spriteFrame = this.gold50Icon;
this.videoIcon.active = !0;
this.nextLabel.opacity = 255;
break;

case 4:
this.shareIcon.active = !1;
this.turnIcon.active = !0;
this.videoIcon.active = !1;
this.nextLabel.opacity = 255;
this.settlePopupPriorityArray.push(this.storageUtil.TURNTABLE);
break;

default:
this.shareIcon.active = !1;
this.turnIcon.active = !1;
this.videoIcon.active = !1;
this.nextLabel.active = !0;
this.nextLabel.opacity = 255;
}
this.m_WinLayout.getChildByName("DialogBg").getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.m_Bg.node.parent.getChildByName("bg").getComponent(cc.Sprite).spriteFrame;
this.shareLabelIndex % 4 == 0 && this.shareLabelIndex > 0 && this.settlePopupPriorityArray.push(this.storageUtil.MOREGAMEPAGE);
e % 3 == 0 && this.shareLabelIndex > 0 && this.settlePopupPriorityArray.push(this.storageUtil.INTERSTITIAL);
this.doubleLayout.scale = .8;
this.videoNextBtn.active = !0;
this.nextLabel.opacity = 0;
this.m_SingleSettleView.getComponent(cc.Button).interactable = !1;
this.showPopSettle();
this.shareLabelIndex++;
};
t.prototype.showPopSettle = function() {
for (var e = 99, t = 0; t < this.settlePopupPriorityArray.length; t++) this.settlePopupPriorityArray[t] < e && (e = this.settlePopupPriorityArray[t]);
};
t.prototype.onShareBtnClick = function() {
switch ((this.shareLabelIndex - 1) % (this.storageUtil.settleShareLabel.length - 1) + 1) {
case 0:
this.onPassNextLevelClick();
break;

case 1:
this.onDoubleCoinsClick(50);
break;

case 2:
this.onSettleShareClick();
break;

case 3:
this.onDoubleCoinsClick(100);
}
};
t.prototype.onVideoNextClick = function() {
if (1 == this.videoNextBtn.active) {
A.aldSendEvent("闯关赛下一关视频点击", {
"关卡": this.currentLevel
});
var e = this.mainCanvas.getComponent("MainControl");
e.loadVideoAd(e.NextLevel);
}
};
t.prototype.justHideAd = function() {
this.gameAdshow = !1;
null != this.iconAd && this.iconAd.hide();
};
t.prototype.showAd = function() {
this.gameAdshow = !0;
null != this.iconAd && this.iconAd.show();
};
t.prototype.hideGameAd = function() {
this.gameAdshow = !1;
if (null != this.iconAd) {
this.iconAd.hide();
this.iconAd = null;
}
};
t.prototype.resetBannerPos = function() {};
t.prototype.moveBanner = function() {};
t.prototype.initLevelEvent = function() {
A.aldStage.onStart({
stageId: this.currentLevel,
stageName: w.i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString())
});
};
t.prototype.runningLevelEvent = function(e) {
A.aldStage.onRunning({
stageId: this.currentLevel,
stageName: w.i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString()),
event: "tools",
params: {
itemName: e
}
});
};
t.prototype.endLevelEvent = function() {
A.aldStage.onEnd({
stageId: this.currentLevel,
stageName: w.i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString()),
event: "complete",
params: {
desc: "关卡完成"
}
});
};
o([ h(cc.Label) ], t.prototype, "label", void 0);
o([ h(cc.Node) ], t.prototype, "m_WinLayout", void 0);
o([ h(cc.Node) ], t.prototype, "m_CatFace", void 0);
o([ h(cc.Node) ], t.prototype, "m_InnerLayout", void 0);
o([ h(cc.Label) ], t.prototype, "settleLevelLabel", void 0);
o([ h(cc.Label) ], t.prototype, "settlePercentLabel", void 0);
o([ h(cc.Label) ], t.prototype, "settlePlayerLabel", void 0);
o([ h(cc.Node) ], t.prototype, "countDownNode", void 0);
o([ h(cc.Node) ], t.prototype, "countDownNodeBg", void 0);
o([ h(cc.Label) ], t.prototype, "countDownLabel", void 0);
o([ h(cc.Node) ], t.prototype, "limitFailDialog", void 0);
o([ h(cc.Node) ], t.prototype, "mainCanvas", void 0);
o([ h(cc.Node) ], t.prototype, "m_CatTail", void 0);
o([ h(cc.Node) ], t.prototype, "m_NextPlayer", void 0);
o([ h(cc.Node) ], t.prototype, "m_NextPlayerMask", void 0);
o([ h(cc.Node) ], t.prototype, "m_ChallengeBg", void 0);
o([ h(cc.Node) ], t.prototype, "m_LimitBg", void 0);
o([ h(cc.Node) ], t.prototype, "m_Tool", void 0);
o([ h(cc.Node) ], t.prototype, "m_ToolBg", void 0);
o([ h(cc.Node) ], t.prototype, "m_GoldLayout", void 0);
o([ h(cc.Label) ], t.prototype, "m_CoinLabel", void 0);
o([ h(cc.Node) ], t.prototype, "m_PressureProgress", void 0);
o([ h(cc.Label) ], t.prototype, "m_PressureLabel", void 0);
o([ h(cc.Node) ], t.prototype, "m_AwardView", void 0);
o([ h(cc.Node) ], t.prototype, "m_DialogRank", void 0);
o([ h(cc.Sprite) ], t.prototype, "m_BottomView", void 0);
o([ h(cc.Sprite) ], t.prototype, "m_Bg", void 0);
o([ h(cc.Node) ], t.prototype, "m_DoubleSettleView", void 0);
o([ h(cc.Node) ], t.prototype, "m_SingleSettleView", void 0);
o([ h(cc.Node) ], t.prototype, "m_AnswerSolvedRewardView", void 0);
o([ h(cc.Prefab) ], t.prototype, "getWxInfoDialog", void 0);
o([ h(cc.Node) ], t.prototype, "coinTargetNode", void 0);
o([ h(cc.ParticleSystem) ], t.prototype, "settilePassAnim", void 0);
o([ h(cc.Button) ], t.prototype, "notifyBtn", void 0);
o([ h(cc.Button) ], t.prototype, "awardBtn", void 0);
o([ h(cc.Node) ], t.prototype, "awardBtnBg", void 0);
o([ h(cc.Sprite) ], t.prototype, "awardBtnMask", void 0);
o([ h(cc.Label) ], t.prototype, "awardCountDownLabel", void 0);
o([ h(cc.Node) ], t.prototype, "limitBackDialog", void 0);
o([ h(cc.Node) ], t.prototype, "fingerGuide", void 0);
o([ h(cc.Prefab) ], t.prototype, "turnTablePrefab", void 0);
o([ h(cc.Label) ], t.prototype, "shareLabel", void 0);
o([ h(cc.Node) ], t.prototype, "shareIcon", void 0);
o([ h(cc.Node) ], t.prototype, "nextLabel", void 0);
o([ h(cc.Node) ], t.prototype, "videoIcon", void 0);
o([ h(cc.Node) ], t.prototype, "turnIcon", void 0);
o([ h(cc.Label) ], t.prototype, "goldIconLabel", void 0);
o([ h(cc.Node) ], t.prototype, "notifyCostLabel", void 0);
o([ h(cc.Node) ], t.prototype, "notifyCostVedio", void 0);
o([ h(cc.SpriteFrame) ], t.prototype, "gold50Icon", void 0);
o([ h(cc.Node) ], t.prototype, "pressureBtn", void 0);
o([ h(cc.Node) ], t.prototype, "pressureCheckBox", void 0);
o([ h(cc.Node) ], t.prototype, "answerSolvedCheckBox", void 0);
o([ h(cc.Prefab) ], t.prototype, "gameGridPreb", void 0);
o([ h(cc.Prefab) ], t.prototype, "gamePagePrefab", void 0);
o([ h(cc.Node) ], t.prototype, "doubleLayout", void 0);
o([ h(cc.Node) ], t.prototype, "videoNextBtn", void 0);
o([ h(cc.Node) ], t.prototype, "giftAnim", void 0);
o([ h(cc.SpriteFrame) ], t.prototype, "BgSprite", void 0);
return o([ l ], t);
}(S.BaseLayer);
i.default = M;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./Bean/PassLevelBean": "PassLevelBean",
"./Bean/UserData": "UserData",
"./CoinsUtil": "CoinsUtil",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./DualBlur/DualBlur": "DualBlur",
"./GlobalConst": "GlobalConst",
"./LevelData": "LevelData",
"./LevelUtils": "LevelUtils",
"./SettleUtil": "SettleUtil",
"./SubDomainControl": "SubDomainControl",
"./base/BaseLayer": "BaseLayer",
"./native/CallNative": "CallNative",
"./native/JsBridge": "JsBridge"
} ],
GaussianBlur: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "94a05j2eGhHQ6sPLzL1+XrR", "GaussianBlur");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
}, a = this && this.__awaiter || function(e, t, i, n) {
return new (i || (i = Promise))(function(r, o) {
function a(e) {
try {
c(n.next(e));
} catch (e) {
o(e);
}
}
function s(e) {
try {
c(n.throw(e));
} catch (e) {
o(e);
}
}
function c(e) {
e.done ? r(e.value) : (t = e.value, t instanceof i ? t : new i(function(e) {
e(t);
})).then(a, s);
var t;
}
c((n = n.apply(e, t || [])).next());
});
}, s = this && this.__generator || function(e, t) {
var i, n, r, o, a = {
label: 0,
sent: function() {
if (1 & r[0]) throw r[1];
return r[1];
},
trys: [],
ops: []
};
return o = {
next: s(0),
throw: s(1),
return: s(2)
}, "function" == typeof Symbol && (o[Symbol.iterator] = function() {
return this;
}), o;
function s(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(o) {
if (i) throw new TypeError("Generator is already executing.");
for (;a; ) try {
if (i = 1, n && (r = 2 & o[0] ? n.return : o[0] ? n.throw || ((r = n.return) && r.call(n), 
0) : n.next) && !(r = r.call(n, o[1])).done) return r;
(n = 0, r) && (o = [ 2 & o[0], r.value ]);
switch (o[0]) {
case 0:
case 1:
r = o;
break;

case 4:
a.label++;
return {
value: o[1],
done: !1
};

case 5:
a.label++;
n = o[1];
o = [ 0 ];
continue;

case 7:
o = a.ops.pop();
a.trys.pop();
continue;

default:
if (!(r = a.trys, r = r.length > 0 && r[r.length - 1]) && (6 === o[0] || 2 === o[0])) {
a = 0;
continue;
}
if (3 === o[0] && (!r || o[1] > r[0] && o[1] < r[3])) {
a.label = o[1];
break;
}
if (6 === o[0] && a.label < r[1]) {
a.label = r[1];
r = o;
break;
}
if (r && a.label < r[2]) {
a.label = r[2];
a.ops.push(o);
break;
}
r[2] && a.ops.pop();
a.trys.pop();
continue;
}
o = t.call(e, a);
} catch (e) {
o = [ 6, e ];
n = 0;
} finally {
i = r = 0;
}
if (5 & o[0]) throw o[1];
return {
value: o[0] ? o[1] : void 0,
done: !0
};
}
};
Object.defineProperty(i, "__esModule", {
value: !0
});
e("../../misc/EditorAsset");
var c = cc._decorator, l = c.ccclass, h = c.property, u = c.requireComponent, f = c.executeInEditMode, d = c.disallowMultiple, p = c.executionOrder, m = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t._effect = null;
t._radius = 10;
t.sprite = null;
t.material = null;
return t;
}
Object.defineProperty(t.prototype, "effect", {
get: function() {
return this._effect;
},
set: function(e) {
this._effect = e;
this.init();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "radius", {
get: function() {
return this._radius;
},
set: function(e) {
this._radius = e > 500 ? 500 : e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
this.init();
};
t.prototype.resetInEditor = function() {
this.init();
};
t.prototype.init = function() {
return a(this, void 0, void 0, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
return [ 3, 2 ];

case 1:
e.sent();
e.label = 2;

case 2:
if (!this._effect) return [ 2 ];
this.sprite = this.node.getComponent(cc.Sprite);
this.sprite.spriteFrame && (this.sprite.spriteFrame.getTexture().packable = !1);
this.material = cc.Material.create(this._effect);
this.sprite.setMaterial(0, this.material);
this.updateProperties();
return [ 2 ];
}
});
});
};
t.prototype.updateProperties = function() {
this.material.setProperty("size", this.getNodeSize());
this.material.setProperty("radius", this.radius);
};
t.prototype.getNodeSize = function() {
return cc.v2(this.node.width, this.node.height);
};
o([ h ], t.prototype, "_effect", void 0);
o([ h({
type: cc.EffectAsset,
tooltip: !1,
readonly: !0
}) ], t.prototype, "effect", null);
o([ h ], t.prototype, "_radius", void 0);
o([ h({
tooltip: !1
}) ], t.prototype, "radius", null);
return o([ l, u(cc.Sprite), f, d, p(-100) ], t);
}(cc.Component);
i.default = m;
cc._RF.pop();
}, {
"../../misc/EditorAsset": void 0
} ],
GetWXInfoDialog: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "749acQsjndD65qE17HG1wje", "GetWXInfoDialog");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./Common/HttpRequest"), h = e("./Common/StorageUtil"), u = e("./Bean/WXUserBean"), f = window.wx, d = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.content = null;
t.WXInfoButton = null;
t.httpRequest = new l.default();
t.userBean = null;
t.gameBean = null;
t.storageUtil = new h.default();
t.callback = null;
t.wxButton = null;
return t;
}
t.prototype.start = function() {};
t.prototype.close = function() {
this.node.active = !1;
this.wxButton.hide();
};
t.prototype.setContent = function(e) {
this.content.string = e;
};
t.prototype.initWXLogin = function() {
cc.sys.platform == cc.sys.WECHAT_GAME && this.wxCheckUserState();
};
t.prototype.wxCheckUserState = function() {
f.getSetting({
fail: function() {}.bind(this),
success: function(e) {
e.authSetting["scope.userInfo"] && this.getUserInfo();
}.bind(this)
});
};
t.prototype.showWxDialog = function(e) {
this.initWxButton();
this.node.active = !0;
this.callback = e;
};
t.prototype.initWxButton = function() {
var e = this, t = cc.size(this.WXInfoButton.width + 10, this.WXInfoButton.height + 10), i = cc.view.getFrameSize(), n = cc.director.getWinSize(), r = (.5 * n.width + this.WXInfoButton.x - .5 * t.width) / n.width * i.width, o = (.5 * n.height - this.WXInfoButton.y - .5 * t.height) / n.height * i.height, a = t.width / n.width * i.width, s = t.height / n.height * i.height;
this.wxButton = f.createUserInfoButton({
type: "text",
text: "",
style: {
left: r,
top: o,
width: a,
height: s,
lineHeight: s,
backgroundColor: "#ffff0000",
color: "#ffffff",
textAlign: "center",
fontSize: 20,
borderRadius: 4
}
});
this.wxButton.onTap(function(t) {
console.log("wxGetUserInfo:", t);
e.updateWXUserInfo(t.userInfo);
e.node.active = !1;
e.wxButton.hide();
});
};
t.prototype.updateWXUserInfo = function(e) {
console.log("updateWXUserInfo userInfo: ", e);
var t = this.storageUtil.getData("userData");
if (null != t) {
console.log("updateWXUserInfo userbean uid: ", t.uid);
var i = new u.default();
i.uid = t.uid;
i.avatarUrl = e.avatarUrl;
i.nickName = e.nickName;
this.storageUtil.saveData("wxUserInfo", i);
this.httpRequest.saveUserInfo(t.uid, e, function(t) {
console.log("updateWXUserInfo data: ", t);
this.callback(e.nickName, e.avatarUrl);
}.bind(this));
}
};
t.prototype.getUserInfo = function() {
f.getUserInfo({
fail: function(e) {
(e.errMsg.indexOf("auth deny") > -1 || e.errMsg.indexOf("auth denied") > -1) && this.guideActive();
}.bind(this),
success: function(e) {
console.log("getUserInfo: ", e);
this.updateWXUserInfo(e.userInfo);
}.bind(this)
});
};
o([ c(cc.Label) ], t.prototype, "content", void 0);
o([ c(cc.Node) ], t.prototype, "WXInfoButton", void 0);
return o([ s ], t);
}(cc.Component);
i.default = d;
cc._RF.pop();
}, {
"./Bean/WXUserBean": "WXUserBean",
"./Common/HttpRequest": "HttpRequest",
"./Common/StorageUtil": "StorageUtil"
} ],
GlobalConst: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "b2eb5n2ySZEuJ2EYGgnfDHa", "GlobalConst");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.themeCount = 6;
this.chapterCount = 9;
this.levelCount = 20;
this.countDownTime = 30;
this.awardTime = 15;
this.awardCoin = 100;
this.doubleGetAwardStep = 5;
this.doubleGetAward = 50;
this.doubleGetAward1 = 100;
this.notifyCost = 80;
this.unlockChapterCost = 200;
this.shareAnswerCoin = 100;
this.inviteCoin = 50;
this.dayAwardCoin = 100;
this.adddCoinClick = 50;
this.addCoinGod = 200;
this.costTimeThre = 30;
this.helpMe = 5;
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
HttpRequestNew: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "a9f91dLWghHYJFnrzjaCM2b", "HttpRequestNew");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = e("../native/JsBridge"), o = e("crypto-js"), a = cc._decorator, s = a.ccclass, c = (a.property, 
"https://pay-api.weililiang1.com/"), l = function() {
function e() {
this.key = o.enc.Utf8.parse("*****weililiang1");
this.retryCount = 0;
}
e.prototype.order = function(e, t) {
console.log("order");
var i = "api/v1/order?userId=" + e.userId;
r.JsBridge.IsNull(e.deviceId) || (i += "&deviceId=" + e.deviceId);
r.JsBridge.IsNull(e.platformProductId) || (i += "&platformProductId=" + e.platformProductId);
r.JsBridge.IsNull(e.payType) || (i += "&payType=" + e.payType);
r.JsBridge.IsNull(e.businessId) || (i += "&businessId=" + e.businessId);
this.requestGet(i, function(e) {
t(e);
}.bind(this));
};
e.prototype.callback = function(e, t, i) {
console.log("callback");
this.requestPost("api/v1/order/callback/" + e, t, function(e) {
i(e);
}.bind(this));
};
e.prototype.requestGet = function(e, t) {
new Promise(function() {
var i = cc.loader.getXMLHttpRequest();
i.onreadystatechange = function() {
console.log("xhr.readyState=" + i.readyState + "  xhr.status=" + i.status);
if (4 === i.readyState && i.status >= 200 && i.status < 300) {
var e = i.responseText;
console.log("request response:", e);
var n = JSON.parse(e);
t(n);
}
};
i.ontimeout = function() {
t({
data: null
});
};
i.onabort = function() {
t({
data: null
});
};
i.onerror = function() {
t({
data: null
});
};
var n = c + e;
console.log("url_temp", n);
i.timeout = 1e4;
i.open("GET", n, !0);
i.setRequestHeader("Content-Type", "application/json; charset=utf-8");
i.send();
});
};
e.prototype.requestPost = function(e, t, i) {
new Promise(function() {
var n = cc.loader.getXMLHttpRequest();
n.onreadystatechange = function() {
console.log("xhr.readyState=" + n.readyState + "  xhr.status=" + n.status);
if (4 === n.readyState && n.status >= 200 && n.status < 300) {
var e = n.responseText;
console.log("request response:", e);
var t = JSON.parse(e);
i(t);
}
};
n.ontimeout = function() {
console.log("timeout");
};
var r = c + e;
console.log("url_temp", r);
console.log("bodyParams", JSON.stringify(t));
n.timeout = 1e4;
n.open("POST", r, !0);
n.setRequestHeader("Content-Type", "application/json; charset=utf-8");
var o = JSON.stringify(t);
n.send(o);
});
};
e.prototype.event = function(e, t, i) {
var n = this;
new Promise(function() {
var r = cc.loader.getXMLHttpRequest();
r.onreadystatechange = function() {
console.log("xhr.readyState=" + r.readyState + "  xhr.status=" + r.status);
if (4 === r.readyState && r.status >= 200 && r.status < 300) {
var e = JSON.parse(r.responseText), t = e.Data;
if (t) {
t = JSON.parse(n.decrypt(e.Data));
e.Data = t;
}
console.log("request response:", e.Code, e.Msg);
console.log("request data:", e.Data);
i(t);
}
};
r.ontimeout = function() {
console.log("timeout");
};
var o = "https://global-postback.weililiang1.com/" + e;
console.log("url_temp", o);
console.log("bodyParams", JSON.stringify(t));
r.timeout = 1e4;
r.open("POST", o, !0);
r.setRequestHeader("Content-Type", "application/json; charset=utf-8");
var a = n.encrypt(JSON.stringify(t));
console.log("encrypt", a);
var s = n.decrypt(a);
console.log("decode", s);
r.send(a);
});
};
e.prototype.decrypt = function(e) {
return o.AES.decrypt(e, this.key, {
iv: this.key,
mode: o.mode.CBC,
padding: o.pad.Pkcs7
}).toString(o.enc.Utf8).toString();
};
e.prototype.encrypt = function(e) {
var t = o.enc.Utf8.parse(e);
return o.AES.encrypt(t, this.key, {
iv: this.key,
mode: o.mode.CBC,
padding: o.pad.Pkcs7
}).toString();
};
return n([ s("HttpRequestNew") ], e);
}();
i.default = l;
cc._RF.pop();
}, {
"../native/JsBridge": "JsBridge",
"crypto-js": "crypto-js"
} ],
HttpRequest: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "2f376ENZXlMQIo9Hl/EEnNr", "HttpRequest");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, "https://ad-api-test.weililiang1.com/pay/api/v1/order?userId=3898c904-5009-49de-a921-464ee3aa7858&deviceId=3898c904-5009-49de-a921-464ee3aa7858&platformProductId=onelinecat&payType=2&businessId=com.wll.onelinecat"), s = function() {
function e() {}
e.prototype.saveUserInfo = function(e, t, i) {
var n = {
uid: e,
nickName: t.nickName,
gender: t.gender,
citiy: t.city,
province: t.province,
country: t.country,
avatarUrl: t.avatarUrl
};
this.request("user/nosign/saveUserInfo", n, i);
};
e.prototype.login = function(e, t) {
var i = {
jsCode: e
};
this.request("user/nosign/userRegist", i, function(e) {
t(e.data);
});
};
e.prototype.saveGameData = function(e, t, i) {
console.log("saveGameData");
var n = {
uid: e,
gold: t.gold,
gameLevel: t.gameLevel,
limitLevel: t.limitLevel
};
this.request("user/nosign/uploadLevel", n, function(e) {
for (var t = new Array(), n = 0; n < e.length; n++) t[Number(e[n].lvl)] = e[n].conf;
i(JSON.parse(JSON.stringify(t)));
}.bind(this));
};
e.prototype.getUserGameData = function(e, t) {
var i = {
uid: e
};
this.request("user/nosign/getUserInfo", i, function(e) {
t(e.data);
});
};
e.prototype.getUserLevel = function(e, t, i, n) {
var r = {
uid: e,
page: t,
pageSize: i
};
this.request("user/nosign/getUserGameLevel", r, n);
};
e.prototype.getUserLimitLevel = function(e, t, i, n) {
var r = {
uid: e,
page: t,
pageSize: i
};
this.request("user/nosign/getUserLimitLevel", r, n);
};
e.prototype.uploadShareInfo = function(e, t, i, n) {
var r = {
uid: e,
shareUid: t,
rewardStatus: i
};
console.log("uploadShareInfo bodyParams = " + r);
this.request("share/nosign/uploadShareInfo", r, n);
};
e.prototype.uploadReward = function(e, t, i, n) {
var r = {
uid: e,
shareUid: t,
rewardStatus: 1
};
console.log("uploadReward bodyParams = " + r);
this.request("share/nosign/getRewards", r, n);
};
e.prototype.queryShareUser = function(e, t, i, n) {
var r = {
uid: e,
page: t,
pageSize: i
};
this.request("share/nosign/queryShareUser", r, n);
};
e.prototype.request = function(e, t, i) {
new Promise(function() {
var e = cc.loader.getXMLHttpRequest();
e.onreadystatechange = function() {
console.log("xhr.readyState=" + e.readyState + "  xhr.status=" + e.status);
if (4 === e.readyState && e.status >= 200 && e.status < 300) {
var t = e.responseText;
console.log("request response:", t);
var n = JSON.parse(t);
i(n);
}
};
e.open("GET", "https://ad-api-test.weililiang1.com/pay/api/v1/order?userId=3898c904-5009-49de-a921-464ee3aa7858&deviceId=3898c904-5009-49de-a921-464ee3aa7858&platformProductId=onelinecat&payType=2&businessId=com.wll.onelinecat", !0);
e.timeout = 5e3;
e.setRequestHeader("Content-Type", "application/json");
e.send();
});
};
e.prototype.requestGet = function(e, t) {
new Promise(function() {
var i = cc.loader.getXMLHttpRequest();
i.onreadystatechange = function() {
console.log("xhr.readyState=" + i.readyState + "  xhr.status=" + i.status);
if (4 === i.readyState && i.status >= 200 && i.status < 300) {
var e = i.responseText;
console.log("request response:", e);
var n = JSON.parse(e);
t(n);
}
};
var n = a + e;
console.log("url_temp", n);
i.open("GET", n, !0);
i.timeout = 5e3;
i.setRequestHeader("Content-Type", "application/json");
i.send();
});
};
e.prototype.loadImage = function(e, t) {
cc.loader.load({
url: e,
type: "png"
}, function(e, i) {
e && console.error(e);
t.spriteFrame = new cc.SpriteFrame(i);
});
};
return n([ o("HttpRequest") ], e);
}();
i.default = s;
cc._RF.pop();
}, {} ],
InnerBox: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "dbe9a6FHiRBRoDGV8huShc+", "InnerBox");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./LevelData"), h = e("./Bean/UserData"), u = e("./Common/StorageUtil"), f = e("./LevelUtils"), d = e("./GlobalConst"), p = function() {
this.x = 0;
this.y = 0;
this.width = 0;
this.height = 0;
}, m = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.smallInnerPrefab = null;
t.parentNode = null;
t.maxItemWidth = 10;
t.lockNode = null;
t.playingNode = null;
t.lockIcon = null;
t.lockSprite = [];
t.levelData = null;
t.userData = null;
t.m_Level = 1;
t.spacing = 4;
t.itemWidth = 0;
t.m_BoxArray = new Array();
t.m_BoxNodeArray = new Array();
t.storageUtil = new u.default();
t.drawedColor = new cc.Color();
t.playColor = new cc.Color();
t.levelUtil = new f.default();
t.globalConst = new d.default();
t.m_RowNum = 0;
t.m_ColNum = 0;
t.m_BoxWidth = 0;
t.m_MaxBoxWidth = 85;
t.m_LevelConfigArray = new Array();
t.pathColor = "";
t.theme = 0;
t.themeView = null;
return t;
}
t.prototype.init = function(e) {
this.m_Level = e;
cc.log(this.m_Level, "this.m_Level____________");
this.levelData = cc.find("Canvas").getComponent("MainControl").getLevelPathData(this.m_Level);
this.userData = this.storageUtil.getData("userData");
this.removeAllSmallInnerBox();
null == this.userData && (this.userData = new h.default());
this.changeThemeColor();
};
t.prototype.onLoad = function() {};
t.prototype.refreshInnerBox = function() {
var e = Math.max(this.levelData.colNum, this.levelData.rowNum);
this.itemWidth = (this.node.width - (e - 1) * this.spacing) / e;
this.itemWidth = Math.min(this.itemWidth, this.maxItemWidth);
this.drawedColor.fromHEX(this.levelData.currentColor);
this.getCatBodyInfo(this.levelData.currentPathIndex, this.levelData.rowNum, this.levelData.colNum, this.itemWidth, this.spacing);
this.getOriAndUpdatePos(this.levelData.currentPathIndex);
this.generateSmallNode();
};
t.prototype.getCatBodyInfo = function(e, t, i, n, r) {
this.m_BoxArray.splice(0, this.m_BoxArray.length);
for (var o = 0; o < e.length; o++) {
this.m_BoxArray[o] = new p();
this.m_BoxArray[o].x = this.getPosByIndex(e[o][1], i, n, r);
this.m_BoxArray[o].y = this.getPosByIndex(t - 1 - e[o][0], t, n, r);
this.m_BoxArray[o].height = this.itemWidth;
this.m_BoxArray[o].width = this.itemWidth;
}
};
t.prototype.getPosByIndex = function(e, t, i, n) {
return -(t * i + (t - 1) * n) / 2 + i / 2 + e * (i + n);
};
t.prototype.getOriAndUpdatePos = function(e) {
for (var t = 1; t < e.length; t++) {
var i = e[t], n = e[t - 1], r = i[0] - n[0], o = i[1] - n[1];
if (r < 0) {
this.m_BoxArray[t - 1].height = 2 * this.itemWidth;
this.m_BoxArray[t - 1].y += this.itemWidth / 2;
}
if (r > 0) {
this.m_BoxArray[t - 1].height = 2 * this.itemWidth;
this.m_BoxArray[t - 1].y -= this.itemWidth / 2;
}
if (0 == r) {
if (o < 0) {
this.m_BoxArray[t - 1].width = 2 * this.itemWidth;
this.m_BoxArray[t - 1].x -= this.itemWidth / 2;
}
if (o > 0) {
this.m_BoxArray[t - 1].width = 2 * this.itemWidth;
this.m_BoxArray[t - 1].x += this.itemWidth / 2;
}
}
}
};
t.prototype.generateSmallNode = function() {
this.m_BoxArray.length > 0 && this.removeAllSmallInnerBox();
this.m_BoxNodeArray.splice(0, this.m_BoxNodeArray.length);
for (var e = 0; e < this.m_BoxArray.length; e++) {
var t = cc.instantiate(this.smallInnerPrefab);
this.parentNode.addChild(t);
t.x = this.m_BoxArray[e].x;
t.y = this.m_BoxArray[e].y;
t.width = this.m_BoxArray[e].width;
t.height = this.m_BoxArray[e].height;
this.m_BoxNodeArray[e] = t;
this.m_BoxNodeArray[e].color = new cc.Color().fromHEX("#FFFFFF");
}
};
t.prototype.setThemeView = function(e) {
this.themeView = e;
};
t.prototype.onClick = function() {
if (this.lockNode.active) {
if (this.lockIcon.getComponent(cc.Sprite).spriteFrame == this.lockSprite[1]) {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.node.getParent().getComponent("InnerLevelControl").onLevelSelected(this.m_Level);
return;
}
if (this.storageUtil.getData("unLockLevel" + (this.m_Level - 1))) {
this.themeView.getComponent("ThemeViewControl").unlockLevel = this.m_Level;
this.themeView.getComponent("ThemeViewControl").theme = this.theme;
var e = cc.find("Canvas").getComponent("MainControl");
e.loadVideoAd(e.unlockLevel);
}
} else {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.node.getParent().getComponent("InnerLevelControl").onLevelSelected(this.m_Level);
}
};
t.prototype.removeAllSmallInnerBox = function() {
this.parentNode.destroyAllChildren();
};
t.prototype.changeThemeColor = function() {
var e = this.m_Level, t = Math.ceil(e / this.globalConst.chapterCount / this.globalConst.levelCount);
this.levelUtil.loadThemeJson(function(e) {
this.playColor = new cc.Color().fromHEX(e[t].play);
this.pathColor = e[t].body;
this.refreshNodeState();
}.bind(this));
};
t.prototype.saveAnswer2LevelPath = function() {
this.levelData = new l.default();
var e = Math.ceil(this.m_Level / this.globalConst.chapterCount / this.globalConst.levelCount), t = this.m_Level - (e - 1) * this.globalConst.chapterCount * this.globalConst.levelCount, i = Math.ceil(t / this.globalConst.levelCount);
this.levelUtil.loadLevelJson(e, i, function(t) {
var n = this.m_Level - (e - 1) * this.globalConst.chapterCount * this.globalConst.levelCount - (i - 1) * this.globalConst.levelCount - 1, r = this.levelUtil.hex_to_levelData(this.levelUtil.getBinString(t[n]));
this.initRowColNum(r);
this.caculateBoxWidth();
var o = 0;
for (var a in r) {
this.levelData.currentPathIndex.push([]);
this.levelData.currentPathIndex[o][0] = r[a][1];
this.levelData.currentPathIndex[o][1] = r[a][0];
o++;
}
this.saveLevelData();
this.refreshInnerBox();
}.bind(this));
};
t.prototype.saveLevelData = function() {
this.levelData.currentColor = this.pathColor;
this.levelData.perfect = !0;
this.levelData.colNum = this.m_ColNum;
this.levelData.rowNum = this.m_RowNum;
this.levelData.itemWidth = this.m_BoxWidth;
cc.find("Canvas").getComponent("MainControl").saveLevelPathData(this.m_Level, this.levelData);
};
t.prototype.initRowColNum = function(e) {
this.m_RowNum = 0;
this.m_ColNum = 0;
for (var t = 0; t < e.length; t++) {
var i = e[t];
i[1] > this.m_RowNum && (this.m_RowNum = i[1]);
i[0] > this.m_ColNum && (this.m_ColNum = i[0]);
}
this.m_RowNum += 1;
this.m_ColNum += 1;
};
t.prototype.caculateBoxWidth = function() {
this.m_BoxWidth = (cc.winSize.width - (this.m_ColNum - 1)) / (this.m_ColNum + 1);
this.m_BoxWidth = Math.min(this.m_BoxWidth, this.m_MaxBoxWidth);
};
t.prototype.refreshNodeState = function() {
var e = this.storageUtil.getData("unLockLevel" + this.m_Level), t = Math.ceil(this.m_Level / this.globalConst.levelCount), i = this.storageUtil.getData("unLockThemeMaxLevel" + t), n = cc.find("Canvas").getComponent("MainControl");
n.Membership && (i = t * this.globalConst.levelCount);
var r = this.node.getParent().getComponent("InnerLevelControl"), o = this.m_Level % this.globalConst.levelCount;
if (e || n.Membership) if (e && 2 == e) {
this.playingNode.active = !1;
this.lockNode.active = !1;
this.saveAnswer2LevelPath();
} else {
this.playingNode.active = !1;
this.lockNode.active = !0;
this.lockIcon.getComponent(cc.Sprite).spriteFrame = this.lockSprite[1];
0 == o ? r.noPassNodeIdx > 19 && (r.noPassNodeIdx = 19) : r.noPassNodeIdx > o - 1 && (r.noPassNodeIdx = o - 1);
} else {
this.playingNode.active = !1;
this.lockNode.active = !0;
i + 1 == this.m_Level ? this.lockIcon.getComponent(cc.Sprite).spriteFrame = this.lockSprite[2] : this.lockIcon.getComponent(cc.Sprite).spriteFrame = this.lockSprite[0];
}
if (0 == o && r.noPassNodeIdx < 20) {
r.node.children[r.noPassNodeIdx].getComponent("InnerBox").playingNode.active = !0;
r.node.children[r.noPassNodeIdx].getComponent("InnerBox").lockNode.active = !1;
}
};
o([ c(cc.Prefab) ], t.prototype, "smallInnerPrefab", void 0);
o([ c(cc.Node) ], t.prototype, "parentNode", void 0);
o([ c({
type: cc.Integer
}) ], t.prototype, "maxItemWidth", void 0);
o([ c(cc.Node) ], t.prototype, "lockNode", void 0);
o([ c(cc.Node) ], t.prototype, "playingNode", void 0);
o([ c(cc.Node) ], t.prototype, "lockIcon", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "lockSprite", void 0);
return o([ s ], t);
}(cc.Component);
i.default = m;
cc._RF.pop();
}, {
"./Bean/UserData": "UserData",
"./Common/StorageUtil": "StorageUtil",
"./GlobalConst": "GlobalConst",
"./LevelData": "LevelData",
"./LevelUtils": "LevelUtils"
} ],
InnerLevelControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "88dc7S49FxO5a33b63c0O4e", "InnerLevelControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./Common/StorageUtil"), h = e("./GlobalConst"), u = e("./InnerBox"), f = e("./LevelData"), d = e("./base/BaseLayer"), p = window.wx, m = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.gameNode = null;
t.m_InnerBoxPrefab = null;
t.m_BottomView = null;
t.m_Bg = null;
t.themeNode = null;
t.m_InnerBoxArray = new Array();
t.m_RowNum = 5;
t.m_ColNum = 4;
t.m_BoxWidth = 0;
t.m_BoxHeight = 0;
t.m_Theme = 1;
t.m_Chapter = 1;
t.globalConst = new h.default();
t.chapterCount = 0;
t.levelCount = 0;
t.noPassNodeIdx = 20;
t.storageUtil = new l.default();
return t;
}
t.prototype.onLoad = function() {
this.chapterCount = this.globalConst.chapterCount;
this.levelCount = this.globalConst.levelCount;
this.initBoxArray();
};
t.prototype.initBoxArray = function() {
var e = this.node.getComponent(cc.Layout);
this.m_BoxWidth = (this.node.width - (this.m_ColNum - 1) * e.spacingX) / (this.m_ColNum + 1);
e.paddingLeft = .5 * this.m_BoxWidth;
e.paddingRight = .5 * this.m_BoxWidth;
for (var t = 0; t < this.m_RowNum; t++) {
this.m_InnerBoxArray.push([]);
for (var i = 0; i < this.m_ColNum; i++) {
var n = cc.instantiate(this.m_InnerBoxPrefab);
n.getComponent(u.default).setThemeView(this.themeNode);
this.m_InnerBoxArray[t][i] = n;
this.m_InnerBoxArray[t][i].width = this.m_BoxWidth - 1;
this.m_InnerBoxArray[t][i].height = this.m_BoxWidth - 1;
this.node.addChild(this.m_InnerBoxArray[t][i]);
}
}
};
t.prototype.onLevelSelected = function(e) {
cc.find("Canvas").getComponent("MainControl").hideBanner();
this.node.getParent().active = !1;
this.gameNode.getParent().active = !0;
this.gameNode.getComponent("GameControl").setAnswerShare(!1, null);
this.gameNode.getComponent("GameControl").playSelectedLevel(e, !1);
};
t.prototype.loadInnerBoxState = function() {
for (var e = 1, t = 0; t < this.m_RowNum; t++) for (var i = 0; i < this.m_ColNum; i++) {
this.m_InnerBoxArray[t][i].getComponent("InnerBox").init((this.m_Theme - 1) * this.chapterCount * this.levelCount + (this.m_Chapter - 1) * this.levelCount + e);
e++;
}
this.changeBg();
};
t.prototype.initLevelData = function(e, t) {
cc.log(e, t, "theme, chapter+++++");
this.noPassNodeIdx = 20;
this.m_Theme = e;
this.m_Chapter = t;
p.aldSendEvent("关卡选择界面曝光", {
"主题": this.m_Theme,
"子类别": this.m_Chapter
});
for (var i = 1, n = 0; n < this.m_RowNum; n++) for (var r = 0; r < this.m_ColNum; r++) {
this.m_InnerBoxArray[n][r].getComponent("InnerBox").init((this.m_Theme - 1) * this.chapterCount * this.levelCount + (this.m_Chapter - 1) * this.levelCount + i);
i++;
}
this.changeBg();
var o = cc.find("Canvas").getComponent("MainControl");
o.showBanner(o.InnerSelectBanner, function() {}.bind(this));
};
t.prototype.changeBg = function() {
var e = this;
this.m_BottomView.spriteFrame = null;
var t = (this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter;
this.boxChapter = t;
var i = new f.default().currentUrl + "/themeIcon/bg" + t + ".jpg";
this.m_Bg.boxChapter != t && (this.m_Bg.spriteFrame = null);
this.m_Bg.boxChapter = t;
console.log("+++++++++++++++++++++++++++");
this.LoadNetImg(i, function(i) {
if (e.boxChapter == t) {
var n = new cc.SpriteFrame(i);
cc.loader.setAutoReleaseRecursively(n, !0);
if (e.m_Bg.boxChapter == t) {
e.m_Bg.node.parent.getComponent(cc.Widget).updateAlignment();
if (e.m_Bg.node.parent.height > i.height) {
e.m_Bg.node.height = e.m_Bg.node.parent.height;
console.log(e.m_Bg.node.height);
e.m_Bg.node.width = e.m_Bg.node.height / i.height * i.width;
console.log(e.m_Bg.node.width);
}
e.m_Bg.node.y = -e.m_Bg.node.parent.height / 2;
e.m_Bg.spriteFrame = n;
}
}
});
};
o([ c(cc.Node) ], t.prototype, "gameNode", void 0);
o([ c(cc.Prefab) ], t.prototype, "m_InnerBoxPrefab", void 0);
o([ c(cc.Sprite) ], t.prototype, "m_BottomView", void 0);
o([ c(cc.Sprite) ], t.prototype, "m_Bg", void 0);
o([ c(cc.Node) ], t.prototype, "themeNode", void 0);
return o([ s ], t);
}(d.BaseLayer);
i.default = m;
cc._RF.pop();
}, {
"./Common/StorageUtil": "StorageUtil",
"./GlobalConst": "GlobalConst",
"./InnerBox": "InnerBox",
"./LevelData": "LevelData",
"./base/BaseLayer": "BaseLayer"
} ],
InviteControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "1b08dNk4GNPTox5hNDbbqX6", "InviteControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("./Common/HttpRequest"), s = e("./Common/StorageUtil"), c = cc._decorator, l = c.ccclass, h = c.property, u = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.inviteItem = null;
t.m_ArrayLength = 5;
t.m_InviteListArray = new Array();
t.wxCommon = null;
t.httpRequest = new a.default();
t.storageUtill = new s.default();
return t;
}
t.prototype.onLoad = function() {};
t.prototype.start = function() {};
t.prototype.initListView = function() {
this.node.getParent().getParent().height = .57 * cc.winSize.height;
this.node.getParent().getParent().height, this.m_ArrayLength;
this.node.removeAllChildren();
this.m_InviteListArray.splice(0, this.m_InviteListArray.length);
for (var e = this.storageUtill.getNumberData(this.getDate() + "inviteNum", 0), t = 0; t < e; t++) {
var i = cc.instantiate(this.inviteItem).getComponent("InviteItem");
this.m_InviteListArray.push(i.node);
i.setIndex(t + 1);
i.setIsInvited(!0);
this.node.addChild(i.node);
}
for (t = e; t < this.m_ArrayLength; t++) {
i = cc.instantiate(this.inviteItem).getComponent("InviteItem");
this.m_InviteListArray.push(i.node);
i.setIndex(t + 1);
i.setIsInvited(!1);
console.log("invite item height = " + i.node.height);
this.node.addChild(i.node);
}
this.node.getComponent(cc.Layout).updateLayout();
};
t.prototype.getDate = function() {
var e = new Date();
return e.getFullYear() + "-" + e.getMonth() + "-" + e.getDate();
};
t.prototype.setData = function() {
this.node.removeAllChildren();
var e = this.storageUtill.getData("userData");
this.httpRequest.queryShareUser(e.uid, 1, 20, function(e) {
console.log("queryShareUser ", e);
for (var t = e.data.length > 5 ? 5 : e.data.length, i = 0; i < t; i++) {
var n = e.data[i];
(r = cc.instantiate(this.inviteItem).getComponent("InviteItem")).setIndex(i + 1);
r.setData(n);
this.node.addChild(r.node);
}
for (i = t; i < 5; i++) {
var r;
(r = cc.instantiate(this.inviteItem).getComponent("InviteItem")).setIndex(i + 1);
r.setIsInvited(!1);
this.node.addChild(r.node);
}
}.bind(this));
};
t.prototype.updateList = function() {
console.log("updateList");
this.initListView();
this.setData();
};
o([ h(cc.Prefab) ], t.prototype, "inviteItem", void 0);
return o([ l ], t);
}(cc.Component);
i.default = u;
cc._RF.pop();
}, {
"./Common/HttpRequest": "HttpRequest",
"./Common/StorageUtil": "StorageUtil"
} ],
InviteItem: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "65f63pUSt1GYLCBGNrxYC2v", "InviteItem");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("./Common/WXCommon"), s = e("./Common/HttpRequest"), c = e("./Common/StorageUtil"), l = e("./CoinsUtil"), h = e("./GlobalConst"), u = cc._decorator, f = u.ccclass, d = u.property, p = window.wx, m = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.icon = null;
t.inviteBtn = null;
t.inviteLabel = null;
t.coinLabel = null;
t.inviteFrame = null;
t.inviteFinishFrame = null;
t.getGoldFrame = null;
t.getGoldFinishFrame = null;
t.defaultHeader = null;
t.activeHeader = null;
t.getWxInfoDialog = null;
t.divider = null;
t.wxCommon = new a.default();
t.httpRequest = new s.default();
t.storageUtill = new c.default();
t.data = null;
t.goldNum = 20;
t.coinsUtil = new l.default();
t.globalConst = new h.default();
t.index = 0;
return t;
}
t.prototype.setIsInvited = function() {
this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.inviteFrame;
};
t.prototype.start = function() {};
t.prototype.setData = function(e) {
this.data = e;
console.log("setData ", e);
this.icon.spriteFrame = this.activeHeader;
if (null == e.rewardStatus || "" == e.rewardStatus || "0" == e.rewardStatus) {
var t = this.storageUtill.getData("wxUserInfo");
p.aldSendEvent("邀请按钮曝光", {
"位置": this.index,
uid: t.uid
});
this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.getGoldFrame;
} else {
this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.getGoldFinishFrame;
this.inviteBtn.parent.getComponent(cc.Button).interactable = !1;
}
};
t.prototype.setIndex = function(e) {
this.index = e;
this.inviteLabel.string = "第" + e + "位好友";
this.icon.node.width = .6 * this.node.height;
this.icon.node.height = this.icon.node.width;
this.inviteBtn.height = .8 * this.node.height;
this.inviteBtn.width = 2 * this.inviteBtn.height;
this.icon.spriteFrame = this.defaultHeader;
this.divider.y = -this.node.height / 2 + 2;
this.coinLabel.string = "奖励猫币" + this.globalConst.inviteCoin * e + "枚";
this.goldNum = this.globalConst.inviteCoin * e;
};
t.prototype.onShareClick = function() {
if (this.inviteBtn.getComponent(cc.Sprite).spriteFrame == this.getGoldFrame) {
var e = this.storageUtill.getData("userData");
this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.getGoldFinishFrame;
this.inviteBtn.parent.getComponent(cc.Button).interactable = !1;
this.coinsUtil.addCoins(this.goldNum);
var t = this.storageUtill.getData("wxUserInfo");
p.aldSendEvent("邀请领取按钮点击", {
"位置": this.index,
uid: t.uid,
"猫币值": this.goldNum
});
cc.find("Canvas").getComponent("MainControl").getVolumeControl().playCountDownAudio();
this.httpRequest.uploadReward(e.uid, this.data.uid, 1, function() {}.bind(this));
} else if (cc.sys.platform == cc.sys.WECHAT_GAME) {
null == this.wxCommon && (this.wxCommon = new a.default());
if (null == (t = this.storageUtill.getData("wxUserInfo")).nickName || "" == t.nickName) {
var i = cc.instantiate(this.getWxInfoDialog);
p.aldSendEvent("邀请授权曝光");
i.getComponent("GetWXInfoDialog").setContent("允许获取头像和昵称可领取邀请奖励");
i.getComponent("GetWXInfoDialog").showWxDialog(function() {
p.aldSendEvent("邀请授权成功");
var e = this.storageUtill.getData("wxUserInfo");
this.wxCommon.inviteFriends("", "", e.uid);
p.aldSendEvent("邀请按钮点击", {
"位置": this.index,
uid: e.uid
});
var t = this.storageUtill.getNumberData(this.getDate() + "inviteNum", 0);
this.storageUtill.saveNumberData(this.getDate() + "inviteNum", t++);
this.inviteBtn.getComponent(cc.Sprite).spriteFrame = this.inviteFinishFrame;
this.inviteBtn.parent.getComponent(cc.Button).interactable = !1;
}.bind(this));
cc.find("Canvas").addChild(i);
} else {
this.wxCommon.inviteFriends("", "", t.uid);
p.aldSendEvent("邀请按钮点击", {
"位置": this.index,
uid: t.uid
});
var n = this.storageUtill.getNumberData(this.getDate() + "inviteNum", 0);
this.storageUtill.saveNumberData(this.getDate() + "inviteNum", n++);
}
}
};
t.prototype.getDate = function() {
var e = new Date();
return e.getFullYear() + "-" + e.getMonth() + "-" + e.getDate();
};
o([ d(cc.Sprite) ], t.prototype, "icon", void 0);
o([ d(cc.Node) ], t.prototype, "inviteBtn", void 0);
o([ d(cc.Label) ], t.prototype, "inviteLabel", void 0);
o([ d(cc.Label) ], t.prototype, "coinLabel", void 0);
o([ d(cc.SpriteFrame) ], t.prototype, "inviteFrame", void 0);
o([ d(cc.SpriteFrame) ], t.prototype, "inviteFinishFrame", void 0);
o([ d(cc.SpriteFrame) ], t.prototype, "getGoldFrame", void 0);
o([ d(cc.SpriteFrame) ], t.prototype, "getGoldFinishFrame", void 0);
o([ d(cc.SpriteFrame) ], t.prototype, "defaultHeader", void 0);
o([ d(cc.SpriteFrame) ], t.prototype, "activeHeader", void 0);
o([ d(cc.Prefab) ], t.prototype, "getWxInfoDialog", void 0);
o([ d(cc.Node) ], t.prototype, "divider", void 0);
return o([ f ], t);
}(cc.Component);
i.default = m;
cc._RF.pop();
}, {
"./CoinsUtil": "CoinsUtil",
"./Common/HttpRequest": "HttpRequest",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./GlobalConst": "GlobalConst"
} ],
JsBridge: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "1f0ferKqUpK35dEnfQ4avhZ", "JsBridge");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.JsBridge = void 0;
var n = e("./MethodManager"), r = e("./CallNative"), o = function() {
function e() {}
e.callWithCallback = function(e, t, i) {
var o = {};
o.cmd = e;
o.msg = t;
null != i && n.MethodManager.instance.addMethod(e, i);
r.CallNative.callNative(JSON.stringify(o));
};
e.isAndroidPlatform = function() {
return cc.sys.platform == cc.sys.ANDROID;
};
e.isIosPlatform = function() {
return cc.sys.platform == cc.sys.IPHONE || cc.sys.platform == cc.sys.IPAD;
};
e.isMobile = function() {
return this.isAndroidPlatform() || this.isIosPlatform();
};
e.IsNull = function(e) {
return null === e || "" === e || "null" === e || "Null" === e || "NULL" === e || void 0 === e;
};
e.PAY = "PAY";
e.CONSUME = "CONSUME";
e.DEVICE = "DEVICE";
e.AD_REWARD = "AD_REWARD";
e.AD_BANNER = "AD_BANNER";
e.AD_SPLASH = "AD_SPLASH";
e.AD_BANNER_HIDE = "AD_BANNER_HIDE";
e.TOAST = "TOAST";
e.MEMBERSHIP = "MEMBERSHIP";
e.SAVE = "SAVE";
return e;
}();
i.JsBridge = o;
cc._RF.pop();
}, {
"./CallNative": "CallNative",
"./MethodManager": "MethodManager"
} ],
LevelData: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "e6447Ke1AhLDoDzRRg9/n5w", "LevelData");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.currentColor = "#000000";
this.currentUrl = "https://rescuebeauty.weililiang1.com/theme_20231027";
this.currentPathIndex = new Array();
this.perfect = !1;
this.rowNum = 1;
this.colNum = 1;
this.itemWidth = 1;
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
LevelUtils: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "64da54iBVlAl4HQNQUqh/tf", "LevelUtils");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, window.wx), s = function() {
function e() {
this.theme_config = "";
}
e.prototype.loadThemeJson = function(e) {
"" === this.theme_config ? cc.loader.loadRes("theme", cc.JsonAsset, function(t, i) {
if (!t) {
var n = i.json;
this.theme_config = n;
e(n);
}
}.bind(this)) : e(this.theme_config);
};
e.prototype.loadLevelJson = function(e, t, i) {
var n = e.toString() + "/" + t.toString();
console.log("levelData loadLevelJson jsonFileName  = ", n);
cc.loader.loadRes(n, cc.JsonAsset, function(e, t) {
console.log("levelData loadLevelJson err  = ", e);
console.log("levelData loadLevelJson jsonAsset  = ", t);
if (e) this.plsWaitUpdate(); else {
var n = t.json.json;
i(n);
}
}.bind(this));
};
e.prototype.getBinString = function(e) {
for (var t = "", i = 0; i < e.length; i++) {
var n = e.charAt(i);
t += this.hex_to_bin(n);
}
return t;
};
e.prototype.hex_to_bin = function(e) {
for (var t = [ {
key: 0,
val: "0000"
}, {
key: 1,
val: "0001"
}, {
key: 2,
val: "0010"
}, {
key: 3,
val: "0011"
}, {
key: 4,
val: "0100"
}, {
key: 5,
val: "0101"
}, {
key: 6,
val: "0110"
}, {
key: 7,
val: "0111"
}, {
key: 8,
val: "1000"
}, {
key: 9,
val: "1001"
}, {
key: "a",
val: "1010"
}, {
key: "b",
val: "1011"
}, {
key: "c",
val: "1100"
}, {
key: "d",
val: "1101"
}, {
key: "e",
val: "1110"
}, {
key: "f",
val: "1111"
} ], i = "", n = 0; n < e.length; n++) for (var r = 0; r < t.length; r++) if (e.charAt(n) == t[r].key) {
i = i.concat(t[r].val);
break;
}
return i;
};
e.prototype.hex_to_levelData = function(e) {
var t = [], i = e;
"1111" == i.substring(0, 4) && (i = i.substring(6));
var n = i.substring(0, 4), r = i.substring(4, 8);
i = i.substring(8);
var o = parseInt(n, 2), a = parseInt(r, 2);
t.push([ o, a ]);
for (;i.length >= 2; ) {
var s = i.substring(0, 2);
"00" == s && (a -= 1);
"01" == s && (a += 1);
"10" == s && (o -= 1);
"11" == s && (o += 1);
t.push([ o, a ]);
i = i.substring(2);
}
return t;
};
e.prototype.plsWaitUpdate = function() {
cc.find("Canvas").getComponent("MainControl").back2LaunchView();
cc.sys.platform == cc.sys.WECHAT_GAME && a.showToast({
title: "恭喜您闯关成功，敬请期待新的挑战！",
icon: "none",
duration: 2e3
});
};
return n([ o ], e);
}();
i.default = s;
cc._RF.pop();
}, {} ],
LimitFailControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "6aff81m2C1NwZaBruuXuj9V", "LimitFailControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./Common/WXCommon"), h = e("./SubDomainControl"), u = e("./Common/StorageUtil"), f = e("../i18n/i18nMgr"), d = window.wx, p = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.currentLabel = null;
t.overPlayerLabel = null;
t.percentLabel = null;
t.progressBar = null;
t.bgSprite = null;
t.subView = null;
t.gameAdviceBorder = null;
t.reviveBtn = null;
t.currentLevel = 0;
t.wxCommon = new l.default();
t.subDomainControl = new h.default();
t.storageUtil = new u.default();
t.userData = null;
t.iconAd = null;
t.gameAdshow = !0;
return t;
}
t.prototype.onLoad = function() {};
t.prototype.showDialog = function(e) {
this.node.active = !0;
this.currentLevel = e;
this.currentLabel.string = f.i18nMgr._getLabel("txt_06").replace("&", this.currentLevel.toString());
this.userData = this.storageUtil.getData("userData");
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
if (this.userData.limitLevel < e) {
this.userData.limitLevel = e;
this.storageUtil.saveData("userData", this.userData);
this.wxCommon.saveLimitDataToWx(this.currentLevel);
}
var t = cc.find("Canvas").getComponent("MainControl");
this.resetBannerPos(this.reviveBtn);
t.showBanner(t.LimitBanner, function(e) {
this.moveBanner(e, this.reviveBtn);
}.bind(this));
this.subView.active = !0;
}
};
t.prototype.closeDialog = function() {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
this.subView.active = !1;
cc.find("Canvas").getComponent("MainControl").hideBanner();
}
};
t.prototype.showGameIcon = function() {
var e = this;
this.iconAd = null;
var t = d.getSystemInfoSync(), i = t.screenHeight, n = t.screenWidth, r = this.gameAdviceBorder.getParent().convertToWorldSpaceAR(this.gameAdviceBorder.getPosition()), o = (this.node.convertToNodeSpaceAR(r).y, 
i / 2 - this.gameAdviceBorder.y), a = n / 2, s = .14 * n, c = o, l = o - 1.4 * s;
if (this.storageUtil.isSpaceShapeScreen()) {
c = c;
l = o - 1.4 * s;
} else c = c;
var h = s + 20;
d.createGameIcon && (this.iconAd = d.createGameIcon({
adUnitId: "PBgAAswp0YQ4591A",
count: 8,
style: [ {
appNameHidden: !1,
color: "#A65309 ",
size: s,
left: a - 2 * h,
borderWidth: 2,
borderColor: "#FFFFFF",
top: l
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a - 1 * h,
borderWidth: 2,
borderColor: "#FFFFFF",
top: l
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a,
borderWidth: 2,
borderColor: "#FFFFFF",
top: l
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a + 1 * h,
borderWidth: 2,
borderColor: "#FFFFFF",
top: l
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a - 2 * h,
borderWidth: 2,
borderColor: "#FFFFFF",
top: c
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a - 1 * h,
borderWidth: 2,
borderColor: "#FFFFFF",
top: c
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a,
borderWidth: 2,
borderColor: "#FFFFFF",
top: c
}, {
appNameHidden: !1,
color: "#A65309",
size: s,
left: a + 1 * h,
borderWidth: 2,
borderColor: "#FFFFFF",
top: c
} ]
}));
this.iconAd && this.iconAd.load().then(function() {
e.gameAdshow && e.iconAd.show();
}).catch(function(e) {
console.error(e);
});
};
t.prototype.justHideAd = function() {
this.gameAdshow = !1;
null != this.iconAd && this.iconAd.hide();
};
t.prototype.showAd = function() {
this.gameAdshow = !0;
null != this.iconAd && this.iconAd.show();
};
t.prototype.hideGameAd = function() {
this.gameAdshow = !1;
if (null != this.iconAd) {
this.iconAd.hide();
this.iconAd = null;
}
};
t.prototype.resetBannerPos = function() {};
t.prototype.moveBanner = function() {};
o([ c(cc.Label) ], t.prototype, "currentLabel", void 0);
o([ c(cc.Label) ], t.prototype, "overPlayerLabel", void 0);
o([ c(cc.Label) ], t.prototype, "percentLabel", void 0);
o([ c(cc.Node) ], t.prototype, "progressBar", void 0);
o([ c(cc.Sprite) ], t.prototype, "bgSprite", void 0);
o([ c(cc.Node) ], t.prototype, "subView", void 0);
o([ c(cc.Node) ], t.prototype, "gameAdviceBorder", void 0);
o([ c(cc.Node) ], t.prototype, "reviveBtn", void 0);
return o([ s ], t);
}(cc.Component);
i.default = p;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./SubDomainControl": "SubDomainControl"
} ],
ListItem: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "e7dc6ZAYPVLJKxNcW8aTO1J", "ListItem");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a, s = cc._decorator, c = s.ccclass, l = s.property, h = s.disallowMultiple, u = s.menu, f = s.executionOrder;
(function(e) {
e[e.NONE = 0] = "NONE";
e[e.TOGGLE = 1] = "TOGGLE";
e[e.SWITCH = 2] = "SWITCH";
})(a || (a = {}));
var d = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.icon = null;
t.title = null;
t.selectedMode = a.NONE;
t.selectedFlag = null;
t.selectedSpriteFrame = null;
t._unselectedSpriteFrame = null;
t.adaptiveSize = !1;
t._selected = !1;
t._eventReg = !1;
return t;
}
Object.defineProperty(t.prototype, "selected", {
get: function() {
return this._selected;
},
set: function(e) {
this._selected = e;
if (this.selectedFlag) switch (this.selectedMode) {
case a.TOGGLE:
this.selectedFlag.active = e;
break;

case a.SWITCH:
var t = this.selectedFlag.getComponent(cc.Sprite);
t && (t.spriteFrame = e ? this.selectedSpriteFrame : this._unselectedSpriteFrame);
}
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "btnCom", {
get: function() {
this._btnCom || (this._btnCom = this.node.getComponent(cc.Button));
return this._btnCom;
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
if (this.selectedMode == a.SWITCH) {
var e = this.selectedFlag.getComponent(cc.Sprite);
this._unselectedSpriteFrame = e.spriteFrame;
}
};
t.prototype.onDestroy = function() {
this.node.off(cc.Node.EventType.SIZE_CHANGED, this._onSizeChange, this);
};
t.prototype._registerEvent = function() {
if (!this._eventReg) {
this.btnCom && this.list.selectedMode > 0 && this.btnCom.clickEvents.unshift(this.createEvt(this, "onClickThis"));
this.adaptiveSize && this.node.on(cc.Node.EventType.SIZE_CHANGED, this._onSizeChange, this);
this._eventReg = !0;
}
};
t.prototype._onSizeChange = function() {
this.list._onItemAdaptive(this.node);
};
t.prototype.createEvt = function(e, t, i) {
void 0 === i && (i = null);
if (e.isValid) {
e.comName = e.comName || e.name.match(/\<(.*?)\>/g).pop().replace(/\<|>/g, "");
var n = new cc.Component.EventHandler();
n.target = i || e.node;
n.component = e.comName;
n.handler = t;
return n;
}
};
t.prototype.showAni = function(e, t, i) {
var n, r = this;
switch (e) {
case 0:
n = cc.tween(r.node).to(.2, {
scale: .7
}).by(.3, {
y: 2 * r.node.height
});
break;

case 1:
n = cc.tween(r.node).to(.2, {
scale: .7
}).by(.3, {
x: 2 * r.node.width
});
break;

case 2:
n = cc.tween(r.node).to(.2, {
scale: .7
}).by(.3, {
y: -2 * r.node.height
});
break;

case 3:
n = cc.tween(r.node).to(.2, {
scale: .7
}).by(.3, {
x: -2 * r.node.width
});
break;

default:
n = cc.tween(r.node).to(.3, {
scale: .1
});
}
(t || i) && n.call(function() {
if (i) {
r.list._delSingleItem(r.node);
for (var e = r.list.displayData.length - 1; e >= 0; e--) if (r.list.displayData[e].id == r.listId) {
r.list.displayData.splice(e, 1);
break;
}
}
t();
});
n.start();
};
t.prototype.onClickThis = function() {
this.list.selectedId = this.listId;
};
o([ l({
type: cc.Sprite,
tooltip: !1
}) ], t.prototype, "icon", void 0);
o([ l({
type: cc.Node,
tooltip: !1
}) ], t.prototype, "title", void 0);
o([ l({
type: cc.Enum(a),
tooltip: !1
}) ], t.prototype, "selectedMode", void 0);
o([ l({
type: cc.Node,
tooltip: !1,
visible: function() {
return this.selectedMode > a.NONE;
}
}) ], t.prototype, "selectedFlag", void 0);
o([ l({
type: cc.SpriteFrame,
tooltip: !1,
visible: function() {
return this.selectedMode == a.SWITCH;
}
}) ], t.prototype, "selectedSpriteFrame", void 0);
o([ l({
tooltip: !1
}) ], t.prototype, "adaptiveSize", void 0);
return o([ c, h(), u("自定义组件/List Item"), f(-5001) ], t);
}(cc.Component);
i.default = d;
cc._RF.pop();
}, {} ],
List: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "1465aK3BeFERYvLMRafKnXR", "List");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a, s, c, l = cc._decorator, h = l.ccclass, u = l.property, f = l.disallowMultiple, d = l.menu, p = l.executionOrder, m = l.requireComponent, g = e("./ListItem");
(function(e) {
e[e.NODE = 1] = "NODE";
e[e.PREFAB = 2] = "PREFAB";
})(a || (a = {}));
(function(e) {
e[e.NORMAL = 1] = "NORMAL";
e[e.ADHERING = 2] = "ADHERING";
e[e.PAGE = 3] = "PAGE";
})(s || (s = {}));
(function(e) {
e[e.NONE = 0] = "NONE";
e[e.SINGLE = 1] = "SINGLE";
e[e.MULT = 2] = "MULT";
})(c || (c = {}));
var y = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.templateType = a.NODE;
t.tmpNode = null;
t.tmpPrefab = null;
t._slideMode = s.NORMAL;
t.pageDistance = .3;
t.pageChangeEvent = new cc.Component.EventHandler();
t._virtual = !0;
t.cyclic = !1;
t.lackCenter = !1;
t.lackSlide = !1;
t._updateRate = 0;
t.frameByFrameRenderNum = 0;
t.renderEvent = new cc.Component.EventHandler();
t.selectedMode = c.NONE;
t.repeatEventSingle = !1;
t.selectedEvent = new cc.Component.EventHandler();
t._selectedId = -1;
t._forceUpdate = !1;
t._updateDone = !0;
t._actualNumItems = 0;
t._numItems = 0;
t._inited = !1;
t._needUpdateWidget = !1;
t._aniDelRuning = !1;
t._doneAfterUpdate = !1;
t.adhering = !1;
t._adheringBarrier = !1;
t.curPageNum = 0;
return t;
}
Object.defineProperty(t.prototype, "slideMode", {
get: function() {
return this._slideMode;
},
set: function(e) {
this._slideMode = e;
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "virtual", {
get: function() {
return this._virtual;
},
set: function(e) {
null != e && (this._virtual = e);
0 != this._numItems && this._onScrolling();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "updateRate", {
get: function() {
return this._updateRate;
},
set: function(e) {
e >= 0 && e <= 6 && (this._updateRate = e);
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "selectedId", {
get: function() {
return this._selectedId;
},
set: function(e) {
var t, i = this;
switch (i.selectedMode) {
case c.SINGLE:
if (!i.repeatEventSingle && e == i._selectedId) return;
t = i.getItemByListId(e);
var n = void 0;
i._selectedId >= 0 ? i._lastSelectedId = i._selectedId : i._lastSelectedId = null;
i._selectedId = e;
t && ((n = t.getComponent(g.default)).selected = !0);
if (i._lastSelectedId >= 0 && i._lastSelectedId != i._selectedId) {
var r = i.getItemByListId(i._lastSelectedId);
r && (r.getComponent(g.default).selected = !1);
}
i.selectedEvent && cc.Component.EventHandler.emitEvents([ i.selectedEvent ], t, e % this._actualNumItems, null == i._lastSelectedId ? null : i._lastSelectedId % this._actualNumItems);
break;

case c.MULT:
if (!(t = i.getItemByListId(e))) return;
n = t.getComponent(g.default);
i._selectedId >= 0 && (i._lastSelectedId = i._selectedId);
i._selectedId = e;
var o = !n.selected;
n.selected = o;
var a = i.multSelected.indexOf(e);
o && a < 0 ? i.multSelected.push(e) : !o && a >= 0 && i.multSelected.splice(a, 1);
i.selectedEvent && cc.Component.EventHandler.emitEvents([ i.selectedEvent ], t, e % this._actualNumItems, null == i._lastSelectedId ? null : i._lastSelectedId % this._actualNumItems, o);
}
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "numItems", {
get: function() {
return this._actualNumItems;
},
set: function(e) {
var t = this;
if (t.checkInited(!1)) if (null == e || e < 0) cc.error("numItems set the wrong::", e); else {
t._actualNumItems = t._numItems = e;
t._forceUpdate = !0;
if (t._virtual) {
t._resizeContent();
t.cyclic && (t._numItems = t._cyclicNum * t._numItems);
t._onScrolling();
t.frameByFrameRenderNum || t.slideMode != s.PAGE || (t.curPageNum = t.nearestListId);
} else {
if (t.cyclic) {
t._resizeContent();
t._numItems = t._cyclicNum * t._numItems;
}
var i = t.content.getComponent(cc.Layout);
i && (i.enabled = !0);
t._delRedundantItem();
t.firstListId = 0;
if (t.frameByFrameRenderNum > 0) {
for (var n = t.frameByFrameRenderNum > t._numItems ? t._numItems : t.frameByFrameRenderNum, r = 0; r < n; r++) t._createOrUpdateItem2(r);
if (t.frameByFrameRenderNum < t._numItems) {
t._updateCounter = t.frameByFrameRenderNum;
t._updateDone = !1;
}
} else {
for (r = 0; r < t._numItems; r++) t._createOrUpdateItem2(r);
t.displayItemNum = t._numItems;
}
}
}
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "scrollView", {
get: function() {
return this._scrollView;
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
this._init();
};
t.prototype.onDestroy = function() {
var e = this;
cc.isValid(e._itemTmp) && e._itemTmp.destroy();
cc.isValid(e.tmpNode) && e.tmpNode.destroy();
e._pool && e._pool.clear();
};
t.prototype.onEnable = function() {
this._registerEvent();
this._init();
if (this._aniDelRuning) {
this._aniDelRuning = !1;
if (this._aniDelItem) {
if (this._aniDelBeforePos) {
this._aniDelItem.position = this._aniDelBeforePos;
delete this._aniDelBeforePos;
}
if (this._aniDelBeforeScale) {
this._aniDelItem.scale = this._aniDelBeforeScale;
delete this._aniDelBeforeScale;
}
delete this._aniDelItem;
}
if (this._aniDelCB) {
this._aniDelCB();
delete this._aniDelCB;
}
}
};
t.prototype.onDisable = function() {
this._unregisterEvent();
};
t.prototype._registerEvent = function() {
var e = this;
e.node.on(cc.Node.EventType.TOUCH_START, e._onTouchStart, e, !0);
e.node.on("touch-up", e._onTouchUp, e);
e.node.on(cc.Node.EventType.TOUCH_CANCEL, e._onTouchCancelled, e, !0);
e.node.on("scroll-began", e._onScrollBegan, e, !0);
e.node.on("scroll-ended", e._onScrollEnded, e, !0);
e.node.on("scrolling", e._onScrolling, e, !0);
e.node.on(cc.Node.EventType.SIZE_CHANGED, e._onSizeChanged, e);
};
t.prototype._unregisterEvent = function() {
var e = this;
e.node.off(cc.Node.EventType.TOUCH_START, e._onTouchStart, e, !0);
e.node.off("touch-up", e._onTouchUp, e);
e.node.off(cc.Node.EventType.TOUCH_CANCEL, e._onTouchCancelled, e, !0);
e.node.off("scroll-began", e._onScrollBegan, e, !0);
e.node.off("scroll-ended", e._onScrollEnded, e, !0);
e.node.off("scrolling", e._onScrolling, e, !0);
e.node.off(cc.Node.EventType.SIZE_CHANGED, e._onSizeChanged, e);
};
t.prototype._init = function() {
var e = this;
if (!e._inited) {
e._scrollView = e.node.getComponent(cc.ScrollView);
e.content = e._scrollView.content;
if (e.content) {
e._layout = e.content.getComponent(cc.Layout);
e._align = e._layout.type;
e._resizeMode = e._layout.resizeMode;
e._startAxis = e._layout.startAxis;
e._topGap = e._layout.paddingTop;
e._rightGap = e._layout.paddingRight;
e._bottomGap = e._layout.paddingBottom;
e._leftGap = e._layout.paddingLeft;
e._columnGap = e._layout.spacingX;
e._lineGap = e._layout.spacingY;
e._colLineNum;
e._verticalDir = e._layout.verticalDirection;
e._horizontalDir = e._layout.horizontalDirection;
e.setTemplateItem(cc.instantiate(e.templateType == a.PREFAB ? e.tmpPrefab : e.tmpNode));
if (e._slideMode == s.ADHERING || e._slideMode == s.PAGE) {
e._scrollView.inertia = !1;
e._scrollView._onMouseWheel = function() {};
}
e.virtual || (e.lackCenter = !1);
e._lastDisplayData = [];
e.displayData = [];
e._pool = new cc.NodePool();
e._forceUpdate = !1;
e._updateCounter = 0;
e._updateDone = !0;
e.curPageNum = 0;
if (e.cyclic) {
e._scrollView._processAutoScrolling = this._processAutoScrolling.bind(e);
e._scrollView._startBounceBackIfNeeded = function() {
return !1;
};
}
switch (e._align) {
case cc.Layout.Type.HORIZONTAL:
switch (e._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
e._alignCalcType = 1;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
e._alignCalcType = 2;
}
break;

case cc.Layout.Type.VERTICAL:
switch (e._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
e._alignCalcType = 3;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
e._alignCalcType = 4;
}
break;

case cc.Layout.Type.GRID:
switch (e._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
switch (e._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
e._alignCalcType = 3;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
e._alignCalcType = 4;
}
break;

case cc.Layout.AxisDirection.VERTICAL:
switch (e._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
e._alignCalcType = 1;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
e._alignCalcType = 2;
}
}
}
e.content.removeAllChildren();
e._inited = !0;
} else cc.error(e.node.name + "'s cc.ScrollView unset content!");
}
};
t.prototype._processAutoScrolling = function(e) {
this._scrollView._autoScrollAccumulatedTime += 1 * e;
var t = Math.min(1, this._scrollView._autoScrollAccumulatedTime / this._scrollView._autoScrollTotalTime);
if (this._scrollView._autoScrollAttenuate) {
var i = t - 1;
t = i * i * i * i * i + 1;
}
var n = this._scrollView._autoScrollStartPosition.add(this._scrollView._autoScrollTargetDelta.mul(t)), r = this._scrollView.getScrollEndedEventTiming(), o = Math.abs(t - 1) <= r;
if (Math.abs(t - 1) <= this._scrollView.getScrollEndedEventTiming() && !this._scrollView._isScrollEndedWithThresholdEventFired) {
this._scrollView._dispatchEvent("scroll-ended-with-threshold");
this._scrollView._isScrollEndedWithThresholdEventFired = !0;
}
o && (this._scrollView._autoScrolling = !1);
var a = n.sub(this._scrollView.getContentPosition());
this._scrollView._moveContent(this._scrollView._clampDelta(a), o);
this._scrollView._dispatchEvent("scrolling");
if (!this._scrollView._autoScrolling) {
this._scrollView._isBouncing = !1;
this._scrollView._scrolling = !1;
this._scrollView._dispatchEvent("scroll-ended");
}
};
t.prototype.setTemplateItem = function(e) {
if (e) {
var t = this;
t._itemTmp = e;
t._resizeMode == cc.Layout.ResizeMode.CHILDREN ? t._itemSize = t._layout.cellSize : t._itemSize = cc.size(e.width, e.height);
var i = e.getComponent(g.default), n = !1;
i || (n = !0);
n && (t.selectedMode = c.NONE);
(i = e.getComponent(cc.Widget)) && i.enabled && (t._needUpdateWidget = !0);
t.selectedMode == c.MULT && (t.multSelected = []);
switch (t._align) {
case cc.Layout.Type.HORIZONTAL:
t._colLineNum = 1;
t._sizeType = !1;
break;

case cc.Layout.Type.VERTICAL:
t._colLineNum = 1;
t._sizeType = !0;
break;

case cc.Layout.Type.GRID:
switch (t._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
var r = t.content.width - t._leftGap - t._rightGap;
t._colLineNum = Math.floor((r + t._columnGap) / (t._itemSize.width + t._columnGap));
t._sizeType = !0;
break;

case cc.Layout.AxisDirection.VERTICAL:
var o = t.content.height - t._topGap - t._bottomGap;
t._colLineNum = Math.floor((o + t._lineGap) / (t._itemSize.height + t._lineGap));
t._sizeType = !1;
}
}
}
};
t.prototype.checkInited = function(e) {
void 0 === e && (e = !0);
if (!this._inited) {
e && cc.error("List initialization not completed!");
return !1;
}
return !0;
};
t.prototype._resizeContent = function() {
var e, t = this;
switch (t._align) {
case cc.Layout.Type.HORIZONTAL:
if (t._customSize) {
var i = t._getFixedSize(null);
e = t._leftGap + i.val + t._itemSize.width * (t._numItems - i.count) + t._columnGap * (t._numItems - 1) + t._rightGap;
} else e = t._leftGap + t._itemSize.width * t._numItems + t._columnGap * (t._numItems - 1) + t._rightGap;
break;

case cc.Layout.Type.VERTICAL:
if (t._customSize) {
i = t._getFixedSize(null);
e = t._topGap + i.val + t._itemSize.height * (t._numItems - i.count) + t._lineGap * (t._numItems - 1) + t._bottomGap;
} else e = t._topGap + t._itemSize.height * t._numItems + t._lineGap * (t._numItems - 1) + t._bottomGap;
break;

case cc.Layout.Type.GRID:
t.lackCenter && (t.lackCenter = !1);
switch (t._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
var n = Math.ceil(t._numItems / t._colLineNum);
e = t._topGap + t._itemSize.height * n + t._lineGap * (n - 1) + t._bottomGap;
break;

case cc.Layout.AxisDirection.VERTICAL:
var r = Math.ceil(t._numItems / t._colLineNum);
e = t._leftGap + t._itemSize.width * r + t._columnGap * (r - 1) + t._rightGap;
}
}
var o = t.content.getComponent(cc.Layout);
o && (o.enabled = !1);
t._allItemSize = e;
t._allItemSizeNoEdge = t._allItemSize - (t._sizeType ? t._topGap + t._bottomGap : t._leftGap + t._rightGap);
if (t.cyclic) {
var a = t._sizeType ? t.node.height : t.node.width;
t._cyclicPos1 = 0;
a -= t._cyclicPos1;
t._cyclicNum = Math.ceil(a / t._allItemSizeNoEdge) + 1;
var s = t._sizeType ? t._lineGap : t._columnGap;
t._cyclicPos2 = t._cyclicPos1 + t._allItemSizeNoEdge + s;
t._cyclicAllItemSize = t._allItemSize + t._allItemSizeNoEdge * (t._cyclicNum - 1) + s * (t._cyclicNum - 1);
t._cycilcAllItemSizeNoEdge = t._allItemSizeNoEdge * t._cyclicNum;
t._cycilcAllItemSizeNoEdge += s * (t._cyclicNum - 1);
}
t._lack = !t.cyclic && t._allItemSize < (t._sizeType ? t.node.height : t.node.width);
var c = t._lack && t.lackCenter || !t.lackSlide ? .1 : 0, l = t._lack ? (t._sizeType ? t.node.height : t.node.width) - c : t.cyclic ? t._cyclicAllItemSize : t._allItemSize;
l < 0 && (l = 0);
t._sizeType ? t.content.height = l : t.content.width = l;
};
t.prototype._onScrolling = function(e) {
void 0 === e && (e = null);
null == this.frameCount && (this.frameCount = this._updateRate);
if (!this._forceUpdate && e && "scroll-ended" != e.type && this.frameCount > 0) this.frameCount--; else {
this.frameCount = this._updateRate;
if (!this._aniDelRuning) {
if (this.cyclic) {
var t = this.content.getPosition();
t = this._sizeType ? t.y : t.x;
var i = this._allItemSizeNoEdge + (this._sizeType ? this._lineGap : this._columnGap), n = this._sizeType ? cc.v2(0, i) : cc.v2(i, 0);
switch (this._alignCalcType) {
case 1:
if (t >= -this._cyclicPos1) {
this.content.x = -this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(n));
} else if (t <= -this._cyclicPos2) {
this.content.x = -this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(n));
}
break;

case 2:
if (t < this._cyclicPos1) {
this.content.x = this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(n));
} else if (t > this._cyclicPos2) {
this.content.x = this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(n));
}
break;

case 3:
if (t < this._cyclicPos1) {
this.content.y = this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(n));
} else if (t > this._cyclicPos2) {
this.content.y = this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(n));
}
break;

case 4:
if (t > -this._cyclicPos1) {
this.content.y = -this._cyclicPos2;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.sub(n));
} else if (t < -this._cyclicPos2) {
this.content.y = -this._cyclicPos1;
this._scrollView.isAutoScrolling() && (this._scrollView._autoScrollStartPosition = this._scrollView._autoScrollStartPosition.add(n));
}
}
}
this._calcViewPos();
var r, o, a, s;
if (this._sizeType) {
r = this.viewTop;
a = this.viewBottom;
} else {
o = this.viewRight;
s = this.viewLeft;
}
if (this._virtual) {
this.displayData = [];
var c = void 0, l = 0, h = this._numItems - 1;
if (this._customSize) {
var u = !1;
for (l = 0; l <= h && !u; l++) {
c = this._calcItemPos(l);
switch (this._align) {
case cc.Layout.Type.HORIZONTAL:
c.right >= s && c.left <= o ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (u = !0);
break;

case cc.Layout.Type.VERTICAL:
c.bottom <= r && c.top >= a ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (u = !0);
break;

case cc.Layout.Type.GRID:
switch (this._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
c.bottom <= r && c.top >= a ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (u = !0);
break;

case cc.Layout.AxisDirection.VERTICAL:
c.right >= s && c.left <= o ? this.displayData.push(c) : 0 != l && this.displayData.length > 0 && (u = !0);
}
}
}
} else {
var f = this._itemSize.width + this._columnGap, d = this._itemSize.height + this._lineGap;
switch (this._alignCalcType) {
case 1:
l = (s - this._leftGap) / f;
h = (o - this._leftGap) / f;
break;

case 2:
l = (-o - this._rightGap) / f;
h = (-s - this._rightGap) / f;
break;

case 3:
l = (-r - this._topGap) / d;
h = (-a - this._topGap) / d;
break;

case 4:
l = (a - this._bottomGap) / d;
h = (r - this._bottomGap) / d;
}
l = Math.floor(l) * this._colLineNum;
h = Math.ceil(h) * this._colLineNum;
l < 0 && (l = 0);
--h >= this._numItems && (h = this._numItems - 1);
for (;l <= h; l++) this.displayData.push(this._calcItemPos(l));
}
this._delRedundantItem();
if (this.displayData.length <= 0 || !this._numItems) {
this._lastDisplayData = [];
return;
}
this.firstListId = this.displayData[0].id;
this.displayItemNum = this.displayData.length;
var p = this._lastDisplayData.length, m = this.displayItemNum != p;
if (m) {
this.frameByFrameRenderNum > 0 && this._lastDisplayData.sort(function(e, t) {
return e - t;
});
m = this.firstListId != this._lastDisplayData[0] || this.displayData[this.displayItemNum - 1].id != this._lastDisplayData[p - 1];
}
if (this._forceUpdate || m) if (this.frameByFrameRenderNum > 0) if (this._numItems > 0) {
this._updateDone ? this._updateCounter = 0 : this._doneAfterUpdate = !0;
this._updateDone = !1;
} else {
this._updateCounter = 0;
this._updateDone = !0;
} else {
this._lastDisplayData = [];
for (var g = 0; g < this.displayItemNum; g++) this._createOrUpdateItem(this.displayData[g]);
this._forceUpdate = !1;
}
this._calcNearestItem();
}
}
}
};
t.prototype._calcViewPos = function() {
var e = this.content.getPosition();
switch (this._alignCalcType) {
case 1:
this.elasticLeft = e.x > 0 ? e.x : 0;
this.viewLeft = (e.x < 0 ? -e.x : 0) - this.elasticLeft;
this.viewRight = this.viewLeft + this.node.width;
this.elasticRight = this.viewRight > this.content.width ? Math.abs(this.viewRight - this.content.width) : 0;
this.viewRight += this.elasticRight;
this.viewLeft = Math.round(this.viewLeft);
this.viewRight = Math.round(this.viewRight);
break;

case 2:
this.elasticRight = e.x < 0 ? -e.x : 0;
this.viewRight = (e.x > 0 ? -e.x : 0) + this.elasticRight;
this.viewLeft = this.viewRight - this.node.width;
this.elasticLeft = this.viewLeft < -this.content.width ? Math.abs(this.viewLeft + this.content.width) : 0;
this.viewLeft -= this.elasticLeft;
break;

case 3:
this.elasticTop = e.y < 0 ? Math.abs(e.y) : 0;
this.viewTop = (e.y > 0 ? -e.y : 0) + this.elasticTop;
this.viewBottom = this.viewTop - this.node.height;
this.elasticBottom = this.viewBottom < -this.content.height ? Math.abs(this.viewBottom + this.content.height) : 0;
this.viewBottom += this.elasticBottom;
break;

case 4:
this.elasticBottom = e.y > 0 ? Math.abs(e.y) : 0;
this.viewBottom = (e.y < 0 ? -e.y : 0) - this.elasticBottom;
this.viewTop = this.viewBottom + this.node.height;
this.elasticTop = this.viewTop > this.content.height ? Math.abs(this.viewTop - this.content.height) : 0;
this.viewTop -= this.elasticTop;
}
};
t.prototype._calcItemPos = function(e) {
var t, i, n, r, o, a, s, c;
switch (this._align) {
case cc.Layout.Type.HORIZONTAL:
switch (this._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
if (this._customSize) {
var l = this._getFixedSize(e);
o = this._leftGap + (this._itemSize.width + this._columnGap) * (e - l.count) + (l.val + this._columnGap * l.count);
t = (h = this._customSize[e]) > 0 ? h : this._itemSize.width;
} else {
o = this._leftGap + (this._itemSize.width + this._columnGap) * e;
t = this._itemSize.width;
}
if (this.lackCenter) {
o -= this._leftGap;
o += this.content.width / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
left: o,
right: a = o + t,
x: o + this._itemTmp.anchorX * t,
y: this._itemTmp.y
};

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
if (this._customSize) {
l = this._getFixedSize(e);
a = -this._rightGap - (this._itemSize.width + this._columnGap) * (e - l.count) - (l.val + this._columnGap * l.count);
t = (h = this._customSize[e]) > 0 ? h : this._itemSize.width;
} else {
a = -this._rightGap - (this._itemSize.width + this._columnGap) * e;
t = this._itemSize.width;
}
if (this.lackCenter) {
a += this._rightGap;
a -= this.content.width / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
right: a,
left: o = a - t,
x: o + this._itemTmp.anchorX * t,
y: this._itemTmp.y
};
}
break;

case cc.Layout.Type.VERTICAL:
switch (this._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
if (this._customSize) {
l = this._getFixedSize(e);
n = -this._topGap - (this._itemSize.height + this._lineGap) * (e - l.count) - (l.val + this._lineGap * l.count);
i = (h = this._customSize[e]) > 0 ? h : this._itemSize.height;
} else {
n = -this._topGap - (this._itemSize.height + this._lineGap) * e;
i = this._itemSize.height;
}
if (this.lackCenter) {
n += this._topGap;
n -= this.content.height / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
top: n,
bottom: r = n - i,
x: this._itemTmp.x,
y: r + this._itemTmp.anchorY * i
};

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
if (this._customSize) {
var h;
l = this._getFixedSize(e);
r = this._bottomGap + (this._itemSize.height + this._lineGap) * (e - l.count) + (l.val + this._lineGap * l.count);
i = (h = this._customSize[e]) > 0 ? h : this._itemSize.height;
} else {
r = this._bottomGap + (this._itemSize.height + this._lineGap) * e;
i = this._itemSize.height;
}
if (this.lackCenter) {
r -= this._bottomGap;
r += this.content.height / 2 - this._allItemSizeNoEdge / 2;
}
return {
id: e,
top: n = r + i,
bottom: r,
x: this._itemTmp.x,
y: r + this._itemTmp.anchorY * i
};
}

case cc.Layout.Type.GRID:
var u = Math.floor(e / this._colLineNum);
switch (this._startAxis) {
case cc.Layout.AxisDirection.HORIZONTAL:
switch (this._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
c = (r = (n = -this._topGap - (this._itemSize.height + this._lineGap) * u) - this._itemSize.height) + this._itemTmp.anchorY * this._itemSize.height;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
n = (r = this._bottomGap + (this._itemSize.height + this._lineGap) * u) + this._itemSize.height;
c = r + this._itemTmp.anchorY * this._itemSize.height;
}
s = this._leftGap + e % this._colLineNum * (this._itemSize.width + this._columnGap);
switch (this._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
s += this._itemTmp.anchorX * this._itemSize.width;
s -= this.content.anchorX * this.content.width;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
s += (1 - this._itemTmp.anchorX) * this._itemSize.width;
s -= (1 - this.content.anchorX) * this.content.width;
s *= -1;
}
return {
id: e,
top: n,
bottom: r,
x: s,
y: c
};

case cc.Layout.AxisDirection.VERTICAL:
switch (this._horizontalDir) {
case cc.Layout.HorizontalDirection.LEFT_TO_RIGHT:
a = (o = this._leftGap + (this._itemSize.width + this._columnGap) * u) + this._itemSize.width;
s = o + this._itemTmp.anchorX * this._itemSize.width;
s -= this.content.anchorX * this.content.width;
break;

case cc.Layout.HorizontalDirection.RIGHT_TO_LEFT:
s = (o = (a = -this._rightGap - (this._itemSize.width + this._columnGap) * u) - this._itemSize.width) + this._itemTmp.anchorX * this._itemSize.width;
s += (1 - this.content.anchorX) * this.content.width;
}
c = -this._topGap - e % this._colLineNum * (this._itemSize.height + this._lineGap);
switch (this._verticalDir) {
case cc.Layout.VerticalDirection.TOP_TO_BOTTOM:
c -= (1 - this._itemTmp.anchorY) * this._itemSize.height;
c += (1 - this.content.anchorY) * this.content.height;
break;

case cc.Layout.VerticalDirection.BOTTOM_TO_TOP:
c -= this._itemTmp.anchorY * this._itemSize.height;
c += this.content.anchorY * this.content.height;
c *= -1;
}
return {
id: e,
left: o,
right: a,
x: s,
y: c
};
}
}
};
t.prototype._calcExistItemPos = function(e) {
var t = this.getItemByListId(e);
if (!t) return null;
var i = {
id: e,
x: t.x,
y: t.y
};
if (this._sizeType) {
i.top = t.y + t.height * (1 - t.anchorY);
i.bottom = t.y - t.height * t.anchorY;
} else {
i.left = t.x - t.width * t.anchorX;
i.right = t.x + t.width * (1 - t.anchorX);
}
return i;
};
t.prototype.getItemPos = function(e) {
return this._virtual ? this._calcItemPos(e) : this.frameByFrameRenderNum ? this._calcItemPos(e) : this._calcExistItemPos(e);
};
t.prototype.IsNull = function(e) {
return null === e || "" === e || "null" === e || "Null" === e || "NULL" === e || void 0 === e;
};
t.prototype._getFixedSize = function(e) {
if (!this._customSize) return null;
null == e && (e = this._numItems);
var t = 0, i = 0;
if (!this.IsNull(this._sizeList) && this._sizeList[e - 1]) return this._sizeList[e - 1];
for (var n in this._customSize) if (parseInt(n) < e) {
t += this._customSize[n];
i++;
}
return {
val: t,
count: i
};
};
t.prototype._onScrollBegan = function() {
this._beganPos = this._sizeType ? this.viewTop : this.viewLeft;
};
t.prototype._onScrollEnded = function() {
var e = this;
e.curScrollIsTouch = !1;
if (null != e.scrollToListId) {
var t = e.getItemByListId(e.scrollToListId);
e.scrollToListId = null;
t && cc.tween(t).to(.1, {
scale: 1.06
}).to(.1, {
scale: 1
}).start();
}
e._onScrolling();
e._slideMode != s.ADHERING || e.adhering ? e._slideMode == s.PAGE && (null != e._beganPos && e.curScrollIsTouch ? this._pageAdhere() : e.adhere()) : e.adhere();
};
t.prototype._onTouchStart = function(e, t) {
if (!this._scrollView.hasNestedViewGroup(e, t)) {
this.curScrollIsTouch = !0;
if (e.eventPhase !== cc.Event.AT_TARGET || e.target !== this.node) {
for (var i = e.target; null == i._listId && i.parent; ) i = i.parent;
this._scrollItem = null != i._listId ? i : e.target;
}
}
};
t.prototype._onTouchUp = function() {
var e = this;
e._scrollPos = null;
if (e._slideMode == s.ADHERING) {
this.adhering && (this._adheringBarrier = !0);
e.adhere();
} else e._slideMode == s.PAGE && (null != e._beganPos ? this._pageAdhere() : e.adhere());
this._scrollItem = null;
};
t.prototype._onTouchCancelled = function(e, t) {
var i = this;
if (!i._scrollView.hasNestedViewGroup(e, t) && !e.simulate) {
i._scrollPos = null;
if (i._slideMode == s.ADHERING) {
i.adhering && (i._adheringBarrier = !0);
i.adhere();
} else i._slideMode == s.PAGE && (null != i._beganPos ? i._pageAdhere() : i.adhere());
this._scrollItem = null;
}
};
t.prototype._onSizeChanged = function() {
this.checkInited(!1) && this._onScrolling();
};
t.prototype._onItemAdaptive = function(e) {
if (!this._sizeType && e.width != this._itemSize.width || this._sizeType && e.height != this._itemSize.height) {
this._customSize || (this._customSize = {});
var t = this._sizeType ? e.height : e.width;
if (this._customSize[e._listId] != t) {
cc.log("不一致的");
this._customSize[e._listId] = t;
this._resizeContent();
this.updateAll();
if (null != this._scrollToListId) {
this._scrollPos = null;
this.unschedule(this._scrollToSo);
this.scrollTo(this._scrollToListId, Math.max(0, this._scrollToEndTime - new Date().getTime() / 1e3));
}
}
}
};
t.prototype._pageAdhere = function() {
var e = this;
if (e.cyclic || !(e.elasticTop > 0 || e.elasticRight > 0 || e.elasticBottom > 0 || e.elasticLeft > 0)) {
var t = e._sizeType ? e.viewTop : e.viewLeft, i = (e._sizeType ? e.node.height : e.node.width) * e.pageDistance;
if (Math.abs(e._beganPos - t) > i) switch (e._alignCalcType) {
case 1:
case 4:
e._beganPos > t ? e.prePage(.2) : e.nextPage(.2);
break;

case 2:
case 3:
e._beganPos < t ? e.prePage(.2) : e.nextPage(.2);
} else e.elasticTop <= 0 && e.elasticRight <= 0 && e.elasticBottom <= 0 && e.elasticLeft <= 0 && e.adhere();
e._beganPos = null;
}
};
t.prototype.adhere = function() {
var e = this;
if (e.checkInited() && !(e.elasticTop > 0 || e.elasticRight > 0 || e.elasticBottom > 0 || e.elasticLeft > 0)) {
e.adhering = !0;
e._calcNearestItem();
var t = (e._sizeType ? e._topGap : e._leftGap) / (e._sizeType ? e.node.height : e.node.width);
e.scrollTo(e.nearestListId, .5, t);
}
};
t.prototype.update = function() {
if (!(this.frameByFrameRenderNum <= 0 || this._updateDone)) if (this._virtual) {
for (var e = this._updateCounter + this.frameByFrameRenderNum > this.displayItemNum ? this.displayItemNum : this._updateCounter + this.frameByFrameRenderNum, t = this._updateCounter; t < e; t++) {
var i = this.displayData[t];
i && this._createOrUpdateItem(i);
}
if (this._updateCounter >= this.displayItemNum - 1) if (this._doneAfterUpdate) {
this._updateCounter = 0;
this._updateDone = !1;
this._doneAfterUpdate = !1;
} else {
this._updateDone = !0;
this._delRedundantItem();
this._forceUpdate = !1;
this._calcNearestItem();
this.slideMode == s.PAGE && (this.curPageNum = this.nearestListId);
} else this._updateCounter += this.frameByFrameRenderNum;
} else if (this._updateCounter < this._numItems) {
for (e = this._updateCounter + this.frameByFrameRenderNum > this._numItems ? this._numItems : this._updateCounter + this.frameByFrameRenderNum, 
t = this._updateCounter; t < e; t++) this._createOrUpdateItem2(t);
this._updateCounter += this.frameByFrameRenderNum;
} else {
this._updateDone = !0;
this._calcNearestItem();
this.slideMode == s.PAGE && (this.curPageNum = this.nearestListId);
}
};
t.prototype._createOrUpdateItem = function(e) {
var t = this.getItemByListId(e.id);
if (t) {
if (this._forceUpdate && this.renderEvent) {
t.setPosition(cc.v2(e.x, e.y));
this._resetItemSize(t);
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], t, e.id % this._actualNumItems);
}
} else {
var i = this._pool.size() > 0;
t = i ? this._pool.get() : cc.instantiate(this._itemTmp);
if (!i || !cc.isValid(t)) {
t = cc.instantiate(this._itemTmp);
i = !1;
}
if (t._listId != e.id) {
t._listId = e.id;
t.setContentSize(this._itemSize);
}
t.setPosition(cc.v2(e.x, e.y));
this._resetItemSize(t);
this.content.addChild(t);
if (i && this._needUpdateWidget) {
var n = t.getComponent(cc.Widget);
n && n.updateAlignment();
}
t.setSiblingIndex(this.content.childrenCount - 1);
var r = t.getComponent(g.default);
t.listItem = r;
if (r) {
r.listId = e.id;
r.list = this;
r._registerEvent();
}
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], t, e.id % this._actualNumItems);
}
this._resetItemSize(t);
this._updateListItem(t.listItem);
this._lastDisplayData.indexOf(e.id) < 0 && this._lastDisplayData.push(e.id);
};
t.prototype._createOrUpdateItem2 = function(e) {
var t, i = this.content.children[e];
if (i) {
if (this._forceUpdate && this.renderEvent) {
i._listId = e;
t && (t.listId = e);
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], i, e % this._actualNumItems);
}
} else {
(i = cc.instantiate(this._itemTmp))._listId = e;
this.content.addChild(i);
t = i.getComponent(g.default);
i.listItem = t;
if (t) {
t.listId = e;
t.list = this;
t._registerEvent();
}
this.renderEvent && cc.Component.EventHandler.emitEvents([ this.renderEvent ], i, e % this._actualNumItems);
}
this._updateListItem(t);
this._lastDisplayData.indexOf(e) < 0 && this._lastDisplayData.push(e);
};
t.prototype._updateListItem = function(e) {
if (e && this.selectedMode > c.NONE) {
var t = e.node;
switch (this.selectedMode) {
case c.SINGLE:
e.selected = this.selectedId == t._listId;
break;

case c.MULT:
e.selected = this.multSelected.indexOf(t._listId) >= 0;
}
}
};
t.prototype._resetItemSize = function() {};
t.prototype._updateItemPos = function(e) {
var t = isNaN(e) ? e : this.getItemByListId(e), i = this.getItemPos(t._listId);
t.setPosition(i.x, i.y);
};
t.prototype.setMultSelected = function(e, t) {
var i = this;
if (i.checkInited()) {
Array.isArray(e) || (e = [ e ]);
if (null == t) i.multSelected = e; else {
var n = void 0, r = void 0;
if (t) for (var o = e.length - 1; o >= 0; o--) {
n = e[o];
(r = i.multSelected.indexOf(n)) < 0 && i.multSelected.push(n);
} else for (o = e.length - 1; o >= 0; o--) {
n = e[o];
(r = i.multSelected.indexOf(n)) >= 0 && i.multSelected.splice(r, 1);
}
}
i._forceUpdate = !0;
i._onScrolling();
}
};
t.prototype.getMultSelected = function() {
return this.multSelected;
};
t.prototype.hasMultSelected = function(e) {
return this.multSelected && this.multSelected.indexOf(e) >= 0;
};
t.prototype.updateItem = function(e) {
if (this.checkInited()) {
Array.isArray(e) || (e = [ e ]);
for (var t = 0, i = e.length; t < i; t++) {
var n = e[t], r = this.getItemByListId(n);
r && cc.Component.EventHandler.emitEvents([ this.renderEvent ], r, n % this._actualNumItems);
}
}
};
t.prototype.updateAll = function() {
this.checkInited() && (this.numItems = this.numItems);
};
t.prototype.getItemByListId = function(e) {
if (this.content) for (var t = this.content.childrenCount - 1; t >= 0; t--) {
var i = this.content.children[t];
if (i._listId == e) return i;
}
};
t.prototype._getOutsideItem = function() {
for (var e, t = [], i = this.content.childrenCount - 1; i >= 0; i--) {
e = this.content.children[i];
this.displayData.find(function(t) {
return t.id == e._listId;
}) || t.push(e);
}
return t;
};
t.prototype._delRedundantItem = function() {
if (this._virtual) for (var e = this._getOutsideItem(), t = e.length - 1; t >= 0; t--) {
var i = e[t];
if (!this._scrollItem || i._listId != this._scrollItem._listId) {
i.isCached = !0;
this._pool.put(i);
for (var n = this._lastDisplayData.length - 1; n >= 0; n--) if (this._lastDisplayData[n] == i._listId) {
this._lastDisplayData.splice(n, 1);
break;
}
}
} else for (;this.content.childrenCount > this._numItems; ) this._delSingleItem(this.content.children[this.content.childrenCount - 1]);
};
t.prototype._delSingleItem = function(e) {
e.removeFromParent();
e.destroy && e.destroy();
e = null;
};
t.prototype.aniDelItem = function(e, t, i) {
var n = this;
if (!n.checkInited() || n.cyclic || !n._virtual) return cc.error("This function is not allowed to be called!");
if (!t) return cc.error("CallFunc are not allowed to be NULL, You need to delete the corresponding index in the data array in the CallFunc!");
if (n._aniDelRuning) return cc.warn("Please wait for the current deletion to finish!");
var r, o = n.getItemByListId(e);
if (o) {
r = o.getComponent(g.default);
n._aniDelRuning = !0;
n._aniDelCB = t;
n._aniDelItem = o;
n._aniDelBeforePos = o.position;
n._aniDelBeforeScale = o.scale;
var a = n.displayData[n.displayData.length - 1].id, s = r.selected;
r.showAni(i, function() {
var i, r, l;
a < n._numItems - 2 && (i = a + 1);
if (null != i) {
var h = n._calcItemPos(i);
n.displayData.push(h);
n._virtual ? n._createOrUpdateItem(h) : n._createOrUpdateItem2(i);
} else n._numItems--;
if (n.selectedMode == c.SINGLE) s ? n._selectedId = -1 : n._selectedId - 1 >= 0 && n._selectedId--; else if (n.selectedMode == c.MULT && n.multSelected.length) {
var u = n.multSelected.indexOf(e);
u >= 0 && n.multSelected.splice(u, 1);
for (var f = n.multSelected.length - 1; f >= 0; f--) (m = n.multSelected[f]) >= e && n.multSelected[f]--;
}
if (n._customSize) {
n._customSize[e] && delete n._customSize[e];
var d = {}, p = void 0;
for (var m in n._customSize) {
p = n._customSize[m];
var g = parseInt(m);
d[g - (g >= e ? 1 : 0)] = p;
}
n._customSize = d;
}
for (f = null != i ? i : a; f >= e + 1; f--) if (o = n.getItemByListId(f)) {
var y = n._calcItemPos(f - 1);
r = cc.tween(o).to(.2333, {
position: cc.v2(y.x, y.y)
});
if (f <= e + 1) {
l = !0;
r.call(function() {
n._aniDelRuning = !1;
t(e);
delete n._aniDelCB;
});
}
r.start();
}
if (!l) {
n._aniDelRuning = !1;
t(e);
n._aniDelCB = null;
}
}, !0);
} else t(e);
};
t.prototype.scrollTo = function(e, t, i, n) {
void 0 === t && (t = .5);
void 0 === i && (i = null);
void 0 === n && (n = !1);
var r = this;
if (r.checkInited(!1)) {
r._scrollView.stopAutoScroll();
null == t ? t = .5 : t < 0 && (t = 0);
e < 0 ? e = 0 : e >= r._numItems && (e = r._numItems - 1);
!r._virtual && r._layout && r._layout.enabled && r._layout.updateLayout();
var o, a, s = r.getItemPos(e);
if (!s) return !1;
switch (r._alignCalcType) {
case 1:
o = s.left;
o -= null != i ? r.node.width * i : r._leftGap;
s = cc.v2(o, 0);
break;

case 2:
o = s.right - r.node.width;
o += null != i ? r.node.width * i : r._rightGap;
s = cc.v2(o + r.content.width, 0);
break;

case 3:
a = s.top;
a += null != i ? r.node.height * i : r._topGap;
s = cc.v2(0, -a);
break;

case 4:
a = s.bottom + r.node.height;
a -= null != i ? r.node.height * i : r._bottomGap;
s = cc.v2(0, -a + r.content.height);
}
var c = r.content.getPosition();
c = Math.abs(r._sizeType ? c.y : c.x);
var l = r._sizeType ? s.y : s.x;
if (Math.abs((null != r._scrollPos ? r._scrollPos : c) - l) > .5) {
r._scrollView.stopAutoScroll();
r._scrollView.scrollToOffset(s, t);
r._scrollToListId = e;
r._scrollToEndTime = new Date().getTime() / 1e3 + t;
r._scrollToSo = r.scheduleOnce(function() {
r._adheringBarrier || (r.adhering = r._adheringBarrier = !1);
r._scrollPos = r._scrollToListId = r._scrollToEndTime = r._scrollToSo = null;
if (n) {
var t = r.getItemByListId(e);
t && cc.tween(t).to(.1, {
scale: 1.05
}).to(.1, {
scale: 1
}).call(function() {}).start();
}
r._onScrolling();
}, t + .1);
t <= 0 && r._onScrolling();
}
}
};
t.prototype._calcNearestItem = function() {
var e, t, i, n, r, o, a = this;
a.nearestListId = null;
a._virtual && a._calcViewPos();
i = a.viewTop;
n = a.viewRight;
r = a.viewBottom;
o = a.viewLeft;
for (var s = !1, c = 0; c < a.content.childrenCount && !s; c += a._colLineNum) if (e = a._virtual ? a.displayData[c] : a._calcExistItemPos(c)) {
t = a._sizeType ? (e.top + e.bottom) / 2 : t = (e.left + e.right) / 2;
switch (a._alignCalcType) {
case 1:
if (e.right >= o) {
a.nearestListId = e.id;
o > t && (a.nearestListId += a._colLineNum);
s = !0;
}
break;

case 2:
if (e.left <= n) {
a.nearestListId = e.id;
n < t && (a.nearestListId += a._colLineNum);
s = !0;
}
break;

case 3:
if (e.bottom <= i) {
a.nearestListId = e.id;
i < t && (a.nearestListId += a._colLineNum);
s = !0;
}
break;

case 4:
if (e.top >= r) {
a.nearestListId = e.id;
r > t && (a.nearestListId += a._colLineNum);
s = !0;
}
}
}
if ((e = a._virtual ? a.displayData[a.displayItemNum - 1] : a._calcExistItemPos(a._numItems - 1)) && e.id == a._numItems - 1) {
t = a._sizeType ? (e.top + e.bottom) / 2 : t = (e.left + e.right) / 2;
switch (a._alignCalcType) {
case 1:
n > t && (a.nearestListId = e.id);
break;

case 2:
o < t && (a.nearestListId = e.id);
break;

case 3:
r < t && (a.nearestListId = e.id);
break;

case 4:
i > t && (a.nearestListId = e.id);
}
}
};
t.prototype.prePage = function(e) {
void 0 === e && (e = .3);
this.checkInited() && this.skipPage(this.curPageNum - 1, e);
};
t.prototype.nextPage = function(e) {
void 0 === e && (e = .3);
this.checkInited() && this.skipPage(this.curPageNum + 1, e);
};
t.prototype.skipPage = function(e, t) {
var i = this;
if (i.checkInited()) {
if (i._slideMode != s.PAGE) return cc.error("This function is not allowed to be called, Must SlideMode = PAGE!");
if (!(e < 0 || e >= i._numItems) && i.curPageNum != e) {
i.curPageNum = e;
i.pageChangeEvent && cc.Component.EventHandler.emitEvents([ i.pageChangeEvent ], e);
i.scrollTo(e, t);
}
}
};
t.prototype.calcCustomSize = function(e) {
var t = this;
if (t.checkInited()) {
if (!t._itemTmp) return cc.error("Unset template item!");
if (!t.renderEvent) return cc.error("Unset Render-Event!");
t._customSize = {};
var i = cc.instantiate(t._itemTmp);
t.content.addChild(i);
for (var n = 0; n < e; n++) {
cc.Component.EventHandler.emitEvents([ t.renderEvent ], i, n);
i.height == t._itemSize.height && i.width == t._itemSize.width || (t._customSize[n] = t._sizeType ? i.height : i.width);
}
Object.keys(t._customSize).length || (t._customSize = null);
i.removeFromParent();
i.destroy && i.destroy();
return t._customSize;
}
};
o([ u({
type: cc.Enum(a),
tooltip: !1
}) ], t.prototype, "templateType", void 0);
o([ u({
type: cc.Node,
tooltip: !1,
visible: function() {
return this.templateType == a.NODE;
}
}) ], t.prototype, "tmpNode", void 0);
o([ u({
type: cc.Prefab,
tooltip: !1,
visible: function() {
return this.templateType == a.PREFAB;
}
}) ], t.prototype, "tmpPrefab", void 0);
o([ u() ], t.prototype, "_slideMode", void 0);
o([ u({
type: cc.Enum(s),
tooltip: !1
}) ], t.prototype, "slideMode", null);
o([ u({
type: cc.Float,
range: [ 0, 1, .1 ],
tooltip: !1,
slide: !0,
visible: function() {
return this._slideMode == s.PAGE;
}
}) ], t.prototype, "pageDistance", void 0);
o([ u({
type: cc.Component.EventHandler,
tooltip: !1,
visible: function() {
return this._slideMode == s.PAGE;
}
}) ], t.prototype, "pageChangeEvent", void 0);
o([ u() ], t.prototype, "_virtual", void 0);
o([ u({
type: cc.Boolean,
tooltip: !1
}) ], t.prototype, "virtual", null);
o([ u({
tooltip: !1,
visible: function() {
var e = this.slideMode == s.NORMAL;
e || (this.cyclic = !1);
return e;
}
}) ], t.prototype, "cyclic", void 0);
o([ u({
tooltip: !1,
visible: function() {
return this.virtual;
}
}) ], t.prototype, "lackCenter", void 0);
o([ u({
tooltip: !1,
visible: function() {
var e = this.virtual && !this.lackCenter;
e || (this.lackSlide = !1);
return e;
}
}) ], t.prototype, "lackSlide", void 0);
o([ u({
type: cc.Integer
}) ], t.prototype, "_updateRate", void 0);
o([ u({
type: cc.Integer,
range: [ 0, 6, 1 ],
tooltip: !1,
slide: !0
}) ], t.prototype, "updateRate", null);
o([ u({
type: cc.Integer,
range: [ 0, 12, 1 ],
tooltip: !1,
slide: !0
}) ], t.prototype, "frameByFrameRenderNum", void 0);
o([ u({
type: cc.Component.EventHandler,
tooltip: !1
}) ], t.prototype, "renderEvent", void 0);
o([ u({
type: cc.Enum(c),
tooltip: !1
}) ], t.prototype, "selectedMode", void 0);
o([ u({
tooltip: !1,
visible: function() {
return this.selectedMode == c.SINGLE;
}
}) ], t.prototype, "repeatEventSingle", void 0);
o([ u({
type: cc.Component.EventHandler,
tooltip: !1,
visible: function() {
return this.selectedMode > c.NONE;
}
}) ], t.prototype, "selectedEvent", void 0);
o([ u({
serializable: !1
}) ], t.prototype, "_numItems", void 0);
return o([ h, f(), d("自定义组件/List"), m(cc.ScrollView), p(-5e3) ], t);
}(cc.Component);
i.default = y;
cc._RF.pop();
}, {
"./ListItem": "ListItem"
} ],
LoadingLabel: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "858cdnK+TxIAKMR8ReATomI", "LoadingLabel");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("../i18n/i18nMgr"), s = e("./Common/StorageUtil"), c = cc._decorator, l = c.ccclass, h = c.property, u = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.label = null;
t.storageUtil = new s.default();
t.costTime = 0;
return t;
}
t.prototype.start = function() {
var e = this.storageUtil.getData("userLang");
null == e && ("zh" === (e = (e = cc.sys.languageCode).split("_")[0].split("-")[0]) ? e = -1 !== (e = cc.sys.languageCode).indexOf("tw") || -1 !== e.indexOf("hk") || -1 !== e.indexOf("mo") ? "ft" : "zh" : "id" === e ? e = "in" : "nb" === e ? e = "no" : "he" === e && (e = "iw"));
a.i18nMgr.setLanguage(e);
};
t.prototype.update = function() {
this.costTime++;
var e = this.costTime % 90, t = a.i18nMgr._getLabel("txt_01");
"txt_01" != t && (this.label.string = e <= 30 ? t + "." : e <= 60 ? t + ".." : t + "...");
};
o([ h(cc.Label) ], t.prototype, "label", void 0);
return o([ l ], t);
}(cc.Component);
i.default = u;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./Common/StorageUtil": "StorageUtil"
} ],
MainControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "83ca4LeQl9DXKiVdlwg/lAY", "MainControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("./base/BaseConfig"), s = cc._decorator, c = s.ccclass, l = s.property, h = e("./SettleUtil"), u = e("./GlobalConst"), f = e("./Bean/UserData"), d = e("./Common/StorageUtil"), p = e("./SubDomainControl"), m = e("./Common/HttpRequest"), g = e("./Bean/WXUserBean"), y = e("./Common/WXCommon"), b = e("./CoinsUtil"), v = e("./Bean/RecordBean"), _ = e("./Bean/PassLevelBean"), w = e("./scrollViewTools/List"), C = e("../i18n/i18nMgr"), S = e("./LevelData"), A = e("./base/BaseLayer"), M = e("./native/JsBridge"), B = e("./base/BaseUtils"), x = window.wx, k = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.PressureDoubleGold = 0;
t.Gold50 = 1;
t.Gold100 = 2;
t.TurnTable = 3;
t.AwardDoubleGold = 4;
t.DayDoubleGold = 5;
t.Revive = 6;
t.UnLockChapter = 7;
t.Notify = 8;
t.NotifyDialog = 9;
t.NextLevel = 10;
t.Reset = 11;
t.unlockTheme = 12;
t.unlockLevel = 13;
t.download = 14;
t.downloadAlbum = 15;
t.SettleViewBanner = 0;
t.AwardBanner = 1;
t.DailyBanner = 2;
t.PressureBanner = 3;
t.LimitBanner = 4;
t.InnerSelectBanner = 5;
t.NotifyGuide = 6;
t.PlayGameBanner = 7;
t.SettleViewInter = 0;
t.launchView = null;
t.themeView = null;
t.gameView = null;
t.innerView = null;
t.gameControlView = null;
t.rankView = null;
t.albumLabel = null;
t.rankTitle = null;
t.rankTopSprite = null;
t.limitPlayBtn = null;
t.volumeControl = null;
t.inviteView = null;
t.inviteControlNode = null;
t.getWxInfoDialog = null;
t.rankScrollView = null;
t.awardView = null;
t.snowPref = null;
t.launchBg = null;
t.dayAwardDialog = null;
t.helperGuideDialog = null;
t.answerSolveView = null;
t.gameRecommendPref = null;
t.gameRecommendNode = null;
t.gainDayAwardBtn = null;
t.dayAwardCheckBox = null;
t.recordShareDialog = null;
t.list = null;
t.list1 = null;
t.list2 = null;
t.list3 = null;
t.settleUtil = new h.default();
t.globalConst = new u.default();
t.subDomainControl = new p.default();
t.userData = null;
t.storageUtil = new d.default();
t.httpRequest = new m.default();
t.wxUserInfo = null;
t.recordData = null;
t.passData = null;
t.wxCommon = new y.default();
t.coinUtil = new b.default();
t.catAudioScheduleID = 0;
t.doubleCheckBoxChecked = !1;
t.coinsUtil = new b.default();
t.requsterLevel = 1;
t.isRequestHelp = !1;
t.isAnswer = !1;
t.isNewPlayer = !1;
t.unLockThemeCount = 1;
t.Membership = !1;
t.SubViewRequestCount = 0;
t.innerBoxArray = new Array();
t.launcheGameNode = null;
t.rewardedVideoAd = null;
t.adPressureDoubleGold = !1;
t.adGold50 = !1;
t.adGold100 = !1;
t.adTurnTable = !1;
t.adAwardDoubleGold = !1;
t.adDayDoubleGold = !1;
t.adRevive = !1;
t.adUnlock = !1;
t.adNotify = !1;
t.adDialogNotify = !1;
t.adNextLevel = !1;
t.adRest = !1;
t.adUnlockTheme = !1;
t.adUnlockLevel = !1;
t.adDownload = !1;
t.adDownloadAlbum = !1;
t.iconAd = null;
t.gameAdshow = !0;
t.bannerAdshow = !0;
t.adTimes = 0;
t.isNetConnect = !0;
t.loadTimer = null;
t.bannerAd = null;
t.interstitialAd = null;
t.langList = {
zh: "简体中文",
ft: "繁體中文",
en: "English",
fr: "Français",
de: "Deutsch",
ru: "Русский",
ja: "日本語",
ko: "한국어",
es: "Español",
ar: "العربية",
pt: "Português",
hi: "हिंदी",
it: "Italiano",
nl: "Nederlands",
tr: "Türkçe",
in: "Bahasa Indonesia",
th: "ไทย",
vi: "Tiếng Việt",
ph: "Filipino",
ms: "Bahasa Melayu",
pl: "Polski",
sv: "Svenska",
no: "Norsk",
da: "Dansk",
el: "Ελληνικά",
fi: "Suomi",
cs: "Čeština",
uk: "Українська",
ro: "Română",
hu: "Magyar",
bg: "Български",
iw: "עברית",
fa: "فارسی",
bn: "বাংলা",
gu: "ગુજરાતી",
mr: "मराठी",
ta: "தமிழ்",
te: "తెలుగు"
};
t.langIdx = 0;
t.colorList = [ "#DDC5FF", "#FFC5CA", "#D1C8FF", "#C9FDF6", "#FFF2C8", "#DCC4FF" ];
t.selectTheme = 1;
t.picLv = 0;
t.passPic = [];
t.albumBoo = !1;
return t;
}
t.prototype.onLoad = function() {
var e = this.storageUtil.getData("unLockThemeCount");
e && (this.unLockThemeCount = e);
this.node.getChildByName("label").getComponent(cc.Label).string = cc.sys.language + "===" + cc.sys.languageCode;
this.initGlobalData();
this.themeView.active = !0;
x.aldSendEvent("主界面加载");
x.aldSendEvent("主界面曝光");
this.userData = this.storageUtil.getData("userData");
null == this.userData && (this.userData = new f.default());
this.wxUserInfo = this.storageUtil.getData("wxUserInfo");
this.themeView.setPosition(0, 0);
this.innerView.setPosition(0, 0);
this.scheduleCatAudio();
this.updateAlbum();
this.Membership = cc.sys.localStorage.getItem("Membership");
if (this.isFirstLaunch()) {
this.storageUtil.saveData("first_launch", !1);
this.storageUtil.saveData("unLockTheme1", !0);
this.storageUtil.saveData("unLockLevel1", 1);
this.storageUtil.saveData("unLockThemeMaxLevel1", 1);
this.adTimes = this.storageUtil.getData("adTimes");
} else {
var t = this.storageUtil.getData("lastTime");
t || (t = 0);
if (!this.Membership) if (this.todayJudge(t)) this.adTimes = this.storageUtil.getData("adTimes"); else {
this.btnOpenShop();
this.storageUtil.saveData("lastTime", new Date().getTime());
this.adTimes = 0;
this.storageUtil.saveData("adTimes", 0);
}
}
};
t.prototype.todayJudge = function(e, t) {
void 0 === t && (t = "timestamp");
"string" == typeof e && "timestamp" == t && (e = Number(e));
"datetime" == t && (e = e.replace(/-/g, "/"));
return new Date().setHours(0, 0, 0, 0) == new Date(e).setHours(0, 0, 0, 0);
};
t.prototype.onStartGameClick = function() {
this.unScheduleCatAudio();
x.aldSendEvent("主界面开始闯关点击");
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
if (this.isFirstLaunch()) {
this.storageUtil.saveData("first_launch", !1);
this.goSeletedLevel(1, !0);
} else {
if (this.isSecondLaunch() && cc.sys.platform == cc.sys.WECHAT_GAME) {
this.wxUserInfo = this.storageUtil.getData("wxUserInfo");
if (null == this.wxUserInfo || null == this.wxUserInfo.nickName) {
var e = cc.instantiate(this.getWxInfoDialog);
x.aldSendEvent("第二次登陆授权曝光");
e.getComponent("GetWXInfoDialog").setContent("允许获取头像和昵称可查看排行榜");
e.getComponent("GetWXInfoDialog").showWxDialog(function() {
x.aldSendEvent("第二次登陆授权成功");
}.bind(this));
this.node.addChild(e);
}
}
this.themeView.getChildByName("ThemeScrollView").getComponent(cc.ScrollView).scrollToTop();
this.themeView.active = !0;
this.themeView.getComponent("ThemeViewControl").refreshTheme();
this.gameView.active = !1;
this.innerView.active = !1;
}
};
t.prototype.back2ThemeView = function() {
if (this.albumBoo) {
this.albumBoo = !1;
this.node.getChildByName("Album").active = !0;
this.list1.node.active = !0;
this.list1.updateAll();
}
this.hideBanner1();
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.innerView.active = !1;
this.themeView.active = !0;
};
t.prototype.back2LaunchView = function() {
this.hideBanner1();
this.gameControlView.getComponent("GameControl").closeAllDialog();
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.themeView.active = !0;
this.gameView.active = !1;
this.innerView.active = !1;
this.scheduleOnce(this.scheduleCatAudio.bind(this), .5);
};
t.prototype.showLimitRankList = function() {
this.unScheduleCatAudio();
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.limitPlayBtn.active = !0;
this.rankTopSprite.spriteFrame = this.limitRankTitle;
};
t.prototype.playLimitGame = function() {
this.unScheduleCatAudio();
x.aldSendEvent("挑战赛开始挑战点击");
this.themeView.active = !1;
this.gameView.active = !0;
this.innerView.active = !1;
this.gameView.getChildByName("GameView").getComponent("GameControl").playLimitGame();
};
t.prototype.showRankList = function() {
x.aldSendEvent("主界面排行榜点击");
this.unScheduleCatAudio();
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.limitPlayBtn.active = !1;
this.rankTopSprite.spriteFrame = this.rankTitle;
};
t.prototype.onRankBackClick = function() {
x.aldSendEvent("主界面曝光");
this.scheduleCatAudio();
};
t.prototype.initWXshare = function() {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
x.showShareMenu();
var e = this.storageUtil.getShareIndex();
x.aldOnShareAppMessage(function() {
return {
title: this.storageUtil.shareTitle[e],
imageUrl: this.storageUtil.shareImageUrl[e]
};
}.bind(this));
var t = x.getLaunchOptionsSync().query;
this.isAnswer = !1;
console.log("queryData = ", t);
var i = null;
null != t && null != t.time && (i = t.time);
if (null != i) {
var n = cc.sys.localStorage.getItem(i);
if ((null == n || this.isNull(n)) && null != t && null != t.type) {
cc.sys.localStorage.setItem(i, 1);
var r = new g.default();
r.uid = t.uid;
r.nickName = t.nickName;
r.avatarUrl = t.avatarUrl;
if (t.type == this.wxCommon.SHARE_INVITE) {
console.log("initWXShare", t);
var o = t.uid;
x.aldSendEvent("邀请分享被点击", {
uid: o
});
var a = this.storageUtil.getData("userData").uid;
this.httpRequest.uploadShareInfo(o, a, 0, function() {});
} else if (t.type == this.wxCommon.SHARE_ANSWER) {
console.log("queryData.isAnswer = " + t.isAnswer);
x.aldSendEvent("求助答案分享被点击", {
"关卡": t.level
});
this.unScheduleCatAudio();
this.gameControlView.getComponent("GameControl").setAnswerShare(!0, r);
this.isAnswer = !0;
this.goSeletedLevel(t.level, this.isAnswer);
} else if (t.type == this.wxCommon.REQUEST_HELPER) {
this.isRequestHelp = !0;
x.aldSendEvent("求助分享被点击");
console.log("queryData.level = " + t.level);
this.unScheduleCatAudio();
this.gameControlView.getComponent("GameControl").setAnswerShare(!1, r);
this.requsterLevel = t.level;
this.helperGuideDialog.getComponent("HelperGuidControl").showGuideDialog(r.nickName, r.avatarUrl);
}
}
}
x.onShow(function(e) {
console.log("WX.onShow res = " + e);
var t = e.query;
console.log("onShow queryData = ", t);
var i = null;
null != t && null != t.time && (i = t.time);
if (null != i) {
var n = cc.sys.localStorage.getItem(i);
if ((null == n || this.isNull(n)) && null != t && null != t.type) {
cc.sys.localStorage.setItem(i, 1);
var r = new g.default();
r.uid = t.uid;
r.nickName = t.nickName;
r.avatarUrl = t.avatarUrl;
if (t.type == this.wxCommon.SHARE_INVITE) {
console.log("initWXShare", t);
var o = t.uid;
x.aldSendEvent("邀请分享被点击", {
uid: o
});
var a = this.storageUtil.getData("userData");
this.httpRequest.uploadShareInfo(o, a, 0, function() {});
} else if (null != t && t.type == this.wxCommon.SHARE_ANSWER) {
console.log("onShow queryData.isAnswer = " + t.isAnswer);
x.aldSendEvent("求助答案分享被点击", {
"关卡": t.level
});
this.isAnswer = !0;
this.unScheduleCatAudio();
this.gameControlView.getComponent("GameControl").setAnswerShare(!0, r);
this.goSeletedLevel(t.level, this.isAnswer);
} else if (null != t && t.type == this.wxCommon.REQUEST_HELPER) {
x.aldSendEvent("求助分享被点击");
console.log("onShow queryData.level = " + t.level);
this.unScheduleCatAudio();
this.gameControlView.getComponent("GameControl").setAnswerShare(!1, r);
this.requsterLevel = t.level;
this.helperGuideDialog.getComponent("HelperGuidControl").showGuideDialog(r.nickName, r.avatarUrl);
}
}
} else this.shareResult();
}.bind(this));
}
};
t.prototype.goSeletedLevel = function(e, t) {
this.unScheduleCatAudio();
this.themeView.active = !1;
this.gameView.active = !0;
this.innerView.active = !1;
this.gameControlView.getComponent("GameControl").playSelectedLevel(e, !t);
};
t.prototype.isFirstLaunch = function() {
var e = this.storageUtil.getData("first_launch");
return !(!e && null != e);
};
t.prototype.isSecondLaunch = function() {
var e = this.storageUtil.getData("second_launch");
if (e || null == e) {
e = !1;
this.storageUtil.saveData("second_launch", e);
return !0;
}
return !1;
};
t.prototype.getVolumeControl = function() {
return this.volumeControl.getComponent("AudioController");
};
t.prototype.onInviteClick = function() {
x.aldSendEvent("主界面邀请点击");
x.aldSendEvent("邀请界面曝光");
this.unScheduleCatAudio();
this.inviteView.active = !0;
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.inviteControlNode.getComponent("InviteControl").updateList();
};
t.prototype.closeInviteView = function() {
x.aldSendEvent("主界面曝光");
this.scheduleCatAudio();
this.inviteView.active = !1;
};
t.prototype.isNull = function(e) {
return "undefined" == typeof e || "" == e || null == e;
};
t.prototype.shareResult = function() {
console.log("shareResult");
var e = new Date().getTime(), t = (cc.sys.localStorage.getItem("requestTime"), cc.sys.localStorage.getItem("shareTime")), i = this.storageUtil.getData("shareType");
cc.sys.localStorage.removeItem("shareTime");
cc.sys.localStorage.removeItem("shareType");
if (null != t && !this.isNull(t)) {
console.log("shareTime = " + t);
var n = e - Number(t);
console.log("tempTime = " + n);
console.log("tempTime " + n);
console.log("shareType " + i);
if (n > 3e3 && n < 6e4) {
if (null != i && !this.isNull(i)) if (i == this.wxCommon.PICK_ICON_AWARD) this.awardView.getComponent("AwardViewControl").gainAwardCoins(); else if (i == this.wxCommon.PICK_PRESSURE) this.gameControlView.getComponent("GameControl").gainPressure(); else if (i == this.wxCommon.PICK_DOUBLE) this.gameControlView.getComponent("GameControl").gainDoubleCoins(); else if (i == this.wxCommon.PICK_REVIVE) this.gameControlView.getComponent("GameControl").gainRevive(); else if (i == this.wxCommon.PICK_UNLOCK) this.themeView.getComponent("ThemeViewControl").onShared2Friends(); else if (i == this.wxCommon.SHARE_ANSWER) this.gameControlView.getComponent("GameControl").onShared2Friends(); else if (i == this.wxCommon.PICK_ANSWER) this.gameControlView.getComponent("GameControl").gainAnswerSolvedCoins(); else if (i == this.wxCommon.SHARE_NOTIFY) {
this.gameControlView.getComponent("GameControl").isShareNotify = !0;
this.gameControlView.getComponent("GameControl").onNotifyClick(null);
} else if (i == this.wxCommon.PICK_DAYAWARD) this.gainDayAward(); else if (i == this.wxCommon.PICK_ADD_COIN) {
console.log("分享添加猫币");
this.gameControlView.getComponent("GameControl").gainAddCoins();
} else if (i == this.wxCommon.GOD_SHARE) {
console.log("超神分享添加猫币");
this.gameControlView.getComponent("GameControl").gainAddCoin(this.globalConst.addCoinGod);
var r = this.gameControlView.getComponent("GameControl"), o = r.currentLevel % r.awardStep;
r.showSettleDialog(o);
}
} else {
var a = this;
x.showModal({
title: "提示",
content: "分享到不同的群才能获得奖励~",
showCancel: !0,
success: function(e) {
if (e.confirm) {
console.log("用户点击确定");
a.wxCommon.shareToFrends("", "", "", a.wxCommon.getAldShareDesc(i));
console.log("用户点击确定 shareType =", i);
var t = new Date().getTime();
a.storageUtil.saveData("shareTime", t);
a.storageUtil.saveData("shareType", i);
}
}
});
}
}
};
t.prototype.scheduleCatAudio = function() {
console.log("miao  playCatAudio schedule ");
this.schedule(this.playCatAudio, 10);
};
t.prototype.unScheduleCatAudio = function() {
console.log("miao  playCatAudio unSchedule");
this.unschedule(this.playCatAudio);
};
t.prototype.playCatAudio = function() {
console.log("miao  playCatAudio scheduleCatAudio= " + this.catAudioScheduleID);
this.volumeControl.getComponent("AudioController").playCatAudio();
};
t.prototype.showDayAward = function() {
var e = this.storageUtil.getData("dayAward"), t = new Date(), i = t.getFullYear() + "-" + t.getMonth() + "-" + t.getDate();
if (i != e && !this.isFirstLaunch() && !this.isRequestHelp && !this.isAnswer && this.launchViewIsTop()) {
x.aldSendEvent("登陆奖励曝光");
this.dayAwardDialog.active = !0;
if (this.bannerAdshow) {
this.resetBannerPos(this.gainDayAwardBtn);
this.showBanner(this.DailyBanner, function(e) {
this.moveBanner(e, this.gainDayAwardBtn);
}.bind(this));
this.dayAwardCheckBox.getChildByName("New Toggle").getComponent(cc.Toggle).check();
this.dayAwardCheckBox.opacity = 0;
var n = cc.fadeIn(1), r = cc.delayTime(2);
this.dayAwardCheckBox.runAction(cc.sequence(r, n));
this.justHideAd();
}
this.storageUtil.saveData("dayAward", i);
}
};
t.prototype.onDayAwardClick = function() {
if (this.doubleCheckBoxChecked) this.loadVideoAd(this.DayDoubleGold); else {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.gainDayAward();
}
this.hideBanner();
};
t.prototype.gainDayAward = function() {
this.doubleCheckBoxChecked ? this.coinsUtil.addCoins(2 * this.globalConst.dayAwardCoin) : this.coinsUtil.addCoins(this.globalConst.dayAwardCoin);
this.getVolumeControl().playGetCoinsAudio();
this.doubleCheckBoxChecked = !0;
this.dayAwardDialog.active = !1;
};
t.prototype.setDoubleAwardState = function(e) {
console.log("toggle is checked  = " + e.isChecked);
this.doubleCheckBoxChecked = e.isChecked;
};
t.prototype.onFromHelperGuidePlayingClick = function() {
this.helperGuideDialog.active = !1;
this.goSeletedLevel(this.requsterLevel, !1);
};
t.prototype.onChallengeMoreClick = function() {
x.aldSendEvent("求助挑战更多关卡点击");
this.answerSolveView.active = !1;
this.back2LaunchView();
};
t.prototype.loadInnerLevelData = function() {
var e = this, t = this.userData.gameLevel;
this.innerBoxArray.splice(0, this.innerBoxArray.length);
var i = this;
return new Promise(function(n) {
for (var r = 1; r <= t; r++) i.innerBoxArray.push(e.storageUtil.getData("levelData" + r));
console.log("Inner level data load finish");
n();
});
};
t.prototype.getLevelPathData = function(e) {
var t = this.innerBoxArray[e - 1];
null != t && "undefined" != typeof t || (t = this.storageUtil.getData("levelData" + e));
return t;
};
t.prototype.saveLevelPathData = function(e, t) {
this.storageUtil.saveData("levelData" + e, t);
e < this.innerBoxArray.length && (this.innerBoxArray[e - 1] = t);
};
t.prototype.showGameRecommend = function() {
if (null == this.gameRecommendNode) {
this.gameRecommendNode = cc.instantiate(this.gameRecommendPref);
this.node.addChild(this.gameRecommendNode);
}
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.gameRecommendNode.getComponent("Slider").show();
};
t.prototype.onDestroy = function() {
cc.sys.platform == cc.sys.WECHAT_GAME && null != this.rewardedVideoAd && this.compareVersion(x.getSystemInfoSync().SDKVersion, "2.7.7") >= 0 && this.rewardedVideoAd.offClose(this.closeFun);
};
t.prototype.initWXAd = function() {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
var e = this;
this.rewardedVideoAd = x.createRewardedVideoAd({
adUnitId: "adunit-18b4228aa861187b"
});
this.rewardedVideoAd.onLoad(function() {
console.log("激励视频 广告加载成功");
});
this.rewardedVideoAd.onError(function(e) {
console.log("激励视频 广告加载失败");
console.log(e);
});
this.isNetConnect = !0;
x.onNetworkStatusChange(function(t) {
e.isNetConnect = t.isConnected;
console.log(t.isConnected);
console.log(t.networkType);
});
x.getNetworkType({
success: function(t) {
console.log(t.networkType);
"none" == t.networkType ? e.isNetConnect = !1 : e.isNetConnect = !0;
}
});
}
};
t.prototype.closeFun = function(e) {
cc.log(e, "res");
if (e && e.isEnded || void 0 === e) {
if (this.adPressureDoubleGold) {
this.adPressureDoubleGold = !1;
this.gameControlView.getComponent("GameControl").gainPressure();
} else if (this.adGold50) {
this.gameControlView.getComponent("GameControl").gainDoubleCoins();
this.adGold50 = !1;
} else if (this.adGold100) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("100猫币视频完整看完");
this.gameControlView.getComponent("GameControl").gainDoubleCoins();
this.adGold100 = !1;
} else if (this.adTurnTable) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("转盘抽奖视频完整看完");
this.gameControlView.getComponent("GameControl").startRolling();
this.adTurnTable = !1;
} else if (this.adAwardDoubleGold) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("抢猫币双倍领取视频完整看完");
this.awardView.getComponent("AwardViewControl").gainAwardCoins();
this.adAwardDoubleGold = !1;
} else if (this.adDayDoubleGold) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("每日登录双倍领取视频完整看完");
this.gainDayAward();
this.adDayDoubleGold = !1;
} else if (this.adRevive) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("挑战赛复活视频完整看完");
this.gameControlView.getComponent("GameControl").gainRevive();
this.adRevive = !1;
} else if (this.adUnlock) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("解锁主题视频完整看完");
this.themeView.getComponent("ThemeViewControl").onShared2Friends();
this.adUnlock = !1;
} else if (this.adNotify) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("提示视频完整看完");
console.log("视频看完提示 返回");
this.gameControlView.getComponent("GameControl").isShareNotify = !0;
this.gameControlView.getComponent("GameControl").onNotifyClick(null);
this.adNotify = !1;
} else if (this.adDialogNotify) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("对话框提示视频完整看完");
console.log("视频看完提示 返回");
this.gameControlView.getComponent("GameControl").onDialogNotifyClick();
this.adDialogNotify = !1;
} else if (this.adNextLevel) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("下一关视频完整看完");
console.log("视频看完提示 返回");
this.themeView.getComponent("ThemeViewControl").successLockLevel();
this.gameControlView.getComponent("GameControl").passNextLevel();
this.adNextLevel = !1;
} else if (this.adRest) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("重玩视频完整看完");
console.log("视频看完提示 返回");
this.gameControlView.getComponent("GameControl").resetClick();
this.adRest = !1;
} else if (this.adUnlockTheme) {
console.log("视频看完提示 返回");
this.themeView.getComponent("ThemeViewControl").successLockTheme();
this.adUnlockTheme = !1;
} else if (this.adUnlockLevel) {
console.log("视频看完提示 返回");
this.themeView.getComponent("ThemeViewControl").successLockLevel();
this.adUnlockLevel = !1;
} else if (this.adDownload) {
console.log("视频看完提示 返回");
this.gameControlView.getComponent("GameControl").successAd();
this.adDownload = !1;
} else if (this.adDownloadAlbum) {
console.log("视频看完提示 返回");
this.successAdSave();
this.adDownloadAlbum = !1;
}
} else {
console.log("wx ad onclose game 333");
if (this.adPressureDoubleGold) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("宝箱结算双倍奖励视频未看完，中途关闭");
this.adPressureDoubleGold = !1;
} else if (this.adGold50) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("50猫币视频未看完，中途关闭");
this.gameControlView.getComponent("GameControl").showAd();
this.showBanner(this.SettleViewBanner, null);
this.adGold50 = !1;
} else if (this.adGold100) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("100猫币视频未看完，中途关闭");
this.gameControlView.getComponent("GameControl").showAd();
this.showBanner(this.SettleViewBanner, null);
this.adGold100 = !1;
} else if (this.adTurnTable) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("转盘抽奖视频未看完，中途关闭");
this.showBanner(this.TurnTable, null);
this.gameControlView.getComponent("GameControl").turnTableNode.getComponent("Turntable").showAd();
this.adTurnTable = !1;
} else if (this.adAwardDoubleGold) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("抢猫币双倍领取视频未看完，中途关闭");
this.showBanner(this.AwardBanner, null);
this.adAwardDoubleGold = !1;
} else if (this.adDayDoubleGold) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("每日登录双倍领取视频未看完，中途关闭");
this.adDayDoubleGold = !1;
this.showBanner(this.DailyBanner, function(e) {
this.moveBanner(e, this.gainDayAwardBtn);
}.bind(this));
} else if (this.adRevive) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("挑战赛复活视频未看完，中途关闭");
this.showBanner(this.LimitBanner, null);
this.gameControlView.getComponent("GameControl").limitFailDialog.getComponent("LimitFailControl").showAd();
this.adRevive = !1;
} else if (this.adUnlock) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("解锁主题视频未看完，中途关闭");
this.adUnlock = !1;
} else if (this.adNotify) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("提示视频未看完，中途关闭");
this.gameControlView.getComponent("GameControl").startCountDown = !0;
this.adNotify = !1;
} else if (this.adDialogNotify) {
cc.sys.platform == cc.sys.WECHAT_GAME && x.aldSendEvent("对话框提示视频未看完，中途关闭");
this.gameControlView.getComponent("GameControl").startCountDown = !0;
this.adDialogNotify = !1;
} else this.adNextLevel ? this.adNextLevel = !1 : this.adRest ? this.adRest = !1 : this.adUnlockTheme ? this.adUnlockTheme = !1 : this.adUnlockLevel ? this.adUnlockLevel = !1 : this.adDownload ? this.adDownload = !1 : this.adDownloadAlbum && (this.adDownloadAlbum = !1);
}
};
t.prototype.showWxToast = function() {
cc.sys.platform == cc.sys.WECHAT_GAME && x.showToast({
title: "视频广告暂时不可用",
icon: "none",
duration: 2e3
});
};
t.prototype.loadVideoAd = function(e) {
var t = this;
if (this.Membership) {
this.parseAdType(e);
this.closeFun(void 0);
} else if (cc.sys.platform == cc.sys.WECHAT_GAME) ; else if (M.JsBridge.isMobile()) {
var i = "50bdf98e86ebe38a";
M.JsBridge.isIosPlatform() ? i = "b6507d22a2673d" : M.JsBridge.isAndroidPlatform() && (8 == e ? i = "50bdf98e86ebe38a" : 10 == e ? i = "fe7b9fa64a67bf8a" : 12 == e ? i = "b8e94116936fbbd1" : 13 == e && (i = "2ad03c4968bf06e8"));
console.log("ad type...", e);
this.loadTimer = function() {
t.node.getChildByName("loading").active = !0;
};
this.scheduleOnce(this.loadTimer, 1);
M.JsBridge.callWithCallback(M.JsBridge.AD_REWARD, {
data: i
}, function(n) {
if (null != n) {
var r = n.resultType;
console.log(r, "======native ad callback=======");
if (null != r) {
if (0 != r) {
t.unschedule(t.loadTimer);
t.node.getChildByName("loading").active = !1;
}
switch (r) {
case 0:
(o = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
o.adType = "reword_video";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = i;
B.Utils.event("ad_request", o);
break;

case 1:
(o = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
o.ecpm = n.ecpm;
o.adType = "reword_video";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = i;
o.adNetworkPlatformName = n.adPlatform;
B.Utils.event("ad_full", o);
(o = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
o.ecpm = n.ecpm;
o.adType = "reword_video";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = i;
o.adNetworkPlatformName = n.adPlatform;
B.Utils.event("ad_show", o);
break;

case 3:
break;

case 7:
(o = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
o.ecpm = n.ecpm;
o.adType = "reword_video";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = i;
o.adNetworkPlatformName = n.adPlatform;
B.Utils.event("ad_close", o);
break;

case 8:
(o = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
o.ecpm = n.ecpm;
o.adType = "reword_video";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = i;
o.adNetworkPlatformName = n.adPlatform;
B.Utils.event("ad_click", o);
break;

case 6:
t.adTimes++;
t.storageUtil.saveData("adTimes", t.adTimes);
1 != t.adTimes && 3 != t.adTimes && 5 != t.adTimes || t.scheduleOnce(function() {
t.btnOpenShop();
}, .5);
t.parseAdType(e);
t.closeFun(void 0);
break;

case -1:
case -2:
var o;
(o = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
o.adType = "reword_video";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = i;
B.Utils.event("ad_request_failed", o);
if (e == t.NextLevel) {
t.parseAdType(e);
t.closeFun(void 0);
} else {
t.parseAdType(e);
t.closeFun(null);
if (-2 != r) {
var s = C.i18nMgr._getLabel("txt_120");
M.JsBridge.callWithCallback(M.JsBridge.TOAST, {
tips: s
}, function() {});
}
}
}
}
}
});
} else {
this.parseAdType(e);
this.closeFun();
}
};
t.prototype.parseAdType = function(e) {
e == this.PressureDoubleGold ? this.adPressureDoubleGold = !0 : e == this.Gold50 ? this.adGold50 = !0 : e == this.Gold100 ? this.adGold100 = !0 : e == this.TurnTable ? this.adTurnTable = !0 : e == this.AwardDoubleGold ? this.adAwardDoubleGold = !0 : e == this.DayDoubleGold ? this.adDayDoubleGold = !0 : e == this.Revive ? this.adRevive = !0 : e == this.UnLockChapter ? this.adUnlock = !0 : e == this.Notify ? this.adNotify = !0 : e == this.NotifyDialog ? this.adDialogNotify = !0 : e == this.NextLevel ? this.adNextLevel = !0 : e == this.Reset ? this.adRest = !0 : e == this.unlockTheme ? this.adUnlockTheme = !0 : e == this.unlockLevel ? this.adUnlockLevel = !0 : e == this.download ? this.adDownload = !0 : e == this.downloadAlbum && (this.adDownloadAlbum = !0);
};
t.prototype.compareVersion = function(e, t) {
e = e.split(".");
t = t.split(".");
for (var i = Math.max(e.length, t.length); e.length < i; ) e.push("0");
for (;t.length < i; ) t.push("0");
for (var n = 0; n < i; n++) {
var r = parseInt(e[n]), o = parseInt(t[n]);
if (r > o) return 1;
if (r < o) return -1;
}
return 0;
};
t.prototype.showBanner = function(e, t) {
var i = this;
if (7 == e) {
console.log("show banner");
cc.sys.platform == cc.sys.WECHAT_GAME && (e == this.SettleViewBanner || e == this.AwardBanner || e == this.DailyBanner || e == this.PressureBanner || e == this.LimitBanner || e == this.InnerSelectBanner || this.NotifyGuide);
if (cc.sys.platform == cc.sys.WECHAT_GAME) try {
var n = this;
null != this.bannerAd && this.bannerAd.destroy();
var r = x.getSystemInfoSync(), o = r.windowWidth, s = r.screenHeight, c = r.SDKVersion;
if (this.compareVersion(c, "2.0.4") >= 0) {
this.bannerAd = x.createBannerAd({
adUnitId: "adunit-ed8b2ba746f65ab7",
style: {
left: (o - 300) / 2,
top: s - 80 - 10,
width: 300
}
});
this.bannerAd.onError(function(e) {
console.log(e);
});
this.bannerAd.show().then(function() {
e == i.SettleViewBanner || e == i.AwardBanner || e == i.DailyBanner || e == i.PressureBanner || e == i.LimitBanner || e == i.InnerSelectBanner || i.NotifyGuide;
}).catch(function(t) {
e == i.SettleViewBanner || e == i.AwardBanner || e == i.DailyBanner || e == i.PressureBanner || e == i.LimitBanner || e == i.InnerSelectBanner || i.NotifyGuide;
var n = "banner无配额原因code:" + t.errMsg;
console.log(n);
x.aldSendEvent(n);
});
this.bannerAd.onResize(function() {
n.bannerAd.style.top = s - n.bannerAd.style.realHeight;
null != t && t(n.bannerAd.style.realHeight);
});
}
} catch (e) {} else if (M.JsBridge.isMobile()) {
var l = "fd7c7cd26f485e88";
M.JsBridge.isIosPlatform() && (l = "b651145b5e67ca");
M.JsBridge.callWithCallback(M.JsBridge.AD_BANNER, {
data: l
}, function(e) {
if (null != e) {
var t = e.resultType;
console.log(t, "======native ad callback=======");
if (null != t) switch (t) {
case 0:
(i = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
i.adType = "banner";
i.realEcpm = 0;
i.biddingType = 0;
i.networkPlacementId = l;
B.Utils.event("ad_request", i);
break;

case 1:
(i = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
i.ecpm = e.ecpm;
i.adType = "banner";
i.realEcpm = 0;
i.biddingType = 0;
i.networkPlacementId = l;
i.adNetworkPlatformName = e.adPlatform;
B.Utils.event("ad_full", i);
(i = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
i.ecpm = e.ecpm;
i.adType = "banner";
i.realEcpm = 0;
i.biddingType = 0;
i.networkPlacementId = l;
i.adNetworkPlatformName = e.adPlatform;
B.Utils.event("ad_show", i);
break;

case 3:
break;

case 7:
var i;
(i = a.BaseConfig.DeviceInfo).ad_time = B.Utils.nowTime().toString();
i.ecpm = e.ecpm;
i.adType = "banner";
i.realEcpm = 0;
i.biddingType = 0;
i.networkPlacementId = l;
i.adNetworkPlatformName = e.adPlatform;
B.Utils.event("ad_close", i);
}
}
});
}
}
};
t.prototype.hideBanner = function() {};
t.prototype.hideBanner1 = function() {
if (cc.sys.platform == cc.sys.WECHAT_GAME) try {
null != this.bannerAd && this.bannerAd.hide();
} catch (e) {} else M.JsBridge.isMobile() && M.JsBridge.callWithCallback(M.JsBridge.AD_BANNER_HIDE, {}, function() {});
};
t.prototype.showInterstitial = function(e) {
var t = this;
console.log("show interstitial");
cc.sys.platform == cc.sys.WECHAT_GAME && e == this.SettleViewInter && x.aldSendEvent("过关结算界面插屏广告请求");
if (cc.sys.platform == cc.sys.WECHAT_GAME) try {
var i = x.getSystemInfoSync().SDKVersion;
if (this.compareVersion(i, "2.0.4") >= 0) {
this.interstitialAd = x.createInterstitialAd({
adUnitId: "adunit-1234f31c97215143"
});
this.interstitialAd.onError(function(e) {
console.log(e);
});
this.interstitialAd.show().then(function() {
t.hideBanner();
e == t.SettleViewInter && x.aldSendEvent("过关结算界面插屏展示成功");
}).catch(function(i) {
e == t.SettleViewInter && x.aldSendEvent("过关结算界面插屏无配额");
var n = "插屏无配额原因code:" + i.errMsg;
console.log(n);
x.aldSendEvent(n);
});
this.interstitialAd.onClose(function() {
console.log("插屏 广告关闭");
});
}
} catch (e) {}
};
t.prototype.showGameIcon = function() {
var e = this;
this.iconAd = null;
var t = x.getSystemInfoSync(), i = t.screenHeight, n = t.screenWidth, r = .13 * n, o = n / 2, a = i - 1.5 * r, s = r + 20;
x.createGameIcon && (this.iconAd = x.createGameIcon({
adUnitId: "PBgAAswp0YQ4591A",
count: 5,
style: [ {
appNameHidden: !1,
color: "#FFFFFF",
size: r,
left: o - 2.5 * s,
borderWidth: 2,
borderColor: "#FFFFFF",
top: a
}, {
appNameHidden: !1,
color: "#FFFFFF",
size: r,
left: o - 1.5 * s,
borderWidth: 2,
borderColor: "#FFFFFF",
top: a
}, {
appNameHidden: !1,
color: "#FFFFFF",
size: r,
left: o - .5 * s,
borderWidth: 2,
borderColor: "#FFFFFF",
top: a
}, {
appNameHidden: !1,
color: "#FFFFFF",
size: r,
left: o + .5 * s,
borderWidth: 2,
borderColor: "#FFFFFF",
top: a
}, {
appNameHidden: !1,
color: "#FFFFFF",
size: r,
left: o + 1.5 * s,
borderWidth: 2,
borderColor: "#FFFFFF",
top: a
} ]
}));
this.iconAd && this.iconAd.load().then(function() {
e.gameAdshow && e.iconAd.show();
}).catch(function(e) {
console.error(e);
});
};
t.prototype.justHideAd = function() {
this.gameAdshow = !1;
null != this.iconAd && this.iconAd.hide();
};
t.prototype.showAd = function() {
this.gameAdshow = !0;
null != this.iconAd && this.launchViewIsTop() && this.iconAd.show();
};
t.prototype.hideGameAd = function() {
this.gameAdshow = !1;
if (null != this.iconAd) {
this.iconAd.hide();
this.iconAd = null;
}
};
t.prototype.resetBannerPos = function() {};
t.prototype.moveBanner = function() {};
t.prototype.launchViewIsTop = function() {
return 1 == this.launchView.active && 0 == this.dayAwardDialog.active;
};
t.prototype.showGameIcon1 = function() {
window.popUpMgr.getComponent("Pop").addPopByName("moreGame/GameBox", null, !0, !0, !0, "left");
};
t.prototype.initGlobalData = function() {
window.popUpMgr = this.node;
window.facadeMgr = this.node;
window.facadeMgr.wxAppId = "wxfd9901848f148530";
};
t.prototype.isgameRecorderDurationValid = function() {
this.userData = this.storageUtil.getData("userData");
this.recordData = this.storageUtil.getData("recordData");
null == this.recordData && (this.recordData = new v.default());
if (this.recordData.date === new Date().getMonth() + "_" + new Date().getDate() && this.recordData.num >= 3) {
console.log("isgameRecorderDurationValid recordData", this.recordData.num);
return !1;
}
this.passData = this.storageUtil.getData("passData");
null == this.passData && (this.passData = new _.default());
if (this.passData.date === new Date().getMonth() + "_" + new Date().getDate() && this.passData.level % 3 == 0) {
console.log("isgameRecorderDurationValid passData", this.passData.level);
return !0;
}
return !1;
};
t.prototype.showRecordShareDialog = function() {};
t.prototype.btnOpenSettingLang = function() {
var e = this;
this.node.getChildByName("settingView").active = !0;
this.list.node.active = !0;
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.volumeControl.getComponent("AudioController").showUI();
var t = C.i18nMgr.getLanguage();
if (t) for (var i = 0; i < Object.keys(this.langList).length; i++) if (t == Object.keys(this.langList)[i]) {
this.langIdx = i;
break;
}
this.node.getChildByName("settingView").getChildByName("txtLabel").getComponent(cc.Label).string = Object.values(this.langList)[this.langIdx];
this.scheduleOnce(function() {
0 == e.list.numItems && (e.list.numItems = Object.values(e.langList).length);
e.list.scrollTo(e.langIdx - 3, 0);
}, .1);
};
t.prototype.btnCloseSettingLang = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.list.scrollTo(0, 0);
this.node.getChildByName("settingView").active = !1;
};
t.prototype.btnSettingLang = function(e) {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.storageUtil.saveData("userLang", Object.keys(this.langList)[e.target._index]);
this.btnCloseSettingLang();
cc.game.restart();
};
t.prototype.onListRender = function(e, t) {
e._index = t;
e.getChildByName("txt").getComponent(cc.Label).string = Object.values(this.langList)[t];
e.getChildByName("txt").color = new cc.Color().fromHEX(this.langIdx == t ? "#000000" : "#A797B4");
};
t.prototype.btnOpenAlbum = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.node.getChildByName("Album").active = !0;
this.list3.node.active = !0;
this.list3.numItems = this.globalConst.themeCount;
};
t.prototype.onListRender3 = function(e, t) {
e.getChildByName("nameLabel").color = new cc.Color().fromHEX(this.colorList[t]);
e.getChildByName("nameLabel").getComponent(cc.Label).string = C.i18nMgr._getLabel("txt_" + (1e3 + t));
for (var i = function(i) {
var r = e.getChildByName("node").children[i], o = t * n.globalConst.chapterCount + i + 1, a = (o - 1) * n.globalConst.levelCount + 1, s = n.storageUtil.getData("unLockTheme" + o), c = r.getChildByName("sprite");
n.Membership || s ? r.getChildByName("lock").active = !1 : r.getChildByName("lock").active = !0;
c.active = !0;
c.lv != a && (c.getComponent(cc.Sprite).spriteFrame = n.rankTitle);
c.lv = a;
r.theme = o;
r.unlock = s;
r.lv = a;
var l = new S.default().currentUrl + "/bg/id_" + o + "/level_bg_1.jpg";
n.LoadNetImg(l, function(e) {
if (c.lv == a) {
var t = new cc.SpriteFrame(e);
cc.loader.setAutoReleaseRecursively(t, !0);
c.getComponent(cc.Sprite).spriteFrame = t;
}
});
r.getChildByName("nameLabel").getComponent(cc.Label).string = C.i18nMgr._getLabel("txt_" + (1005 + o));
}, n = this, r = 0; r < this.globalConst.chapterCount; r++) i(r);
};
t.prototype.btnOpenIsInner = function(e) {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
if (e.target.unlock || this.Membership) {
this.list1.node.active = !0;
this.list3.node.active = !1;
this.list1._customSize = {};
this.selectTheme = e.target.theme;
for (var t = 0; t < 6; t++) this.list1._customSize[t] = 0 == t ? 80 : 229;
this.list1.numItems = 6;
} else this.themeView.getComponent("ThemeViewControl").showUnlockView(e.target.theme, e.target.lv);
};
t.prototype.btnCloseAlbum = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
if (this.list1.node.active) {
this.list1.scrollTo(0, 0);
this.list3.node.active = !0;
this.list1.node.active = !1;
} else this.closeAlbum();
};
t.prototype.closeAlbum = function() {
this.list3.scrollTo(0, 0);
this.node.getChildByName("Album").active = !1;
this.list3.node.active = !1;
this.list1.node.active = !1;
};
t.prototype.onListRender1 = function(e, t) {
e._index = t;
if (t % 6 == 0) {
e.height = 50;
e.getChildByName("img1").active = !1;
e.getChildByName("img2").active = !1;
e.getChildByName("img3").active = !1;
e.getChildByName("img4").active = !1;
e.getChildByName("label").active = !0;
e.getChildByName("label").getComponent(cc.Label).string = C.i18nMgr._getLabel("txt_" + (1005 + this.selectTheme));
} else {
e.height = 229;
e.getChildByName("img1").active = !0;
e.getChildByName("img2").active = !0;
e.getChildByName("img3").active = !0;
e.getChildByName("img4").active = !0;
e.getChildByName("label").active = !1;
for (var i = 4 * (t % 6 - 1), n = function(t) {
var n = e.getChildByName("img" + [ t ]);
n.theme = Math.ceil(r.selectTheme / r.globalConst.chapterCount);
var o = (r.selectTheme - 1) * r.globalConst.levelCount + i + t, a = o - (n.theme - 1) * r.globalConst.chapterCount * r.globalConst.levelCount;
n.chapter = Math.ceil(a / r.globalConst.levelCount);
var s = r.storageUtil.getData("unLockLevel" + o);
n.levelData = s;
n.lv = o;
n.getChildByName("sprite").active = !1;
if (s && 2 == s) {
var c = new S.default().currentUrl + "/bg/id_" + r.selectTheme + "/level_bg_" + (i + t) + ".jpg";
r.LoadNetImg(c, function(e) {
if (n.lv == o) {
var t = new cc.SpriteFrame(e);
cc.loader.setAutoReleaseRecursively(t, !0);
n.getChildByName("sprite").active = !0;
n.getChildByName("sprite").getComponent(cc.Sprite).spriteFrame = t;
}
});
} else n.getChildByName("sprite").active = !1;
}, r = this, o = 1; o < 5; o++) n(o);
}
};
t.prototype.btnOpenInner = function(e) {
var t = this;
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
var i = Math.ceil(e.target.lv / this.globalConst.levelCount), n = this.storageUtil.getData("unLockTheme" + i);
cc.log(i, "theme", n);
if (2 == e.target.levelData) {
this.passPic = [];
var r = this.node.getChildByName("pic");
r.active = !0;
r.opacity = 0;
for (var o = 1; o <= this.globalConst.levelCount; o++) {
var a = (i - 1) * this.globalConst.levelCount + o;
2 == this.storageUtil.getData("unLockLevel" + a) && this.passPic.push(a);
}
var s = this.passPic.indexOf(e.target.lv);
this.picLv = -1 != s ? s : 0;
r.getChildByName("titLabel").getComponent(cc.Label).string = this.passPic.length + "/" + this.globalConst.levelCount;
this.list2.numItems = this.passPic.length;
this.hideVideo();
this.scheduleOnce(function() {
t.list2.skipPage(t.picLv, 0);
r.opacity = 255;
}, .1);
} else {
this.list1.scrollView.stopAutoScroll();
this.node.getChildByName("Album").active = !1;
this.albumBoo = !0;
this.innerView.active = !0;
this.themeView.active = !1;
this.innerView.getChildByName("GridLayout").getComponent("InnerLevelControl").initLevelData(e.target.theme, e.target.chapter);
}
};
t.prototype.updateAlbum = function() {
var e = 0;
(e = this.storageUtil.getData("passCount")) || (e = 0);
var t = e / (this.globalConst.levelCount * this.globalConst.themeCount * this.globalConst.chapterCount) * 100;
this.albumLabel.string = C.i18nMgr._getLabel("txt_118").replace("@", Math.ceil(t) + "%");
};
t.prototype.hideVideo = function() {
var e = this.node.getChildByName("pic");
if (this.Membership) {
e.getChildByName("btn1").active = !1;
e.getChildByName("btn").getChildByName("layout").getChildByName("icon").active = !1;
}
this.showPic();
};
t.prototype.btnOpenShop = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.node.getChildByName("Membership").active = !0;
};
t.prototype.btnLeft = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.picLv--;
this.showPic();
this.list2.skipPage(this.picLv, .3);
};
t.prototype.btnRight = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.picLv++;
this.showPic();
this.list2.skipPage(this.picLv, .3);
};
t.prototype.pageEvent = function(e) {
cc.log(e, "idx");
this.picLv = e;
this.showPic();
};
t.prototype.onListRender2 = function(e, t) {
var i = this.passPic[t], n = Math.ceil(i / this.globalConst.levelCount), r = i % this.globalConst.levelCount;
e.picLv != i && (e.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = this.rankTitle);
e.picLv = i;
var o = new S.default().currentUrl + "/bg/id_" + n + "/level_bg_" + (r || 20) + ".jpg";
this.LoadNetImg(o, function(t) {
if (e.picLv == i) {
var n = new cc.SpriteFrame(t);
cc.loader.setAutoReleaseRecursively(n, !0);
e.getChildByName("Sprite").getComponent(cc.Sprite).spriteFrame = n;
}
});
};
t.prototype.showPic = function() {
var e = this.node.getChildByName("pic");
if (1 == this.list2.numItems) {
e.getChildByName("btnright").active = !1;
e.getChildByName("btnleft").active = !1;
} else if (this.picLv == this.list2.numItems - 1) {
e.getChildByName("btnright").active = !1;
e.getChildByName("btnleft").active = !0;
} else if (0 == this.picLv) {
e.getChildByName("btnright").active = !0;
e.getChildByName("btnleft").active = !1;
} else {
e.getChildByName("btnright").active = !0;
e.getChildByName("btnleft").active = !0;
}
};
t.prototype.saveBtn = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.Membership ? this.successAdSave() : this.loadVideoAd(this.downloadAlbum);
};
t.prototype.successAdSave = function() {
var e = Math.ceil(this.passPic[this.picLv] / this.globalConst.levelCount), t = this.passPic[this.picLv] % this.globalConst.levelCount, i = new S.default().currentUrl + "/bg/id_" + e + "/level_bg_" + t + ".jpg";
cc.log(i);
M.JsBridge.isMobile() && M.JsBridge.callWithCallback(M.JsBridge.SAVE, {
type: "img",
url: i
}, function() {});
};
t.prototype.btnCloseBigPic = function() {
this.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.node.getChildByName("pic").active = !1;
};
o([ l(cc.Node) ], t.prototype, "launchView", void 0);
o([ l(cc.Node) ], t.prototype, "themeView", void 0);
o([ l(cc.Node) ], t.prototype, "gameView", void 0);
o([ l(cc.Node) ], t.prototype, "innerView", void 0);
o([ l(cc.Node) ], t.prototype, "gameControlView", void 0);
o([ l(cc.Node) ], t.prototype, "rankView", void 0);
o([ l(cc.Label) ], t.prototype, "albumLabel", void 0);
o([ l(cc.SpriteFrame) ], t.prototype, "rankTitle", void 0);
o([ l(cc.Sprite) ], t.prototype, "rankTopSprite", void 0);
o([ l(cc.Node) ], t.prototype, "limitPlayBtn", void 0);
o([ l(cc.Node) ], t.prototype, "volumeControl", void 0);
o([ l(cc.Node) ], t.prototype, "inviteView", void 0);
o([ l(cc.Node) ], t.prototype, "inviteControlNode", void 0);
o([ l(cc.Prefab) ], t.prototype, "getWxInfoDialog", void 0);
o([ l(cc.Node) ], t.prototype, "rankScrollView", void 0);
o([ l(cc.Node) ], t.prototype, "awardView", void 0);
o([ l(cc.Prefab) ], t.prototype, "snowPref", void 0);
o([ l(cc.Node) ], t.prototype, "launchBg", void 0);
o([ l(cc.Node) ], t.prototype, "dayAwardDialog", void 0);
o([ l(cc.Node) ], t.prototype, "helperGuideDialog", void 0);
o([ l(cc.Node) ], t.prototype, "answerSolveView", void 0);
o([ l(cc.Prefab) ], t.prototype, "gameRecommendPref", void 0);
o([ l(cc.Node) ], t.prototype, "gainDayAwardBtn", void 0);
o([ l(cc.Node) ], t.prototype, "dayAwardCheckBox", void 0);
o([ l(cc.Node) ], t.prototype, "recordShareDialog", void 0);
o([ l(w.default) ], t.prototype, "list", void 0);
o([ l(w.default) ], t.prototype, "list1", void 0);
o([ l(w.default) ], t.prototype, "list2", void 0);
o([ l(w.default) ], t.prototype, "list3", void 0);
return o([ c ], t);
}(A.BaseLayer);
i.default = k;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./Bean/PassLevelBean": "PassLevelBean",
"./Bean/RecordBean": "RecordBean",
"./Bean/UserData": "UserData",
"./Bean/WXUserBean": "WXUserBean",
"./CoinsUtil": "CoinsUtil",
"./Common/HttpRequest": "HttpRequest",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./GlobalConst": "GlobalConst",
"./LevelData": "LevelData",
"./SettleUtil": "SettleUtil",
"./SubDomainControl": "SubDomainControl",
"./base/BaseConfig": "BaseConfig",
"./base/BaseLayer": "BaseLayer",
"./base/BaseUtils": "BaseUtils",
"./native/JsBridge": "JsBridge",
"./scrollViewTools/List": "List"
} ],
MethodManager: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "a6363vh2fVIJLr8J9lGjwX5", "MethodManager");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.MethodManager = void 0;
var n = function() {
function e() {
this.methodMap = new Map();
e.instance = this;
}
e.prototype.addMethod = function(e, t) {
this.hasMethod(e) && this.removeMethod(e);
this.methodMap.set(e, t);
return !0;
};
e.prototype.applyMethod = function(e, t) {
if (!this.methodMap.get(e)) return !1;
var i = this.methodMap.get(e);
try {
null == i || i.call(null, t);
return !0;
} catch (e) {
console.log("Function trigger error: " + e);
return !1;
}
};
e.prototype.removeMethod = function(e) {
return this.methodMap.delete(e);
};
e.prototype.hasMethod = function(e) {
return this.methodMap.has(e);
};
e.instance = new e();
return e;
}();
i.MethodManager = n;
cc._RF.pop();
}, {} ],
ModuleEventEnum: [ function(e, t) {
"use strict";
cc._RF.push(t, "10eecVlx19APLmuCQ3j3+as", "ModuleEventEnum");
t.exports = {
INIT_ROLEINFO_COMPLETED: "INIT_ROLEINFO_COMPLETED",
WX_REGISTERED: "WX_REGISTERED",
POP_REMOVED: "POP_REMOVED",
POP_ADDED: "POP_ADDED",
GOT_HTTP_RES: "GOT_HTTP_RES",
WX_HIDE: "WX_HIDE",
WX_SHOW: "WX_SHOW",
GO_GAME: "GO_GAME",
GO_LOADING: "GO_LOADING"
};
cc._RF.pop();
}, {} ],
Money: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "81ec9z12CJD6ZO1d+ctHiSj", "Money");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.moneyLabel = null;
t.mng = null;
return t;
}
t.prototype.start = function() {};
t.prototype.startMove = function() {
var e = this, t = cc.sequence(cc.spawn(cc.moveBy(1, cc.v2(0, 100)), cc.fadeOut(1)), cc.callFunc(function() {
e.scheduleOnce(function() {
e.despawn();
}, .3);
}));
this.node.runAction(t);
};
t.prototype.setFontSize = function(e) {
this.moneyLabel.fontSize = 100 * e;
};
t.prototype.despawn = function() {
this.node.stopAllActions();
this.node.opacity = 255;
null != this.mng ? this.mng.despawn(this.node) : this.node.removeFromParent();
};
t.prototype.reuse = function(e) {
this.init(e);
};
t.prototype.init = function(e) {
this.mng = e;
this.enabled = !0;
};
o([ c(cc.Label) ], t.prototype, "moneyLabel", void 0);
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {} ],
Network: [ function(e, t) {
"use strict";
cc._RF.push(t, "415dan57eJNo749V6x9cWz6", "Network");
var i = {
getRequest: function(e, t, i) {
var n = new XMLHttpRequest();
n.onreadystatechange = function() {
if (4 == n.readyState && n.status >= 200 && n.status < 300) {
var e = JSON.parse(n.responseText);
i.success && i.success(e);
}
}.bind(this);
n.onerror = function(e) {
i.failure() && i.failure(e);
};
n.open("GET", e, !1);
n.send(t);
},
postRequest: function(e, t, i) {
var n = new XMLHttpRequest();
n.onreadystatechange = function() {
if (4 == n.readyState && n.status >= 200 && n.status < 300) {
var e = JSON.parse(n.responseText);
0 != e.code ? i.failure && i.failure(e.code) : i.success && i.success(e.data);
}
}.bind(this);
n.onerror = function(e) {
i.failure() && i.failure(e);
};
n.open("POST", e, !1);
n.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
n.send(t);
},
saveGameData: function(e) {
var t = window.Config.RECORD_SAVE;
e.value = 0 == e.value ? -1 : e.value;
e.value = JSON.stringify(e.value);
var i = "record_type=" + e.key + "&record=" + e.value;
this.postRequest(t, i, {
success: function(t) {
e.suc(t);
},
failure: function(t) {
e.fail && e.fail(t);
}
});
},
getGameData: function(e) {
var t = window.Config.RECORD_GET, i = "record_type=" + e.key;
this.postRequest(t, i, {
success: function(t) {
t.record = JSON.parse(t.record);
t.record = -1 == t.record ? 0 : t.record;
e.suc(t);
},
failure: function(t) {
e.fail && e.fail(t);
}
});
}
};
t.exports = i;
cc._RF.pop();
}, {} ],
NodePool: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "b1b70oaynVMEq3SO2JOCGIS", "NodePool");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.NodePool = void 0;
var r = cc._decorator, o = r.ccclass, a = r.property, s = function() {
function e() {
this.prefab = null;
this.size = 0;
}
e.prototype.init = function(e) {
this.enemyPool = new cc.NodePool(e);
for (var t = 0; t < this.size; t++) {
var i = cc.instantiate(this.prefab);
this.enemyPool.put(i);
}
};
e.prototype.spawn = function() {
return this.enemyPool.size() > 0 ? this.enemyPool.get(this) : cc.instantiate(this.prefab);
};
e.prototype.despawn = function(e) {
this.enemyPool.put(e);
};
n([ a(cc.Prefab) ], e.prototype, "prefab", void 0);
n([ a(cc.Integer) ], e.prototype, "size", void 0);
return n([ o("NodePool") ], e);
}();
i.NodePool = s;
cc._RF.pop();
}, {} ],
NotifyLayout: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "c4c51zOwTRNubS7LTB4g2J1", "NotifyLayout");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = window.wx, h = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.notify = null;
t.closeBtn = null;
t.videoNode = null;
t.gameControl = null;
return t;
}
t.prototype.setGameControl = function(e) {
this.gameControl = e;
};
t.prototype.onLoad = function() {};
t.prototype.show = function() {
this.gameControl.startCountDown = !1;
this.node.active = !0;
this.closeBtn.opacity = 0;
var e = cc.fadeIn(1), t = cc.delayTime(2);
this.closeBtn.runAction(cc.sequence(t, e));
l.aldSendEvent("提示弹框界面展示");
var i = cc.find("Canvas").getComponent("MainControl");
i.Membership && (this.videoNode.active = !1);
i.showBanner(i.NotifyGuide, function() {}.bind(this));
};
t.prototype.onCloseClick = function() {
l.aldSendEvent("提示弹框界面关闭");
this.close();
};
t.prototype.close = function() {
cc.find("Canvas").getComponent("MainControl");
this.node.active = !1;
this.gameControl.startCountDown = !0;
};
t.prototype.onNotifyClick = function() {
l.aldSendEvent("提示弹框界面-看提示点击");
this.gameControl.onNotifyVideoClick();
};
o([ c(cc.Node) ], t.prototype, "notify", void 0);
o([ c(cc.Node) ], t.prototype, "closeBtn", void 0);
o([ c(cc.Node) ], t.prototype, "videoNode", void 0);
return o([ s ], t);
}(cc.Component);
i.default = h;
cc._RF.pop();
}, {} ],
PassLevelBean: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "c1b006VZXFJDJw77EmfQPUE", "PassLevelBean");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.level = 0;
this.date = "";
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
PoolMng: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "2c892nQfYVN7bF1ZnAzwu/I", "PoolMng");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./NodePool"), h = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.boxPool = [];
t.coinPool = [];
t.coinAnimPool = [];
return t;
}
t.prototype.init = function() {
for (var e = 0; e < this.boxPool.length; e++) this.boxPool[e].init("Box");
for (e = 0; e < this.coinPool.length; e++) this.coinPool[e].init("Coin");
for (e = 0; e < this.coinAnimPool.length; e++) this.coinAnimPool[e].init("GoldAnim");
};
t.prototype.spawnBox = function() {
return this.boxPool[0].spawn();
};
t.prototype.despwanBox = function(e, t) {
this.boxPool[e].despawn(t);
};
t.prototype.spawnCoin = function() {
return this.coinPool[0].spawn();
};
t.prototype.despwanCoin = function(e, t) {
this.coinPool[e].despawn(t);
};
t.prototype.spawnCoinAnim = function() {
return this.coinAnimPool[0].spawn();
};
t.prototype.despwanCoinAnim = function(e) {
return this.coinAnimPool[0].despawn(e);
};
t.prototype.start = function() {};
o([ c(l.NodePool) ], t.prototype, "boxPool", void 0);
o([ c(l.NodePool) ], t.prototype, "coinPool", void 0);
o([ c(l.NodePool) ], t.prototype, "coinAnimPool", void 0);
return o([ s ], t);
}(cc.Component);
i.default = h;
cc._RF.pop();
}, {
"./NodePool": "NodePool"
} ],
Pop: [ function(e, t) {
"use strict";
cc._RF.push(t, "de7e4KxjchNs6jAN0Od5C/F", "Pop");
var i = e("ModuleEventEnum");
cc.Class({
extends: cc.Component,
properties: {},
start: function() {},
modeClick: function() {
this.modeClose && this.removeTop();
},
releasePop: function(e) {
if (this.IN_POOL_POPS[e.name]) window.pool.has(e.name) ? window.pool.put(e.name, e) : window.pool.create(e.name, 1, e); else {
e.removeFromParent();
e.destroy();
}
},
removeAll: function(e) {
for (var t in this.loadingUrls) t != e && delete this.loadingUrls[t];
for (var n = 0; n < this.popList.length; n++) if (this.popList[n].name != e) {
null != this.popList[n].getComponent(this.popList[n].name).doClose && this.popList[n].getComponent(this.popList[n].name).doClose();
this.releasePop(this.popList[n]);
this.popList.splice(n, 1);
n--;
}
if (this.popList.length <= 0 && this.mode) {
this.mode.removeFromParent();
this.mode = null;
this.modeShowed = !1;
}
cc.systemEvent.emit(i.POP_REMOVED);
},
getPopComponent: function(e) {
if (null == this.popList || this.popList.length <= 0) return null;
for (var t = null, i = 0; i < this.popList.length; i++) if (this.popList[i].name == e) {
t = this.popList[i];
break;
}
return t ? t.getComponent(e + "JS") : null;
},
getPopByName: function(e) {
if (null == this.popList || this.popList.length <= 0) return null;
for (var t = null, i = 0; i < this.popList.length; i++) if (this.popList[i].name == e) {
t = this.popList[i];
break;
}
return t || null;
},
removeByName: function(e, t) {
if (!(null == this.popList || this.popList.length <= 0)) {
for (var i = null, n = 0; n < this.popList.length; n++) if (this.popList[n].name == e) {
i = this.popList[n];
break;
}
if (i) {
this._clearOne = i;
null != t && (this.doAction = t);
if (this.doAction) {
var r = 0;
null != this._clearOne.getComponent(this._clearOne.name).closeActTime && (r = this._clearOne.getComponent(this._clearOne.name).closeActTime);
null != this._clearOne.getComponent(this._clearOne.name).doClose && this._clearOne.getComponent(this._clearOne.name).doClose();
if ("top" == i.fadeType) {
i.runAction(cc.sequence(cc.delayTime(r), cc.moveBy(.3, cc.v2(0, 2e3)), cc.callFunc(this.clearPop, this)));
return;
}
if ("bottom" == i.fadeType) i.runAction(cc.sequence(cc.delayTime(r), cc.moveBy(.3, cc.v2(0, -2e3)), cc.callFunc(this.clearPop, this))); else if ("left" == i.fadeType) {
var o = i.getChildByName("ht_boxbg").width, a = i.getChildByName("ht_close").width, s = cc.moveTo(.2, cc.v2(-(o / 2 + a), 0)), c = cc.sequence(s, cc.callFunc(this.clearPop, this));
i.runAction(c);
return;
}
i.runAction(cc.sequence(cc.delayTime(r), cc.scaleTo(.1, 1.1), cc.scaleTo(.1, 0), cc.callFunc(this.clearPop, this)));
} else this.clearPop();
}
}
},
clearPop: function() {
for (var e = 0; e < this.popList.length; e++) if (this.popList[e] == this._clearOne) {
this.popList.splice(e, 1);
break;
}
this.modeShowed && this.mode && this.mode.parent && (this.mode.zIndex = window.facadeMgr.PopOrder + this.popList.length - 1);
this.releasePop(this._clearOne);
cc.systemEvent.emit(i.POP_REMOVED, this._clearOne.name);
if (this.popList.length <= 0 && this.mode) {
this.mode.removeFromParent();
this.mode = null;
this.modeShowed = !1;
"HoverUI" == this._clearOne.name && facadeMgr.getComponent("GameModel").isNeedEnergy && this.addPopByName("VideoTipUI", null, !0);
}
},
removeTop: function(e) {
if (!(null == this.popList || this.popList.length <= 0)) {
var t = this.popList[this.popList.length - 1];
this._clearOne = t;
null != e && (this.doAction = e);
if (this.doAction) {
var i = 0;
this._clearOne.getComponent(this._clearOne.name) && null != this._clearOne.getComponent(this._clearOne.name).closeActTime && (i = this._clearOne.getComponent(this._clearOne.name).closeActTime);
this._clearOne.getComponent(this._clearOne.name) && null != this._clearOne.getComponent(this._clearOne.name).doClose && this._clearOne.getComponent(this._clearOne.name).doClose();
if ("top" == t.fadeType) {
t.runAction(cc.sequence(cc.delayTime(i), cc.moveBy(.3, cc.v2(0, 2e3)), cc.callFunc(this.clearPop, this)));
return;
}
if ("bottom" == t.fadeType) t.runAction(cc.sequence(cc.delayTime(i), cc.moveBy(.3, cc.v2(0, -2e3)), cc.callFunc(this.clearPop, this))); else if ("left" == t.fadeType) {
var n = t.getChildByName("ht_boxbg").width, r = t.getChildByName("ht_close").width, o = cc.moveTo(.5, cc.v2(-(n / 2 + r), 0)), a = cc.sequence(o, cc.callFunc(this.clearPop, this));
t.runAction(a);
return;
}
t.runAction(cc.sequence(cc.delayTime(i), cc.scaleTo(.1, 1.1), cc.scaleTo(.1, 0), cc.callFunc(this.clearPop, this)));
} else this.clearPop();
}
},
showMode: function(e) {
if (this.modeShowed && this.mode && this.mode.parent) {
this.mode.opacity = 204;
this.mode.zIndex = window.facadeMgr.PopOrder + this.popList.length + 1;
console.log("addMode:", this.mode.zIndex);
} else {
this.mode = new cc.Node();
this.mode.addComponent(cc.Sprite);
this.mode.getComponent(cc.Sprite).spriteFrame = cc.loader.getRes("modeBg", cc.SpriteFrame);
this.mode.color = "#383838";
this.mode.scale = 100;
this.mode.opacity = 220;
this.modeShowed = !0;
this.mode.runAction(cc.sequence(cc.delayTime(.21), cc.callFunc(function() {
this.mode.addComponent(cc.Button);
this.mode.on("click", this.modeClick, this);
this.modeClose = e;
}.bind(this))));
console.log("addMode:", facadeMgr.PopOrder);
cc.director.getScene().getChildByName("Canvas").addChild(this.mode, window.facadeMgr.PopOrder);
}
},
popFadeIn: function(e) {
var t = e.getComponent(e.name);
t && null != t.viewDidAppear && t.viewDidAppear();
},
fadeInPop: function(e, t, i) {
e.fadeType = t;
null == i && (i = cc.v2(0, 0));
if ("top" != t) if ("bottom" != t) if ("left" != t) {
e.scale = .1;
e.runAction(cc.sequence(cc.scaleTo(.1, 1.1), cc.scaleTo(.1, 1), cc.callFunc(this.popFadeIn, this)));
} else {
var n = e.getChildByName("ht_boxbg").width, r = e.getChildByName("ht_close").width;
e.x = -(n / 2 + r);
var o = cc.moveTo(.5, cc.v2(n / 2 - cc.winSize.width / 2, 0));
o.easing(cc.easeElasticOut(1));
var a = cc.sequence(o, cc.callFunc(this.popFadeIn, this));
e.runAction(a);
} else {
e.y = -2e3;
var s = cc.moveTo(.3, i);
s.easing(cc.easeElasticOut(2));
e.runAction(s);
} else {
e.y = 2e3;
var c = cc.moveTo(.3, i);
c.easing(cc.easeElasticOut(2));
e.runAction(c);
}
},
addPop: function(e, t, n, r, o) {
if (cc.director.getScene() && cc.director.getScene().getChildByName("Canvas")) {
t && this.showMode(n);
cc.director.getScene().getChildByName("Canvas").addChild(e, window.facadeMgr.PopOrder + this.popList.length + 1);
e.x = 0;
e.y = 0;
this.popList.push(e);
console.log("addPop:", e.name, e.zIndex);
if (this.doAction) this.fadeInPop(e, r, o); else {
var a = e.getComponent(e.name);
a && null != a.viewDidAppear && a.viewDidAppear();
}
cc.systemEvent.emit(i.POP_ADDED, e.name);
}
},
createNew: function(e, t, i) {
console.log("createNew:", t);
var n = null;
(n = (this.IN_POOL_POPS[t], cc.instantiate(e))).getComponent(t) && null != i && n.getComponent(t).initData(i);
return n;
},
hideLoading: function() {
if (null != this.loading) {
this.loading.removeFromParent();
this.loading = null;
}
if (null != this.loadingAni) {
this.loadingAni.removeFromParent();
this.loadingAni = null;
}
},
showLoading: function() {
if (null == this.loading) {
this.loading = new cc.Node();
this.loading.addComponent(cc.Sprite);
this.loading.getComponent(cc.Sprite).spriteFrame = cc.loader.getRes("modeBg", cc.SpriteFrame);
this.loading.scale = 100;
this.loading.opacity = 40;
this.loading.addComponent(cc.Button);
cc.director.getScene().getChildByName("Canvas").addChild(this.loading, window.facadeMgr.PopOrder - 1);
} else this.loading.active = !0;
},
addPopByName: function(e, t, i, n, r, o, a) {
console.log("addPopByName:", e);
this._tempDatas || (this._tempDatas = {});
this._tempDatas[e] = null;
this.loadingUrls || (this.loadingUrls = {});
for (var s = 0; s < this.popList.length; s++) if (this.popList[s].getName() == e) {
this._clearOne = this.popList[s];
null != this._clearOne.getComponent(this._clearOne.name).doClose && this._clearOne.getComponent(this._clearOne.name).doClose();
this.clearPop();
break;
}
null == n && (n = !0);
this.doAction = 0 != r;
if (this.loadedKeys[e] || cc.loader.getRes("pfb/" + e)) {
var c = cc.loader.getRes("pfb/" + e), l = this.createNew(c, e, t);
l.setName(e);
this.addPop(l, i, n, o, a);
} else {
this._tempDatas[e] = t;
this.showLoading();
this.loadingUrls[e] = !0;
cc.loader.loadRes("pfb/" + e, function(t, r) {
this.hideLoading();
if (this.loadingUrls[e]) {
delete this.loadingUrls[e];
var s = this.createNew(r, e, this._tempDatas[e]);
s.setName(e);
this.loadedKeys[e] = !0;
this.addPop(s, i, n, o, a);
}
}.bind(this));
}
},
addAlert: function(e, t, i, n) {
var r = {
detailString: e,
enterCallBack: t,
needCancel: i,
animSpeed: n
};
this.addPopByName("Alert", r, !0, !0);
},
reset: function() {
this.removeAll();
this.popList = [];
this.modeShowed = !1;
},
onLoad: function() {
this.loadedKeys = {};
this.popList = [];
this.modeShowed = !1;
this.doAction = !0;
cc.loader.loadRes("modeBg", cc.SpriteFrame);
this.IN_POOL_POPS = {
DailyReward: 1,
DatingEdit: 1,
LoveSeaLucky: 1,
VipReward: 1,
WXRank: 1,
InfoEdit: 1,
GoodInfo: 1,
Alert: 1,
Claim: 1
};
}
});
cc._RF.pop();
}, {
ModuleEventEnum: "ModuleEventEnum"
} ],
ProfiledScreenAdapter: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "92a21nO+yNIkproft74yCaC", "ProfiledScreenAdapter");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, e("./Common/StorageUtil")), l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.storageUtil = new c.default();
return t;
}
t.prototype.onLoad = function() {
if (this.storageUtil.isSpaceShapeScreen()) {
this.node.getComponent(cc.Widget).top = .04;
this.node.getComponent(cc.Widget).bottom = -.04;
}
};
t.prototype.start = function() {};
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {
"./Common/StorageUtil": "StorageUtil"
} ],
PropManager: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "51de9HEa5hBT5b1kALJBkd4", "PropManager");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.PropManager = void 0;
var n = e("./BaseConfig"), r = function() {
function e() {}
e.get = function() {
console.log(n.BaseConfig.UserData.property);
return n.BaseConfig.UserData.property;
};
e.set = function(e) {
console.log(n.BaseConfig.UserData.property);
n.BaseConfig.UserData.property.balance = null != e.balance ? e.balance : n.BaseConfig.UserData.property.balance;
n.BaseConfig.UserData.property.items = e.items && e.items.length ? e.items : n.BaseConfig.UserData.property.items;
};
return e;
}();
i.PropManager = r;
cc._RF.pop();
}, {
"./BaseConfig": "BaseConfig"
} ],
RankItem: [ function(e, t) {
"use strict";
cc._RF.push(t, "39c54YEh51B0ro1dmNecyow", "RankItem");
cc.Class({
extends: cc.Component,
properties: {
labelRank: {
type: cc.Label,
default: null
},
labelName: {
type: cc.Label,
default: null
},
labelScore: {
type: cc.Label,
default: null
},
avator: {
type: cc.Sprite,
default: null
},
avatorMask: {
type: cc.Node,
default: null
},
top1Sprite: {
type: cc.SpriteFrame,
default: null
},
top2Sprite: {
type: cc.SpriteFrame,
default: null
},
top3Sprite: {
type: cc.SpriteFrame,
default: null
},
normalSprite: {
type: cc.SpriteFrame,
default: null
},
rankSprite: {
type: cc.Sprite,
default: null
}
},
setUser: function(e, t, i) {
for (var n = this, r = e.nickName ? e.nickName : e.nickname, o = e.avatarUrl, a = e.KVDataList, s = null, c = 0; c < a.length; c++) if (4 == i) {
if ("limit_level" == a[c].key) {
s = a[c];
break;
}
} else if ("level" == a[c].key) {
s = a[c];
break;
}
console.log("setUser user: ", e);
console.log("setUser kvData: ", s);
if (null != s) {
var l;
if (0 == a.length) l = 1; else {
l = s.value;
isNaN(parseInt(l)) && (l = 1);
}
if (1 == t) {
this.rankSprite.getComponent(cc.Sprite).spriteFrame = this.top1Sprite;
this.labelRank.node.active = !1;
this.setTopRankColor();
} else if (2 == t) {
this.rankSprite.getComponent(cc.Sprite).spriteFrame = this.top2Sprite;
this.labelRank.node.active = !1;
this.setTopRankColor();
} else if (3 == t) {
this.rankSprite.getComponent(cc.Sprite).spriteFrame = this.top3Sprite;
this.labelRank.node.active = !1;
this.setTopRankColor();
} else {
this.rankSprite.getComponent(cc.Sprite).spriteFrame = null;
this.labelRank.node.active = !0;
}
this.labelRank.string = t + "";
this.labelScore.string = "第" + l + "关";
this.labelName.string = r;
"" != o && cc.loader.load({
url: o,
type: "png"
}, function(e, t) {
e && console.error(e);
n.avator.spriteFrame = new cc.SpriteFrame(t);
console.log("this.avatormask width = " + n.avator.node.width + ",height = " + n.avator.node.height);
});
} else this.node.active = !1;
},
onLoad: function() {},
start: function() {},
setTopRankColor: function() {
this.labelScore.node.color = new cc.Color().fromHEX("#FA6F00");
this.labelName.node.color = new cc.Color().fromHEX("#FA6F00");
}
});
cc._RF.pop();
}, {} ],
"RecordBean copy": [ function(e, t, i) {
"use strict";
cc._RF.push(t, "626fd63RAlNbIlY9T3xCM2J", "RecordBean copy");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.num = 0;
this.date = "";
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
RecordBean: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "c3825vx1PdPN54k0j8REbFb", "RecordBean");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.num = 0;
this.date = "";
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
RecordShareDialog: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "34e2bGh0C5JB6KMgR5bPwuC", "RecordShareDialog");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./Common/StorageUtil"), h = e("./Bean/RecordBean"), u = e("./platform/analytics/AnalyticsUtilities"), f = e("./Common/WXCommon"), d = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.gameView = null;
t.getBtn = null;
t.closeBtn = null;
t.storageUtil = new l.default();
t.recordData = new h.default();
t.wxCommon = new f.default();
return t;
}
t.prototype.onLoad = function() {};
t.prototype.start = function() {};
t.prototype.show = function() {
var e = this;
u.default.logEvent("record_share_show");
this.node.active = !0;
this.closeBtn.node.active = !1;
setTimeout(function() {
e.closeBtn.node.active = !0;
}, 3e3);
this.recordData = this.storageUtil.getData("recordData");
null == this.recordData && (this.recordData = new h.default());
if (this.recordData.date && this.recordData.date === new Date().getMonth() + "_" + new Date().getDate()) this.recordData.num = this.recordData.num + 1; else {
this.recordData.date = new Date().getMonth() + "_" + new Date().getDate();
this.recordData.num = 1;
}
this.storageUtil.saveData("recordData", this.recordData);
};
t.prototype.onGetBtnClick = function() {
this.wxCommon.shareToFrends("", "", "", "超神时刻分享领取奖励");
var e = new Date().getTime();
this.storageUtil.saveData("shareTime", e);
this.storageUtil.saveData("shareType", this.wxCommon.GOD_SHARE);
this.node.active = !1;
u.default.logEvent("record_share_click");
};
t.prototype.onCloseBtnClick = function() {
var e = cc.find("Canvas").getComponent("MainControl").gameControlView.getComponent("GameControl"), t = e.currentLevel % e.awardStep;
e.showSettleDialog(t);
this.node.active = !1;
};
o([ c(cc.Node) ], t.prototype, "gameView", void 0);
o([ c(cc.Button) ], t.prototype, "getBtn", void 0);
o([ c(cc.Button) ], t.prototype, "closeBtn", void 0);
return o([ s ], t);
}(cc.Component);
i.default = d;
cc._RF.pop();
}, {
"./Bean/RecordBean": "RecordBean",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./platform/analytics/AnalyticsUtilities": "AnalyticsUtilities"
} ],
RedPackage: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "25d18L3rvFPsZVgUfT9JWaI", "RedPackage");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, window.wx, function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.gameview = null;
return t;
}
t.prototype.onLoad = function() {};
t.prototype.start = function() {};
t.prototype.onEnable = function() {
this.node.x = -cc.winSize.width / 2;
var e = cc.moveTo(10, cc.v2(cc.winSize.width, this.node.y)), t = cc.moveTo(10, cc.v2(-cc.winSize.width / 2, this.node.y)), i = cc.sequence(e, t);
this.node.runAction(i.repeatForever());
};
t.prototype.onClick = function() {
this.gameview && this.gameview.m_AwardView.getComponent("AwardViewControl").startAwardFromRedPack();
this.node.active = !1;
};
t.prototype.onDisable = function() {
this.node.stopAllActions();
};
return o([ s ], t);
}(cc.Component));
i.default = c;
cc._RF.pop();
}, {} ],
RemindAnamation: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "53edbywM8hD9pjaDng/gEVK", "RemindAnamation");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, function(e) {
r(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {
this.startAnamation();
};
t.prototype.startAnamation = function() {
this.node.stopAllActions();
var e = this.node.getPosition(), t = cc.moveTo(.3, e.x, e.y + 20);
t.easing(cc.easeSineOut());
var i = cc.scaleTo(.3, .9, 1.1);
i.easing(cc.easeSineOut());
var n = cc.spawn(t, i), r = cc.moveTo(.3, e.x, e.y - 10);
r.easing(cc.easeSineIn());
var o = cc.scaleTo(.3, 1.1, .9);
o.easing(cc.easeSineIn());
var a = cc.spawn(r, o), s = cc.moveTo(.2, e.x, e.y), c = cc.scaleTo(.2, 1, 1), l = cc.spawn(s, c), h = cc.sequence(n, a, l);
h = cc.repeat(h, 2);
h = cc.sequence(h, cc.delayTime(5));
this.node.runAction(cc.repeatForever(h));
};
t.prototype.start = function() {};
return o([ s ], t);
}(cc.Component));
i.default = c;
cc._RF.pop();
}, {} ],
RoundRectMask: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "fe55b3onsRCXqdl/QO/f7Kr", "RoundRectMask");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.RoundRectMask = void 0;
var a = cc._decorator.property, s = cc._decorator.ccclass, c = cc._decorator.executeInEditMode, l = cc._decorator.disallowMultiple, h = cc._decorator.requireComponent, u = cc._decorator.menu, f = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t._radius = 50;
t.mask = null;
return t;
}
Object.defineProperty(t.prototype, "radius", {
get: function() {
return this._radius;
},
set: function(e) {
this._radius = e;
this.updateMask(e);
},
enumerable: !1,
configurable: !0
});
t.prototype.onEnable = function() {
this.mask = this.getComponent(cc.Mask);
this.updateMask(this.radius);
};
t.prototype.updatesMask = function() {
this.mask = this.getComponent(cc.Mask);
this.updateMask(this.radius);
};
t.prototype.updateMask = function(e) {
var t = e >= 0 ? e : 0;
t < 1 && (t = Math.min(this.node.width, this.node.height) * t);
if (this.node.activeInHierarchy) {
this.mask.radius = t;
this.mask.onDraw = this.onDraw.bind(this.mask);
this.mask._updateGraphics = this._updateGraphics.bind(this.mask);
this.mask.type = cc.Mask.Type.RECT;
}
};
t.prototype._updateGraphics = function() {
var e = this._graphics;
e && this.onDraw(e);
};
t.prototype.onDraw = function(e) {
e.clear(!1);
if (this.node.isValid) {
var t = this.node, i = t.width, n = t.height, r = -i * t.anchorX, o = -n * t.anchorY;
e.roundRect(r, o, i, n, this.radius || 0);
cc.game.renderType === cc.game.RENDER_TYPE_CANVAS ? e.stroke() : e.fill();
}
};
o([ a() ], t.prototype, "_radius", void 0);
o([ a({
tooltip: "圆角半径:\n0-1之间为最小边长比例值, \n>1为具体像素值"
}) ], t.prototype, "radius", null);
return o([ s(), c(!0), l(!0), h(cc.Mask), u("渲染组件/圆角遮罩") ], t);
}(cc.Component);
i.RoundRectMask = f;
cc._RF.pop();
}, {} ],
ScrollViewCtrl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "931b7gSw8FB9qQfqXLO6IZb", "ScrollViewCtrl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
}, a = this && this.__awaiter || function(e, t, i, n) {
return new (i || (i = Promise))(function(r, o) {
function a(e) {
try {
c(n.next(e));
} catch (e) {
o(e);
}
}
function s(e) {
try {
c(n.throw(e));
} catch (e) {
o(e);
}
}
function c(e) {
e.done ? r(e.value) : (t = e.value, t instanceof i ? t : new i(function(e) {
e(t);
})).then(a, s);
var t;
}
c((n = n.apply(e, t || [])).next());
});
}, s = this && this.__generator || function(e, t) {
var i, n, r, o, a = {
label: 0,
sent: function() {
if (1 & r[0]) throw r[1];
return r[1];
},
trys: [],
ops: []
};
return o = {
next: s(0),
throw: s(1),
return: s(2)
}, "function" == typeof Symbol && (o[Symbol.iterator] = function() {
return this;
}), o;
function s(e) {
return function(t) {
return c([ e, t ]);
};
}
function c(o) {
if (i) throw new TypeError("Generator is already executing.");
for (;a; ) try {
if (i = 1, n && (r = 2 & o[0] ? n.return : o[0] ? n.throw || ((r = n.return) && r.call(n), 
0) : n.next) && !(r = r.call(n, o[1])).done) return r;
(n = 0, r) && (o = [ 2 & o[0], r.value ]);
switch (o[0]) {
case 0:
case 1:
r = o;
break;

case 4:
a.label++;
return {
value: o[1],
done: !1
};

case 5:
a.label++;
n = o[1];
o = [ 0 ];
continue;

case 7:
o = a.ops.pop();
a.trys.pop();
continue;

default:
if (!(r = a.trys, r = r.length > 0 && r[r.length - 1]) && (6 === o[0] || 2 === o[0])) {
a = 0;
continue;
}
if (3 === o[0] && (!r || o[1] > r[0] && o[1] < r[3])) {
a.label = o[1];
break;
}
if (6 === o[0] && a.label < r[1]) {
a.label = r[1];
r = o;
break;
}
if (r && a.label < r[2]) {
a.label = r[2];
a.ops.push(o);
break;
}
r[2] && a.ops.pop();
a.trys.pop();
continue;
}
o = t.call(e, a);
} catch (e) {
o = [ 6, e ];
n = 0;
} finally {
i = r = 0;
}
if (5 & o[0]) throw o[1];
return {
value: o[0] ? o[1] : void 0,
done: !0
};
}
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var c = cc._decorator, l = c.ccclass, h = c.property, u = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.scrollView = null;
t.itemPrefab = null;
t.worldRankData = new Array();
t.type = 0;
t.itemIndex = 0;
t.executeCount = 0;
return t;
}
t.prototype.setRankType = function(e) {
this.type = e;
};
t.prototype._initItem = function(e) {
if (!(this.executeCount < e)) if (e < this.worldRankData.length) {
this.itemIndex++;
var t = cc.instantiate(this.itemPrefab);
t.width = this.node.width;
t.setPosition(0, 0);
t.parent = this.scrollView.content;
t.getComponent("WorldRankItem").updateData(this.worldRankData[e], this.itemIndex, this.type);
} else if (e == this.worldRankData.length) {
var i = cc.instantiate(this.itemPrefab);
i.getComponent("WorldRankItem").hideAll();
i.width = this.node.width;
i.height = 3 * i.height;
i.color = cc.color().fromHEX("#FF0000");
i.setPosition(0, 0);
i.parent = this.scrollView.content;
i.getComponent("WorldRankItem").setBlankNodeActive(!0);
}
};
t.prototype.directLoad = function(e) {
return a(this, void 0, void 0, function() {
var t = this;
return s(this, function(i) {
switch (i.label) {
case 0:
return [ 4, new Promise(function(i) {
t.scrollView.content.removeAllChildren();
for (var n = 0; n < e; n++) t._initItem(n);
i();
}) ];

case 1:
i.sent();
return [ 2 ];
}
});
});
};
t.prototype.framingLoad = function() {
return a(this, void 0, void 0, function() {
return s(this, function(e) {
switch (e.label) {
case 0:
this.scrollView.content.removeAllChildren();
return [ 4, this.executePreFrame(this._getItemGenerator(this.worldRankData.length), 1) ];

case 1:
e.sent();
return [ 2 ];
}
});
});
};
t.prototype.executePreFrame = function(e, t) {
var i = this;
return new Promise(function(n) {
var r = e;
i.executeCount = 0;
console.log("framingload executePreFrame");
var o = function() {
var e = new Date().getTime();
i.executeCount++;
for (var a = r.next(); ;a = r.next()) {
if (null == a || a.done) {
n();
return;
}
if (new Date().getTime() - e > t) {
i.scheduleOnce(function() {
o();
});
return;
}
}
};
o();
});
};
t.prototype._getItemGenerator = function(e) {
var t;
return s(this, function(i) {
switch (i.label) {
case 0:
this.itemIndex = 0;
t = 0;
i.label = 1;

case 1:
return t < e + 1 ? [ 4, this._initItem(t) ] : [ 3, 4 ];

case 2:
i.sent();
i.label = 3;

case 3:
t++;
return [ 3, 1 ];

case 4:
return [ 2 ];
}
});
};
o([ h(cc.ScrollView) ], t.prototype, "scrollView", void 0);
o([ h(cc.Prefab) ], t.prototype, "itemPrefab", void 0);
return o([ l ], t);
}(cc.Component);
i.default = u;
cc._RF.pop();
}, {} ],
SettleUtil: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "08b4eh1nNVNyLsQ69+Ge69C", "SettleUtil");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.settle_timer_config = "";
this.settle_rank_config = "";
}
e.prototype.loadTimerJson = function(e) {
"" === this.settle_timer_config ? cc.loader.loadRes("timer", cc.JsonAsset, function(t, i) {
if (!t) {
var n = i.json;
this.settle_timer_config = n;
e(n);
}
}.bind(this)) : e(this.settle_timer_config);
};
e.prototype.getOverPlayerPercent = function(e, t, i) {
this.loadTimerJson(function(n) {
var r = this.getValueByNumber(t, n), o = this.getValueByNumber(e, r);
i(this.getRandom(o[1], o[0]));
}.bind(this));
};
e.prototype.getValueByNumber = function(e, t) {
for (var i in t) {
if ("other" == i) return t.other;
var n = Number(i), r = Math.floor(Number(e) / n) * n + n;
if (r === n) return t[r];
}
};
e.prototype.getRandom = function(e, t) {
t *= 100;
e *= 100;
return Math.floor(Math.random() * (t - e + 1) + e) / 100;
};
e.prototype.getOverPlayerNums = function(e) {
var t = 1e5 * (Math.random() + e - 1) + 3e5;
return Math.floor(t);
};
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
SpriteRadius: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "741baMh61BOiYfYSOoDkBqK", "SpriteRadius");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.InputMode = void 0;
var a, s = cc._decorator, c = s.ccclass, l = s.property, h = s.requireComponent, u = s.executeInEditMode, f = s.disallowMultiple, d = s.executionOrder;
(function(e) {
e[e.CocosEditor = 1] = "CocosEditor";
e[e.Web = 2] = "Web";
})(a = i.InputMode || (i.InputMode = {}));
var p = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t._effect = null;
t._inputMode = a.CocosEditor;
t._leftTop = cc.v2();
t._rightTop = cc.v2();
t._rightBottom = cc.v2();
t._leftBottom = cc.v2();
t._configString = "0px";
t.sprite = null;
t.material = null;
return t;
}
Object.defineProperty(t.prototype, "effect", {
get: function() {
return this._effect;
},
set: function(e) {
this._effect = e;
this.init();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "inputMode", {
get: function() {
return this._inputMode;
},
set: function(e) {
this._inputMode = e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "leftTop", {
get: function() {
return this._leftTop;
},
set: function(e) {
this._leftTop = e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "rightTop", {
get: function() {
return this._rightTop;
},
set: function(e) {
this._rightTop = e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "rightBottom", {
get: function() {
return this._rightBottom;
},
set: function(e) {
this._rightBottom = e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "leftBottom", {
get: function() {
return this._leftBottom;
},
set: function(e) {
this._leftBottom = e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
Object.defineProperty(t.prototype, "configString", {
get: function() {
return this._configString;
},
set: function(e) {
this._configString = e;
this.updateProperties();
},
enumerable: !1,
configurable: !0
});
t.prototype.onLoad = function() {
this.init();
};
t.prototype.resetInEditor = function() {
this.init();
};
t.prototype.init = function() {
this.sprite = this.node.getComponent(cc.Sprite);
this.sprite.spriteFrame && (this.sprite.spriteFrame.getTexture().packable = !1);
if (this._effect) {
this.material = cc.Material.create(this._effect);
this.sprite.setMaterial(0, this.material);
this.updateProperties();
} else cc.warn("[SpriteRadius]: 请添加Effect资源");
};
t.prototype.updateProperties = function() {
switch (this._inputMode) {
case a.CocosEditor:
this.cocosEditor(this._leftTop, this._rightTop, this._rightBottom, this._leftBottom);
break;

case a.Web:
this.web(this._configString);
}
};
t.prototype.cocosEditor = function(e, t, i, n) {
this.material.setProperty("size", cc.v2(this.node.width, this.node.height));
this.material.setProperty("leftTop", this.percentageToPixel(e));
this.material.setProperty("rightTop", this.percentageToPixel(t));
this.material.setProperty("rightBottom", this.percentageToPixel(i));
this.material.setProperty("leftBottom", this.percentageToPixel(n));
};
t.prototype.web = function(e) {
if (0 !== e.length) {
do {
e = e.replace("  ", " ");
} while (-1 !== e.indexOf("  "));
e.endsWith(";") && (e = e.slice(0, -1));
this.material.setProperty("size", cc.v2(this.node.width, this.node.height));
for (var t = e.split(" "), i = 0; i < t.length; i++) if (-1 !== t[i].indexOf("px")) t[i] = parseFloat(t[i].slice(0, -2)); else {
if (-1 === t[i].indexOf("%")) {
if ("/" === t[i]) continue;
return;
}
t[i] = parseFloat(t[i].slice(0, -1)) / 100;
}
var n = [];
if (-1 !== t.indexOf("/")) {
if (t.indexOf("/") !== t.lastIndexOf("/")) return;
var r = t.indexOf("/");
if (3 === t.length) {
if (1 !== r) return;
n.push(cc.v2(t[0], t[2]));
n.push.apply(n, n);
n.push.apply(n, n);
} else if (4 === t.length) {
if (1 !== r && 2 !== r) return;
if (1 === r) {
n.push(cc.v2(t[0], t[2]));
n.push(cc.v2(t[0], t[3]));
n.push.apply(n, n);
} else if (2 === r) {
n.push(cc.v2(t[0], t[3]));
n.push(cc.v2(t[1], t[3]));
n.push.apply(n, n);
}
} else if (5 === t.length) {
if (1 !== r && 2 !== r && 3 !== r) return;
if (1 === r) {
n.push(cc.v2(t[0], t[2]));
n.push(cc.v2(t[0], t[3]));
n.push(cc.v2(t[0], t[4]));
n.push(cc.v2(t[0], t[3]));
} else if (2 === r) {
n.push(cc.v2(t[0], t[3]));
n.push(cc.v2(t[1], t[4]));
n.push.apply(n, n);
} else if (3 === r) {
n.push(cc.v2(t[0], t[4]));
n.push(cc.v2(t[1], t[4]));
n.push(cc.v2(t[2], t[4]));
n.push(cc.v2(t[1], t[4]));
}
}
} else {
for (i = 0; i < t.length; i++) n.push(cc.v2(t[i], t[i]));
if (1 === t.length) {
n.push.apply(n, n);
n.push.apply(n, n);
} else 2 === t.length ? n.push.apply(n, n) : 3 === t.length && n.push(n[1]);
}
this.material.setProperty("leftTop", this.percentageToPixel(n[0]));
this.material.setProperty("rightTop", this.percentageToPixel(n[1]));
this.material.setProperty("rightBottom", this.percentageToPixel(n[2]));
this.material.setProperty("leftBottom", this.percentageToPixel(n[3]));
}
};
t.prototype.percentageToPixel = function(e) {
var t = cc.v2();
e.x < 1 ? t.x = e.x * this.node.width : t.x = e.x;
e.y < 1 ? t.y = e.y * this.node.height : t.y = e.y;
return t;
};
o([ l ], t.prototype, "_effect", void 0);
o([ l({
type: cc.EffectAsset,
tooltip: !1
}) ], t.prototype, "effect", null);
o([ l ], t.prototype, "_inputMode", void 0);
o([ l({
type: cc.Enum(a),
tooltip: !1
}) ], t.prototype, "inputMode", null);
o([ l ], t.prototype, "_leftTop", void 0);
o([ l({
tooltip: !1,
visible: function() {
return this.inputMode === a.CocosEditor;
}
}) ], t.prototype, "leftTop", null);
o([ l ], t.prototype, "_rightTop", void 0);
o([ l({
tooltip: !1,
visible: function() {
return this.inputMode === a.CocosEditor;
}
}) ], t.prototype, "rightTop", null);
o([ l ], t.prototype, "_rightBottom", void 0);
o([ l({
tooltip: !1,
visible: function() {
return this.inputMode === a.CocosEditor;
}
}) ], t.prototype, "rightBottom", null);
o([ l ], t.prototype, "_leftBottom", void 0);
o([ l({
tooltip: !1,
visible: function() {
return this.inputMode === a.CocosEditor;
}
}) ], t.prototype, "leftBottom", null);
o([ l ], t.prototype, "_configString", void 0);
o([ l({
tooltip: !1,
visible: function() {
return this.inputMode === a.Web;
}
}) ], t.prototype, "configString", null);
return o([ c, h(cc.Sprite), u, f, d(-100) ], t);
}(cc.Component);
i.default = p;
cc._RF.pop();
}, {} ],
StorageUtil: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "85b33YW3A5Gg6DK1Kxif7VV", "StorageUtil");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, e("./HttpRequest")), s = e("../Bean/WXUserBean"), c = e("../Bean/UserData"), l = function() {
function e() {
this.gameBean = new c.default();
this.httpRequest = new a.default();
this.wxUserBean = new s.default();
this.shareTitle = [ "还记得小时候一起玩的这个吗......", "还记得小时候一起玩的这个吗......", "这小学入学考试题出的太过分了！", "老同学，还记得当年的我们吗？" ];
this.shareImageUrl = [ "http://n.qikucdn.com/t/2re2762ef66d0a066ftsyu.png", "http://n.qikucdn.com/t/2r635f4d4d287c0b13tszo.png", "http://n.qikucdn.com/t/2re6c4c0da38dc3777tt2j.png", "http://n.qikucdn.com/t/2r38e181c1c29ee89att4c.png" ];
this.settleShareLabel = [ "下一关", "领取猫币", "分享", "领取猫币", "免费抽大奖" ];
this.TURNTABLE = 0;
this.INTERSTITIAL = 1;
this.MOREGAMEPAGE = 2;
}
e.prototype.saveData = function(e, t) {
cc.sys.localStorage.setItem(e, JSON.stringify(t));
if ("userData" === e && cc.sys.platform == cc.sys.WECHAT_GAME) {
var i = this.getData("userData").uid;
if (null == i || "" == i || "undefined" == typeof i) return;
this.httpRequest.saveGameData(i, t, function(e) {
console.log("userdata saved", e);
});
}
};
e.prototype.getData = function(e) {
var t = cc.sys.localStorage.getItem(e);
return null == t || "" === t || "undefined" == typeof t ? null : JSON.parse(t);
};
e.prototype.getGameBeanData = function(e) {
var t = cc.sys.localStorage.getItem("userData");
if (null == t || "" === t || "undefined" == typeof t) this.httpRequest.getUserGameData(this.getData("wxUserInfo").uid, function(i) {
console.log("userdata get", i);
if (null == t || "undefined" == typeof t) e(null); else {
this.saveData("userData", i);
e(i);
}
}.bind(this)); else {
var i = JSON.parse(t);
"undefined" == typeof i.gameLevel && (i.gameLevel = 1);
this.saveData("userData", i);
e(JSON.parse(i));
}
};
e.prototype.getNumberData = function(e, t) {
var i = cc.sys.localStorage.getItem(e);
return null == i || "" === i ? t : i;
};
e.prototype.saveNumberData = function(e, t) {
cc.sys.localStorage.setItem(e, t);
};
e.prototype.createGoldAnim = function(e, t, i, n, r, o, a, s) {
for (var c = this.getPoint(i, e.x, e.y, n), l = new Array(), h = 0; h < c.length; h++) {
var u = this.createGold(a, s), f = cc.v2(c[h].x + this.random(0, 30), c[h].y + this.random(0, 30));
u.setPosition(e);
l.push({
gold: u,
randPos: f
});
}
var d = this;
l.sort(function(e, i) {
return d.distance(e.randPos, t) - d.distance(i.randPos, t);
});
var p = !1;
for (h = 0; h < l.length; h++) {
var m = l[h].randPos, g = l[h].gold;
g.zIndex = 1e3;
l[h].gold.id = h;
var y = .5, b = .03;
if (!r) {
y = 0;
b = 0;
}
var v = cc.sequence(cc.moveTo(y, m), cc.delayTime(h * b), cc.moveTo(.5, t), cc.callFunc(function(e) {
if (!p) {
p = !0;
var t = cc.sequence(cc.scaleTo(.1, 1.5, 1.5), cc.scaleTo(.1, 1, 1), cc.callFunc(function() {
p = !1;
}));
o.runAction(t);
}
this.onGoldKilled(e, s);
}.bind(this)));
g.runAction(v);
}
};
e.prototype.createGold = function(e, t) {
var i = t.spawnCoinAnim();
i.parent = e;
return i;
};
e.prototype.onGoldKilled = function(e, t) {
t.despwanCoinAnim(e);
};
e.prototype.getPoint = function(e, t, i, n) {
for (var r = [], o = Math.PI / 180 * Math.round(360 / n), a = 0; a < n; a++) {
var s = t + e * Math.sin(o * a), c = i + e * Math.cos(o * a);
r.unshift({
x: s,
y: c
});
}
return r;
};
e.prototype.random = function(e, t) {
return Math.floor(Math.random() * (t - e)) + e;
};
e.prototype.distance = function(e, t) {
var i = Math.abs(t.x - e.x), n = Math.abs(t.y - e.y);
return Math.sqrt(Math.pow(i, 2) + Math.pow(n, 2));
};
e.prototype.isSpaceShapeScreen = function() {
return cc.winSize.height / cc.winSize.width >= 2;
};
e.prototype.getShareIndex = function() {
return this.random(0, this.shareTitle.length);
};
return n([ o ], e);
}();
i.default = l;
cc._RF.pop();
}, {
"../Bean/UserData": "UserData",
"../Bean/WXUserBean": "WXUserBean",
"./HttpRequest": "HttpRequest"
} ],
SubContextView: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "697bfGsrchIb48f9U6WKIkd", "SubContextView");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.SubContextView = void 0;
var a, s = cc._decorator.ccclass, c = e("./Wx"), l = cc._decorator.property, h = e("./StorageUtil");
(function(e) {
e[e.NONE = 0] = "NONE";
e[e.WEEK = 1] = "WEEK";
e[e.MONTH = 2] = "MONTH";
})(a || (a = {}));
window.wx;
var u = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.sprite = null;
t.texture = new cc.Texture2D();
t.viewName = "";
t.key = "";
t.sortKey = "";
t.storageUtil = new h.default();
t.level = -1;
t.enableUpdate = !1;
t.dateFilter = a.NONE;
return t;
}
t.prototype.onLoad = function() {
this.sprite = this.getComponent(cc.Sprite);
this.sprite || (this.sprite = this.addComponent(cc.Sprite));
};
t.prototype.start = function() {};
t.prototype.onEnable = function() {
console.log("SubContextView onEnable");
this.enableUpdate = !0;
this.node.opacity = 0;
if (c.default.isWeChatEnv()) if (this.viewName.length <= 0 || this.key.length <= 0) console.error("SubContextView 未设置子域viewName 和key ,无法显示子域"); else {
console.log("SubContextView 显示子域:", this.viewName, this.key);
var e = c.default.getShareCanvas();
e.width = this.node.width;
e.height = this.node.height;
cc.find("Canvas").getComponent("MainControl").SubViewRequestCount++;
c.default.sendMsgToContext("action_init_context", {
view: this.viewName,
bound: this.node.getContentSize(),
updateCount: cc.find("Canvas").getComponent("MainControl").SubViewRequestCount
});
this.scheduleOnce(function() {
this.node.opacity = 255;
this.enableUpdate = !1;
this.schedule(function() {
this.render();
}.bind(this), .5, 5, .01);
}.bind(this), 2);
}
};
t.prototype.onDisable = function() {};
t.prototype.updateView = function() {
c.default.sendMsgToContext(this.viewName, {
sortKey: this.sortKey,
key: this.key,
dateFilter: this.dateFilter,
updateCount: cc.find("Canvas").getComponent("MainControl").SubViewRequestCount,
level: this.level
});
};
t.prototype.setPage = function(e) {
c.default.sendMsgToContext("action_paging", {
page: e,
view: this.viewName
});
};
t.prototype.pageUp = function() {
c.default.sendMsgToContext("action_paging", {
offset: -1,
view: this.viewName
});
};
t.prototype.pageDown = function() {
c.default.sendMsgToContext("action_paging", {
offset: 1,
view: this.viewName
});
};
t.prototype.lateUpdate = function() {
(this.enableUpdate || "Forthcoming" != this.viewName) && this.render();
};
t.prototype.render = function() {
if (c.default.isWeChatEnv()) {
var e = c.default.getShareCanvas();
this.texture.initWithElement(e);
this.texture.handleLoadedTexture();
this.sprite.spriteFrame ? this.sprite.spriteFrame.setTexture(this.texture) : this.sprite.spriteFrame = new cc.SpriteFrame(this.texture);
}
};
t.prototype.clearRender = function() {
if (c.default.isWeChatEnv()) if (this.viewName.length <= 0 || this.key.length <= 0) console.error("SubContextView 未设置子域viewName 和key ,无法显示子域"); else {
console.log("SubContextView  clear render 显示子域:", this.viewName, this.key);
var e = c.default.getShareCanvas();
e.width = this.node.width;
e.height = this.node.height;
c.default.sendMsgToContext("close_all", {
view: this.viewName,
bound: this.node.getContentSize()
});
}
};
t.prototype.setSelfData = function(e) {
this.level = e;
this.updateView();
console.log("SubContextView setSelfData level; = ", e);
};
o([ l ], t.prototype, "viewName", void 0);
o([ l ], t.prototype, "key", void 0);
o([ l ], t.prototype, "sortKey", void 0);
o([ l({
type: cc.Enum(a),
serializable: !0,
displayName: "过滤方式",
tooltip: "指定排行数据是否按周/月进行过滤,仅显示本周/月的数据."
}) ], t.prototype, "dateFilter", void 0);
return o([ s ], t);
}(cc.Component);
i.SubContextView = u;
cc._RF.pop();
}, {
"./StorageUtil": "StorageUtil",
"./Wx": "Wx"
} ],
SubDomainControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "d91051lKXJL+p0R4gJssyZD", "SubDomainControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("./LevelUtils"), s = cc._decorator, c = s.ccclass, l = (s.property, window.wx, 
function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.levelUtil = new a.default();
t.bottomTheme = 1;
t.bottomChapter = 1;
return t;
}
t.prototype.removeSubView = function(e, t) {
if (null != t) {
t.opacity = 0;
var i = t.getComponent(cc.BlockInputEvents);
null != i && (i.enabled = !1);
}
this.scheduleOnce(function() {
if (null != t) {
t.opacity = 255;
t.active = !1;
}
null != e && (e.active = !1);
}.bind(this), .5);
};
t.prototype.showSubView = function(e, t, i) {
e.opacity = 0;
e.active = !0;
null != i && (i.active = !0);
this.scheduleOnce(function() {
console.log("Subnode show width = " + e.width);
console.log("Subnode show height = " + e.height);
e.opacity = 255;
}.bind(this), 2);
if (null != i) {
var n = i.getComponent(cc.BlockInputEvents);
null != n && (n.enabled = !0);
}
};
t.prototype.start = function() {};
t.prototype.setBottomTheme = function(e, t) {
this.bottomTheme = e;
this.bottomChapter = t;
};
return o([ c ], t);
}(cc.Component));
i.default = l;
cc._RF.pop();
}, {
"./LevelUtils": "LevelUtils"
} ],
SubPackage: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "ea134FfMp1LCaPVkePKd81c", "SubPackage");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("./base/BaseConfig"), s = cc._decorator, c = s.ccclass, l = (s.property, 
e("./Common/WXCommon")), h = e("./Common/StorageUtil"), u = e("./Common/HttpRequest"), f = e("./Bean/UserData"), d = e("./Bean/WXUserBean"), p = e("./native/JsBridge"), m = e("./native/CallNative"), g = e("./Common/HttpRequestNew"), y = e("./base/BaseUtils"), b = window.wx, v = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.storageUtill = new h.default();
t.wXCommon = new l.default();
t.isCanLoadMain = !1;
t.httpRequest = new u.default();
t.httpRequestNew = new g.default();
t.userBean = null;
t.Membership = !1;
return t;
}
t.prototype.onLoad = function() {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
this.userBean = this.storageUtill.getData("wxUserInfo");
console.log("this.userBean =  ", this.userBean);
if (null == this.userBean || null == this.userBean.openId || null == this.userBean.openId) this.registUser(); else {
b.aldSendOpenid(this.userBean.openId);
this.isCanLoadMain ? this.loadMain() : this.isCanLoadMain = !0;
}
}
};
t.prototype.registUser = function() {
var e = this.userBean, t = this;
if (null == e || null == e.uid) this.wXCommon.login(function(i) {
console.log("wxcode ", i);
null == this.httpRequest && (this.httpRequest = new u.default());
this.httpRequest.login(i, function(i) {
console.log("httpRequest.login ", i);
(e = new d.default()).uid = i.uid;
if (null != i.nickName && "" != i.nickName) {
e.nickName = i.nickName;
e.avatarUrl = i.avatarUrl;
}
e.openId = i.openId;
console.log("data.gameLevel ", i.gameLevel);
var n = new f.default();
n.uid = i.uid;
0 == Number(i.gameLevel) || "undefined" == typeof i.gameLevel ? n.gameLevel = 1 : n.gameLevel = Number(i.gameLevel);
0 == Number(i.limitLevel) || "undefined" == typeof i.limitLevel ? n.limitLevel = 0 : n.limitLevel = Number(i.limitLevel);
0 == Number(i.gold) || "undefined" == typeof i.gold ? n.gold = 0 : n.gold = Number(i.gold);
var r = new h.default();
r.saveData("wxUserInfo", e);
r.saveData("userData", n);
var o = r.getData("userData");
console.log("login userbean uid: ", o.uid);
b.aldSendOpenid(e.openId);
if (n.gameLevel >= 1) {
r.saveData("first_launch", !1);
r.saveData("guaidLevel1", !0);
n.gameLevel >= 2 && r.saveData("guaidLevel2", !0);
n.gameLevel >= 3 && r.saveData("guaidLevel3", !0);
}
t.isCanLoadMain ? t.loadMain() : t.isCanLoadMain = !0;
}.bind(this));
}.bind(this)); else {
console.log("getRemountGameData userBean", e);
this.isCanLoadMain ? this.loadMain() : this.isCanLoadMain = !0;
}
};
t.prototype.start = function() {
var e = this;
if (p.JsBridge.isMobile()) {
cc.CallNative = m.CallNative;
p.JsBridge.isIosPlatform() && cc.sys.localStorage.setItem("Membership", !0);
this.Membership = cc.sys.localStorage.getItem("Membership");
p.JsBridge.callWithCallback(p.JsBridge.DEVICE, {}, function(t) {
if (null != t) {
a.BaseConfig.DeviceInfo.deviceId = t.deviceId;
a.BaseConfig.DeviceInfo.mac = t.mac;
a.BaseConfig.DeviceInfo.version = t.version;
a.BaseConfig.DeviceInfo.uuid = t.uuid;
a.BaseConfig.DeviceInfo.user_id = t.uuid;
a.BaseConfig.DeviceInfo.channel = t.channel;
a.BaseConfig.DeviceInfo.app_code = t.app_code;
a.BaseConfig.DeviceInfo.os = p.JsBridge.isIosPlatform() ? 2 : 1;
a.BaseConfig.DeviceInfo.android_id = t.android_id;
a.BaseConfig.DeviceInfo.oaid = t.oaid;
a.BaseConfig.DeviceInfo.vendor = t.vendor;
a.BaseConfig.DeviceInfo.model = t.model;
a.BaseConfig.DeviceInfo.os_version = t.os_version;
var i = t.params;
e.Membership ? p.JsBridge.callWithCallback(p.JsBridge.MEMBERSHIP, {}, function() {}) : p.JsBridge.IsNull(a.BaseConfig.DeviceInfo.deviceId) || e.httpRequestNew.order(i, function(e) {
console.log("oder result:", e);
!p.JsBridge.IsNull(e.Data) && e.Data.length > 0 && cc.sys.localStorage.setItem("Membership", !0);
});
}
var n = a.BaseConfig.DeviceInfo;
n.ad_time = y.Utils.nowTime().toString();
y.Utils.event("user_active", n);
e.loadMain();
if (p.JsBridge.isMobile()) {
var r = "969359704a9a0be0";
p.JsBridge.isIosPlatform() && (r = "");
p.JsBridge.callWithCallback(p.JsBridge.AD_SPLASH, {}, function(e) {
if (null != e) {
var t = e.resultType;
console.log(t, "======native ad callback=======");
if (null != t) switch (t) {
case 0:
var i = a.BaseConfig.DeviceInfo;
i.ad_time = y.Utils.nowTime().toString();
i.adType = "splash";
i.realEcpm = 0;
i.biddingType = 0;
i.networkPlacementId = r;
y.Utils.event("ad_request", i);
break;

case 1:
var n = a.BaseConfig.DeviceInfo;
n.ad_time = y.Utils.nowTime().toString();
n.ecpm = e.ecpm;
n.adType = "splash";
n.realEcpm = 0;
n.biddingType = 0;
n.networkPlacementId = r;
n.adNetworkPlatformName = e.adPlatform;
y.Utils.event("ad_full", n);
var o = a.BaseConfig.DeviceInfo;
o.ad_time = y.Utils.nowTime().toString();
o.ecpm = e.ecpm;
o.adType = "splash";
o.realEcpm = 0;
o.biddingType = 0;
o.networkPlacementId = r;
o.adNetworkPlatformName = e.adPlatform;
y.Utils.event("ad_show", o);
break;

case 3:
break;

case 7:
var s = a.BaseConfig.DeviceInfo;
s.ad_time = y.Utils.nowTime().toString();
s.ecpm = e.ecpm;
s.adType = "splash";
s.realEcpm = 0;
s.biddingType = 0;
s.networkPlacementId = r;
s.adNetworkPlatformName = e.adPlatform;
y.Utils.event("ad_close", s);
break;

case 8:
var c = a.BaseConfig.DeviceInfo;
c.ad_time = y.Utils.nowTime().toString();
c.ecpm = e.ecpm;
c.adType = "splash";
c.realEcpm = 0;
c.biddingType = 0;
c.networkPlacementId = r;
c.adNetworkPlatformName = e.adPlatform;
y.Utils.event("ad_click", c);
}
}
});
}
});
} else this.loadMain();
};
t.prototype.loadMain = function() {
this.scheduleOnce(function() {
cc.director.loadScene("gameScence");
}, 1);
};
return o([ c ], t);
}(cc.Component);
i.default = v;
cc._RF.pop();
}, {
"./Bean/UserData": "UserData",
"./Bean/WXUserBean": "WXUserBean",
"./Common/HttpRequest": "HttpRequest",
"./Common/HttpRequestNew": "HttpRequestNew",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./base/BaseConfig": "BaseConfig",
"./base/BaseUtils": "BaseUtils",
"./native/CallNative": "CallNative",
"./native/JsBridge": "JsBridge"
} ],
ThemeBox: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "a1e40GAwptBP6n/0SCxL4g+", "ThemeBox");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("../i18n/i18nMgr"), h = e("./Bean/UserData"), u = e("./Common/StorageUtil"), f = e("./GlobalConst"), d = e("./LevelData"), p = e("./base/BaseLayer"), m = (window.wx, 
function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.unlockNode = null;
t.spriteNode = null;
t.txtNode = null;
t.themeView = null;
t.innerLevelGrid = null;
t.m_Chapter = null;
t.m_Theme = null;
t.storageUtil = new u.default();
t.globalConst = new f.default();
t.lockEnable = !0;
return t;
}
t.prototype.onLoad = function() {};
t.prototype.setChapterTheme = function(e, t) {
this.m_Chapter = e;
this.m_Theme = t;
this.changeBoxBg();
};
t.prototype.onClick = function() {
if (1 != this.unlockNode.active) {
if (1 != this.lockEnable) {
this.themeView.active = !1;
this.innerLevelGrid.getParent().active = !0;
this.innerLevelGrid.getComponent("InnerLevelControl").initLevelData(this.m_Theme, this.m_Chapter);
this.themeView.getParent().getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
}
} else {
var e = (this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter, t = ((this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter - 1) * this.globalConst.levelCount + 1;
this.themeView.getComponent("ThemeViewControl").showUnlockView(e, t);
}
};
t.prototype.setInnerGird = function(e) {
this.innerLevelGrid = e;
};
t.prototype.setThemeView = function(e) {
this.themeView = e;
};
t.prototype.changeBoxBg = function() {
var e = this;
this.unlockNode.active = !1;
this.userData = this.storageUtil.getData("userData");
null == this.userData && (this.userData = new h.default());
var t = this.storageUtil.getNumberData("unLockLevel", 0), i = (Math.ceil(t / this.globalConst.levelCount), 
Math.ceil(this.userData.gameLevel / this.globalConst.levelCount), (this.m_Theme - 1) * this.globalConst.chapterCount + this.m_Chapter), n = cc.find("Canvas").getComponent("MainControl"), r = this.storageUtil.getData("unLockTheme" + i), o = [ "#713D97", "#D948A3", "#4852D9", "#30B450", "#E36D28", "#4852D9" ];
if (n.Membership || 1 == i || r) {
this.lockEnable = !1;
var a = new d.default().currentUrl + "/themeIcon/icon" + i + ".png";
this.spriteNode.picLv = i;
this.LoadNetImg(a, function(t) {
if (e.spriteNode.picLv == i) {
var n = new cc.SpriteFrame(t);
cc.loader.setAutoReleaseRecursively(n, !0);
e.spriteNode.getComponent(cc.Sprite).spriteFrame = n;
}
});
this.txtNode.active = !0;
this.txtNode.getComponent(cc.LabelOutline).color = new cc.Color().fromHEX(o[this.m_Theme - 1]);
this.txtNode.getComponent(cc.LabelShadow).color = new cc.Color().fromHEX(o[this.m_Theme - 1]);
this.txtNode.getComponent(cc.Label).string = l.i18nMgr._getLabel("txt_" + (1005 + i));
this.unlockNode.active = !1;
} else {
this.unlockNode.active = !0;
var s = new d.default().currentUrl + "/themeIcon/iconBurl" + i + ".png";
this.spriteNode.picLv = i;
this.LoadNetImg(s, function(t) {
if (e.spriteNode.picLv == i) {
var n = new cc.SpriteFrame(t);
cc.loader.setAutoReleaseRecursively(n, !0);
e.spriteNode.getComponent(cc.Sprite).spriteFrame = n;
}
});
}
var c = "ThemeTitle/bg" + this.m_Theme;
cc.loader.loadRes(c, cc.SpriteFrame, function(e, t) {
if (e) cc.log("error to loadRes: " + c + ", " + e || e.message); else {
cc.loader.setAutoReleaseRecursively(t, !0);
this.node.getComponent(cc.Sprite).spriteFrame = t;
}
}.bind(this));
};
t.prototype.isNull = function(e) {
return "undefined" == typeof e || "" == e;
};
o([ c(cc.Node) ], t.prototype, "unlockNode", void 0);
o([ c(cc.Node) ], t.prototype, "spriteNode", void 0);
o([ c(cc.Node) ], t.prototype, "txtNode", void 0);
return o([ s ], t);
}(p.BaseLayer));
i.default = m;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./Bean/UserData": "UserData",
"./Common/StorageUtil": "StorageUtil",
"./GlobalConst": "GlobalConst",
"./LevelData": "LevelData",
"./base/BaseLayer": "BaseLayer"
} ],
ThemeGridControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "26a4fDZxFZDRI2XuhmcbRdu", "ThemeGridControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = e("../i18n/i18nMgr"), s = cc._decorator, c = s.ccclass, l = s.property, h = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.m_ThemeBoxPrefab = null;
t.m_ThemeLabel = null;
t.m_ThemeBoxArray = new Array();
t.m_InnerGrid = null;
t.m_ThemeView = null;
t.m_RowNum = 3;
t.m_ColNum = 3;
t.m_BoxWidth = 0;
t.m_BoxHeight = 0;
t.m_Theme = 1;
t.m_Chapter = 1;
return t;
}
t.prototype.onLoad = function() {};
t.prototype.initBoxArray = function() {
for (var e = 0; e < this.m_RowNum; e++) {
this.m_ThemeBoxArray.push([]);
for (var t = 0; t < this.m_ColNum; t++) {
var i = cc.instantiate(this.m_ThemeBoxPrefab);
this.m_ThemeBoxArray[e][t] = i;
i.y = 0;
this.node.addChild(this.m_ThemeBoxArray[e][t]);
}
}
this.scheduleOnce(function() {
this.initThemeBoxData();
}, .1);
};
t.prototype.setTheme = function(e) {
this.m_Theme = e;
this.initBoxArray();
this.changeTheme();
};
t.prototype.setInnerGird = function(e) {
this.m_InnerGrid = e;
};
t.prototype.setThemeView = function(e) {
this.m_ThemeView = e;
};
t.prototype.initThemeBoxData = function() {
for (var e = 1, t = 0; t < this.m_RowNum; t++) for (var i = 0; i < this.m_ColNum; i++) {
this.m_ThemeBoxArray[t][i].getComponent("ThemeBox").setChapterTheme(e, this.m_Theme);
null != this.m_InnerGrid && this.m_ThemeBoxArray[t][i].getComponent("ThemeBox").setInnerGird(this.m_InnerGrid);
null != this.m_ThemeView && this.m_ThemeBoxArray[t][i].getComponent("ThemeBox").setThemeView(this.m_ThemeView);
e++;
}
};
t.prototype.initThemeBoxData1 = function() {
for (var e = 1, t = 0; t < this.m_RowNum; t++) for (var i = 0; i < this.m_ColNum; i++) {
this.m_ThemeBoxArray[t][i].getComponent("ThemeBox").setChapterTheme(e, this.m_Theme);
null != this.m_InnerGrid && this.m_ThemeBoxArray[t][i].getComponent("ThemeBox").setInnerGird(this.m_InnerGrid);
null != this.m_ThemeView && this.m_ThemeBoxArray[t][i].getComponent("ThemeBox").setThemeView(this.m_ThemeView);
e++;
}
};
t.prototype.changeTheme = function() {
var e = "ThemeBg/bg" + this.m_Theme;
this.m_ThemeLabel.getComponent(cc.Label).string = a.i18nMgr._getLabel("txt_" + (999 + this.m_Theme));
cc.loader.loadRes(e, cc.SpriteFrame, function(t, i) {
if (t) cc.log("error to loadRes: " + e + ", " + t || t.message); else {
cc.loader.setAutoReleaseRecursively(i, !0);
this.node.parent.getComponent(cc.Sprite).spriteFrame = i;
}
}.bind(this));
};
o([ l(cc.Prefab) ], t.prototype, "m_ThemeBoxPrefab", void 0);
o([ l(cc.Node) ], t.prototype, "m_ThemeLabel", void 0);
return o([ c ], t);
}(cc.Component);
i.default = h;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr"
} ],
ThemeScrollControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "da0bdM/sRFHaoN5vXCn7+UO", "ThemeScrollControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("../i18n/i18nMgr"), h = e("./Bean/UserData"), u = e("./Common/StorageUtil"), f = e("./GlobalConst"), d = (window.wx, 
function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.m_ThemeGridPrefab = null;
t.m_ThemeView = null;
t.m_InnerGridView = null;
t.m_ThemePage = null;
t.m_unLockLabel = null;
t.leftNode = null;
t.rightNode = null;
t.m_ThemeGridArray = new Array();
t.userData = null;
t.storageUtil = new u.default();
t.globalConst = new f.default();
t.m_ArrayLength = t.globalConst.themeCount;
t.pageNum = 0;
return t;
}
t.prototype.onLoad = function() {
var e = this;
this.initThemeArray();
var t = this.storageUtil.getData("playLevel");
cc.log(t, "playLevel");
t && (this.pageNum = t - 1);
this.scheduleOnce(function() {
e.skip();
}, .1);
};
t.prototype.initThemeArray = function() {
for (var e = 0; e < this.m_ArrayLength; e++) {
this.m_ThemeGridArray[e] = cc.instantiate(this.m_ThemeGridPrefab);
this.m_ThemePage.addPage(this.m_ThemeGridArray[e]);
this.m_ThemeGridArray[e].getChildByName("ThemeGridLayout").getComponent("ThemeGridControl").setTheme(e + 1);
this.m_ThemeGridArray[e].getChildByName("ThemeGridLayout").getComponent("ThemeGridControl").setThemeView(this.m_ThemeView);
this.m_ThemeGridArray[e].getChildByName("ThemeGridLayout").getComponent("ThemeGridControl").setInnerGird(this.m_InnerGridView);
}
this.getUnlockNum();
};
t.prototype.getUnlockNum = function() {
this.userData = this.storageUtil.getData("userData");
null == this.userData && (this.userData = new h.default());
var e, t = cc.sys.localStorage.getItem("Membership"), i = cc.find("Canvas").getComponent("MainControl");
if (t) e = l.i18nMgr._getLabel("txt_05").replace("&", (this.globalConst.themeCount * this.globalConst.chapterCount).toString()).replace("@", (this.globalConst.themeCount * this.globalConst.chapterCount).toString()); else {
this.globalConst.themeCount * this.globalConst.chapterCount < i.unLockThemeCount && (i.unLockThemeCount = this.globalConst.themeCount * this.globalConst.chapterCount);
e = l.i18nMgr._getLabel("txt_05").replace("&", i.unLockThemeCount.toString()).replace("@", (this.globalConst.themeCount * this.globalConst.chapterCount).toString());
}
this.m_unLockLabel.string = e;
};
t.prototype.refreshTheme = function() {
for (var e = 0; e < this.m_ThemeGridArray.length; e++) this.m_ThemeGridArray[e].getChildByName("ThemeGridLayout").getComponent("ThemeGridControl").initThemeBoxData1();
this.getUnlockNum();
};
t.prototype.btnRight = function() {
if (this.pageNum != this.globalConst.themeCount - 1) {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.pageNum++;
this.skip();
}
};
t.prototype.skip = function(e) {
e && (this.pageNum = e);
if (this.pageNum == this.globalConst.themeCount - 1) {
this.rightNode.active = !1;
this.leftNode.active = !0;
} else if (0 == this.pageNum) {
this.leftNode.active = !1;
this.rightNode.active = !0;
} else {
this.leftNode.active = !0;
this.rightNode.active = !0;
}
this.m_ThemePage.scrollToPage(this.pageNum, .3);
};
t.prototype.skinEvent = function() {
this.pageNum = this.m_ThemePage.getCurrentPageIndex();
this.skip();
};
t.prototype.btnLeft = function() {
if (0 != this.pageNum) {
cc.find("Canvas").getComponent("MainControl").volumeControl.getComponent("AudioController").playClickButtonAudio();
this.pageNum--;
this.skip();
}
};
o([ c(cc.Prefab) ], t.prototype, "m_ThemeGridPrefab", void 0);
o([ c(cc.Node) ], t.prototype, "m_ThemeView", void 0);
o([ c(cc.Node) ], t.prototype, "m_InnerGridView", void 0);
o([ c(cc.PageView) ], t.prototype, "m_ThemePage", void 0);
o([ c(cc.Label) ], t.prototype, "m_unLockLabel", void 0);
o([ c(cc.Node) ], t.prototype, "leftNode", void 0);
o([ c(cc.Node) ], t.prototype, "rightNode", void 0);
return o([ s ], t);
}(cc.Component));
i.default = d;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./Bean/UserData": "UserData",
"./Common/StorageUtil": "StorageUtil",
"./GlobalConst": "GlobalConst"
} ],
ThemeViewControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "8b7bad+PGNKu5T7p7PTDIBQ", "ThemeViewControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = e("./Common/StorageUtil"), h = e("./Common/WXCommon"), u = e("./CoinsUtil"), f = e("./GlobalConst"), d = e("../i18n/i18nMgr"), p = e("./MainControl"), m = e("./Common/HttpRequestNew"), g = e("./native/JsBridge"), y = e("./scrollViewTools/List"), b = window.wx, v = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.m_ScrollView = null;
t.m_UnlockChapterView = null;
t.m_UnlockAnim = null;
t.GridLayout = null;
t.m_Dialog = null;
t.unlockLabel = null;
t.shopingView = null;
t.shopingLabel = null;
t.btnSprite = [];
t.list3 = null;
t.m_ThemeGridArray = new Array();
t.userData = null;
t.storageUtil = new l.default();
t.globalConst = new f.default();
t.httpRequestNew = new m.default();
t.wxCommon = new h.default();
t.coinsUtil = new u.default();
return t;
}
t.prototype.start = function() {};
t.prototype.onUnlockShareClick = function() {
this.storageUtil.saveData("unLockLevel", 0);
var e = cc.find("Canvas").getComponent("MainControl");
e.volumeControl.getComponent("AudioController").playClickButtonAudio();
e.loadVideoAd(e.UnLockChapter);
};
t.prototype.successLockTheme = function() {
this.storageUtil.saveData("unLockTheme" + this.theme, !0);
var e = cc.find("Canvas").getComponent("MainControl");
e.unLockThemeCount++;
this.storageUtil.saveData("unLockThemeCount", e.unLockThemeCount);
this.storageUtil.saveData("unLockLevel" + this.unlockLevel, 1);
this.storageUtil.saveData("playLevel", Math.ceil(this.theme / this.globalConst.chapterCount));
this.storageUtil.saveData("unLockThemeMaxLevel" + this.theme, this.unlockLevel);
this.m_UnlockAnim.active = !0;
this.m_Dialog.active = !1;
this.m_UnlockAnim.getComponent(dragonBones.ArmatureDisplay).playAnimation("newAnimation", 1);
this.m_UnlockAnim.getComponent(dragonBones.ArmatureDisplay).addEventListener(dragonBones.EventObject.COMPLETE, this.animEventHandler, this);
};
t.prototype.successLockLevel = function() {
cc.log(this.unlockLevel);
this.storageUtil.saveData("unLockLevel" + this.unlockLevel, 1);
var e = Math.ceil(this.unlockLevel / this.globalConst.levelCount);
this.storageUtil.saveData("unLockThemeMaxLevel" + e, this.unlockLevel);
var t = Math.ceil(this.unlockLevel / this.globalConst.chapterCount / this.globalConst.levelCount), i = this.unlockLevel - (t - 1) * this.globalConst.chapterCount * this.globalConst.levelCount, n = Math.ceil(i / this.globalConst.levelCount);
cc.log(t, n, "theme1, chapter");
this.GridLayout.getComponent("InnerLevelControl").initLevelData(t, n);
};
t.prototype.onUnlockCostClick = function() {
var e = cc.find("Canvas").getComponent("MainControl");
e.loadVideoAd(e.unlockTheme);
};
t.prototype.animEventHandler = function(e) {
if (e.type === dragonBones.EventObject.COMPLETE) {
this.m_UnlockAnim.active = !1;
this.m_UnlockChapterView.active = !1;
this.m_ScrollView.getComponent("ThemeScrollControl").refreshTheme();
this.list3.node.active && this.list3.updateAll();
}
};
t.prototype.showUnlockView = function(e, t) {
cc.log(e, t, "theme, unlockLevel");
this.theme = e;
this.unlockLevel = t;
this.m_UnlockChapterView.active = !0;
this.m_Dialog.active = !0;
this.m_Dialog.active = !0;
};
t.prototype.closeUnlockView = function() {
this.m_UnlockChapterView.active = !1;
this.m_ScrollView.getComponent("ThemeScrollControl").refreshTheme();
};
t.prototype.onBackClick = function() {
b.aldSendEvent("解锁界面返回点击");
this.closeUnlockView();
};
t.prototype.refreshTheme = function() {
this.m_ScrollView.getComponent("ThemeScrollControl").refreshTheme();
};
t.prototype.btnOpenShop = function() {
this.shopingView.active = !0;
var e = cc.find("Canvas").getComponent(p.default);
e.volumeControl.getComponent("AudioController").playClickButtonAudio();
this.shopingLabel.string = d.i18nMgr._getLabel(e.Membership ? "txt_116" : "txt_114");
var t = this.shopingView.getChildByName("bg").getComponent(cc.Sprite), i = this.shopingView.getChildByName("btn").getComponent(cc.Sprite), n = i.node.getChildByName("layout").getChildByName("icon").getComponent(cc.Sprite);
if (e.Membership) {
t.node.height = 1028;
t.spriteFrame = this.btnSprite[1];
i.spriteFrame = this.btnSprite[3];
n.spriteFrame = this.btnSprite[5];
i.node.getComponent(cc.Button).enabled = !1;
this.shopingView.getChildByName("recover").active = !1;
} else {
this.shopingView.getChildByName("recover").active = !0;
t.node.height = 1090;
t.spriteFrame = this.btnSprite[0];
i.spriteFrame = this.btnSprite[2];
n.spriteFrame = this.btnSprite[4];
i.getComponent(cc.Button).enabled = !0;
}
};
t.prototype.btnOpenShop1 = function() {
this.m_UnlockChapterView.active = !1;
this.shopingView.active = !0;
};
t.prototype.btnCloseShop = function() {
cc.find("Canvas").getComponent(p.default).volumeControl.getComponent("AudioController").playClickButtonAudio();
this.shopingView.active = !1;
};
t.prototype.btnPaySuccess = function() {
var e = this;
cc.find("Canvas").getComponent(p.default).Membership ? this.shopingView.active = !1 : g.JsBridge.callWithCallback(g.JsBridge.PAY, {}, function(t) {
if (null != t) {
var i = t.resultType, n = t.params;
if (99 == i) {
var r = g.JsBridge.isIosPlatform() ? 1 : 2;
e.httpRequestNew.callback(r, n, function(t) {
console.log("oder result:", t);
if (0 == t.Code || 10004 == t.Code) {
g.JsBridge.callWithCallback(g.JsBridge.CONSUME, {
token: n.token
}, function(t) {
null != t && 1 == t.resultType && e.paySuccess();
});
10004 == t.Code && e.paySuccess();
}
});
}
}
});
};
t.prototype.paySuccess = function() {
this.shopingView.active = !1;
var e = cc.find("Canvas").getComponent(p.default);
e.Membership = !0;
cc.sys.localStorage.setItem("Membership", !0);
this.refreshTheme();
e.hideVideo();
this.list3.node.active && this.list3.updateAll();
g.JsBridge.callWithCallback(g.JsBridge.MEMBERSHIP, {}, function() {});
};
o([ c(cc.Node) ], t.prototype, "m_ScrollView", void 0);
o([ c(cc.Node) ], t.prototype, "m_UnlockChapterView", void 0);
o([ c(cc.Node) ], t.prototype, "m_UnlockAnim", void 0);
o([ c(cc.Node) ], t.prototype, "GridLayout", void 0);
o([ c(cc.Node) ], t.prototype, "m_Dialog", void 0);
o([ c(cc.Label) ], t.prototype, "unlockLabel", void 0);
o([ c(cc.Node) ], t.prototype, "shopingView", void 0);
o([ c(cc.Label) ], t.prototype, "shopingLabel", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "btnSprite", void 0);
o([ c(y.default) ], t.prototype, "list3", void 0);
return o([ s ], t);
}(cc.Component);
i.default = v;
cc._RF.pop();
}, {
"../i18n/i18nMgr": "i18nMgr",
"./CoinsUtil": "CoinsUtil",
"./Common/HttpRequestNew": "HttpRequestNew",
"./Common/StorageUtil": "StorageUtil",
"./Common/WXCommon": "WXCommon",
"./GlobalConst": "GlobalConst",
"./MainControl": "MainControl",
"./native/JsBridge": "JsBridge",
"./scrollViewTools/List": "List"
} ],
ToolsControl: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "63cb6C1QulOToBE7iLTNes4", "ToolsControl");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.colorBg = null;
t.helpBg = null;
t.helpBtn = null;
t.awardBg = null;
t.awardBtn = null;
t.resetBg = null;
t.resetBtn = null;
t.notifyBg = null;
t.notifyCostLabel = null;
t.notifyVideo = null;
return t;
}
t.prototype.onLoad = function() {};
t.prototype.setLimitMode = function() {
this.colorBg.active = !1;
this.helpBg.opacity = 0;
this.awardBg.opacity = 0;
this.resetBg.opacity = 0;
this.resetBtn.interactable = !1;
this.helpBtn.interactable = !1;
this.awardBtn.interactable = !1;
this.notifyCostLabel.node.active = !1;
this.notifyVideo.active = !0;
};
t.prototype.setChallengeMode = function() {
this.colorBg.active = !0;
this.helpBg.opacity = 255;
this.awardBg.opacity = 255;
this.resetBg.opacity = 255;
this.resetBtn.interactable = !0;
this.helpBtn.interactable = !0;
this.awardBtn.interactable = !0;
this.notifyCostLabel.node.active = !0;
};
o([ c(cc.Node) ], t.prototype, "colorBg", void 0);
o([ c(cc.Node) ], t.prototype, "helpBg", void 0);
o([ c(cc.Button) ], t.prototype, "helpBtn", void 0);
o([ c(cc.Node) ], t.prototype, "awardBg", void 0);
o([ c(cc.Button) ], t.prototype, "awardBtn", void 0);
o([ c(cc.Node) ], t.prototype, "resetBg", void 0);
o([ c(cc.Button) ], t.prototype, "resetBtn", void 0);
o([ c(cc.Node) ], t.prototype, "notifyBg", void 0);
o([ c(cc.Label) ], t.prototype, "notifyCostLabel", void 0);
o([ c(cc.Node) ], t.prototype, "notifyVideo", void 0);
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {} ],
UserData: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "4d735TwNDVIDpdyDdf6PWpT", "UserData");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.uid = "";
this.gold = 0;
this.gameLevel = 1;
this.limitLevel = 0;
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
UserInfo: [ function(e, t) {
"use strict";
cc._RF.push(t, "f54225DQ21JJ5BS3WcIEPlk", "UserInfo");
t.exports = {
token: "",
userId: "",
nickName: null,
gameUrl: null,
avatarUrl: null,
gender: null,
isAuthorize: !1
};
cc._RF.pop();
}, {} ],
WXCommon: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "dbe97/iOpVIwaV23sCvfV+R", "WXCommon");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = e("./StorageUtil"), o = cc._decorator, a = o.ccclass, s = (o.property, window.wx), c = function() {
function e() {
this.REQUEST_HELPER = 0;
this.SHARE_ANSWER = 1;
this.SHARE_INVITE = 2;
this.PICK_ICON_AWARD = 3;
this.PICK_PRESSURE = 4;
this.PICK_DOUBLE = 5;
this.PICK_REVIVE = 6;
this.PICK_UNLOCK = 7;
this.PICK_ANSWER = 8;
this.SHARE_NOTIFY = 9;
this.PICK_DAYAWARD = 10;
this.PICK_ADD_COIN = 11;
this.SHARE_SETTLE = 13;
this.GOD_SHARE = 14;
this.type = 0;
this.storageUtil = new r.default();
this.hasShared = !1;
}
e.prototype.shareToFrends = function(e, t, i, n) {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
this.hasShared = !0;
var r = 0;
if ("" == e) {
r = this.storageUtil.getShareIndex();
console.log("share index =", r);
e = this.storageUtil.shareTitle[r];
t = this.storageUtil.shareImageUrl[r];
}
this.requestWx(e, t, "", n);
}
};
e.prototype.requestHelper = function(e, t, i, n) {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
e = "我卡在第" + i + "关了，帮我过一下~~";
var r = n.nickName;
null != r && "" != r && "undefined" != typeof r || (r = "好友");
var o = "uid=" + n.uid + "&level=" + i + "&nickName=" + r + "&avatarUrl=" + n.avatarUrl + "&type=" + this.REQUEST_HELPER + "&time=" + this.getTime();
console.log("requestHelper query = " + o);
this.requestWx(e, t, o, "闯关赛求助分享");
}
};
e.prototype.shareAnswer = function(e, t, i, n) {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
var r = n.nickName;
null != r && "" != r && "undefined" != typeof r || (r = "好友");
e = r + "做不出来的难题被我解决了，答案在此，要的速来！";
var o = "uid=" + n.uid + "&level=" + i + "&nickName=" + r + "&avatarUrl=" + n.avatarUrl + "&type=" + this.SHARE_ANSWER + "&time=" + this.getTime();
console.log("requestHelper query = " + o);
this.requestWx(e, t, o, "求助分享答案分享");
}
};
e.prototype.inviteFriends = function(e, t, i) {
var n = "uid=" + i + "&type=" + this.SHARE_INVITE + "&time=" + this.getTime();
console.log("inviteFriends query = " + n);
this.requestWx(e, t, n, "邀请分享");
};
e.prototype.getTime = function() {
return new Date().getTime();
};
e.prototype.requestWx = function(e, t, i, n) {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
"" == e && (e = "能一口气画完的，都是天才！！");
"" == t && (t = "http://n.qikucdn.com/t/2i75bf43df4254fe83ta52.png");
s.aldShareAppMessage({
title: e,
imageUrl: t,
ald_desc: n,
query: i
});
var r = this.getTime();
console.log("currentTime = " + r);
cc.sys.localStorage.setItem("requestTime", r);
}
};
e.prototype.share = function(e, t) {
if (cc.sys.platform == cc.sys.WECHAT_GAME) {
s.shareAppMessage({
title: e,
imageUrl: t
});
var i = this.getTime();
console.log("currentTime = " + i);
cc.sys.localStorage.setItem("shareTime", i);
}
};
e.prototype.saveDataToWx = function(e) {
var t = new Array();
t.push({
key: "level",
value: String(e)
});
t.push({
key: "time",
value: String(this.getTime())
});
s.setUserCloudStorage({
KVDataList: t,
success: function(e) {
console.log("ok success", e);
},
fail: function(e) {
console.log("fail " + e);
}
});
};
e.prototype.saveLimitDataToWx = function(e) {
var t = new Array();
t.push({
key: "limit_level",
value: String(e)
});
t.push({
key: "limit_time",
value: String(this.getTime())
});
s.setUserCloudStorage({
KVDataList: t,
success: function(e) {
console.log("upload limit data ok success", e);
},
fail: function(e) {
console.log("upload limit data fail " + e);
}
});
};
e.prototype.login = function(e) {
s.login({
success: function(t) {
if (t.code) {
console.log("登录成功！" + t.code);
e(t.code);
} else console.log("登录失败！" + t.errMsg);
}
});
};
e.prototype.start = function() {};
e.prototype.getAldShareDesc = function(e) {
var t = "";
e == this.PICK_ICON_AWARD ? t = "闯关赛奖励结算界面再次分享" : e == this.PICK_PRESSURE ? t = "闯关赛宝箱领取再次分享" : e == this.PICK_DOUBLE ? t = "闯关赛领取奖励再次分享" : e == this.PICK_REVIVE ? t = "挑战赛失败复活再次分享" : e == this.PICK_UNLOCK ? t = "解锁界面再次分享" : e == this.SHARE_ANSWER ? t = "求助答案再次分享" : e == this.PICK_ANSWER ? t = "求助分享答案奖励领取再次分享" : e == this.SHARE_NOTIFY ? t = "提示再次分享" : e == this.PICK_DAYAWARD ? t = "登陆领取再次分享" : e == this.PICK_ADD_COIN && (t = "闯关赛添加猫币再次分享");
return t;
};
return n([ a("WXCommon") ], e);
}();
i.default = c;
cc._RF.pop();
}, {
"./StorageUtil": "StorageUtil"
} ],
WXUserBean: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "bada0ZEGBVPmbutdxnkJz+n", "WXUserBean");
var n = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var r = cc._decorator, o = r.ccclass, a = (r.property, function() {
function e() {
this.nickName = null;
this.uid = null;
this.avatarUrl = null;
this.gameLevel = 0;
this.openID = null;
}
return n([ o ], e);
}());
i.default = a;
cc._RF.pop();
}, {} ],
Wx: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "c67bbNIbltFSKFxNw9dxZyX", "Wx");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.wx = void 0;
i.wx = window.wx || !1;
var n = function() {
function e() {}
e.isWeChatEnv = function() {
return !1;
};
e.updateUserData = function(t, n, r) {
return e.isWeChatEnv() ? t && n ? new Promise(function(e, o) {
n.uuid = "" + Math.random();
n.timestamp = cc.sys.now();
r && (n.wxgame = {
score: n[r],
update_time: n.timestamp
});
i.wx.setUserCloudStorage({
KVDataList: [ {
key: t,
value: JSON.stringify(n)
} ],
success: function(t) {
cc.log("上传排行榜成功.", t);
e(t);
},
fail: function(e) {
cc.log("上传排行榜失败.", e);
o(e);
}
});
}) : void 0 : Promise.resolve();
};
e.removeUserData = function() {
for (var t = [], n = 0; n < arguments.length; n++) t[n] = arguments[n];
e.isWeChatEnv() && i.wx.removeUserCloudStorage({
keyList: t,
success: function() {
cc.log("清除玩家数据成功.");
},
fail: function() {
cc.log("清除玩家数据失败.", t);
}
});
};
e.getShareContext = function() {
return e.isWeChatEnv() ? i.wx.getOpenDataContext() : null;
};
e.getShareCanvas = function() {
return e.isWeChatEnv() ? i.wx.getOpenDataContext().canvas : null;
};
e.clearSubContext = function() {
if (e.isWeChatEnv()) {
var t = i.wx.getOpenDataContext();
if (t) {
t.width = cc.winSize.width;
t.height = cc.winSize.height;
cc.log("屏幕尺寸:", cc.winSize);
}
}
};
e.sendMsgToContext = function(e, t) {
var i = this.getShareContext();
null != i ? i.postMessage({
what: e,
arguments: t
}) : console.info("微信环境异常.无法调用context.");
};
e.updateSubContextDrawable = function(t, i) {
return new Promise(function() {
t.spriteFrame = e.getSubContextDrawable(i);
});
};
e.getSubContextDrawable = function(e) {
var t = this.getShareCanvas();
if (t) {
var i = e;
e || (i = this.texture);
i.initWithElement(t);
i.handleLoadedTexture();
return new cc.SpriteFrame(i);
}
};
e.clearUserCloudStorage = function(t) {
e.isWeChatEnv() && i.wx.removeUserCloudStorage({
keyList: t || [],
success: function(e) {
console.warn("已成功清除微信托管数据", e);
}
});
};
e.texture = new cc.Texture2D();
return e;
}();
i.default = n;
cc._RF.pop();
}, {} ],
"crypto-js": [ function(e, t, i) {
(function(n) {
"use strict";
cc._RF.push(t, "8aa01vC/n1Lnbs8356+fu/r", "crypto-js");
r = function() {
var i, r, o, a, s, c = c || function(t) {
var i;
"undefined" != typeof window && window.crypto && (i = window.crypto);
"undefined" != typeof self && self.crypto && (i = self.crypto);
"undefined" != typeof globalThis && globalThis.crypto && (i = globalThis.crypto);
!i && "undefined" != typeof window && window.msCrypto && (i = window.msCrypto);
!i && "undefined" != typeof n && n.crypto && (i = n.crypto);
if (!i && "function" == typeof e) try {
i = e("crypto");
} catch (e) {}
var r = function() {
if (i) {
if ("function" == typeof i.getRandomValues) try {
return i.getRandomValues(new Uint32Array(1))[0];
} catch (e) {}
if ("function" == typeof i.randomBytes) try {
return i.randomBytes(4).readInt32LE();
} catch (e) {}
}
throw new Error("Native crypto module could not be used to get secure random number.");
}, o = Object.create || function() {
function e() {}
return function(t) {
var i;
e.prototype = t;
i = new e();
e.prototype = null;
return i;
};
}(), a = {}, s = a.lib = {}, c = s.Base = {
extend: function(e) {
var t = o(this);
e && t.mixIn(e);
t.hasOwnProperty("init") && this.init !== t.init || (t.init = function() {
t.$super.init.apply(this, arguments);
});
t.init.prototype = t;
t.$super = this;
return t;
},
create: function() {
var e = this.extend();
e.init.apply(e, arguments);
return e;
},
init: function() {},
mixIn: function(e) {
for (var t in e) e.hasOwnProperty(t) && (this[t] = e[t]);
e.hasOwnProperty("toString") && (this.toString = e.toString);
},
clone: function() {
return this.init.prototype.extend(this);
}
}, l = s.WordArray = c.extend({
init: function(e, t) {
e = this.words = e || [];
this.sigBytes = null != t ? t : 4 * e.length;
},
toString: function(e) {
return (e || u).stringify(this);
},
concat: function(e) {
var t = this.words, i = e.words, n = this.sigBytes, r = e.sigBytes;
this.clamp();
if (n % 4) for (var o = 0; o < r; o++) {
var a = i[o >>> 2] >>> 24 - o % 4 * 8 & 255;
t[n + o >>> 2] |= a << 24 - (n + o) % 4 * 8;
} else for (var s = 0; s < r; s += 4) t[n + s >>> 2] = i[s >>> 2];
this.sigBytes += r;
return this;
},
clamp: function() {
var e = this.words, i = this.sigBytes;
e[i >>> 2] &= 4294967295 << 32 - i % 4 * 8;
e.length = t.ceil(i / 4);
},
clone: function() {
var e = c.clone.call(this);
e.words = this.words.slice(0);
return e;
},
random: function(e) {
for (var t = [], i = 0; i < e; i += 4) t.push(r());
return new l.init(t, e);
}
}), h = a.enc = {}, u = h.Hex = {
stringify: function(e) {
for (var t = e.words, i = e.sigBytes, n = [], r = 0; r < i; r++) {
var o = t[r >>> 2] >>> 24 - r % 4 * 8 & 255;
n.push((o >>> 4).toString(16));
n.push((15 & o).toString(16));
}
return n.join("");
},
parse: function(e) {
for (var t = e.length, i = [], n = 0; n < t; n += 2) i[n >>> 3] |= parseInt(e.substr(n, 2), 16) << 24 - n % 8 * 4;
return new l.init(i, t / 2);
}
}, f = h.Latin1 = {
stringify: function(e) {
for (var t = e.words, i = e.sigBytes, n = [], r = 0; r < i; r++) {
var o = t[r >>> 2] >>> 24 - r % 4 * 8 & 255;
n.push(String.fromCharCode(o));
}
return n.join("");
},
parse: function(e) {
for (var t = e.length, i = [], n = 0; n < t; n++) i[n >>> 2] |= (255 & e.charCodeAt(n)) << 24 - n % 4 * 8;
return new l.init(i, t);
}
}, d = h.Utf8 = {
stringify: function(e) {
try {
return decodeURIComponent(escape(f.stringify(e)));
} catch (e) {
throw new Error("Malformed UTF-8 data");
}
},
parse: function(e) {
return f.parse(unescape(encodeURIComponent(e)));
}
}, p = s.BufferedBlockAlgorithm = c.extend({
reset: function() {
this._data = new l.init();
this._nDataBytes = 0;
},
_append: function(e) {
"string" == typeof e && (e = d.parse(e));
this._data.concat(e);
this._nDataBytes += e.sigBytes;
},
_process: function(e) {
var i, n = this._data, r = n.words, o = n.sigBytes, a = this.blockSize, s = o / (4 * a), c = (s = e ? t.ceil(s) : t.max((0 | s) - this._minBufferSize, 0)) * a, h = t.min(4 * c, o);
if (c) {
for (var u = 0; u < c; u += a) this._doProcessBlock(r, u);
i = r.splice(0, c);
n.sigBytes -= h;
}
return new l.init(i, h);
},
clone: function() {
var e = c.clone.call(this);
e._data = this._data.clone();
return e;
},
_minBufferSize: 0
}), m = (s.Hasher = p.extend({
cfg: c.extend(),
init: function(e) {
this.cfg = this.cfg.extend(e);
this.reset();
},
reset: function() {
p.reset.call(this);
this._doReset();
},
update: function(e) {
this._append(e);
this._process();
return this;
},
finalize: function(e) {
e && this._append(e);
return this._doFinalize();
},
blockSize: 16,
_createHelper: function(e) {
return function(t, i) {
return new e.init(i).finalize(t);
};
},
_createHmacHelper: function(e) {
return function(t, i) {
return new m.HMAC.init(e, i).finalize(t);
};
}
}), a.algo = {});
return a;
}(Math);
r = (i = c).lib, o = r.Base, a = r.WordArray, (s = i.x64 = {}).Word = o.extend({
init: function(e, t) {
this.high = e;
this.low = t;
}
}), s.WordArray = o.extend({
init: function(e, t) {
e = this.words = e || [];
this.sigBytes = null != t ? t : 8 * e.length;
},
toX32: function() {
for (var e = this.words, t = e.length, i = [], n = 0; n < t; n++) {
var r = e[n];
i.push(r.high);
i.push(r.low);
}
return a.create(i, this.sigBytes);
},
clone: function() {
for (var e = o.clone.call(this), t = e.words = this.words.slice(0), i = t.length, n = 0; n < i; n++) t[n] = t[n].clone();
return e;
}
});
(function() {
if ("function" == typeof ArrayBuffer) {
var e = c.lib.WordArray, t = e.init;
(e.init = function(e) {
e instanceof ArrayBuffer && (e = new Uint8Array(e));
(e instanceof Int8Array || "undefined" != typeof Uint8ClampedArray && e instanceof Uint8ClampedArray || e instanceof Int16Array || e instanceof Uint16Array || e instanceof Int32Array || e instanceof Uint32Array || e instanceof Float32Array || e instanceof Float64Array) && (e = new Uint8Array(e.buffer, e.byteOffset, e.byteLength));
if (e instanceof Uint8Array) {
for (var i = e.byteLength, n = [], r = 0; r < i; r++) n[r >>> 2] |= e[r] << 24 - r % 4 * 8;
t.call(this, n, i);
} else t.apply(this, arguments);
}).prototype = e;
}
})();
(function() {
var e = c, t = e.lib.WordArray, i = e.enc;
i.Utf16 = i.Utf16BE = {
stringify: function(e) {
for (var t = e.words, i = e.sigBytes, n = [], r = 0; r < i; r += 2) {
var o = t[r >>> 2] >>> 16 - r % 4 * 8 & 65535;
n.push(String.fromCharCode(o));
}
return n.join("");
},
parse: function(e) {
for (var i = e.length, n = [], r = 0; r < i; r++) n[r >>> 1] |= e.charCodeAt(r) << 16 - r % 2 * 16;
return t.create(n, 2 * i);
}
};
i.Utf16LE = {
stringify: function(e) {
for (var t = e.words, i = e.sigBytes, r = [], o = 0; o < i; o += 2) {
var a = n(t[o >>> 2] >>> 16 - o % 4 * 8 & 65535);
r.push(String.fromCharCode(a));
}
return r.join("");
},
parse: function(e) {
for (var i = e.length, r = [], o = 0; o < i; o++) r[o >>> 1] |= n(e.charCodeAt(o) << 16 - o % 2 * 16);
return t.create(r, 2 * i);
}
};
function n(e) {
return e << 8 & 4278255360 | e >>> 8 & 16711935;
}
})();
(function() {
var e = c, t = e.lib.WordArray;
e.enc.Base64 = {
stringify: function(e) {
var t = e.words, i = e.sigBytes, n = this._map;
e.clamp();
for (var r = [], o = 0; o < i; o += 3) for (var a = (t[o >>> 2] >>> 24 - o % 4 * 8 & 255) << 16 | (t[o + 1 >>> 2] >>> 24 - (o + 1) % 4 * 8 & 255) << 8 | t[o + 2 >>> 2] >>> 24 - (o + 2) % 4 * 8 & 255, s = 0; s < 4 && o + .75 * s < i; s++) r.push(n.charAt(a >>> 6 * (3 - s) & 63));
var c = n.charAt(64);
if (c) for (;r.length % 4; ) r.push(c);
return r.join("");
},
parse: function(e) {
var t = e.length, n = this._map, r = this._reverseMap;
if (!r) {
r = this._reverseMap = [];
for (var o = 0; o < n.length; o++) r[n.charCodeAt(o)] = o;
}
var a = n.charAt(64);
if (a) {
var s = e.indexOf(a);
-1 !== s && (t = s);
}
return i(e, t, r);
},
_map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
};
function i(e, i, n) {
for (var r = [], o = 0, a = 0; a < i; a++) if (a % 4) {
var s = n[e.charCodeAt(a - 1)] << a % 4 * 2 | n[e.charCodeAt(a)] >>> 6 - a % 4 * 2;
r[o >>> 2] |= s << 24 - o % 4 * 8;
o++;
}
return t.create(r, o);
}
})();
(function() {
var e = c, t = e.lib.WordArray;
e.enc.Base64url = {
stringify: function(e, t) {
void 0 === t && (t = !0);
var i = e.words, n = e.sigBytes, r = t ? this._safe_map : this._map;
e.clamp();
for (var o = [], a = 0; a < n; a += 3) for (var s = (i[a >>> 2] >>> 24 - a % 4 * 8 & 255) << 16 | (i[a + 1 >>> 2] >>> 24 - (a + 1) % 4 * 8 & 255) << 8 | i[a + 2 >>> 2] >>> 24 - (a + 2) % 4 * 8 & 255, c = 0; c < 4 && a + .75 * c < n; c++) o.push(r.charAt(s >>> 6 * (3 - c) & 63));
var l = r.charAt(64);
if (l) for (;o.length % 4; ) o.push(l);
return o.join("");
},
parse: function(e, t) {
void 0 === t && (t = !0);
var n = e.length, r = t ? this._safe_map : this._map, o = this._reverseMap;
if (!o) {
o = this._reverseMap = [];
for (var a = 0; a < r.length; a++) o[r.charCodeAt(a)] = a;
}
var s = r.charAt(64);
if (s) {
var c = e.indexOf(s);
-1 !== c && (n = c);
}
return i(e, n, o);
},
_map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
_safe_map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
};
function i(e, i, n) {
for (var r = [], o = 0, a = 0; a < i; a++) if (a % 4) {
var s = n[e.charCodeAt(a - 1)] << a % 4 * 2 | n[e.charCodeAt(a)] >>> 6 - a % 4 * 2;
r[o >>> 2] |= s << 24 - o % 4 * 8;
o++;
}
return t.create(r, o);
}
})();
(function(e) {
var t = c, i = t.lib, n = i.WordArray, r = i.Hasher, o = t.algo, a = [];
(function() {
for (var t = 0; t < 64; t++) a[t] = 4294967296 * e.abs(e.sin(t + 1)) | 0;
})();
var s = o.MD5 = r.extend({
_doReset: function() {
this._hash = new n.init([ 1732584193, 4023233417, 2562383102, 271733878 ]);
},
_doProcessBlock: function(e, t) {
for (var i = 0; i < 16; i++) {
var n = t + i, r = e[n];
e[n] = 16711935 & (r << 8 | r >>> 24) | 4278255360 & (r << 24 | r >>> 8);
}
var o = this._hash.words, s = e[t + 0], c = e[t + 1], d = e[t + 2], p = e[t + 3], m = e[t + 4], g = e[t + 5], y = e[t + 6], b = e[t + 7], v = e[t + 8], _ = e[t + 9], w = e[t + 10], C = e[t + 11], S = e[t + 12], A = e[t + 13], M = e[t + 14], B = e[t + 15], x = o[0], k = o[1], T = o[2], D = o[3];
x = l(x, k, T, D, s, 7, a[0]);
D = l(D, x, k, T, c, 12, a[1]);
T = l(T, D, x, k, d, 17, a[2]);
k = l(k, T, D, x, p, 22, a[3]);
x = l(x, k, T, D, m, 7, a[4]);
D = l(D, x, k, T, g, 12, a[5]);
T = l(T, D, x, k, y, 17, a[6]);
k = l(k, T, D, x, b, 22, a[7]);
x = l(x, k, T, D, v, 7, a[8]);
D = l(D, x, k, T, _, 12, a[9]);
T = l(T, D, x, k, w, 17, a[10]);
k = l(k, T, D, x, C, 22, a[11]);
x = l(x, k, T, D, S, 7, a[12]);
D = l(D, x, k, T, A, 12, a[13]);
T = l(T, D, x, k, M, 17, a[14]);
x = h(x, k = l(k, T, D, x, B, 22, a[15]), T, D, c, 5, a[16]);
D = h(D, x, k, T, y, 9, a[17]);
T = h(T, D, x, k, C, 14, a[18]);
k = h(k, T, D, x, s, 20, a[19]);
x = h(x, k, T, D, g, 5, a[20]);
D = h(D, x, k, T, w, 9, a[21]);
T = h(T, D, x, k, B, 14, a[22]);
k = h(k, T, D, x, m, 20, a[23]);
x = h(x, k, T, D, _, 5, a[24]);
D = h(D, x, k, T, M, 9, a[25]);
T = h(T, D, x, k, p, 14, a[26]);
k = h(k, T, D, x, v, 20, a[27]);
x = h(x, k, T, D, A, 5, a[28]);
D = h(D, x, k, T, d, 9, a[29]);
T = h(T, D, x, k, b, 14, a[30]);
x = u(x, k = h(k, T, D, x, S, 20, a[31]), T, D, g, 4, a[32]);
D = u(D, x, k, T, v, 11, a[33]);
T = u(T, D, x, k, C, 16, a[34]);
k = u(k, T, D, x, M, 23, a[35]);
x = u(x, k, T, D, c, 4, a[36]);
D = u(D, x, k, T, m, 11, a[37]);
T = u(T, D, x, k, b, 16, a[38]);
k = u(k, T, D, x, w, 23, a[39]);
x = u(x, k, T, D, A, 4, a[40]);
D = u(D, x, k, T, s, 11, a[41]);
T = u(T, D, x, k, p, 16, a[42]);
k = u(k, T, D, x, y, 23, a[43]);
x = u(x, k, T, D, _, 4, a[44]);
D = u(D, x, k, T, S, 11, a[45]);
T = u(T, D, x, k, B, 16, a[46]);
x = f(x, k = u(k, T, D, x, d, 23, a[47]), T, D, s, 6, a[48]);
D = f(D, x, k, T, b, 10, a[49]);
T = f(T, D, x, k, M, 15, a[50]);
k = f(k, T, D, x, g, 21, a[51]);
x = f(x, k, T, D, S, 6, a[52]);
D = f(D, x, k, T, p, 10, a[53]);
T = f(T, D, x, k, w, 15, a[54]);
k = f(k, T, D, x, c, 21, a[55]);
x = f(x, k, T, D, v, 6, a[56]);
D = f(D, x, k, T, B, 10, a[57]);
T = f(T, D, x, k, y, 15, a[58]);
k = f(k, T, D, x, A, 21, a[59]);
x = f(x, k, T, D, m, 6, a[60]);
D = f(D, x, k, T, C, 10, a[61]);
T = f(T, D, x, k, d, 15, a[62]);
k = f(k, T, D, x, _, 21, a[63]);
o[0] = o[0] + x | 0;
o[1] = o[1] + k | 0;
o[2] = o[2] + T | 0;
o[3] = o[3] + D | 0;
},
_doFinalize: function() {
var t = this._data, i = t.words, n = 8 * this._nDataBytes, r = 8 * t.sigBytes;
i[r >>> 5] |= 128 << 24 - r % 32;
var o = e.floor(n / 4294967296), a = n;
i[15 + (r + 64 >>> 9 << 4)] = 16711935 & (o << 8 | o >>> 24) | 4278255360 & (o << 24 | o >>> 8);
i[14 + (r + 64 >>> 9 << 4)] = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8);
t.sigBytes = 4 * (i.length + 1);
this._process();
for (var s = this._hash, c = s.words, l = 0; l < 4; l++) {
var h = c[l];
c[l] = 16711935 & (h << 8 | h >>> 24) | 4278255360 & (h << 24 | h >>> 8);
}
return s;
},
clone: function() {
var e = r.clone.call(this);
e._hash = this._hash.clone();
return e;
}
});
function l(e, t, i, n, r, o, a) {
var s = e + (t & i | ~t & n) + r + a;
return (s << o | s >>> 32 - o) + t;
}
function h(e, t, i, n, r, o, a) {
var s = e + (t & n | i & ~n) + r + a;
return (s << o | s >>> 32 - o) + t;
}
function u(e, t, i, n, r, o, a) {
var s = e + (t ^ i ^ n) + r + a;
return (s << o | s >>> 32 - o) + t;
}
function f(e, t, i, n, r, o, a) {
var s = e + (i ^ (t | ~n)) + r + a;
return (s << o | s >>> 32 - o) + t;
}
t.MD5 = r._createHelper(s);
t.HmacMD5 = r._createHmacHelper(s);
})(Math);
(function() {
var e = c, t = e.lib, i = t.WordArray, n = t.Hasher, r = e.algo, o = [], a = r.SHA1 = n.extend({
_doReset: function() {
this._hash = new i.init([ 1732584193, 4023233417, 2562383102, 271733878, 3285377520 ]);
},
_doProcessBlock: function(e, t) {
for (var i = this._hash.words, n = i[0], r = i[1], a = i[2], s = i[3], c = i[4], l = 0; l < 80; l++) {
if (l < 16) o[l] = 0 | e[t + l]; else {
var h = o[l - 3] ^ o[l - 8] ^ o[l - 14] ^ o[l - 16];
o[l] = h << 1 | h >>> 31;
}
var u = (n << 5 | n >>> 27) + c + o[l];
u += l < 20 ? 1518500249 + (r & a | ~r & s) : l < 40 ? 1859775393 + (r ^ a ^ s) : l < 60 ? (r & a | r & s | a & s) - 1894007588 : (r ^ a ^ s) - 899497514;
c = s;
s = a;
a = r << 30 | r >>> 2;
r = n;
n = u;
}
i[0] = i[0] + n | 0;
i[1] = i[1] + r | 0;
i[2] = i[2] + a | 0;
i[3] = i[3] + s | 0;
i[4] = i[4] + c | 0;
},
_doFinalize: function() {
var e = this._data, t = e.words, i = 8 * this._nDataBytes, n = 8 * e.sigBytes;
t[n >>> 5] |= 128 << 24 - n % 32;
t[14 + (n + 64 >>> 9 << 4)] = Math.floor(i / 4294967296);
t[15 + (n + 64 >>> 9 << 4)] = i;
e.sigBytes = 4 * t.length;
this._process();
return this._hash;
},
clone: function() {
var e = n.clone.call(this);
e._hash = this._hash.clone();
return e;
}
});
e.SHA1 = n._createHelper(a);
e.HmacSHA1 = n._createHmacHelper(a);
})();
(function(e) {
var t = c, i = t.lib, n = i.WordArray, r = i.Hasher, o = t.algo, a = [], s = [];
(function() {
function t(t) {
for (var i = e.sqrt(t), n = 2; n <= i; n++) if (!(t % n)) return !1;
return !0;
}
function i(e) {
return 4294967296 * (e - (0 | e)) | 0;
}
for (var n = 2, r = 0; r < 64; ) {
if (t(n)) {
r < 8 && (a[r] = i(e.pow(n, .5)));
s[r] = i(e.pow(n, 1 / 3));
r++;
}
n++;
}
})();
var l = [], h = o.SHA256 = r.extend({
_doReset: function() {
this._hash = new n.init(a.slice(0));
},
_doProcessBlock: function(e, t) {
for (var i = this._hash.words, n = i[0], r = i[1], o = i[2], a = i[3], c = i[4], h = i[5], u = i[6], f = i[7], d = 0; d < 64; d++) {
if (d < 16) l[d] = 0 | e[t + d]; else {
var p = l[d - 15], m = (p << 25 | p >>> 7) ^ (p << 14 | p >>> 18) ^ p >>> 3, g = l[d - 2], y = (g << 15 | g >>> 17) ^ (g << 13 | g >>> 19) ^ g >>> 10;
l[d] = m + l[d - 7] + y + l[d - 16];
}
var b = n & r ^ n & o ^ r & o, v = (n << 30 | n >>> 2) ^ (n << 19 | n >>> 13) ^ (n << 10 | n >>> 22), _ = f + ((c << 26 | c >>> 6) ^ (c << 21 | c >>> 11) ^ (c << 7 | c >>> 25)) + (c & h ^ ~c & u) + s[d] + l[d];
f = u;
u = h;
h = c;
c = a + _ | 0;
a = o;
o = r;
r = n;
n = _ + (v + b) | 0;
}
i[0] = i[0] + n | 0;
i[1] = i[1] + r | 0;
i[2] = i[2] + o | 0;
i[3] = i[3] + a | 0;
i[4] = i[4] + c | 0;
i[5] = i[5] + h | 0;
i[6] = i[6] + u | 0;
i[7] = i[7] + f | 0;
},
_doFinalize: function() {
var t = this._data, i = t.words, n = 8 * this._nDataBytes, r = 8 * t.sigBytes;
i[r >>> 5] |= 128 << 24 - r % 32;
i[14 + (r + 64 >>> 9 << 4)] = e.floor(n / 4294967296);
i[15 + (r + 64 >>> 9 << 4)] = n;
t.sigBytes = 4 * i.length;
this._process();
return this._hash;
},
clone: function() {
var e = r.clone.call(this);
e._hash = this._hash.clone();
return e;
}
});
t.SHA256 = r._createHelper(h);
t.HmacSHA256 = r._createHmacHelper(h);
})(Math);
(function() {
var e = c, t = e.lib.WordArray, i = e.algo, n = i.SHA256, r = i.SHA224 = n.extend({
_doReset: function() {
this._hash = new t.init([ 3238371032, 914150663, 812702999, 4144912697, 4290775857, 1750603025, 1694076839, 3204075428 ]);
},
_doFinalize: function() {
var e = n._doFinalize.call(this);
e.sigBytes -= 4;
return e;
}
});
e.SHA224 = n._createHelper(r);
e.HmacSHA224 = n._createHmacHelper(r);
})();
(function() {
var e = c, t = e.lib.Hasher, i = e.x64, n = i.Word, r = i.WordArray, o = e.algo;
function a() {
return n.create.apply(n, arguments);
}
var s = [ a(1116352408, 3609767458), a(1899447441, 602891725), a(3049323471, 3964484399), a(3921009573, 2173295548), a(961987163, 4081628472), a(1508970993, 3053834265), a(2453635748, 2937671579), a(2870763221, 3664609560), a(3624381080, 2734883394), a(310598401, 1164996542), a(607225278, 1323610764), a(1426881987, 3590304994), a(1925078388, 4068182383), a(2162078206, 991336113), a(2614888103, 633803317), a(3248222580, 3479774868), a(3835390401, 2666613458), a(4022224774, 944711139), a(264347078, 2341262773), a(604807628, 2007800933), a(770255983, 1495990901), a(1249150122, 1856431235), a(1555081692, 3175218132), a(1996064986, 2198950837), a(2554220882, 3999719339), a(2821834349, 766784016), a(2952996808, 2566594879), a(3210313671, 3203337956), a(3336571891, 1034457026), a(3584528711, 2466948901), a(113926993, 3758326383), a(338241895, 168717936), a(666307205, 1188179964), a(773529912, 1546045734), a(1294757372, 1522805485), a(1396182291, 2643833823), a(1695183700, 2343527390), a(1986661051, 1014477480), a(2177026350, 1206759142), a(2456956037, 344077627), a(2730485921, 1290863460), a(2820302411, 3158454273), a(3259730800, 3505952657), a(3345764771, 106217008), a(3516065817, 3606008344), a(3600352804, 1432725776), a(4094571909, 1467031594), a(275423344, 851169720), a(430227734, 3100823752), a(506948616, 1363258195), a(659060556, 3750685593), a(883997877, 3785050280), a(958139571, 3318307427), a(1322822218, 3812723403), a(1537002063, 2003034995), a(1747873779, 3602036899), a(1955562222, 1575990012), a(2024104815, 1125592928), a(2227730452, 2716904306), a(2361852424, 442776044), a(2428436474, 593698344), a(2756734187, 3733110249), a(3204031479, 2999351573), a(3329325298, 3815920427), a(3391569614, 3928383900), a(3515267271, 566280711), a(3940187606, 3454069534), a(4118630271, 4000239992), a(116418474, 1914138554), a(174292421, 2731055270), a(289380356, 3203993006), a(460393269, 320620315), a(685471733, 587496836), a(852142971, 1086792851), a(1017036298, 365543100), a(1126000580, 2618297676), a(1288033470, 3409855158), a(1501505948, 4234509866), a(1607167915, 987167468), a(1816402316, 1246189591) ], l = [];
(function() {
for (var e = 0; e < 80; e++) l[e] = a();
})();
var h = o.SHA512 = t.extend({
_doReset: function() {
this._hash = new r.init([ new n.init(1779033703, 4089235720), new n.init(3144134277, 2227873595), new n.init(1013904242, 4271175723), new n.init(2773480762, 1595750129), new n.init(1359893119, 2917565137), new n.init(2600822924, 725511199), new n.init(528734635, 4215389547), new n.init(1541459225, 327033209) ]);
},
_doProcessBlock: function(e, t) {
for (var i = this._hash.words, n = i[0], r = i[1], o = i[2], a = i[3], c = i[4], h = i[5], u = i[6], f = i[7], d = n.high, p = n.low, m = r.high, g = r.low, y = o.high, b = o.low, v = a.high, _ = a.low, w = c.high, C = c.low, S = h.high, A = h.low, M = u.high, B = u.low, x = f.high, k = f.low, T = d, D = p, E = m, L = g, P = y, R = b, I = v, N = _, O = w, U = C, j = S, F = A, G = M, V = B, z = x, H = k, W = 0; W < 80; W++) {
var q, X, K = l[W];
if (W < 16) {
X = K.high = 0 | e[t + 2 * W];
q = K.low = 0 | e[t + 2 * W + 1];
} else {
var J = l[W - 15], Y = J.high, Z = J.low, Q = (Y >>> 1 | Z << 31) ^ (Y >>> 8 | Z << 24) ^ Y >>> 7, $ = (Z >>> 1 | Y << 31) ^ (Z >>> 8 | Y << 24) ^ (Z >>> 7 | Y << 25), ee = l[W - 2], te = ee.high, ie = ee.low, ne = (te >>> 19 | ie << 13) ^ (te << 3 | ie >>> 29) ^ te >>> 6, re = (ie >>> 19 | te << 13) ^ (ie << 3 | te >>> 29) ^ (ie >>> 6 | te << 26), oe = l[W - 7], ae = oe.high, se = oe.low, ce = l[W - 16], le = ce.high, he = ce.low;
X = (X = (X = Q + ae + ((q = $ + se) >>> 0 < $ >>> 0 ? 1 : 0)) + ne + ((q += re) >>> 0 < re >>> 0 ? 1 : 0)) + le + ((q += he) >>> 0 < he >>> 0 ? 1 : 0);
K.high = X;
K.low = q;
}
var ue, fe = O & j ^ ~O & G, de = U & F ^ ~U & V, pe = T & E ^ T & P ^ E & P, me = D & L ^ D & R ^ L & R, ge = (T >>> 28 | D << 4) ^ (T << 30 | D >>> 2) ^ (T << 25 | D >>> 7), ye = (D >>> 28 | T << 4) ^ (D << 30 | T >>> 2) ^ (D << 25 | T >>> 7), be = (O >>> 14 | U << 18) ^ (O >>> 18 | U << 14) ^ (O << 23 | U >>> 9), ve = (U >>> 14 | O << 18) ^ (U >>> 18 | O << 14) ^ (U << 23 | O >>> 9), _e = s[W], we = _e.high, Ce = _e.low, Se = z + be + ((ue = H + ve) >>> 0 < H >>> 0 ? 1 : 0), Ae = ye + me;
z = G;
H = V;
G = j;
V = F;
j = O;
F = U;
O = I + (Se = (Se = (Se = Se + fe + ((ue += de) >>> 0 < de >>> 0 ? 1 : 0)) + we + ((ue += Ce) >>> 0 < Ce >>> 0 ? 1 : 0)) + X + ((ue += q) >>> 0 < q >>> 0 ? 1 : 0)) + ((U = N + ue | 0) >>> 0 < N >>> 0 ? 1 : 0) | 0;
I = P;
N = R;
P = E;
R = L;
E = T;
L = D;
T = Se + (ge + pe + (Ae >>> 0 < ye >>> 0 ? 1 : 0)) + ((D = ue + Ae | 0) >>> 0 < ue >>> 0 ? 1 : 0) | 0;
}
p = n.low = p + D;
n.high = d + T + (p >>> 0 < D >>> 0 ? 1 : 0);
g = r.low = g + L;
r.high = m + E + (g >>> 0 < L >>> 0 ? 1 : 0);
b = o.low = b + R;
o.high = y + P + (b >>> 0 < R >>> 0 ? 1 : 0);
_ = a.low = _ + N;
a.high = v + I + (_ >>> 0 < N >>> 0 ? 1 : 0);
C = c.low = C + U;
c.high = w + O + (C >>> 0 < U >>> 0 ? 1 : 0);
A = h.low = A + F;
h.high = S + j + (A >>> 0 < F >>> 0 ? 1 : 0);
B = u.low = B + V;
u.high = M + G + (B >>> 0 < V >>> 0 ? 1 : 0);
k = f.low = k + H;
f.high = x + z + (k >>> 0 < H >>> 0 ? 1 : 0);
},
_doFinalize: function() {
var e = this._data, t = e.words, i = 8 * this._nDataBytes, n = 8 * e.sigBytes;
t[n >>> 5] |= 128 << 24 - n % 32;
t[30 + (n + 128 >>> 10 << 5)] = Math.floor(i / 4294967296);
t[31 + (n + 128 >>> 10 << 5)] = i;
e.sigBytes = 4 * t.length;
this._process();
return this._hash.toX32();
},
clone: function() {
var e = t.clone.call(this);
e._hash = this._hash.clone();
return e;
},
blockSize: 32
});
e.SHA512 = t._createHelper(h);
e.HmacSHA512 = t._createHmacHelper(h);
})();
(function() {
var e = c, t = e.x64, i = t.Word, n = t.WordArray, r = e.algo, o = r.SHA512, a = r.SHA384 = o.extend({
_doReset: function() {
this._hash = new n.init([ new i.init(3418070365, 3238371032), new i.init(1654270250, 914150663), new i.init(2438529370, 812702999), new i.init(355462360, 4144912697), new i.init(1731405415, 4290775857), new i.init(2394180231, 1750603025), new i.init(3675008525, 1694076839), new i.init(1203062813, 3204075428) ]);
},
_doFinalize: function() {
var e = o._doFinalize.call(this);
e.sigBytes -= 16;
return e;
}
});
e.SHA384 = o._createHelper(a);
e.HmacSHA384 = o._createHmacHelper(a);
})();
(function(e) {
var t = c, i = t.lib, n = i.WordArray, r = i.Hasher, o = t.x64.Word, a = t.algo, s = [], l = [], h = [];
(function() {
for (var e = 1, t = 0, i = 0; i < 24; i++) {
s[e + 5 * t] = (i + 1) * (i + 2) / 2 % 64;
var n = (2 * e + 3 * t) % 5;
e = t % 5;
t = n;
}
for (e = 0; e < 5; e++) for (t = 0; t < 5; t++) l[e + 5 * t] = t + (2 * e + 3 * t) % 5 * 5;
for (var r = 1, a = 0; a < 24; a++) {
for (var c = 0, u = 0, f = 0; f < 7; f++) {
if (1 & r) {
var d = (1 << f) - 1;
d < 32 ? u ^= 1 << d : c ^= 1 << d - 32;
}
128 & r ? r = r << 1 ^ 113 : r <<= 1;
}
h[a] = o.create(c, u);
}
})();
var u = [];
(function() {
for (var e = 0; e < 25; e++) u[e] = o.create();
})();
var f = a.SHA3 = r.extend({
cfg: r.cfg.extend({
outputLength: 512
}),
_doReset: function() {
for (var e = this._state = [], t = 0; t < 25; t++) e[t] = new o.init();
this.blockSize = (1600 - 2 * this.cfg.outputLength) / 32;
},
_doProcessBlock: function(e, t) {
for (var i = this._state, n = this.blockSize / 2, r = 0; r < n; r++) {
var o = e[t + 2 * r], a = e[t + 2 * r + 1];
o = 16711935 & (o << 8 | o >>> 24) | 4278255360 & (o << 24 | o >>> 8);
a = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8);
(k = i[r]).high ^= a;
k.low ^= o;
}
for (var c = 0; c < 24; c++) {
for (var f = 0; f < 5; f++) {
for (var d = 0, p = 0, m = 0; m < 5; m++) {
d ^= (k = i[f + 5 * m]).high;
p ^= k.low;
}
var g = u[f];
g.high = d;
g.low = p;
}
for (f = 0; f < 5; f++) {
var y = u[(f + 4) % 5], b = u[(f + 1) % 5], v = b.high, _ = b.low;
for (d = y.high ^ (v << 1 | _ >>> 31), p = y.low ^ (_ << 1 | v >>> 31), m = 0; m < 5; m++) {
(k = i[f + 5 * m]).high ^= d;
k.low ^= p;
}
}
for (var w = 1; w < 25; w++) {
var C = (k = i[w]).high, S = k.low, A = s[w];
if (A < 32) {
d = C << A | S >>> 32 - A;
p = S << A | C >>> 32 - A;
} else {
d = S << A - 32 | C >>> 64 - A;
p = C << A - 32 | S >>> 64 - A;
}
var M = u[l[w]];
M.high = d;
M.low = p;
}
var B = u[0], x = i[0];
B.high = x.high;
B.low = x.low;
for (f = 0; f < 5; f++) for (m = 0; m < 5; m++) {
var k = i[w = f + 5 * m], T = u[w], D = u[(f + 1) % 5 + 5 * m], E = u[(f + 2) % 5 + 5 * m];
k.high = T.high ^ ~D.high & E.high;
k.low = T.low ^ ~D.low & E.low;
}
k = i[0];
var L = h[c];
k.high ^= L.high;
k.low ^= L.low;
}
},
_doFinalize: function() {
var t = this._data, i = t.words, r = (this._nDataBytes, 8 * t.sigBytes), o = 32 * this.blockSize;
i[r >>> 5] |= 1 << 24 - r % 32;
i[(e.ceil((r + 1) / o) * o >>> 5) - 1] |= 128;
t.sigBytes = 4 * i.length;
this._process();
for (var a = this._state, s = this.cfg.outputLength / 8, c = s / 8, l = [], h = 0; h < c; h++) {
var u = a[h], f = u.high, d = u.low;
f = 16711935 & (f << 8 | f >>> 24) | 4278255360 & (f << 24 | f >>> 8);
d = 16711935 & (d << 8 | d >>> 24) | 4278255360 & (d << 24 | d >>> 8);
l.push(d);
l.push(f);
}
return new n.init(l, s);
},
clone: function() {
for (var e = r.clone.call(this), t = e._state = this._state.slice(0), i = 0; i < 25; i++) t[i] = t[i].clone();
return e;
}
});
t.SHA3 = r._createHelper(f);
t.HmacSHA3 = r._createHmacHelper(f);
})(Math);
(function() {
var e = c, t = e.lib, i = t.WordArray, n = t.Hasher, r = e.algo, o = i.create([ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 7, 4, 13, 1, 10, 6, 15, 3, 12, 0, 9, 5, 2, 14, 11, 8, 3, 10, 14, 4, 9, 15, 8, 1, 2, 7, 0, 6, 13, 11, 5, 12, 1, 9, 11, 10, 0, 8, 12, 4, 13, 3, 7, 15, 14, 5, 6, 2, 4, 0, 5, 9, 7, 12, 2, 10, 14, 1, 3, 8, 11, 6, 15, 13 ]), a = i.create([ 5, 14, 7, 0, 9, 2, 11, 4, 13, 6, 15, 8, 1, 10, 3, 12, 6, 11, 3, 7, 0, 13, 5, 10, 14, 15, 8, 12, 4, 9, 1, 2, 15, 5, 1, 3, 7, 14, 6, 9, 11, 8, 12, 2, 10, 0, 4, 13, 8, 6, 4, 1, 3, 11, 15, 0, 5, 12, 2, 13, 9, 7, 10, 14, 12, 15, 10, 4, 1, 5, 8, 7, 6, 2, 13, 14, 0, 3, 9, 11 ]), s = i.create([ 11, 14, 15, 12, 5, 8, 7, 9, 11, 13, 14, 15, 6, 7, 9, 8, 7, 6, 8, 13, 11, 9, 7, 15, 7, 12, 15, 9, 11, 7, 13, 12, 11, 13, 6, 7, 14, 9, 13, 15, 14, 8, 13, 6, 5, 12, 7, 5, 11, 12, 14, 15, 14, 15, 9, 8, 9, 14, 5, 6, 8, 6, 5, 12, 9, 15, 5, 11, 6, 8, 13, 12, 5, 12, 13, 14, 11, 8, 5, 6 ]), l = i.create([ 8, 9, 9, 11, 13, 15, 15, 5, 7, 7, 8, 11, 14, 14, 12, 6, 9, 13, 15, 7, 12, 8, 9, 11, 7, 7, 12, 7, 6, 15, 13, 11, 9, 7, 15, 11, 8, 6, 6, 14, 12, 13, 5, 14, 13, 13, 7, 5, 15, 5, 8, 11, 14, 14, 6, 14, 6, 9, 12, 9, 12, 5, 15, 8, 8, 5, 12, 9, 12, 5, 14, 6, 8, 13, 6, 5, 15, 13, 11, 11 ]), h = i.create([ 0, 1518500249, 1859775393, 2400959708, 2840853838 ]), u = i.create([ 1352829926, 1548603684, 1836072691, 2053994217, 0 ]), f = r.RIPEMD160 = n.extend({
_doReset: function() {
this._hash = i.create([ 1732584193, 4023233417, 2562383102, 271733878, 3285377520 ]);
},
_doProcessBlock: function(e, t) {
for (var i = 0; i < 16; i++) {
var n = t + i, r = e[n];
e[n] = 16711935 & (r << 8 | r >>> 24) | 4278255360 & (r << 24 | r >>> 8);
}
var c, f, v, _, w, C, S, A, M, B, x, k = this._hash.words, T = h.words, D = u.words, E = o.words, L = a.words, P = s.words, R = l.words;
C = c = k[0];
S = f = k[1];
A = v = k[2];
M = _ = k[3];
B = w = k[4];
for (i = 0; i < 80; i += 1) {
x = c + e[t + E[i]] | 0;
x += i < 16 ? d(f, v, _) + T[0] : i < 32 ? p(f, v, _) + T[1] : i < 48 ? m(f, v, _) + T[2] : i < 64 ? g(f, v, _) + T[3] : y(f, v, _) + T[4];
x = (x = b(x |= 0, P[i])) + w | 0;
c = w;
w = _;
_ = b(v, 10);
v = f;
f = x;
x = C + e[t + L[i]] | 0;
x += i < 16 ? y(S, A, M) + D[0] : i < 32 ? g(S, A, M) + D[1] : i < 48 ? m(S, A, M) + D[2] : i < 64 ? p(S, A, M) + D[3] : d(S, A, M) + D[4];
x = (x = b(x |= 0, R[i])) + B | 0;
C = B;
B = M;
M = b(A, 10);
A = S;
S = x;
}
x = k[1] + v + M | 0;
k[1] = k[2] + _ + B | 0;
k[2] = k[3] + w + C | 0;
k[3] = k[4] + c + S | 0;
k[4] = k[0] + f + A | 0;
k[0] = x;
},
_doFinalize: function() {
var e = this._data, t = e.words, i = 8 * this._nDataBytes, n = 8 * e.sigBytes;
t[n >>> 5] |= 128 << 24 - n % 32;
t[14 + (n + 64 >>> 9 << 4)] = 16711935 & (i << 8 | i >>> 24) | 4278255360 & (i << 24 | i >>> 8);
e.sigBytes = 4 * (t.length + 1);
this._process();
for (var r = this._hash, o = r.words, a = 0; a < 5; a++) {
var s = o[a];
o[a] = 16711935 & (s << 8 | s >>> 24) | 4278255360 & (s << 24 | s >>> 8);
}
return r;
},
clone: function() {
var e = n.clone.call(this);
e._hash = this._hash.clone();
return e;
}
});
function d(e, t, i) {
return e ^ t ^ i;
}
function p(e, t, i) {
return e & t | ~e & i;
}
function m(e, t, i) {
return (e | ~t) ^ i;
}
function g(e, t, i) {
return e & i | t & ~i;
}
function y(e, t, i) {
return e ^ (t | ~i);
}
function b(e, t) {
return e << t | e >>> 32 - t;
}
e.RIPEMD160 = n._createHelper(f);
e.HmacRIPEMD160 = n._createHmacHelper(f);
})(Math);
(function() {
var e = c, t = e.lib.Base, i = e.enc.Utf8;
e.algo.HMAC = t.extend({
init: function(e, t) {
e = this._hasher = new e.init();
"string" == typeof t && (t = i.parse(t));
var n = e.blockSize, r = 4 * n;
t.sigBytes > r && (t = e.finalize(t));
t.clamp();
for (var o = this._oKey = t.clone(), a = this._iKey = t.clone(), s = o.words, c = a.words, l = 0; l < n; l++) {
s[l] ^= 1549556828;
c[l] ^= 909522486;
}
o.sigBytes = a.sigBytes = r;
this.reset();
},
reset: function() {
var e = this._hasher;
e.reset();
e.update(this._iKey);
},
update: function(e) {
this._hasher.update(e);
return this;
},
finalize: function(e) {
var t = this._hasher, i = t.finalize(e);
t.reset();
return t.finalize(this._oKey.clone().concat(i));
}
});
})();
(function() {
var e = c, t = e.lib, i = t.Base, n = t.WordArray, r = e.algo, o = r.SHA1, a = r.HMAC, s = r.PBKDF2 = i.extend({
cfg: i.extend({
keySize: 4,
hasher: o,
iterations: 1
}),
init: function(e) {
this.cfg = this.cfg.extend(e);
},
compute: function(e, t) {
for (var i = this.cfg, r = a.create(i.hasher, e), o = n.create(), s = n.create([ 1 ]), c = o.words, l = s.words, h = i.keySize, u = i.iterations; c.length < h; ) {
var f = r.update(t).finalize(s);
r.reset();
for (var d = f.words, p = d.length, m = f, g = 1; g < u; g++) {
m = r.finalize(m);
r.reset();
for (var y = m.words, b = 0; b < p; b++) d[b] ^= y[b];
}
o.concat(f);
l[0]++;
}
o.sigBytes = 4 * h;
return o;
}
});
e.PBKDF2 = function(e, t, i) {
return s.create(i).compute(e, t);
};
})();
(function() {
var e = c, t = e.lib, i = t.Base, n = t.WordArray, r = e.algo, o = r.MD5, a = r.EvpKDF = i.extend({
cfg: i.extend({
keySize: 4,
hasher: o,
iterations: 1
}),
init: function(e) {
this.cfg = this.cfg.extend(e);
},
compute: function(e, t) {
for (var i, r = this.cfg, o = r.hasher.create(), a = n.create(), s = a.words, c = r.keySize, l = r.iterations; s.length < c; ) {
i && o.update(i);
i = o.update(e).finalize(t);
o.reset();
for (var h = 1; h < l; h++) {
i = o.finalize(i);
o.reset();
}
a.concat(i);
}
a.sigBytes = 4 * c;
return a;
}
});
e.EvpKDF = function(e, t, i) {
return a.create(i).compute(e, t);
};
})();
c.lib.Cipher || function(e) {
var t = c, i = t.lib, n = i.Base, r = i.WordArray, o = i.BufferedBlockAlgorithm, a = t.enc, s = (a.Utf8, 
a.Base64), l = t.algo.EvpKDF, h = i.Cipher = o.extend({
cfg: n.extend(),
createEncryptor: function(e, t) {
return this.create(this._ENC_XFORM_MODE, e, t);
},
createDecryptor: function(e, t) {
return this.create(this._DEC_XFORM_MODE, e, t);
},
init: function(e, t, i) {
this.cfg = this.cfg.extend(i);
this._xformMode = e;
this._key = t;
this.reset();
},
reset: function() {
o.reset.call(this);
this._doReset();
},
process: function(e) {
this._append(e);
return this._process();
},
finalize: function(e) {
e && this._append(e);
return this._doFinalize();
},
keySize: 4,
ivSize: 4,
_ENC_XFORM_MODE: 1,
_DEC_XFORM_MODE: 2,
_createHelper: function() {
function e(e) {
return "string" == typeof e ? v : y;
}
return function(t) {
return {
encrypt: function(i, n, r) {
return e(n).encrypt(t, i, n, r);
},
decrypt: function(i, n, r) {
return e(n).decrypt(t, i, n, r);
}
};
};
}()
}), u = (i.StreamCipher = h.extend({
_doFinalize: function() {
return this._process(!0);
},
blockSize: 1
}), t.mode = {}), f = i.BlockCipherMode = n.extend({
createEncryptor: function(e, t) {
return this.Encryptor.create(e, t);
},
createDecryptor: function(e, t) {
return this.Decryptor.create(e, t);
},
init: function(e, t) {
this._cipher = e;
this._iv = t;
}
}), d = u.CBC = function() {
var t = f.extend();
t.Encryptor = t.extend({
processBlock: function(e, t) {
var n = this._cipher, r = n.blockSize;
i.call(this, e, t, r);
n.encryptBlock(e, t);
this._prevBlock = e.slice(t, t + r);
}
});
t.Decryptor = t.extend({
processBlock: function(e, t) {
var n = this._cipher, r = n.blockSize, o = e.slice(t, t + r);
n.decryptBlock(e, t);
i.call(this, e, t, r);
this._prevBlock = o;
}
});
function i(t, i, n) {
var r, o = this._iv;
if (o) {
r = o;
this._iv = e;
} else r = this._prevBlock;
for (var a = 0; a < n; a++) t[i + a] ^= r[a];
}
return t;
}(), p = (t.pad = {}).Pkcs7 = {
pad: function(e, t) {
for (var i = 4 * t, n = i - e.sigBytes % i, o = n << 24 | n << 16 | n << 8 | n, a = [], s = 0; s < n; s += 4) a.push(o);
var c = r.create(a, n);
e.concat(c);
},
unpad: function(e) {
var t = 255 & e.words[e.sigBytes - 1 >>> 2];
e.sigBytes -= t;
}
}, m = (i.BlockCipher = h.extend({
cfg: h.cfg.extend({
mode: d,
padding: p
}),
reset: function() {
var e;
h.reset.call(this);
var t = this.cfg, i = t.iv, n = t.mode;
if (this._xformMode == this._ENC_XFORM_MODE) e = n.createEncryptor; else {
e = n.createDecryptor;
this._minBufferSize = 1;
}
if (this._mode && this._mode.__creator == e) this._mode.init(this, i && i.words); else {
this._mode = e.call(n, this, i && i.words);
this._mode.__creator = e;
}
},
_doProcessBlock: function(e, t) {
this._mode.processBlock(e, t);
},
_doFinalize: function() {
var e, t = this.cfg.padding;
if (this._xformMode == this._ENC_XFORM_MODE) {
t.pad(this._data, this.blockSize);
e = this._process(!0);
} else {
e = this._process(!0);
t.unpad(e);
}
return e;
},
blockSize: 4
}), i.CipherParams = n.extend({
init: function(e) {
this.mixIn(e);
},
toString: function(e) {
return (e || this.formatter).stringify(this);
}
})), g = (t.format = {}).OpenSSL = {
stringify: function(e) {
var t = e.ciphertext, i = e.salt;
return (i ? r.create([ 1398893684, 1701076831 ]).concat(i).concat(t) : t).toString(s);
},
parse: function(e) {
var t, i = s.parse(e), n = i.words;
if (1398893684 == n[0] && 1701076831 == n[1]) {
t = r.create(n.slice(2, 4));
n.splice(0, 4);
i.sigBytes -= 16;
}
return m.create({
ciphertext: i,
salt: t
});
}
}, y = i.SerializableCipher = n.extend({
cfg: n.extend({
format: g
}),
encrypt: function(e, t, i, n) {
n = this.cfg.extend(n);
var r = e.createEncryptor(i, n), o = r.finalize(t), a = r.cfg;
return m.create({
ciphertext: o,
key: i,
iv: a.iv,
algorithm: e,
mode: a.mode,
padding: a.padding,
blockSize: e.blockSize,
formatter: n.format
});
},
decrypt: function(e, t, i, n) {
n = this.cfg.extend(n);
t = this._parse(t, n.format);
return e.createDecryptor(i, n).finalize(t.ciphertext);
},
_parse: function(e, t) {
return "string" == typeof e ? t.parse(e, this) : e;
}
}), b = (t.kdf = {}).OpenSSL = {
execute: function(e, t, i, n) {
n || (n = r.random(8));
var o = l.create({
keySize: t + i
}).compute(e, n), a = r.create(o.words.slice(t), 4 * i);
o.sigBytes = 4 * t;
return m.create({
key: o,
iv: a,
salt: n
});
}
}, v = i.PasswordBasedCipher = y.extend({
cfg: y.cfg.extend({
kdf: b
}),
encrypt: function(e, t, i, n) {
var r = (n = this.cfg.extend(n)).kdf.execute(i, e.keySize, e.ivSize);
n.iv = r.iv;
var o = y.encrypt.call(this, e, t, r.key, n);
o.mixIn(r);
return o;
},
decrypt: function(e, t, i, n) {
n = this.cfg.extend(n);
t = this._parse(t, n.format);
var r = n.kdf.execute(i, e.keySize, e.ivSize, t.salt);
n.iv = r.iv;
return y.decrypt.call(this, e, t, r.key, n);
}
});
}();
c.mode.CFB = function() {
var e = c.lib.BlockCipherMode.extend();
e.Encryptor = e.extend({
processBlock: function(e, i) {
var n = this._cipher, r = n.blockSize;
t.call(this, e, i, r, n);
this._prevBlock = e.slice(i, i + r);
}
});
e.Decryptor = e.extend({
processBlock: function(e, i) {
var n = this._cipher, r = n.blockSize, o = e.slice(i, i + r);
t.call(this, e, i, r, n);
this._prevBlock = o;
}
});
function t(e, t, i, n) {
var r, o = this._iv;
if (o) {
r = o.slice(0);
this._iv = void 0;
} else r = this._prevBlock;
n.encryptBlock(r, 0);
for (var a = 0; a < i; a++) e[t + a] ^= r[a];
}
return e;
}();
c.mode.CTR = function() {
var e = c.lib.BlockCipherMode.extend(), t = e.Encryptor = e.extend({
processBlock: function(e, t) {
var i = this._cipher, n = i.blockSize, r = this._iv, o = this._counter;
if (r) {
o = this._counter = r.slice(0);
this._iv = void 0;
}
var a = o.slice(0);
i.encryptBlock(a, 0);
o[n - 1] = o[n - 1] + 1 | 0;
for (var s = 0; s < n; s++) e[t + s] ^= a[s];
}
});
e.Decryptor = t;
return e;
}();
c.mode.CTRGladman = function() {
var e = c.lib.BlockCipherMode.extend();
function t(e) {
if (255 == (e >> 24 & 255)) {
var t = e >> 16 & 255, i = e >> 8 & 255, n = 255 & e;
if (255 === t) {
t = 0;
if (255 === i) {
i = 0;
255 === n ? n = 0 : ++n;
} else ++i;
} else ++t;
e = 0;
e += t << 16;
e += i << 8;
e += n;
} else e += 1 << 24;
return e;
}
function i(e) {
0 === (e[0] = t(e[0])) && (e[1] = t(e[1]));
return e;
}
var n = e.Encryptor = e.extend({
processBlock: function(e, t) {
var n = this._cipher, r = n.blockSize, o = this._iv, a = this._counter;
if (o) {
a = this._counter = o.slice(0);
this._iv = void 0;
}
i(a);
var s = a.slice(0);
n.encryptBlock(s, 0);
for (var c = 0; c < r; c++) e[t + c] ^= s[c];
}
});
e.Decryptor = n;
return e;
}();
c.mode.OFB = function() {
var e = c.lib.BlockCipherMode.extend(), t = e.Encryptor = e.extend({
processBlock: function(e, t) {
var i = this._cipher, n = i.blockSize, r = this._iv, o = this._keystream;
if (r) {
o = this._keystream = r.slice(0);
this._iv = void 0;
}
i.encryptBlock(o, 0);
for (var a = 0; a < n; a++) e[t + a] ^= o[a];
}
});
e.Decryptor = t;
return e;
}();
c.mode.ECB = function() {
var e = c.lib.BlockCipherMode.extend();
e.Encryptor = e.extend({
processBlock: function(e, t) {
this._cipher.encryptBlock(e, t);
}
});
e.Decryptor = e.extend({
processBlock: function(e, t) {
this._cipher.decryptBlock(e, t);
}
});
return e;
}();
c.pad.AnsiX923 = {
pad: function(e, t) {
var i = e.sigBytes, n = 4 * t, r = n - i % n, o = i + r - 1;
e.clamp();
e.words[o >>> 2] |= r << 24 - o % 4 * 8;
e.sigBytes += r;
},
unpad: function(e) {
var t = 255 & e.words[e.sigBytes - 1 >>> 2];
e.sigBytes -= t;
}
};
c.pad.Iso10126 = {
pad: function(e, t) {
var i = 4 * t, n = i - e.sigBytes % i;
e.concat(c.lib.WordArray.random(n - 1)).concat(c.lib.WordArray.create([ n << 24 ], 1));
},
unpad: function(e) {
var t = 255 & e.words[e.sigBytes - 1 >>> 2];
e.sigBytes -= t;
}
};
c.pad.Iso97971 = {
pad: function(e, t) {
e.concat(c.lib.WordArray.create([ 2147483648 ], 1));
c.pad.ZeroPadding.pad(e, t);
},
unpad: function(e) {
c.pad.ZeroPadding.unpad(e);
e.sigBytes--;
}
};
c.pad.ZeroPadding = {
pad: function(e, t) {
var i = 4 * t;
e.clamp();
e.sigBytes += i - (e.sigBytes % i || i);
},
unpad: function(e) {
var t = e.words, i = e.sigBytes - 1;
for (i = e.sigBytes - 1; i >= 0; i--) if (t[i >>> 2] >>> 24 - i % 4 * 8 & 255) {
e.sigBytes = i + 1;
break;
}
}
};
c.pad.NoPadding = {
pad: function() {},
unpad: function() {}
};
(function() {
var e = c, t = e.lib.CipherParams, i = e.enc.Hex;
e.format.Hex = {
stringify: function(e) {
return e.ciphertext.toString(i);
},
parse: function(e) {
var n = i.parse(e);
return t.create({
ciphertext: n
});
}
};
})();
(function() {
var e = c, t = e.lib.BlockCipher, i = e.algo, n = [], r = [], o = [], a = [], s = [], l = [], h = [], u = [], f = [], d = [];
(function() {
for (var e = [], t = 0; t < 256; t++) e[t] = t < 128 ? t << 1 : t << 1 ^ 283;
var i = 0, c = 0;
for (t = 0; t < 256; t++) {
var p = c ^ c << 1 ^ c << 2 ^ c << 3 ^ c << 4;
p = p >>> 8 ^ 255 & p ^ 99;
n[i] = p;
r[p] = i;
var m = e[i], g = e[m], y = e[g], b = 257 * e[p] ^ 16843008 * p;
o[i] = b << 24 | b >>> 8;
a[i] = b << 16 | b >>> 16;
s[i] = b << 8 | b >>> 24;
l[i] = b;
b = 16843009 * y ^ 65537 * g ^ 257 * m ^ 16843008 * i;
h[p] = b << 24 | b >>> 8;
u[p] = b << 16 | b >>> 16;
f[p] = b << 8 | b >>> 24;
d[p] = b;
if (i) {
i = m ^ e[e[e[y ^ m]]];
c ^= e[e[c]];
} else i = c = 1;
}
})();
var p = [ 0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54 ], m = i.AES = t.extend({
_doReset: function() {
if (!this._nRounds || this._keyPriorReset !== this._key) {
for (var e = this._keyPriorReset = this._key, t = e.words, i = e.sigBytes / 4, r = 4 * ((this._nRounds = i + 6) + 1), o = this._keySchedule = [], a = 0; a < r; a++) if (a < i) o[a] = t[a]; else {
l = o[a - 1];
if (a % i) i > 6 && a % i == 4 && (l = n[l >>> 24] << 24 | n[l >>> 16 & 255] << 16 | n[l >>> 8 & 255] << 8 | n[255 & l]); else {
l = n[(l = l << 8 | l >>> 24) >>> 24] << 24 | n[l >>> 16 & 255] << 16 | n[l >>> 8 & 255] << 8 | n[255 & l];
l ^= p[a / i | 0] << 24;
}
o[a] = o[a - i] ^ l;
}
for (var s = this._invKeySchedule = [], c = 0; c < r; c++) {
a = r - c;
if (c % 4) var l = o[a]; else l = o[a - 4];
s[c] = c < 4 || a <= 4 ? l : h[n[l >>> 24]] ^ u[n[l >>> 16 & 255]] ^ f[n[l >>> 8 & 255]] ^ d[n[255 & l]];
}
}
},
encryptBlock: function(e, t) {
this._doCryptBlock(e, t, this._keySchedule, o, a, s, l, n);
},
decryptBlock: function(e, t) {
var i = e[t + 1];
e[t + 1] = e[t + 3];
e[t + 3] = i;
this._doCryptBlock(e, t, this._invKeySchedule, h, u, f, d, r);
i = e[t + 1];
e[t + 1] = e[t + 3];
e[t + 3] = i;
},
_doCryptBlock: function(e, t, i, n, r, o, a, s) {
for (var c = this._nRounds, l = e[t] ^ i[0], h = e[t + 1] ^ i[1], u = e[t + 2] ^ i[2], f = e[t + 3] ^ i[3], d = 4, p = 1; p < c; p++) {
var m = n[l >>> 24] ^ r[h >>> 16 & 255] ^ o[u >>> 8 & 255] ^ a[255 & f] ^ i[d++], g = n[h >>> 24] ^ r[u >>> 16 & 255] ^ o[f >>> 8 & 255] ^ a[255 & l] ^ i[d++], y = n[u >>> 24] ^ r[f >>> 16 & 255] ^ o[l >>> 8 & 255] ^ a[255 & h] ^ i[d++], b = n[f >>> 24] ^ r[l >>> 16 & 255] ^ o[h >>> 8 & 255] ^ a[255 & u] ^ i[d++];
l = m;
h = g;
u = y;
f = b;
}
m = (s[l >>> 24] << 24 | s[h >>> 16 & 255] << 16 | s[u >>> 8 & 255] << 8 | s[255 & f]) ^ i[d++], 
g = (s[h >>> 24] << 24 | s[u >>> 16 & 255] << 16 | s[f >>> 8 & 255] << 8 | s[255 & l]) ^ i[d++], 
y = (s[u >>> 24] << 24 | s[f >>> 16 & 255] << 16 | s[l >>> 8 & 255] << 8 | s[255 & h]) ^ i[d++], 
b = (s[f >>> 24] << 24 | s[l >>> 16 & 255] << 16 | s[h >>> 8 & 255] << 8 | s[255 & u]) ^ i[d++];
e[t] = m;
e[t + 1] = g;
e[t + 2] = y;
e[t + 3] = b;
},
keySize: 8
});
e.AES = t._createHelper(m);
})();
(function() {
var e = c, t = e.lib, i = t.WordArray, n = t.BlockCipher, r = e.algo, o = [ 57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4 ], a = [ 14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32 ], s = [ 1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28 ], l = [ {
0: 8421888,
268435456: 32768,
536870912: 8421378,
805306368: 2,
1073741824: 512,
1342177280: 8421890,
1610612736: 8389122,
1879048192: 8388608,
2147483648: 514,
2415919104: 8389120,
2684354560: 33280,
2952790016: 8421376,
3221225472: 32770,
3489660928: 8388610,
3758096384: 0,
4026531840: 33282,
134217728: 0,
402653184: 8421890,
671088640: 33282,
939524096: 32768,
1207959552: 8421888,
1476395008: 512,
1744830464: 8421378,
2013265920: 2,
2281701376: 8389120,
2550136832: 33280,
2818572288: 8421376,
3087007744: 8389122,
3355443200: 8388610,
3623878656: 32770,
3892314112: 514,
4160749568: 8388608,
1: 32768,
268435457: 2,
536870913: 8421888,
805306369: 8388608,
1073741825: 8421378,
1342177281: 33280,
1610612737: 512,
1879048193: 8389122,
2147483649: 8421890,
2415919105: 8421376,
2684354561: 8388610,
2952790017: 33282,
3221225473: 514,
3489660929: 8389120,
3758096385: 32770,
4026531841: 0,
134217729: 8421890,
402653185: 8421376,
671088641: 8388608,
939524097: 512,
1207959553: 32768,
1476395009: 8388610,
1744830465: 2,
2013265921: 33282,
2281701377: 32770,
2550136833: 8389122,
2818572289: 514,
3087007745: 8421888,
3355443201: 8389120,
3623878657: 0,
3892314113: 33280,
4160749569: 8421378
}, {
0: 1074282512,
16777216: 16384,
33554432: 524288,
50331648: 1074266128,
67108864: 1073741840,
83886080: 1074282496,
100663296: 1073758208,
117440512: 16,
134217728: 540672,
150994944: 1073758224,
167772160: 1073741824,
184549376: 540688,
201326592: 524304,
218103808: 0,
234881024: 16400,
251658240: 1074266112,
8388608: 1073758208,
25165824: 540688,
41943040: 16,
58720256: 1073758224,
75497472: 1074282512,
92274688: 1073741824,
109051904: 524288,
125829120: 1074266128,
142606336: 524304,
159383552: 0,
176160768: 16384,
192937984: 1074266112,
209715200: 1073741840,
226492416: 540672,
243269632: 1074282496,
260046848: 16400,
268435456: 0,
285212672: 1074266128,
301989888: 1073758224,
318767104: 1074282496,
335544320: 1074266112,
352321536: 16,
369098752: 540688,
385875968: 16384,
402653184: 16400,
419430400: 524288,
436207616: 524304,
452984832: 1073741840,
469762048: 540672,
486539264: 1073758208,
503316480: 1073741824,
520093696: 1074282512,
276824064: 540688,
293601280: 524288,
310378496: 1074266112,
327155712: 16384,
343932928: 1073758208,
360710144: 1074282512,
377487360: 16,
394264576: 1073741824,
411041792: 1074282496,
427819008: 1073741840,
444596224: 1073758224,
461373440: 524304,
478150656: 0,
494927872: 16400,
511705088: 1074266128,
528482304: 540672
}, {
0: 260,
1048576: 0,
2097152: 67109120,
3145728: 65796,
4194304: 65540,
5242880: 67108868,
6291456: 67174660,
7340032: 67174400,
8388608: 67108864,
9437184: 67174656,
10485760: 65792,
11534336: 67174404,
12582912: 67109124,
13631488: 65536,
14680064: 4,
15728640: 256,
524288: 67174656,
1572864: 67174404,
2621440: 0,
3670016: 67109120,
4718592: 67108868,
5767168: 65536,
6815744: 65540,
7864320: 260,
8912896: 4,
9961472: 256,
11010048: 67174400,
12058624: 65796,
13107200: 65792,
14155776: 67109124,
15204352: 67174660,
16252928: 67108864,
16777216: 67174656,
17825792: 65540,
18874368: 65536,
19922944: 67109120,
20971520: 256,
22020096: 67174660,
23068672: 67108868,
24117248: 0,
25165824: 67109124,
26214400: 67108864,
27262976: 4,
28311552: 65792,
29360128: 67174400,
30408704: 260,
31457280: 65796,
32505856: 67174404,
17301504: 67108864,
18350080: 260,
19398656: 67174656,
20447232: 0,
21495808: 65540,
22544384: 67109120,
23592960: 256,
24641536: 67174404,
25690112: 65536,
26738688: 67174660,
27787264: 65796,
28835840: 67108868,
29884416: 67109124,
30932992: 67174400,
31981568: 4,
33030144: 65792
}, {
0: 2151682048,
65536: 2147487808,
131072: 4198464,
196608: 2151677952,
262144: 0,
327680: 4198400,
393216: 2147483712,
458752: 4194368,
524288: 2147483648,
589824: 4194304,
655360: 64,
720896: 2147487744,
786432: 2151678016,
851968: 4160,
917504: 4096,
983040: 2151682112,
32768: 2147487808,
98304: 64,
163840: 2151678016,
229376: 2147487744,
294912: 4198400,
360448: 2151682112,
425984: 0,
491520: 2151677952,
557056: 4096,
622592: 2151682048,
688128: 4194304,
753664: 4160,
819200: 2147483648,
884736: 4194368,
950272: 4198464,
1015808: 2147483712,
1048576: 4194368,
1114112: 4198400,
1179648: 2147483712,
1245184: 0,
1310720: 4160,
1376256: 2151678016,
1441792: 2151682048,
1507328: 2147487808,
1572864: 2151682112,
1638400: 2147483648,
1703936: 2151677952,
1769472: 4198464,
1835008: 2147487744,
1900544: 4194304,
1966080: 64,
2031616: 4096,
1081344: 2151677952,
1146880: 2151682112,
1212416: 0,
1277952: 4198400,
1343488: 4194368,
1409024: 2147483648,
1474560: 2147487808,
1540096: 64,
1605632: 2147483712,
1671168: 4096,
1736704: 2147487744,
1802240: 2151678016,
1867776: 4160,
1933312: 2151682048,
1998848: 4194304,
2064384: 4198464
}, {
0: 128,
4096: 17039360,
8192: 262144,
12288: 536870912,
16384: 537133184,
20480: 16777344,
24576: 553648256,
28672: 262272,
32768: 16777216,
36864: 537133056,
40960: 536871040,
45056: 553910400,
49152: 553910272,
53248: 0,
57344: 17039488,
61440: 553648128,
2048: 17039488,
6144: 553648256,
10240: 128,
14336: 17039360,
18432: 262144,
22528: 537133184,
26624: 553910272,
30720: 536870912,
34816: 537133056,
38912: 0,
43008: 553910400,
47104: 16777344,
51200: 536871040,
55296: 553648128,
59392: 16777216,
63488: 262272,
65536: 262144,
69632: 128,
73728: 536870912,
77824: 553648256,
81920: 16777344,
86016: 553910272,
90112: 537133184,
94208: 16777216,
98304: 553910400,
102400: 553648128,
106496: 17039360,
110592: 537133056,
114688: 262272,
118784: 536871040,
122880: 0,
126976: 17039488,
67584: 553648256,
71680: 16777216,
75776: 17039360,
79872: 537133184,
83968: 536870912,
88064: 17039488,
92160: 128,
96256: 553910272,
100352: 262272,
104448: 553910400,
108544: 0,
112640: 553648128,
116736: 16777344,
120832: 262144,
124928: 537133056,
129024: 536871040
}, {
0: 268435464,
256: 8192,
512: 270532608,
768: 270540808,
1024: 268443648,
1280: 2097152,
1536: 2097160,
1792: 268435456,
2048: 0,
2304: 268443656,
2560: 2105344,
2816: 8,
3072: 270532616,
3328: 2105352,
3584: 8200,
3840: 270540800,
128: 270532608,
384: 270540808,
640: 8,
896: 2097152,
1152: 2105352,
1408: 268435464,
1664: 268443648,
1920: 8200,
2176: 2097160,
2432: 8192,
2688: 268443656,
2944: 270532616,
3200: 0,
3456: 270540800,
3712: 2105344,
3968: 268435456,
4096: 268443648,
4352: 270532616,
4608: 270540808,
4864: 8200,
5120: 2097152,
5376: 268435456,
5632: 268435464,
5888: 2105344,
6144: 2105352,
6400: 0,
6656: 8,
6912: 270532608,
7168: 8192,
7424: 268443656,
7680: 270540800,
7936: 2097160,
4224: 8,
4480: 2105344,
4736: 2097152,
4992: 268435464,
5248: 268443648,
5504: 8200,
5760: 270540808,
6016: 270532608,
6272: 270540800,
6528: 270532616,
6784: 8192,
7040: 2105352,
7296: 2097160,
7552: 0,
7808: 268435456,
8064: 268443656
}, {
0: 1048576,
16: 33555457,
32: 1024,
48: 1049601,
64: 34604033,
80: 0,
96: 1,
112: 34603009,
128: 33555456,
144: 1048577,
160: 33554433,
176: 34604032,
192: 34603008,
208: 1025,
224: 1049600,
240: 33554432,
8: 34603009,
24: 0,
40: 33555457,
56: 34604032,
72: 1048576,
88: 33554433,
104: 33554432,
120: 1025,
136: 1049601,
152: 33555456,
168: 34603008,
184: 1048577,
200: 1024,
216: 34604033,
232: 1,
248: 1049600,
256: 33554432,
272: 1048576,
288: 33555457,
304: 34603009,
320: 1048577,
336: 33555456,
352: 34604032,
368: 1049601,
384: 1025,
400: 34604033,
416: 1049600,
432: 1,
448: 0,
464: 34603008,
480: 33554433,
496: 1024,
264: 1049600,
280: 33555457,
296: 34603009,
312: 1,
328: 33554432,
344: 1048576,
360: 1025,
376: 34604032,
392: 33554433,
408: 34603008,
424: 0,
440: 34604033,
456: 1049601,
472: 1024,
488: 33555456,
504: 1048577
}, {
0: 134219808,
1: 131072,
2: 134217728,
3: 32,
4: 131104,
5: 134350880,
6: 134350848,
7: 2048,
8: 134348800,
9: 134219776,
10: 133120,
11: 134348832,
12: 2080,
13: 0,
14: 134217760,
15: 133152,
2147483648: 2048,
2147483649: 134350880,
2147483650: 134219808,
2147483651: 134217728,
2147483652: 134348800,
2147483653: 133120,
2147483654: 133152,
2147483655: 32,
2147483656: 134217760,
2147483657: 2080,
2147483658: 131104,
2147483659: 134350848,
2147483660: 0,
2147483661: 134348832,
2147483662: 134219776,
2147483663: 131072,
16: 133152,
17: 134350848,
18: 32,
19: 2048,
20: 134219776,
21: 134217760,
22: 134348832,
23: 131072,
24: 0,
25: 131104,
26: 134348800,
27: 134219808,
28: 134350880,
29: 133120,
30: 2080,
31: 134217728,
2147483664: 131072,
2147483665: 2048,
2147483666: 134348832,
2147483667: 133152,
2147483668: 32,
2147483669: 134348800,
2147483670: 134217728,
2147483671: 134219808,
2147483672: 134350880,
2147483673: 134217760,
2147483674: 134219776,
2147483675: 0,
2147483676: 133120,
2147483677: 2080,
2147483678: 131104,
2147483679: 134350848
} ], h = [ 4160749569, 528482304, 33030144, 2064384, 129024, 8064, 504, 2147483679 ], u = r.DES = n.extend({
_doReset: function() {
for (var e = this._key.words, t = [], i = 0; i < 56; i++) {
var n = o[i] - 1;
t[i] = e[n >>> 5] >>> 31 - n % 32 & 1;
}
for (var r = this._subKeys = [], c = 0; c < 16; c++) {
var l = r[c] = [], h = s[c];
for (i = 0; i < 24; i++) {
l[i / 6 | 0] |= t[(a[i] - 1 + h) % 28] << 31 - i % 6;
l[4 + (i / 6 | 0)] |= t[28 + (a[i + 24] - 1 + h) % 28] << 31 - i % 6;
}
l[0] = l[0] << 1 | l[0] >>> 31;
for (i = 1; i < 7; i++) l[i] = l[i] >>> 4 * (i - 1) + 3;
l[7] = l[7] << 5 | l[7] >>> 27;
}
var u = this._invSubKeys = [];
for (i = 0; i < 16; i++) u[i] = r[15 - i];
},
encryptBlock: function(e, t) {
this._doCryptBlock(e, t, this._subKeys);
},
decryptBlock: function(e, t) {
this._doCryptBlock(e, t, this._invSubKeys);
},
_doCryptBlock: function(e, t, i) {
this._lBlock = e[t];
this._rBlock = e[t + 1];
f.call(this, 4, 252645135);
f.call(this, 16, 65535);
d.call(this, 2, 858993459);
d.call(this, 8, 16711935);
f.call(this, 1, 1431655765);
for (var n = 0; n < 16; n++) {
for (var r = i[n], o = this._lBlock, a = this._rBlock, s = 0, c = 0; c < 8; c++) s |= l[c][((a ^ r[c]) & h[c]) >>> 0];
this._lBlock = a;
this._rBlock = o ^ s;
}
var u = this._lBlock;
this._lBlock = this._rBlock;
this._rBlock = u;
f.call(this, 1, 1431655765);
d.call(this, 8, 16711935);
d.call(this, 2, 858993459);
f.call(this, 16, 65535);
f.call(this, 4, 252645135);
e[t] = this._lBlock;
e[t + 1] = this._rBlock;
},
keySize: 2,
ivSize: 2,
blockSize: 2
});
function f(e, t) {
var i = (this._lBlock >>> e ^ this._rBlock) & t;
this._rBlock ^= i;
this._lBlock ^= i << e;
}
function d(e, t) {
var i = (this._rBlock >>> e ^ this._lBlock) & t;
this._lBlock ^= i;
this._rBlock ^= i << e;
}
e.DES = n._createHelper(u);
var p = r.TripleDES = n.extend({
_doReset: function() {
var e = this._key.words;
if (2 !== e.length && 4 !== e.length && e.length < 6) throw new Error("Invalid key length - 3DES requires the key length to be 64, 128, 192 or >192.");
var t = e.slice(0, 2), n = e.length < 4 ? e.slice(0, 2) : e.slice(2, 4), r = e.length < 6 ? e.slice(0, 2) : e.slice(4, 6);
this._des1 = u.createEncryptor(i.create(t));
this._des2 = u.createEncryptor(i.create(n));
this._des3 = u.createEncryptor(i.create(r));
},
encryptBlock: function(e, t) {
this._des1.encryptBlock(e, t);
this._des2.decryptBlock(e, t);
this._des3.encryptBlock(e, t);
},
decryptBlock: function(e, t) {
this._des3.decryptBlock(e, t);
this._des2.encryptBlock(e, t);
this._des1.decryptBlock(e, t);
},
keySize: 6,
ivSize: 2,
blockSize: 2
});
e.TripleDES = n._createHelper(p);
})();
(function() {
var e = c, t = e.lib.StreamCipher, i = e.algo, n = i.RC4 = t.extend({
_doReset: function() {
for (var e = this._key, t = e.words, i = e.sigBytes, n = this._S = [], r = 0; r < 256; r++) n[r] = r;
r = 0;
for (var o = 0; r < 256; r++) {
var a = r % i, s = t[a >>> 2] >>> 24 - a % 4 * 8 & 255;
o = (o + n[r] + s) % 256;
var c = n[r];
n[r] = n[o];
n[o] = c;
}
this._i = this._j = 0;
},
_doProcessBlock: function(e, t) {
e[t] ^= r.call(this);
},
keySize: 8,
ivSize: 0
});
function r() {
for (var e = this._S, t = this._i, i = this._j, n = 0, r = 0; r < 4; r++) {
i = (i + e[t = (t + 1) % 256]) % 256;
var o = e[t];
e[t] = e[i];
e[i] = o;
n |= e[(e[t] + e[i]) % 256] << 24 - 8 * r;
}
this._i = t;
this._j = i;
return n;
}
e.RC4 = t._createHelper(n);
var o = i.RC4Drop = n.extend({
cfg: n.cfg.extend({
drop: 192
}),
_doReset: function() {
n._doReset.call(this);
for (var e = this.cfg.drop; e > 0; e--) r.call(this);
}
});
e.RC4Drop = t._createHelper(o);
})();
(function() {
var e = c, t = e.lib.StreamCipher, i = e.algo, n = [], r = [], o = [], a = i.Rabbit = t.extend({
_doReset: function() {
for (var e = this._key.words, t = this.cfg.iv, i = 0; i < 4; i++) e[i] = 16711935 & (e[i] << 8 | e[i] >>> 24) | 4278255360 & (e[i] << 24 | e[i] >>> 8);
var n = this._X = [ e[0], e[3] << 16 | e[2] >>> 16, e[1], e[0] << 16 | e[3] >>> 16, e[2], e[1] << 16 | e[0] >>> 16, e[3], e[2] << 16 | e[1] >>> 16 ], r = this._C = [ e[2] << 16 | e[2] >>> 16, 4294901760 & e[0] | 65535 & e[1], e[3] << 16 | e[3] >>> 16, 4294901760 & e[1] | 65535 & e[2], e[0] << 16 | e[0] >>> 16, 4294901760 & e[2] | 65535 & e[3], e[1] << 16 | e[1] >>> 16, 4294901760 & e[3] | 65535 & e[0] ];
this._b = 0;
for (i = 0; i < 4; i++) s.call(this);
for (i = 0; i < 8; i++) r[i] ^= n[i + 4 & 7];
if (t) {
var o = t.words, a = o[0], c = o[1], l = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8), h = 16711935 & (c << 8 | c >>> 24) | 4278255360 & (c << 24 | c >>> 8), u = l >>> 16 | 4294901760 & h, f = h << 16 | 65535 & l;
r[0] ^= l;
r[1] ^= u;
r[2] ^= h;
r[3] ^= f;
r[4] ^= l;
r[5] ^= u;
r[6] ^= h;
r[7] ^= f;
for (i = 0; i < 4; i++) s.call(this);
}
},
_doProcessBlock: function(e, t) {
var i = this._X;
s.call(this);
n[0] = i[0] ^ i[5] >>> 16 ^ i[3] << 16;
n[1] = i[2] ^ i[7] >>> 16 ^ i[5] << 16;
n[2] = i[4] ^ i[1] >>> 16 ^ i[7] << 16;
n[3] = i[6] ^ i[3] >>> 16 ^ i[1] << 16;
for (var r = 0; r < 4; r++) {
n[r] = 16711935 & (n[r] << 8 | n[r] >>> 24) | 4278255360 & (n[r] << 24 | n[r] >>> 8);
e[t + r] ^= n[r];
}
},
blockSize: 4,
ivSize: 2
});
function s() {
for (var e = this._X, t = this._C, i = 0; i < 8; i++) r[i] = t[i];
t[0] = t[0] + 1295307597 + this._b | 0;
t[1] = t[1] + 3545052371 + (t[0] >>> 0 < r[0] >>> 0 ? 1 : 0) | 0;
t[2] = t[2] + 886263092 + (t[1] >>> 0 < r[1] >>> 0 ? 1 : 0) | 0;
t[3] = t[3] + 1295307597 + (t[2] >>> 0 < r[2] >>> 0 ? 1 : 0) | 0;
t[4] = t[4] + 3545052371 + (t[3] >>> 0 < r[3] >>> 0 ? 1 : 0) | 0;
t[5] = t[5] + 886263092 + (t[4] >>> 0 < r[4] >>> 0 ? 1 : 0) | 0;
t[6] = t[6] + 1295307597 + (t[5] >>> 0 < r[5] >>> 0 ? 1 : 0) | 0;
t[7] = t[7] + 3545052371 + (t[6] >>> 0 < r[6] >>> 0 ? 1 : 0) | 0;
this._b = t[7] >>> 0 < r[7] >>> 0 ? 1 : 0;
for (i = 0; i < 8; i++) {
var n = e[i] + t[i], a = 65535 & n, s = n >>> 16, c = ((a * a >>> 17) + a * s >>> 15) + s * s, l = ((4294901760 & n) * n | 0) + ((65535 & n) * n | 0);
o[i] = c ^ l;
}
e[0] = o[0] + (o[7] << 16 | o[7] >>> 16) + (o[6] << 16 | o[6] >>> 16) | 0;
e[1] = o[1] + (o[0] << 8 | o[0] >>> 24) + o[7] | 0;
e[2] = o[2] + (o[1] << 16 | o[1] >>> 16) + (o[0] << 16 | o[0] >>> 16) | 0;
e[3] = o[3] + (o[2] << 8 | o[2] >>> 24) + o[1] | 0;
e[4] = o[4] + (o[3] << 16 | o[3] >>> 16) + (o[2] << 16 | o[2] >>> 16) | 0;
e[5] = o[5] + (o[4] << 8 | o[4] >>> 24) + o[3] | 0;
e[6] = o[6] + (o[5] << 16 | o[5] >>> 16) + (o[4] << 16 | o[4] >>> 16) | 0;
e[7] = o[7] + (o[6] << 8 | o[6] >>> 24) + o[5] | 0;
}
e.Rabbit = t._createHelper(a);
})();
(function() {
var e = c, t = e.lib.StreamCipher, i = e.algo, n = [], r = [], o = [], a = i.RabbitLegacy = t.extend({
_doReset: function() {
var e = this._key.words, t = this.cfg.iv, i = this._X = [ e[0], e[3] << 16 | e[2] >>> 16, e[1], e[0] << 16 | e[3] >>> 16, e[2], e[1] << 16 | e[0] >>> 16, e[3], e[2] << 16 | e[1] >>> 16 ], n = this._C = [ e[2] << 16 | e[2] >>> 16, 4294901760 & e[0] | 65535 & e[1], e[3] << 16 | e[3] >>> 16, 4294901760 & e[1] | 65535 & e[2], e[0] << 16 | e[0] >>> 16, 4294901760 & e[2] | 65535 & e[3], e[1] << 16 | e[1] >>> 16, 4294901760 & e[3] | 65535 & e[0] ];
this._b = 0;
for (var r = 0; r < 4; r++) s.call(this);
for (r = 0; r < 8; r++) n[r] ^= i[r + 4 & 7];
if (t) {
var o = t.words, a = o[0], c = o[1], l = 16711935 & (a << 8 | a >>> 24) | 4278255360 & (a << 24 | a >>> 8), h = 16711935 & (c << 8 | c >>> 24) | 4278255360 & (c << 24 | c >>> 8), u = l >>> 16 | 4294901760 & h, f = h << 16 | 65535 & l;
n[0] ^= l;
n[1] ^= u;
n[2] ^= h;
n[3] ^= f;
n[4] ^= l;
n[5] ^= u;
n[6] ^= h;
n[7] ^= f;
for (r = 0; r < 4; r++) s.call(this);
}
},
_doProcessBlock: function(e, t) {
var i = this._X;
s.call(this);
n[0] = i[0] ^ i[5] >>> 16 ^ i[3] << 16;
n[1] = i[2] ^ i[7] >>> 16 ^ i[5] << 16;
n[2] = i[4] ^ i[1] >>> 16 ^ i[7] << 16;
n[3] = i[6] ^ i[3] >>> 16 ^ i[1] << 16;
for (var r = 0; r < 4; r++) {
n[r] = 16711935 & (n[r] << 8 | n[r] >>> 24) | 4278255360 & (n[r] << 24 | n[r] >>> 8);
e[t + r] ^= n[r];
}
},
blockSize: 4,
ivSize: 2
});
function s() {
for (var e = this._X, t = this._C, i = 0; i < 8; i++) r[i] = t[i];
t[0] = t[0] + 1295307597 + this._b | 0;
t[1] = t[1] + 3545052371 + (t[0] >>> 0 < r[0] >>> 0 ? 1 : 0) | 0;
t[2] = t[2] + 886263092 + (t[1] >>> 0 < r[1] >>> 0 ? 1 : 0) | 0;
t[3] = t[3] + 1295307597 + (t[2] >>> 0 < r[2] >>> 0 ? 1 : 0) | 0;
t[4] = t[4] + 3545052371 + (t[3] >>> 0 < r[3] >>> 0 ? 1 : 0) | 0;
t[5] = t[5] + 886263092 + (t[4] >>> 0 < r[4] >>> 0 ? 1 : 0) | 0;
t[6] = t[6] + 1295307597 + (t[5] >>> 0 < r[5] >>> 0 ? 1 : 0) | 0;
t[7] = t[7] + 3545052371 + (t[6] >>> 0 < r[6] >>> 0 ? 1 : 0) | 0;
this._b = t[7] >>> 0 < r[7] >>> 0 ? 1 : 0;
for (i = 0; i < 8; i++) {
var n = e[i] + t[i], a = 65535 & n, s = n >>> 16, c = ((a * a >>> 17) + a * s >>> 15) + s * s, l = ((4294901760 & n) * n | 0) + ((65535 & n) * n | 0);
o[i] = c ^ l;
}
e[0] = o[0] + (o[7] << 16 | o[7] >>> 16) + (o[6] << 16 | o[6] >>> 16) | 0;
e[1] = o[1] + (o[0] << 8 | o[0] >>> 24) + o[7] | 0;
e[2] = o[2] + (o[1] << 16 | o[1] >>> 16) + (o[0] << 16 | o[0] >>> 16) | 0;
e[3] = o[3] + (o[2] << 8 | o[2] >>> 24) + o[1] | 0;
e[4] = o[4] + (o[3] << 16 | o[3] >>> 16) + (o[2] << 16 | o[2] >>> 16) | 0;
e[5] = o[5] + (o[4] << 8 | o[4] >>> 24) + o[3] | 0;
e[6] = o[6] + (o[5] << 16 | o[5] >>> 16) + (o[4] << 16 | o[4] >>> 16) | 0;
e[7] = o[7] + (o[6] << 8 | o[6] >>> 24) + o[5] | 0;
}
e.RabbitLegacy = t._createHelper(a);
})();
t.exports = c;
return c;
}, "object" == typeof i ? t.exports = i = r() : "function" == typeof define && define.amd ? define([], r) : (void 0).CryptoJS = r();
var r;
cc._RF.pop();
}).call(this, "undefined" != typeof global ? global : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {});
}, {
crypto: 75
} ],
gameFit: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "35aa5iiFztCwoL7rWrsr1oa", "gameFit");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = (a.property, function(e) {
r(t, e);
function t() {
return null !== e && e.apply(this, arguments) || this;
}
t.prototype.onLoad = function() {
var e = cc.view.getCanvasSize().width / this.node.width, t = cc.view.getCanvasSize().height / this.node.height, i = Math.min(cc.view.getCanvasSize().width / this.node.width, cc.view.getCanvasSize().height / this.node.height);
if (e < t) {
var n = this.node.width * i, r = this.node.height * i;
this.node.width = this.node.width * (cc.view.getCanvasSize().width / n);
this.node.height = this.node.height * (cc.view.getCanvasSize().height / r);
} else {
r = this.node.height * t;
this.node.width = this.node.width * (cc.view.getCanvasSize().height / r);
this.node.height = this.node.height * (cc.view.getCanvasSize().height / r);
}
};
return o([ s ], t);
}(cc.Component));
i.default = c;
cc._RF.pop();
}, {} ],
i18nLabel: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "41ebaVoPpRA4Kp67XEdHfZn", "i18nLabel");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.i18nLabel = void 0;
var a = e("./i18nMgr"), s = cc._decorator, c = s.ccclass, l = s.property, h = (s.executeInEditMode, 
s.disallowMultiple, s.requireComponent, s.menu), u = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.i18n_string = "";
return t;
}
t.prototype.onLoad = function() {
a.i18nMgr._addOrDelLabel(this, !0);
this._resetValue();
};
Object.defineProperty(t.prototype, "string", {
get: function() {
return this.i18n_string;
},
set: function(e) {
this.i18n_string = e;
this.setEndValue();
},
enumerable: !1,
configurable: !0
});
t.prototype.init = function(e) {
this.i18n_string = e;
this.setEndValue();
};
t.prototype.setEndValue = function() {
var e = this.getComponent(cc.Label);
cc.isValid(e) && a.i18nMgr._getLabel(this.i18n_string) != this.i18n_string && (e.string = a.i18nMgr._getLabel(this.i18n_string));
};
t.prototype._resetValue = function() {
"" != this.i18n_string && (this.string = this.i18n_string);
};
t.prototype.onDestroy = function() {
a.i18nMgr._addOrDelLabel(this, !1);
};
o([ l({
visible: !1
}) ], t.prototype, "i18n_string", void 0);
o([ l({
type: cc.String
}) ], t.prototype, "string", null);
return o([ c, h("多语言/i18nLabel") ], t);
}(cc.Component);
i.i18nLabel = u;
cc._RF.pop();
}, {
"./i18nMgr": "i18nMgr"
} ],
i18nMgr: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "1e36aJmHntDxqZis49hy52h", "i18nMgr");
Object.defineProperty(i, "__esModule", {
value: !0
});
i.i18nMgr = void 0;
var n = function() {
function e() {}
e.checkInit = function() {
this.language || this.setLanguage("en");
};
e.setLanguage = function(e) {
if (this.language !== e) {
this.language = e;
this.language = "en";
this.reloadLabel();
this.reloadSprite();
}
};
e.getLanguage = function() {
return this.language;
};
e._addOrDelLabel = function(e, t) {
if (t) this.labelArr.push(e); else {
var i = this.labelArr.indexOf(e);
-1 !== i && this.labelArr.splice(i, 1);
}
};
e._getLabel = function(e) {
this.checkInit();
return this.labelData[e] || e;
};
e._addOrDelSprite = function(e, t) {
if (t) this.spriteArr.push(e); else {
var i = this.spriteArr.indexOf(e);
-1 !== i && this.spriteArr.splice(i, 1);
}
};
e._getSprite = function(e, t) {
this.checkInit();
cc.resources.load("i18n/sprite/" + this.language + "/" + e, cc.SpriteFrame, function(e, i) {
if (e) return t(null);
t(i);
});
};
e.reloadLabel = function() {
var e = this, t = "i18n/label/" + this.language;
cc.resources.load(t, function(t, i) {
if (t) {
console.error(t);
e.labelData = {};
e.setLanguage("en");
} else e.labelData = i.json;
for (var n = 0, r = e.labelArr; n < r.length; n++) r[n]._resetValue();
});
};
e.reloadSprite = function() {
for (var e = 0, t = this.spriteArr; e < t.length; e++) t[e]._resetValue();
};
e.language = "";
e.labelArr = [];
e.labelData = {};
e.spriteArr = [];
return e;
}();
i.i18nMgr = n;
cc._RF.pop();
}, {} ],
i18nSprite: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "0ffbcbl89xEj5yYYH4SQgf+", "i18nSprite");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
i.i18nSprite = void 0;
var a = e("./i18nMgr"), s = cc._decorator, c = s.ccclass, l = s.property, h = s.executeInEditMode, u = s.disallowMultiple, f = s.requireComponent, d = s.menu, p = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.i18n_string = "";
return t;
}
t.prototype.onLoad = function() {
a.i18nMgr._addOrDelSprite(this, !0);
this._resetValue();
};
Object.defineProperty(t.prototype, "string", {
get: function() {
return this.i18n_string;
},
set: function(e) {
var t = this;
this.i18n_string = e;
var i = this.getComponent(cc.Sprite);
cc.isValid(i) && a.i18nMgr._getSprite(e, function(e) {
if (cc.isValid(i)) {
i.spriteFrame = e;
t.node.active = !0;
}
});
},
enumerable: !1,
configurable: !0
});
t.prototype._resetValue = function() {
this.string = this.i18n_string;
};
t.prototype.onDestroy = function() {
a.i18nMgr._addOrDelSprite(this, !1);
};
o([ l({
visible: !1
}) ], t.prototype, "i18n_string", void 0);
o([ l({
type: cc.String
}) ], t.prototype, "string", null);
return o([ c, h, f(cc.Sprite), u, d("多语言/i18nSprite") ], t);
}(cc.Component);
i.i18nSprite = p;
cc._RF.pop();
}, {
"./i18nMgr": "i18nMgr"
} ],
test: [ function(e, t, i) {
"use strict";
cc._RF.push(t, "9b604izk7lOBoGjrka9E83+", "test");
var n, r = this && this.__extends || (n = function(e, t) {
return (n = Object.setPrototypeOf || {
__proto__: []
} instanceof Array && function(e, t) {
e.__proto__ = t;
} || function(e, t) {
for (var i in t) Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
})(e, t);
}, function(e, t) {
n(e, t);
function i() {
this.constructor = e;
}
e.prototype = null === t ? Object.create(t) : (i.prototype = t.prototype, new i());
}), o = this && this.__decorate || function(e, t, i, n) {
var r, o = arguments.length, a = o < 3 ? t : null === n ? n = Object.getOwnPropertyDescriptor(t, i) : n;
if ("object" == typeof Reflect && "function" == typeof Reflect.decorate) a = Reflect.decorate(e, t, i, n); else for (var s = e.length - 1; s >= 0; s--) (r = e[s]) && (a = (o < 3 ? r(a) : o > 3 ? r(t, i, a) : r(t, i)) || a);
return o > 3 && a && Object.defineProperty(t, i, a), a;
};
Object.defineProperty(i, "__esModule", {
value: !0
});
var a = cc._decorator, s = a.ccclass, c = a.property, l = function(e) {
r(t, e);
function t() {
var t = null !== e && e.apply(this, arguments) || this;
t.BackGround = null;
t.BackGround1 = null;
t.speed = -100;
return t;
}
t.prototype.onLoad = function() {
this.node1 = this.addnode(1);
this.node2 = this.addnode();
this.winHeight = 1880;
this.node1.y = 0;
this.speed > 0 ? this.node2.y = -this.winHeight : this.node2.y = this.winHeight;
};
t.prototype.start = function() {};
t.prototype.update = function(e) {
this.node2.y += this.speed * e;
this.node1.y += this.speed * e;
if (this.speed > 0) {
this.node1.y >= this.winHeight && (this.node1.y = this.node2.y - this.winHeight);
this.node2.y >= this.winHeight && (this.node2.y = this.node1.y - this.winHeight);
} else {
this.node1.y <= -this.winHeight && (this.node1.y = this.node2.y + this.winHeight);
this.node2.y <= -this.winHeight && (this.node2.y = this.node1.y + this.winHeight);
}
};
t.prototype.addnode = function(e) {
var t = new cc.Node();
t.addComponent(cc.Sprite).spriteFrame = e ? this.BackGround : this.BackGround1;
t.parent = this.node;
t.width = 750;
t.height = 1880;
return t;
};
o([ c(cc.SpriteFrame) ], t.prototype, "BackGround", void 0);
o([ c(cc.SpriteFrame) ], t.prototype, "BackGround1", void 0);
o([ c ], t.prototype, "speed", void 0);
return o([ s ], t);
}(cc.Component);
i.default = l;
cc._RF.pop();
}, {} ],
use_reversed_rotateBy: [ function(e, t) {
"use strict";
cc._RF.push(t, "cb29ckhv3xAYIGD3wXnUyke", "use_reversed_rotateBy");
cc.RotateBy._reverse = !0;
cc._RF.pop();
}, {} ],
"use_v2.1-2.2.1_cc.Toggle_event": [ function(e, t) {
"use strict";
cc._RF.push(t, "57b543o3LFGopZTkMRNqndb", "use_v2.1-2.2.1_cc.Toggle_event");
cc.Toggle && (cc.Toggle._triggerEventInScript_isChecked = !0);
cc._RF.pop();
}, {} ]
}, {}, [ "AudioController", "AwardCoin", "AwardViewControl", "BackgroundAdapter", "PassLevelBean", "RecordBean copy", "RecordBean", "UserData", "WXUserBean", "Box", "BtnSound", "CatFace", "CoinsUtil", "HttpRequest", "HttpRequestNew", "ModuleEventEnum", "Pop", "RemindAnamation", "StorageUtil", "SubContextView", "WXCommon", "Wx", "ContentAdapter", "DialogAnimation", "DualBlur", "Facade", "GameControl", "GetWXInfoDialog", "GlobalConst", "InnerBox", "InnerLevelControl", "InviteControl", "InviteItem", "LevelData", "LevelUtils", "LimitFailControl", "LoadingLabel", "MainControl", "Money", "Network", "NodePool", "NotifyLayout", "PoolMng", "ProfiledScreenAdapter", "RankItem", "RecordShareDialog", "RedPackage", "ScrollViewCtrl", "SettleUtil", "FluxayTexture", "GaussianBlur", "RoundRectMask", "SpriteRadius", "EditorAsset", "SubDomainControl", "SubPackage", "ThemeBox", "ThemeGridControl", "ThemeScrollControl", "ThemeViewControl", "ToolsControl", "UserInfo", "BaseConfig", "BaseLayer", "BaseUtils", "ButtonScaler", "PropManager", "gameFit", "CallNative", "JsBridge", "MethodManager", "AnalyticsUtilities", "List", "ListItem", "test", "crypto-js", "i18nLabel", "i18nMgr", "i18nSprite", "use_reversed_rotateBy", "use_v2.1-2.2.1_cc.Toggle_event" ]);