package com.wll.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import com.android.billingclient.api.*;
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.collect.ImmutableList;
import com.wll.rescue.beauty.FbfCallback;
import com.wll.rescue.beauty.JsBridge;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class PayHelper {

    private static final String TAG = "PayHelper";
    private BillingClient billingClient;
    private Context context;

    private static final class Holder{
        private static final PayHelper instance = new PayHelper();
    }

    public static PayHelper getInstance(){
        return PayHelper.Holder.instance;
    }

    public void queryProduct(Activity act) {
        context = act;
        PurchasesUpdatedListener purchasesUpdatedListener = new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {
                Log.e(TAG, "pay result:" + billingResult);
                Log.e(TAG, "pay result list:" + purchases);
                // To be implemented in a later section.
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                        && purchases != null) {
                    for (Purchase purchase : purchases) {
                        handlePurchase(purchase);
                    }
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                    // Handle an error caused by a user cancelling the purchase flow.
                } else {
                    // Handle any other error codes.
                }
            }
        };
        billingClient = BillingClient.newBuilder(act)
                .setListener(purchasesUpdatedListener)
                .enablePendingPurchases()
                .build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                Log.e(TAG, "res code is :" + billingResult.getResponseCode());
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // The BillingClient is ready. You can query purchases here.
                    QueryProductDetailsParams queryProductDetailsParams =
                            QueryProductDetailsParams.newBuilder()
                                    .setProductList(
                                            ImmutableList.of(
                                                    QueryProductDetailsParams.Product.newBuilder()
                                                            .setProductId("rescuebeauty")
                                                            .setProductType(BillingClient.ProductType.INAPP)
                                                            .build()))
                                    .build();

                    billingClient.queryProductDetailsAsync(
                            queryProductDetailsParams,
                            new ProductDetailsResponseListener() {
                                public void onProductDetailsResponse(BillingResult billingResult,
                                                                     List<ProductDetails> productDetailsList) {
                                    // check billingResult
                                    // process returned productDetailsList
                                    Log.e(TAG, "res is :" + billingResult);
                                    Log.e(TAG, "list is :" + productDetailsList.toString());
                                    if (!productDetailsList.isEmpty()) {
                                        for (ProductDetails details : productDetailsList){
                                            if (details !=  null && "rescuebeauty".equals(details.getProductId())){
                                                queryPurchase(new FbfCallback() {
                                                    @Override
                                                    public void callback(int code, String msg, Map<String, Object> data) {
                                                        if (code == 0){
                                                            ThreadUtils.runOnUIThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    pay(act, billingClient, details);
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }

                                    }
                                }
                            }
                    );
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Log.e(TAG, "connect googleplay fail...");
            }
        });

    }

    public void pay(Activity act, BillingClient billingClient, ProductDetails productDetails) {
        ImmutableList productDetailsParamsList =
                ImmutableList.of(
                        BillingFlowParams.ProductDetailsParams.newBuilder()
                                // retrieve a value for "productDetails" by calling queryProductDetailsAsync()
                                .setProductDetails(productDetails)
                                // to get an offer token, call ProductDetails.getSubscriptionOfferDetails()
                                // for a list of offers that are available to the user
//                                .setOfferToken(selectedOfferToken)
                                .build()
                );
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setProductDetailsParamsList(productDetailsParamsList)
                .build();

        // Launch the billing flow
        BillingResult billingResult = billingClient.launchBillingFlow(act, billingFlowParams);
        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
            Log.e(TAG, "launch pay success...");
        }
    }

    public void handlePurchase(Purchase purchase) {
        try {
            isBack = false;
            JSONObject result = new JSONObject();
            result.put("cmd", JsBridge.PAY);
            {
                JSONObject jsonData = new JSONObject();
                jsonData.put("resultType", 99);
                jsonData.put("resultMsg", "pay success");
                JSONObject params = new JSONObject();
                params.put("deviceId", JsBridge.DEVICE_ID);
                params.put("userId", JsBridge.DEVICE_ID);
                params.put("token", purchase.getPurchaseToken());
                params.put("packageName", context.getPackageName());
                params.put("productId", "rescuebeauty");
                params.put("businessId", context.getPackageName());
                jsonData.put("params", params);
                result.put("msg", jsonData);
            }
            JsBridge.toCocos(JsBridge.formatJson(result.toString()));
            ThreadUtils.mMainHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!isBack){
                        JsBridge.toCocos(JsBridge.formatJson(result.toString()));
                    }
                }
            }, 5000);
        }catch (Throwable e){

        }
    }

     private boolean isBack;

    public void consume(String token){
        isBack = true;
        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(token)
                        .build();

        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                    Log.e(TAG, "consume success...");
                    try {
                        JSONObject result = new JSONObject();
                        result.put("cmd", JsBridge.CONSUME);
                        {
                            JSONObject jsonData = new JSONObject();
                            jsonData.put("resultType", 1);
                            jsonData.put("resultMsg", "consume success");
                            result.put("msg", jsonData);
                        }
                        JsBridge.toCocos(JsBridge.formatJson(result.toString()));
                    }catch (Throwable e){

                    }
                }
            }
        };
        billingClient.consumeAsync(consumeParams, listener);
    }

    public void queryPurchase(FbfCallback callback) {
        billingClient.queryPurchasesAsync(BillingClient.SkuType.INAPP, new PurchasesResponseListener() {
            @Override
            public void onQueryPurchasesResponse(@NonNull BillingResult billingResult, @NonNull List<Purchase> list) {
                Log.e(TAG, "queryPurchase code = " + billingResult.getResponseCode() + " getPurchasesList = " + list);
                boolean checkLast = false;
                // 查询成功且列表不为空
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && !list.isEmpty()) {
                    for (Purchase purchase : list) {
                        checkLast = true;
                        handlePurchase(purchase);
                    }
                }
                if (!checkLast && callback != null){
                    callback.callback(0, "", null);
                }
            }
        });

    }
}
