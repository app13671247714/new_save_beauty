package com.wll.utils;

import android.content.Context;

import com.wll.constant.BaseConstant;
import com.wll.rescue.beauty.BuildConfig;
import com.wll.rescue.beauty.MyApp;

import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

public class SaveUtils {

    public static int getChour() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String getDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        month++;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String ss = String.format("%04d%02d%02d", year, month, day);
        return ss;
    }

    public static long getDateLong() {
        Calendar calendar = Calendar.getInstance();
        long lTime = calendar.getTimeInMillis();
        lTime /= 1000;
        return lTime;
    }

    public static String getCountryCode() {
        return DeviceInfo.getCountryDialCode().toUpperCase();
    }

    public static String getAppCode() {
        return BaseConstant.app_code;
    }

    public static String getGoogleId() {
        return (String) SPUtil.get(MyApp.appContext, BaseConstant.google_id, "");
    }


    public String getLanguage(String country) {
        String[] languageArray = {"IN", "PT", "ES", "HI", "GU", "AR", "UR"};
        List<String> languageList = Arrays.asList(languageArray);
        if (!languageList.contains(country)) {
            return "EN";
        }
        return country;
    }

    public String getCountry(String countryCode) {
        String[] countryArray = {"BR", "IN", "ID", "PT", "SF"};
        List<String> countryList = Arrays.asList(countryArray);
        if (!countryList.contains(countryCode)) {
            return "other";
        }
        return countryCode;
    }

    public static int getVc() {
        return BuildConfig.VERSION_CODE;
    }

    public static String getVn() {
        return BuildConfig.VERSION_NAME;
    }


    public static String getRegisterDate() {
        return (String) SPUtil.get(MyApp.appContext, BaseConstant.register_date, "");
    }

    public static long getRegisterTime() {
        return (long) SPUtil.get(MyApp.appContext, BaseConstant.register_time, 0L);
    }

    /**
     * 是否正在使用VPN
     */
    public static int vpn() {
        try {
            Enumeration niList = NetworkInterface.getNetworkInterfaces();
            if (niList != null) {
                for (Object intf : java.util.Collections.list(niList)) {
                    if (intf instanceof NetworkInterface) {
                        NetworkInterface networkInterface = (NetworkInterface) intf;
                        if (!networkInterface.isUp() || networkInterface.getInterfaceAddresses().size() == 0) {
                            continue;
                        }
                        if ("tun0".equalsIgnoreCase(networkInterface.getName())) {
                            return 1;
                        }
                        if ("ppp0".equalsIgnoreCase(networkInterface.getName())) {
                            return 1;
                        }
                    } else {
                        return 0;
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getLocalLanguage() {
        String language = (String) SPUtil.get(MyApp.appContext, "local_language", "");
        if (language.isEmpty()) {
            String s = Locale.getDefault().getLanguage().toLowerCase();
            if (s.equals("zh") || s.equals("ft")) {
                return "en";
            }
            return s;
        } else {
            return language;
        }
    }

    public static int dip2px(Context context, float dpValue) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5F);
    }
}
