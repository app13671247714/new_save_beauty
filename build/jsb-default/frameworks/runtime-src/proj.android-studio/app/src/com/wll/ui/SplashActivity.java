package com.wll.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;

import com.demo.mydemo.lib.UtilsAppsflyer;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.ad.AdProxyHelper;
//import com.wll.ad.SplashAdHelper;
import com.wll.rescue.beauty.R;
import com.wll.utils.SPUtil;

import org.cocos2dx.javascript.AppActivity;

import java.util.concurrent.atomic.AtomicBoolean;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UtilsLog.aliveStart();
        UtilsAppsflyer.start();
        setContentView(R.layout.activity_splash);
        startActivity(new Intent(SplashActivity.this, AppActivity.class));
        finish();
    }

    @Override
    protected void onStop(){
        super.onStop();
        UtilsLog.send();
    }
}
