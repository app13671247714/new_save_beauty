package com.wll.ad;

import android.app.Activity;
import android.content.Context;

import com.applovin.sdk.AppLovinSdk;
import com.wll.rescue.beauty.FbfCallback;
import com.wll.rescue.beauty.MyApp;

import java.util.Map;


public class AdProxyHelper {

    public static void initAd(Activity act) {
        AdHelper.initSDK(act, new FbfCallback() {
            @Override
            public void callback(int code, String msg, Map<String, Object> data) {
                if (code == 0) {
                    RewardAdHelper.getInstance().load(act, "evt_show_home", "page_home");
                    InterstitialAdHelper.getInstance().load(act, "evt_show_home", "page_home");
                    BannerAdHelper.getInstance().load(act);
                }
            }
        });
    }

    public static void initApplovinSdk(Context context, FbfCallback callback) {
        AdHelper.initSDK(context, callback);
    }

    public static boolean showRewardVideoAd(Activity act, String clickAction, String page, FbfCallback callback) {
        if (AppLovinSdk.getInstance(MyApp.appContext).isInitialized()) {
            return RewardAdHelper.getInstance().showRewardVideoAd(act, clickAction, page, callback);
        }
        return false;
    }

    public static boolean showInterstitialAd(Activity act, String clickAction, String page, FbfCallback callback) {
        if (AppLovinSdk.getInstance(MyApp.appContext).isInitialized()) {
            return InterstitialAdHelper.getInstance().showInterstitialAd(act, clickAction, page, callback);
        }
        return false;
    }

    public static void showBannerAd(Activity act, String adId, FbfCallback callback) {
        if (AppLovinSdk.getInstance(MyApp.appContext).isInitialized()) {
            BannerAdHelper.getInstance().showBannerAd(act, callback);
        }

    }

    public static void hiddenBannerAd() {
        BannerAdHelper.getInstance().hiddenAdView();
    }


    public static void cancelRewardAd() {
        RewardAdHelper.getInstance().cancelRewardAd();
    }

}
