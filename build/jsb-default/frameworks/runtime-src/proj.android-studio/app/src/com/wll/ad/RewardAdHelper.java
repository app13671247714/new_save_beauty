package com.wll.ad;

import static com.wll.constant.EventConstant.ad_error;
import static com.wll.constant.EventConstant.ad_init_error;
import static com.wll.constant.EventConstant.ad_request_start;
import static com.wll.constant.EventConstant.ad_reward;
import static com.wll.constant.EventConstant.ad_type_rewarded_video;
import static com.wll.constant.EventConstant.view_reward;

import android.app.Activity;
import android.util.Log;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdRevenueListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.demo.mydemo.lib.MaxLogHelper;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.rescue.beauty.FbfCallback;
import com.wll.utils.ThreadUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class RewardAdHelper implements MaxRewardedAdListener, MaxAdRevenueListener {
    private static final String TAG = "RewardAdHelper";
    public static final String AD_UNIT_ID = "b75a2f0d4f8a1275";
    private AtomicBoolean rewardIsLoading = new AtomicBoolean(false);
    private ConcurrentHashMap<String, MaxAd> cacheAdMap = new ConcurrentHashMap<>();

    private static AtomicInteger retryAttemptReward = new AtomicInteger(0);


    private MaxRewardedAd rewardedAd;
    private FbfCallback mCallback;

    private Activity mActivity;

    private MaxLogHelper rewardLog;


    private static final class Holder {
        private static final RewardAdHelper instance = new RewardAdHelper();
    }

    public static RewardAdHelper getInstance() {
        return Holder.instance;
    }

    public void load(Activity act, String clickAction, String page) {
        this.mActivity = act;
        rewardLog = new MaxLogHelper(view_reward, ad_reward, AD_UNIT_ID,clickAction,page);
        rewardedAd = MaxRewardedAd.getInstance(AD_UNIT_ID, act);
        loadReward();
    }

    private void loadReward() {
        if (rewardIsLoading.get()) {
            return;
        }

        if (rewardLog!=null){
            rewardLog.onRequest();
        }
        rewardIsLoading.set(true);
        UtilsLog.log(ad_type_rewarded_video, ad_request_start, null);
        ThreadUtils.mMainHandler.removeCallbacks(delayFailCallback);
        ThreadUtils.mMainHandler.postDelayed(delayFailCallback, getLoadAdDelayMillis(retryAttemptReward.get()));
        Log.e(TAG, "preload...");
    }

    public boolean showRewardVideoAd(Activity act, String clickAction, String page, FbfCallback callback) {
        this.mActivity = act;
        this.mCallback = callback;
        rewardLog.refreshPage(clickAction,page);
        if (cacheAdMap.get(AD_UNIT_ID) != null) {
            if (rewardedAd.isReady()) {
                rewardedAd.setListener(this);
                rewardedAd.showAd();
                if (mCallback != null) {
                    mCallback.callback(1, "load success", null);
                    return true;
                }
            } else {
                load(act,clickAction,page);
            }
        } else {
            if (rewardedAd == null) {
                UtilsLog.log(ad_init_error, ad_error, null);
                load(act,clickAction,page);
            }
        }
        return false;
    }

    private Runnable delayFailCallback = new Runnable() {
        @Override
        public void run() {
            if (rewardedAd == null) {
                rewardedAd = MaxRewardedAd.getInstance(AD_UNIT_ID, mActivity);
            }
            rewardedAd.setListener(RewardAdHelper.this);
            rewardedAd.setRevenueListener(null);
            rewardedAd.loadAd();
        }
    };

    @Override
    public void onUserRewarded(MaxAd maxAd, MaxReward maxReward) {
        Log.e(TAG, "onUserRewarded...");
    }

    @Override
    public void onRewardedVideoStarted(MaxAd maxAd) {

    }

    @Override
    public void onRewardedVideoCompleted(MaxAd maxAd) {

    }

    @Override
    public void onAdLoaded(MaxAd maxAd) {
        Log.e(TAG, "onAdLoaded...");
        if (retryAttemptReward == null) {
            retryAttemptReward = new AtomicInteger(0);
        }

        if (cacheAdMap == null) {
            cacheAdMap = new ConcurrentHashMap<>();
        }
        cacheAdMap.put(AD_UNIT_ID, maxAd);
        retryAttemptReward.set(0);
        rewardIsLoading.set(false);
        if (rewardLog != null) {
            rewardLog.onAdLoaded(maxAd);
        }

        if (mCallback != null) {
            ThreadUtils.mMainHandler.removeCallbacks(delayFailCallback);
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(1, "load success", data);
        }
    }

    @Override
    public void onAdDisplayed(MaxAd ad) {
        if (rewardLog != null) {
            rewardLog.onImpression();
        }
    }

    @Override
    public void onAdHidden(MaxAd maxAd) {
        Log.e(TAG, "onAdHidden...");
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(7, "rewarded close", data);
            mCallback.callback(6, "rewarded verify", data);
        }
        loadReward();
        if (rewardLog != null) {
            rewardLog.onClose();
        }
    }

    @Override
    public void onAdClicked(MaxAd maxAd) {
        Log.e(TAG, "onAdClicked...");
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(8, "rewarded click", data);
        }
        if (rewardLog != null) {
            rewardLog.onClicked();
        }
    }

    @Override
    public void onAdLoadFailed(String s, MaxError maxError) {
        Log.e(TAG, "onAdLoadFailed...");
        if (retryAttemptReward == null) {
            retryAttemptReward = new AtomicInteger(0);
        }
        retryAttemptReward.set(retryAttemptReward.get() + 1);
        rewardIsLoading.set(false);
        loadReward();
        if (rewardLog != null) {
            rewardLog.onLoadFail(maxError);
        }
        if (mCallback != null) {
            mCallback.callback(-1, "load fail..." + s + maxError, null);
        }
    }

    @Override
    public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
        if (rewardLog != null) {
            rewardLog.onShowFail(maxError);
        }
        loadReward();
    }

    @Override
    public void onAdRevenuePaid(MaxAd maxAd) {
    }

    public void cancelRewardAd() {
        if (mCallback != null) {
            mCallback.callback(-2, "load fail...", null);
        }
    }


    public static long getLoadAdDelayMillis(int retryAttempt) {
        if (retryAttempt == 0) {
            return 0L;
        }
        long pow = (long) Math.pow(2, retryAttempt);
        if (pow >= 5 * 64) {
            pow = 5 * 64;
        }
        return TimeUnit.SECONDS.toMillis(pow);
    }


}
