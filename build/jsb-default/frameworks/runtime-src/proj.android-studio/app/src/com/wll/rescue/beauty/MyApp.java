package com.wll.rescue.beauty;

import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.webkit.WebView;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.demo.mydemo.lib.AppConfigMgr;
import com.demo.mydemo.lib.MMConfigMgr;
import com.demo.mydemo.lib.UtilsInit;
import com.facebook.FacebookSdk;
import com.wll.constant.BaseConstant;
import com.wll.utils.DeviceInfo;
import com.wll.utils.LogUtils;
import com.wll.utils.SPUtil;
import com.wll.utils.SaveUtils;

import java.util.Map;

public class MyApp extends Application {
    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化日志工具
        UtilsInit.init(this, "adNKJmgu#E6beDJVXb8c", "wllrescuebeauty.com",
                "GP", "QKdwT8Pz5wJFfRzCEy8Wfn");
        appContext = this;
        //Android 9及以上必须设置
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            String processName = getProcessName();
            if (!getPackageName().equals(processName)) {
                WebView.setDataDirectorySuffix(processName);
            }
        }
        LogUtils.init(BuildConfig.DEBUG, getString(R.string.app_name));
        initFacebook(this);
        initAppsFlyer();
        preInit();
        MMConfigMgr.getInstance().addListener(new MMConfigMgr.IConfigMgrListener() {
            @Override
            public void onRequestConfigAsyncComplete(int i, boolean b) {
                LogUtils.i("configMgr", "" + AppConfigMgr.getConfig().album + " " + AppConfigMgr.getConfig().challenge);
            }
        });
        LogUtils.i("configMgr", "" + AppConfigMgr.getConfig().album + " " + AppConfigMgr.getConfig().challenge);
    }

    private void preInit() {
        String googleId = (String) SPUtil.get(this, BaseConstant.google_id, "");
        if (googleId.isEmpty() || googleId.equals("null")) {
            try {
                String googleAdId = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(this).getId();
                SPUtil.put(this, BaseConstant.google_id, googleAdId);
            } catch (Throwable e) {
                SPUtil.put(this, BaseConstant.google_id, DeviceInfo.getAndroidId(MyApp.appContext));
            }
        }
        String registerDate = (String) SPUtil.get(this, BaseConstant.register_date, "");
        if (registerDate.isEmpty()) {
            SPUtil.put(this, BaseConstant.register_date, SaveUtils.getDate());
        }
        long registerTime = (long) SPUtil.get(this, BaseConstant.register_time, 0L);
        if (registerTime == 0) {
            SPUtil.put(this, BaseConstant.register_time, SaveUtils.getDateLong());
        }

    }


    public void initFacebook(Application application) {
        try {
            FacebookSdk.setApplicationId(application.getString(R.string.facebook_app_id));
            FacebookSdk.setClientToken(application.getString(R.string.facebook_client_token));
            FacebookSdk.sdkInitialize(application);
            FacebookSdk.setAutoInitEnabled(true);
            FacebookSdk.setAutoLogAppEventsEnabled(true);
            FacebookSdk.setAdvertiserIDCollectionEnabled(true);
            FacebookSdk.fullyInitialize();
        } catch (Throwable throwable) {

        }
    }


    public void initAppsFlyer() {
        AppsFlyerConversionListener conversionListener = new AppsFlyerConversionListener() {
            @Override
            public void onConversionDataSuccess(Map<String, Object> map) {

            }

            @Override
            public void onConversionDataFail(String s) {

            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {

            }

            @Override
            public void onAttributionFailure(String s) {

            }
        };
        AppsFlyerLib.getInstance().init("QKdwT8Pz5wJFfRzCEy8Wfn", conversionListener, this);
        AppsFlyerLib.getInstance().start(this);
    }
}
