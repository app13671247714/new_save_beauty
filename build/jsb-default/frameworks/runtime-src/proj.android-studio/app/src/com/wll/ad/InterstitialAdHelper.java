package com.wll.ad;

import static com.wll.constant.EventConstant.ad_error;
import static com.wll.constant.EventConstant.ad_init_error;
import static com.wll.constant.EventConstant.ad_interstitial;
import static com.wll.constant.EventConstant.ad_request_start;
import static com.wll.constant.EventConstant.ad_type_interstitial_video;
import static com.wll.constant.EventConstant.view_inter;

import android.app.Activity;
import android.util.Log;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdRevenueListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.demo.mydemo.lib.MaxLogHelper;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.rescue.beauty.FbfCallback;
import com.wll.utils.ThreadUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class InterstitialAdHelper implements MaxAdListener, MaxAdRevenueListener {
    private static final String TAG = "InterstitialAdHelper";
    public static final String AD_UNIT_ID = "f64ce2f4cf7f2520";
    private AtomicBoolean interstitialIsLoading = new AtomicBoolean(false);
    private ConcurrentHashMap<String, MaxAd> cacheAdMap = new ConcurrentHashMap<>();

    private static AtomicInteger retryAttemptInter = new AtomicInteger(0);


    private MaxInterstitialAd interstitialAd;
    private FbfCallback mCallback;

    private Activity mActivity;

    private MaxLogHelper interstitialLog;


    private static final class Holder {
        private static final InterstitialAdHelper instance = new InterstitialAdHelper();
    }

    public static InterstitialAdHelper getInstance() {
        return Holder.instance;
    }

    public void load(Activity act, String clickAction, String page) {
        this.mActivity = act;
        interstitialLog = new MaxLogHelper(view_inter, ad_interstitial, AD_UNIT_ID, clickAction, page);
        interstitialAd = new MaxInterstitialAd(AD_UNIT_ID, act);
        loadInterstitial();
    }

    private void loadInterstitial() {
        if (interstitialIsLoading.get()) {
            return;
        }

        if (interstitialLog != null) {
            interstitialLog.onRequest();
        }
        interstitialIsLoading.set(true);
        UtilsLog.log(ad_type_interstitial_video, ad_request_start, null);
        ThreadUtils.mMainHandler.removeCallbacks(delayFailCallback);
        ThreadUtils.mMainHandler.postDelayed(delayFailCallback, getLoadAdDelayMillis(retryAttemptInter.get()));
        Log.e(TAG, "preload...");
    }

    public boolean showInterstitialAd(Activity act, String clickAction, String page, FbfCallback callback) {
        this.mActivity = act;
        this.mCallback = callback;
        interstitialLog.refreshPage(clickAction, page);
        if (cacheAdMap.get(AD_UNIT_ID) != null) {
            if (interstitialAd.isReady()) {
                interstitialAd.setListener(this);
                interstitialAd.showAd();
                if (mCallback != null) {
                    mCallback.callback(1, "load success", null);
                    return true;
                }
            } else {
                load(act, clickAction, page);
            }
        } else {
            if (interstitialAd == null) {
                UtilsLog.log(ad_init_error, ad_error, null);
                load(act, clickAction, page);
            }
        }
        return false;
    }

    private Runnable delayFailCallback = new Runnable() {
        @Override
        public void run() {
            if (interstitialAd == null) {
                interstitialAd = new MaxInterstitialAd(AD_UNIT_ID, mActivity);
            }
            interstitialAd.setListener(InterstitialAdHelper.this);
            interstitialAd.loadAd();
        }
    };


    @Override
    public void onAdLoaded(MaxAd maxAd) {
        Log.e(TAG, "onAdLoaded...");
        if (retryAttemptInter == null) {
            retryAttemptInter = new AtomicInteger(0);
        }

        if (cacheAdMap == null) {
            cacheAdMap = new ConcurrentHashMap<>();
        }
        cacheAdMap.put(AD_UNIT_ID, maxAd);
        retryAttemptInter.set(0);
        interstitialIsLoading.set(false);
        if (interstitialLog != null) {
            interstitialLog.onAdLoaded(maxAd);
        }

        if (mCallback != null) {
            ThreadUtils.mMainHandler.removeCallbacks(delayFailCallback);
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(1, "load success", data);
        }
    }

    @Override
    public void onAdDisplayed(MaxAd ad) {
        if (interstitialLog != null) {
            interstitialLog.onImpression();
        }
    }

    @Override
    public void onAdHidden(MaxAd maxAd) {
        Log.e(TAG, "onAdHidden...");
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(7, "inter close", data);
            mCallback.callback(6, "inter verify", data);
        }
        loadInterstitial();
        if (interstitialLog != null) {
            interstitialLog.onClose();
        }
    }

    @Override
    public void onAdClicked(MaxAd maxAd) {
        Log.e(TAG, "onAdClicked...");
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(8, "rewarded click", data);
        }
        if (interstitialLog != null) {
            interstitialLog.onClicked();
        }
    }

    @Override
    public void onAdLoadFailed(String s, MaxError maxError) {
        Log.e(TAG, "onAdLoadFailed...");
        if (retryAttemptInter == null) {
            retryAttemptInter = new AtomicInteger(0);
        }
        retryAttemptInter.set(retryAttemptInter.get() + 1);
        interstitialIsLoading.set(false);
        loadInterstitial();
        if (interstitialLog != null) {
            interstitialLog.onLoadFail(maxError);
        }
        if (mCallback != null) {
            mCallback.callback(-1, "load fail..." + s + maxError, null);
        }
    }

    @Override
    public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
        if (interstitialLog != null) {
            interstitialLog.onShowFail(maxError);
        }
        loadInterstitial();
    }

    @Override
    public void onAdRevenuePaid(MaxAd maxAd) {
    }

    public static long getLoadAdDelayMillis(int retryAttempt) {
        if (retryAttempt == 0) {
            return 0L;
        }
        long pow = (long) Math.pow(2, retryAttempt);
        if (pow >= 5 * 64) {
            pow = 5 * 64;
        }
        return TimeUnit.SECONDS.toMillis(pow);
    }
}
