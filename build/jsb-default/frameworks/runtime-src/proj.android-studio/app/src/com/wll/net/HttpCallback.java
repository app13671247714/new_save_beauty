package com.wll.net;

/**
 * @Class: HttpCallback
 * @Date: 2023/2/7
 * @Description:
 */
public interface HttpCallback {
    void onResponse(String data);
    void onFailure(String errMsg);
}
