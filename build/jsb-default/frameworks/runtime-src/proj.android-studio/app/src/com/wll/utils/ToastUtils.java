package com.wll.utils;

import android.content.Context;
import android.os.Looper;
import android.widget.Toast;

public class ToastUtils {
    private static Toast toast;

    /**
     * 子/主线程-弹长时间toast
     */
    public static void showLong(Context context, final String text) {
        //判断是否是主线程,如果不是就调用Looper.prepare()方法
        if (!"main".equals(Thread.currentThread().getName())) {
            Looper.prepare();
        }
        if (toast == null) {
//            toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
            toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        } else {
            toast.setText(text);//如果不为空，则直接改变当前toast的文本
        }
        //判断是否在主线程中,如果是主线程就直接show,如果不是就调用Looper.loop()方法
        if ("main".equals(Thread.currentThread().getName())) {
            toast.show();
        } else {
            toast.show();
            Looper.loop();
        }
    }

    /**
     * 子/主线程-弹短时间toast
     */
    public static void showShort(Context context, final String text) {
        //判断是否是主线程,如果不是就调用Looper.prepare()方法
        if (!"main".equals(Thread.currentThread().getName())) {
            Looper.prepare();
        }
        if (toast == null) {
            toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
//            toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        } else {
            toast.setText(text);//如果不为空，则直接改变当前toast的文本
        }
        //判断是否在主线程中,如果是主线程就直接show,如果不是就调用Looper.loop()方法
        if ("main".equals(Thread.currentThread().getName())) {
            toast.show();
        } else {
            toast.show();
            Looper.loop();
        }
    }
}
