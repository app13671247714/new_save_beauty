package com.wll.ad;

import static com.wll.constant.EventConstant.ad_request_start;
import static com.wll.constant.EventConstant.ad_splash;
import static com.wll.constant.EventConstant.ad_type_splash;
import static com.wll.constant.EventConstant.view_splash;

import android.content.Context;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxAppOpenAd;
import com.applovin.sdk.AppLovinSdk;
import com.demo.mydemo.lib.MaxLogHelper;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.rescue.beauty.FbfCallback;
import com.wll.rescue.beauty.MyApp;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class SplashAdHelper implements MaxAdListener {

    private static final String TAG = "SplashAdHelper";
    private static final String AD_UNIT_ID = "da9ac33a98beed0d";

    private AtomicBoolean splashIsLoading = new AtomicBoolean(false);


    private MaxAppOpenAd appOpenAd;
    private FbfCallback mCallback;

    private MaxLogHelper splashLog;

    public SplashAdHelper() {
    }

    private static final class Holder {
        private static final SplashAdHelper instance = new SplashAdHelper();
    }

    public static SplashAdHelper getInstance() {
        return SplashAdHelper.Holder.instance;
    }

    public void initAndLoad(Context context, FbfCallback callback) {
        this.mCallback = callback;
        appOpenAd = new MaxAppOpenAd(AD_UNIT_ID, context);
        splashLog = new MaxLogHelper(view_splash, ad_splash, AD_UNIT_ID, "", "");
        loadSplashAd();
    }

    private void loadSplashAd() {
        if (splashIsLoading.get()) {
            return;
        }
        if (splashLog != null) {
            splashLog.onRequest();
        }
        splashIsLoading.set(true);
        UtilsLog.log(ad_type_splash, ad_request_start, null);
        appOpenAd.setListener(this);
        appOpenAd.setRevenueListener(null);
        appOpenAd.loadAd();
    }

    public void showSplashAd() {
        if (appOpenAd != null) {
            try {
                if (appOpenAd.isReady() && AppLovinSdk.getInstance(MyApp.appContext).isInitialized()) {
                    appOpenAd.showAd();
                }
            } catch (Throwable throwable) {

            }
        }
    }

    @Override
    public void onAdLoaded(MaxAd ad) {
        splashIsLoading.set(false);
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", ad.getRevenue() * 1000);
            data.put("adPlatform", ad.getNetworkName());
            mCallback.callback(1, "load success", data);
        }
        if (splashLog != null) {
            splashLog.onAdLoaded(ad);
        }

    }

    @Override
    public void onAdDisplayed(MaxAd ad) {
        if (splashLog != null) {
            splashLog.onImpression();
        }
        if (mCallback != null) {
            mCallback.callback(99, "show success", new HashMap<>());
        }
    }

    @Override
    public void onAdHidden(MaxAd maxAd) {
        if (splashLog != null) {
            splashLog.onClose();
        }
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(7, "rewarded close", data);
        }
    }

    @Override
    public void onAdClicked(MaxAd maxAd) {
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(8, "rewarded click", data);
        }
        if (splashLog != null) {
            splashLog.onClicked();
        }

    }

    @Override
    public void onAdLoadFailed(String s, MaxError maxError) {
        splashIsLoading.set(false);
        if (mCallback != null) {
            mCallback.callback(-1, "load fail..." + s + maxError, null);
        }
        if (splashLog != null) {
            splashLog.onLoadFail(maxError);
        }
    }

    @Override
    public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
        if (mCallback != null) {
            mCallback.callback(-1, "load fail..." + maxError, null);
        }
        if (splashLog != null) {
            splashLog.onShowFail(maxError);
        }
    }
}
