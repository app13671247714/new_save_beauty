package com.wll.ad;

import android.content.Context;

import com.applovin.sdk.AppLovinSdk;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.constant.EventConstant;
import com.wll.rescue.beauty.FbfCallback;

public class AdHelper {
    private static boolean isInit = false;

    public static void initSDK(Context context, FbfCallback callback) {
        if (!isInit) {
            AppLovinSdk.getInstance(context).setMediationProvider("max");
            AppLovinSdk.initializeSdk(context, configuration -> {
                if (callback != null) {
                    callback.callback(0, "init success", null);
                }
                UtilsLog.log(EventConstant.ad_init, EventConstant.ad_error, null);
//                OneLogCore.Companion.getInstance().sendEvent(EventConstant.ad_error, SaveUtils.getEventMsgStr(EventConstant.ad_init, "init success"));
                isInit = true;
            });
        } else {
            if (callback != null) {
                callback.callback(0, "init success", null);
            }
        }

    }
}
