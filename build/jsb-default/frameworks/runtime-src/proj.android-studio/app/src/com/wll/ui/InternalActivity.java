package com.wll.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.applovin.sdk.AppLovinSdk;
import com.wll.constant.BaseConstant;
import com.wll.rescue.beauty.MyApp;
import com.wll.rescue.beauty.R;
import com.wll.utils.DeviceInfo;
import com.wll.utils.SPUtil;
import com.wll.utils.SaveUtils;

public class InternalActivity extends BaseActivity {
    private TextView tvCountry, tvVpn, tvUerId, tvGoogleId, tvVersion, tvDeviceId, tvOpenDebug;
    private ImageView ivBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal);
        tvCountry = findViewById(R.id.tv_country);
        tvVpn = findViewById(R.id.tv_vpn);
        tvUerId = findViewById(R.id.tv_user_id);
        tvGoogleId = findViewById(R.id.tv_google_id);
        tvVersion = findViewById(R.id.tv_version);
        tvDeviceId = findViewById(R.id.tv_device_id);
        ivBack = findViewById(R.id.iv_back);
        tvOpenDebug = findViewById(R.id.tv_ad_debug);
        ivBack.setOnClickListener(view -> {
            finish();
        });
        tvOpenDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppLovinSdk.getInstance(InternalActivity.this).showMediationDebugger();
            }
        });

        tvCountry.setText(SaveUtils.getCountryCode());
        tvVpn.setText(SaveUtils.vpn() + "");
        String googleId = (String) SPUtil.get(MyApp.appContext, BaseConstant.google_id, "");
        tvUerId.setText(googleId);
        tvGoogleId.setText(googleId);
        tvVersion.setText(SaveUtils.getVn());
        tvDeviceId.setText(DeviceInfo.getAndroidId(this));


    }
}
