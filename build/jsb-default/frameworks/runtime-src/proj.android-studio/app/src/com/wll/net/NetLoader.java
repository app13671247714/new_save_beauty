package com.wll.net;

import android.util.Log;

import androidx.annotation.NonNull;

import com.wll.utils.AesUtils;
import com.wll.constant.BaseConstant;

import org.cocos2dx.okhttp3.Call;
import org.cocos2dx.okhttp3.Callback;
import org.cocos2dx.okhttp3.Headers;
import org.cocos2dx.okhttp3.MediaType;
import org.cocos2dx.okhttp3.OkHttpClient;
import org.cocos2dx.okhttp3.Request;
import org.cocos2dx.okhttp3.RequestBody;
import org.cocos2dx.okhttp3.Response;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

public class NetLoader {
    public static NetLoader netloader;

    public static synchronized NetLoader getInstance() {
        if (netloader == null) {
            synchronized (NetLoader.class) {
                if (netloader == null) {
                    netloader = new NetLoader();
                }
            }
        }
        return netloader;
    }

    public void get(String url, WeakHashMap<String, String> headers, HttpCallback callback) {
        OkHttpClient client = new OkHttpClient();
        // 添加Header
        Headers.Builder builder_header = new Headers.Builder();
        if (headers != null && headers.size() > 0) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                builder_header.add(entry.getKey(), entry.getValue());
            }
        }
        Headers headerBuild = builder_header.build();
        Request request = new Request.Builder()
                .headers(headerBuild)
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                if (callback != null) {
                    callback.onFailure(e.toString());
                }
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.body() != null) {
                    String result = response.body().string();
                    if (callback != null) {
                        callback.onResponse(result);
                    }
                }
            }
        });
    }

    public void post(String url, String params
            , final HttpCallback callback) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(20000L, TimeUnit.MILLISECONDS) //连接超时
                .readTimeout(20000L, TimeUnit.MILLISECONDS) //读取超时
                .writeTimeout(20000L, TimeUnit.MILLISECONDS) //写入超时
                .build();
        MediaType JSON = MediaType.parse("text/plain");
        if (params.isEmpty()) {
            if (callback != null) {
                callback.onFailure("input is null");
                return;
            }
        }
        RequestBody body = RequestBody.create(JSON, params);
        // 添加Header
        Headers.Builder builder_header = new Headers.Builder();
        builder_header.add(BaseConstant.app, BaseConstant.app_code);
        Headers headerBuild = builder_header.build();
        Request request = new Request.Builder()
                .headers(headerBuild)
                .url(url)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                if (callback != null) {
                    callback.onFailure(e.toString());
                    Log.i("gaoyuan:",e.toString());
                }
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == HttpURLConnection.HTTP_OK) {
                    String result = response.body().string();
                    Log.i("gaoyuan:",AesUtils.decrypt(result));
                    if (callback != null) {
                        callback.onResponse(result);
                    }
                } else {
                    if (callback != null) {
                        callback.onFailure(response.message());
                    }
                }
            }
        });
    }

}
