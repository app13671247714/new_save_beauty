package com.wll.ad;

import static com.wll.constant.EventConstant.ad_banner;
import static com.wll.constant.EventConstant.ad_error;
import static com.wll.constant.EventConstant.ad_init_error;
import static com.wll.constant.EventConstant.ad_request_start;
import static com.wll.constant.EventConstant.ad_type_banner;
import static com.wll.constant.EventConstant.view_banner;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxAdView;
import com.demo.mydemo.lib.MaxLogHelper;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.rescue.beauty.FbfCallback;
import com.wll.rescue.beauty.R;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class BannerAdHelper implements MaxAdViewAdListener {
    private static final String TAG = "BannerAdHelper";
    private static final String AD_UNIT_ID = "cc134c3b9dbdd014";

    private MaxAdView adView;
    private FbfCallback mCallback;


    private AtomicBoolean bannerIsLoading = new AtomicBoolean(false);


    @Override
    public void onAdExpanded(MaxAd maxAd) {

    }

    @Override
    public void onAdCollapsed(MaxAd maxAd) {

    }

    private static final class Holder {
        private static final BannerAdHelper instance = new BannerAdHelper();
    }

    public static BannerAdHelper getInstance() {
        return Holder.instance;
    }

    public void load(Activity act) {
        bannerLog = new MaxLogHelper(view_banner, ad_banner, AD_UNIT_ID, "evt_show_home", "page_home");
        adView = new MaxAdView(AD_UNIT_ID, act);
        loadBannerAd();
    }

    private void loadBannerAd() {
        if (bannerIsLoading.get()) {
            return;
        }
        if (bannerLog != null) {
            bannerLog.onRequest();
        }
        bannerIsLoading.set(true);
        UtilsLog.log(ad_type_banner, ad_request_start, null);
        adView.setListener(this);
        adView.loadAd();
    }

    private MaxLogHelper bannerLog;

    public void showBannerAd(Activity act, FbfCallback callback) {

        if (adView == null) {
            UtilsLog.log(ad_init_error, ad_error, null);
            load(act);
        }
        mCallback = callback;
        if (mCallback != null) {
            mCallback.callback(0, "start load", null);
        }
        if (adView.getParent() != null) {
            ((ViewGroup) adView.getParent()).removeView(adView);
        }
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        // Banner height on phones and tablets is 50 and 90, respectively
        int heightPx = act.getResources().getDimensionPixelSize(R.dimen.banner_height);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, heightPx);
        params.bottomMargin = 50;
        params.gravity = Gravity.BOTTOM;
        adView.setLayoutParams(params);
        ViewGroup rootView = act.findViewById(android.R.id.content);
        rootView.addView(adView);
    }

    @Override
    public void onAdLoaded(MaxAd maxAd) {
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(1, "load success", data);
        }
        bannerIsLoading.set(false);
        if (bannerLog != null) {
            bannerLog.onAdLoaded(maxAd);
        }
        adView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAdDisplayed(MaxAd ad) {
        if (bannerLog != null) {
            bannerLog.onImpression();
        }
    }

    @Override
    public void onAdHidden(MaxAd maxAd) {
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(7, "rewarded close", data);
        }
        if (bannerLog != null) {
            bannerLog.onClose();
        }
        loadBannerAd();
    }

    @Override
    public void onAdClicked(MaxAd maxAd) {
        if (mCallback != null) {
            Map<String, Object> data = new HashMap<>();
            data.put("ecpm", maxAd.getRevenue() * 1000);
            data.put("adPlatform", maxAd.getNetworkName());
            mCallback.callback(8, "rewarded click", data);
        }
        if (bannerLog != null) {
            bannerLog.onClicked();
        }
    }

    @Override
    public void onAdLoadFailed(String s, MaxError maxError) {
        if (mCallback != null) {
            mCallback.callback(-1, "load fail..." + s + maxError, null);
        }
        bannerIsLoading.set(false);
        if (bannerLog != null) {
            bannerLog.onLoadFail(maxError);
        }
        loadBannerAd();
    }

    @Override
    public void onAdDisplayFailed(MaxAd maxAd, MaxError maxError) {
        if (bannerLog != null) {
            bannerLog.onShowFail(maxError);
        }
        loadBannerAd();
    }

    public void hiddenAdView() {
        if (adView != null) {
            adView.setVisibility(View.GONE);
            adView.destroy();
            adView = null;
        }
    }


}
