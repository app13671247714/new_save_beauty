package com.wll.constant;

public class EventConstant {

    public static String ad_request_start = "ad_req_start";
    public static String ad_error = "ad_error";
    public static String ad_init = "ad_init";

    public static String ad_load_error = "ad_load_error";
    public static String ad_init_error = "ad_init_error";
    public static String ad_display_error = "ad_display_error";
    public static String ad_type_rewarded_video = "ad_type_rewarded_video";
    public static String ad_type_banner = "ad_type_banner";
    public static String ad_type_splash = "ad_type_splash";

    public static String ad_type_interstitial_video = "ad_type_interstitial_video";
    public static String view_splash = "page_ad_splash";
    public static String view_reward = "page_ad_rewarded_video";

    public static String view_inter = "page_ad_inter";

    public static String view_banner = "view_ad_banner";

    public static String ad_splash = "splash";
    public static String ad_banner = "banner";
    public static String ad_reward = "reward_video";

    public static String ad_interstitial = "interstitial";
}
