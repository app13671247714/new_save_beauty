package com.wll.rescue.beauty;
import java.util.Map;

public interface FbfCallback {
    void callback(int code, String msg, Map<String, Object> data);
}
