package com.wll.utils;

import android.util.Base64;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class AesUtils {
    public static String ENCRYPT_KEY = "BD3A6DE43FF080A512A00AE7B036134092A0600D1FAF0BF641183F95D4EEC016";
    public static String ENCRYPT_IV = "8DA8F20226552F19F907767B3529C016";
    private static String CODE_TYPE = "UTF-8";

    private static AesUtils instance = null;

    private static Cipher cipher;

    private static SecretKeySpec key = null;

    private static AlgorithmParameterSpec iv = null;


    static {
        String key = ENCRYPT_KEY, iv = ENCRYPT_IV;
        if (instance == null) {
            instance = new AesUtils(key, iv);
        }
    }

    /**
     * 使用其他的key/iv时调用此方法
     *
     * @param key 密钥
     * @param iv  偏移量
     * @return
     */
    public static AesUtils getInstance(String key, String iv) {
        instance = new AesUtils(key, iv);
        return instance;
    }


    private AesUtils(String keyStr, String ivStr) {
        try {
            iv = new IvParameterSpec(hex2Byte(ivStr));
            key = new SecretKeySpec(hex2Byte(keyStr), "AES");
            //cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            cipher = Cipher.getInstance("AES/CTR/NoPadding");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 加密
     *
     * @param bytes 字节数据
     * @param flag  Base标记
     * @return 加密后字节数组
     */
    public static byte[] encrypt(byte[] bytes, int flag) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            byte[] b = cipher.doFinal(bytes);
            return Base64.encode(b, 0, b.length, flag);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 加密
     *
     * @param input 输入
     * @return String 加密后字符串
     */
    public static String encrypt(String input) {
        // 注意使用的是 Base64.NO_WRAP
        return encrypt(input, Base64.NO_WRAP);
    }

    /**
     * 加密
     *
     * @param txt
     * @return String
     */
    public static String encrypt(String txt, int flag) {
        try {
            byte[] b = encrypt(txt.getBytes(CODE_TYPE), flag);
            return new String(b, CODE_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     *
     * @param bytes 字节数组
     * @param flag  标记
     * @return byte[] 解密后的字节数组
     */
    public static byte[] decrypt(byte[] bytes, int flag) {
        try {
            byte[] b = bytes;
            b = Base64.decode(b, 0, b.length, flag);
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            return cipher.doFinal(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 解密
     *
     * @param input
     * @return String
     */
    public static String decrypt(String input) {

        return decrypt(input, Base64.NO_WRAP);
    }

    /**
     * 解密
     *
     * @param input 输入
     * @return String 返回
     */
    public static String decrypt(String input, int flag) {

        try {

            byte[] bytes = input.getBytes(CODE_TYPE);

            byte[] decryptArray = decrypt(bytes, flag);

            if (decryptArray != null) {
                return new String(decryptArray, CODE_TYPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] hex2Byte(String string) {
        if (string == null || string.length() < 1) {
            return null;
        }
        // 因为一个byte生成两个字符，长度对应1:2，所以byte[]数组长度是字符串长度一半
        byte[] bytes = new byte[string.length() / 2];
        // 遍历byte[]数组，遍历次数是字符串长度一半
        for (int i = 0; i < string.length() / 2; i++) {
            // 截取没两个字符的前一个，将其转为int数值
            int high = Integer.parseInt(string.substring(i * 2, i * 2 + 1), 16);
            // 截取没两个字符的后一个，将其转为int数值
            int low = Integer.parseInt(string.substring(i * 2 + 1, i * 2 + 2), 16);
            // 高位字符对应的int值*16+低位的int值，强转成byte数值即可
            // 如dd，高位13*16+低位13=221(强转成byte二进制11011101，对应十进制-35)
            bytes[i] = (byte) (high * 16 + low);
        }
        return bytes;
    }

}
