package com.wll.constant;

public class BaseConstant {

    public static final String app = "app";

    public static final String app_code = "happyfruit2048";

    public static final String appCode = "app_code";

    public static String appName = "app_name";
    public static String appvc = "app_vc";
    public static String appvn = "app_vn";
    public static String is_debug = "is_debug";
    public static String google_id = "google_id";
    public static String register_date = "register_date";
    public static String register_time = "register_time";

    public static String data = "data";
    public static String token = "token";
    public static String cmd = "cmd";

    public static String device_id = "deviceId";
    public static String mac = "mac";
    public static String version = "version";
    public static String uuid = "uuid";
    public static String oaid = "oaid";
    public static String channel = "channel";
    public static String android_id = "android_id";
    public static String vendor = "vendor";
    public static String model = "model";
    public static String os_version = "os_version";

    public static String language = "language";
    public static String nextcount = "nextcount";
    public static String userId = "userId";
    public static String businessId = "businessId";
    public static String payType = "payType";
    public static String platform_product_id = "platformProductId";
    public static String params = "params";
    public static String CloudInfo = "CloudInfo";
    public static String msg = "msg";
    public static String local_language = "local_language";

    public static String ecpm = "ecpm";

    public static String ad_platform = "adPlatform";

    public static String result_type = "resultType";
    public static String result_msg = "resultMsg";

    public static String tips = "tips";

    public static String url = "url";

    public static String event = "event";
    public static String event_msg = "event_msg";

    public static String event_json = "event_json";


    public static String ad_album_count="ad_album_count";

    public static String ad_challenge_count="ad_challenge_count";

}
