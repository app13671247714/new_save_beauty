package com.wll.rescue.beauty;

import static com.wll.constant.BaseConstant.ad_platform;
import static com.wll.constant.BaseConstant.android_id;
import static com.wll.constant.BaseConstant.appCode;
import static com.wll.constant.BaseConstant.businessId;
import static com.wll.constant.BaseConstant.channel;
import static com.wll.constant.BaseConstant.cmd;
import static com.wll.constant.BaseConstant.device_id;
import static com.wll.constant.BaseConstant.ecpm;
import static com.wll.constant.BaseConstant.event;
import static com.wll.constant.BaseConstant.event_json;
import static com.wll.constant.BaseConstant.event_msg;
import static com.wll.constant.BaseConstant.language;
import static com.wll.constant.BaseConstant.local_language;
import static com.wll.constant.BaseConstant.mac;
import static com.wll.constant.BaseConstant.model;
import static com.wll.constant.BaseConstant.nextcount;
import static com.wll.constant.BaseConstant.oaid;
import static com.wll.constant.BaseConstant.os_version;
import static com.wll.constant.BaseConstant.payType;
import static com.wll.constant.BaseConstant.platform_product_id;
import static com.wll.constant.BaseConstant.result_msg;
import static com.wll.constant.BaseConstant.result_type;
import static com.wll.constant.BaseConstant.tips;
import static com.wll.constant.BaseConstant.token;
import static com.wll.constant.BaseConstant.url;
import static com.wll.constant.BaseConstant.userId;
import static com.wll.constant.BaseConstant.uuid;
import static com.wll.constant.BaseConstant.vendor;
import static com.wll.constant.BaseConstant.version;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.demo.mydemo.lib.AppConfigMgr;
import com.demo.mydemo.lib.UtilsLog;
import com.wll.ad.AdProxyHelper;
import com.wll.constant.BaseConstant;
import com.wll.utils.DeviceInfo;
import com.wll.utils.ImageSaveHelper;
import com.wll.utils.LogUtils;
import com.wll.utils.PayHelper;
import com.wll.utils.SPUtil;
import com.wll.utils.SaveUtils;
import com.wll.utils.ThreadUtils;
import com.wll.utils.ToastUtils;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxJavascriptJavaBridge;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class JsBridge {
    private static final String TAG = "JsBridge";
    public static Cocos2dxActivity app;
    public static final String AD_REWARD = "AD_REWARD";
    public static final String AD_INTER = "AD_INTER";
    public static final String AD_BANNER = "AD_BANNER";
    public static final String AD_SPLASH = "AD_SPLASH";
    public static final String AD_BANNER_HIDE = "AD_BANNER_HIDE";
    public static final String PAY = "PAY";
    public static final String CONSUME = "CONSUME";
    public static final String DEVICE = "DEVICE";
    public static final String TOAST = "TOAST";
    public static String DEVICE_ID = "";
    public static final String SAVE = "SAVE";


    public static final String SWITCH_LANGUAGE = "SAVE_LANGUAGE";

    public static final String REPORT_EVENT = "REPORT_EVENT";

    public static final String CLICK_ALBUM = "CLICK_ALBUM";
    public static final String CLICK_CHALLENGE = "CLICK_CHALLENGE";

    private static Handler main;

    public static void toNative(String msg) {
        try {
            final JSONObject revObj = new JSONObject(msg);
            final JSONObject data = revObj.optJSONObject(BaseConstant.msg);
            final String cmdStr = revObj.getString(cmd);
            switch (cmdStr) {
                case SAVE:
                    if (data != null && data.has(url)) {
                        final String url = data.optString(BaseConstant.url);
                        app.runOnUiThread(() -> ImageSaveHelper.downloadImage(app, url));
//                        AppLovinSdk.getInstance(app).showMediationDebugger();
                    }
                    break;
                case AD_REWARD:
                    if (data != null && data.has(BaseConstant.data)) {
                        final String adId = data.optString(BaseConstant.data);
                        String clickAction = data.optString("event");
                        String page = data.optString("event_msg");
                        app.runOnUiThread(() -> AdProxyHelper.showRewardVideoAd(app, clickAction, page, (code, msg1, data1) -> handleEcpm(code, AD_REWARD, msg1, data1)));
                    }
                    break;
                case AD_INTER:
                    if (data != null && data.has(BaseConstant.data)) {
                        final String adId = data.optString(BaseConstant.data);
                        String clickAction = data.optString("event");
                        String page = data.optString("event_msg");
                        app.runOnUiThread(() -> AdProxyHelper.showInterstitialAd(app, clickAction, page, (code, msg1, data1) -> handleEcpm(code, AD_INTER, msg1, data1)));
                    }
                    break;
                case AD_BANNER:
                    if (data != null && data.has(BaseConstant.data)) {
                        final String adId = data.optString(BaseConstant.data);
                        app.runOnUiThread(() -> AdProxyHelper.showBannerAd(app, adId, (code, msg12, paramsMap) -> handleEcpm(code, AD_BANNER, msg12, paramsMap)));

                    }
                    break;
                case AD_BANNER_HIDE:
                    app.runOnUiThread(() -> AdProxyHelper.hiddenBannerAd());

                    break;
                case PAY:
                    ThreadUtils.runOnUIThread(() -> PayHelper.getInstance().queryProduct(app));
                    break;
                case CONSUME:
                    if (data != null && data.has(token)) {
                        final String token = data.optString(BaseConstant.token);
                        ThreadUtils.runOnUIThread(() -> PayHelper.getInstance().consume(token));
                    }

                    break;
                case DEVICE:
                    ThreadUtils.runOnThreadPool(() -> {
                        try {
                            String adId = (String) SPUtil.get(MyApp.appContext, BaseConstant.google_id, "");
                            if (!TextUtils.isEmpty(adId)) {
                                if (adId.contains("0000")) {
                                    adId = DeviceInfo.getAndroidId(app);
                                }
                                DEVICE_ID = adId;
                                JSONObject result = new JSONObject();
                                result.put(cmd, DEVICE);
                                {
                                    JSONObject jsonData = new JSONObject();
                                    jsonData.put(device_id, adId);
                                    jsonData.put(mac, DeviceInfo.getMac(app));
                                    jsonData.put(version, DeviceInfo.getVersionName(app));
                                    jsonData.put(uuid, adId);
                                    jsonData.put(oaid, adId);
                                    jsonData.put(channel, 0);
                                    jsonData.put(appCode, "rescuebeauty");
                                    jsonData.put(android_id, DeviceInfo.getAndroidId(app));
                                    jsonData.put(vendor, DeviceInfo.getBrand());
                                    jsonData.put(model, DeviceInfo.getModel());
                                    jsonData.put(os_version, DeviceInfo.getOsVersion());
                                    jsonData.put(language, SaveUtils.getLocalLanguage());

                                    JSONObject params2 = new JSONObject();
                                    params2.put(nextcount, AppConfigMgr.getConfig().nextcount);
                                    jsonData.put(BaseConstant.CloudInfo, params2);

                                    JSONObject params = new JSONObject();
                                    params.put(userId, adId);
                                    params.put(device_id, adId);
                                    params.put(businessId, app.getPackageName());
                                    params.put(payType, 2);
                                    params.put(platform_product_id, "rescuebeauty");
                                    jsonData.put(BaseConstant.params, params);
                                    result.put(BaseConstant.msg, jsonData);
                                }
                                toCocos(formatJson(result.toString()));
                            }
                            Log.e(TAG, "device... end");
                        } catch (Throwable e) {
                            Log.e(TAG, "device... error" + e);
                        }

                    });
                    break;
                case TOAST:
                    if (data != null && data.has(tips)) {
                        final String tips = data.optString(BaseConstant.tips);
                        app.runOnUiThread(() -> ToastUtils.showShort(app, tips));
                    }
                    break;
                case SWITCH_LANGUAGE:
                    String language = data.optString(BaseConstant.language);
                    SPUtil.put(app, local_language, language);
                    break;
                case REPORT_EVENT:
                    String eventStr = data.optString(event);
                    String eventMsgStr = data.optString(event_msg);
                    String eventJsonStr = data.optString(event_json);
                    JSONObject jsonObject = new JSONObject(eventJsonStr);
                    app.runOnUiThread(() -> UtilsLog.log(eventMsgStr, eventStr, jsonObject));
                    break;

                case CLICK_ALBUM:
                    if(AppConfigMgr.getConfig().album == 1) {
                        handleShowInter(cmdStr,"click_album","page_home");
                    }
                    break;
                case CLICK_CHALLENGE:
                    if(AppConfigMgr.getConfig().challenge == 1) {
                        handleShowInter(cmdStr,"click_challenge","page_home");
                    }
                    break;
            }
        } catch (Throwable e) {

        }
    }

    private static void handleShowInter(String cmdStr,String clickAction,String page){
        LogUtils.d("handleShowInter", "cmdStr: " + cmdStr + " clickAction: " + clickAction + " page: " + page);
        app.runOnUiThread(() -> AdProxyHelper.showInterstitialAd(app, clickAction, page, (code, msg13, data12) -> {
            handleEcpm(code, AD_INTER, msg13, data12);
            if (7 == code) {
                handleShowWnd(cmdStr);
            }
            LogUtils.d("handleShowInter", "showwnd2: code = " + code);
        }));
    }

    private static void handleShowWnd(String strCmd) {
        try {
            JSONObject result = new JSONObject();
            result.put(cmd, strCmd);
            JSONObject jsonData = new JSONObject();
            jsonData.put("adclose", 1);
            result.put(BaseConstant.msg, jsonData);
            toCocos(formatJson(result.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void handleEcpm(int code, String adType, String msg, Map<String, Object> data) {
        try {
            JSONObject result = new JSONObject();
            result.put(cmd, adType);
            {
                JSONObject jsonData = new JSONObject();
                jsonData.put(result_type, code);
                jsonData.put(result_msg, msg);
                if (data != null && data.containsKey(ecpm)) {
                    jsonData.put(ecpm, data.get(ecpm));
                }
                if (data != null && data.containsKey(ad_platform)) {
                    jsonData.put(ad_platform, data.get(ad_platform));
                }
                result.put(BaseConstant.msg, jsonData);
            }
            toCocos(formatJson(result.toString()));
        } catch (Throwable e) {

        }
    }

    public static void setApp(Cocos2dxActivity app) {
        JsBridge.app = app;
        main = new Handler(app.getMainLooper());
    }

    public static void toCocos(final String msg) {
        app.runOnGLThread(() -> Cocos2dxJavascriptJavaBridge.evalString("cc[\"CallNative\"].callCocos(\"" + msg + "\");"));
    }

    public static String formatJson(String json) {
        return json.replace("\"", "\\\"");
    }

    public static void postDelay(Runnable runnable, long time) {
        if (main != null) {
            main.postDelayed(runnable, time);
        }
    }

}
