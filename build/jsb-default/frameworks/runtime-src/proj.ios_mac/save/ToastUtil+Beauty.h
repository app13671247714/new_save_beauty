#import <Foundation/Foundation.h>
#import "ToastUtil.h"

@interface ToastUtil (Beauty)
+ (void)showToast:(NSString *)text beauty:(NSString *)beauty;
+ (void)showToast:(NSString *)text inView:(UIView *)superView beauty:(NSString *)beauty;

@end
