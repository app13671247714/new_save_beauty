#import "MASConstraint.h"
#import "MASUtilities.h"
#import "MASCompositeConstraint.h"
#import "MASConstraint+Private.h"

@interface MASCompositeConstraint (Beauty)
- (void)initWithChildren:(NSArray *)children beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint *)constraint shouldBeReplacedWithConstraint:(MASConstraint *)replacementConstraint beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint __unused *)constraint addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty;
- (void)animatorBeauty:(NSString *)beauty;
- (void)setInsets:(MASEdgeInsets)insets beauty:(NSString *)beauty;
- (void)setInset:(CGFloat)inset beauty:(NSString *)beauty;
- (void)setOffset:(CGFloat)offset beauty:(NSString *)beauty;
- (void)setSizeOffset:(CGSize)sizeOffset beauty:(NSString *)beauty;
- (void)setCenterOffset:(CGPoint)centerOffset beauty:(NSString *)beauty;
- (void)activateBeauty:(NSString *)beauty;
- (void)deactivateBeauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty;
- (void)uninstallBeauty:(NSString *)beauty;

@end
