#import "MASUtilities.h"
#import "MASViewAttribute.h"
#import "MASViewAttribute+Beauty.h"

@interface MASViewAttribute (BeautyBeauty)
- (void)initWithView:(MAS_VIEW *)view layoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)initWithView:(MAS_VIEW *)view item:(id)item layoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)isSizeAttributeBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)isEqual:(MASViewAttribute *)viewAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)hashBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
