#import <Foundation/Foundation.h>
#import "SDKWrapper.h"
#import "SDKDelegate.h"

@interface SDKWrapper (Beauty)
+ (void)getInstanceBeauty:(NSString *)beauty;
+ (void)allocWithZone:(struct _NSZone *)zone beauty:(NSString *)beauty;
+ (void)copyWithZone:(struct _NSZone *)zone beauty:(NSString *)beauty;
- (void)initSDKWrapperBeauty:(NSString *)beauty;
- (void)loadSDKClassBeauty:(NSString *)beauty;
- (void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions beauty:(NSString *)beauty;
- (void)applicationDidBecomeActive:(UIApplication *)application beauty:(NSString *)beauty;
- (void)applicationWillResignActive:(UIApplication *)application beauty:(NSString *)beauty;
- (void)applicationDidEnterBackground:(UIApplication *)application beauty:(NSString *)beauty;
- (void)applicationWillEnterForeground:(UIApplication *)application beauty:(NSString *)beauty;
- (void)applicationWillTerminate:(UIApplication *)application beauty:(NSString *)beauty;

@end
