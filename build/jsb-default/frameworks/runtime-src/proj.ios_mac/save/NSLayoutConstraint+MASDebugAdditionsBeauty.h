#import "MASUtilities.h"
#import "NSLayoutConstraint+MASDebugAdditions.h"
#import "MASConstraint.h"
#import "MASLayoutConstraint.h"

@interface NSLayoutConstraint (MASDebugAdditionsBeauty)
+ (void)layoutRelationDescriptionsByValueBeauty:(NSString *)beauty;
+ (void)layoutAttributeDescriptionsByValueBeauty:(NSString *)beauty;
+ (void)layoutPriorityDescriptionsByValueBeauty:(NSString *)beauty;
+ (void)descriptionForObject:(id)obj beauty:(NSString *)beauty;
- (void)descriptionBeauty:(NSString *)beauty;

@end
