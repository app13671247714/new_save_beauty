#import "MASUtilities.h"
#import "MASConstraintMaker.h"
#import "MASViewAttribute.h"
#import "NSArray+MASAdditions.h"
#import "View+MASAdditions.h"

@interface NSArray (MASAdditionsBeauty)
- (void)mas_makeConstraints:(void(^)(MASConstraintMaker *make))block beauty:(NSString *)beauty;
- (void)mas_updateConstraints:(void(^)(MASConstraintMaker *make))block beauty:(NSString *)beauty;
- (void)mas_remakeConstraints:(void(^)(MASConstraintMaker *make))block beauty:(NSString *)beauty;
- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType withFixedSpacing:(CGFloat)fixedSpacing leadSpacing:(CGFloat)leadSpacing tailSpacing:(CGFloat)tailSpacing beauty:(NSString *)beauty;
- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType withFixedItemLength:(CGFloat)fixedItemLength leadSpacing:(CGFloat)leadSpacing tailSpacing:(CGFloat)tailSpacing beauty:(NSString *)beauty;
- (void)mas_commonSuperviewOfViewsBeauty:(NSString *)beauty;

@end
