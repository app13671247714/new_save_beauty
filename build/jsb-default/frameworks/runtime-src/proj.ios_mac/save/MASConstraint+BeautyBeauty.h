#import "MASUtilities.h"
#import "MASConstraint.h"
#import "MASConstraint+Private.h"
#import "MASConstraint+Beauty.h"

@interface MASConstraint (BeautyBeauty)
- (void)initBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setLayoutConstantWithValue:(NSValue *)value beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)withBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)andBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute __unused)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leftBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)topBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)rightBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)bottomBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leadingBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)trailingBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)widthBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)heightBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerXBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerYBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)baselineBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)firstBaselineBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)lastBaselineBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leftMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)rightMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)topMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)bottomMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leadingMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)trailingMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerXWithinMarginsBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerYWithinMarginsBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setInsets:(MASEdgeInsets __unused)insets beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setInset:(CGFloat __unused)inset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setSizeOffset:(CGSize __unused)sizeOffset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setCenterOffset:(CGPoint __unused)centerOffset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setOffset:(CGFloat __unused)offset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)animatorBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)activateBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)deactivateBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)uninstallBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
