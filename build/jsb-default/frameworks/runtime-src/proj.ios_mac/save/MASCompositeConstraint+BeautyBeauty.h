#import "MASConstraint.h"
#import "MASUtilities.h"
#import "MASCompositeConstraint.h"
#import "MASConstraint+Private.h"
#import "MASCompositeConstraint+Beauty.h"

@interface MASCompositeConstraint (BeautyBeauty)
- (void)initWithChildren:(NSArray *)children beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint *)constraint shouldBeReplacedWithConstraint:(MASConstraint *)replacementConstraint beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint __unused *)constraint addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)animatorBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setInsets:(MASEdgeInsets)insets beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setInset:(CGFloat)inset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setOffset:(CGFloat)offset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setSizeOffset:(CGSize)sizeOffset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setCenterOffset:(CGPoint)centerOffset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)activateBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)deactivateBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)uninstallBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
