#import <Foundation/Foundation.h>
#import "ToastUtil.h"
#import "ToastUtil+Beauty.h"

@interface ToastUtil (BeautyBeauty)
+ (void)showToast:(NSString *)text beauty:(NSString *)beauty beauty:(NSString *)beauty;
+ (void)showToast:(NSString *)text inView:(UIView *)superView beauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
