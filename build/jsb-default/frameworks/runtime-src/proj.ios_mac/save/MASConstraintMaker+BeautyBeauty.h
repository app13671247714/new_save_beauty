#import "MASConstraint.h"
#import "MASUtilities.h"
#import "MASConstraintMaker.h"
#import "MASViewConstraint.h"
#import "MASCompositeConstraint.h"
#import "MASConstraint+Private.h"
#import "MASViewAttribute.h"
#import "View+MASAdditions.h"
#import "MASConstraintMaker+Beauty.h"

@interface MASConstraintMaker (BeautyBeauty)
- (void)initWithView:(MAS_VIEW *)view beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint *)constraint shouldBeReplacedWithConstraint:(MASConstraint *)replacementConstraint beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint *)constraint addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)addConstraintWithAttributes:(MASAttribute)attrs beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leftBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)topBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)rightBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)bottomBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leadingBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)trailingBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)widthBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)heightBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerXBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerYBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)baselineBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)firstBaselineBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)lastBaselineBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leftMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)rightMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)topMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)bottomMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)leadingMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)trailingMarginBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerXWithinMarginsBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerYWithinMarginsBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)edgesBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)sizeBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)centerBeauty:(NSString *)beauty beauty:(NSString *)beauty;
@end
