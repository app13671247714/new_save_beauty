#import "MASConstraint.h"
#import "MASUtilities.h"
#import "MASConstraintMaker.h"
#import "MASViewConstraint.h"
#import "MASCompositeConstraint.h"
#import "MASConstraint+Private.h"
#import "MASViewAttribute.h"
#import "View+MASAdditions.h"

@interface MASConstraintMaker (Beauty)
- (void)initWithView:(MAS_VIEW *)view beauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty;
- (void)constraint:(MASConstraint *)constraint shouldBeReplacedWithConstraint:(MASConstraint *)replacementConstraint beauty:(NSString *)beauty;
- (void)constraint:(MASConstraint *)constraint addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty;
- (void)addConstraintWithAttributes:(MASAttribute)attrs beauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty;
- (void)leftBeauty:(NSString *)beauty;
- (void)topBeauty:(NSString *)beauty;
- (void)rightBeauty:(NSString *)beauty;
- (void)bottomBeauty:(NSString *)beauty;
- (void)leadingBeauty:(NSString *)beauty;
- (void)trailingBeauty:(NSString *)beauty;
- (void)widthBeauty:(NSString *)beauty;
- (void)heightBeauty:(NSString *)beauty;
- (void)centerXBeauty:(NSString *)beauty;
- (void)centerYBeauty:(NSString *)beauty;
- (void)baselineBeauty:(NSString *)beauty;
- (void)firstBaselineBeauty:(NSString *)beauty;
- (void)lastBaselineBeauty:(NSString *)beauty;
- (void)leftMarginBeauty:(NSString *)beauty;
- (void)rightMarginBeauty:(NSString *)beauty;
- (void)topMarginBeauty:(NSString *)beauty;
- (void)bottomMarginBeauty:(NSString *)beauty;
- (void)leadingMarginBeauty:(NSString *)beauty;
- (void)trailingMarginBeauty:(NSString *)beauty;
- (void)centerXWithinMarginsBeauty:(NSString *)beauty;
- (void)centerYWithinMarginsBeauty:(NSString *)beauty;
- (void)edgesBeauty:(NSString *)beauty;
- (void)sizeBeauty:(NSString *)beauty;
- (void)centerBeauty:(NSString *)beauty;

@end
