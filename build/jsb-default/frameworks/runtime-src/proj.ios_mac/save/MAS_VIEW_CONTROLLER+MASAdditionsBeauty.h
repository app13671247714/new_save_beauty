#import "MASUtilities.h"
#import "MASConstraintMaker.h"
#import "MASViewAttribute.h"
#import "ViewController+MASAdditions.h"

@interface MAS_VIEW_CONTROLLER (MASAdditionsBeauty)
- (void)mas_topLayoutGuideBeauty:(NSString *)beauty;
- (void)mas_topLayoutGuideTopBeauty:(NSString *)beauty;
- (void)mas_topLayoutGuideBottomBeauty:(NSString *)beauty;
- (void)mas_bottomLayoutGuideBeauty:(NSString *)beauty;
- (void)mas_bottomLayoutGuideTopBeauty:(NSString *)beauty;
- (void)mas_bottomLayoutGuideBottomBeauty:(NSString *)beauty;

@end
