#import "MASViewAttribute.h"
#import "MASConstraint.h"
#import "MASLayoutConstraint.h"
#import "MASUtilities.h"
#import "MASViewConstraint.h"
#import "MASConstraint+Private.h"
#import "MASCompositeConstraint.h"
#import "MASLayoutConstraint.h"
#import "View+MASAdditions.h"
#import <objc/runtime.h>
#import "MAS_VIEW+MASConstraintsBeauty.h"

@interface MAS_VIEW (MASConstraintsBeautyBeauty)
- (void)mas_installedConstraintsBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
