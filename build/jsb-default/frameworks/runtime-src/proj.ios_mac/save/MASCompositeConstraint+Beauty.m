#import "MASCompositeConstraint+Beauty.h"
@implementation MASCompositeConstraint (Beauty)
- (void)initWithChildren:(NSArray *)children beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)constraint:(MASConstraint *)constraint shouldBeReplacedWithConstraint:(MASConstraint *)replacementConstraint beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)constraint:(MASConstraint __unused *)constraint addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)animatorBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)setInsets:(MASEdgeInsets)insets beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)setInset:(CGFloat)inset beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)setOffset:(CGFloat)offset beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)setSizeOffset:(CGSize)sizeOffset beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)setCenterOffset:(CGPoint)centerOffset beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)activateBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)deactivateBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)installBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)uninstallBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}

@end
