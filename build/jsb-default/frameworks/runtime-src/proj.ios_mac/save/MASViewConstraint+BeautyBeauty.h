#import "MASViewAttribute.h"
#import "MASConstraint.h"
#import "MASLayoutConstraint.h"
#import "MASUtilities.h"
#import "MASViewConstraint.h"
#import "MASConstraint+Private.h"
#import "MASCompositeConstraint.h"
#import "MASLayoutConstraint.h"
#import "View+MASAdditions.h"
#import <objc/runtime.h>
#import "MASViewConstraint+Beauty.h"

@interface MASViewConstraint (BeautyBeauty)
- (void)initWithFirstViewAttribute:(MASViewAttribute *)firstViewAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)copyWithZone:(NSZone __unused *)zone beauty:(NSString *)beauty beauty:(NSString *)beauty;
+ (void)installedConstraintsForView:(MAS_VIEW *)view beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setLayoutConstant:(CGFloat)layoutConstant beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setLayoutRelation:(NSLayoutRelation)layoutRelation beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)supportsActivePropertyBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)isActiveBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)hasBeenInstalledBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setSecondViewAttribute:(id)secondViewAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)withBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)andBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)animatorBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setInsets:(MASEdgeInsets)insets beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setInset:(CGFloat)inset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setOffset:(CGFloat)offset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setSizeOffset:(CGSize)sizeOffset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)setCenterOffset:(CGPoint)centerOffset beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)activateBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)deactivateBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)layoutConstraintSimilarTo:(MASLayoutConstraint *)layoutConstraint beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)uninstallBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
