#import "MASUtilities.h"
#import "MASViewAttribute.h"

@interface MASViewAttribute (Beauty)
- (void)initWithView:(MAS_VIEW *)view layoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty;
- (void)initWithView:(MAS_VIEW *)view item:(id)item layoutAttribute:(NSLayoutAttribute)layoutAttribute beauty:(NSString *)beauty;
- (void)isSizeAttributeBeauty:(NSString *)beauty;
- (void)isEqual:(MASViewAttribute *)viewAttribute beauty:(NSString *)beauty;
- (void)hashBeauty:(NSString *)beauty;

@end
