#import "MASUtilities.h"
#import "MASConstraintMaker.h"
#import "MASViewAttribute.h"
#import "View+MASAdditions.h"
#import <objc/runtime.h>

@interface MAS_VIEW (MASAdditionsBeauty)
- (void)mas_makeConstraints:(void(^)(MASConstraintMaker *))block beauty:(NSString *)beauty;
- (void)mas_updateConstraints:(void(^)(MASConstraintMaker *))block beauty:(NSString *)beauty;
- (void)mas_remakeConstraints:(void(^)(MASConstraintMaker *make))block beauty:(NSString *)beauty;
- (void)mas_leftBeauty:(NSString *)beauty;
- (void)mas_topBeauty:(NSString *)beauty;
- (void)mas_rightBeauty:(NSString *)beauty;
- (void)mas_bottomBeauty:(NSString *)beauty;
- (void)mas_leadingBeauty:(NSString *)beauty;
- (void)mas_trailingBeauty:(NSString *)beauty;
- (void)mas_widthBeauty:(NSString *)beauty;
- (void)mas_heightBeauty:(NSString *)beauty;
- (void)mas_centerXBeauty:(NSString *)beauty;
- (void)mas_centerYBeauty:(NSString *)beauty;
- (void)mas_baselineBeauty:(NSString *)beauty;
- (void)mas_lastBaselineBeauty:(NSString *)beauty;
- (void)mas_leftMarginBeauty:(NSString *)beauty;
- (void)mas_rightMarginBeauty:(NSString *)beauty;
- (void)mas_topMarginBeauty:(NSString *)beauty;
- (void)mas_bottomMarginBeauty:(NSString *)beauty;
- (void)mas_leadingMarginBeauty:(NSString *)beauty;
- (void)mas_trailingMarginBeauty:(NSString *)beauty;
- (void)mas_centerXWithinMarginsBeauty:(NSString *)beauty;
- (void)mas_centerYWithinMarginsBeauty:(NSString *)beauty;
- (void)mas_safeAreaLayoutGuideBeauty:(NSString *)beauty;
- (void)mas_safeAreaLayoutGuideTopBeauty:(NSString *)beauty;
- (void)mas_safeAreaLayoutGuideBottomBeauty:(NSString *)beauty;
- (void)mas_safeAreaLayoutGuideLeftBeauty:(NSString *)beauty;
- (void)mas_safeAreaLayoutGuideRightBeauty:(NSString *)beauty;
- (void)mas_keyBeauty:(NSString *)beauty;
- (void)setMas_key:(id)key beauty:(NSString *)beauty;
- (void)mas_closestCommonSuperview:(MAS_VIEW *)view beauty:(NSString *)beauty;

@end
