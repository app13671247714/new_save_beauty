#import "MASUtilities.h"
#import "MASConstraintMaker.h"
#import "MASViewAttribute.h"
#import "ViewController+MASAdditions.h"
#import "MAS_VIEW_CONTROLLER+MASAdditionsBeauty.h"

@interface MAS_VIEW_CONTROLLER (MASAdditionsBeautyBeauty)
- (void)mas_topLayoutGuideBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)mas_topLayoutGuideTopBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)mas_topLayoutGuideBottomBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)mas_bottomLayoutGuideBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)mas_bottomLayoutGuideTopBeauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)mas_bottomLayoutGuideBottomBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
