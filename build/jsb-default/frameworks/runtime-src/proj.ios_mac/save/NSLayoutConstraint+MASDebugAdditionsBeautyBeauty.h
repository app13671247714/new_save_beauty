#import "MASUtilities.h"
#import "NSLayoutConstraint+MASDebugAdditions.h"
#import "MASConstraint.h"
#import "MASLayoutConstraint.h"
#import "NSLayoutConstraint+MASDebugAdditionsBeauty.h"

@interface NSLayoutConstraint (MASDebugAdditionsBeautyBeauty)
+ (void)layoutRelationDescriptionsByValueBeauty:(NSString *)beauty beauty:(NSString *)beauty;
+ (void)layoutAttributeDescriptionsByValueBeauty:(NSString *)beauty beauty:(NSString *)beauty;
+ (void)layoutPriorityDescriptionsByValueBeauty:(NSString *)beauty beauty:(NSString *)beauty;
+ (void)descriptionForObject:(id)obj beauty:(NSString *)beauty beauty:(NSString *)beauty;
- (void)descriptionBeauty:(NSString *)beauty beauty:(NSString *)beauty;

@end
