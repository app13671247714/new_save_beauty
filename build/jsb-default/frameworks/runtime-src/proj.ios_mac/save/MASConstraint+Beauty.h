#import "MASUtilities.h"
#import "MASConstraint.h"
#import "MASConstraint+Private.h"

@interface MASConstraint (Beauty)
- (void)initBeauty:(NSString *)beauty;
- (void)setLayoutConstantWithValue:(NSValue *)value beauty:(NSString *)beauty;
- (void)withBeauty:(NSString *)beauty;
- (void)andBeauty:(NSString *)beauty;
- (void)addConstraintWithLayoutAttribute:(NSLayoutAttribute __unused)layoutAttribute beauty:(NSString *)beauty;
- (void)leftBeauty:(NSString *)beauty;
- (void)topBeauty:(NSString *)beauty;
- (void)rightBeauty:(NSString *)beauty;
- (void)bottomBeauty:(NSString *)beauty;
- (void)leadingBeauty:(NSString *)beauty;
- (void)trailingBeauty:(NSString *)beauty;
- (void)widthBeauty:(NSString *)beauty;
- (void)heightBeauty:(NSString *)beauty;
- (void)centerXBeauty:(NSString *)beauty;
- (void)centerYBeauty:(NSString *)beauty;
- (void)baselineBeauty:(NSString *)beauty;
- (void)firstBaselineBeauty:(NSString *)beauty;
- (void)lastBaselineBeauty:(NSString *)beauty;
- (void)leftMarginBeauty:(NSString *)beauty;
- (void)rightMarginBeauty:(NSString *)beauty;
- (void)topMarginBeauty:(NSString *)beauty;
- (void)bottomMarginBeauty:(NSString *)beauty;
- (void)leadingMarginBeauty:(NSString *)beauty;
- (void)trailingMarginBeauty:(NSString *)beauty;
- (void)centerXWithinMarginsBeauty:(NSString *)beauty;
- (void)centerYWithinMarginsBeauty:(NSString *)beauty;
- (void)setInsets:(MASEdgeInsets __unused)insets beauty:(NSString *)beauty;
- (void)setInset:(CGFloat __unused)inset beauty:(NSString *)beauty;
- (void)setSizeOffset:(CGSize __unused)sizeOffset beauty:(NSString *)beauty;
- (void)setCenterOffset:(CGPoint __unused)centerOffset beauty:(NSString *)beauty;
- (void)setOffset:(CGFloat __unused)offset beauty:(NSString *)beauty;
- (void)animatorBeauty:(NSString *)beauty;
- (void)activateBeauty:(NSString *)beauty;
- (void)deactivateBeauty:(NSString *)beauty;
- (void)installBeauty:(NSString *)beauty;
- (void)uninstallBeauty:(NSString *)beauty;

@end
