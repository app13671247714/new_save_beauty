#import "NSLayoutConstraint+MASDebugAdditionsBeauty.h"
@implementation NSLayoutConstraint (MASDebugAdditionsBeauty)
+ (void)layoutRelationDescriptionsByValueBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
+ (void)layoutAttributeDescriptionsByValueBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
+ (void)layoutPriorityDescriptionsByValueBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
+ (void)descriptionForObject:(id)obj beauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}
- (void)descriptionBeauty:(NSString *)beauty {
    NSLog(@"%@", beauty);
}

@end
