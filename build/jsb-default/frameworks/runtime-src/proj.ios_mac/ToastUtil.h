#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface ToastUtil : NSObject
+ (void)showToast:(NSString *)text;
+ (void)showToast:(NSString *)text inView:(UIView *)superView;
@end
NS_ASSUME_NONNULL_END
