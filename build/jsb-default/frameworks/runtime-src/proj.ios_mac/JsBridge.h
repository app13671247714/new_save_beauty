#import <Foundation/Foundation.h>
@interface JsBridge: NSObject
+(instancetype)bridge;
+(void)toNative:(NSString *) msg;
+(void)callCocos:(NSDictionary *) msg;
@end
