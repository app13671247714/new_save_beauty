#import "JsBridge.h"
#import "cocos2d.h"
#include <cocos/base/CCScheduler.h>
#include <cocos/platform/CCApplication.h>
#include <cocos/scripting/js-bindings/jswrapper/SeApi.h>
#import "ToastUtil.h"
#import "SSKeychain.h"

@implementation JsBridge

#pragma mark - LifeCycle
+(instancetype)bridge {
    static dispatch_once_t onceToken;
    static JsBridge *instance;
    dispatch_once(&onceToken, ^{
        instance = [[JsBridge alloc] init];
    });
    return instance;
}


+(void)toNative:(NSString *) msg{
    // JSON的反序列化
    NSData *jsonData = [msg dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    NSString *cmd = [dic objectForKey:@"cmd"];
    if([cmd isEqualToString:@"DEVICE"]){
        NSString *deviceId = [self getDeviceUUID];
        NSDictionary *params = @{@"deviceId":deviceId, @"userId":deviceId, @"businessId":@"com.wll.rescue.beauty", @"payType":@1, @"platformProductId":@"rescuebeauty"};
        NSDictionary *info = @{@"deviceId":deviceId, @"params":params};
        NSDictionary *back = @{@"cmd":@"DEVICE",@"msg":info};
        [JsBridge callCocos: back];
    } else if([cmd isEqualToString:@"SAVE"]){
        NSDictionary *data = [dic objectForKey:@"msg"];
        NSString *url = [data objectForKey:@"url"];
        NSData *imgData=[NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage *image=[UIImage imageWithData:imgData];
        [self saveImageAlbum:image];
    }
}

+(void)callCocos:(NSDictionary *) back {
    BOOL is_YES = [NSJSONSerialization isValidJSONObject:back];
    if (is_YES) {
        NSData *dataJson = [NSJSONSerialization dataWithJSONObject:back options:0 error:NULL];
        NSString *jsonString = [[NSString alloc]initWithData:dataJson encoding:NSUTF8StringEncoding];
        auto json = jsonString;
        cocos2d::Application::getInstance()->getScheduler()->performFunctionInCocosThread([json]{
            NSMutableString *mutStr = [NSMutableString stringWithString:json];
            //            printf("==============1=============%s \n", [mutStr UTF8String]);
            NSRange range = {0,json.length};
            [mutStr replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSLiteralSearch range:range];
            std::string json = [(NSString*)mutStr UTF8String];
            
            std::string jsCallStr = cocos2d::StringUtils::format("cc[\"CallNative\"].callCocos(\"%s\");", json.c_str());
            //            printf("==============1=============%s \n", [[NSString stringWithUTF8String:jsCallStr.c_str()] UTF8String]);
            se::ScriptEngine::getInstance()->evalString(jsCallStr.c_str());
        });
    }
    
}

#pragma mark - 保存图片到相册
+ (void)saveImageAlbum:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

+ (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        NSLog(@"保存失败！ error = %@", error);
        [ToastUtil showToast:@"save fail."];
    }
    else {
        [ToastUtil showToast:@"save success."];
        NSLog(@"保存成功！ image = %@, contextInfo = %@", image, contextInfo);
    }
}

+ (NSString *)getDeviceUUID{
    NSString *currentDeviceUUIDStr = [SSKeychain passwordForService:@"com.wll.rescue.beauty" account:@"uuid"];
    if (currentDeviceUUIDStr == nil || [currentDeviceUUIDStr isEqualToString:@""])
    {
        NSUUID *currentDeviceUUID  = [UIDevice currentDevice].identifierForVendor;
        currentDeviceUUIDStr = currentDeviceUUID.UUIDString;
        currentDeviceUUIDStr = [currentDeviceUUIDStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        currentDeviceUUIDStr = [currentDeviceUUIDStr lowercaseString];
        [SSKeychain setPassword: currentDeviceUUIDStr forService:@"com.wll.rescue.beauty" account:@"uuid"];
    }
    return currentDeviceUUIDStr;
}


@end
